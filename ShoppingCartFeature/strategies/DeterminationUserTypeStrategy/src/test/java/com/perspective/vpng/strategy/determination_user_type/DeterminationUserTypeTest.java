package com.perspective.vpng.strategy.determination_user_type;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class DeterminationUserTypeTest {

    private final IResolveDependencyStrategy strategy;

    public DeterminationUserTypeTest() {
        this.strategy = new DeterminationUserTypeStrategy();
    }

    @Test
    public void should_ReturnStandardUserType() throws Exception {
        IObject user = mock(IObject.class);
        String type = strategy.resolve(user);
        assertEquals(type, "standard");
    }

    @Test(expected = ResolveDependencyStrategyException.class)
    public void should_ThrowException_WithReason_IllegalArgumentType() throws Exception {
        Object user = new Object();
        strategy.resolve(user);
    }

}
