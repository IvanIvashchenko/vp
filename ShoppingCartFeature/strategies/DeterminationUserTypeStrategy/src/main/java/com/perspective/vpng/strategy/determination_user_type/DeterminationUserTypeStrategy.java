package com.perspective.vpng.strategy.determination_user_type;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

/**
 *
 */
public class DeterminationUserTypeStrategy implements IResolveDependencyStrategy {

    /**
     *
     * @param args
     * @param <T>
     * @return
     * @throws ResolveDependencyStrategyException
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject user = (IObject) args[0];
            return (T) "standard";
        } catch (Exception ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }

}
