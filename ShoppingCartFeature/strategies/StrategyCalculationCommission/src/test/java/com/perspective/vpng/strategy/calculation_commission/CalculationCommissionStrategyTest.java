package com.perspective.vpng.strategy.calculation_commission;

import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class CalculationCommissionStrategyTest {

    private static IResolveDependencyStrategy strategy;

    private static IField fixCommissionStrategyF = mock(IField.class);
    private static IField percentCommissionStrategyF = mock(IField.class);

    private static CalculationCommissionStrategy.ICommissionStrategy fixCommissionStrategy =
            mock(CalculationCommissionStrategy.ICommissionStrategy.class);
    private static CalculationCommissionStrategy.ICommissionStrategy percentCommissionStrategy =
            mock(CalculationCommissionStrategy.ICommissionStrategy.class);

    @BeforeClass
    public static void setUp() {
        int commissionScale = 2;
        Map<IField, CalculationCommissionStrategy.ICommissionStrategy> strategies =
                new HashMap<IField, CalculationCommissionStrategy.ICommissionStrategy>() {{
                    put(fixCommissionStrategyF, fixCommissionStrategy);
                    put(percentCommissionStrategyF, percentCommissionStrategy);
        }};

        strategy = new CalculationCommissionStrategy(commissionScale, strategies);
    }

    @Test
    public void calculateCommissionValueTest() throws Exception {
        final IObject commissionRule = mock(IObject.class);
        final String sumWithoutCommissionStr = "100";
        final BigDecimal sumWithoutCommission = new BigDecimal(sumWithoutCommissionStr);
        final BigDecimal fixCommissionValue = BigDecimal.valueOf(2);
        final BigDecimal percentCommissionValue = BigDecimal.valueOf(2);

        when(fixCommissionStrategyF.in(eq(commissionRule))).thenReturn(fixCommissionValue);
        when(percentCommissionStrategyF.in(eq(commissionRule))).thenReturn(percentCommissionValue);
        when(fixCommissionStrategy.calculate(eq(sumWithoutCommission), eq(fixCommissionValue)))
                .thenReturn(BigDecimal.valueOf(2));
        when(percentCommissionStrategy.calculate(eq(sumWithoutCommission), eq(percentCommissionValue)))
                .thenReturn(BigDecimal.valueOf(2));

        BigDecimal commissionValue = strategy.resolve(commissionRule, sumWithoutCommissionStr);

        verify(fixCommissionStrategyF).in(eq(commissionRule));
        verify(percentCommissionStrategyF).in(eq(commissionRule));
        verify(fixCommissionStrategy).calculate(eq(sumWithoutCommission), eq(fixCommissionValue));
        verify(percentCommissionStrategy).calculate(eq(sumWithoutCommission), eq(percentCommissionValue));

        assertEquals(commissionValue, new BigDecimal("4.00"));
    }

    @After
    public void resetMocks() {
        reset(
                fixCommissionStrategy, fixCommissionStrategyF,
                percentCommissionStrategy, percentCommissionStrategyF
        );
    }
}
