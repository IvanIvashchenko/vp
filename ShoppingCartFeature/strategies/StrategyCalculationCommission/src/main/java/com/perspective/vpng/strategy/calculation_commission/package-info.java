/**
 * Contains a rule for calculation value of invoice's commission.
 */
package com.perspective.vpng.strategy.calculation_commission;