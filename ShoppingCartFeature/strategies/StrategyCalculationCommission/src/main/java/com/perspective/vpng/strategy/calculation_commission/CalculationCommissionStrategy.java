package com.perspective.vpng.strategy.calculation_commission;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * Strategy for calculation invoice's commission.
 */
public class CalculationCommissionStrategy implements IResolveDependencyStrategy {

    private final int commissionScale;
    private final Map<IField, ICommissionStrategy> commissionStrategies;

    /**
     * Constructor.
     *
     * @param commissionScale - scale for calculation commission value.
     * @param commissionStrategies - strategies for calculation different commissions.
     */
    public CalculationCommissionStrategy(
            final int commissionScale,
            final Map<IField, ICommissionStrategy> commissionStrategies
    ) {
        this.commissionStrategies = new HashMap<>(commissionStrategies);
        this.commissionScale = commissionScale;
    }

    /**
     * Calculates invoice's commission by some rule.
     *
     * @param args - arguments for calculation invoice's commission;
     *             args[0] - commission rule type of {@link IObject} with different commissions.
     *             args[1] - invoice's sum without commission.
     *
     * @param <T> - type of resolution result.
     *
     * @return a invoice's commission value.
     *
     * @throws ResolveDependencyStrategyException when:
     *             1. Invalid value or type of commission rule or sum without commission.
     *             2. Couldn't read commissions field from commission rule.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject commissionRule = (IObject) args[0];
            BigDecimal sumWithoutCommission = convert(args[1]);
            BigDecimal commissionValue = BigDecimal.ZERO;
            for (Map.Entry<IField, ICommissionStrategy> strategy : commissionStrategies.entrySet()) {
                Object commission = strategy.getKey().in(commissionRule);
                if (commission != null) {
                    commissionValue = commissionValue.add(
                            strategy.getValue().calculate(sumWithoutCommission, convert(commission))
                    );
                }
            }

            return (T) commissionValue.setScale(commissionScale, RoundingMode.HALF_UP);
        } catch (NullPointerException ex) {
            throw new ResolveDependencyStrategyException("Invalid arguments!", ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Invalid type of commission rule: " + ex.getMessage(), ex);
        } catch (ReadValueException ex) {
            throw new ResolveDependencyStrategyException("Couldn't read commission from rule: " + ex.getMessage(), ex);
        } catch (InvalidArgumentException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }

    private BigDecimal convert(final Object object) {
        if (object instanceof BigDecimal) {
            return (BigDecimal) object;
        }
        if (object instanceof Number) {
            return new BigDecimal(((Number) object).doubleValue());
        }
        if (object instanceof String) {
            return new BigDecimal((String) object);
        }

        throw new ClassCastException("Couldn't casted object: [" +
                object.getClass().getCanonicalName() + "] to BigDecimal type!");
    }

    /**
     * Strategy for calculation a special type of commission.
     */
    public interface ICommissionStrategy {
        /**
         * Calculates special type of commission.
         *
         * @param sumWithoutCommission - invoice's sum without commission.
         * @param commission - invoice's commission object.
         *
         * @return a value of invoice's commission.
         */
        BigDecimal calculate(final BigDecimal sumWithoutCommission, final BigDecimal commission);
    }
}
