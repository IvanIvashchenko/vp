package com.perspective.vpng.rule.getting_commission_from_services;

import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
@SuppressWarnings("unchecked")
public class GettingCommissionFromServicesRuleTest {

    private static IResolveDependencyStrategy strategy;
    private static IField commissionF;

    @BeforeClass
    public static void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        commissionF = mock(IField.class);
        final String commissionFN = "testCommission";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), commissionFN))
                .thenReturn(commissionF);

        strategy = new GettingCommissionFromServicesRule(commissionFN);
    }

    @Test
    public void gettingCommissionFromServicesTest() throws Exception {
        IObject service1 = mock(IObject.class);
        IObject service2 = mock(IObject.class);
        IObject service3 = mock(IObject.class);
        IObject serviceCommission = mock(IObject.class);

        when(commissionF.in(service1)).thenReturn(false);
        when(commissionF.in(service2)).thenReturn("false");
        when(commissionF.in(service3)).thenReturn(null);
        when(commissionF.in(serviceCommission)).thenReturn(true);

        List<IObject> services = Arrays.asList(service1, service2, service3, serviceCommission);
        IObject resolvedServiceCommission = strategy.resolve(services);

        verify(commissionF, times(4)).in(anyObject());
        assertEquals(resolvedServiceCommission, serviceCommission);
    }

    @Test
    public void should_ThrowException_When_GettingCommission_WithReason_InvalidTypeOfCommissionIntoService()
            throws Exception {

        IObject service = mock(IObject.class);
        when(commissionF.in(service)).thenReturn(0);
        try {
            strategy.resolve(Collections.singletonList(service));
        } catch (ResolveDependencyStrategyException ex) {
            verify(commissionF).in(service);
            assertEquals(
                    ex.getMessage(),
                    "Value of commission field into invoice's service must be a " +
                            Boolean.class.getCanonicalName() + " or " +
                            String.class.getCanonicalName() +"type!"
            );
        }
    }

    @After
    public void resetMocks() {
        reset(commissionF);
    }
}
