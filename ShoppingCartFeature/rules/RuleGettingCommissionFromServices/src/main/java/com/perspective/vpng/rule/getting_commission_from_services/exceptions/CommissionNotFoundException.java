package com.perspective.vpng.rule.getting_commission_from_services.exceptions;

/**
 * Throws when commission-service couldn't found.
 */
public class CommissionNotFoundException extends RuntimeException {
    public CommissionNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
