/**
 * Contains rule for getting service-commission between another invoice's services.
 */
package com.perspective.vpng.rule.getting_commission_from_services;