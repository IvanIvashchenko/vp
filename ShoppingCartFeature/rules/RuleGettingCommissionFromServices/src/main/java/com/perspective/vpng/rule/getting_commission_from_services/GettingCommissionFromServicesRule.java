package com.perspective.vpng.rule.getting_commission_from_services;

import com.perspective.vpng.rule.getting_commission_from_services.exceptions.CommissionNotFoundException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Gets commission service between another invoice's services.
 * @see GettingCommissionFromServicesRule#resolve(Object...)
 */
public class GettingCommissionFromServicesRule implements IResolveDependencyStrategy {
    /** Field-flag for detection commission between services. */
    private final IField commissionF;
    private final Map<Class, IClassCastStrategy> classCastStrategies;

    /**
     * Default constructor.
     *
     * @param commissionFieldName - name of commission field.
     * @see GettingCommissionFromServicesRule#commissionF
     *
     * @throws ResolveDependencyStrategyException when field resolution errors.
     */
    public GettingCommissionFromServicesRule(final String commissionFieldName)
            throws ResolveDependencyStrategyException {
        try {
            this.commissionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), commissionFieldName);
            this.classCastStrategies = new HashMap<Class, IClassCastStrategy>() {{
                put(Boolean.class, object -> (Boolean) object);
                put(String.class, object -> Boolean.valueOf((String) object));
            }};
        } catch (ResolutionException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }

    /**
     * Takes a first find commission service between another invoice's services.
     *
     * @param args - resolution arguments;
     *             must contains list of invoice's services:
     *             "services" : [
     *               {...}, ...
     *             ]
     *             NOTE: if service contains a commission field and
     *             value of this field is true then this service is service-commission.
     *
     * @param <T> - type of rule return value.
     *
     * @return a service-commission type of {@link IObject}.
     *
     * @throws ResolveDependencyStrategyException when error handling given services.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            Stream<IObject> services = ((List<IObject>) args[0]).stream();
            Optional<IObject> commissionRuleOpt = services.filter(service -> {
                try {
                    Object tmpValue = commissionF.in(service);
                    return tmpValue != null ?
                            classCastStrategies.get(tmpValue.getClass()).cast(tmpValue) : false;
                } catch (ReadValueException | InvalidArgumentException ex) {
                    throw new CommissionNotFoundException(ex.getMessage(), ex);
                } catch (NullPointerException ex) {
                    throw new CommissionNotFoundException(
                            "Value of commission field into invoice's service must be a " +
                                    Boolean.class.getCanonicalName() + " or " +
                                    String.class.getCanonicalName() +"type!",
                            ex
                    );
                }
            }).findFirst();

            return (T) commissionRuleOpt.orElseThrow(
                    () -> new ResolveDependencyStrategyException("Invoice hasn't contains commission rule!"));
        } catch (CommissionNotFoundException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Invalid arguments! Expected a list of invoices's services", ex);
        } catch (NullPointerException ex) {
            throw new ResolveDependencyStrategyException("Invalid arguments for resolution rule!", ex);
        }
    }

    private interface IClassCastStrategy {
        Boolean cast(Object object);
    }
}
