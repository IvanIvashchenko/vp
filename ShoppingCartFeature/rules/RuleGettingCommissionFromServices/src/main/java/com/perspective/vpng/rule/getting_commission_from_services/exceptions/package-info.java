/**
 * Contains exceptions for {@link com.perspective.vpng.rule.getting_commission_from_services.GettingCommissionFromServicesRule}
 */
package com.perspective.vpng.rule.getting_commission_from_services.exceptions;