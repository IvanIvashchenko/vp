package com.perspective.vpng.rule.building_list;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BuildingListRuleTest {

    private final IResolveDependencyStrategy strategy = new BuildingListRule();

    @Test
    public void should_BuildNonEmptyList() throws Exception {
        Object[] args = new Object[] {
                "val1",
                Arrays.asList(15, "val2", true),
                new Object[] {"val3", null, false, 5}
        };
        List list = strategy.resolve(args);
        assertFalse(list.isEmpty());
        for (Object arg : args) {
            if (arg instanceof List) {
                list.containsAll((List) arg);
            } else if (arg instanceof Objects[]) {
                list.containsAll(Arrays.asList((Object[]) arg));
            } else {
                list.contains(arg);
            }
        }
    }

    @Test
    public void should_BuildEmptyList() throws Exception {
        Object[] args = new Object[0];
        List list = strategy.resolve(args);
        assertTrue(list.isEmpty());
        list = strategy.resolve();
        assertTrue(list.isEmpty());
    }
}
