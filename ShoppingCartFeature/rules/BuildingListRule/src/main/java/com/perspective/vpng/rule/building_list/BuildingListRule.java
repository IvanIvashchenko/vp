package com.perspective.vpng.rule.building_list;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;

import java.util.*;

public class BuildingListRule implements IResolveDependencyStrategy {

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            List list = new ArrayList(args.length);
            for (Object arg : args) {
                if (arg instanceof Collection) {
                    list.addAll((Collection) arg);
                } else if (arg instanceof Object[]) {
                    list.addAll(Arrays.asList((Object[]) arg));
                } else {
                    list.add(arg);
                }
            }
            return (T) list;
        } catch (Exception ex) {
            throw new ResolveDependencyStrategyException(
                    String.format("list hasn't built: [%1$s]", ex.getMessage()),
                    ex
            );
        }
    }
}
