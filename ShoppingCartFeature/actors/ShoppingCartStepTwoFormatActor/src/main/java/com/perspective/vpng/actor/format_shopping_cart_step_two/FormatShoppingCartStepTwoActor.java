package com.perspective.vpng.actor.format_shopping_cart_step_two;


import com.perspective.vpng.actor.format_shopping_cart_step_two.exception.ShoppingCartStepTwoException;
import com.perspective.vpng.actor.format_shopping_cart_step_two.wrapper.ShoppingCartStepTwoMessage;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.time.LocalDateTime;
import java.util.*;

public class FormatShoppingCartStepTwoActor {
    private static IField formIdF;
    private static IField formF;
    private static IField blockF;
    private static IField checkedF;
    private static IField titleF;
    private static IField priceF;
    private static IField summaF;
    private static IField summaWithoutCommissionF;
    private static IField statusF;
    private static IField contentF;
    private static IField commissionF;
    private static IField invoiceIdF;
    private static IField itemsF;
    private static IField schemaF;
    private static IField bodyF;
    private static IField typeF;
    private static IField imageGroupF;
    private static IField userIdentifierF;
    private static IField identifyFieldNameF;

    private static ICachedCollection providerCollection;
    private static ICachedCollection formCollection;

    private static String template =
            "{" +
                "\"block\":\"modal\"," +
                "\"title\":\"Оплата платежей\"," +
                "\"submitButton\":" +
                    "{" +
                        "\"label\":\"Оплатить\"" +
                    "}" +
            "}";



    public FormatShoppingCartStepTwoActor(IObject params) throws ShoppingCartStepTwoException {
        try {
            IField providerCollectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerCollectionName");
            IField formCollectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "formCollectionName");
            formIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
            formF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "form");
            blockF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "block");
            checkedF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "checked");
            titleF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "title");
            priceF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "price");
            summaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "всего-к-оплате");
            summaWithoutCommissionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "сумма_без_комиссии");
            statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
            contentF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "content");
            commissionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "комиссия");
            invoiceIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "guid");
            itemsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "items");
            schemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schema");
            bodyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "body");
            typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            imageGroupF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "imageGroup");

            IField userIdentifierFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userIdentifier");
            userIdentifierF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), (String) userIdentifierFieldNameF.in(params));
            identifyFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userIdentifyField");

            formCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), formCollectionNameF.in(params), "formKey");
            providerCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), providerCollectionNameF.in(params), "name");
        } catch (Exception e) {
            throw new ShoppingCartStepTwoException("Failed to create FormatShoppingCartStepTwoActor", e);
        }
    }

    public void generateForm(final ShoppingCartStepTwoMessage message) throws ShoppingCartStepTwoException {
        try {
            List<IObject> invoices = message.getInvoices();

            String shoppingCartType = typeF.in(providerCollection.getItems(formIdF.in(invoices.get(0))).get(0));
            Map<String, IObject> payments = new HashMap<>();
            List<String> schemaItems = new ArrayList<>();

            for (IObject invoice : invoices) {
                IObject form = formCollection.getItems(formIdF.in(invoice)).get(0);
                IObject provider = providerCollection.getItems(formIdF.in(invoice)).get(0);
                IObject payment = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

                shoppingCartType = shoppingCartType.equals(typeF.in(provider)) ? shoppingCartType : "mixed";

                blockF.out(payment, "checkGroupListItem");
                checkedF.out(payment, true);
                titleF.out(payment, titleF.in(form));
                priceF.out(payment, summaF.in(invoice));
                statusF.out(payment, "Платёж создан " + LocalDateTime.now().format(IOC.resolve(Keys.getOrAdd("datetime_formatter"))));

                //Content generating
                contentF.out(payment, String.format(
                        "<p><strong>Приём платежа в пользу:</strong> %s</p><p><strong>Параметры платежа:</strong>&nbsp;%s: %s;</p><p>Сумма к зачислению: %s&nbsp;руб.; Комиссия: %s&nbsp;руб.</p>",
                        titleF.in(form),
                        identifyFieldNameF.in(provider),
                        userIdentifierF.in(invoice),
                        summaWithoutCommissionF.in(invoice),
                        commissionF.in(invoice)
                ));

                payments.put(invoiceIdF.in(invoice), payment);
                schemaItems.add(invoiceIdF.in(invoice));
            }

            payments.put("Сумма", IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{" +
                            "\"block\": \"static\"," +
                            "\"label\": \"Итого к оплате:\"," +
                            "\"labelWidth\": 3," +
                            "\"value\": 0," +
                            "\"rightMark\": \"<span class=glyphicon glyphicon-ruble aria-hidden=true></span>\"," +
                            "\"summarize\": [" +
                                "\"Платежи\"" +
                            "]" +
                    "}"
            ));

            IObject body = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            formF.out(body, payments);

            IObject schemaElement = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{" +
                        "\"name\": \"Платежи\"," +
                        "\"block\": \"checkGroupList\"" +
                    "}"
            );
            itemsF.out(schemaElement, schemaItems);
            schemaF.out(body, Arrays.asList(schemaElement, "Сумма"));

            IObject resultForm = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), template);
            imageGroupF.out(resultForm, IOC.resolve(Keys.getOrAdd("get_payment_method"), shoppingCartType));
            bodyF.out(resultForm, body);

            message.setResultForm(resultForm);
        } catch (Exception e) {
            throw new ShoppingCartStepTwoException("Failed to generate shopping cart step two form", e);
        }
    }
}
