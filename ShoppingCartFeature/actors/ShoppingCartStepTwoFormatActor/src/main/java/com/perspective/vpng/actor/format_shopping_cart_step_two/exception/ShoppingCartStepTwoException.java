package com.perspective.vpng.actor.format_shopping_cart_step_two.exception;

public class ShoppingCartStepTwoException extends Exception {
    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public ShoppingCartStepTwoException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause   specific cause
     */
    public ShoppingCartStepTwoException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public ShoppingCartStepTwoException(final Throwable cause) {
        super(cause);
    }
}
