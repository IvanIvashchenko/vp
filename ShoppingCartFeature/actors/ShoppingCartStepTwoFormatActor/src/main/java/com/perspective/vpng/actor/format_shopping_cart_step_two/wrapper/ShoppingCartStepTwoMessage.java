package com.perspective.vpng.actor.format_shopping_cart_step_two.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ShoppingCartStepTwoMessage {
    List<IObject> getInvoices() throws ReadValueException;
    void setResultForm(IObject form) throws ChangeValueException;
}
