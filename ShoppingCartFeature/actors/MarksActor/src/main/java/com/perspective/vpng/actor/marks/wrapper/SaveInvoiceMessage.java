package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface SaveInvoiceMessage {
    IObject getInvoice() throws ReadValueException;
    String getUserId() throws ReadValueException;
    String getMark() throws ReadValueException;
    List<String> getUserMarks() throws ReadValueException;
    void setUserMarks(List<String> marks) throws ChangeValueException;
}
