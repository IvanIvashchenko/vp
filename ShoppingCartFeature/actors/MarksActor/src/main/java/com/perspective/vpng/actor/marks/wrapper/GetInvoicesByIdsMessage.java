package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface GetInvoicesByIdsMessage {
    String getUserId() throws ReadValueException;
    List<String> getIds() throws ReadValueException;
    void setInvoices(List<IObject> invoices) throws ChangeValueException;
}
