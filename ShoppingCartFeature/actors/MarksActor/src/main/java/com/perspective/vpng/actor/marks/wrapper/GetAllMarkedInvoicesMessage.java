package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.nio.ReadOnlyBufferException;
import java.util.List;

public interface GetAllMarkedInvoicesMessage {
    String getUserId() throws ReadValueException;
    String getMark() throws ReadValueException;
    List<String> getInvoiceGuids() throws ReadValueException;
    void setInvoices(List<IObject> invoices) throws ChangeValueException;
}
