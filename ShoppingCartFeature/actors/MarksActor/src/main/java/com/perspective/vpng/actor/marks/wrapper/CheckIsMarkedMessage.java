package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CheckIsMarkedMessage {
    Boolean getIsMarked() throws ReadValueException;
    void setIsMarkedFlag(Boolean flag) throws ChangeValueException;
}
