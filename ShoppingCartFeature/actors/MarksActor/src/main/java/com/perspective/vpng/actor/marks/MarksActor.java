package com.perspective.vpng.actor.marks;

import com.perspective.vpng.actor.marks.exception.MarksActorException;
import com.perspective.vpng.actor.marks.wrapper.*;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.database.cached_collection.exception.UpsertCacheItemException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.*;

public class MarksActor {
    private ICachedCollection markCollection;
    private ICachedCollection providerCollection;
    private final IField userIdF;
    private final IField invoicesF;
    private final IField numberF;
    private final IField markF;

    private final IField amountF;
    private final IField dateF;
    private final IField descriptionF;
    private final IField enabledProviderF;
    private final IField groupNameF;
    private final IField humanDateF;
    private final IField logoF;
    private final IField operationF;
    private final IField paymentIdF;
    private final IField statusF;
    private final IField typeF;
    private final IField guidF;
    private final IField providerIdF;
    private final IField providerDescriptionF;
    private final IField sumF;
    private final IField tagF;

    public MarksActor(IObject params) {
        try {
            this.numberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "number");
            this.invoicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoices");
            this.userIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userId");
            this.markF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "mark");

            this.amountF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "amount");
            this.dateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "date");
            this.descriptionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "description");
            this.enabledProviderF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "enabledProvider");
            this.groupNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "groupName");
            this.humanDateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "humanDate");
            this.logoF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "logo");
            this.operationF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "operation");
            this.paymentIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentId");
            this.statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
            this.typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            this.guidF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "guid");
            this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
            this.providerDescriptionF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "полное-наименование");
            this.sumF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "сумма_без_комиссии");
            this.tagF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "метка");

            IField markCollectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "markCollectionName");
            this.markCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), markCollectionNameF.in(params), "userId");

            IField providerCollectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerCollectionName");
            this.providerCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), providerCollectionNameF.in(params), "name");
        } catch (InvalidArgumentException | ReadValueException e) {
            throw new RuntimeException("Failed to create MarksActor: error occurred reading config file.", e);
        } catch (ResolutionException e) {
            throw new RuntimeException("Failed to create MarksActor.", e);
        }
    }

    public void checkInvoiceIsMarked(CheckIsMarkedMessage message) throws Exception {
        if (message.getIsMarked() != null && message.getIsMarked()) {
            message.setIsMarkedFlag(true);
        } else {
            message.setIsMarkedFlag(false);
        }
    }

    public void getInvoices(GetMarkedInvoicesMessage message) throws MarksActorException {
        try {
            List<IObject> userObjects = markCollection.getItems(message.getUserId());
            if (userObjects.isEmpty()) {
                message.setInvoices(new LinkedList<>());
                return;
            }

            List<IObject> result = new ArrayList<>();
            for (IObject invoice : (List<IObject>) invoicesF.in(markCollection.getItems(message.getUserId()).get(0))) {
                result.add(formatInvoice(invoice));
            }
            message.setInvoices(result);
        } catch (Exception e) {
            throw new MarksActorException("Failed to get marked invoices", e);
        }
    }

    public void saveInvoiceToMarkedInvoices(SaveInvoiceMessage message) throws MarksActorException {
        try {
            String mark = message.getMark();
            List<String> userMarks = message.getUserMarks() != null ? message.getUserMarks() : new ArrayList<>();
            IObject invoice = message.getInvoice();
            List<IObject> userObjects = markCollection.getItems(message.getUserId());

            if (!userMarks.contains(mark)) {
                userMarks.add(mark);
            }
            message.setUserMarks(userMarks);

            IObject userObject;
            if (userObjects.isEmpty()) {
                userObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                userIdF.out(userObject, message.getUserId());
                invoicesF.out(userObject, Arrays.asList(invoice));
            } else {
                userObject = userObjects.get(0);
                List<IObject> invoices = invoicesF.in(userObject);
                invoices.add(message.getInvoice());
            }

            markCollection.upsert(userObject);
        } catch (UpsertCacheItemException e) {
            throw new MarksActorException("Failed to save invoice to marked invoices: can't upsert marked invoice collection", e);
        } catch (GetCacheItemException e) {
            throw new MarksActorException("Failed to save invoice to marked invoices: can't get item from marked invoice collection", e);
        } catch (Exception e) {
            throw new MarksActorException("Failed to save invoice to marked invoices", e);
        }
    }

    public void repeatInvoice(RepeatInvoiceMessage message) {
        try {
            String userId = message.getUserId();
            String invoiceNumber = message.getInvoiceNumber();
            List<IObject> userInvoices = invoicesF.in(markCollection.getItems(userId).get(0));

            for (IObject invoice : userInvoices) {
                if(numberF.in(invoice).toString().equals(invoiceNumber)) {
                    message.setInvoice(invoice);
                    return;
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to repeat marked invoice", e);
        }
    }

    public void getInvoicesByMark(GetAllMarkedInvoicesMessage message) {
        try {
            String mark = message.getMark();
            String userId = message.getUserId();
            List<String> guids = message.getInvoiceGuids();
            List<IObject> resultInvoices = new ArrayList<>();
            List<IObject> userInvoices = invoicesF.in(markCollection.getItems(userId).get(0));
            for (IObject invoice : userInvoices) {
                if (tagF.in(invoice).toString().equals(mark) && guids.contains(numberF.in(invoice))) {
                    resultInvoices.add(invoice);
                }
            }
            message.setInvoices(resultInvoices);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void getInvoicesByIds(GetInvoicesByIdsMessage message) {
        try {
            List<String> guids = message.getIds();
            List<IObject> result = new ArrayList<>();
            List<IObject> userInvoices = invoicesF.in(markCollection.getItems(message.getUserId()).get(0));
            for (String guid : guids) {
                for (IObject invoice : userInvoices) {
                    if (numberF.in(invoice).toString().equals(guid)) {
                        result.add(invoice);
                        break;
                    }
                }
            }
            message.setInvoices(result);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get marked invoices by ids", e);
        }
    }

    public void deleteInvoice(DeleteInvoiceMessage message) throws MarksActorException {
        try {
            String number = message.getInvoiceNumber();
            IObject userObject = markCollection.getItems(message.getUserId()).get(0);
            List<IObject> userInvoices = invoicesF.in(userObject);

            userInvoices.removeIf(invoice -> {
                try {
                    return numberF.in(invoice).toString().equals(number);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
            markCollection.upsert(userObject);
        } catch (Exception e) {
            throw new MarksActorException("Failed to delete marked invoice", e);
        }
    }

    public void deleteMark(DeleteMarkMessage message) throws MarksActorException {
        try {
            String mark = message.getMark();
            IObject userObject = markCollection.getItems(message.getUserId()).get(0);
            List<IObject> userInvoices = invoicesF.in(userObject);

            userInvoices.removeIf(invoice -> {
                try {
                    return tagF.in(invoice).equals(mark);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
            markCollection.upsert(userObject);
        } catch (Exception e) {
            throw new MarksActorException("Failed to delete mark", e);
        }
    }

    public void editMark(EditMarkMessage message) throws MarksActorException {
        try {
            String mark = message.getOldMark();
            String newMark = message.getNewMark();
            IObject userObject = markCollection.getItems(message.getUserId()).get(0);
            List<IObject> userInvoices = invoicesF.in(userObject);

            for (IObject invoice : userInvoices) {
                if (tagF.in(invoice).equals(mark)) {
                    tagF.out(invoice, newMark);
                }
            }
            markCollection.upsert(userObject);
        } catch (Exception e) {
            throw new MarksActorException("Failed to rename mark", e);
        }
    }

    private IObject formatInvoice(IObject invoice) {
        try {
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

            numberF.out(result, numberF.in(invoice));
            paymentIdF.out(result, guidF.in(invoice));
            IObject provider = providerCollection.getItems(providerIdF.in(invoice)).get(0);
            descriptionF.out(result, providerDescriptionF.in(provider));
            logoF.out(result, logoF.in(provider));
            amountF.out(result, sumF.in(invoice));
            markF.out(result, tagF.in(invoice));

            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
