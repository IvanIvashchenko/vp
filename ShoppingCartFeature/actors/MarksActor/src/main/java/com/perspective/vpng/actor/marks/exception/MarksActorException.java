package com.perspective.vpng.actor.marks.exception;

/**
 * Exception for error in {@link com.perspective.vpng.actor.marks.MarksActor} methods
 */
public class MarksActorException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public MarksActorException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public MarksActorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public MarksActorException(final Throwable cause) {
        super(cause);
    }
}