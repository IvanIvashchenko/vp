package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface EditMarkMessage {
    String getUserId() throws ReadValueException;
    String getOldMark() throws ReadValueException;
    String getNewMark() throws ReadValueException;
}
