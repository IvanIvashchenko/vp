package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface DeleteInvoiceMessage {
    String getInvoiceNumber() throws ReadValueException;
    String getUserId() throws ReadValueException;
}
