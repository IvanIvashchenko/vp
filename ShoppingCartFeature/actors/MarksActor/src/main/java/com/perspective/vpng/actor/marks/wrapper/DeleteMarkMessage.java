package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface DeleteMarkMessage {
    String getUserId() throws ReadValueException;
    String getMark() throws ReadValueException;
}
