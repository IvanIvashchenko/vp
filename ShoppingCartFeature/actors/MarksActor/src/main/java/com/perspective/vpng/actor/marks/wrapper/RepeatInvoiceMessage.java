package com.perspective.vpng.actor.marks.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface RepeatInvoiceMessage {
    String getUserId() throws ReadValueException;
    String getInvoiceNumber() throws ReadValueException;
    void setInvoice(IObject invoice) throws ChangeValueException;
}
