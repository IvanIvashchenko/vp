package com.perspective.vpng.actor.commission.exceptions;

import com.perspective.vpng.actor.commission.CommissionActor;

/**
 * Exception for constructors of {@link CommissionActor} actor.
 */
public class CommissionActorException extends Exception {

    public CommissionActorException(String message) {
        super(message);
    }

    public CommissionActorException(String message, Throwable cause) {
        super(message, cause);
    }
}
