package com.perspective.vpng.actor.commission.wrappers;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.math.BigDecimal;

/**
 * Wrapper for service-commission object.
 */
public interface ICommissionService {
    /**
     * Changes field of sum into service-commission object.
     *
     * @param sum - commission value for some invoice.
     *
     * @throws ChangeValueException when couldn't changed a <code>sum</code> field into service-commission object.
     */
    void setSum(BigDecimal sum) throws ChangeValueException;

    /**
     * Gets environment object for this wrapper.
     *
     * @return environment object for this wrapper.
     *
     * @throws ReadValueException when couldn't take wrapper's environment object.
     */
    IObject getEnvironment() throws ReadValueException;
}
