/**
 * Contains exceptions for {@link com.perspective.vpng.actor.commission.CommissionActor} actor.
 */
package com.perspective.vpng.actor.commission.exceptions;