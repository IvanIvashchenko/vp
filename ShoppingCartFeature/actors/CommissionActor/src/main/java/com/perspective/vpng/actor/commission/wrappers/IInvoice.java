package com.perspective.vpng.actor.commission.wrappers;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.math.BigDecimal;

/**
 * Wrapper for invoice object.
 */
public interface IInvoice {
    /**
     * Gets a sum of invoice.
     * After calculate commission for invoice sum change on sum with commission value.
     *
     * @return a sum of invoice.
     *
     * @throws ReadValueException when couldn't read <code>sum</code> field from invoice object.
     */
    BigDecimal getSum() throws ReadValueException;

    /**
     * Gets a service-commission for invoice.
     * NOTE: Invoice object contains a list of services and
     *                  service-commission selected from list of invoice's services by some rule.
     *
     * @return a service-commission for invoice.
     *
     * @throws ReadValueException when couldn't selected service_commission between another invoice's service.
     */
    ICommissionService getCommissionService() throws ReadValueException;

    /**
     * Changes <code>sumWithoutCommission</code> field valued into invoice's service.
     *
     * @param sumWithoutCommission - originally invoice sum with commission.
     *
     * @throws ChangeValueException when couldn't changed <code>SumWithoutCommission</code> into invoice object.
     */
    void setSumWithoutCommission(BigDecimal sumWithoutCommission) throws ChangeValueException;

    /**
     * Changes <code>commission</code> field valued into invoice's service.
     *
     * @param commission - invoice's commission value.
     *
     * @throws ChangeValueException when couldn't changed <code>commission</code> into invoice object.
     */
    void setCommission(BigDecimal commission) throws ChangeValueException;

    /**
     * Changes <code>sum</code> field valued into invoice's service.
     *
     * @param sum - sum of invoice.
     *
     * @throws ChangeValueException when couldn't changed <code>sum</code> into invoice object.
     */
    void setSum(BigDecimal sum) throws ChangeValueException;
}
