/**
 * Contains actor for calculation and additional commission into invoice.
 */
package com.perspective.vpng.actor.commission;