package com.perspective.vpng.actor.commission;

import com.perspective.vpng.actor.commission.exceptions.CalculationCommissionException;
import com.perspective.vpng.actor.commission.exceptions.CommissionActorException;
import com.perspective.vpng.actor.commission.wrappers.ICalculationCommissionMessage;
import com.perspective.vpng.actor.commission.wrappers.ICommissionActorParams;
import com.perspective.vpng.actor.commission.wrappers.ICommissionService;
import com.perspective.vpng.actor.commission.wrappers.IInvoice;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Actor for work with commissions.
 *
 * @see CommissionActor#addCommissionValueToInvoice(ICalculationCommissionMessage)
 * @see CommissionActor#calculateCommissionValue(IInvoice)
 */
public class CommissionActor {
    /**  */
    private final IKey commissionStrategyKey;

    /**
     * Constructor by commission actor params.
     *
     * @param params - must contains <code>commissionScale</code> field.
     */
    public CommissionActor(final ICommissionActorParams params) throws CommissionActorException {
        try {
            commissionStrategyKey = Keys.getOrAdd(params.getCommissionStrategy());
        } catch (Exception ex) {
            throw new CommissionActorException(ex.getMessage(), ex);
        }
    }

    /**
     * Calculates and adds commission for single invoice.
     * @see CommissionActor#calculateCommissionValue(IInvoice)
     *
     * @param message - must contains single invoice.
     *
     * @throws CalculationCommissionException when message hasn't contains a some invoice
     *                  or error handling given message.
     */
    public void addCommissionValueToInvoice(final ICalculationCommissionMessage message)
            throws CalculationCommissionException {
        try {
            IInvoice invoice = message.getInvoice();
            BigDecimal sum = Optional.ofNullable(invoice.getSum())
                    .orElseThrow(() -> new CalculationCommissionException("Invoice hasn't contains payment sum!"));
            invoice.setSumWithoutCommission(sum);

            BigDecimal commissionValue = calculateCommissionValue(invoice);

            invoice.setCommission(commissionValue);
            invoice.setSum(sum.add(commissionValue));
        } catch (ReadValueException ex) {
            throw new CalculationCommissionException("Invalid given message: can't read invoice!" + ex.getMessage(), ex);
        } catch (ChangeValueException ex) {
            throw new CalculationCommissionException("Can't write invoice:" + ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            throw new CalculationCommissionException("Given message hasn't contains invoice!", ex);
        }
    }

    /**
     * Calculates invoice commission value by commission rule.
     *                  Commission rule: fixed commission or percent.
     *                  If exists both commissions then both are taken into account.
     *
     * @param invoice - must contains: payment sum; optional: fixed or percent commission.
     *                "invoice" : {
     *                  "сумма" : "paymentSum"
     *                }
     *
     * @return the invoice commission.
     *
     * @throws CalculationCommissionException when error handling invoice.
     */
    protected BigDecimal calculateCommissionValue(final IInvoice invoice)
            throws CalculationCommissionException {
        try {
            ICommissionService commissionService = invoice.getCommissionService();
            BigDecimal commissionValue = IOC.resolve(
                    commissionStrategyKey,
                    commissionService.getEnvironment(),
                    invoice.getSum()
            );
            commissionService.setSum(commissionValue);

            return commissionValue;
        } catch (ReadValueException ex) {
            throw new CalculationCommissionException("Can't read commission service: " + ex.getMessage(), ex);
        } catch (ChangeValueException  ex) {
            throw new CalculationCommissionException("Couldn't changed commission service: " + ex.getMessage(), ex);
        } catch (ResolutionException ex) {
            throw new CalculationCommissionException(ex.getMessage(), ex);
        }
    }
}
