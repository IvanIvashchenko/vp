package com.perspective.vpng.actor.commission.wrappers;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for calculation commission message.
 */
public interface ICalculationCommissionMessage {
    /**
     * Gets a invoice that calculates a commission.
     *
     * @return a invoice that calculates a commission.
     *
     * @throws ReadValueException when couldn't read <code>invoice</code> from message.
     */
    IInvoice getInvoice() throws ReadValueException;
}
