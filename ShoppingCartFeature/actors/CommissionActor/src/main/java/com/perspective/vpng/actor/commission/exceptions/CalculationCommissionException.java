package com.perspective.vpng.actor.commission.exceptions;

/**
 * Exception for errors in calculation invoice's commission process.
 */
public class CalculationCommissionException extends Exception {
    public CalculationCommissionException(String message) {
        super(message);
    }

    public CalculationCommissionException(String message, Throwable cause) {
        super(message, cause);
    }
}
