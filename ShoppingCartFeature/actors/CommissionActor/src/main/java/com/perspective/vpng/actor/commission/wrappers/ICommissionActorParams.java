package com.perspective.vpng.actor.commission.wrappers;

import com.perspective.vpng.actor.commission.CommissionActor;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for params for {@link CommissionActor} actor.
 */
public interface ICommissionActorParams {
    /**
     * Gets a calculation commission rule key for resolution IOC.
     *
     * @return a key of calculation commission rule.
     *
     * @throws ReadValueException when couldn't read value of <code>commissionStrategy</code> field from message.
     */
    String getCommissionStrategy() throws ReadValueException;
}
