package com.perspective.vpng.actor.commission;

import com.perspective.vpng.actor.commission.exceptions.CalculationCommissionException;
import com.perspective.vpng.actor.commission.wrappers.ICalculationCommissionMessage;
import com.perspective.vpng.actor.commission.wrappers.ICommissionActorParams;
import com.perspective.vpng.actor.commission.wrappers.ICommissionService;
import com.perspective.vpng.actor.commission.wrappers.IInvoice;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ IOC.class, Keys.class })
@SuppressWarnings("unchecked")
public class CommissionActorTest {

    private static CommissionActor actor;
    private static ICalculationCommissionMessage message;
    private static IInvoice invoice;
    private static ICommissionService commissionService;
    private static IKey commissionStrategyKey;

    @BeforeClass
    public static void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        message = mock(ICalculationCommissionMessage.class);
        invoice = mock(IInvoice.class);
        commissionService = mock(ICommissionService.class);

        final String commissionStrategyName = "testStrategy";
        ICommissionActorParams params = mock(ICommissionActorParams.class);
        when(params.getCommissionStrategy()).thenReturn(commissionStrategyName);
        commissionStrategyKey = mock(IKey.class);
        when(Keys.getOrAdd(commissionStrategyName)).thenReturn(commissionStrategyKey);

        actor = new CommissionActor(params);
    }

    @Test
    public void ShouldCalculateAndAddCommissionValue_When_SumAndRuleAreGiven() throws Exception {
        ICalculationCommissionMessage message = mock(ICalculationCommissionMessage.class);
        IInvoice invoice = mock(IInvoice.class);
        ICommissionService commissionService = mock(ICommissionService.class);
        mockStatic(IOC.class);

        message = mock(ICalculationCommissionMessage.class);
        when(message.getInvoice()).thenReturn(invoice);
        BigDecimal invoiceSum = new BigDecimal(100.0);
        when(invoice.getSum()).thenReturn(invoiceSum);
        when(invoice.getCommissionService()).thenReturn(commissionService);
        when(commissionService.getEnvironment()).thenReturn(mock(IObject.class));
        BigDecimal commissionValue = BigDecimal.valueOf(5);
        when(IOC.resolve(eq(commissionStrategyKey), anyObject(), eq(invoiceSum)))
                .thenReturn(commissionValue);
        ArgumentCaptor<BigDecimal> argumentCaptor = ArgumentCaptor.forClass(BigDecimal.class);

        actor.addCommissionValueToInvoice(message);

        verify(message).getInvoice();
        verify(invoice, times(2)).getSum();
        verify(invoice).getCommissionService();
        verify(commissionService).setSum(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue(), commissionValue);
        verify(invoice).setSum(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue(), invoiceSum.add(commissionValue));
        verify(invoice).setSumWithoutCommission(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue(), invoiceSum);
        verify(invoice).setCommission(argumentCaptor.capture());
        assertEquals(argumentCaptor.getValue(), commissionValue);
    }

    @Test
    public void Should_ThrowException_WhitReason_GivenMessageHasNotContainsInvoice() throws Exception {
        try {
            when(message.getInvoice()).thenReturn(null);
            actor.addCommissionValueToInvoice(message);
        } catch (CalculationCommissionException ex) {
            assertEquals(ex.getMessage(), "Given message hasn't contains invoice!");
        }
    }

    @Test
    public void Should_ThrowException_WhitReason_InvoiceHasNotContainsPaymentSum() throws Exception {
        message = mock(ICalculationCommissionMessage.class);
        when(message.getInvoice()).thenReturn(invoice);
        when(invoice.getCommissionService()).thenReturn(commissionService);

        try {
            actor.addCommissionValueToInvoice(message);
        } catch (CalculationCommissionException ex) {
            assertEquals(ex.getMessage(), "Invoice hasn't contains payment sum!");
        }
    }

    @After
    public void resetMock() {
        reset(message);
    }
}
