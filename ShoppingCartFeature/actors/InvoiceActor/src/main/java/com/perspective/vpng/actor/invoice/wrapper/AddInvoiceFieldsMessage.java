package com.perspective.vpng.actor.invoice.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface AddInvoiceFieldsMessage {

    /**
     * @return list of field names which fields should be added to schema
     * @throws ReadValueException if any error is occurred
     */
    List<String> getFieldNames() throws ReadValueException;

    /**
     * @return list of invoices into inner format
     * @throws ReadValueException if any error is occurred
     */
    List<IObject> getInvoices() throws ReadValueException;

    /**
     * @return list of schemas
     * @throws ReadValueException if any error is occurred
     */
    List<List<IObject>> getSchemas() throws ReadValueException;

    /**
     * @param res the list of invoice's schemas with forms
     * @throws ChangeValueException sometimes
     */
    void setResult(List<IObject> res) throws ChangeValueException;
}
