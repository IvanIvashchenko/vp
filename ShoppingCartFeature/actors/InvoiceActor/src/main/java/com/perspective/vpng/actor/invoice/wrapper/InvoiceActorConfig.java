package com.perspective.vpng.actor.invoice.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface InvoiceActorConfig {

    String getProviderCollectionName() throws ReadValueException;
    String getProviderCacheKey() throws ReadValueException;
    String getFormCollectionName() throws ReadValueException;
    String getFormCacheKey() throws ReadValueException;

    String getFullProviderFieldName() throws ReadValueException;
    String getShortProviderFieldName() throws ReadValueException;
    String getUserIdentifierFieldName() throws ReadValueException;
    String getProviderIdentifyFieldName() throws ReadValueException;
    String getDateFieldName() throws ReadValueException;

    /**
     * @return initial sequence value for number of operation field
     * @throws ReadValueException
     */
    Integer getInitialSequenceValue() throws ReadValueException;
}
