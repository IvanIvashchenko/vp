package com.perspective.vpng.actor.invoice.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for message for {@link com.perspective.vpng.actor.invoice.InvoiceActor}
 */
public interface InvoiceActorMessage {
    /**
     *
     * @return
     * @throws ReadValueException
     */
    String getProviderId() throws ReadValueException;

    /**
     * getter for services
     * @throws ChangeValueException sometimes
     */
    List<IObject> getServices() throws ReadValueException;
    String getType() throws ReadValueException;
    Integer getOldNumber() throws ReadValueException;
    IObject getTargetObject() throws ReadValueException;

    void setProviderId(String key) throws ChangeValueException;
    void setFullProviderName(String fullProviderName) throws ChangeValueException;
    void setShortProviderName(String shortProviderName) throws ChangeValueException;
    void setUserIdentifier(String userIdentifier) throws ChangeValueException;
    void setUserIdentifyField(String userIdentifierFieldName) throws ChangeValueException;

    void setPaymentType(String paymentType) throws ChangeValueException;
    void setNumber(Integer number) throws ChangeValueException;
}
