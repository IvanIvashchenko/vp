package com.perspective.vpng.actor.invoice.exceptions;

/**
 * Exception for {@link com.perspective.vpng.actor.invoice.InvoiceActor}
 */
public class InvoiceActorException extends Exception {

    public InvoiceActorException(String message) {
        super(message);
    }

    public InvoiceActorException(String message, Throwable cause) {
        super(message, cause);
    }
}
