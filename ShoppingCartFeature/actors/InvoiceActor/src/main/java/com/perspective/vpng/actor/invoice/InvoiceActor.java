package com.perspective.vpng.actor.invoice;

import com.perspective.vpng.actor.invoice.exceptions.InvoiceActorException;
import com.perspective.vpng.actor.invoice.wrapper.AddInvoiceFieldsMessage;
import com.perspective.vpng.actor.invoice.wrapper.InvoiceActorConfig;
import com.perspective.vpng.actor.invoice.wrapper.InvoiceActorMessage;
import com.perspective.vpng.util.sequence_holder.SequenceHolder;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Actor for operations with invoice
 */
public class InvoiceActor {

    private static final String INVOICE_TYPE = "invoice";

    private final ICachedCollection providerCollection;
    private final ICachedCollection formCollection;
    private final DateTimeFormatter formatter;
    private final Integer initialSequenceValue;

    private final IField fullProviderNameF;
    private final IField shortProviderNameF;
    private final IField userIdentifierF;
    private final IField identifierFieldNameF;
    private final IField dateField;
    private final IField paymentTypeF;
    private final IField commissionF;
    private final IField guidF;
    private final IField nameF;
    private final IField valueF;
    private final IField toPayF;
    private final IField allToPayF;
    private final IField schemaF;
    private final IField formF;
    private final IField providerIdF;

    public InvoiceActor(final InvoiceActorConfig config) throws InvoiceActorException {
        try {
            this.providerCollection = IOC.resolve(
                Keys.getOrAdd(ICachedCollection.class.getCanonicalName()),
                config.getProviderCollectionName(),
                config.getProviderCacheKey()
            );
            this.formCollection = IOC.resolve(
                    Keys.getOrAdd(ICachedCollection.class.getCanonicalName()),
                    config.getFormCollectionName(),
                    config.getFormCacheKey()
            );
            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
            this.fullProviderNameF = IOC.resolve(fieldKey, config.getFullProviderFieldName());
            this.shortProviderNameF = IOC.resolve(fieldKey, config.getShortProviderFieldName());
            this.userIdentifierF = IOC.resolve(fieldKey, config.getUserIdentifierFieldName());
            this.identifierFieldNameF = IOC.resolve(nestedFieldKey, config.getProviderIdentifyFieldName());
            this.dateField = IOC.resolve(fieldKey, config.getDateFieldName());
            this.commissionF = IOC.resolve(fieldKey, "комиссия");
            this.paymentTypeF = IOC.resolve(fieldKey, "type");
            this.formatter = IOC.resolve(Keys.getOrAdd("datetime_formatter"));
            this.initialSequenceValue = config.getInitialSequenceValue();
            this.guidF = IOC.resolve(fieldKey, "guid");
            this.nameF = IOC.resolve(fieldKey, "name");
            this.valueF = IOC.resolve(fieldKey, "value");
            this.toPayF = IOC.resolve(fieldKey, "к-оплате");
            this.allToPayF = IOC.resolve(fieldKey, "всего-к-оплате");
            this.schemaF = IOC.resolve(fieldKey, "schema");
            this.formF = IOC.resolve(fieldKey, "form");
            this.providerIdF = IOC.resolve(nestedFieldKey, "поставщик/id");
        } catch (Exception e) {
            throw new InvoiceActorException("Failed to create InvoiceActor", e);
        }
    }

    /**
     * Adds some general data such as commission, provider name, date etc from provider and message to invoice
     * @param message with invoice
     * @throws InvoiceActorException
     */
    public void addDataFromProvider(final InvoiceActorMessage message) throws InvoiceActorException {

        try {
            List<IObject> providers = providerCollection.getItems(message.getProviderId());
            if (providers.size() != 1) {
                throw new InvoiceActorException(
                    "One provider should be found by guid: " + message.getProviderId() + ", but has been found " + providers.size()
                );
            }
            IObject provider =  providers.get(0);

            //copy some fields from provider to invoice/meters for later purposes
            message.setShortProviderName(shortProviderNameF.in(provider));
            message.setFullProviderName(fullProviderNameF.in(provider));
            IObject targetObject = message.getTargetObject();

            dateField.out(targetObject, LocalDateTime.now().format(formatter));
            String userIdentifierFieldName = userIdentifierF.in(provider);
            message.setUserIdentifier(identifierFieldNameF.in(targetObject));
            message.setUserIdentifyField(userIdentifierFieldName);
            message.setProviderId(message.getProviderId());
            if (message.getType().equals(INVOICE_TYPE)) {
                message.setPaymentType(paymentTypeF.in(provider));
                List<IObject> services = message.getServices();
                services.add(commissionF.in(provider));
                if (allToPayF.in(targetObject) == null) {
                    allToPayF.out(targetObject, IOC.resolve(Keys.getOrAdd("CalculateServicesSum"), services));
                }
            }
            if (message.getOldNumber() == null) {
                SequenceHolder sequenceHolder = IOC.resolve(Keys.getOrAdd("SequenceHolder"), message.getType(), this.initialSequenceValue);
                message.setNumber(sequenceHolder.getNewId());
            } else {
                message.setNumber(message.getOldNumber());
            }
        }  catch (ReadValueException | ChangeValueException | GetCacheItemException |
            InvalidArgumentException | ResolutionException ex) {
            throw new InvoiceActorException("Couldn't add data to invoice: " + ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            throw new InvoiceActorException("Illegal given message: list of services was null!", ex);
        }
    }

    /**
     * Adds fields from shopping cart invoices to shopping cart schemas
     * Passes by schemas and invoices, compares invoice guid and schema guid and adds needed fields,
     * which names are contained in message, add forms for smartactors.
     * @param message contains invoices and schemas
     * @throws InvoiceActorException if any error is occurred
     */
    public void addFieldsToSchema(final AddInvoiceFieldsMessage message) throws InvoiceActorException {

        try {
            List<IObject> resultInvoices = new ArrayList<>();
            List<String> fieldsToAdd = message.getFieldNames();
            List<IObject> invoices = message.getInvoices();
            List<List<IObject>> schemas = message.getSchemas();

            String name;
            boolean nextSchema = false;
            List<IObject> objectsToAdd;

            for (List<IObject> schema : schemas) {
                objectsToAdd = new ArrayList<>();
                for (IObject invoice : invoices) {
                    for (IObject objectFromSchema : schema) {
                        name = nameF.in(objectFromSchema);
                        if (name == null || !name.equals("guid")) {
                            continue;
                        }
                        if (guidF.in(invoice).equals(valueF.in(objectFromSchema))) {
                            IObject resultInvoice = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                            formF.out(resultInvoice, formCollection.getItems(providerIdF.in(invoice)).get(0));
                            schemaF.out(resultInvoice, schema);
                            resultInvoices.add(resultInvoice);

                            for (String fieldName : fieldsToAdd) {
                                IField invoiceField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), fieldName);
                                //TODO:: clarify, if we need visible field here
                                IObject additionalObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                                    "{\"name\": \"" + fieldName + "\", \"value\": \"" + invoiceField.in(invoice) + "\", \"visible\": false}"
                                );
                                objectsToAdd.add(additionalObject);
                            }
                            nextSchema = true;
                            break;
                        }
                    }
                    if (nextSchema) {
                        schema.addAll(objectsToAdd);
                        nextSchema = false;
                        break;
                    }
                }
            }
            message.setResult(resultInvoices);
        } catch (Exception e) {
            throw new InvoiceActorException("Can't add custom fields for shopping cart", e);
        }
    }
}
