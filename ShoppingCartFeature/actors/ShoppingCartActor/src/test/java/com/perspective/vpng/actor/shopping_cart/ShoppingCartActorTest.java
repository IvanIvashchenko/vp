package com.perspective.vpng.actor.shopping_cart;

import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import com.perspective.vpng.actor.shopping_cart.exception.ShoppingCartException;
import com.perspective.vpng.actor.shopping_cart.wrapper.ShoppingCartMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.delete.DeleteInvoiceByIdMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.delete.DeleteInvoicesByIdsMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.take.TakeInvoiceByIdMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.take.TakeInvoicesByIdsMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.take.TakeInvoicesPortionMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.upsert.UpsertInvoicesMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, LinkedHashMap.class, ShoppingCartInMemoryActor.class})
@SuppressWarnings("unchecked")
public class ShoppingCartActorTest {

    private ShoppingCartInMemoryActor actor;
    private IField GUID_FIELD;

    private Map<String, IObject> invoices;
    private IObject invoice1;
    private IObject invoice2;
    private IObject invoice3;

    @org.junit.Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(IOC.class);

        IKey key = mock(IKey.class);
        IKey iFieldKey = mock(IKey.class);
        when(IOC.getKeyForKeyStorage()).thenReturn(key);
        when(IOC.resolve(eq(key), eq(IField.class.getCanonicalName()))).thenReturn(iFieldKey);
        GUID_FIELD = mock(IField.class);
        when(IOC.resolve(eq(iFieldKey), eq("guid"))).thenReturn(GUID_FIELD);

        invoices = new LinkedHashMap<>();
        invoice1 = mock(IObject.class);
        invoice2 = mock(IObject.class);
        invoice3 = mock(IObject.class);
        when(GUID_FIELD.in(invoice1)).thenReturn("111");
        when(GUID_FIELD.in(invoice2)).thenReturn("222");
        when(GUID_FIELD.in(invoice3)).thenReturn("333");
        invoices.put("111", invoice1);
        invoices.put("222", invoice2);
        invoices.put("333", invoice3);
        PowerMockito.whenNew(LinkedHashMap.class).withNoArguments().thenReturn((LinkedHashMap) invoices);

        IObject config = mock(IObject.class);

        actor = new ShoppingCartInMemoryActor(config);
    }


    /////////////////////////////////create
    @Test
    public void Should_CreateNewReceiptList_And_AddInputReceipts_When_InputMessageIsCorrect() throws Exception {
        Map<String, IObject> receipts = new LinkedHashMap<>();
        PowerMockito.whenNew(LinkedHashMap.class).withNoArguments().thenReturn((LinkedHashMap) receipts);

        IObject receipt1 = mock(IObject.class);
        IObject receipt2 = mock(IObject.class);
        String guid1 = String.valueOf(UUID.randomUUID());
        String guid2 = String.valueOf(UUID.randomUUID());
        when(GUID_FIELD.in(receipt1)).thenReturn(guid1);
        when(GUID_FIELD.in(receipt2)).thenReturn(guid2);
        List<IObject> mockList = Arrays.asList(receipt1, receipt2);

        ShoppingCartMessage inputMessage = mock(ShoppingCartMessage.class);
        when(inputMessage.getInvoices()).thenReturn(mockList);

        Map<String, IObject> correctMap = new LinkedHashMap<>();
        correctMap.put(guid1, receipt1);
        correctMap.put(guid2, receipt2);

        actor.createShoppingCart(inputMessage);

        assertEquals(receipts, correctMap);
    }

    @Test
    public void Should_CreateNewReceiptList_And_AddInputReceiptsWithRandomGUID_When_InputMessageIsCorrect() throws Exception {
        Map<String, IObject> receipts = new LinkedHashMap<>();
        PowerMockito.whenNew(LinkedHashMap.class).withNoArguments().thenReturn((LinkedHashMap) receipts);

        IObject receipt1 = mock(IObject.class);
        IObject receipt2 = mock(IObject.class);
        UUID guid1 = UUID.randomUUID();
        UUID guid2 = UUID.randomUUID();
        PowerMockito.mockStatic(UUID.class);
        when(UUID.randomUUID()).thenReturn(guid1);
        when(UUID.randomUUID()).thenReturn(guid2);
        when(GUID_FIELD.in(receipt1)).thenReturn(null);
        when(GUID_FIELD.in(receipt2)).thenReturn(null);
        List<IObject> mockList = Arrays.asList(receipt1, receipt2);

        ShoppingCartMessage inputMessage = mock(ShoppingCartMessage.class);
        when(inputMessage.getInvoices()).thenReturn(mockList);

        actor.createShoppingCart(inputMessage);

        Map<String, IObject> correctMap = new LinkedHashMap<>();
        correctMap.put(String.valueOf(guid1), receipt1);
        correctMap.put(String.valueOf(guid2), receipt2);

        assertEquals(receipts, correctMap);
    }

    @Test(expected = ShoppingCartException.class)
    public void Should_ThrowException_When_InputReceiptListIsNull () throws ReadValueException, ShoppingCartException {
        ShoppingCartMessage inputMessage = mock(ShoppingCartMessage.class);
        when(inputMessage.getInvoices()).thenReturn(null);

        actor.createShoppingCart(inputMessage);
    }

    @Test(expected = ShoppingCartException.class)
    public void Should_ThrowException_When_InputReceiptListIsEmpty () throws ReadValueException, ShoppingCartException {
        ShoppingCartMessage inputMessage = mock(ShoppingCartMessage.class);
        when(inputMessage.getInvoices()).thenReturn(Collections.EMPTY_LIST);

        actor.createShoppingCart(inputMessage);
    }
    /////////////////////////////////

    @Test
    public void takeInvoiceByIdTest() throws Exception {
        TakeInvoiceByIdMessage message = mock(TakeInvoiceByIdMessage.class);
        when(message.getId()).thenReturn("111");

        ArgumentCaptor<IObject> selectedInvoices =
                ArgumentCaptor.forClass(IObject.class);

        actor.takeInvoiceById(message);

        verify(message).setInvoice(any());
        verify(message).setInvoice(selectedInvoices.capture());
        assertEquals(GUID_FIELD.in(selectedInvoices.getValue()), "111");
    }

    @Test
    public void should_ThrowException_WhenTakeInvoice_WithReason_InvoiceWithGivenIDNotExist() {
        List<String> ids = Arrays.asList("no exist!", null);
        TakeInvoiceByIdMessage message = null;
        try {
            message = mock(TakeInvoiceByIdMessage.class);
            when(message.getId()).thenReturn(ids.get(0)).thenReturn(ids.get(1));
        } catch (ReadValueException ex) {
            // Without processing.
        }

        for (int i = 0; i < 2; ++i) {
            try {
                actor.takeInvoiceById(message);
            } catch (ShoppingCartException ex) {
                assertEquals(ex.getMessage(), "Not found receipt with GUID:[" + ids.get(i) + "]");
            }
        }
    }


    @Test
    public void takeInvoicesPortionTest() throws Exception {
        addInvoicesInTestingActor();

        TakeInvoicesPortionMessage message = mock(TakeInvoicesPortionMessage.class);
        when(message.getPortionNumber()).thenReturn(1).thenReturn(2).thenReturn(3);
        when(message.getPortionSize()).thenReturn(2).thenReturn(2).thenReturn(2);

        ArgumentCaptor<List> selectedInvoices =
                ArgumentCaptor.forClass(List.class);

        for (int i = 0; i < 3; ++i) {
            actor.takeInvoicesPortion(message);
        }

        verify(message, times(3)).setInvoices(selectedInvoices.capture());
        List<String> ids = Arrays.asList("111", "222", "333", "444", "555");
        int indentIndex = 0;
        for (int i = 0; i < selectedInvoices.getAllValues().size(); ++i) {
            List<IObject> invoices = selectedInvoices.getAllValues().get(i);
            for (int j = invoices.size() - 1; j >= 0; --j) {
                assertEquals(GUID_FIELD.in(invoices.get(j)), ids.get(indentIndex + j));
            }
            indentIndex += invoices.size();
        }
    }

    @Test
    public void should_ThrowException_WhenTakeInvoicePortion_WithReason_GivenMessageWithInvalidPortionSize() {
        try {
            TakeInvoicesPortionMessage message = mock(TakeInvoicesPortionMessage.class);
            // Init portion number, but missing init portion size.
            when(message.getPortionNumber()).thenReturn(1);

            actor.takeInvoicesPortion(message);
        } catch (ReadValueException ex) {
            // Without processing.
        } catch (ShoppingCartException ex) {
            assertEquals(ex.getMessage(), "Invalid given message!");
        }
    }

    @Test
    public void should_ThrowException_WhenTakeInvoicePortion_WithReason_GivenMessageWithInvalidPortionNumber() {
        try {
            TakeInvoicesPortionMessage message = mock(TakeInvoicesPortionMessage.class);
            // Init portion size, but missing init portion number.
            when(message.getPortionSize()).thenReturn(1);

            actor.takeInvoicesPortion(message);
        } catch (ReadValueException ex) {
            // Without processing.
        } catch (ShoppingCartException ex) {
            assertEquals(ex.getMessage(), "Invalid given message!");
        }
    }

    /////////////////////////////////read
    @Test
    public void Should_readFromCartReceiptsByGUID() throws Exception {
        List<IObject> mockList = new ArrayList<>();
        PowerMockito.whenNew(ArrayList.class).withAnyArguments().thenReturn((ArrayList) mockList);

        TakeInvoicesByIdsMessage inputMessage = mock(TakeInvoicesByIdsMessage.class);
        when(inputMessage.getIds()).thenReturn(Arrays.asList("111", "222", "333"));

        actor.takeInvoicesByIds(inputMessage);

        List<IObject> correctList = Arrays.asList(invoice1, invoice2, invoice3);
        verify(inputMessage).setInvoices(any());
        assertEquals(correctList, mockList);
    }
    /////////////////////////////////


    /////////////////////////////////upsert
    @Test
    public void Should_AddInputReceipts() throws ReadValueException, InvalidArgumentException, ShoppingCartException {
        IObject receipt4 = mock(IObject.class);
        String guid4 = String.valueOf(UUID.randomUUID());
        when(GUID_FIELD.in(receipt4)).thenReturn(guid4);

        UpsertInvoicesMessage inputMessage = mock(UpsertInvoicesMessage.class);
        when(inputMessage.getInvoices()).thenReturn(Arrays.asList(receipt4));

        actor.upsertInvoices(inputMessage);

        assertEquals(receipt4, invoices.get(guid4));
    }
    @Test
    public void Should_UpdateReceiptList() throws ReadValueException, InvalidArgumentException, ShoppingCartException {
        IObject receipt4 = mock(IObject.class);
        when(GUID_FIELD.in(receipt4)).thenReturn("111");

        UpsertInvoicesMessage inputMessage = mock(UpsertInvoicesMessage.class);
        when(inputMessage.getInvoices()).thenReturn(Arrays.asList(receipt4));

        IObject after = invoices.get("111");

        actor.upsertInvoices(inputMessage);

        assertEquals(receipt4, invoices.get("111"));
        assertEquals(GUID_FIELD.<String>in(after), GUID_FIELD.<String>in(invoices.get("111")));
    }

    @Test
    public void MustCorrectUpdateOnvoice() throws ReadValueException, InvalidArgumentException, ShoppingCartException {
        String guid = "asd";
        IObject invoice = mock(IObject.class);
        when(GUID_FIELD.in(invoice)).thenReturn(guid);

        UpsertInvoicesMessage inputMessage = mock(UpsertInvoicesMessage.class);
        when(inputMessage.getInvoice()).thenReturn(invoice);
        when(inputMessage.getInvoiceId()).thenReturn(guid);

        actor.upsertInvoice(inputMessage);

        assertTrue(invoices.get(guid).equals(invoice));
    }
    /////////////////////////////////

    /////////////////////////////////delete

    @Test
    public void deleteInvoiceByIdFromShoppingCartTest() throws Exception {
        DeleteInvoiceByIdMessage inputMessage = mock(DeleteInvoiceByIdMessage.class);
        when(inputMessage.getId()).thenReturn("111").thenReturn("222").thenReturn("notExistsKey");

        actor.deleteInvoiceById(inputMessage);

        assertEquals(invoices.get("111"), null);
        assertEquals(invoices.get("222"), invoice2);
        assertEquals(invoices.get("333"), invoice3);

        actor.deleteInvoiceById(inputMessage);

        assertEquals(invoices.get("111"), null);
        assertEquals(invoices.get("222"), null);
        assertEquals(invoices.get("333"), invoice3);

        // Testing deletion by no exists key.
        actor.deleteInvoiceById(inputMessage);

        assertEquals(invoices.get("111"), null);
        assertEquals(invoices.get("222"), null);
        assertEquals(invoices.get("333"), invoice3);
    }

    @Test
    public void should_ThrowException_WhenDeleteInvoiceByID_WithReason_InvoiceIdIsNull() throws Exception {
        DeleteInvoiceByIdMessage inputMessage = mock(DeleteInvoiceByIdMessage.class);
        try {
            actor.deleteInvoiceById(inputMessage);
        } catch (ShoppingCartException ex) {
            assertEquals(ex.getMessage(), "Invoice's id is null!");
        }
    }

    @Test
    public void Should_DeleteReceiptsByGUID() throws ReadValueException, ShoppingCartException {
        DeleteInvoicesByIdsMessage inputMessage = mock(DeleteInvoicesByIdsMessage.class);
        when(inputMessage.getIds()).thenReturn(Arrays.asList("111", "222", "notExistsKey"));

        actor.deleteInvoicesByIds(inputMessage);

        assertEquals(invoices.get("111"), null);
        assertEquals(invoices.get("222"), null);
        assertEquals(invoices.get("333"), invoice3);
    }
    /////////////////////////////////

    private void addInvoicesInTestingActor() throws Exception {
        IObject invoice4 = mock(IObject.class);
        IObject invoice5 = mock(IObject.class);
        when(GUID_FIELD.in(invoice4)).thenReturn("444");
        when(GUID_FIELD.in(invoice5)).thenReturn("555");

        List<IObject> invoices = new ArrayList<>();
        invoices.add(invoice4);
        invoices.add(invoice5);

        UpsertInvoicesMessage upsertInvoicesMessage = mock(UpsertInvoicesMessage.class);
        when(upsertInvoicesMessage.getInvoices()).thenReturn(invoices);

        actor.upsertInvoices(upsertInvoicesMessage);
    }
}