package com.perspective.vpng.actor.shopping_cart.wrapper.delete;

import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface DeleteInvoiceByIdMessage extends ActorIdMessage {
    /**
     * Gets invoice GUID from message.
     *
     * @return strings-GUIDs.
     * @throws ReadValueException Throw when can't correct read value.
     */
    String getId() throws ReadValueException;
}
