package com.perspective.vpng.actor.shopping_cart.exception;

/**
 * Exception for ShoppingCartActor
 */
public class ShoppingCartException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public ShoppingCartException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause   specific cause
     */
    public ShoppingCartException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public ShoppingCartException(final Throwable cause) {
        super(cause);
    }
}
