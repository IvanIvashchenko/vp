package com.perspective.vpng.actor.shopping_cart.wrapper.delete;

import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Message for {@link ShoppingCartInMemoryActor#deleteInvoicesByIds(DeleteInvoicesByIdsMessage)}.
 */
public interface DeleteInvoicesByIdsMessage extends ActorIdMessage {
    /**
     * Gets invoices GUIDs from message.
     *
     * @return List of strings-GUIDs.
     * @throws ReadValueException Throw when can't correct read value.
     */
    List<String> getIds() throws ReadValueException;
}
