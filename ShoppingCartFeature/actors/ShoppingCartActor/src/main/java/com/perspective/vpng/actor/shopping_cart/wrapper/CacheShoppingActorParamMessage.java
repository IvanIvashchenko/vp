package com.perspective.vpng.actor.shopping_cart.wrapper;

import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CacheShoppingActorParamMessage extends ActorIdMessage {
    String getCollectionName() throws ReadValueException;
    IPool getConnectionPool() throws ReadValueException;
}
