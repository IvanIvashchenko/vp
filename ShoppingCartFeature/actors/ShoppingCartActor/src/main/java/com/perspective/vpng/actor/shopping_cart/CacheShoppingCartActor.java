package com.perspective.vpng.actor.shopping_cart;

import com.perspective.vpng.actor.shopping_cart.exception.ShoppingCartException;
import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.CacheShoppingActorParamMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.delete.DeleteInvoiceByIdMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.delete.DeleteInvoicesByIdsMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.take.*;
import com.perspective.vpng.actor.shopping_cart.wrapper.upsert.UpsertInvoicesMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.*;

public class
CacheShoppingCartActor {

    private IField guidField;
    private Map<String, IObject> invoices;

    private IPool connectionPool;
    private String collectionName;
    private IObject shoppingCart;

    private IField invoicesField;

    /**
     * Constructor with params
     * @param config starter config
     * @exception ShoppingCartException calls when throws any exception
     */
    public CacheShoppingCartActor(final CacheShoppingActorParamMessage config) throws ShoppingCartException {
        try {
            guidField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "guid");
            collectionName = config.getCollectionName();
            connectionPool = config.getConnectionPool();
            invoicesField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoices");
            shoppingCart = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

            String shoppingCartId = config.getShoppingCartId();
            guidField.out(shoppingCart, shoppingCartId);
            refreshInvoicesFromDB(shoppingCartId);
        } catch (ResolutionException | ReadValueException | ChangeValueException | InvalidArgumentException e) {
            throw new ShoppingCartException("Can't create ShoppingCartActor", e);
        }
    }

    /**
     * Create new ShopCart(HashMap) with invoices by {guid,receipt}
     * @param message input message which contains new invoices
     * @throws ShoppingCartException calls when throws any exception
     */
    public void createShoppingCart(final ActorIdMessage message) throws ShoppingCartException {
        try {
            refreshInvoicesFromDB(message.getShoppingCartId());
        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read shoppingCartId");
        }
    }

    /**
     * Takes an invoice by specific given id.
     *
     * @param message - incoming message with specific invoice id.
     * @see TakeInvoiceByIdMessage
     *
     * @throws ShoppingCartException when invoice with specific id is not exists
     *              or errors in handling given message.
     */
    public void takeInvoiceById(final TakeInvoiceByIdMessage message) throws ShoppingCartException {
        try {
            String id = message.getId();
            message.setInvoice(Optional.ofNullable(invoices.get(id))
                    .orElseThrow(() -> new ShoppingCartException("Not found invoice with GUID:[" + id + "]")));
        } catch (ReadValueException ex) {
            throw new ShoppingCartException("Can't read identifier from given message!", ex);
        } catch (ChangeValueException ex) {
            throw new ShoppingCartException("Can't write invoice in message!", ex);
        }
    }

    /**
     * Read invoices by input GUIDs
     * @param message contain identifiers of receipt that be reading
     * @exception ShoppingCartException calls when throws any exception
     * TODO:: check
     */
    public void takeInvoicesByIds(final TakeInvoicesByIdsMessage message) throws ShoppingCartException {
        try {
            List<String> guids = message.getIds();
            if (guids == null || guids.isEmpty()) {
                throw new ShoppingCartException("Read GUID list is empty or null.");
            }
            List<IObject> result = new ArrayList<>();
            for (String guid : guids) {
                if (invoices.containsKey(guid)) {
                    result.add(invoices.get(guid));
                } else {
                    //TODO:: Clarify, should we throw error even for one incorrect guid?
                    throw new ShoppingCartException("Not found receipt with GUID: " + guid);
                }
            }
            message.setInvoices(result);
        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read identifiers from input message.", e);
        } catch (ChangeValueException e) {
            throw new ShoppingCartException("Can't write result in message", e);
        }
    }

    /**
     * Takes an invoices by portions by portion size and number.
     *
     * @param message - incoming message with invoices portion size and number.
     * @see TakeInvoicesPortionMessage
     *
     * @throws ShoppingCartException when invalid given message or errors in handling given message.
     */
    public void takeInvoicesPortion(final TakeInvoicesPortionMessage message) throws ShoppingCartException {
        try {
            int limit = message.getPortionSize();
            int portionNumber = message.getPortionNumber() - 1;
            portionNumber = portionNumber < 0 ? 0 : portionNumber;
            int offset = portionNumber * limit;

            List<IObject> allInvoices = new ArrayList<>(invoices.values());
            List<IObject> portion = new ArrayList<>(limit);
            for (int i = offset; i < offset + limit && i < allInvoices.size(); ++i) {
                portion.add(allInvoices.get(i));
            }

            message.setInvoices(portion);
        } catch (NullPointerException ex) {
            throw new ShoppingCartException("Invalid given message!", ex);
        } catch (ReadValueException ex) {
            throw new ShoppingCartException("Can't read given message!", ex);
        } catch (ChangeValueException ex) {
            throw new ShoppingCartException("Can't write invoices in message!", ex);
        }
    }

    /**
     * Takes all invoices.
     *
     * @param message - incoming message with invoices portion size and number.
     * @see TakeAllInvoicesMessage
     *
     * @throws ShoppingCartException when invalid given message or errors in handling given message.
     */
    public void takeAllInvoices(final TakeAllInvoicesMessage message) throws ShoppingCartException {
        try {
            message.setInvoices(new ArrayList<>(invoices.values()));
        } catch (NullPointerException ex) {
            throw new ShoppingCartException("Invalid given message!", ex);
        } catch (ChangeValueException ex) {
            throw new ShoppingCartException("Can't write invoices in message!", ex);
        }
    }

    /**
     * Upsert ShoppingCart: extract receipt list from message and update or add receipt in ShopCart by GUID
     * @param message input message which contains invoices
     * @throws ShoppingCartException calls when throws any exception
     * TODO:: check
     */
    public void upsertInvoices(final UpsertInvoicesMessage message) throws ShoppingCartException {

        try {
            List<IObject> updateInvoices = message.getInvoices();
            if (updateInvoices != null && !updateInvoices.isEmpty()) {
                for (IObject invoice : updateInvoices) {
                    String guid = guidField.in(invoice);
                    invoices.put(guid, invoice);
                }
                invoicesField.out(shoppingCart, invoices.values());
                upsertCartInDB(shoppingCart);
            }
        } catch (ReadValueException | ChangeValueException e) {
            throw new ShoppingCartException("Can't read/write updating invoices from message.", e);
        } catch (InvalidArgumentException e) {
            throw new ShoppingCartException("Can't get global uuid of some receipt.", e);
        }
    }

    /**
     * Upsert ShoppingCart: extract receipt list from message and update or add receipt in ShopCart by GUID
     * @param message input message which contains invoices
     * @throws ShoppingCartException calls when throws any exception
     */
    public void upsertInvoice(final UpsertInvoicesMessage message) throws ShoppingCartException {

        try {
            IObject updateInvoice = message.getInvoice();
            if (updateInvoice == null) {
                throw new ShoppingCartException("Updating invoice is null.");
            }
            String guid = message.getInvoiceId();
            if (guid == null) {
                guid = IOC.resolve(Keys.getOrAdd("db.collection.nextid"));
            }
            guidField.out(updateInvoice, guid);
            invoices.put(guid, updateInvoice);

            Collection<IObject> invoicesList = invoices.values();
            invoicesField.out(shoppingCart, invoicesList);
            upsertCartInDB(shoppingCart);

        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read updating invoices from message.", e);
        } catch (InvalidArgumentException e) {
            throw new ShoppingCartException("Can't get global uid of some receipt.", e);
        } catch (ChangeValueException e) {
            throw new ShoppingCartException("Can't insert shopping cart id in object.", e);
        } catch (ResolutionException e) {
            throw new ShoppingCartException("Can't resolve unique identifier.", e);
        }
    }

    /**
     * Deletes invoice by identifier.
     *
     * @param message - contains id of invoice that be delete.
     * @throws ShoppingCartException when invoice id is null or error reading invoice id from given message.
     */
    public void deleteInvoiceById(final DeleteInvoiceByIdMessage message) throws ShoppingCartException {
        try {
            invoices.remove(message.getId());
            invoicesField.out(shoppingCart, invoices.values());
            upsertCartInDB(shoppingCart);
        } catch (NullPointerException ex) {
            throw new ShoppingCartException("Invoice's id is null!", ex);
        } catch (ReadValueException ex) {
            throw new ShoppingCartException("Can't read invoice's id from message!", ex);
        } catch (InvalidArgumentException | ChangeValueException ex) {
            throw new ShoppingCartException("Can't write invoices into shopping cart message!", ex);
        }
    }

    /**
     * Deletes invoices by identifiers
     * @param message contain identifiers of receipt that be delete
     * @exception ShoppingCartException calls when throws any exception
     */
    public void deleteInvoicesByIds(final DeleteInvoicesByIdsMessage message) throws ShoppingCartException {
        try {
            List<String> guids = message.getIds();
            for (String guid : guids) {
                invoices.remove(guid);
            }
            invoicesField.out(shoppingCart, invoices.values());
            upsertCartInDB(shoppingCart);
        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read deleting GUIDs from message.", e);
        } catch (InvalidArgumentException | ChangeValueException e) {
            throw new ShoppingCartException("Can't write invoices into shopping cart message!", e);
        }
    }

    private void upsertCartInDB(IObject shoppingCart) throws ShoppingCartException{
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            ITask upsertTask = IOC.resolve(
                Keys.getOrAdd("db.collection.upsert"),
                poolGuard.getObject(),
                collectionName,
                shoppingCart
            );
            upsertTask.execute();
        } catch (PoolGuardException | ResolutionException | TaskExecutionException e) {
            throw new ShoppingCartException("Can't execute database operation.", e);
        }
    }

    private void refreshInvoicesFromDB(String id) throws ShoppingCartException {

        invoices = new LinkedHashMap<>();
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                Keys.getOrAdd(IObject.class.getCanonicalName()),
                "{\"filter\": {\"guid\": {\"$eq\": \"" + id + "\"}}, \"collectionName\": \"" + collectionName + "\"}"
            );
            ITask searchTask = IOC.resolve(
                Keys.getOrAdd("db.collection.search"),
                poolGuard.getObject(),
                collectionName,
                query,
                (IAction<IObject[]>) foundDocs -> {
                    try {
                        searchResult.addAll(Arrays.asList(foundDocs));
                    } catch (Exception e) {
                        throw new ActionExecuteException(e);
                    }
                }
            );

            searchTask.execute();
            if (!searchResult.isEmpty()) {
                IObject shoppingCart = searchResult.get(0);
                this.shoppingCart = shoppingCart;
                List<IObject> cartInvoices = invoicesField.in(shoppingCart);

                if (cartInvoices == null) {
                    throw new ShoppingCartException("Shopping cart without invoices field has been found by id: " + id);
                }

                for (IObject currentInvoice : cartInvoices) {
                    String guid = guidField.in(currentInvoice);
                    if (guid == null) {
                        guid = String.valueOf(UUID.randomUUID());
                        guidField.out(currentInvoice, guid);
                    }

                    invoices.put(guid, currentInvoice);
                }
            }

        } catch (InvalidArgumentException e) {
            throw new ShoppingCartException("Can't read or write in some iobject", e);
        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read from some iobject", e);
        } catch (ChangeValueException e) {
            throw new ShoppingCartException("Can't write in some iobject", e);
        } catch (ResolutionException e) {
            throw new ShoppingCartException("Can't resolve", e);
        } catch (PoolGuardException | TaskExecutionException e) {
            throw new ShoppingCartException("Can't execute database operation", e);
        }
    }

    /**
     * Set count of invoices to message
     * @param message
     */
    public void getCount(GetInvoicesCountMessage message) throws ShoppingCartException {
        try {
            message.setCount(invoices.size());
        } catch (ChangeValueException e) {
            throw new ShoppingCartException("Failed to set shopping cart size", e);
        }
    }
}
