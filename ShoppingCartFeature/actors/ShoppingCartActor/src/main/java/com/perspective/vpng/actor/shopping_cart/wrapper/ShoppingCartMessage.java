package com.perspective.vpng.actor.shopping_cart.wrapper;

import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Message for {@link ShoppingCartInMemoryActor#createShoppingCart(ShoppingCartMessage)}.
 */
public interface ShoppingCartMessage extends ActorIdMessage  {

    /**
     * Gets invoices for creation shopping cart actor.
     *
     * @return List of invoices.
     * @throws ReadValueException Throw when can't correct read value.
     */
    List<IObject> getInvoices() throws ReadValueException;
}
