package com.perspective.vpng.actor.shopping_cart.wrapper.take;

import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Message for {@link ShoppingCartInMemoryActor#takeInvoicesByIds(TakeInvoicesByIdsMessage)}.
 */
public interface TakeInvoicesByIdsMessage extends ActorIdMessage {
    /**
     * Gets invoices GUIDs from message.
     *
     * @return List of strings-GUIDs.
     * @throws ReadValueException Throw when can't correct read value.
     */
    List<String> getIds() throws ReadValueException;

    /**
     * Adds invoices in message
     *
     * @param invoices List of object with data for payment.
     * @throws ChangeValueException Throw when can't correct change value
     */
    void setInvoices(List<IObject> invoices) throws ChangeValueException;
}
