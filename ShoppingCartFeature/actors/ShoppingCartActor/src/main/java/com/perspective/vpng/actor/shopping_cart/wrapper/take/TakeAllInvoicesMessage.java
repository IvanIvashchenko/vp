package com.perspective.vpng.actor.shopping_cart.wrapper.take;

import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;

import java.util.List;

/**
 * Message for {@link com.perspective.vpng.actor.shopping_cart.CacheShoppingCartActor#takeAllInvoices(TakeAllInvoicesMessage)}.
 */
public interface TakeAllInvoicesMessage extends ActorIdMessage {
    /**
     * Adds into the message an invoices portion.
     *
     * @param invoices - added invoices portion.
     * @throws ChangeValueException when errors in addition invoices into message.
     */
    void setInvoices(List<IObject> invoices) throws ChangeValueException;
}
