package com.perspective.vpng.actor.shopping_cart.wrapper.take;

import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Message for {@link ShoppingCartInMemoryActor#takeInvoicesPortion(TakeInvoicesPortionMessage)}.
 */
public interface TakeInvoicesPortionMessage extends ActorIdMessage {
    /**
     * Gets a invoices portion number.
     *
     * @return a number of invoices portion.
     * @throws ReadValueException when errors in reading portion number from message.
     */
    Integer getPortionNumber() throws ReadValueException;

    /**
     * Gets a size of invoices portion.
     *
     * @return a size of invoice portion.
     * @throws ReadValueException when errors in reading portion size from message.
     */
    Integer getPortionSize() throws ReadValueException;

    /**
     * Adds into the message an invoices portion.
     *
     * @param invoices - added invoices portion.
     * @throws ChangeValueException when errors in addition invoices into message.
     */
    void setInvoices(List<IObject> invoices) throws ChangeValueException;
}
