package com.perspective.vpng.actor.shopping_cart.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ActorIdMessage {

    /**
     * @return userId or sessionId
     * @throws ReadValueException
     */
    String getShoppingCartId() throws ReadValueException;
}
