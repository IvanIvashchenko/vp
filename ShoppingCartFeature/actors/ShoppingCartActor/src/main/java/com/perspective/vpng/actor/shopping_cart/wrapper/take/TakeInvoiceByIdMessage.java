package com.perspective.vpng.actor.shopping_cart.wrapper.take;

import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Message for {@link ShoppingCartInMemoryActor#takeInvoiceById(TakeInvoiceByIdMessage)}.
 */
public interface TakeInvoiceByIdMessage extends ActorIdMessage {
    /**
     * Gets invoice GUID from message.
     *
     * @return strings-GUIDs.
     * @throws ReadValueException Throw when can't correct read value.
     */
    String getId() throws ReadValueException;

    /**
     * Adds invoices in message.
     *
     * @param invoice object with data for payment.
     * @throws ChangeValueException Throw when can't correct change value.
     */
    void setInvoice(IObject invoice) throws ChangeValueException;
}
