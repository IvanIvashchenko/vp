package com.perspective.vpng.actor.shopping_cart.wrapper.upsert;

import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Message for {@link ShoppingCartInMemoryActor#upsertInvoices(UpsertInvoicesMessage)}.
 */
public interface UpsertInvoicesMessage extends ActorIdMessage {
    /**
     * Get invoices from message.
     *
     * @return List of invoices.
     * @throws ReadValueException Throw when can't correct read value.
     */
    List<IObject> getInvoices() throws ReadValueException;
    /**
     * Get invoice from message.
     *
     * @return IObject - invoices.
     * @throws ReadValueException Throw when can't correct read value.
     */
    IObject getInvoice() throws ReadValueException;

    /**
     * Getter for invoiceId
     * @return invoiceId
     * @throws ReadValueException
     */
    String getInvoiceId() throws ReadValueException;
}
