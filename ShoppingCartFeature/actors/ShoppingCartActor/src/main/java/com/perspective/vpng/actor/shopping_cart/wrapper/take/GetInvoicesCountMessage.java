package com.perspective.vpng.actor.shopping_cart.wrapper.take;

import com.perspective.vpng.actor.shopping_cart.wrapper.ActorIdMessage;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;

public interface GetInvoicesCountMessage extends ActorIdMessage {
    /**
     * Set count of invoices in current cart to message
     * @param count count of invoices in shopping cart
     * @throws ChangeValueException
     */
    void setCount(Integer count) throws ChangeValueException;
}
