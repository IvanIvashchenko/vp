package com.perspective.vpng.actor.shopping_cart;

import com.perspective.vpng.actor.shopping_cart.exception.ShoppingCartException;
import com.perspective.vpng.actor.shopping_cart.wrapper.ShoppingCartMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.delete.DeleteInvoiceByIdMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.delete.DeleteInvoicesByIdsMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.take.TakeInvoiceByIdMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.take.TakeInvoicesByIdsMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.take.TakeInvoicesPortionMessage;
import com.perspective.vpng.actor.shopping_cart.wrapper.upsert.UpsertInvoicesMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.*;

/**
 * Actor for storage of invoices and which is carrying out CRUD
 */
public class ShoppingCartInMemoryActor {

    private static IField GUID_FIELD;
    private Map<String, IObject> invoices;

    /**
     * Constructor with params
     * @param config starter config
     * @exception ShoppingCartException calls when throws any exception
     */
    public ShoppingCartInMemoryActor(final IObject config) throws ShoppingCartException {
        invoices = new LinkedHashMap<>();
        try {
            GUID_FIELD = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "guid");
        } catch (ResolutionException e) {
            throw new ShoppingCartException("Can't create ShoppingCartActor", e);
        }
    }

    /**
     * Create new ShopCart(HashMap) with invoices by {guid,receipt}
     * @param message input message which contains new invoices
     * @throws ShoppingCartException calls when throws any exception
     */
    public void createShoppingCart(final ShoppingCartMessage message) throws ShoppingCartException {
        try {
            List<IObject> newReceipts = message.getInvoices();
            if (newReceipts == null || newReceipts.isEmpty()) {
                throw new ShoppingCartException("Added receipt list is empty or null.");
            }
            invoices = new LinkedHashMap<>();
            for (IObject iObject : newReceipts) {
                String guid = GUID_FIELD.in(iObject);
                if (guid == null || guid.equals("")) {
                    guid = String.valueOf(UUID.randomUUID());
                }
                invoices.put(guid, iObject);
            }
        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't get receipt list from message.", e);
        } catch (InvalidArgumentException e) {
            throw new ShoppingCartException("Can't get global uid of some receipt.", e);
        }
    }

    /**
     * Takes an invoice by specific given id.
     *
     * @param message - incoming message with specific invoice id.
     * @see TakeInvoiceByIdMessage
     *
     * @throws ShoppingCartException when invoice with specific id is not exists
     *              or errors in handling given message.
     */
    public void takeInvoiceById(final TakeInvoiceByIdMessage message) throws ShoppingCartException {
        try {
            String id = message.getId();
            message.setInvoice(Optional.ofNullable(invoices.get(id))
                    .orElseThrow(() -> new ShoppingCartException("Not found receipt with GUID:[" + id + "]")));
        } catch (ReadValueException ex) {
            throw new ShoppingCartException("Can't read identifier from given message!", ex);
        } catch (ChangeValueException ex) {
            throw new ShoppingCartException("Can't write invoice in message!", ex);
        }
    }

    /**
     * Read invoices by input GUIDs
     * @param message contain identifiers of receipt that be reading
     * @exception ShoppingCartException calls when throws any exception
     */
    public void takeInvoicesByIds(final TakeInvoicesByIdsMessage message) throws ShoppingCartException {
        try {
            List<String> guids = message.getIds();
            if (guids == null || guids.isEmpty()) {
                throw new ShoppingCartException("Read GUID list is empty or null.");
            }
            List<IObject> result = new ArrayList<>();
            for (String guid : guids) {
                if (invoices.containsKey(guid)) {
                    result.add(invoices.get(guid));
                } else {
                    throw new ShoppingCartException("Not found receipt with GUID: " + guid);
                }
            }
            message.setInvoices(result);
        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read identifiers from input message.", e);
        } catch (ChangeValueException e) {
            throw new ShoppingCartException("Can't write result in message", e);
        }
    }

    /**
     * Takes an invoices by portions by portion size and number.
     *
     * @param message - incoming message with invoices portion size and number.
     * @see TakeInvoicesPortionMessage
     *
     * @throws ShoppingCartException when invalid given message or errors in handling given message.
     */
    public void takeInvoicesPortion(final TakeInvoicesPortionMessage message) throws ShoppingCartException {
        try {
            int limit = message.getPortionSize();
            int portionNumber = message.getPortionNumber() - 1;
            portionNumber = portionNumber < 0 ? 0 : portionNumber;
            int offset = portionNumber * limit;

            List<IObject> allInvoices = new ArrayList<>(invoices.values());
            List<IObject> portion = new ArrayList<>(limit);
            for (int i = offset; i < offset + limit && i < allInvoices.size(); ++i) {
                portion.add(allInvoices.get(i));
            }

            message.setInvoices(portion);
        } catch (NullPointerException ex) {
            throw new ShoppingCartException("Invalid given message!", ex);
        } catch (ReadValueException ex) {
            throw new ShoppingCartException("Can't read given message!", ex);
        } catch (ChangeValueException ex) {
            throw new ShoppingCartException("Can't write invoices in message!", ex);
        }
    }

    /**
     * Upsert ShoppingCart: extract receipt list from message and update or add receipt in ShopCart by GUID
     * @param message input message which contains invoices
     * @throws ShoppingCartException calls when throws any exception
     */
    public void upsertInvoices(final UpsertInvoicesMessage message) throws ShoppingCartException {
        try {
            List<IObject> updateInvoices = message.getInvoices();
            if (updateInvoices == null || updateInvoices.isEmpty()) {
                throw new ShoppingCartException("Updating receipt list is empty or null.");
            }
            for (IObject invoice : updateInvoices) {
                String guid = GUID_FIELD.in(invoice);
                invoices.put(guid, invoice);
            }

        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read updating invoices from message.", e);
        } catch (InvalidArgumentException e) {
            throw new ShoppingCartException("Can't get global uid of some receipt.", e);
        }
    }

    /**
     * Upsert ShoppingCart: extract receipt list from message and update or add receipt in ShopCart by GUID
     * @param message input message which contains invoices
     * @throws ShoppingCartException calls when throws any exception
     */
    public void upsertInvoice(final UpsertInvoicesMessage message) throws ShoppingCartException {
        try {
            IObject updateInvoice = message.getInvoice();
            if (updateInvoice == null) {
                throw new ShoppingCartException("Updating invoice is null.");
            }
            String guid = message.getInvoiceId();
            if (guid == null) {
                guid = String.valueOf(UUID.randomUUID());
            }
            GUID_FIELD.out(updateInvoice, guid);
            invoices.put(guid, updateInvoice);

        } catch (ReadValueException | ChangeValueException e) {
            throw new ShoppingCartException("Can't read updating invoices from message.", e);
        } catch (InvalidArgumentException e) {
            throw new ShoppingCartException("Can't get global uid of some receipt.", e);
        }
    }

    /**
     * Deletes invoice by identifier.
     *
     * @param message - contains id of invoice that be delete.
     * @throws ShoppingCartException when invoice id is null or error reading invoice id from given message.
     */
    public void deleteInvoiceById(final DeleteInvoiceByIdMessage message) throws ShoppingCartException {
        try {
            invoices.remove(message.getId());
        } catch (NullPointerException ex) {
            throw new ShoppingCartException("Invoice's id is null!", ex);
        } catch (ReadValueException ex) {
            throw new ShoppingCartException("Can't read invoice's id from message!", ex);
        }
    }

    /**
     * Deletes invoices by identifiers
     * @param message contain identifiers of receipt that be delete
     * @exception ShoppingCartException calls when throws any exception
     */
    public void deleteInvoicesByIds(final DeleteInvoicesByIdsMessage message) throws ShoppingCartException {
        try {
            List<String> guids = message.getIds();
            for (String guid : guids) {
                invoices.remove(guid);
            }
        } catch (ReadValueException e) {
            throw new ShoppingCartException("Can't read deleting GUIDs from message.", e);
        }
    }

}