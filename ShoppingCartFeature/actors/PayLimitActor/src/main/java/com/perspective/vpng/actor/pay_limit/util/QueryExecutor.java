package com.perspective.vpng.actor.pay_limit.util;

import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.database.database_storage.exceptions.QueryExecutionException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueryExecutor implements IQueryExecutor {

    private final IPool connectionPool;

    public QueryExecutor(IPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public List<IObject> executeSearch(String collectionName, IObject params)
            throws PoolGuardException, QueryExecutionException {

        try(IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> result = new ArrayList<>();
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    collectionName,
                    params,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            result.addAll(Arrays.asList(foundDocs));
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    }
            );
            searchTask.execute();
            return result;
        } catch (ResolutionException | TaskExecutionException ex) {
            throw new QueryExecutionException(ex.getMessage(), ex);
        }
    }

    @Override
    public void executeUpsert(String collectionName, IObject value)
            throws PoolGuardException, QueryExecutionException {

        try (IPoolGuard poolGuard = new PoolGuard(this.connectionPool)) {
            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.upsert"),
                    poolGuard.getObject(),
                    collectionName,
                    value
            );
            task.execute();
        } catch (ResolutionException | TaskExecutionException e) {
            throw new QueryExecutionException("Can't upsert value in database", e);
        }
    }

}
