package com.perspective.vpng.actor.pay_limit.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ICheckUserPayMessage {

    List<IObject> getInvoices() throws ReadValueException;

    String getUserId() throws ReadValueException;

}
