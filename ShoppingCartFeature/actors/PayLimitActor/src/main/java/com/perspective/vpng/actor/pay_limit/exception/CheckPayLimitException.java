package com.perspective.vpng.actor.pay_limit.exception;

import com.perspective.vpng.actor.pay_limit.ProviderPayLimitActor;

/**
 * Exception for {@link ProviderPayLimitActor}
 */
public class CheckPayLimitException extends Exception {

    public CheckPayLimitException(final String message) {
        super(message);
    }

    public CheckPayLimitException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
