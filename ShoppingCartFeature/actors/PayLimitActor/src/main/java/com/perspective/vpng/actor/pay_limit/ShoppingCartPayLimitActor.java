package com.perspective.vpng.actor.pay_limit;

import com.perspective.vpng.actor.pay_limit.exception.CheckPayLimitException;
import com.perspective.vpng.actor.pay_limit.exception.ExceedingPayLimitException;
import com.perspective.vpng.actor.pay_limit.wrapper.ICheckShoppingCartPayLimitMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.List;

/**
 * Actor for check limits for one shopping cart
 */
public class ShoppingCartPayLimitActor {

    private final BigDecimal shoppingCartPayLimit;

    private final IField invoiceSumF;

    public ShoppingCartPayLimitActor(final BigDecimal shoppingCartPayLimitParam) throws CheckPayLimitException {

        try {
            shoppingCartPayLimit = shoppingCartPayLimitParam;
            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            invoiceSumF = IOC.resolve(fieldKey, "сумма_без_комиссии");
        } catch (ResolutionException e) {
            throw new CheckPayLimitException("Can't create CheckShoppingCartLimitActor");
        }
    }

    public void checkPayLimit(final ICheckShoppingCartPayLimitMessage message)
            throws CheckPayLimitException, ExceedingPayLimitException {

        try {
            List<IObject> invoices = message.getInvoices();
            BigDecimal shoppingCartSum = BigDecimal.ZERO;
            for (IObject invoice : invoices) {
                BigDecimal invoiceSum = invoiceSumF.in(invoice, BigDecimal.class);
                shoppingCartSum = shoppingCartSum.add(invoiceSum);
            }
            if (shoppingCartSum.compareTo(shoppingCartPayLimit) == 1) {
                throw new ExceedingPayLimitException("Pay limit for shopping cart has been exceeded");
            }
        } catch (ReadValueException | InvalidArgumentException e) {
            throw new CheckPayLimitException("Error during check shopping cart pay limits", e);
        }
    }

}
