package com.perspective.vpng.actor.pay_limit;

import com.perspective.vpng.actor.pay_limit.exception.CheckPayLimitException;
import com.perspective.vpng.actor.pay_limit.exception.ExceedingPayLimitException;
import com.perspective.vpng.actor.pay_limit.exception.UpdatingPaidAmountException;
import com.perspective.vpng.actor.pay_limit.util.IQueryExecutor;
import com.perspective.vpng.actor.pay_limit.wrapper.ICheckProviderPayLimitMessage;
import com.perspective.vpng.actor.pay_limit.wrapper.IUpdatePaidAmountMessage;
import com.perspective.vpng.database.field.IConditionField;
import com.perspective.vpng.database.field.IDBFieldsHolder;
import com.perspective.vpng.database.field.ISearchQueryField;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.database.database_storage.exceptions.QueryExecutionException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Actor for check payment limits
 */
public class ProviderPayLimitActor {

    private final BigDecimal providerDefaultDailyLimit;
    private final BigDecimal providerDefaultMonthlyLimit;

    private final ICachedCollection cachedCollection;
    private final String paidAmountCollection;

    private final IQueryExecutor queryExecutor;
    private final DateTimeFormatter standardFormatter;

    private final ISearchQueryField searchQueryField;
    private final IConditionField conditionField;

    private final IField providerIdF;
    private final IField accountF;
    private final IField providerDailyLimitF;
    private final IField providerMonthlyLimitF;
    private final IField invoiceSumF;
    private final IField currentDailyAmountF;
    private final IField currentMonthlyAmountF;
    private final IField lastPaymentDateF;
    private final IField providerIdDbF;
    private final IField accountDbF;

    private class Key {

        final String PROVIDER_ID;
        final String ACCOUNT;

        public Key(String providerId, String account) {
            PROVIDER_ID = providerId;
            ACCOUNT = account;
        }

        @Override
        public int hashCode() {
            return PROVIDER_ID.hashCode() ^ ACCOUNT.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Key)) {
                return false;
            }
            Key key = (Key) obj;
            return key.PROVIDER_ID.equals(this.PROVIDER_ID) &&
                    key.ACCOUNT.equals(this.ACCOUNT);
        }

    }

    public ProviderPayLimitActor(
            BigDecimal providerDefaultDailyLimit,
            BigDecimal providerDefaultMonthlyLimit,
            String paidAmountCollection,
            String providerCollection,
            String cacheCollectionKey,
            IQueryExecutor queryExecutor
    ) throws CheckPayLimitException {
        try {
            this.providerDefaultDailyLimit = providerDefaultDailyLimit;
            this.providerDefaultMonthlyLimit = providerDefaultMonthlyLimit;
            this.paidAmountCollection = paidAmountCollection;
            this.queryExecutor = queryExecutor;
            cachedCollection = IOC.resolve(
                    Keys.getOrAdd(ICachedCollection.class.getCanonicalName()),
                    providerCollection,
                    cacheCollectionKey
            );
            IDBFieldsHolder dbFieldsHolder = IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()));
            searchQueryField = dbFieldsHolder.searchQuery();
            conditionField = dbFieldsHolder.conditions();

            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
            providerIdF = IOC.resolve(nestedFieldKey, "поставщик/id");
            accountF = IOC.resolve(nestedFieldKey, "клиент/лицевой-счет/одной-строкой");
            providerDailyLimitF = IOC.resolve(fieldKey, "дневной-лимит");
            providerMonthlyLimitF = IOC.resolve(fieldKey, "месячный-лимит");
            invoiceSumF = IOC.resolve(fieldKey, "сумма_без_комиссии");
            standardFormatter = IOC.resolve(Keys.getOrAdd("datetime_formatter"));
            currentDailyAmountF = IOC.resolve(fieldKey, "currentDailyAmount");
            currentMonthlyAmountF = IOC.resolve(fieldKey, "currentMonthlyAmount");
            lastPaymentDateF = IOC.resolve(fieldKey, "date");
            providerIdDbF = IOC.resolve(fieldKey, "providerId");
            accountDbF = IOC.resolve(fieldKey, "account");
        } catch (ResolutionException ex) {
            throw new CheckPayLimitException("Can't create ProviderLimitActor", ex);
        }
    }

    public void checkPayLimit(final ICheckProviderPayLimitMessage message)
            throws CheckPayLimitException, PoolGuardException, ExceedingPayLimitException {

        try {
            List<IObject> invoices = message.getInvoices();
            Map<Key, List<IObject>> invoicesGroups = groupInvoicesByProviderIdAndAccount(invoices);
            String providerId, account;
            for (Map.Entry<Key, List<IObject>> group : invoicesGroups.entrySet()) {
                providerId = group.getKey().PROVIDER_ID;
                account = group.getKey().ACCOUNT;

                IObject provider = IOC.resolve(
                        Keys.getOrAdd("SelectionProviderStrategy"),
                        new Object[]{cachedCollection.getItems(providerId)}
                );

                IObject paidAmount;
                try {
                    paidAmount = getPaidAmount(providerId, account);
                } catch (IndexOutOfBoundsException ex) {
                    paidAmount = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                    initPaidAmount(paidAmount, providerId, account);
                }

                LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
                LocalDateTime firstDayOfMonth = LocalDateTime.of(
                        LocalDate.now().withDayOfMonth(1), LocalTime.MIDNIGHT);
                LocalDateTime lastPaymentDate = LocalDateTime.from(
                        standardFormatter.parse(lastPaymentDateF.in(paidAmount)));

                BigDecimal providerDailyLimit = providerDailyLimitF.in(provider, BigDecimal.class);
                providerDailyLimit = providerDailyLimit.equals(BigDecimal.ZERO) ?
                        providerDefaultDailyLimit : providerDailyLimit;
                BigDecimal providerMonthlyLimit = providerMonthlyLimitF.in(provider, BigDecimal.class);
                providerMonthlyLimit = providerMonthlyLimit.equals(BigDecimal.ZERO) ?
                        providerDefaultMonthlyLimit : providerMonthlyLimit;

                BigDecimal commonDailyAmount = lastPaymentDate.isBefore(today) ?
                        BigDecimal.ZERO :
                        currentDailyAmountF.in(paidAmount, BigDecimal.class);
                BigDecimal commonMonthlyAmount = lastPaymentDate.isBefore(firstDayOfMonth) ?
                        BigDecimal.ZERO :
                        currentMonthlyAmountF.in(paidAmount, BigDecimal.class);

                BigDecimal sum;
                for (IObject invoice : group.getValue()) {
                    sum = invoiceSumF.in(invoice, BigDecimal.class);
                    commonDailyAmount = commonDailyAmount.add(sum);
                    commonMonthlyAmount = commonMonthlyAmount.add(sum);
                    checkLimit(commonDailyAmount, providerDailyLimit,
                            "Daily pay_limit for provider " + providerId +
                                    " and account " + account + " has been exceeded");
                    checkLimit(commonMonthlyAmount, providerMonthlyLimit,
                            "Monthly pay_limit for provider " + providerId +
                                    " and account " + account + " has been exceeded");
                }
            }
        } catch (ReadValueException | InvalidArgumentException |
                ResolutionException | GetCacheItemException | QueryExecutionException ex) {
            throw new CheckPayLimitException("Error during check provider payment limits: " +
                    ex.getMessage(), ex);
        }
    }

    public void updatePaidAmount(final IUpdatePaidAmountMessage message)
            throws UpdatingPaidAmountException, PoolGuardException {

        try {
            List<IObject> invoices = message.getInvoices();
            LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
            LocalDateTime firstDayOfMonth = LocalDateTime.of(LocalDate.now().withDayOfMonth(1), LocalTime.MIDNIGHT);

            Map<Key, List<IObject>> invoicesGroups = groupInvoicesByProviderIdAndAccount(invoices);
            String providerId, account;
            for (Map.Entry<Key, List<IObject>> group : invoicesGroups.entrySet()) {
                providerId = group.getKey().PROVIDER_ID;
                account = group.getKey().ACCOUNT;

                IObject paidAmount;
                try {
                    paidAmount = getPaidAmount(providerId, account);
                } catch (IndexOutOfBoundsException ex) {
                    throw new UpdatingPaidAmountException("Payment amount for provider id : [" +
                            providerId + "] and account: [" + account + "] is not found!", ex);
                }

                LocalDateTime lastPaymentDate = LocalDateTime.from(
                        standardFormatter.parse(lastPaymentDateF.in(paidAmount)));

                for (IObject invoice : group.getValue()) {
                    BigDecimal invoiceSum = invoiceSumF.in(invoice, BigDecimal.class);

                    BigDecimal currentDailyAmount = lastPaymentDate.isBefore(today) ?
                            invoiceSum : invoiceSum.add(currentDailyAmountF.in(paidAmount, BigDecimal.class));
                    BigDecimal currentMonthlyAmount = lastPaymentDate.isBefore(firstDayOfMonth) ?
                            invoiceSum : invoiceSum.add(currentMonthlyAmountF.in(paidAmount, BigDecimal.class));

                    currentDailyAmountF.out(paidAmount, currentDailyAmount);
                    currentMonthlyAmountF.out(paidAmount, currentMonthlyAmount);
                    //TODO:: Clarify, that we should set current day
                    lastPaymentDateF.out(paidAmount, today.format(standardFormatter));

                    queryExecutor.executeUpsert(paidAmountCollection, paidAmount);
                }
            }
        } catch (ReadValueException | InvalidArgumentException | ChangeValueException |
                ResolutionException | QueryExecutionException | NullPointerException ex) {
            throw new UpdatingPaidAmountException("Can't update current pay_limit values", ex);
        }
    }

    private Map<Key, List<IObject>> groupInvoicesByProviderIdAndAccount(final List<IObject> invoices)
            throws ReadValueException, InvalidArgumentException {

        Map<Key, List<IObject>> groups = new HashMap<>(invoices.size());
        for (IObject invoice : invoices) {
            Key key = new Key(providerIdF.in(invoice), accountF.in(invoice));
            List<IObject> groupInvoices = Optional.ofNullable(groups.get(key))
                    .orElse(new ArrayList<>());
            groupInvoices.add(invoice);
            groups.putIfAbsent(key, groupInvoices);
        }
        return groups;
    }

    private void checkLimit(BigDecimal amount, BigDecimal limit, String error) throws ExceedingPayLimitException {
        if (amount.compareTo(limit) == 1) {
            throw new ExceedingPayLimitException(error);
        }
    }

    private void initPaidAmount(IObject paidAmount, String providerId, String account)
            throws QueryExecutionException, PoolGuardException {
        try {
            accountDbF.out(paidAmount, account);
            providerIdDbF.out(paidAmount, providerId);
            currentDailyAmountF.out(paidAmount, BigDecimal.ZERO);
            currentMonthlyAmountF.out(paidAmount, BigDecimal.ZERO);
            LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
            lastPaymentDateF.out(paidAmount, today.format(standardFormatter));
            queryExecutor.executeUpsert(paidAmountCollection, paidAmount);
        } catch (ChangeValueException | InvalidArgumentException ex) {
            throw new QueryExecutionException(ex.getMessage(), ex);
        }
    }

    private IObject getPaidAmount(String providerId, String account)
            throws ResolutionException, PoolGuardException, QueryExecutionException {

        try {
            List<IObject> result = queryExecutor.executeSearch(
                    paidAmountCollection,
                    prepareProviderQueryParams(providerId, account)
            );
            if (result.size() > 1) {
                throw new QueryExecutionException("Search result has too many records with provider id: ["
                        + providerId + "] and account: [" + account + "]!");
            }
            return result.get(0);
        } catch (ChangeValueException | InvalidArgumentException ex) {
            throw new QueryExecutionException(ex.getMessage(), ex);
        }
    }

    private IObject prepareProviderQueryParams(final String providerId, final String account)
            throws ResolutionException, ChangeValueException, InvalidArgumentException {

        IKey iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
        IObject page = IOC.resolve(iObjectKey);
        IObject searchQuery = IOC.resolve(iObjectKey);
        IObject searchFilter = IOC.resolve(iObjectKey);
        IObject accountCriteria = IOC.resolve(iObjectKey);
        IObject providerIdCriteria = IOC.resolve(iObjectKey);

        conditionField.eq().out(providerIdCriteria, providerId);
        providerIdDbF.out(searchFilter, providerIdCriteria);

        conditionField.eq().out(accountCriteria, account);
        accountDbF.out(searchFilter, accountCriteria);

        searchQueryField.pageNumber().out(page, 1);
        searchQueryField.pageSize().out(page, 1);

        searchQueryField.page().out(searchQuery, page);
        searchQueryField.filter().out(searchQuery, searchFilter);

        return searchQuery;
    }

}
