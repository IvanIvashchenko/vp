package com.perspective.vpng.actor.pay_limit;

import com.perspective.vpng.actor.pay_limit.exception.CheckPayLimitException;
import com.perspective.vpng.actor.pay_limit.exception.ExceedingPayLimitException;
import com.perspective.vpng.actor.pay_limit.exception.UpdatingPaidAmountException;
import com.perspective.vpng.actor.pay_limit.util.IQueryExecutor;
import com.perspective.vpng.actor.pay_limit.wrapper.ICheckUserPayMessage;
import com.perspective.vpng.actor.pay_limit.wrapper.IUpdatePaidAmountMessage;
import com.perspective.vpng.database.field.IConditionField;
import com.perspective.vpng.database.field.IDBFieldsHolder;
import com.perspective.vpng.database.field.ISearchQueryField;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.database.database_storage.exceptions.QueryExecutionException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 *
 */
public class UserPayLimitActor {

    private final String userCollection;
    private final String paidAmountCollection;
    private final IQueryExecutor queryExecutor;
    private final IObject usersPaymentsLimits;

    private final IKey determinationUserTypeStrategyKey;
    private final IKey fieldKey;

    private final DateTimeFormatter standardFormatter;

    private final ISearchQueryField searchQueryField;
    private final IConditionField conditionField;
    private final IField userIdF;
    private final IField invoiceSumF;
    private final IField currentDailyAmountF;
    private final IField currentMonthlyAmountF;
    private final IField dailyPaymentLimitF;
    private final IField monthlyPaymentLimitF;
    private final IField lastPaymentDateF;

    /**
     * @param userCollection
     * @param paidAmountCollection
     * @param determinationUserTypeStrategy
     * @param usersPaymentsLimits
     * @param queryExecutor
     * @throws CheckPayLimitException
     */
    public UserPayLimitActor(
            String userCollection,
            String paidAmountCollection,
            String determinationUserTypeStrategy,
            final IObject usersPaymentsLimits,
            IQueryExecutor queryExecutor
    ) throws CheckPayLimitException {
        try {
            this.userCollection = userCollection;
            this.paidAmountCollection = paidAmountCollection;
            determinationUserTypeStrategyKey = Keys.getOrAdd(determinationUserTypeStrategy);
            this.usersPaymentsLimits = usersPaymentsLimits;
            this.queryExecutor = queryExecutor;

            IDBFieldsHolder dbFieldsHolder = IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()));
            searchQueryField = dbFieldsHolder.searchQuery();
            conditionField = dbFieldsHolder.conditions();

            fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            standardFormatter = IOC.resolve(Keys.getOrAdd("datetime_formatter"));
            userIdF = IOC.resolve(fieldKey, "userId");
            invoiceSumF = IOC.resolve(fieldKey, "сумма_без_комиссии");
            currentDailyAmountF = IOC.resolve(fieldKey, "currentDailyAmount");
            currentMonthlyAmountF = IOC.resolve(fieldKey, "currentMonthlyAmount");
            dailyPaymentLimitF = IOC.resolve(fieldKey, "dailyPaymentLimit");
            monthlyPaymentLimitF = IOC.resolve(fieldKey, "monthlyPaymentLimit");
            lastPaymentDateF = IOC.resolve(fieldKey, "date");
        } catch (ResolutionException ex) {
            throw new CheckPayLimitException(ex.getMessage(), ex);
        }
    }

    public void checkPayLimit(final ICheckUserPayMessage message)
            throws CheckPayLimitException, PoolGuardException, ExceedingPayLimitException {

        try {
            final String userId = message.getUserId();
            if (userId == null || userId.isEmpty()) {
                return;
            }

            IObject paidAmount;
            try {
                paidAmount = getPaidAmount(userId);
            } catch (IndexOutOfBoundsException ex) {
                paidAmount = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                initPaidAmount(paidAmount, userId);
            }

            LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
            LocalDateTime firstDayOfMonth = LocalDateTime.of(
                    LocalDate.now().withDayOfMonth(1),
                    LocalTime.MIDNIGHT);
            LocalDateTime lastPaymentDate = LocalDateTime.from(
                    standardFormatter.parse(lastPaymentDateF.in(paidAmount)));

            final IObject user = getUserById(userId);
            final String userType = IOC.resolve(determinationUserTypeStrategyKey, user);
            final IField userTypeF = IOC.resolve(fieldKey, userType);
            final IObject userPaymentLimits = userTypeF.in(usersPaymentsLimits);

            BigDecimal dailyPaymentLimit = dailyPaymentLimitF.in(userPaymentLimits, BigDecimal.class);
            BigDecimal monthlyPaymentLimit = monthlyPaymentLimitF.in(userPaymentLimits, BigDecimal.class);

            BigDecimal commonDailyAmount = lastPaymentDate.isBefore(today) ?
                    BigDecimal.ZERO :
                    currentDailyAmountF.in(paidAmount, BigDecimal.class);
            BigDecimal commonMonthlyAmount = lastPaymentDate.isBefore(firstDayOfMonth) ?
                    BigDecimal.ZERO :
                    currentMonthlyAmountF.in(paidAmount, BigDecimal.class);

            BigDecimal sum;
            final List<IObject> invoices = message.getInvoices();
            for (IObject invoice : invoices) {
                sum = invoiceSumF.in(invoice, BigDecimal.class);
                commonDailyAmount = commonDailyAmount.add(sum);
                commonMonthlyAmount = commonMonthlyAmount.add(sum);
                checkLimit(commonDailyAmount, dailyPaymentLimit,
                        "Daily payment pay_limit for user with id: [" + userId + "] has been exceeded!");
                checkLimit(commonMonthlyAmount, monthlyPaymentLimit,
                        "Monthly payment pay_limit for user with id: [" + userId + "] has been exceeded!");
            }
        } catch (ResolutionException | QueryExecutionException |
                ReadValueException | InvalidArgumentException ex) {
            throw new CheckPayLimitException("Error during check user payment limits: " + ex.getMessage(), ex);
        }
    }

    public void updatePaidAmount(final IUpdatePaidAmountMessage message)
            throws PoolGuardException, UpdatingPaidAmountException {

        String userId = null;
        try {
            userId = message.getUserId();
            if (userId == null || userId.isEmpty()) {
                return;
            }

            LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
            LocalDateTime firstDayOfMonth = LocalDateTime.of(LocalDate.now().withDayOfMonth(1), LocalTime.MIDNIGHT);

            BigDecimal commonSum = BigDecimal.ZERO;
            List<IObject> invoices = message.getInvoices();
            for (IObject invoice : invoices) {
                commonSum = commonSum.add(invoiceSumF.in(invoice, BigDecimal.class));
            }

            IObject paidAmount = getPaidAmount(userId);
            LocalDateTime lastPaymentDate = LocalDateTime.from(
                    standardFormatter.parse(lastPaymentDateF.in(paidAmount)));
            BigDecimal currentDailyAmount = lastPaymentDate.isBefore(today) ?
                    commonSum : commonSum.add(currentDailyAmountF.in(paidAmount, BigDecimal.class));
            BigDecimal currentMonthlyAmount = lastPaymentDate.isBefore(firstDayOfMonth) ?
                    commonSum : commonSum.add(currentMonthlyAmountF.in(paidAmount, BigDecimal.class));

            currentDailyAmountF.out(paidAmount, currentDailyAmount);
            currentMonthlyAmountF.out(paidAmount, currentMonthlyAmount);
            lastPaymentDateF.out(paidAmount, today.format(standardFormatter));

            queryExecutor.executeUpsert(paidAmountCollection, paidAmount);
        } catch (ResolutionException | QueryExecutionException | ReadValueException |
                InvalidArgumentException | ChangeValueException | NullPointerException ex) {
            throw new UpdatingPaidAmountException("Can't update payment amount: " + ex.getMessage(), ex);
        } catch (IndexOutOfBoundsException ex) {
            throw new UpdatingPaidAmountException("Payment amount for user with id: [" +
                    userId + "] is not found!", ex);
        }
    }

    private void checkLimit(BigDecimal amount, BigDecimal limit, String error) throws ExceedingPayLimitException {
        if (amount.compareTo(limit) == 1) {
            throw new ExceedingPayLimitException(error);
        }
    }

    private void initPaidAmount(IObject paidAmount, String userId)
            throws PoolGuardException, QueryExecutionException {

        try {
            userIdF.out(paidAmount, userId);
            currentDailyAmountF.out(paidAmount, BigDecimal.ZERO);
            currentMonthlyAmountF.out(paidAmount, BigDecimal.ZERO);
            LocalDateTime today = LocalDateTime.of(LocalDate.now(), LocalTime.MIDNIGHT);
            lastPaymentDateF.out(paidAmount, today.format(standardFormatter));
            queryExecutor.executeUpsert(paidAmountCollection, paidAmount);
        } catch (ChangeValueException | InvalidArgumentException ex) {
            throw new QueryExecutionException(ex.getMessage(), ex);
        }
    }

    private IObject getUserById(String userId)
            throws ResolutionException, PoolGuardException, QueryExecutionException {
        try {
            List<IObject> users = queryExecutor.executeSearch(
                    userCollection,
                    prepareSearchByUserIdQueryParams(userId)
            );
            if (users.size() > 1) {
                throw new QueryExecutionException("Search result has too many users with id: [" + userId + "]!");
            }
            return users.get(0);
        } catch (ChangeValueException | InvalidArgumentException ex) {
            throw new QueryExecutionException(ex.getMessage(), ex);
        }
    }

    private IObject getPaidAmount(String userId)
            throws ResolutionException, PoolGuardException, QueryExecutionException {

        try {
            List<IObject> result = queryExecutor.executeSearch(
                    paidAmountCollection,
                    prepareSearchByUserIdQueryParams(userId)
            );
            if (result.size() > 1) {
                throw new QueryExecutionException("Search result has too many records for user id: [" + userId + "]!");
            }
            return result.get(0);
        } catch (ChangeValueException | InvalidArgumentException ex) {
            throw new QueryExecutionException(ex.getMessage(), ex);
        }
    }

    private IObject prepareSearchByUserIdQueryParams(String id)
            throws ResolutionException, ChangeValueException, InvalidArgumentException {

        IKey iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
        IObject page = IOC.resolve(iObjectKey);
        IObject searchQuery = IOC.resolve(iObjectKey);
        IObject searchFilter = IOC.resolve(iObjectKey);
        IObject userIdCriteria = IOC.resolve(iObjectKey);

        conditionField.eq().out(userIdCriteria, id);
        userIdF.out(searchFilter, userIdCriteria);

        searchQueryField.pageNumber().out(page, 1);
        searchQueryField.pageSize().out(page, 1);
        searchQueryField.page().out(searchQuery, page);
        searchQueryField.filter().out(searchQuery, searchFilter);

        return searchQuery;
    }

}
