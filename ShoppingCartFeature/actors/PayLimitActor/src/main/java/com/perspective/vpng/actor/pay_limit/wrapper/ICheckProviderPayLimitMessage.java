package com.perspective.vpng.actor.pay_limit.wrapper;

import com.perspective.vpng.actor.pay_limit.ProviderPayLimitActor;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for message of {@link ProviderPayLimitActor}
 */
public interface ICheckProviderPayLimitMessage {

    /**
     * @return list of payment's invoices
     * @throws ReadValueException if any error is occurred
     */
    List<IObject> getInvoices() throws ReadValueException;

}
