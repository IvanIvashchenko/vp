package com.perspective.vpng.actor.pay_limit.exception;

public class ExceedingPayLimitException extends Exception {

    public ExceedingPayLimitException(String message) {
        super(message);
    }

}
