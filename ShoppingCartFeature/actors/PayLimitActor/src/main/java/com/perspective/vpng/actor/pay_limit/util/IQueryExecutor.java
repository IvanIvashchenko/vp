package com.perspective.vpng.actor.pay_limit.util;

import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.database.database_storage.exceptions.QueryExecutionException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

import java.util.List;

public interface IQueryExecutor {

    List<IObject> executeSearch(String collectionName, IObject params)
            throws PoolGuardException, QueryExecutionException;

    void executeUpsert(String collectionName, IObject value)
            throws PoolGuardException, QueryExecutionException;

}
