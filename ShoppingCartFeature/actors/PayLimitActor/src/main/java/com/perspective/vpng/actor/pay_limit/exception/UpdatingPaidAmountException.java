package com.perspective.vpng.actor.pay_limit.exception;

public class UpdatingPaidAmountException extends Exception {

    public UpdatingPaidAmountException(String message, Throwable cause) {
        super(message, cause);
    }

}
