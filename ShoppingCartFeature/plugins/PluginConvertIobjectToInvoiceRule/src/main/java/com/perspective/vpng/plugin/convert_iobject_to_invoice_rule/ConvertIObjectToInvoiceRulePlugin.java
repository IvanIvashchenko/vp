package com.perspective.vpng.plugin.convert_iobject_to_invoice_rule;

import com.perspective.vpng.actor.commission.wrappers.IInvoice;
import com.perspective.vpng.rule.convert_iobject_to_wrapper_rule.ConvertIObjectToWrapperRule;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.iobject_extension.configuration_object.ConfigurationObject;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Collections;

/**
 * Converts object type of {@link IObject} to {@link IInvoice} wrapper.
 */
public class ConvertIObjectToInvoiceRulePlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ConvertIObjectToInvoiceRulePlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link ConvertIObjectToWrapperRule} for {@link IInvoice} wrapper and
     *                  registers in <code>IOC</code> with key <code>convertIObjectToServiceRule</code>.
     * Begin loads after loading plugins: <code>IOC</code>, <code>wds_object</code> and
     *                  before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ConvertIObjectToInvoiceRulePlugin");
            item
//                    .after("IOC")
//                    .after("wds_object")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IObject wrapperConfig = new ConfigurationObject(
                                    "{" +
                                    "   \"in_getSum\" : \"wrapperData/всего-к-оплате\"," +
                                    "   \"in_getCommissionService\" : [" +
                                    "       {" +
                                    "           \"name\" : \"gettingCommissionFromServicesRule\"," +
                                    "           \"args\" :  [\"wrapperData/услуги\"]" +
                                    "       }," +
                                    "       {" +
                                    "           \"name\" : \"convertIObjectToServiceRule\"," +
                                    "           \"args\" : [\"local/value\"]" +
                                    "       }" +
                                    "   ]," +
                                    "   \"out_setSumWithoutCommission\" : \"wrapperData/сумма_без_комиссии\"," +
                                    "   \"out_setCommission\" : \"wrapperData/комиссия\"," +
                                    "   \"out_setSum\" : \"wrapperData/всего-к-оплате\"" +
                                    "}"
                            );
                            IOC.resolve(
                                    Keys.getOrAdd(IResolveDependencyStrategy.class.getCanonicalName()),
                                    "convertIObjectToInvoiceRule",
                                    new ConvertIObjectToWrapperRule(
                                            IInvoice.class,
                                            wrapperConfig,
                                            Collections.singletonList("wrapperData")
                                    )
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException(
                                    "ConvertIObjectToWrapperRule plugin can't load: " +
                                            "can't get ConvertIObjectToWrapperRule key", ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
