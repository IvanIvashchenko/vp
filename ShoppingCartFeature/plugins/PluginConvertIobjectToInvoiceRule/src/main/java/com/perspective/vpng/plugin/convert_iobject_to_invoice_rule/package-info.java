/**
 * Contains transformation rule for convert a some object from
 *                  {@link info.smart_tools.smartactors.core.iobject.IObject} type to
 *                  {@link com.perspective.vpng.actor.commission.wrappers.IInvoice} type.
 */
package com.perspective.vpng.plugin.convert_iobject_to_invoice_rule;