/**
 * Contains a loading plugin for {@link com.perspective.vpng.rule.getting_commission_from_services.GettingCommissionFromServicesRule} rule.
 */
package com.perspective.vpng.plugin.getting_commission_from_services_rule;