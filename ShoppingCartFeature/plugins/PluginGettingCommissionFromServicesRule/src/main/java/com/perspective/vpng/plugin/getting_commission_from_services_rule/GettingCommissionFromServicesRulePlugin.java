package com.perspective.vpng.plugin.getting_commission_from_services_rule;

import com.perspective.vpng.rule.getting_commission_from_services.GettingCommissionFromServicesRule;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link GettingCommissionFromServicesRule} rule.
 */
public class GettingCommissionFromServicesRulePlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor.
     * @param bootstrap bootstrap element.
     */
    public GettingCommissionFromServicesRulePlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link GettingCommissionFromServicesRule} rule and
     *              registers in <code>IOC</code> with key <code>gettingCommissionFromServicesRule</code>.
     * Begin loads rule after loading plugins: <code>IOC</code>, <code>wds_object</code>
     *              and before: <code>configure</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("GettingCommissionFromServicesRule");
            item
/*                    .after("IOC")
                    .after("wds_object")
                    .before("starter")*/
                    .process(() -> {
                        try {
                            IOC.resolve(
                                    Keys.getOrAdd(IResolveDependencyStrategy.class.getCanonicalName()),
                                    "gettingCommissionFromServicesRule",
                                    new GettingCommissionFromServicesRule("комиссия")
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException("GettingCommissionFromServicesRule plugin can't load: " +
                                    "can't get GettingCommissionFromServicesRule key", ex
                            );
                        } catch (ResolveDependencyStrategyException ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
