package com.perspective.vpng.plugin.format_shopping_cart_step_two;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Plugin for register strategies for choosing payment method.
 */
public class PaymentMethodPlugin implements IPlugin {
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor
     * @param bootstrap the bootstrap
     */
    public PaymentMethodPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            Map<String, IObject> map = new HashMap<>();
            IBootstrapItem<String> item = new BootstrapItem("GetPaymentMethodPlugin");

            item
//                .after("IOC")
//                .after("iobject")
//                .before("starter")
                .process(() -> {
                    try {

                        map.put("communal", IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                            "{" +
                                "\"label\":\"Выбрать способ оплаты:\"," +
                                "\"name\":\"Способ оплаты\"," +
                                "\"images\": [" +
                                    "{" +
                                        "\"selected\": true," +
                                        "\"sourceType\": \"VISA\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/visa.png\"" +
                                    "}," +
                                    "{" +
                                        "\"sourceType\": \"VISA_ELECTRON\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/visa-electron.png\"" +
                                    "}," +
                                    "{" +
                                        "\"sourceType\": \"MASTERCARD\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/mastercard.png\"" +
                                    "}," +
                                    "{" +
                                        "\"sourceType\": \"MAESTRO\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/maestro.png\"" +
                                    "}" +
                                "]" +
                            "}"
                        ));
                        map.put("standard", IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                            "{" +
                                "\"label\":\"Выбрать способ оплаты:\"," +
                                "\"name\":\"Способ оплаты\"," +
                                "\"images\": [" +
                                    "{" +
                                        "\"sourceType\": \"VISA_ELECTRON\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/visa-electron.png\"" +
                                    "}," +
                                    "{" +
                                        "\"sourceType\": \"MASTERCARD\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/mastercard.png\"" +
                                    "}," +
                                    "{" +
                                        "\"sourceType\": \"MAESTRO\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/maestro.png\"" +
                                    "}" +
                                "]" +
                            "}"
                        ));
                        map.put("mixed", IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                            "{" +
                                "\"label\":\"Выбрать способ оплаты:\"," +
                                "\"name\":\"Способ оплаты\"," +
                                "\"images\": [" +
                                    "{" +
                                        "\"selected\": true," +
                                        "\"sourceType\": \"VISA\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/visa.png\"" +
                                    "}," +
                                    "{" +
                                        "\"sourceType\": \"MAESTRO\"," +
                                        "\"url\": \"https://vp.ru/resources/media/images/cart/card/maestro.png\"" +
                                    "}" +
                                "]" +
                            "}"
                        ));

                        IKey actorKey = Keys.getOrAdd("get_payment_method");
                        IOC.register(actorKey,
                            new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    String type = (String) args[0];
                                    return map.get(type);
                                }
                            ));
                    } catch (ResolutionException e) {
                        throw new ActionExecuteException("UserPaymentHistory plugin can't load: can't get key");
                    } catch (InvalidArgumentException e) {
                        throw new ActionExecuteException("UserPaymentHistory plugin can't load: can't create rule");
                    } catch (RegistrationException e) {
                        throw new ActionExecuteException("UserPaymentHistory plugin can't load: can't register new rule");
                    }
                });
            bootstrap.add(item);
        }  catch (Exception e) {
            throw new PluginException("Can't get BootstrapItem", e);
        }
    }
}