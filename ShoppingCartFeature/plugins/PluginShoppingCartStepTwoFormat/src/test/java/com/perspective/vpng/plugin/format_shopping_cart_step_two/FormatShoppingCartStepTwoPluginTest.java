package com.perspective.vpng.plugin.format_shopping_cart_step_two;

import com.perspective.vpng.actor.format_shopping_cart_step_two.FormatShoppingCartStepTwoActor;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.base.interfaces.iaction.IPoorAction;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;
import static org.powermock.api.mockito.PowerMockito.verifyNew;
import static org.powermock.api.mockito.PowerMockito.whenNew;

@PrepareForTest({IOC.class, Keys.class, IPoorAction.class, ApplyFunctionToArgumentsStrategy.class, FormatShoppingCartStepTwoPlugin.class})
@RunWith(PowerMockRunner.class)
public class FormatShoppingCartStepTwoPluginTest {
    private FormatShoppingCartStepTwoPlugin targetPlugin;
    private IBootstrap bootstrap;

    @Before
    public void before() {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        bootstrap = mock((IBootstrap.class));

        targetPlugin = new FormatShoppingCartStepTwoPlugin(bootstrap);
    }

    @Test
    public void Should_CorrectLoadPlugin() throws Exception {
        BootstrapItem item = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("FormatShoppingCartStepTwoActorPlugin").thenReturn(item);
        when(item.after(any())).thenReturn(item);
        when(item.before(any())).thenReturn(item);

        targetPlugin.load();

        verifyNew(BootstrapItem.class).withArguments("FormatShoppingCartStepTwoActorPlugin");
//        verify(item).after("IOC");
//        verify(item).before("starter");

        verify(bootstrap).add(item);

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);
        verify(item).process(actionArgumentCaptor.capture());

        IKey actorKey = mock(IKey.class);
        PowerMockito.when(Keys.getOrAdd(FormatShoppingCartStepTwoActor.class.getCanonicalName())).thenReturn(actorKey);

        ArgumentCaptor<ApplyFunctionToArgumentsStrategy> createNewInstanceStrategyArgumentCaptor =
                ArgumentCaptor.forClass(ApplyFunctionToArgumentsStrategy.class);
        actionArgumentCaptor.getValue().execute();

        verifyStatic();
        IOC.register(eq(actorKey), createNewInstanceStrategyArgumentCaptor.capture());

        FormatShoppingCartStepTwoActor actor = mock(FormatShoppingCartStepTwoActor.class);
        whenNew(FormatShoppingCartStepTwoActor.class).withAnyArguments().thenReturn(actor);
    }

    @Test(expected = PluginException.class)
    public void Should_ThrowPluginException_When_BootstrapItemError() throws Exception {
        whenNew(BootstrapItem.class).withAnyArguments().thenThrow(new InvalidArgumentException(""));
        targetPlugin.load();

        verifyNew(BootstrapItem.class).withArguments("FormatShoppingCartStepTwoActorPlugin");
    }
}
