package com.perspective.vpng.plugin.convert_iobject_to_service_rule;

import com.perspective.vpng.actor.commission.wrappers.ICommissionService;
import com.perspective.vpng.rule.convert_iobject_to_wrapper_rule.ConvertIObjectToWrapperRule;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.iobject_extension.configuration_object.ConfigurationObject;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Collections;

/**
 * Converts object type of {@link IObject} to {@link ICommissionService} wrapper.
 */
public class ConvertIObjectToServiceRulePlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ConvertIObjectToServiceRulePlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link ConvertIObjectToWrapperRule} for {@link ICommissionService} wrapper and
     *                  registers in <code>IOC</code> with key <code>convertIObjectToServiceRule</code>.
     * Begin loads after loading plugins: <code>IOC</code>, <code>wds_object</code> and
     *                  before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ConvertIObjectToServiceRulePlugin");
            item
//                    .after("IOC")
//                    .after("wds_object")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IObject wrapperConfig = new ConfigurationObject(
                                    "{" +
                                    "   \"out_setSum\" : \"wrapperData/сумма\"," +
                                    "   \"in_getEnvironment\" : \"wrapperData\"" +
                                    "}"
                            );
                            IOC.resolve(
                                    Keys.getOrAdd(IResolveDependencyStrategy.class.getCanonicalName()),
                                    "convertIObjectToServiceRule",
                                    new ConvertIObjectToWrapperRule(
                                            ICommissionService.class,
                                            wrapperConfig,
                                            Collections.singletonList("wrapperData")
                                    )
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException(
                                    "ConvertIObjectToWrapperRule plugin can't load: " +
                                            "can't get ConvertIObjectToWrapperRule key", ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
