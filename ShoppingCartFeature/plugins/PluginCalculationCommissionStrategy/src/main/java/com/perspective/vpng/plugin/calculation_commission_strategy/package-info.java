/**
 * Contains plugin for loading {@link com.perspective.vpng.strategy.calculation_commission.CalculationCommissionStrategy} rule.
 */
package com.perspective.vpng.plugin.calculation_commission_strategy;