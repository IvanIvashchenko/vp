package com.perspective.vpng.plugin.calculation_commission_strategy;

import com.perspective.vpng.strategy.calculation_commission.CalculationCommissionStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Plugin for loading a {@link CalculationCommissionStrategy} rule.
 */
public class CalculationCommissionStrategyPlugin implements IPlugin {
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor.
     * @param bootstrap bootstrap element.
     */
    public CalculationCommissionStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads a {@link CalculationCommissionStrategy} and
     *                  registers <code>IOC</code> with key <code>CalculationCommissionStrategy</code>.
     * Begin loading plugin after loading plugins: <code>IOC</code>, <code>wds_object</code>, <code>IFieldPlugin</code>
     *                  and before: <code>configure</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("CalculationCommissionStrategy");
            item
/*                    .after("IOC")
                    .after("wds_object")
                    .after("IFieldPlugin")
                    .before("configure")*/
                    .process(() -> {
                        try {
                            int commissionScale = 2;
                            Map<IField, CalculationCommissionStrategy.ICommissionStrategy> strategies = new HashMap<>();
                            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                            strategies.put(
                                    IOC.resolve(fieldKey, "fix"),
                                    (sumWithoutCommission, commission) -> commission
                            );
                            strategies.put(
                                    IOC.resolve(fieldKey, "percent"),
                                    (sumWithoutCommission, commission) ->
                                            sumWithoutCommission.multiply(commission.divide(new BigDecimal(100)))
                            );
                            IOC.register(
                                    Keys.getOrAdd("calculationCommissionStrategy"),
                                    new CalculationCommissionStrategy(commissionScale, strategies)
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException(
                                    "ConvertIObjectToWrapperRule plugin can't load: " +
                                            "can't get ConvertIObjectToWrapperRule key", ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
