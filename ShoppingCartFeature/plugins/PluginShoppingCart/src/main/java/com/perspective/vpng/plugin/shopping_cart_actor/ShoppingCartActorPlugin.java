package com.perspective.vpng.plugin.shopping_cart_actor;

import com.perspective.vpng.actor.shopping_cart.CacheShoppingCartActor;
import com.perspective.vpng.actor.shopping_cart.ShoppingCartInMemoryActor;
import com.perspective.vpng.actor.shopping_cart.wrapper.CacheShoppingActorParamMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading shopping cart actor.
 */
public class ShoppingCartActorPlugin implements IPlugin {
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ShoppingCartActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads shopping cart and register in <code>IOC</code>
     *              with key <code>ShoppingCartInMemoryActor.class.getCanonicalName()</code>.
     *
     * @throws PluginException when errors in loading actor.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ShoppingCartActorPlugin");

            item
//                    .after("IOC")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IKey actorKey = Keys.getOrAdd(ShoppingCartInMemoryActor.class.getCanonicalName());
                            IOC.register(actorKey, new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        try {
                                            return new ShoppingCartInMemoryActor(null);
                                        } catch (Exception ex) {
                                            throw new RuntimeException(
                                                    "Can't create shopping cart actor: " + ex.getMessage(), ex);
                                        }
                                    }
                            ));

                            actorKey = Keys.getOrAdd(CacheShoppingCartActor.class.getCanonicalName());
                            IField collectionNameField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
                            IField idField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
                            IOC.register(actorKey, new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        try {
                                            IObject params = (IObject) args[0];
                                            String collectionName = collectionNameField.in(params);
                                            String id = idField.in(params);
                                            return new CacheShoppingCartActor(new CacheShoppingActorParamMessage() {
                                                @Override
                                                public String getCollectionName() throws ReadValueException {
                                                    return collectionName;
                                                }

                                                @Override
                                                public IPool getConnectionPool() throws ReadValueException {
                                                    try {
                                                        Object connectionOptions = IOC.resolve(
                                                            Keys.getOrAdd("PostgresConnectionOptions")
                                                        );
                                                        return IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
                                                    } catch (Exception e) {
                                                        throw new ReadValueException(e);
                                                    }
                                                }

                                                @Override
                                                public String getShoppingCartId() throws ReadValueException {
                                                    return id;
                                                }
                                            });
                                        } catch (Exception ex) {
                                            throw new RuntimeException(
                                                    "Can't create cache shopping cart actor: " + ex.getMessage(), ex);
                                        }
                                    }
                            ));
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException("ShoppingCartActor plugin can't load: can't get ShoppingCartActor key", ex);
                        } catch (InvalidArgumentException ex) {
                            throw new ActionExecuteException("ShoppingCartActor plugin can't load: can't create rule", ex);
                        } catch (RegistrationException ex) {
                            throw new ActionExecuteException("ShoppingCartActor plugin can't load: can't register new rule", ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
