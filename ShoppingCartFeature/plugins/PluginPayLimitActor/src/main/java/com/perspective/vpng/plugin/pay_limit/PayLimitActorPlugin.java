package com.perspective.vpng.plugin.pay_limit;

import com.perspective.vpng.actor.pay_limit.ProviderPayLimitActor;
import com.perspective.vpng.actor.pay_limit.ShoppingCartPayLimitActor;
import com.perspective.vpng.actor.pay_limit.UserPayLimitActor;
import com.perspective.vpng.actor.pay_limit.exception.CheckPayLimitException;
import com.perspective.vpng.actor.pay_limit.util.IQueryExecutor;
import com.perspective.vpng.actor.pay_limit.util.QueryExecutor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;

/**
 * Plugin for {@link ProviderPayLimitActor}
 */
public class PayLimitActorPlugin extends BootstrapPlugin {

    public PayLimitActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PayLimitActorPlugin")
    public void item()
        throws ResolutionException, RegistrationException, InvalidArgumentException {

        ConnectionOptions connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
        IPool connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
        IQueryExecutor queryExecutor = new QueryExecutor(connectionPool);

        IOC.register(
            Keys.getOrAdd("ProviderPayLimitActor"),
            new ApplyFunctionToArgumentsStrategy(
                (args) -> {
                    try {
                        IObject config = (IObject) args[0];

                        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                        IField providerDefaultDailyLimitF = IOC.resolve(fieldKey, "providerDefaultDailyLimit");
                        IField providerDefaultMonthlyLimitF = IOC.resolve(fieldKey, "providerDefaultMonthlyLimit");
                        IField providerCollectionF = IOC.resolve(fieldKey, "providerCollection");
                        IField paidAmountCollectionF = IOC.resolve(fieldKey, "paidAmountCollection");
                        IField cacheCollectionKeyF = IOC.resolve(fieldKey, "cacheCollectionKey");

                        return new ProviderPayLimitActor(
                                providerDefaultDailyLimitF.in(config, BigDecimal.class),
                                providerDefaultMonthlyLimitF.in(config, BigDecimal.class),
                                paidAmountCollectionF.in(config),
                                providerCollectionF.in(config),
                                cacheCollectionKeyF.in(config),
                                queryExecutor
                        );
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
        );

        IOC.register(
                Keys.getOrAdd("UserPayLimitActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> {
                    try {
                        IObject config = (IObject) args[0];

                        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                        IField userCollectionF = IOC.resolve(fieldKey, "userCollection");
                        IField paymentAmountCollectionF = IOC.resolve(fieldKey, "paidAmountCollection");
                        IField definitionUserTypeStrategyF = IOC.resolve(fieldKey, "determinationUserTypeStrategy");
                        IField usersLimitsF = IOC.resolve(fieldKey, "usersPaymentsLimits");

                        return new UserPayLimitActor(
                                userCollectionF.in(config),
                                paymentAmountCollectionF.in(config),
                                definitionUserTypeStrategyF.in(config),
                                usersLimitsF.in(config),
                                queryExecutor
                        );
                    } catch (ResolutionException | ReadValueException | CheckPayLimitException ex) {
                        throw new RuntimeException(ex.getMessage(), ex);
                    }
                })
        );

        IOC.register(
            Keys.getOrAdd("ShoppingCartPayLimitActor"),
            new ApplyFunctionToArgumentsStrategy(
                (args) -> {
                    try {
                        IObject params = (IObject) args[0];
                        IField shoppingCartLimitF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "shoppingCartPayLimit");

                        return new ShoppingCartPayLimitActor(shoppingCartLimitF.in(params, BigDecimal.class));
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
        );
    }

}
