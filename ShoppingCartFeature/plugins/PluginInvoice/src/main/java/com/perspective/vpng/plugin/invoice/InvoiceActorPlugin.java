package com.perspective.vpng.plugin.invoice;

import com.perspective.vpng.actor.invoice.InvoiceActor;
import com.perspective.vpng.actor.invoice.wrapper.InvoiceActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for {@link InvoiceActor}
 */
public class InvoiceActorPlugin implements IPlugin{

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    public InvoiceActorPlugin(IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("InvoiceActorPlugin");

            item
//                    .after("IOC")
//                    .before("starter")
                    .process(()-> {
                        try {
                            IKey ruleKey = Keys.getOrAdd("InvoiceActor");
                            IOC.register(ruleKey, new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject config = (IObject) args[0];

                                        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                                        IField formCacheKeyF = IOC.resolve(fieldKey, "formCacheKey");
                                        IField formCollectionNameF = IOC.resolve(fieldKey, "formCollectionName");
                                        IField providerCacheKeyF = IOC.resolve(fieldKey, "providerCacheKey");
                                        IField providerCollectionNameF = IOC.resolve(fieldKey, "providerCollectionName");
                                        IField fullProviderFieldNameF = IOC.resolve(fieldKey, "fullProviderName");
                                        IField shortProviderFieldNameF = IOC.resolve(fieldKey, "shortProviderName");
                                        IField userIdentifierFieldNameF = IOC.resolve(fieldKey, "userIdentifier");
                                        IField providerIdentifierFieldNameF = IOC.resolve(fieldKey, "providerIdentifierFieldName");
                                        IField dateFieldNameF = IOC.resolve(fieldKey, "dateFieldName");
                                        IField initialSequenceValueF = IOC.resolve(fieldKey, "initialSequenceValue");

                                        return new InvoiceActor(new InvoiceActorConfig() {
                                            @Override
                                            public String getProviderIdentifyFieldName() throws ReadValueException {
                                                try {
                                                    return providerIdentifierFieldNameF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getProviderCollectionName() throws ReadValueException {
                                                try {
                                                    return providerCollectionNameF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getProviderCacheKey() throws ReadValueException {
                                                try {
                                                    return providerCacheKeyF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getFormCollectionName() throws ReadValueException {
                                                try {
                                                    return formCollectionNameF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getFormCacheKey() throws ReadValueException {
                                                try {
                                                    return formCacheKeyF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getFullProviderFieldName() throws ReadValueException {
                                                try {
                                                    return fullProviderFieldNameF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getShortProviderFieldName() throws ReadValueException {
                                                try {
                                                    return shortProviderFieldNameF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getUserIdentifierFieldName() throws ReadValueException {
                                                try {
                                                    return userIdentifierFieldNameF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }

                                            @Override
                                            public String getDateFieldName() throws ReadValueException {
                                                try {
                                                    return dateFieldNameF.in(config);
                                                } catch (InvalidArgumentException ex) {
                                                    throw new ReadValueException(ex.getMessage(), ex);
                                                }
                                            }
                                            @Override
                                            public Integer getInitialSequenceValue() throws ReadValueException {
                                                try {
                                                    return initialSequenceValueF.in(config, Integer.class);
                                                } catch (InvalidArgumentException e) {
                                                    throw new ReadValueException(e);
                                                }
                                            }
                                        });
                                    } catch (Exception ex) {
                                        throw new RuntimeException(
                                            "Can't create invoice actor: " + ex.getMessage(), ex);
                                    }
                                }
                            ));
                        } catch (ResolutionException e) {
                            throw new ActionExecuteException("Can't get key for load", e);
                        } catch (InvalidArgumentException e) {
                            throw new ActionExecuteException("Can't apply lambda", e);
                        } catch (RegistrationException e) {
                            throw new ActionExecuteException("Can't register the rule", e);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't create BootstrapItem", e);
        }
    }
}
