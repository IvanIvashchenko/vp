/**
 * Contains plugin for loading {@link com.perspective.vpng.actor.commission.CommissionActor} actor.
 */
package com.perspective.vpng.plugin.commission_actor;