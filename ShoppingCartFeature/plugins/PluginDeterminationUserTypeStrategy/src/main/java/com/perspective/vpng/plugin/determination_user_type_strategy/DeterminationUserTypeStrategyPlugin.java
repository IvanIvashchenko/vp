package com.perspective.vpng.plugin.determination_user_type_strategy;

import com.perspective.vpng.strategy.determination_user_type.DeterminationUserTypeStrategy;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class DeterminationUserTypeStrategyPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    public DeterminationUserTypeStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("DeterminationUserTypeStrategyPlugin");
            item.process(() -> {
                try {
                    IOC.register(Keys.getOrAdd("determineUserType"), new DeterminationUserTypeStrategy());
                } catch (ResolutionException ex) {
                    throw new ActionExecuteException(
                            "DeterminationUserTypeStrategyPlugin plugin can't load!", ex);
                } catch (Exception ex) {
                    throw new ActionExecuteException(ex.getMessage(), ex);
                }
            });

            bootstrap.add(item);
        } catch (Exception ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
