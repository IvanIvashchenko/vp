import os
import sys
import zipfile
import subprocess


class KeyNotFoundException(Exception):
    def __init__(self, key):
        super(KeyNotFoundException, self).__init__('Expected parameter with key: [%s] was not found!' % key)


class IllegalArgumentException(Exception):
    def __init__(self, message, *args, **kwargs):
        super(KeyNotFoundException, self).__init__(message +
                                                   ' For help start app with parameter "-h" or "-help"')


class cd:
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def help():
    print '\n================= Features Package Manager Manual ====================='
    print
    print '******************************* NOTE ************************************'
    print ' - Script use python27.'
    print ' - Start the script in the terminal by command: "python package_server.py -s srcPath -d destPath ..."'
    print ' - Description of the available parameters see below.'
    print '*************************************************************************'
    print
    print ' -h or -help = get app help'
    print ' -s = path to directory with feature(s) or path to single feature; ' \
          'may be relative(./...) or absolute(/home/...) path; (required)'
    print ' -d = path to directory to be extracted feature(s); must be absolute(/home/...) path;(required)'
    print ' -f = name of single feature to be assembled; incompatible with parameter "-fl"(optional)'
    print ' -fl = list of features names to be assembled(features names must be separated by "," or ";");(optional)'
    print ' -i = execute command "mvn clean install -Dmaven.test.skip=true" before assembly;(optional)'
    print '\n============================= END ===================================\n'


def get_param(args, key):
    if args.count(key):
        keyIndex = args.index(key)
        return args[keyIndex + 1]
    for arg in args:
        if key in arg:
            return arg.strip(key)

    raise KeyNotFoundException(key)


def contains(args, key):
    if args.count(key):
        return True

    for arg in args:
        if key in arg:
            return True

    return False


def print_banner(banner):
    term_width = int(subprocess.check_output(['stty', 'size']).split()[1])
    ban_len = len(banner)
    border_len = term_width if ban_len > term_width else ban_len
    print
    print '*' * border_len
    print banner
    print '*' * border_len
    print


def package_feature(srcPath, destPath, install, featureName=''):
    if install:
        print_banner('Start clean install feature %s without tests ...' % featureName)
        cd(srcPath + featureName)
        install_features()

    print_banner('Start assembling feature %s' % featureName)
    assembly_feature(srcPath, destPath, featureName)
    print_banner('End of assembling feature.\nFeature placed in %s directory.' % destPath)


def package_features(srcPath, destPath, features, install):
    length = len(features)
    if install:
        print_banner('Start clean install features: %s (count = %s) without tests ...' % (features, length))
        install_features()

    print_banner('Start assembling features: %s (count = %s) ...' % (features, length))
    for index, feature in enumerate(features):
        print_banner('Assembly feature: %s; completed: %s of %s' % (feature, index, length))
        assembly_feature(srcPath, destPath, feature)

    print_banner('End of assembling feature.\nFeature placed in %s directory.' % destPath)


def install_features():
    process = subprocess.Popen("mvn -T 1C clean install -Dmaven.test.skip=true",
                               shell=True, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
    for line in iter(process.stdout.readline, ''):
        sys.stdout.write(line)


def assembly_feature(srcPath, destPath, featureName):
    featurePath = srcPath + featureName
    with cd(featurePath):
        process = subprocess.Popen("mvn clean install assembly:assembly",
                                   shell=True, stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT, stdin=subprocess.PIPE)
        for line in iter(process.stdout.readline, ''):
            sys.stdout.write(line)

        files = os.listdir('./target')
        zipFileName = filter(lambda x: x.endswith('.zip'), files)[0]
        pathToArchive = os.path.abspath(os.curdir) + '/target/' + zipFileName
        zipArchive = zipfile.ZipFile(pathToArchive, 'r')
        with cd(destPath):
            zipArchive.extractall()


def handle_f_param(args, srcPath='./', destPath='./', install=True):
    feature = get_param(args, '-f')
    package_feature(srcPath, destPath, install, feature)


def handle_fl_param(args, srcPath='./', destPath='./', install=True):
    features = get_param(args, '-fl').split(",")
    if len(features) == 0:
        features = get_param(args, '-fl').split(";")
        if len(features) == 0:
            raise IllegalArgumentException('Invalid format of "-fl" parameter!')

    package_features(srcPath, destPath, features, install)


def handle_default(srcPath='./', destPath='./', install=True):
    files = os.listdir(srcPath)
    features = filter(lambda x: x.endswith('Feature'), files)
    package_features(srcPath, destPath, features, install)


def pack_server(args):
    if args.count('-help') or args.count('-h'):
        help()
        return

    install = args.count('-i')
    srcPath = get_param(args, '-s')
    destPath = get_param(args, '-d')
    if os.path.isfile(destPath):
        raise IllegalArgumentException('Parameter -d must have a directory path!')

    if os.path.isfile(srcPath):
        package_feature(srcPath, destPath, install)
        return

    if contains(args, '-fl'):
        handle_fl_param(args, srcPath, destPath, install)
    elif contains(args, '-f'):
        handle_f_param(args, srcPath, destPath, install)
        return
    else:
        handle_default(srcPath, destPath, install)


def main(args):
    try:
        pack_server(args)
    except Exception as ex:
        print('\nApp error: %s\n' % str(ex))
        print('For help to run app with a "-h" or "-help" param.')


if __name__ == '__main__':
    main(sys.argv)
