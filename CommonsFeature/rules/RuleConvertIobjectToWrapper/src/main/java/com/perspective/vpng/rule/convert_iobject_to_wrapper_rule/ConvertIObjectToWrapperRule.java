package com.perspective.vpng.rule.convert_iobject_to_wrapper_rule;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.iobject_extension.wds_object.WDSObject;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject_wrapper.IObjectWrapper;
import info.smart_tools.smartactors.message_processing_interfaces.iwrapper_generator.IWrapperGenerator;
import info.smart_tools.smartactors.message_processing_interfaces.iwrapper_generator.exception.WrapperGeneratorException;

import java.util.ArrayList;
import java.util.List;

/**
 * Transformation rule for convert object from {@link IObject} type to some wrapper.
 */
public class ConvertIObjectToWrapperRule implements IResolveDependencyStrategy {
    private final Class wrapperClass;
    private final IObject wrapperConfig;
    private final List<IField> environmentFields;

    private final IWrapperGenerator wg;

    private final IKey iObjectKey;
    private final IKey wdsObjectKey;

    /**
     * Constructor.
     *
     * @param wrapperClass - wrapper class in which convert object.
     * @param wrapperConfig - config for resolution wrapper object.
     * @param environmentNames - names of data objects into environment's object for init wrapper.
     *
     * @throws ResolveDependencyStrategyException when error in creation new instance of rule.
     */
    public ConvertIObjectToWrapperRule(
            final Class wrapperClass,
            final IObject wrapperConfig,
            final List<String> environmentNames
    ) throws ResolveDependencyStrategyException {
        if (!wrapperClass.isInterface()) {
            throw new ResolveDependencyStrategyException("Given wrapper type must be an interface!");
        }
        this.wrapperClass = wrapperClass;
        this.wrapperConfig = wrapperConfig;
        try {
            this.wg = IOC.resolve(Keys.getOrAdd(IWrapperGenerator.class.getCanonicalName()));
            this.environmentFields = new ArrayList<>(environmentNames.size());
            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            for (String name : environmentNames) {
                environmentFields.add(IOC.resolve(fieldKey, name));
            }
            this.iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
            this.wdsObjectKey = Keys.getOrAdd(WDSObject.class.getCanonicalName());
        } catch (ResolutionException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }

    /**
     * Converts a object from {@link IObject} type to some wrapper.
     *
     * @param args - arguments for resolution rule;
     *             must contains objects with data for environment wrapper;
     *             order of environments object must be equals order of list of environment's names in constructor.
     *             @see ConvertIObjectToWrapperRule#ConvertIObjectToWrapperRule(Class, IObject, List)
     * @param <T> - type of result rule.
     *
     * @return object type of some wrapper.
     *
     * @throws ResolveDependencyStrategyException when errors in generation some wrapper or invalid given arguments.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            Object invoice = wg.generate(wrapperClass);
            IObject wrapperEnvironment = IOC.resolve(iObjectKey);
            for (int i = args.length - 1; i >= 0; --i) {
                environmentFields.get(i).out(wrapperEnvironment, args[i]);
            }
            IObject wrapperInitObject = IOC.resolve(wdsObjectKey, wrapperConfig);
            ((IObjectWrapper) wrapperInitObject).init(wrapperEnvironment);
            ((IObjectWrapper) invoice).init(wrapperInitObject);

            return (T) invoice;
        } catch (WrapperGeneratorException ex) {
            throw new ResolveDependencyStrategyException("Couldn't generate a wrapper: " + ex.getMessage(), ex);
        } catch (InvalidArgumentException ex) {
            throw new ResolveDependencyStrategyException("Invalid argument for generated wrapper: " + ex.getMessage(), ex);
        } catch (ChangeValueException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            throw new ResolveDependencyStrategyException("Invalid arguments for resolution rule!", ex);
        }
    }
}
