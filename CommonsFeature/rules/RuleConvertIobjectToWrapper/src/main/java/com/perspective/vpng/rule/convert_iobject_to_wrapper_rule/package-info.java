/**
 * Contains transformation rule for convert object from
 *              {@link info.smart_tools.smartactors.core.iobject.IObject} type to some wrapper.
 */
package com.perspective.vpng.rule.convert_iobject_to_wrapper_rule;