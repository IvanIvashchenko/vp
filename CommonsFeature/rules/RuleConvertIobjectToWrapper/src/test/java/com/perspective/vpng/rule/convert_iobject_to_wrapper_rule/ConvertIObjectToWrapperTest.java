package com.perspective.vpng.rule.convert_iobject_to_wrapper_rule;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;

import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.iobject_extension.wds_object.WDSObject;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject_wrapper.IObjectWrapper;
import info.smart_tools.smartactors.message_processing_interfaces.iwrapper_generator.IWrapperGenerator;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
@SuppressWarnings("unchecked")
public class ConvertIObjectToWrapperTest {

    private static IResolveDependencyStrategy strategy;
    private static IWrapperGenerator wg;
    private static Class wrapperClass;
    private static IField wrapperDataF;
    private static WDSObject wdsObject;
    private static IObject environmentObject;

    private interface ITest extends IObjectWrapper { }

    @BeforeClass
    public static void setUp() throws Exception {
        wg = mock(IWrapperGenerator.class);
        wrapperClass = ITest.class;
        IObject wrapperConfig = mock(IObject.class);
        List<String> environmentNames = Arrays.asList("testEnvironment1", "testEnvironment2");

        mockStatic(IOC.class);
        mockStatic(Keys.class);

        when(IOC.resolve(Keys.getOrAdd(IWrapperGenerator.class.getCanonicalName())))
                .thenReturn(wg);
        wrapperDataF = mock(IField.class);
        IKey fieldKey = mock(IKey.class);
        when(Keys.getOrAdd(IField.class.getCanonicalName())).thenReturn(fieldKey);
        when(IOC.resolve(eq(fieldKey), anyString())).thenReturn(wrapperDataF);
        IKey iObjectKey = mock(IKey.class);
        IKey wdsObjectKey = mock(IKey.class);
        when(Keys.getOrAdd(IObject.class.getCanonicalName())).thenReturn(iObjectKey);
        when(Keys.getOrAdd(WDSObject.class.getCanonicalName())).thenReturn(wdsObjectKey);
        environmentObject = mock(IObject.class);
        when(IOC.resolve(eq(iObjectKey))).thenReturn(environmentObject);
        wdsObject = mock(WDSObject.class);
        when(IOC.resolve(eq(wdsObjectKey), eq(wrapperConfig))).thenReturn(wdsObject);

        strategy = new ConvertIObjectToWrapperRule(wrapperClass, wrapperConfig, environmentNames);
    }

    @Test
    public void convertIObjectToWrapperTest() throws Exception {
        ITest wrapper = mock(ITest.class);
        when(wg.generate(eq(wrapperClass))).thenReturn(wrapper);

        IObject testEnvironment1 = mock(IObject.class);
        IObject testEnvironment2 = mock(IObject.class);

        Object resolvedWrapper = strategy.resolve(testEnvironment1, testEnvironment2);

        assertNotEquals(resolvedWrapper, null);
        assertEquals(resolvedWrapper.getClass(), wrapper.getClass());
        verify(wg).generate(eq(wrapperClass));
        verify((IObjectWrapper) wdsObject).init(eq(environmentObject));
        verify(wrapper).init(wdsObject);
    }

    @After
    public void resetMocks() {
        reset(wg, wrapperDataF, wdsObject, environmentObject);
    }
}
