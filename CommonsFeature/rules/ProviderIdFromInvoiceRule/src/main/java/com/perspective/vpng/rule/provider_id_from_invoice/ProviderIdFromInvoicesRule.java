package com.perspective.vpng.rule.provider_id_from_invoice;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.ArrayList;
import java.util.List;

public class ProviderIdFromInvoicesRule implements IResolveDependencyStrategy {

    private final IField providerIdF;

    public ProviderIdFromInvoicesRule(IField providerIdF) {
        this.providerIdF = providerIdF;
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            if (args[0] instanceof List) {
                List<IObject> invoices = (List) args[0];
                List<String> providerIds = new ArrayList<>(invoices.size());
                for (IObject invoice: invoices) {
                    providerIds.add(providerIdF.in(invoice));
                }
                return (T) providerIds;
            }
            return (T) providerIdF.in((IObject) args[0]);
        } catch (ReadValueException | InvalidArgumentException ex) {
            throw new ResolveDependencyStrategyException(ex);
        }
    }

}
