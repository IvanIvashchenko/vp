package com.perspective.vpng.plugin.get_provider_requisites_strategy;

import com.perspective.vpng.strategy.get_provider_requisites.GetProviderRequisitesStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class GetProviderRequisitesStrategyPlugin extends BootstrapPlugin {
    public GetProviderRequisitesStrategyPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("GetProviderRequisitesStrategyPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("ProviderRequisites"),
                new GetProviderRequisitesStrategy());
    }
}
