package com.perspective.vpng.plugin.fill_now_exist_field_strategy;

import com.perspective.vpng.strategy.fill_not_exist_field_strategy.FillNotExistFieldStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for {@link FillNotExistFieldStrategy}
 */
public class PluginFillNotExistFieldStrategy implements IPlugin{

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    public PluginFillNotExistFieldStrategy(IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("FillNotExistFieldStrategy");

            item
//                    .after("IOC")
//                    .after("wds_object")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IOC.resolve(
                                    Keys.getOrAdd(IResolveDependencyStrategy.class.getCanonicalName()),
                                    "fillNotExistFieldStrategy",
                                    new FillNotExistFieldStrategy()
                            );
                        } catch (Exception e) {
                            throw new ActionExecuteException(
                                    "FillNotExistFieldStrategy plugin can't load: can't get FillNotExistFieldStrategy key", e
                            );
                        }
                    });
            bootstrap.add(item);
        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't create BootstrapItem", e);
        }
    }
}
