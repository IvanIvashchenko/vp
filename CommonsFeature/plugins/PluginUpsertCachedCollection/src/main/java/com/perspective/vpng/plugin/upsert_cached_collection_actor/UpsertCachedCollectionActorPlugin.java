package com.perspective.vpng.plugin.upsert_cached_collection_actor;

import com.perspective.vpng.actor.upsert_cached_collection.UpsertCachedCollectionActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class UpsertCachedCollectionActorPlugin extends BootstrapPlugin {
    public UpsertCachedCollectionActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("UpsertCachedCollectionActorPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("UpsertCachedCollectionActor"),
                new ApplyFunctionToArgumentsStrategy(
                        (args) -> new UpsertCachedCollectionActor())
        );
    }
}