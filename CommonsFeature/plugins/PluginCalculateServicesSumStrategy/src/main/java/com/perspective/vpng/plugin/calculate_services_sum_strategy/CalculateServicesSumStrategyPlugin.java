package com.perspective.vpng.plugin.calculate_services_sum_strategy;

import com.perspective.vpng.strategy.calculate_services_sum.CalculateServicesSumStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class CalculateServicesSumStrategyPlugin extends BootstrapPlugin {
    public CalculateServicesSumStrategyPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("CalculateServicesSumStrategyPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("CalculateServicesSum"),
                new CalculateServicesSumStrategy());
    }
}