package com.perspective.vpng.plugin.provider;

import com.perspective.vpng.actor.provider.ProviderActor;
import com.perspective.vpng.actor.provider.wrapper.IActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link ProviderActor} actor.
 */
public class ProviderActorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ProviderActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link ProviderActor} actor and registers in <cdoe>IOC</cdoe> with key <code>ProviderActor</code>.
     * Begin loading plugin after loading plugins: <code>IOC</code> and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading plugin.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ProviderActorPlugin");

            item
//                    .after("IOC")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IKey actorKey = Keys.getOrAdd("ProviderActor");
                            IOC.register(actorKey,
                                    new ApplyFunctionToArgumentsStrategy(
                                            (args) -> {
                                                try {
                                                    IObject config = (IObject) args[0];

                                                    IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                                                    IField cacheKeyF = IOC.resolve(fieldKey, "cacheKey");
                                                    IField collectionNameF = IOC.resolve(fieldKey, "collectionName");

                                                    return new ProviderActor(new IActorConfig() {
                                                        @Override
                                                        public String getCollectionName() throws ReadValueException {
                                                            try {
                                                                return collectionNameF.in(config);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }

                                                        @Override
                                                        public String getCacheKey() throws ReadValueException {
                                                            try {
                                                                return cacheKeyF.in(config);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                    });
                                                } catch (Exception ex) {
                                                    throw new RuntimeException(
                                                            "Can't create provider actor: " + ex.getMessage(), ex);
                                                }
                                            }
                                    )
                            );
                        } catch (ResolutionException | InvalidArgumentException | RegistrationException ex) {
                            throw new ActionExecuteException("Provider plugin can't load: " + ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
