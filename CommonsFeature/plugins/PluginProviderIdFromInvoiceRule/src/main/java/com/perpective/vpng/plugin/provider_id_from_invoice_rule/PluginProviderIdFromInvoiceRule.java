package com.perpective.vpng.plugin.provider_id_from_invoice_rule;

import com.perspective.vpng.rule.provider_id_from_invoice.ProviderIdFromInvoicesRule;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginProviderIdFromInvoiceRule extends BootstrapPlugin {

    private IResolveDependencyStrategy strategy;

    public PluginProviderIdFromInvoiceRule(IBootstrap bootstrap) throws ResolutionException {
        super(bootstrap);
        IField providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
        strategy = new ProviderIdFromInvoicesRule(providerIdF);
    }

    @Item("PluginProviderIdFromInvoiceRule")
    public void registerRule() {
        try {
            IOC.resolve(
                    Keys.getOrAdd(IResolveDependencyStrategy.class.getCanonicalName()),
                    "getProviderIdFromInvoice",
                    strategy
            );
        } catch (ResolutionException ex) {
            throw new RuntimeException(ex);
        }
    }

}
