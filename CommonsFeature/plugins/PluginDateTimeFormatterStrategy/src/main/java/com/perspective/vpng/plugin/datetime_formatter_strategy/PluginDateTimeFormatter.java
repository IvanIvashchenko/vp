package com.perspective.vpng.plugin.datetime_formatter_strategy;

import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;

import java.time.format.DateTimeFormatter;

/**
 * Plugin for registration rule for date time formatting.
 * Strategy resolves {@link DateTimeFormatter} by dd-MM-yyyy HH:mm:ss pattern.
 */
public class PluginDateTimeFormatter implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * The constructor
     * @param bootstrap the bootstrap
     */
    public PluginDateTimeFormatter(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {

        try {
            IBootstrapItem<String> bootstrapItem = new BootstrapItem("datetime_formatter_plugin");

            bootstrapItem
//                .after("IOC")
                .process(() -> {
                    try {
                        IOC.register(Keys.getOrAdd("datetime_formatter"),
                            new ApplyFunctionToArgumentsStrategy(args -> DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                        );
                    } catch (ResolutionException e) {
                        throw new ActionExecuteException("DateTimeFormatter plugin can't load: can't get DateTimeFormatter key", e);
                    } catch (InvalidArgumentException e) {
                        throw new ActionExecuteException("DateTimeFormatter plugin can't load: can't create rule", e);
                    } catch (RegistrationException e) {
                        throw new ActionExecuteException("DateTimeFormatter plugin can't load: can't register new rule", e);
                    }
                });
            bootstrap.add(bootstrapItem);
        } catch (InvalidArgumentException e) {
            throw new PluginException(e);
        }
    }
}
