package com.perspective.vpng.plugin.memcached_actor;

import com.perspective.vpng.actor.memcached.MemcachedActor;
import com.perspective.vpng.actor.memcached.wrapper.ActorParams;
import com.sun.org.apache.xpath.internal.operations.Bool;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin.
 * Implements {@link IPlugin}
 * MemcachedActor.
 */
public class MemcachedActorPlugin implements IPlugin {
    /** Local storage for instance of {@link IBootstrap}*/
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor for CreateUserPlugin
     * @param bootstrap instance of {@link IBootstrap}
     * @throws InvalidArgumentException if any errors occurred
     */
    public MemcachedActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) throws InvalidArgumentException {
        if (null == bootstrap) {
            throw new InvalidArgumentException("Incoming argument should be not null.");
        }
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("CreateUserActorPlugin");

            item
                .process(() -> {
                    try {
                        IField serverF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "server");
                        IField nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "serverName");
                        IField failoverF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "failover");
                        IField initConnF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "initConn");
                        IField minConnF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "minConn");
                        IField maxConnF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "maxConn");
                        IField maintSleepF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "maintSleep");
                        IField nagleF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "nagle");
                        IField socketTOF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "socketTO");
                        IField aliveCheckF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "aliveCheck");

                        IKey memcachedActorKey = Keys.getOrAdd("MemcachedActor");
                        try {
                            IOC.register(memcachedActorKey, new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    IObject config = (IObject) args[0];
                                    try {
                                        return new MemcachedActor(
                                            new ActorParams() {
                                                @Override
                                                public String getServer() {
                                                    try {
                                                        return serverF.in(config, String.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public String getName() {
                                                    try {
                                                        return nameF.in(config, String.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Boolean getFailover() {
                                                    try {
                                                        return failoverF.in(config, Boolean.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Integer getInitConn() {
                                                    try {
                                                        return initConnF.in(config, Integer.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Integer getMinConn() {
                                                    try {
                                                        return minConnF.in(config, Integer.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Integer getMaxConn() {
                                                    try {
                                                        return maxConnF.in(config, Integer.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Integer getMaintSleep() {
                                                    try {
                                                        return maintSleepF.in(config, Integer.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Boolean getNagle() {
                                                    try {
                                                        return nagleF.in(config, Boolean.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Integer getSocketTO() {
                                                    try {
                                                        return socketTOF.in(config, Integer.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }

                                                @Override
                                                public Boolean getAliveCheck() {
                                                    try {
                                                        return aliveCheckF.in(config, Boolean.class);
                                                    } catch (Exception e) {
                                                        throw new RuntimeException(e);
                                                    }
                                                }
                                            }
                                        );
                                    } catch (Exception e) {
                                        throw new RuntimeException("Error during resolving MemcachedActor", e);
                                    }
                                }
                            ));
                        } catch (RegistrationException e) {
                            throw new RuntimeException(e);
                        } catch (InvalidArgumentException e) {
                            throw new RuntimeException("Can't create actor with this args: ", e);
                        }
                    } catch (ResolutionException e) {
                        throw new RuntimeException("Can't get ActorParams wrapper or Key for ActorParams", e);
                    }
                });
            bootstrap.add(item);
        } catch (Exception e) {
            throw new PluginException(e);
        }
    }
}
