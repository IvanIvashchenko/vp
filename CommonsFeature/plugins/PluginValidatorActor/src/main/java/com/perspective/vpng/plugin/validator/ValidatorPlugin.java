package com.perspective.vpng.plugin.validator;

import com.perspective.vpng.actor.validator.ValidatorActor;
import com.perspective.vpng.actor.validator.wrapper.IConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class ValidatorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ValidatorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ValidatorPlugin");
            item.process(() -> {
                try {
                    IKey actorKey = Keys.getOrAdd("ValidatorActor");
                    IOC.register(actorKey,
                            new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        try {
                                            IObject config = (IObject) args[0];
                                            IField scopeKey = IOC.resolve(
                                                    Keys.getOrAdd(IField.class.getCanonicalName()),
                                                    "scopeKey"
                                            );
                                            return new ValidatorActor(new IConfig() {
                                                @Override
                                                public String getScopeKey() throws ReadValueException {
                                                    try {
                                                        return scopeKey.in(config);
                                                    } catch (InvalidArgumentException ex) {
                                                        throw new ReadValueException(ex.getMessage(), ex);
                                                    }
                                                }
                                            });
                                        } catch (Exception ex) {
                                            throw new RuntimeException(
                                                    "Can't create validator actor: " + ex.getMessage(), ex);
                                        }
                                    }
                            )
                    );
                } catch (ResolutionException | InvalidArgumentException | RegistrationException ex) {
                    throw new ActionExecuteException("Validator plugin can't load: " + ex.getMessage(), ex);
                }
            });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }

}
