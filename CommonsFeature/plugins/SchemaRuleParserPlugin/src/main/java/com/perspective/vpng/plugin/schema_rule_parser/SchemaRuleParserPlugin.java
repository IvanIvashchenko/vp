package com.perspective.vpng.plugin.schema_rule_parser;

import com.perspective.vpng.util.rule_parser.IRuleParser;
import com.perspective.vpng.util.schema_rule_parser.SchemaRuleParser;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class SchemaRuleParserPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public SchemaRuleParserPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("SchemaRuleParserPlugin");
            item.process(() -> {
                try {
                    IRuleParser parser = SchemaRuleParser.create();
                    IKey actorKey = Keys.getOrAdd(IRuleParser.class.getCanonicalName());
                    IOC.register(actorKey, new ApplyFunctionToArgumentsStrategy((args) -> parser));
                } catch (ResolutionException | InvalidArgumentException | RegistrationException ex) {
                    throw new ActionExecuteException("Schema rule parser plugin can't load: " + ex.getMessage(), ex);
                }
            });
            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }

}
