package com.perspective.vpng.plugin.delete_inactive_operations_actor;

import com.perspective.vpng.actor.delete_inactive_operations.DeleteInactiveOperationsActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class DeleteInactiveOperationsActorPlugin implements IPlugin {
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor
     * @param bootstrap the bootstrap
     */
    public DeleteInactiveOperationsActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("DeleteInactiveOperationsActorPlugin");

            item
                .process(() -> {
                    try {
                        IKey createSessionActorKey = Keys.getOrAdd(DeleteInactiveOperationsActor.class.getCanonicalName());
                        IOC.register(createSessionActorKey, new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        return new DeleteInactiveOperationsActor((IObject) args[0]);
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        ));
                    } catch (ResolutionException e) {
                        throw new ActionExecuteException("DeleteInactiveOperationsActor plugin can't load: can't get DeleteInactiveOperationsActor key", e);
                    } catch (InvalidArgumentException e) {
                        throw new ActionExecuteException("DeleteInactiveOperationsActor plugin can't load: can't create rule", e);
                    } catch (RegistrationException e) {
                        throw new ActionExecuteException("DeleteInactiveOperationsActor plugin can't load: can't register new rule", e);
                    }
                });
            bootstrap.add(item);
        } catch (InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }
}
