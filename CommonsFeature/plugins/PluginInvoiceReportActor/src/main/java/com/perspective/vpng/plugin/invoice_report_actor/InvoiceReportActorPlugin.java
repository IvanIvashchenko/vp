package com.perspective.vpng.plugin.invoice_report_actor;

import com.perspective.vpng.actor.invoice_report.InvoiceReportActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class InvoiceReportActorPlugin extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public InvoiceReportActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("InvoiceReportActorPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("InvoiceReportActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> new InvoiceReportActor((IObject) args[0])));
    }
}
