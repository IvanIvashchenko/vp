package com.perspective.vpng.plugin.standard_transformation_strategy;

import com.perspective.vpng.strategy.standard_transformation.SubstringStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class StandardTransformationStrategyPlugin extends BootstrapPlugin {

    public StandardTransformationStrategyPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("StandardTransformationStrategyPlugin")
    public void item() throws ResolutionException, RegistrationException, InvalidArgumentException {

        try {
            IOC.register(Keys.getOrAdd("transformation_substring"), new SubstringStrategy());
        } catch (Exception e) {
            throw new RegistrationException(e);
        }
    }
}
