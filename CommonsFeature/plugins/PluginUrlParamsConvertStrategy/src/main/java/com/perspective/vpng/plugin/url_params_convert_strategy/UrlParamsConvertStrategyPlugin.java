package com.perspective.vpng.plugin.url_params_convert_strategy;

import com.perspective.vpng.strategy.url_params_convert.IObjectToUrlParamsConvertStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class UrlParamsConvertStrategyPlugin extends BootstrapPlugin {

    public UrlParamsConvertStrategyPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("UrlParamsConvertStrategyPlugin")
    public void item() throws ResolutionException, RegistrationException, InvalidArgumentException {

        try {
            IOC.register(Keys.getOrAdd("convertIObjectToUrlParams"), new IObjectToUrlParamsConvertStrategy());
        } catch (Exception e) {
            throw new RegistrationException(e);
        }
    }
}
