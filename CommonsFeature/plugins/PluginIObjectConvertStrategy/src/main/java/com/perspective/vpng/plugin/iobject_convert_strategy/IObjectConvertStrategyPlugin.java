package com.perspective.vpng.plugin.iobject_convert_strategy;

import com.perspective.vpng.strategy.iobject_convert.XmlToIObjectConvertStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class IObjectConvertStrategyPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    public IObjectConvertStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("IObjectConvertStrategyPlugin");
            item.process(() -> {
                try {
                    IOC.register(Keys.getOrAdd("convertXmlToIObject"), new XmlToIObjectConvertStrategy());
                } catch (RegistrationException | ResolutionException ex) {
                    throw new ActionExecuteException("Can't register a XmlToIObjectConvertStrategy!", ex);
                }
            });
            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem!", ex);
        }
    }

}
