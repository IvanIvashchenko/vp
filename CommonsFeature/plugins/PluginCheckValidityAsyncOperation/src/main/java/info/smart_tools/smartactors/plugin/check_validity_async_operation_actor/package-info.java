/**
 * Package contains plugin for {@link com.perspective.vpng.actor.check_validity_async_operation.CheckValidityAsyncOperationActor}
 */
package info.smart_tools.smartactors.plugin.check_validity_async_operation_actor;