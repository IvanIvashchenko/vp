package com.perspective.vpng.plugin.collect_fts_data;

import com.perspective.vpng.actor.collect_fts_data.CollectSearchDataActor;
import com.perspective.vpng.actor.collect_fts_data.wrapper.CollectSearchDataActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class CollectSearchDataPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    public CollectSearchDataPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {

        try {
            IBootstrapItem<String> item = new BootstrapItem("CollectSearchDataPlugin");

            item
//                .after("IOC")
//                .after("IFieldPlugin")
//                .before("starter")
                .process(() -> {
                    try {
                        IField fromFieldsFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "fromFieldsFN");
                        IField fromListFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "fromListFN");
                        IOC.register(Keys.getOrAdd("CollectSearchDataActor"),
                            new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject params = (IObject) args[0];
                                        return new CollectSearchDataActor(new CollectSearchDataActorConfig() {
                                            @Override
                                            public String getFromFieldsFieldName() throws ReadValueException {
                                                try {
                                                    return fromFieldsFieldNameF.in(params);
                                                } catch (InvalidArgumentException e) {
                                                    throw new ReadValueException(e);
                                                }
                                            }

                                            @Override
                                            public String getFromListFieldName() throws ReadValueException {
                                                try {
                                                    return fromListFieldNameF.in(params);
                                                } catch (InvalidArgumentException e) {
                                                    throw new ReadValueException(e);
                                                }
                                            }
                                        });
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }));
                    } catch (ResolutionException | InvalidArgumentException | RegistrationException ex) {
                        throw new ActionExecuteException("CollectSearchData plugin can't execute: " + ex.getMessage(), ex);
                    }
                });

            bootstrap.add(item);
        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't load CollectSearchData plugin", e);
        }
    }
}
