package com.perspective.vpng.plugin.db_fields_holder;

import com.perspective.vpng.database.field.*;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class DatabaseFieldsHolderPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    private IDBFieldsHolder fieldsHolder;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public DatabaseFieldsHolderPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("DatabaseFieldsHolderPlugin");

            item
//                    .after("IOC")
//                    .after("IFieldPlugin")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IKey actorKey = Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName());
                            fieldsHolder = new DBFieldsHolder(
                                    new ConditionField(),
                                    new SearchQueryField(),
                                    new CountQueryField()
                            );
                            IOC.register(actorKey,
                                    new ApplyFunctionToArgumentsStrategy(
                                            (args) -> {
                                                try {
                                                    return fieldsHolder;
                                                } catch (Exception ex) {
                                                    throw new RuntimeException(
                                                            "Can't create db fields holder component: " + ex.getMessage(), ex);
                                                }
                                            }
                                    )
                            );
                        } catch (ResolutionException | InvalidArgumentException | RegistrationException ex) {
                            throw new ActionExecuteException("Database field plugin can't load: " + ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }

}
