package com.perspective.vpng.database.field;

import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class SearchQueryField implements ISearchQueryField {

    private final IField filter;
    private final IField page;
    private final IField sort;
    private final IField pageNumber;
    private final IField pageSize;

    public SearchQueryField() throws ResolutionException {
        this.filter = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "filter");
        this.page = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "page");
        this.sort = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sort");
        this.pageNumber = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "number");
        this.pageSize = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "size");
    }

    @Override
    public IField filter() {
        return filter;
    }

    @Override
    public IField page() {
        return page;
    }

    @Override
    public IField sort() {
        return sort;
    }

    @Override
    public IField pageSize() {
        return pageSize;
    }

    @Override
    public IField pageNumber() {
        return pageNumber;
    }
}
