package com.perspective.vpng.database.field;

public class DBFieldsHolder implements IDBFieldsHolder {

    private IConditionField conditions;
    private ISearchQueryField searchQuery;
    private ICountQueryField countQuery;

    public DBFieldsHolder(
            final IConditionField conditions,
            final ISearchQueryField searchQuery,
            final ICountQueryField countQuery
    ) {
        this.conditions = conditions;
        this.searchQuery = searchQuery;
        this.countQuery = countQuery;
    }

    @Override
    public IConditionField conditions() {
        return conditions;
    }

    @Override
    public ISearchQueryField searchQuery() {
        return searchQuery;
    }

    @Override
    public ICountQueryField countQuery() {
        return countQuery;
    }
}
