package com.perspective.vpng.database.field;

import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class CountQueryField implements ICountQueryField {

    private IField filter;

    public CountQueryField() throws ResolutionException {
        this.filter = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "filter");
    }

    @Override
    public IField filter() {
        return filter;
    }
}
