package com.perspective.vpng.database.field;

import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class ConditionField implements IConditionField {

    private final IField eq;
    private final IField neq;
    private final IField lt;
    private final IField gt;
    private final IField lte;
    private final IField gte;
    private final IField isNull;
    private final IField dateFrom;
    private final IField dateTo;
    private final IField in;
    private final IField hasTag;

    public ConditionField() throws ResolutionException {
        eq = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$eq");
        neq = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$neq");
        lt = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$lt");
        gt = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$gt");
        lte = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$lte");
        gte = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$gte");
        isNull = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$isNull");
        dateFrom = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$date-from");
        dateTo = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$date-to");
        in = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$in");
        hasTag = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$hasTag");
    }

    @Override
    public IField eq() {
        return eq;
    }

    @Override
    public IField neq() {
        return neq;
    }

    @Override
    public IField lt() {
        return lt;
    }

    @Override
    public IField gt() {
        return gt;
    }

    @Override
    public IField lte() {
        return lte;
    }

    @Override
    public IField gte() {
        return gte;
    }

    @Override
    public IField isNull() {
        return isNull;
    }

    @Override
    public IField dateFrom() {
        return dateFrom;
    }

    @Override
    public IField dateTo() {
        return dateTo;
    }

    @Override
    public IField in() {
        return in;
    }

    @Override
    public IField hasTag() {
        return hasTag;
    }
}
