package com.perspective.vpng.database.field;

import info.smart_tools.smartactors.iobject.ifield.IField;

/**
 * Holds fields for search query in db.
 */
public interface ISearchQueryField {

    IField filter();
    IField page();
    IField sort();
    IField pageSize();
    IField pageNumber();

}
