package com.perspective.vpng.database.field;

import info.smart_tools.smartactors.iobject.ifield.IField;

/**
 * Holds fields for count query in db.
 */
public interface ICountQueryField {

    IField filter();

}
