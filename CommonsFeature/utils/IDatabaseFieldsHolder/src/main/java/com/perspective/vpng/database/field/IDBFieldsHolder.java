package com.perspective.vpng.database.field;

/**
 * Holds fields for some complex database query and for all conditions.
 */
public interface IDBFieldsHolder {

    IConditionField conditions();
    ISearchQueryField searchQuery();
    ICountQueryField countQuery();

}
