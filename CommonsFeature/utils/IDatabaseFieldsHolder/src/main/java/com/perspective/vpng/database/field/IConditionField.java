package com.perspective.vpng.database.field;

import info.smart_tools.smartactors.iobject.ifield.IField;

/**
 * Holds fields of conditions for db query.
 */
public interface IConditionField {

    IField eq();
    IField neq();
    IField lt();
    IField gt();
    IField lte();
    IField gte();
    IField isNull();
    IField dateFrom();
    IField dateTo();
    IField in();
    IField hasTag();

}
