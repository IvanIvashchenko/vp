package com.perspective.vpng.util.sequence_holder;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SequenceHolder {
    private String SEQUENCE_COLLECTION = "sequence";
    private String entityName;
    private IPool connectionPool;
    private IField countF;
    private IField nameF;
    private Integer initialValue;

    public SequenceHolder(final String entityName, final Integer initialValue) {
        try {
            this.entityName = entityName;
            Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
            this.countF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "count");
            this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
            this.initialValue = initialValue;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Integer getNewId() {
        try {
            IObject sequenceObject = getCountFromDB();
            countF.out(sequenceObject, ((Integer) countF.in(sequenceObject)) +1);
            updateCountInDB(sequenceObject);
            return countF.in(sequenceObject);
        } catch (ReadValueException | ChangeValueException | InvalidArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    private void updateCountInDB(IObject obj) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            ITask upsertTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.upsert"),
                    poolGuard.getObject(),
                    SEQUENCE_COLLECTION,
                    obj
            );
            upsertTask.execute();
        } catch (PoolGuardException | ResolutionException | TaskExecutionException e) {
            throw new RuntimeException("Can't execute database operation.", e);
        }
    }

    private IObject getCountFromDB() {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                    Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"filter\": {\"name\": {\"$eq\": \"" + entityName + "\"}}, \"collectionName\": \"" + SEQUENCE_COLLECTION + "\"}"
            );

            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    SEQUENCE_COLLECTION,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            searchResult.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();

            if (!searchResult.isEmpty()) {
                return searchResult.get(0);
            }

            IObject newSequence = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            nameF.out(newSequence, entityName);
            countF.out(newSequence, initialValue);
            return newSequence;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
