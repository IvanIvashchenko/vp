package com.perspective.vpng.util.schema_rule_parser;

import info.smart_tools.smartactors.morph_expressions.interfaces.parser.IProperty;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.exception.ExecutionException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

class PropertiesPackage {

    private static final Pattern RU_LANGUAGE = Pattern.compile("^[А-яЁё.\\- ]{1,255}$");
    private static final Pattern ADDRESS = Pattern.compile("^[0-9А-яЁё\\s\\.,\\-;\\/\\(\\)\"]{1,255}$");
    private static final Pattern HOUSE = Pattern.compile("^[0-9А-яA-zЁё/]{1,7}$");
    private static final Pattern EMAIL = Pattern.compile("^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\" +
            ".,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\." +
            "[0-9]{1,3}\\.[0-9]{1,3}])|(([A-z\\-0-9]+\\.)+[A-z]{2,}))$");
    private static final Pattern PASSWORD = Pattern.compile("^(?=.*[0-9])(?=.*[!@#$%^&*])[A-z0-9!@#$%^&*]{6,16}$");
    private static final Pattern DIGITS = Pattern.compile("^\\d+$");
    private static final Pattern BARCODE = Pattern.compile("^[0-9]{13}$");
    private static final Pattern ARTICLE = Pattern.compile("^[А-яЁё0-9.,\\/]{7,9}$");
    private static final Pattern PHONE = Pattern.compile("^\\+7\\s9[0-9]{9}$");
    private static final Pattern HONE_PHONE = Pattern.compile("^\\+7\\s[0-9]{10}$");
    private static final Pattern NUMBER = Pattern.compile("^[0-9]+(([.,])[0-9]{1,2})?$");

    private final Map<String, IProperty> DEFAULT_PROPERTIES = new HashMap<String, IProperty>() {{
        put("обязательное", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            if (value instanceof String) {
                return !((String) value).isEmpty();
            }
            if (value instanceof List) {
                return !((List) value).isEmpty();
            }
            return value != null;
        });
        put("длина", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            if (value instanceof List) {
                return ((List) value).size();
            }
            if (value instanceof Map) {
                return ((Map) value).size();
            }
            return String.valueOf(value).length();
        });
        put("значение", scope -> seekProperty(scope, (String) scope.get("fieldName")));
        put("русский", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            if (value instanceof List) {
                List values = (List) value;
                if (values.isEmpty()) {
                    return false;
                }
                for (Object val : values) {
                    if (!(val instanceof String && RU_LANGUAGE.matcher((String) val).matches())) {
                        return false;
                    }
                }
                return true;
            }
            return value instanceof String && RU_LANGUAGE.matcher((String) value).matches();
        });
        put("адрес", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return value instanceof String && ADDRESS.matcher(((String) value)).matches() ? value : false;
        });
        put("дом", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return value != null && HOUSE.matcher(String.valueOf(value)).matches() ? value : false;
        });
        put("email", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return value instanceof String && EMAIL.matcher((String) value).matches() ? value : false;
        });
        put("password", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return value instanceof String && PASSWORD.matcher((String) value).matches() ? value : false;
        });
        put("изЦифр", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            if (value instanceof List) {
                List values = (List) value;
                if (values.isEmpty()) {
                    return false;
                }
                for (Object val : values) {
                    if (!(val instanceof Number ||
                            (val instanceof String && DIGITS.matcher((String) val).matches()))) {
                        return false;
                    }
                }
                return true;
            }
            return value instanceof Number ||
                    (value instanceof String && DIGITS.matcher((String) value).matches());
        });
        put("штрихкод", scope -> {
            Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return BARCODE.matcher(String.valueOf(value)).matches();
        });
        put("артикул", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return ARTICLE.matcher(String.valueOf(value)).matches();
        });
        put("телефон", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return PHONE.matcher(String.valueOf(value)).matches();
        });
        put("домашнийТелефон", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            return HONE_PHONE.matcher(String.valueOf(value)).matches();
        });
        put("число", scope -> {
            final Object value = seekProperty(scope, (String) scope.get("fieldName"));
            if (value instanceof List) {
                List values = (List) value;
                if (values.isEmpty()) {
                    return false;
                }
                for (Object val : values) {
                    if (!(val instanceof Number ||
                            (val instanceof String && NUMBER.matcher((String) val).matches()))) {
                        return false;
                    }
                }
                return true;
            }
            return value instanceof Number ||
                    (value instanceof String && NUMBER.matcher((String) value).matches());
        });
    }};

    Map<String, IProperty> getAll() {
        return DEFAULT_PROPERTIES;
    }

    private Object seekProperty(final Map<String, Object> scope, String path) throws ExecutionException {
        try {
            String[] parts = path.split("\\.");
            Map<String, Object> currentScope = scope;
            for (String part : parts) {
                Object value = currentScope.get(part);
                if (value != null) {
                    if (value instanceof Map) {
                        currentScope = (Map) value;
                        continue;
                    }
                    return value;
                }
            }
            return null;
        } catch (ClassCastException ex) {
            throw new ExecutionException("Scope and nested scopes must be a Map<String, Object> type!");
        } catch (NullPointerException ex) {
            throw new ExecutionException("Scope is undefined!");
        }
    }

}
