package com.perspective.vpng.util.schema_rule_parser;

import com.perspective.vpng.util.rule_parser.IRule;
import com.perspective.vpng.util.rule_parser.IRuleFunction;
import com.perspective.vpng.util.rule_parser.IRuleParser;
import com.perspective.vpng.util.rule_parser.IRuleProperty;
import com.perspective.vpng.util.rule_parser.exception.RuleParsingException;
import com.perspective.vpng.util.rule_parser.exception.ValidationByRuleException;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.IEvaluator;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.IParser;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.exception.EvaluatingExpressionException;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.exception.ParsingException;
import info.smart_tools.smartactors.morph_expressions.parser.ExpressionParser;
import info.smart_tools.smartactors.morph_expressions.parser.TypeConverter;

import java.util.Map;

public class SchemaRuleParser implements IRuleParser {

    private final IParser parser;
    
    private SchemaRuleParser() {
        PropertiesPackage properties = new PropertiesPackage();
        TableOperators tableOperators = new TableOperators();
        this.parser = ExpressionParser.create(tableOperators.getAll());
        this.parser.registerProperties(properties.getAll());
    }

    public static SchemaRuleParser create() {
        return new SchemaRuleParser();
    }

    @Override
    public IRule parse(String rule) throws RuleParsingException {
        try {
            final IEvaluator evaluator = parser.parse(rule);
            return new IRule() {

                private String error;

                @Override
                public boolean validate(Map<String, Object> scope) throws ValidationByRuleException {
                    try {
                        return TypeConverter.convertToBool(evaluator.eval(scope));
                    } catch (EvaluatingExpressionException ex) {
                        throw new ValidationByRuleException(ex.getMessage(), ex);
                    }
                }

                @Override
                public String getError() {
                    return this.error;
                }

                @Override
                public void setError(String message) {
                    this.error = message;
                }
            };
        } catch (ParsingException ex) {
            throw new RuleParsingException(ex.getMessage(), ex);
        }
    }

    @Override
    public void registerProperty(String lexeme, IRuleProperty property) {
        parser.registerProperty(lexeme, property);
    }

    @Override
    public void registerProperties(Map<String, IRuleProperty> properties) {
        for (Map.Entry<String, IRuleProperty> property : properties.entrySet()) {
            parser.registerProperty(property.getKey(), property.getValue());
        }
    }

    @Override
    public void registerFunction(String lexeme, IRuleFunction function) {
        parser.registerFunction(lexeme, function);
    }

    @Override
    public void registerFunctions(Map<String, IRuleFunction> functions) {
        for (Map.Entry<String, IRuleFunction> function : functions.entrySet()) {
            parser.registerFunction(function.getKey(), function.getValue());
        }
    }

    @Override
    public void unregisterProperty(String lexeme) {
        parser.unregisterProperty(lexeme);
    }

    @Override
    public void unregisterFunction(String lexeme) {
        parser.unregisterFunction(lexeme);
    }

}
