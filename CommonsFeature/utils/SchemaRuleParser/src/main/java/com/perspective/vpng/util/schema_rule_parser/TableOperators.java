package com.perspective.vpng.util.schema_rule_parser;

import info.smart_tools.smartactors.morph_expressions.interfaces.parser.IFunction;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.exception.ExecutionException;
import info.smart_tools.smartactors.morph_expressions.parser.Operators;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class TableOperators {

    private final Map<String, IFunction> sourceOperators = new Operators().getAll();

    private Object extendedComparison(
            final Object left,
            final Object right,
            final IFunction comparator
    ) throws ExecutionException {
        if (left instanceof List && !(right instanceof List)) {
            return compare((List) left, right, comparator);
        }
        if (!(left instanceof List) && right instanceof List) {
            return compare((List) right, left, comparator);
        }
        if (left instanceof List && right instanceof List) {
            final List l1 = (List) left;
            final List l2 = (List) right;
            final int size = l1.size();
            try {
                for (int i = 0; i < size; i++) {
                    if (!(Boolean) comparator.apply(l1.get(i), l2.get(i))) {
                        return Boolean.FALSE;
                    }
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                throw new ExecutionException("Arrays must have the same size!", ex);
            }
            return Boolean.TRUE;
        }
        return comparator.apply(left, right);
    }

    private Boolean compare(
            final List list,
            final Object val,
            final IFunction comparator
    ) throws ExecutionException {
        final int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!(Boolean) comparator.apply(list.get(i), val)) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }

    private final Map<String, IFunction> operators = new HashMap<String, IFunction>() {{
        put(">", args -> extendedComparison(args[0], args[1], sourceOperators.get(">")));
        put(">=", args -> extendedComparison(args[0], args[1], sourceOperators.get(">=")));
        put("<", args -> extendedComparison(args[0], args[1], sourceOperators.get("<")));
        put("<=", args -> extendedComparison(args[0], args[1], sourceOperators.get("<=")));
    }};

    Map<String, IFunction> getAll() {
        return operators;
    }

}
