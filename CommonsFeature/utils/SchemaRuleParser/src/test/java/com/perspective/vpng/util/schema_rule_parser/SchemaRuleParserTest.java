package com.perspective.vpng.util.schema_rule_parser;

import com.perspective.vpng.util.rule_parser.IRuleFunction;
import com.perspective.vpng.util.rule_parser.IRuleParser;
import com.perspective.vpng.util.rule_parser.IRuleProperty;
import com.perspective.vpng.util.rule_parser.exception.RuleParsingException;
import com.perspective.vpng.util.rule_parser.exception.ValidationByRuleException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SchemaRuleParserTest {

    private static IRuleParser parser = SchemaRuleParser.create();
    private static Map<String, Object> scope = new HashMap<String, Object>() {{
        put("x", 5);
        put("y", 7);
        put("table", new HashMap<String, Object>() {{
            put("fCol", Arrays.asList(3, 4, 2));
            put("sCol", Arrays.asList(5, 8, 9));
            put("thCol", Arrays.asList(6, 9, 5));
        }});
        put("minSum", 20);
        put("maxSum", 25);
    }};

    @BeforeClass
    public static void setUp() {
        parser.registerProperties(new HashMap<String, IRuleProperty>() {{
            put("required", scope -> scope.get("x") != null);
            put("today", scope -> new Date());
            put("checkMinSum", scope -> {
                Map<String, List<Integer>> table = (Map) scope.get("table");
                List<Integer> column = table.get("fCol");
                int sumFCol = column.get(0) + column.get(1) + column.get(2);
                column = table.get("sCol");
                int sumSCol = column.get(0) + column.get(1) + column.get(2);
                column = table.get("thCol");
                int sumThCol = column.get(0) + column.get(1) + column.get(2);

                int minSum = (int) scope.get("minSum");
                return sumFCol > minSum && sumSCol >= minSum && sumThCol == minSum;
            });
        }});
        parser.registerFunctions(new HashMap<String, IRuleFunction>() {{
            put("sqr", args -> (int) args[0] * (int) args[0]);
            put("constant", args -> "constant");
            put("sum", args -> (int) args[0] + (int) args[1]);
            put("resultSum", args -> {
                List<Integer> column = (List) args[0];
                int sum = 0;
                for (Integer item : column) {
                    sum += item;
                }
                return sum;
            });
        }});
    }

    @Test
    public void shouldParseAndSuccessValidateScope() throws RuleParsingException, ValidationByRuleException {
        assertTrue(parser.parse("(x + y / 2 > y) && (x + y % 6 < y)").validate(scope));
        assertTrue(parser.parse("table.fCol < table.sCol && table.thCol > y - x").validate(scope));
        assertFalse(parser.parse("table.fCol >= x + y").validate(scope));
        assertTrue(parser.parse("table.sCol >= x").validate(scope));
    }

    @Test
    public void shouldParseExpressionWithComputedPropertiesAndSuccessValidateScope()
            throws RuleParsingException, ValidationByRuleException {

        assertFalse(parser.parse("required").validate(Collections.emptyMap()));
        assertTrue(parser.parse("today").validate(Collections.emptyMap()));
        assertFalse(parser.parse("checkMinSum").validate(scope));
    }

    @Test(expected = ValidationByRuleException.class)
    public void should_ThrowException_Reason_Variable_Is_Not_Defined()
            throws RuleParsingException, ValidationByRuleException {
        parser.parse(("z")).validate(scope);
    }

    @Test
    public void shouldParseExpressionWithFunctionsAndSuccessValidateScope()
            throws ValidationByRuleException, RuleParsingException {

        assertTrue(parser.parse("constant() == 'constant'").validate(scope));
        assertFalse(parser.parse("sqr(x) < sum(x, y)").validate(scope));
        assertTrue(parser.parse("sqr(sqr(x)) == 625").validate(scope));
        assertTrue(parser.parse("(resultSum(table.fCol) < minSum) && " +
                "(resultSum(table.sCol) >= minSum && resultSum(table.sCol) <= maxSum)").validate(scope));
    }

    @Test(expected = ValidationByRuleException.class)
    public void should_ThrowException_Reason_Function_Is_Not_Defined()
            throws ValidationByRuleException, RuleParsingException {
        parser.parse("unregistered()").validate(Collections.emptyMap());
    }

}

