package com.perspective.vpng.util.schema_rule_parser;

import info.smart_tools.smartactors.morph_expressions.interfaces.parser.IProperty;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.exception.ExecutionException;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PropertiesPackageTest {

    private static Map<String, IProperty> properties;
    private static Map<String, Object> scope = new HashMap<String, Object>() {{
        put("address", "Новогодняя 32, кв 12");
        put("house", "25/2");
        put("email", "test@gmail.com");
        put("password", "awd2@35*32@");
        put("digits", "15214124");
        put("barcode", "0123456789012");
        put("article", "артикль/2");
        put("phone", "+7 9131429553");
        put("home_phone", "+7 212941325");
        put("Double", 4.2);
        put("primitive", new HashMap<String, Object>() {{
            put("int", 4);
            put("null", null);
            put("string", "строка");
            put("emptyString", "");
        }});
        put("iterable", new HashMap<String, Object>() {{
            put("list", Arrays.asList("стр.", "строка"));
            put("listNumbers", Arrays.asList(5, "22"));
            put("emptyList", Collections.emptyList());
        }});
    }};

    @BeforeClass
    public static void setUp() {
        properties = new PropertiesPackage().getAll();
    }

    @Test
    public void checkExistFieldPropertyTest() throws ExecutionException {
        IProperty property = properties.get("обязательное");

        scope.put("fieldName", "Double");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "primitive.int");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "iterable.list");
        Assert.assertEquals(true, property.apply(scope));

        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "iterable.emptyList");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void getLengthPropertyTest() throws ExecutionException {
        IProperty property = properties.get("длина");

        scope.put("fieldName", "Double");
        Assert.assertEquals(3, property.apply(scope));
        scope.put("fieldName", "primitive.int");
        Assert.assertEquals(1, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(4, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(6, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(0, property.apply(scope));
        scope.put("fieldName", "iterable.list");
        Assert.assertEquals(2, property.apply(scope));
        scope.put("fieldName", "iterable.emptyList");
        Assert.assertEquals(0, property.apply(scope));
    }

    @Test
    public void getValueByPathPropertyTest() throws ExecutionException {
        IProperty property = properties.get("значение");

        scope.put("fieldName", "primitive.int");
        Assert.assertEquals(4, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(null, property.apply(scope));
        scope.put("fieldName", "null");
        Assert.assertEquals(null, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals("строка", property.apply(scope));
        scope.put("fieldName", "iterable.list");
        Assert.assertTrue(Arrays.asList("стр.", "строка").equals(property.apply(scope)));
    }

    @Test
    public void checkRussianLanguagePropertyTest() throws ExecutionException {
        IProperty property = properties.get("русский");

        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "iterable.list");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "iterable.emptyList");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkAddressPropertyTest() throws ExecutionException {
        IProperty property = properties.get("адрес");

        scope.put("fieldName", "address");
        Assert.assertEquals("Новогодняя 32, кв 12", property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals("строка", property.apply(scope));

        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkHousePropertyTest() throws ExecutionException {
        IProperty property = properties.get("дом");

        scope.put("fieldName", "house");
        Assert.assertEquals("25/2", property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals("строка", property.apply(scope));

        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "address");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkEmailPropertyTest() throws ExecutionException {
        IProperty property = properties.get("email");

        scope.put("fieldName", "email");
        Assert.assertEquals("test@gmail.com", property.apply(scope));

        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkPasswordPropertyTest() throws ExecutionException {
        IProperty property = properties.get("password");

        scope.put("fieldName", "password");
        Assert.assertEquals("awd2@35*32@", property.apply(scope));

        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "email");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkDigitsPropertyTest() throws ExecutionException {
        IProperty property = properties.get("изЦифр");

        scope.put("fieldName", "digits");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "Double");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "iterable.listNumbers");
        Assert.assertEquals(true, property.apply(scope));

        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "email");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkBarcodePropertyTest() throws ExecutionException {
        IProperty property = properties.get("штрихкод");

        scope.put("fieldName", "barcode");
        Assert.assertEquals(true, property.apply(scope));

        scope.put("fieldName", "email");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "article");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkArticlePropertyTest() throws ExecutionException {
        IProperty property = properties.get("артикул");

        scope.put("fieldName", "article");
        Assert.assertEquals(true, property.apply(scope));

        scope.put("fieldName", "barcode");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkPhonePropertyTest() throws ExecutionException {
        IProperty property = properties.get("телефон");

        scope.put("fieldName", "phone");
        Assert.assertEquals(true, property.apply(scope));

        scope.put("fieldName", "home_phone");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "barcode");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "digits");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkHomePhonePropertyTest() throws ExecutionException {
        IProperty property = properties.get("телефон");

        scope.put("fieldName", "phone");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "barcode");
        Assert.assertEquals(false, property.apply(scope));

        scope.put("fieldName", "Double");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "digits");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

    @Test
    public void checkNumberPropertyTest() throws ExecutionException {
        IProperty property = properties.get("число");

        scope.put("fieldName", "primitive.int");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "Double");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "digits");
        Assert.assertEquals(true, property.apply(scope));
        scope.put("fieldName", "iterable.listNumbers");
        Assert.assertEquals(true, property.apply(scope));

        scope.put("fieldName", "primitive.string");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.emptyString");
        Assert.assertEquals(false, property.apply(scope));
        scope.put("fieldName", "primitive.null");
        Assert.assertEquals(false, property.apply(scope));
    }

}
