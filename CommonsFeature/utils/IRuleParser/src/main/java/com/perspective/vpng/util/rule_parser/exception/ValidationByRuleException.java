package com.perspective.vpng.util.rule_parser.exception;

public class ValidationByRuleException extends Exception {

    public ValidationByRuleException(String message) {
        super(message);
    }

    public ValidationByRuleException(String message, Throwable cause) {
        super(message, cause);
    }

}
