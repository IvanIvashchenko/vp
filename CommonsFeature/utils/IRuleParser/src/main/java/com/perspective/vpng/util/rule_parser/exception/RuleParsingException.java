package com.perspective.vpng.util.rule_parser.exception;

public class RuleParsingException extends Exception {

    public RuleParsingException(String message) {
        super(message);
    }

    public RuleParsingException(String message, Throwable cause) {
        super(message, cause);
    }

}
