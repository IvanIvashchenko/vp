package com.perspective.vpng.util.rule_parser;

import com.perspective.vpng.util.rule_parser.exception.RuleParsingException;

import java.util.Map;

public interface IRuleParser {

    IRule parse(String rule) throws RuleParsingException;

    void registerProperty(String lexeme, IRuleProperty property);

    void registerProperties(Map<String, IRuleProperty> properties);

    void registerFunction(String lexeme, IRuleFunction function);

    void registerFunctions(Map<String, IRuleFunction> functions);

    void unregisterProperty(String lexeme);

    void unregisterFunction(String lexeme);

}
