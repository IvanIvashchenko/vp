package com.perspective.vpng.util.rule_parser;

import com.perspective.vpng.util.rule_parser.exception.ValidationByRuleException;

import java.util.Map;

public interface IRule {

    boolean validate(Map<String, Object> scope) throws ValidationByRuleException;

    String getError();

    void setError(String message);

}
