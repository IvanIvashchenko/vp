package com.perspective.vpng.actor.provider.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ITakeSchemeMessage {

    String getProviderId() throws ReadValueException;

    String getSelectionStrategy() throws ReadValueException;

    List<String> getModificationStrategies() throws ReadValueException;

    void setSchema(List<IObject> scheme) throws ChangeValueException;

    void setProviderId(String providerId) throws ChangeValueException;

}
