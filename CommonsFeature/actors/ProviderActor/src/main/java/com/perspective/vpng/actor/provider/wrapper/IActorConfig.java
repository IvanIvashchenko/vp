package com.perspective.vpng.actor.provider.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 *
 */
public interface IActorConfig {
    /**
     *
     * @return
     * @throws ReadValueException
     */
    String getCollectionName() throws ReadValueException;

    String getCacheKey() throws ReadValueException;
}
