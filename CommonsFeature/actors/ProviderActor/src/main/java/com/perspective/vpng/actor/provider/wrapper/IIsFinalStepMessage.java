package com.perspective.vpng.actor.provider.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface IIsFinalStepMessage {
    String getProviderId() throws ReadValueException;
    void setIsFinalStep(Boolean flag) throws ChangeValueException;
}
