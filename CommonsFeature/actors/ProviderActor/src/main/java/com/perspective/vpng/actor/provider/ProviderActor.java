package com.perspective.vpng.actor.provider;

import com.perspective.vpng.actor.provider.exception.ProviderActorException;
import com.perspective.vpng.actor.provider.exception.ProviderNotFoundException;
import com.perspective.vpng.actor.provider.wrapper.*;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.SerializeException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ProviderActor {

    private final ICachedCollection collection;
    private final IKey selectionSchemaStrategyKey;
    private final IKey modificationSchemaStrategyKey;

    private final IField schemesF;
    private final IField firstStepF;

    /**
     *
     * @param config
     * @throws ProviderActorException
     */
    public ProviderActor(final IActorConfig config) throws ProviderActorException {
        try {
            this.collection = IOC.resolve(
                    Keys.getOrAdd(ICachedCollection.class.getCanonicalName()),
                    config.getCollectionName(),
                    config.getCacheKey()
            );
            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            this.schemesF = IOC.resolve(fieldKey, "schemas");
            this.firstStepF = IOC.resolve(fieldKey, "firstStep");
            this.selectionSchemaStrategyKey = Keys.getOrAdd("ChooseSchemaStrategy");
            this.modificationSchemaStrategyKey = Keys.getOrAdd("ModificationSchemaStrategy");
        } catch (ResolutionException | ReadValueException ex) {
            throw new ProviderActorException("Couldn't created a new instance of provider actor: " + ex.getMessage(), ex);
        }
    }

    public void takeProvider(final ITakeProviderMessage message)
            throws ProviderNotFoundException, ProviderActorException {
        try {
            message.setProvider(takeProviderById(message.getProviderId()));
        } catch (ReadValueException | ChangeValueException ex) {
            throw new ProviderActorException(ex);
        }
    }

    public void takeProviders(ITakeProvidersMessage message)
            throws ProviderNotFoundException, ProviderActorException {
        try {
            List<String> ids = message.getProviderIds();
            List<IObject> providers = new ArrayList<>(ids.size());
            for (String id: ids) {
                providers.add(takeProviderById(id));
            }
            message.setProviders(providers);
        } catch (ReadValueException | ChangeValueException ex) {
            throw new ProviderActorException(ex);
        }
    }

    public void takeSchema(final ITakeSchemeMessage message)
            throws ProviderActorException, ProviderNotFoundException {
        try {
            IObject provider = takeProviderById(message.getProviderId());
            List<IObject> schema = IOC.resolve(
                    selectionSchemaStrategyKey,
                    message.getSelectionStrategy(),
                    schemesF.in(provider)
            );
            schema = cloneSchema(schema);
            message.setSchema(modifySchema(schema, provider, message.getModificationStrategies()));
            message.setProviderId(message.getProviderId());
        } catch (Exception ex) {
            throw new ProviderActorException(ex);
        }
    }

    public void checkIsFinalStep(IIsFinalStepMessage message) throws ProviderActorException {
        try {
            IObject provider = takeProviderById(message.getProviderId());
            List<String> firstStep = firstStepF.in(provider);

            if (firstStep != null) {
                message.setIsFinalStep(false);
            } else {
                message.setIsFinalStep(true);
            }
        } catch (Exception ex) {
            throw new ProviderActorException(ex);
        }
    }

    private IObject takeProviderById(final String providerId)
            throws ProviderNotFoundException, ProviderActorException {
        try {
            List<IObject> providers = collection.getItems(providerId);
            if (providers.size() > 1) {
                throw new ProviderActorException("Providers with specified id was too much!");
            }
            return providers.get(0);
        } catch (GetCacheItemException ex) {
            throw new ProviderActorException(ex.getMessage(), ex);
        } catch (IndexOutOfBoundsException ex) {
            throw new ProviderNotFoundException("Provider with the specified id was not found!", ex);
        } catch (NullPointerException ex) {
            throw new ProviderActorException("Provider's id was null!", ex);
        }
    }

    private List<IObject> cloneSchema(final List<IObject> schema) throws SerializeException, ResolutionException {
        List<IObject> newSchema = new ArrayList<>();
        for (IObject schemaEl : schema) {
            String elString = schemaEl.serialize();
            newSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), elString));
        }
        return newSchema;
    }

    private List<IObject> modifySchema(
            final List<IObject> schema,
            final IObject provider,
            final List<String> modificationStrategies
    ) throws SerializeException, ResolutionException {
        List<IObject> newSchema = schema;
        for (String strategy : modificationStrategies) {
            newSchema = IOC.resolve(modificationSchemaStrategyKey, strategy, newSchema, provider);
        }
        return newSchema;
    }

}
