package com.perspective.vpng.actor.provider.exception;

public class ProviderActorException extends Exception {

    public ProviderActorException(String message) {
        super(message);
    }

    public ProviderActorException(Throwable cause) {
        super(cause);
    }

    public ProviderActorException(String message, Throwable cause) {
        super(message, cause);
    }

}
