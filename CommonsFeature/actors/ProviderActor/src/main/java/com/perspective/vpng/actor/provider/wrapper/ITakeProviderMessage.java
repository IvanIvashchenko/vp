package com.perspective.vpng.actor.provider.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ITakeProviderMessage {

    String getProviderId() throws ReadValueException;

    void setProvider(IObject provider) throws ChangeValueException;

}
