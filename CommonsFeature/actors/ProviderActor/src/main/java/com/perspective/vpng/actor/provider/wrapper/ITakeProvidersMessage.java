package com.perspective.vpng.actor.provider.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ITakeProvidersMessage {

    List<String> getProviderIds() throws ReadValueException;

    void setProviders(List<IObject> providers) throws ChangeValueException;

}
