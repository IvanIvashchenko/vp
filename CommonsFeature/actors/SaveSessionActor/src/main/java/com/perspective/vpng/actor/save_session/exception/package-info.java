/**
 * Exception for {@link com.perspective.vpng.actor.save_session.SaveSessionActor}
 */
package com.perspective.vpng.actor.save_session.exception;