package com.perspective.vpng.actor.save_session.wrapper;

import com.perspective.vpng.actor.save_session.SaveSessionActor;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Message wrapper for {@link SaveSessionActor}
 */
public interface SaveSessionMessage {
    /**
     * getter
     * @return sessionId
     * @throws ReadValueException sometimes
     */
    IObject getSession() throws ReadValueException;


}
