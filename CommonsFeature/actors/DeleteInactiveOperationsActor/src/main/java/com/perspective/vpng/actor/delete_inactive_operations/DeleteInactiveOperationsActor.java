package com.perspective.vpng.actor.delete_inactive_operations;

import com.perspective.vpng.actor.delete_inactive_operations.exception.DeleteInactiveOpsException;
import com.perspective.vpng.actor.delete_inactive_operations.wrapper.DeleteOperationsMessage;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.database.interfaces.istorage_connection.IStorageConnection;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.JDBCCompiledQuery;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.QueryStatement;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.io.Writer;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DeleteInactiveOperationsActor {
    private Integer LIMIT = 100;
    private IPool connectionPool;
    private String collectionName;
    private DateTimeFormatter formatter;

    public DeleteInactiveOperationsActor(IObject params) {
        try {
            IField collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            collectionName = collectionNameF.in(params);
            ConnectionOptions connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
            this.formatter = IOC.resolve(Keys.getOrAdd("datetime_formatter"));
        } catch (Exception e) {
            throw new RuntimeException("Failed to create DeleteInactiveOperationsActor", e);
        }
    }

    public void deleteOperations(DeleteOperationsMessage message) throws DeleteInactiveOpsException {
        try {
            Integer count = LIMIT;
            QueryStatement preparedQuery = prepareQuery();

            while (count.equals(LIMIT)) {
                try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
                    IStorageConnection connection = (IStorageConnection) poolGuard.getObject();


                    JDBCCompiledQuery compiledQuery = (JDBCCompiledQuery) connection.compileQuery(preparedQuery);
                    PreparedStatement statement = compiledQuery.getPreparedStatement();
                    count = statement.executeUpdate();
                    connection.commit();

                } catch (PoolGuardException e) {
                    throw new RuntimeException("Cannot get connection from pool.", e);
                } catch (Exception e) {
                    throw new RuntimeException("Error during delete operations", e);
                }
            }
        } catch (Exception e) {
            throw new DeleteInactiveOpsException("Failed to delete inactive ops", e);
        }
    }

    private QueryStatement prepareQuery() throws Exception {
        QueryStatement preparedQuery = new QueryStatement();
        Writer body = preparedQuery.getBodyWriter();

        body.write("DELETE FROM ");
        body.write(collectionName);
        body.write(" WHERE ctid IN (SELECT ctid");
        body.write(" FROM ");
        body.write(collectionName);
        body.write(" WHERE (parse_timestamp_immutable(document -> 'expiredTime') <= ('" + LocalDateTime.now().format(formatter) + "')::timestamp)");
        body.write(" LIMIT ");
        body.write(LIMIT.toString());
        body.write(")");
        return preparedQuery;
    }
}
