package com.perspective.vpng.actor.delete_inactive_operations.exception;

public class DeleteInactiveOpsException extends Exception {
    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public DeleteInactiveOpsException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public DeleteInactiveOpsException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public DeleteInactiveOpsException(final Throwable cause) {
        super(cause);
    }
}
