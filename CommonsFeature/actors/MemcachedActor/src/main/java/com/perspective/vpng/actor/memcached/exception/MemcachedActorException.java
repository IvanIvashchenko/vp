package com.perspective.vpng.actor.memcached.exception;

/**
 * Exception for error in {@link com.perspective.vpng.actor.memcached.MemcachedActor} methods
 */
public class MemcachedActorException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public MemcachedActorException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public MemcachedActorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public MemcachedActorException(final Throwable cause) {
        super(cause);
    }
}