package com.perspective.vpng.actor.memcached.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ActorParams {
    String getServer() throws ReadValueException;
    String getName() throws ReadValueException;
    Boolean getFailover() throws ReadValueException;
    Integer getInitConn() throws ReadValueException;
    Integer getMinConn() throws ReadValueException;
    Integer getMaxConn() throws ReadValueException;
    Integer getMaintSleep() throws ReadValueException;
    Boolean getNagle() throws ReadValueException;
    Integer getSocketTO() throws ReadValueException;
    Boolean getAliveCheck() throws ReadValueException;
}
 /*pool.setServers(servers);
         pool.setFailover(true);
         pool.setInitConn(10);
         pool.setMinConn(5);
         pool.setMaxConn(250);
         pool.setMaintSleep(30);
         pool.setNagle(false);
         pool.setSocketTO(3000);
         pool.setAliveCheck(true);
         pool.initialize();*/
