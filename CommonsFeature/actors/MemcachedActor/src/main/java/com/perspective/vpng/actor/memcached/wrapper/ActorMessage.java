package com.perspective.vpng.actor.memcached.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ActorMessage {
    String getKey() throws ReadValueException;
    String getValue() throws ReadValueException;
    void setValue(String val) throws ChangeValueException;
}
