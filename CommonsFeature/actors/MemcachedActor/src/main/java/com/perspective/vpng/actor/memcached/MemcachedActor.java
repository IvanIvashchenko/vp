package com.perspective.vpng.actor.memcached;

import com.perspective.vpng.actor.memcached.exception.MemcachedActorException;
import com.perspective.vpng.actor.memcached.wrapper.ActorMessage;
import com.perspective.vpng.actor.memcached.wrapper.ActorParams;
import com.whalin.MemCached.MemCachedClient;
import com.whalin.MemCached.SockIOPool;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public class MemcachedActor {
    private MemCachedClient mcc;

    public MemcachedActor(ActorParams params) throws MemcachedActorException {
        try {
            SockIOPool pool = SockIOPool.getInstance(params.getName());
            pool.setServers(new String[]{params.getServer()});
            pool.setFailover(params.getFailover());
            pool.setInitConn(params.getInitConn());
            pool.setMinConn(params.getMinConn());
            pool.setMaxConn(params.getMaxConn());
            pool.setMaintSleep(params.getMaintSleep());
            pool.setNagle(params.getNagle());
            pool.setSocketTO(params.getSocketTO());
            pool.setAliveCheck(params.getAliveCheck());
            pool.initialize();

            this.mcc = new MemCachedClient(params.getName());
        } catch (ReadValueException e) {
            throw new MemcachedActorException("Failed to create MecachedActor", e);
        }
    }

    public void setItem(ActorMessage message) throws MemcachedActorException {
        try {
            String key = message.getKey();
            String val = message.getValue();

            mcc.set(key, val);
        } catch (ReadValueException e) {
            throw new MemcachedActorException("Failed to save item into memcached", e);
        }
    }
}
