package com.perspective.vpng.actor.memcached;

import com.perspective.vpng.actor.memcached.exception.MemcachedActorException;
import com.perspective.vpng.actor.memcached.wrapper.ActorMessage;
import com.perspective.vpng.actor.memcached.wrapper.ActorParams;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link MemcachedActor}
 */
public class MemcachedActorTest {
    MemcachedActor actor;

    @Before
    public void setUp() throws Exception {
        ActorParams params = mock(ActorParams.class);
        when(params.getAliveCheck()).thenReturn(true);
        when(params.getSocketTO()).thenReturn(3000);
        when(params.getNagle()).thenReturn(false);
        when(params.getMaintSleep()).thenReturn(30);
        when(params.getMaxConn()).thenReturn(250);
        when(params.getMinConn()).thenReturn(5);
        when(params.getInitConn()).thenReturn(10);
        when(params.getFailover()).thenReturn(true);
        when(params.getName()).thenReturn("vpng");
        when(params.getServer()).thenReturn("localhost:11212");
        actor = new MemcachedActor(params);
    }

    @Test(expected = MemcachedActorException.class)
    public void Should_ThrowException_BecauseMessageKeyIsNull() throws MemcachedActorException, ReadValueException {
        ActorMessage message = mock(ActorMessage.class);
        when(message.getKey()).thenThrow(new ReadValueException("TestEx"));
        actor.setItem(message);

        verify(message).getKey();
    }

    @Test(expected = MemcachedActorException.class)
    public void Should_ThrowException_BecauseMessageValueIsNull() throws MemcachedActorException, ReadValueException {
        ActorMessage message = mock(ActorMessage.class);
        when(message.getValue()).thenThrow(new ReadValueException("TestEx"));
        actor.setItem(message);

        verify(message).getValue();
    }

    @Test
    public void Should_CorrectSetItemInMMC() throws ReadValueException, MemcachedActorException {
        ActorMessage message = mock(ActorMessage.class);
        when(message.getValue()).thenReturn("value");
        when(message.getKey()).thenReturn("key");
        actor.setItem(message);

        verify(message).getKey();
        verify(message).getValue();
    }

}