package com.perspective.vpng.actor.collect_fts_data.exception;

/**
 * Exception for errors into {@link com.perspective.vpng.actor.collect_fts_data.CollectSearchDataActor}
 */
public class CollectSearchDataException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public CollectSearchDataException(String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public CollectSearchDataException(String message, Throwable cause) {
        super(message, cause);
    }
}
