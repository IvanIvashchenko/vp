package com.perspective.vpng.actor.collect_fts_data.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface CollectSearchDataMessage {

    /**
     * @return target field into which all data for fts would be copied
     * @throws ReadValueException
     */
    String getFullTextField() throws ReadValueException;

    /**
     * @return array of field names, which values should be copied to FTS field
     * @throws ReadValueException
     */
    List<String> getSimpleCopyFields() throws ReadValueException;

    /**
     * @return array of objects.
     * Each object contains field name for list with sources objects and array of field names,
     * which values should be copied to FTS field, for example:
     * {
     *    "fromFields": ["короткое-наименование", "лицевой-счет"],
     *    "fromList": "invoices"
     * }
     * @throws ReadValueException
     */
    List<IObject> getForEachCopyFields() throws ReadValueException;

    /**
     * @return iobject with needed for fts fields
     * @throws ReadValueException
     */
    IObject getDataObject() throws ReadValueException;

    /**
     * @return list with target iobjects which need dad for fts
     * @throws ReadValueException
     */
    List<IObject> getTargetList() throws ReadValueException;

    /**
     * @param fullTextData string with values for FTS seaprated by space
     * @throws ChangeValueException
     */
    void setFullTextData(String fullTextData) throws ChangeValueException;
}
