package com.perspective.vpng.actor.collect_fts_data;

import com.perspective.vpng.actor.collect_fts_data.exception.CollectSearchDataException;
import com.perspective.vpng.actor.collect_fts_data.wrapper.CollectSearchDataActorConfig;
import com.perspective.vpng.actor.collect_fts_data.wrapper.CollectSearchDataMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

/**
 * Actor for copy data for FTS purposes
 * TODO:: use nested field
 */
public class CollectSearchDataActor {

    private static final String SEPARATOR = " ";

    private IField fromFields;
    private IField fromList;

    public CollectSearchDataActor(final CollectSearchDataActorConfig config) throws CollectSearchDataException {
        try {
            //"fromFields"
            fromFields = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getFromFieldsFieldName());
            //"fromList"
            fromList = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getFromListFieldName());
        } catch (ResolutionException | ReadValueException e) {
            throw new CollectSearchDataException("Error during create CollectSearchDataActor", e);
        }
    }

    /**
     * Collects data for full text search from sources fields and create FTS-field with data.
     * Copies values from simpleCopyFields from dataObject to one field fullTextData
     * @param message {
     *                "getDataObject": "message/invoice",
     *                "setFullTextData": "message/invoice/searchData",
     *                "getSimpleCopyFields": [\"короткое-наименование\", \"полное-наименование\", \"лицевой счёт\"]
     * }
     * @throws CollectSearchDataException
     */
    public void createFullTextSearchField(final CollectSearchDataMessage message) throws CollectSearchDataException {

        try {
            List<String> fieldNamesForCopy = message.getSimpleCopyFields();
            IObject dataObject = message.getDataObject();
            String fullTextString = addToTextSearch(fieldNamesForCopy, dataObject);
            message.setFullTextData(fullTextString);
        } catch (ReadValueException | InvalidArgumentException | ResolutionException | ChangeValueException e) {
            throw new CollectSearchDataException("Error during collect search data", e);
        }
    }

    /**
     * Collects data for full text search from data object to each fts field into some array of objects
     * Example:
     * {
     *     "invoices": [
     *     {
     *         "searchData": "",
     *     },
     *     {
     *         "searchData": "some value",
     *     }
     *     ],
     *     "sourceField": "text for FTS"
     * }
     * so "text for FTS" string would be added to both searchData fields into invoices:
     *     "invoices": [
     *     {
     *         "searchData": "text for FTS",
     *     },
     *     {
     *         "searchData": "some value text for FTS",
     *     }
     *     ]
     * NOTE:: searchData field <b>should be</b> exist into target objects
     * @param message {
     *                "getFullTextField": "const/searchData",
     *                "getDataObject": "message/payment",
     *                "getTargetList": "message/payment/invoices",
     *                "getSimpleCopyFields": [\"paymentMethod\"]
     * }
     * @throws CollectSearchDataException
     */
    public void addFullTextValuesToEachField(final CollectSearchDataMessage message) throws CollectSearchDataException {

        try {
            String fullTextFieldName = message.getFullTextField();
            IField fullTextField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), fullTextFieldName);
            List<IObject> targetList = message.getTargetList();
            IObject dataObject = message.getDataObject();

            List<String> fieldNamesForCopy = message.getSimpleCopyFields();

            for (IObject targetObject: targetList) {
                String currentFTS = fullTextField.in(targetObject);
                String fullTextString = addToTextSearch(fieldNamesForCopy, dataObject);
                fullTextField.out(targetObject, currentFTS.concat(fullTextString));
            }
        } catch (ResolutionException | InvalidArgumentException | ChangeValueException | ReadValueException e) {
            throw new CollectSearchDataException("Error during collect search data from fields to each fts field into array", e);
        }
    }

    /**
     * TODO:: check its work
     * Collects data from fields inside array to one field.
     * Example into message:
     * "forEachCopyFields": { "array": [
     *   {
     *      "fromFields": ["короткое-наименование", "лицевой-счет"], - name of fields, which values should be added to fts field
     *      "fromList": "invoices" - name of field with array of sources objects
     *   },
     *   {
     *      "fromFields": ["фамилия", "адрес"],
     *      "fromList": "users"
     *   }
     * ]}
     *
     * @param message
     * @throws CollectSearchDataException
     */
    public void addFullTextValuesFromEachField(final CollectSearchDataMessage message) throws CollectSearchDataException {

        try {
            List<String> fieldNamesForCopy;
            IObject dataObject = message.getDataObject();
            String fullTextString = "";

            List<IObject> forEachCopyFieldNames = message.getForEachCopyFields();

            String sourceListName;
            IField sourceListField;
            List<IObject> sourceList;
            for (IObject copyDescription: forEachCopyFieldNames) {

                sourceListName = fromList.in(copyDescription);
                sourceListField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), sourceListName);
                sourceList = sourceListField.in(dataObject);
                fieldNamesForCopy = fromFields.in(copyDescription);
                for (IObject sourceObject : sourceList) {
                    fullTextString = fullTextString.concat(addToTextSearch(fieldNamesForCopy, sourceObject));
                }
            }

            message.setFullTextData(fullTextString);
        } catch (ResolutionException | InvalidArgumentException | ChangeValueException | ReadValueException e) {
            throw new CollectSearchDataException("Error during collect search data from array to fts", e);
        }
    }

    private String addToTextSearch(List<String> fieldNamesForCopy, IObject sourceObject)
        throws ResolutionException, ReadValueException, InvalidArgumentException {

        IField sourceField;
        String fullTextString = "";
        String source = null;
        for (String copyFieldName : fieldNamesForCopy) {
            sourceField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), copyFieldName);
            try {
                source = sourceField.in(sourceObject);
            } catch (InvalidArgumentException ignored) {}
            if (source != null) {
                fullTextString = fullTextString.concat(source).concat(SEPARATOR);
            }
        }

        return fullTextString;
    }
}
