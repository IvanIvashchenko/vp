package com.perspective.vpng.actor.collect_fts_data.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CollectSearchDataActorConfig {

    String getFromFieldsFieldName() throws ReadValueException;
    String getFromListFieldName() throws ReadValueException;
}
