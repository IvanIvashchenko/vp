package com.perspective.vpng.actor.collect_fts_data;

import com.perspective.vpng.actor.collect_fts_data.wrapper.CollectSearchDataActorConfig;
import com.perspective.vpng.actor.collect_fts_data.wrapper.CollectSearchDataMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.exception.ProcessExecutionException;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class CollectSearchDataActorTest {

    private CollectSearchDataActor actor;
    private final String searchDataFN = "searchData";

    @BeforeClass
    public static void prepareIOC() throws PluginException, ProcessExecutionException, InvalidArgumentException {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {

        CollectSearchDataActorConfig config = mock(CollectSearchDataActorConfig.class);
        when(config.getFromFieldsFieldName()).thenReturn("fromFields");
        when(config.getFromListFieldName()).thenReturn("fromList");
        actor = new CollectSearchDataActor(config);
    }

    @Test
    public void ShouldCreateDataForTextSearch() throws Exception {

        CollectSearchDataMessage message = mock(CollectSearchDataMessage.class);
        IObject dataObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"paymentMethod\": \"VISA\"," +
                "\"paymentNumber\": \"123123123\"," +
                "\"invoices\": [" +
                    "{" +
                        "\"короткое-наименование\": \"beeline\"," +
                        "\"лицевой-счет\": \"99990000\"" +
                    "}," +
                    "{" +
                        "\"короткое-наименование\": \"vozminet\"," +
                        "\"лицевой-счет\": \"667744\"" +
                    "}" +
                "]" +
            "}"
        );
        when(message.getDataObject()).thenReturn(dataObject);

        List<String> simpleCopyFields = new ArrayList<>();
        simpleCopyFields.add("paymentNumber");
        simpleCopyFields.add("paymentMethod");
        when(message.getSimpleCopyFields()).thenReturn(simpleCopyFields);

        actor.createFullTextSearchField(message);

        verify(message).setFullTextData(eq("123123123 VISA "));
    }

    @Test
    public void ShouldAddDataForEachTargetFields() throws Exception {

        CollectSearchDataMessage message = mock(CollectSearchDataMessage.class);
        IObject dataObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"paymentMethod\": \"VISA\"," +
                "\"paymentNumber\": \"123123123\"" +
            "}"
        );
        when(message.getDataObject()).thenReturn(dataObject);
        when(message.getFullTextField()).thenReturn(searchDataFN);
        List<IObject> targetList = new ArrayList<>();
        targetList.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"лицевой-счет\": \"99990000\", \"searchData\": \"some\"}"));
        targetList.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"лицевой-счет\": \"57656373\", \"searchData\": \"some\"}"));
        IField invoicesField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoices");
        invoicesField.out(dataObject, targetList);
        when(message.getTargetList()).thenReturn(targetList);

        List<String> simpleCopyFields = new ArrayList<>();
        simpleCopyFields.add("paymentNumber");
        simpleCopyFields.add("paymentMethod");
        when(message.getSimpleCopyFields()).thenReturn(simpleCopyFields);


        actor.addFullTextValuesToEachField(message);

        IField searchDataField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), searchDataFN);
        for (IObject targetObj : targetList) {
            String fts = searchDataField.in(targetObj);
            assertTrue(fts.contains("VISA"));
            assertTrue(fts.contains("123123123"));
            assertTrue(fts.contains("some"));
        }

    }
}
