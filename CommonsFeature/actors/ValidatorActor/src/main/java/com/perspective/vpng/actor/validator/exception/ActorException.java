package com.perspective.vpng.actor.validator.exception;

/**
 * Signal of failed create an actor.
 */
public class ActorException extends Exception {
    /**
     * Constructs a ActorException with detail message and cause of exception.
     *
     * @param message the detail message.
     * @param cause the cause of exception.
     */
    public ActorException(String message, Throwable cause) {
        super(message, cause);
    }

}
