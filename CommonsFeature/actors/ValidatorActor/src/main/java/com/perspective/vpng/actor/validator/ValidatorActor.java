package com.perspective.vpng.actor.validator;

import com.perspective.vpng.actor.validator.exception.ActorException;
import com.perspective.vpng.actor.validator.exception.ValidationException;
import com.perspective.vpng.actor.validator.wrapper.IConfig;
import com.perspective.vpng.actor.validator.wrapper.IValidationMessage;
import com.perspective.vpng.util.rule_parser.IRule;
import com.perspective.vpng.util.rule_parser.IRuleParser;
import com.perspective.vpng.util.rule_parser.exception.RuleParsingException;
import com.perspective.vpng.util.rule_parser.exception.ValidationByRuleException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ValidatorActor - is a actor for validation a scope by schema.
 */
public class ValidatorActor {

    private final IRuleParser parser;

    private final IField nameF;
    private final IField columnsF;
    private final IField constraintsF;
    private final IField constraintF;
    private final IField errorF;

    private final String scopeKey;

    /**
     * Constructs a ValidatorActor with specified config.
     *
     * @param config configuration for the actor.
     * @throws ActorException when failed create ValidatorActor.
     */
    public ValidatorActor(final IConfig config) throws ActorException {
        try {
            this.parser = IOC.resolve(Keys.getOrAdd(IRuleParser.class.getCanonicalName()));
            final IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            this.nameF = IOC.resolve(fieldKey, "name");
            this.columnsF = IOC.resolve(fieldKey, "columns");
            this.constraintsF = IOC.resolve(fieldKey, "constraints");
            this.constraintF = IOC.resolve(fieldKey, "constraint");
            this.errorF = IOC.resolve(fieldKey, "error");
            this.scopeKey = config.getScopeKey();
        } catch (ResolutionException | ReadValueException ex) {
            throw new ActorException("Cannot create an actor:["
                    + ValidatorActor.class.getCanonicalName() + "]!", ex);
        }
    }

    /**
     * Validates scope(container with keys and associated values from an incoming schema)
     * by the schema rules.
     * <br>The incoming schema has a lowest priority of trust,
     * so validation rules gets from original schema.
     * <br>If validation is failed then adds to the given message errors and throws ValidationException.
     *
     * @param message wrapper of container with incoming parameters.
     * @throws ValidationException when validation is failed or errors occurred during validation.
     *
     * @see IValidationMessage
     * @see IRule
     * @see IRuleParser
     */
    public final void validateScopeBySchema(IValidationMessage message) throws ValidationException {
        try {
            Map<String, Object> scope = message.getScope();
            List<IObject> schema = message.getSchema();
            IObject errors = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            boolean isValid = true;
            for (IObject block : schema) {
                String fieldName = nameF.in(block);
                if (columnsF.in(block) != null && scope.containsKey(fieldName)) {
                    isValid &= validateTable(block, scope, errors);
                    continue;
                }
                isValid &= validateField(block, scope, errors);
            }
            if (!isValid) {
                message.setErrors(errors);
                throw new ValidationException("Failed validation of the schema!");
            }
        } catch (RuleParsingException | ValidationByRuleException ex) {
            throw new ValidationException("Parsing error:" + ex.getMessage(), ex);
        } catch (Exception ex) {
            throw new ValidationException(ex.getMessage(), ex);
        }
    }

    /**
     * Validates the simple field of schema.
     * <br>The simple field must contains a value else validation failed.
     *
     * @param block simple field of schema.
     * @param scope container with values of schema.
     * @param errors validation errors messages.
     * @return <code>true</code>if field's value is exist and valid else <code>false</code>.
     * @throws Exception when errors occurred during validation of field.
     */
    protected boolean validateField(
            final IObject block,
            Map<String, Object> scope,
            IObject errors
    ) throws Exception {
        List<IObject> constraints = constraintsF.in(block);
        if (constraints == null || constraints.isEmpty()) {
            return true;
        }
        String fieldName = nameF.in(block);
        scope.put(scopeKey, fieldName);
        List<String> errorMessages = new ArrayList<>(constraints.size());
        for (IObject constraint : constraints) {
            IRule rule = parser.parse(constraintF.in(constraint));
            if (!rule.validate(scope)) {
                errorMessages.add(errorF.in(constraint));
            }
        }
        if (!errorMessages.isEmpty()) {
            IField currentField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), fieldName);
            currentField.out(errors, errorMessages);
            return false;
        }
        return true;
    }

    /**
     * Validates the table field of schema.
     * <br>The table field can contains a value or not.
     *              If the scope hasn't values of table then return <code>true</code>.
     *
     * @param block table field of schema.
     * @param scope container with values of schema.
     * @param errors validation errors messages.
     * @return <code>true</code>if all table values is valid or
     *              the scope hasn't values of table else <code>false</code>.
     * @throws Exception when errors occurred during validation of table.
     */
    protected boolean validateTable(
            final IObject block,
            Map<String, Object> scope,
            IObject errors
    ) throws Exception {
        Map<String, List<IRule>> columnRules = getColumnRules(block);
        if (columnRules.isEmpty()) {
            return true;
        }
        boolean isValid = true;
        String blockName = nameF.in(block);
        List<IObject> columns = columnsF.in(block);
        Map<String, List<String>> tableErrors = new HashMap<>(1);
        for (IObject column : columns) {
            String name = nameF.in(column);
            List<IRule> rules = columnRules.get(name);
            if (rules == null) {
                continue;
            }
            List<String> columnErrors = new ArrayList<>(1);
            for (IRule rule : rules) {
                scope.put(scopeKey, blockName + "." + name);
                isValid &= rule.validate(scope);
                if (!isValid) {
                    columnErrors.add(rule.getError());
                }
            }
            if (!columnErrors.isEmpty()) {
                tableErrors.put(name, columnErrors);
            }
        }
        if (!tableErrors.isEmpty()) {
            IField tableF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), blockName);
            tableF.out(errors, tableErrors);
        }
        return isValid;
    }

    /**
     * Gets an all rules for column from the specific table.
     * <br>The table contains rules in a string representation,
     * so this method parses they to IRule objects using the rules parser.
     *
     * @param table table with rules for columns.
     * @return map-container with key(column's name) and associated values(list of IRule objects).
     * @throws Exception when error occurred during getting rules.
     *
     * @see IRule
     * @see IRuleParser
     */
    protected Map<String, List<IRule>> getColumnRules(final IObject table) throws Exception {
        List<IObject> columns = columnsF.in(table);
        Map<String, List<IRule>> columnRules = new HashMap<>(columns.size());
        for (IObject column : columns) {
            String columnName = nameF.in(column);
            List<IObject> constraints = constraintsF.in(column);
            if (constraints != null && !constraints.isEmpty()) {
                List<IRule> rules = new ArrayList<>(constraints.size());
                for (IObject constraint : constraints) {
                    IRule rule = parser.parse(constraintF.in(constraint));
                    rule.setError(errorF.in(constraint));
                    rules.add(rule);
                }
                columnRules.put(columnName, rules);
            }
        }
        return columnRules;
    }

}
