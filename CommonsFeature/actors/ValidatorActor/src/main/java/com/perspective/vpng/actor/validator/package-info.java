/**
 * A package contains a common validator actor.
 * Can verify a scope by schema at the moment.
 */
package com.perspective.vpng.actor.validator;