package com.perspective.vpng.actor.validator.exception;

/**
 * Signal of validation failed or errors.
 */
public class ValidationException extends Exception {
    /**
     * Constructs a ValidationException with detail message.
     *
     * @param message the detail message.
     */
    public ValidationException(String message) {
        super(message);
    }

    /**
     * Constructs a ValidationException with detail message and cause of exception.
     *
     * @param message the detail message.
     * @param cause the cause of exception.
     */
    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

}
