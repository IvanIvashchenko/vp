package com.perspective.vpng.actor.validator.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;
import java.util.Map;

/**
 * Wrapper for message of validation scope by schema.
 *
 * @see com.perspective.vpng.actor.validator.ValidatorActor
 */
public interface IValidationMessage {
    /**
     * Gets a scope with values of an incoming schema.
     *
     * @return the scope of validation.
     * @throws ReadValueException when can't read value from message.
     */
    Map<String, Object> getScope() throws ReadValueException;

    /**
     * Gets original schema with validation rules for scope.
     *
     * @return the original schema with validation rules.
     * @throws ReadValueException when can't read value from message.
     */
    List<IObject> getSchema() throws ReadValueException;

    /**
     * Changes field <code>error</code> in the message.
     *
     * @param errors validation errors messages.
     * @throws ChangeValueException when can't change value from message.
     */
    void setErrors(IObject errors) throws ChangeValueException;

}
