package com.perspective.vpng.actor.validator.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for configuration object of ValidationActor.
 *
 * @see com.perspective.vpng.actor.validator.ValidatorActor
 */
public interface IConfig {
    /**
     * Gets a name of field to be validated.
     *
     * @return the name of field to be validated.
     * @throws ReadValueException when can't read value from message.
     */
    String getScopeKey() throws ReadValueException;

}
