package com.perspective.vpng.actor.validator;

import com.perspective.vpng.actor.validator.exception.ActorException;
import com.perspective.vpng.actor.validator.exception.ValidationException;
import com.perspective.vpng.actor.validator.wrapper.IConfig;
import com.perspective.vpng.actor.validator.wrapper.IValidationMessage;
import com.perspective.vpng.util.rule_parser.IRuleParser;
import com.perspective.vpng.util.schema_rule_parser.SchemaRuleParser;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.field.field.Field;
import info.smart_tools.smartactors.iobject.ds_object.DSObject;
import info.smart_tools.smartactors.iobject.field_name.FieldName;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.morph_expressions.interfaces.parser.IParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.*;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
@SuppressWarnings("all")
public class ValidatorActorTest {

    private ValidatorActor validator;
    private IObject schema;

    private final Pattern RU_LANGUAGE = Pattern.compile("^[А-яЁё.\\- ]{1,255}$");
    private final Pattern NUMBER = Pattern.compile("^[0-9]+(([.,])[0-9]{1,2})?$");
    private final Pattern DIGITS = Pattern.compile("^\\d+$");
    private final Pattern HOUSE = Pattern.compile("^[0-9А-яA-zЁё/]{1,7}$");

    public ValidatorActorTest() throws ActorException, ReadValueException,
            ResolutionException, InvalidArgumentException {

        mockStatic(IOC.class);
        mockStatic(Keys.class);

        IKey iObjectKey = mock(IKey.class);
        when(Keys.getOrAdd(IObject.class.getCanonicalName())).thenReturn(iObjectKey);
        when(IOC.resolve(eq(iObjectKey))).thenReturn(new DSObject());

        IKey fieldKey = mock(IKey.class);
        when(Keys.getOrAdd(IField.class.getCanonicalName())).thenReturn(fieldKey);
        when(IOC.resolve(eq(fieldKey), anyString())).thenAnswer((Answer) invocation ->
                new Field(new FieldName((String) invocation.getArguments()[1])));

        IConfig config = mock(IConfig.class);
        when(config.getScopeKey()).thenReturn("fieldName");

        IRuleParser parser = SchemaRuleParser.create();

        parser.registerFunction("сумма", args -> {
            List<Number> column = (List) args[0];
            double sum = 0.0;
            for (Number value : column) {
                sum += value.doubleValue();
            }
            return sum;
        });

        when(IOC.resolve(Keys.getOrAdd(IParser.class.getCanonicalName()))).thenReturn(parser);

        validator = new ValidatorActor(config);

        schema = new DSObject(
                "{\"schema\": [\n" +
                        "        {\n" +
                        "            \"name\": \"лицевой-счет\",\n" +
                        "            \"constraints\": [\n" +
                        "               {\n" +
                        "                   \"constraint\": \"обязательное && длина == 5 && изЦифр\",\n" +
                        "                    \"error\": \"wrong_code\"\n" +
                        "               }\n" +
                        "            ]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"фамилия\",\n" +
                        "            \"constraints\": [\n" +
                        "                {\n" +
                        "                    \"constraint\": \"обязательное && русский\",\n" +
                        "                    \"error\": \"wrong_lastname\"\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"дом\",\n" +
                        "            \"constraints\": [\n" +
                        "                {\n" +
                        "                    \"constraint\": \"дом\",\n" +
                        "                    \"error\": \"wrong_house\"\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"месяц\",\n" +
                        "            \"constraints\": []\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"год\"\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"всего-к-оплате\",\n" +
                        "            \"constraints\": [\n" +
                        "                {\n" +
                        "                    \"constraint\": \"число\",\n" +
                        "                    \"error\": \"wrong_result_sum\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"constraint\": \"сумма(услуги.к-оплате) == всего-к-оплате\",\n" +
                        "                    \"error\": \"wrong_result_sum\"\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"услуги\",\n" +
                        "            \"columns\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"наименование-услуги\",\n" +
                        "                    \"constraints\": [\n" +
                        "                        {\n" +
                        "                            \"constraint\": \"русский\",\n" +
                        "                            \"error\": \"wrong_service_name\"\n" +
                        "                        }\n" +
                        "                    ]\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"к-оплате\",\n" +
                        "                    \"constraints\": [\n" +
                        "                        {\n" +
                        "                            \"constraint\": \"число\",\n" +
                        "                            \"error\": \"wrong_sum\"\n" +
                        "                        }\n" +
                        "                    ]\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        },\n" +
                        "        {\n" +
                        "            \"name\": \"приборы-учета\",\n" +
                        "            \"columns\": [\n" +
                        "                {\n" +
                        "                    \"name\": \"тип-прибора-учета\"\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"группа\",\n" +
                        "                    \"constraints\": []\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"наименование-услуги\",\n" +
                        "                    \"constraints\": [\n" +
                        "                        {\n" +
                        "                            \"constraint\": \"обязательное\",\n" +
                        "                            \"error\": \"wrong_service_name\"\n" +
                        "                        }\n" +
                        "                    ]\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"предыдущие-показания\",\n" +
                        "                    \"constraints\": [\n" +
                        "                        {\n" +
                        "                            \"constraint\": \"число\",\n" +
                        "                            \"error\": \"wrong_previous_meter_readings\"\n" +
                        "                        }\n" +
                        "                    ]\n" +
                        "                },\n" +
                        "                {\n" +
                        "                    \"name\": \"текущие-показания\",\n" +
                        "                    \"constraints\": [\n" +
                        "                        {\n" +
                        "                            \"constraint\": \"число\",\n" +
                        "                            \"error\": \"wrong_current_meter_readings_number\"\n" +
                        "                        },\n" +
                        "                        {\n" +
                        "                            \"constraint\": \"приборы-учета.текущие-показания > приборы-учета.предыдущие-показания\",\n" +
                        "                            \"error\": \"wrong_current_meter_readings_less_than\"\n" +
                        "                        }\n" +
                        "                    ]\n" +
                        "                }\n" +
                        "            ]\n" +
                        "        }\n" +
                        "      ]}");
    }

    @Test
    public void should_SuccessValidateScopeBySchema() throws Exception {
        final String serviceName = "водоснабжение и водоотведение";
        final Map<String, Object> scope = new HashMap<String, Object>() {{
            put("лицевой-счет", 12421);
            put("фамилия", "Джексон");
            put("дом", 13);
            put("месяц", "январь");
            put("год", null);
            put("всего-к-оплате", 10460);
            put("услуги", new HashMap<String, Object>() {{
                put("наименование-услуги", Arrays.asList(serviceName, serviceName));
                put("к-оплате", Arrays.asList(4360, 6100));
            }});
            put("приборы-учета", new HashMap<String, Object>() {{
                put("тип-прибора-учета", Arrays.asList("Холодная вода", null, "Горячая вода", null));
                put("группа", Arrays.asList("1", null, "2", null));
                put("наименование-услуги", Arrays.asList(serviceName, serviceName, serviceName, serviceName));
                put("предыдущие-показания", Arrays.asList(123, 325, 983, 2500));
                put("текущие-показания", Arrays.asList("250", 400, "2000", 2700));
            }});
        }};
        verifyValidScope(scope);
    }

    @Test
    public void should_SuccessValidateScope_WithoutTableValues() throws Exception {
        final String serviceName = "водоснабжение и водоотведение";
        final Map<String, Object> scope = new HashMap<String, Object>() {{
            put("лицевой-счет", 12421);
            put("фамилия", "Джексон");
            put("дом", 13);
            put("месяц", "январь");
            put("год", null);
            put("всего-к-оплате", 10460);
            put("услуги", new HashMap<String, Object>() {{
                put("наименование-услуги", Arrays.asList(serviceName, serviceName));
                put("к-оплате", Arrays.asList(4360, 6100));
            }});
        }};
        verifyValidScope(scope);
    }

    @Test(expected = ValidationException.class)
    public void should_FailedValidateScope_WithEmptyTable() throws Exception {
        final String serviceName = "водоснабжение и водоотведение";
        final Map<String, Object> scope = new HashMap<String, Object>() {{
            put("лицевой-счет", 12421);
            put("фамилия", "Джексон");
            put("дом", 13);
            put("месяц", "январь");
            put("год", null);
            put("всего-к-оплате", 10460);
            put("услуги", new HashMap<String, Object>() {{
                put("наименование-услуги", Arrays.asList(serviceName, serviceName));
                put("к-оплате", Arrays.asList(4360, 6100));
            }});
            put("приборы-учета", new HashMap<String, Object>() {{}});
        }};

        IValidationMessage message = mock(IValidationMessage.class);
        when(message.getSchema()).thenReturn((List<IObject>) schema.getValue(new FieldName("schema")));
        when(message.getScope()).thenReturn(scope);
        validator.validateScopeBySchema(message);
    }

    @Test(expected = ValidationException.class)
    public void should_FailedValidateScope_WithoutSpecificTableFields() throws Exception {
        final String serviceName = "водоснабжение и водоотведение";
        final Map<String, Object> scope = new HashMap<String, Object>() {{
            put("лицевой-счет", 12421);
            put("фамилия", "Джексон");
            put("дом", 13);
            put("месяц", "январь");
            put("год", null);
            put("всего-к-оплате", 10460);
            put("услуги", new HashMap<String, Object>() {{
                put("наименование-услуги", Arrays.asList(serviceName, serviceName));
                put("к-оплате", Arrays.asList(4360, 6100));
            }});
            put("приборы-учета", new HashMap<String, Object>() {{
                put("тип-прибора-учета", Arrays.asList("Холодная вода", null, "Горячая вода", null));
                put("группа", Arrays.asList("1", null, "2", null));
                put("наименование-услуги", Arrays.asList(serviceName, serviceName, serviceName, serviceName));
            }});
        }};

        IValidationMessage message = mock(IValidationMessage.class);
        when(message.getSchema()).thenReturn((List<IObject>) schema.getValue(new FieldName("schema")));
        when(message.getScope()).thenReturn(scope);
        validator.validateScopeBySchema(message);
    }

    @Test
    public void should_FailedValidateScopeBySchema_AndSetErrors() throws Exception {
        final String serviceName = "водоснабжение и водоотведение";
        final Map<String, Object> scope = new HashMap<String, Object>() {{
            put("лицевой-счет", "123a45");
            put("фамилия", "Jackson");
            put("дом", "kl1m5kl12ml54k");
            put("месяц", "январь");
            put("год", null);
            put("всего-к-оплате", 1250);
            put("услуги", new HashMap<String, Object>() {{
                put("наименование-услуги", Arrays.asList(serviceName, serviceName));
                put("к-оплате", Arrays.asList(4360, 6100));
            }});
            put("приборы-учета", new HashMap<String, Object>() {{
                put("тип-прибора-учета", Arrays.asList("Холодная вода", null, "Горячая вода", null));
                put("группа", Arrays.asList("1", null, "2", null));
                put("наименование-услуги", Arrays.asList(serviceName, serviceName, serviceName, serviceName));
                put("предыдущие-показания", Arrays.asList(123, 425, 983, 2500));
                put("текущие-показания", Arrays.asList("250", 400, "2000", "awdawd"));
            }});
        }};

        IValidationMessage message = mock(IValidationMessage.class);
        when(message.getSchema()).thenReturn((List<IObject>) schema.getValue(new FieldName("schema")));
        when(message.getScope()).thenReturn(scope);

        try {
            validator.validateScopeBySchema(message);
            fail("Expected validation exception!");
        } catch (ValidationException ex) {
            assertEquals("Failed validation of the schema!", ex.getMessage());
        }

        verify(message).getSchema();
        verify(message).getScope();
        ArgumentCaptor<IObject> errorsCaptor = ArgumentCaptor.forClass(IObject.class);
        verify(message).setErrors(errorsCaptor.capture());
        IObject errors = errorsCaptor.getValue();

        List<String> errorsMsg;
        errorsMsg = (List) errors.getValue(new FieldName("лицевой-счет"));
        assertEquals(Collections.singletonList("wrong_code"), errorsMsg);
        errorsMsg = (List) errors.getValue(new FieldName("фамилия"));
        assertEquals(Collections.singletonList("wrong_lastname"), errorsMsg);
        errorsMsg = (List) errors.getValue(new FieldName("дом"));
        assertEquals(Collections.singletonList("wrong_house"), errorsMsg);
        errorsMsg = (List) errors.getValue(new FieldName("всего-к-оплате"));
        assertEquals(Collections.singletonList("wrong_result_sum"), errorsMsg);

        Map<String, List<String>> tableErrors;
        tableErrors = (Map) errors.getValue(new FieldName("услуги"));
        assertEquals(null, tableErrors);
        tableErrors = (Map) errors.getValue(new FieldName("приборы-учета"));
        assertEquals(1, tableErrors.size());
        assertEquals(
                Arrays.asList(
                        "wrong_current_meter_readings_number",
                        "wrong_current_meter_readings_less_than"),
                tableErrors.get("текущие-показания")
        );
    }

    private void verifyValidScope(final Map<String, Object> scope) throws Exception {
        IValidationMessage message = mock(IValidationMessage.class);
        when(message.getSchema()).thenReturn((List<IObject>) schema.getValue(new FieldName("schema")));
        when(message.getScope()).thenReturn(scope);

        validator.validateScopeBySchema(message);

        verify(message).getSchema();
        verify(message).getScope();
        verify(message, times(0)).setErrors(any());
    }

}
