/**
 * Contains exceptions for CheckValidityASyncOperationActor
 */
package com.perspective.vpng.actor.check_validity_async_operation.exception;