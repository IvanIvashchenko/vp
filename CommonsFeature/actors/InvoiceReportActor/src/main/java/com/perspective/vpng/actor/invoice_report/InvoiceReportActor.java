package com.perspective.vpng.actor.invoice_report;

import com.perspective.vpng.actor.invoice_report.wrapper.InvoiceReportMessage;
import com.perspective.vpng.actor.invoice_report.wrapper.PrepareEmailMessage;
import com.perspective.vpng.util.template_engine.ITemplateEngine;
import com.perspective.vpng.util.template_engine.TemplateEngine;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.nio.charset.Charset;
import java.util.*;

public class InvoiceReportActor {
    private ICachedCollection providerCollection;
    private ITemplateEngine templateEngine = TemplateEngine.create();
    private String reportTemplate =
            "   <div id=\"receiptwrapper\">" +
            "      <style type=\"text/css\"> @page { margin-left: 8px; margin-top: 22px; margin-right: 10px; @top-left-corner { 8px }; } </style>" +
            "        <br>" +
            "        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse;\">" +
            "          <tr>" +
            "            <td width=\"320\" valign=\"top\" style=\"font-size: 9.75pt;font-family: arial,serif;line-height:110%\">" +
            "              <table>" +
            "                <tr>" +
            "                  <td><span><img src=\"https://www-test.vseplatezhi.ru/resources/media/images/logo-bw.png\" width=\"213.75pt;\" height=\"37.5pt;\"><br><br><span id=\"sys_name\" style=\"font:9.75pt Arial;\"><b>Система online-платежей «ВсеПлатежи»</b></span><br><br><span style=\"font:9.75pt Arial;\"> НКО «Перспектива» (ООО)<br>ИНН&nbsp;5503135638, ОГРН&nbsp;1155500000017<br> Адрес:&nbsp;644033, г. Омск, ул. В.М. Шукшина, д. 9, пом. 9П<br> Тел.:&nbsp;8 800 700-08-38<br> Электронная почта:&nbsp;abon@vp.ru </span><br><span style=\"font:9.75pt Arial;\">Сайт:&nbsp;</span><span style=\"font:9.75pt Arial;font-weight:bold;\"><a href=\"http://VP.RU\">vp.ru</a></span><br><br><span style=\"font:8.35pt Arial;\">Комиссия системы:&nbsp;${systemCommission}&nbsp;руб.</span><br><span style=\"font:8.35pt Arial;\">Уважаемый пользователь! При необходимости Вы можете получить квитанцию на бумажном носителе, распечатав ее самостоятельно в личном кабинете, или обратиться к нам, написав запрос на эл. адрес <a href=\"mailto:abon@vp.ru\">abon@vp.ru</a>.</span><br><br><span style=\"font:8.35pt Arial;\">По запросу пользователей других городов заверенная квитанция отправляется Почтой России.</span></span></td>" +
            "                </tr>" +
            "                <tr>" +
            "                  <td width=\"320\" align=\"top\" style=\"font-size: 9.75pt;font-family: arial,serif;\">" +
            "                    <table align=\"right\" style=\"width: 135pt;margin-right: 10pt;\">" +
            "                      <tr>" +
            "                        <td>" +
            "                          <div id=\"stamp_block\" style=\"border: 3pt solid #000080;color: #000080;text-align: center;width: 130pt;font:7.25pt Arial;margin-top:7.5pt\"> НКО «Перспектива» (ООО)<br> ИНН&nbsp;5503135638<br><b>ОПЛАЧЕНО</b><br> 07/12/2016 13:12 </div>" +
            "                        </td>" +
            "                      </tr>" +
            "                    </table>" +
            "                  </td>" +
            "                </tr>" +
            "              </table>" +
            "            </td>" +
            "            <td align=\"left\" valign=\"top\" style=\"font-size: 9.75pt;font-family: arial,serif;line-height:110%;padding-left: 22.5pt;border-left: 0.75pt solid #000000;\">" +
            "              <table>" +
            "                <tr>" +
            "                  <td>" +
            "                    <div id=\"payment_props\"><span style=\"font:9.75pt Arial;font-weight:bold;\">Платеж №&nbsp;${invoiceNumber}<br> принят&nbsp;${payDate}</span></div>" +
            "                    <div id=\"company_info\" style=\"margin-top: 9.75pt;\"><span style=\"font:9.75pt Arial;\">Прием платежа в пользу:</span><br><span style=\"font:9.75pt Arial;font-weight:bold;\">${companyName}</span><br><span style=\"font:9.75pt Arial;\"> р/с&nbsp;${checkingAccount}<br> в&nbsp;${companyAddress}<br> БИК&nbsp;${bik}<br> ИНН&nbsp;${inn} </span></div>" +
            "                    <div id=\"payment_method\" style=\"margin-top: 9.75pt;\"><span style=\"font:9.75pt Arial;\"> Средство оплаты:&nbsp;Банковская карта </span></div>" +
            "                    <div id=\"payment_detail\" style=\"margin-top: 9.75pt;\">${credentials}<br><b>Услуга:&nbsp;${serviceName}</b><br><br>${services}</span></div>" +
            "                  </td>" +
            "                </tr>" +
            "                <tr>" +
            "                  <td><span style=\"font:9.75pt Arial;font-weight:bold;\"> Сумма к зачислению:&nbsp;${amountWithoutCommission}&nbsp;руб. </span></td>" +
            "                </tr>" +
            "              </table>" +
            "            </td>" +
            "          </tr>" +
            "        </table>" +
            "      </div>";
    private IField commissionF;
    private IField numberF;
    private IField providerIdF;
    private IField dateF;
    private IField fullNameF;
    private IField sumWithoutCommissionF;
    private IField clientF;
    private IField fullUserNameF;
    private IField phoneF;
    private IField bankBookF;
    private IField addressF;
    private IField nameF;
    private IField lastNameF;
    private IField patronymF;
    private IField periodF;
    private IField yearF;
    private IField monthF;
    private IField serviceNameF;
    private IField requisitesF;
    private IField raschetnySchetF;
    private IField bankAddressF;
    private IField bikF;
    private IField innF;

    private IField signF;
    private IField subjectF;
    private IField typeF;
    private IField mimeF;
    private IField textF;
    private IField recipientF;

    public InvoiceReportActor(IObject params) {
        try {
            this.commissionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "комиссия");
            this.numberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "number");
            this.dateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "date");
            this.fullNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "полное-наименование");
            this.sumWithoutCommissionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "сумма_без_комиссии");
            this.clientF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "клиент");
            this.fullUserNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "фио");
            this.phoneF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "телефон");
            this.bankBookF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "лицевой-счет/одной-строкой");
            this.addressF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "адрес/одной-строкой");
            this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "имя");
            this.lastNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "фамилия");
            this.patronymF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "отчество");
            this.periodF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "период-оплаты");
            this.yearF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "год");
            this.monthF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "месяц");
            this.serviceNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "название-услуги");
            this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");

            this.requisitesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "requisites");
            this.raschetnySchetF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "расчетный-счет");
            this.bankAddressF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "адрес");
            this.bikF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "бик");
            this.innF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "инн");

            this.signF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sign");
            this.subjectF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "subject");
            this.typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            this.mimeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "mime");
            this.textF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "text");
            this.recipientF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "recipient");

            IField providerCollectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerCollectionName");
            IField providerKeyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerKey");
            this.providerCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()),
                    providerCollectionNameF.in(params),
                    providerKeyF.in(params)
            );
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void createReport(InvoiceReportMessage message) {
        try {
            List<String> reports = new ArrayList<>();
            List<IObject> invoices = message.getInvoices();
            for (IObject invoice : invoices) {
                IObject provider = providerCollection.getItems(providerIdF.in(invoice)).get(0);
                IObject requisites = IOC.resolve(Keys.getOrAdd("ProviderRequisites"), new Object[]{requisitesF.in(provider)});

                //TODO:: add values when format is established
                Map<String, Object> tagValues = new HashMap<>();
                tagValues.put("${systemCommission}", commissionF.in(invoice));
                tagValues.put("${invoiceNumber}", numberF.in(invoice));
                tagValues.put("${payDate}", dateF.in(invoice));
                tagValues.put("${companyName}", fullNameF.in(invoice));
                tagValues.put("${checkingAccount}", raschetnySchetF.in(requisites));
                tagValues.put("${companyAddress}", bankAddressF.in(requisites));
                tagValues.put("${bik}", bikF.in(requisites));
                tagValues.put("${inn}", innF.in(requisites));
                tagValues.put("${credentials}", buildCredentials(invoice));
                tagValues.put("${serviceName}", serviceNameF.in(provider));
                tagValues.put("${services}", "");
                tagValues.put("${amountWithoutCommission}", sumWithoutCommissionF.in(invoice));
                String report = templateEngine.fillTemplate(reportTemplate, tagValues);
                reports.add(report);
            }

            message.setReports(reports);
        } catch (Exception e) {
            throw new RuntimeException("Failed to create invoice report", e);
        }
    }

    public void prepareReportsToMail(PrepareEmailMessage message) {
        try {
            IObject recipientObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            recipientF.out(recipientObject, message.getEmail());
            typeF.out(recipientObject, "To");
            message.setRecipients(Collections.singletonList(recipientObject));

            IObject attributes = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            signF.out(attributes, "SmartActors");
            subjectF.out(attributes, "Квитанция о совершении платежа");
            message.setMessageAttributes(attributes);

            List<IObject> messageParts = new ArrayList<>();
            for (String report : message.getReports()) {
                IObject messagePart = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                textF.out(messagePart, new String(report.getBytes(), Charset.forName("ISO-8859-1")));
                mimeF.out(messagePart, "text/html");
                typeF.out(messagePart, "text");
            }
            message.setMessageParts(messageParts);
        } catch (Exception e) {
            throw new RuntimeException("Failed to prepare email with invoices reports", e);
        }
    }

    private String buildCredentials(final IObject invoice) throws Exception {
        IObject client = clientF.in(invoice);
        String credentials ="";

        //TODO:: change to some more normal
        try {
            if (bankBookF.in(client) != null) {
                credentials += "<span style=\"font:9.75pt Arial;font-weight:bold;\">Лицевой счёт: " + bankBookF.in(client) + "</span>";
            }
        } catch (Exception e) {}

        IObject fioObject = fullUserNameF.in(client);
        if (fioObject != null) {
            credentials += "<br>Плательщик: " + lastNameF.in(fioObject) + " " + nameF.in(fioObject) + " " + patronymF.in(fioObject);
        }

        try {
            if (addressF.in(client) != null) {
                credentials += "<br>Адрес: " + addressF.in(client);
            }
        } catch (Exception e) {}

        if (phoneF.in(client) != null) {
            credentials += "<span style=\"font:9.75pt Arial;\"><b>Номер телефона: " + phoneF.in(client) + "</b>";
        }

        IObject periodObject = periodF.in(invoice);
        if (periodObject != null) {
            credentials += "<br>Период оплаты: " + monthF.in(periodObject)  + " " + yearF.in(periodObject) +"<br>";
        }

        return credentials;
    }
}
