package com.perspective.vpng.actor.invoice_report.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface InvoiceReportMessage {
    List<IObject> getInvoices() throws ReadValueException;
    void setReports(List<String> reports) throws ChangeValueException;
}
