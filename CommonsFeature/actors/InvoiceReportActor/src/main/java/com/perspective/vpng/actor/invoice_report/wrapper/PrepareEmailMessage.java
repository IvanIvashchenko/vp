package com.perspective.vpng.actor.invoice_report.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface PrepareEmailMessage {
    String getEmail() throws ReadValueException;

    List<String> getReports() throws ReadValueException;

    /**
     * Setter for email parts
     * @param parts list of IObjects
     * @throws ChangeValueException if any error during set is occurred
     */
    void setMessageParts(List<IObject> parts) throws ChangeValueException;

    /**
     * Setter for email parts
     * @param attr IObject
     * @throws ChangeValueException if any error during set is occurred
     */
    void setMessageAttributes(IObject attr) throws ChangeValueException;

    /**
     * Setter for recipients
     * @param recipients list of emails
     * @throws ChangeValueException if any error during set is occurred
     */
    void setRecipients(List<IObject> recipients) throws ChangeValueException;

}
