package com.perspective.vpng.actor.create_session.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ChangeIdMessage {
    void setSessionId(String id) throws ChangeValueException;

    /**
     * Returns list of cookies
     * @return list of cookies
     * @throws ReadValueException when something happens
     */
    List<IObject> getCookies() throws ReadValueException;
}
