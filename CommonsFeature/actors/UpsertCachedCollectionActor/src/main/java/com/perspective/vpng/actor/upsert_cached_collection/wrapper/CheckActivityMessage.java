package com.perspective.vpng.actor.upsert_cached_collection.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for message which used for check activity flag of object from cached collection
 */
public interface CheckActivityMessage {

    Boolean getIsActive() throws ReadValueException;
    void setIsActive(Boolean isActive) throws ChangeValueException;
}
