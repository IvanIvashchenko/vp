package com.perspective.vpng.actor.upsert_cached_collection.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface UpsertCollectionMessage {
    String getCachedCollectionName() throws ReadValueException;
    String getCachedCollectionKey() throws ReadValueException;
    IObject getObject() throws ReadValueException;

    void setResult(String result) throws ChangeValueException;
}
