package com.perspective.vpng.actor.upsert_cached_collection;

import com.perspective.vpng.actor.upsert_cached_collection.wrapper.CheckActivityMessage;
import com.perspective.vpng.actor.upsert_cached_collection.wrapper.UpsertCollectionMessage;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

//TODO:: handle exceptions
public class UpsertCachedCollectionActor {

    public void upsertCollectionDocument(UpsertCollectionMessage message) {

        try {
            ICachedCollection collection = IOC.resolve(
                    Keys.getOrAdd(ICachedCollection.class.getCanonicalName()),
                    message.getCachedCollectionName(),
                    message.getCachedCollectionKey()
            );
            collection.upsert(message.getObject());

            message.setResult("success");
        } catch (Exception e) {
            throw new RuntimeException("Failed to upsert document into cached collection", e);
        }
    }

    public void disableCollectionDocument(UpsertCollectionMessage message) {

        try {
            ICachedCollection collection = IOC.resolve(
                Keys.getOrAdd(ICachedCollection.class.getCanonicalName()),
                message.getCachedCollectionName(),
                message.getCachedCollectionKey()
            );
            collection.delete(message.getObject());

            message.setResult("success");
        } catch (Exception e) {
            throw new RuntimeException("Failed to delete document from cached collection", e);
        }
    }

    public void checkDocumentIsActive(final CheckActivityMessage message) {

        try {
            Boolean isActive = message.getIsActive();
            message.setIsActive(isActive);
        } catch (ReadValueException | ChangeValueException e) {
            throw new RuntimeException("Can't check activity flag", e);
        }
    }
}
