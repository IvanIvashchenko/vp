package com.perspective.vpng.strategy.url_params_convert;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.ioc_strategy_pack_plugins.resolve_standard_types_strategies_plugin.ResolveStandardTypesStrategiesPlugin;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class IObjectToUrlParamsConvertStrategyTest {

    private final IResolveDependencyStrategy strategy = new IObjectToUrlParamsConvertStrategy();

    @BeforeClass
    public static void setUp() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new ResolveStandardTypesStrategiesPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Test
    public void shouldConvertIObjectToUrlParamsString() throws Exception {

        IObject object = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"name\": \"value\"," +
                "\"name with space\": \"value with space\"," +
                "\"name%\": \"value%\"" +
            "}"
        );

        Pattern[] expectedParams = new Pattern[] {
            Pattern.compile("name=value"),
            Pattern.compile("name\\+with\\+space=value\\+with\\+space"),
            Pattern.compile("name%25=value%25"),
        };

        String url = strategy.resolve(object, "UTF-8");
        for (Pattern pattern : expectedParams) {
            assertTrue("Expected parameter: [" + pattern.toString() + "] is not found!", pattern.matcher(url).find());
        }
        assertEquals(url.split("&").length, 3);
    }
}
