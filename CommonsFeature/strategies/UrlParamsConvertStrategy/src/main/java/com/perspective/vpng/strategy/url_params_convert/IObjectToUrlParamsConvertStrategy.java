package com.perspective.vpng.strategy.url_params_convert;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.SerializeException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class IObjectToUrlParamsConvertStrategy implements IResolveDependencyStrategy {

    private static final String SEPARATOR = "&";
    private static final String EQUALITY_SIGN = "=";

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        try {
            StringBuilder builder = new StringBuilder();
            String jsonStr = ((IObject) args[0]).serialize();
            JSONObject convertObj = new JSONObject(jsonStr);
            String encoding = String.valueOf(args[1]);
            for(String key : convertObj.keySet()) {
                builder = builder.append(URLEncoder.encode(key, encoding))
                        .append(EQUALITY_SIGN)
                        .append(URLEncoder.encode(String.valueOf(convertObj.get(key)), encoding))
                        .append(SEPARATOR);
            }
            String result = builder.toString();

            return (T) result.substring(0, result.length() - 1);
        } catch (SerializeException | JSONException | UnsupportedEncodingException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }
}
