package com.perspective.vpng.strategy.fill_not_exist_field_strategy;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;

/**
 * Strategy that uses in this sample:
 *  you have message: {"blabla": "bla", "any_field": {}}
 *  and need put "anything" in "trata/field/nested_field"
 *  result message: {
 *      "blabla": "bla",
 *      "any_field": {
 *          "field": {
 *              "nested_field": "anything"
 *          }
 *      }
 *  }
 */
public class FillNotExistFieldStrategy implements IResolveDependencyStrategy {

    /**
     * @param args array of needed parameters for resolve dependency
     * @param <T> target type of returned value
     * @throws ResolveDependencyStrategyException when any exception occurred
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            String[] path1 = ((String) args[1]).split("/");
            List<String> path = new ArrayList<>();
            //down to 1 because first field in path must exist
            for (int i = path1.length - 1; i > 0; i--) {
                path.add(path1[i]);
            }
            Object wrapObject = args[0];
            IObject outsider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            for (String fieldName : path) {
                IField field = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), fieldName);
                field.out(outsider, wrapObject);
                wrapObject = outsider;
                outsider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            }

            return (T) wrapObject;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException("Failed to execute strategy FillNotExistFieldStrategy", e);
        }
    }

}
