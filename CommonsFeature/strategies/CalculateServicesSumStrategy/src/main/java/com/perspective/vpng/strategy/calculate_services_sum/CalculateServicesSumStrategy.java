package com.perspective.vpng.strategy.calculate_services_sum;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.List;

public class CalculateServicesSumStrategy implements IResolveDependencyStrategy {
    private static final IField toPayF;
    static {
        try {
            toPayF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "к-оплате");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(Object... objects) throws ResolveDependencyStrategyException {
        try {
            List<IObject> services = (List) objects[0];
            BigDecimal sum = new BigDecimal(0);
            for (IObject service : services) {
                if (toPayF.in(service) != null) {
                    sum = sum.add(toPayF.in(service, BigDecimal.class));
                }
            }
            return (T) sum;
        } catch(Exception e) {
            throw new ResolveDependencyStrategyException("Failed to calculate sum", e);
        }
    }

}
