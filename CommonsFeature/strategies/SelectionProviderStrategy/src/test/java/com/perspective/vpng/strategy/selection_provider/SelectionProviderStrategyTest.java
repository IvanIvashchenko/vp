package com.perspective.vpng.strategy.selection_provider;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

public class SelectionProviderStrategyTest {

    private IResolveDependencyStrategy strategy;

    @Before
    public void setUp() {
        strategy = new SelectionProviderStrategy();
    }

    @BeforeClass
    public static void prepareIOC() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();

        bootstrap.start();
    }

    @Test
    public void shouldGetStrategy() throws Exception {
        List<IObject> providers = new ArrayList<>();
        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"fieldA\": \"valueA\"}");
        providers.add(provider);
        IObject result = strategy.resolve(new Object[] { providers });

        assertNotNull(result);
        IField field = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "fieldA");
        String providerValue = field.in(provider);
        String resultValue = field.in(result);
        assertEquals(providerValue, resultValue);
    }

    @Test(expected = ResolveDependencyStrategyException.class)
    public void shouldThrowResolveDependencyStrategyException_when_InternalExceptionIsThrown() throws Exception {

        strategy.resolve(new ArrayList<>());
        fail();
    }
}
