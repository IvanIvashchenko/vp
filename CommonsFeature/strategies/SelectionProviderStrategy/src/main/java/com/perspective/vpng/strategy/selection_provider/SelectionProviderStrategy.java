package com.perspective.vpng.strategy.selection_provider;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;

import java.util.List;

/**
 * Strategy for choice provider from providers list
 */
public class SelectionProviderStrategy implements IResolveDependencyStrategy {

    @Override
    public <T> T resolve(final Object... objects) throws ResolveDependencyStrategyException {
        try {
            return (T) ((List) objects[0]).get(0);
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }
}
