package com.perspective.vpng.strategy.standard_transformation;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Optional;

public class SubstringStrategy implements IResolveDependencyStrategy {

    private final IField beginIndexF;
    private final IField endIndexF;

    public SubstringStrategy() throws ResolutionException {
        this.beginIndexF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "beginIndex");
        this.endIndexF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "endIndex");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        String source = (String) args[0];
        IObject params = (IObject) args[1];
        checkParamsObject(params);
        try {
            int beginIndex = (int) Optional.ofNullable(beginIndexF.in(params)).orElse(0);
            int endIndex = (int) Optional.ofNullable(endIndexF.in(params)).orElse(source.length());
            source = source.substring(beginIndex, endIndex);
        } catch (ReadValueException | InvalidArgumentException e) {
            throw new ResolveDependencyStrategyException("Can't make substring", e);
        }
        return (T) source;
    }

    private void checkParamsObject(IObject params) throws ResolveDependencyStrategyException {
        try {
            if (beginIndexF.in(params) == null && endIndexF.in(params) == null) {
                throw new ResolveDependencyStrategyException("Both indexes are null");
            }
        } catch (ReadValueException | InvalidArgumentException e) {
            throw new ResolveDependencyStrategyException("Invalid params object", e);
        }
    }
}
