package com.perspective.vpng.strategy.standard_transformation;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubstringStrategyTest {

    private IResolveDependencyStrategy strategy;

    @BeforeClass
    public static void preapreIOC() throws Exception {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws ResolutionException {
        strategy = new SubstringStrategy();
    }

    @Test(expected = ResolveDependencyStrategyException.class)
    public void shouldThrowException_When_ParamsAreNull() throws Exception {

        strategy.resolve("", IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName())));
    }

    @Test
    public void shouldCorrectMakeSubstring_When_StartIndexIsNull() throws Exception {

        IObject params = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"endIndex\": 6}");
        String result = strategy.resolve("source string", params);
        assertEquals(result, "source");
    }

    @Test
    public void shouldCorrectMakeSubstring_When_EndIndexIsNull() throws Exception {

        IObject params = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"beginIndex\": 7}");
        String result = strategy.resolve("source string", params);
        assertEquals(result, "string");
    }

    @Test
    public void shouldCorrectMakeSubstring_When_BothIndexesAreGiven() throws Exception {

        IObject params = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"beginIndex\": 2, \"endIndex\": 10}");
        String result = strategy.resolve("source string", params);
        assertEquals(result, "urce str");
    }
}
