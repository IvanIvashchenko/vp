package com.perspective.vpng.strategy.iobject_convert;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.json.XML;

public class XmlToIObjectConvertStrategy implements IResolveDependencyStrategy {

    private final IKey iObjKey;

    public XmlToIObjectConvertStrategy() throws ResolutionException {
        iObjKey = Keys.getOrAdd(IObject.class.getCanonicalName());
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            String xml = (String) args[0];
            String jsonStr = XML.toJSONObject(xml).toString();
            return IOC.resolve(iObjKey, jsonStr);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Illegal type of given argument!", ex);
        } catch (IndexOutOfBoundsException ex) {
            throw new ResolveDependencyStrategyException("Expected one argument!", ex);
        } catch (ResolutionException ex) {
            throw new ResolveDependencyStrategyException("Convert xml to iobject failed!", ex);
        }
    }

}
