package com.perspective.vpng.strategy.iobject_convert;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.ioc_strategy_pack_plugins.resolve_standard_types_strategies_plugin.ResolveStandardTypesStrategiesPlugin;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.fail;

public class XmlToIObjectConvertStrategyTest {

    private final IResolveDependencyStrategy strategy;

    public XmlToIObjectConvertStrategyTest() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();
        new ResolveStandardTypesStrategiesPlugin(bootstrap).load();

        bootstrap.start();

        strategy = new XmlToIObjectConvertStrategy();
    }

    @Test
    public void should_ConvertXmlToIObject() throws Exception {
        String xml = "<tag_1>test_string_1</tag_1>" +
                "<tag_2>123456</tag_2>" +
                "<tag_3>true</tag_3>" +
                "<tag_4>" +
                    "<tag_4_1>test_string_4_1</tag_4_1>" +
                    "<tag_4_2>1234567</tag_4_2>" +
                    "<tag_4_3>false</tag_4_3>" +
                "</tag_4>" +
                "<tag_5>" +
                    "<tag_5_1>test_string_5_1</tag_5_1>" +
                    "<tag_5_2>1234567</tag_5_2>" +
                    "<tag_5_3>true</tag_5_3>" +
                    "<tag_5_4>" +
                        "<tag_5_4_1>test_string_5_4_1</tag_5_4_1>" +
                        "<tag_5_4_2>12345678</tag_5_4_2>" +
                        "<tag_5_4_3>true</tag_5_4_3>" +
                    "</tag_5_4>" +
                "</tag_5>" +
                "<tag_6>" +
                    "<tag_6_1>test_string_6_1#1</tag_6_1>" +
                    "<tag_6_2>1234567.1</tag_6_2>" +
                    "<tag_6_3>false</tag_6_3>" +
                "</tag_6>" +
                "<tag_6>" +
                    "<tag_6_1>test_string_6_1#2</tag_6_1>" +
                    "<tag_6_2>1234567.2</tag_6_2>" +
                    "<tag_6_3>true</tag_6_3>" +
                "</tag_6>";

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
        Map<IField, Object> expectedFields = new HashMap<IField, Object>() {{
            put(IOC.resolve(fieldKey, "tag_1"), "test_string_1");
            put(IOC.resolve(fieldKey, "tag_2"), 123456);
            put(IOC.resolve(fieldKey, "tag_3"), true);
            put(IOC.resolve(nestedFieldKey, "tag_4/tag_4_1"), "test_string_4_1");
            put(IOC.resolve(nestedFieldKey, "tag_4/tag_4_2"), 1234567);
            put(IOC.resolve(nestedFieldKey, "tag_4/tag_4_3"), false);
            put(IOC.resolve(nestedFieldKey, "tag_5/tag_5_1"), "test_string_5_1");
            put(IOC.resolve(nestedFieldKey, "tag_5/tag_5_2"), 1234567);
            put(IOC.resolve(nestedFieldKey, "tag_5/tag_5_3"), true);
            put(IOC.resolve(nestedFieldKey, "tag_5/tag_5_4/tag_5_4_1"), "test_string_5_4_1");
            put(IOC.resolve(nestedFieldKey, "tag_5/tag_5_4/tag_5_4_2"), 12345678);
            put(IOC.resolve(nestedFieldKey, "tag_5/tag_5_4/tag_5_4_3"), true);
        }};

        IObject iObject = strategy.resolve(xml);

        for (Map.Entry<IField, Object> field : expectedFields.entrySet()) {
            Object value = field.getKey().in(iObject);
            if (!value.equals(field.getValue())) {
                fail("Actual IObject hasn't contains a some required fields!");
            }
        }
        List<IObject> list = ((IField) IOC.resolve(fieldKey, "tag_6")).in(iObject);
        Map<IField, List<Object>> fields = new HashMap<IField, List<Object>>() {{
            put(IOC.resolve(fieldKey, "tag_6_1"), Arrays.asList("test_string_6_1#1", "test_string_6_1#2"));
            put(IOC.resolve(fieldKey, "tag_6_2"), Arrays.asList(1234567.1, 1234567.2));
            put(IOC.resolve(fieldKey, "tag_6_3"), Arrays.asList(false, true));
        }};
        for (int i = 0; i < list.size(); i++) {
            IObject item = list.get(i);
            for (Map.Entry<IField, List<Object>> field : fields.entrySet()) {
                if (!field.getKey().in(item).equals(field.getValue().get(i))) {
                    fail("Incorrect format of the field-list in IObject!");
                }
            }
        }
    }

}
