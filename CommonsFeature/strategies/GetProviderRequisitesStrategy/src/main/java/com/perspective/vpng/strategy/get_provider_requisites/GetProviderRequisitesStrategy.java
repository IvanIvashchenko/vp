package com.perspective.vpng.strategy.get_provider_requisites;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class GetProviderRequisitesStrategy implements IResolveDependencyStrategy {
    private ICachedCollection collection;

    public GetProviderRequisitesStrategy() throws ResolutionException {
        collection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), "requisites", "requisitesKey");
    }

    @Override
    public <T> T resolve(final Object... objects) throws ResolveDependencyStrategyException {
        try {
            return (T) collection.getItems((String) objects[0]).get(0);
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }
}
