package com.perspective.vpng.strategy.xml_convert;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.ioc_strategy_pack_plugins.resolve_standard_types_strategies_plugin.ResolveStandardTypesStrategiesPlugin;
import org.junit.Test;

import java.util.regex.Pattern;

import static org.junit.Assert.fail;

public class IObjectToXmlConvertStrategyTest {

    private final IResolveDependencyStrategy strategy = new IObjectToXmlConvertStrategy();

    public IObjectToXmlConvertStrategyTest() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new ResolveStandardTypesStrategiesPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Test
    public void should_ConvertIObjectToXml() throws Exception {
        String jsonStr = "{\n" +
                "   'tag_1': 'test_string_1',\n" +
                "   'tag_2': 123456,\n" +
                "   'tag_3': true,\n" +
                "   'tag_4': {\n" +
                "       'tag_4_1': 'test_string_4_1',\n" +
                "       'tag_4_2': 1234567,\n" +
                "       'tag_4_3': false\n" +
                "   },\n" +
                "   'tag_5': {\n" +
                "       'tag_5_1': 'test_string_5_1',\n" +
                "       'tag_5_2': 1234567,\n" +
                "       'tag_5_3': true,\n" +
                "       'tag_5_4': {\n" +
                "           'tag_5_4_1': 'test_string_5_4_1',\n" +
                "           'tag_5_4_2': 12345678,\n" +
                "           'tag_5_4_3': true\n" +
                "       }\n" +
                "   },\n" +
                "   'tag_6': [\n" +
                "       {\n" +
                "           'tag_6_1': 'test_string_6_1#1',\n" +
                "           'tag_6_2': 1234567.1,\n" +
                "           'tag_6_3': false\n" +
                "       },\n" +
                "       {\n" +
                "           'tag_6_1': 'test_string_6_1#2',\n" +
                "           'tag_6_2': 1234567.2,\n" +
                "           'tag_6_3': true\n" +
                "       }\n" +
                "   ]\n" +
                "}\n";

        Pattern[] expectedTags = new Pattern[] {
                Pattern.compile("<tag_1>test_string_1</tag_1>"),
                Pattern.compile("<tag_2>123456</tag_2>"),
                Pattern.compile("<tag_3>true</tag_3>"),
                Pattern.compile("<tag_4>.*<tag_4_1>test_string_4_1</tag_4_1>.*</tag_4>"),
                Pattern.compile("<tag_4>.*<tag_4_2>1234567</tag_4_2>.*</tag_4>"),
                Pattern.compile("<tag_4>.*<tag_4_3>false</tag_4_3>.*</tag_4>"),
                Pattern.compile("<tag_5>.*<tag_5_1>test_string_5_1</tag_5_1>.*</tag_5>"),
                Pattern.compile("<tag_5>.*<tag_5_2>1234567</tag_5_2>.*</tag_5>"),
                Pattern.compile("<tag_5>.*<tag_5_3>true</tag_5_3>.*</tag_5>"),
                Pattern.compile("<tag_5>.*<tag_5_4>.*<tag_5_4_1>test_string_5_4_1</tag_5_4_1>.*</tag_5_4>.*</tag_5>"),
                Pattern.compile("<tag_5>.*<tag_5_4>.*<tag_5_4_2>12345678</tag_5_4_2>.*</tag_5_4>.*</tag_5>"),
                Pattern.compile("<tag_5>.*<tag_5_4>.*<tag_5_4_3>true</tag_5_4_3>.*</tag_5_4>.*</tag_5>"),
                Pattern.compile("<tag_6>.*<tag_6_1>test_string_6_1#1</tag_6_1>.*</tag_6>"),
                Pattern.compile("<tag_6>.*<tag_6_2>1234567\\.1</tag_6_2>.*</tag_6>"),
                Pattern.compile("<tag_6>.*<tag_6_3>false</tag_6_3>.*</tag_6>"),
                Pattern.compile("<tag_6>.*<tag_6_1>test_string_6_1#2</tag_6_1>.*</tag_6>"),
                Pattern.compile("<tag_6>.*<tag_6_2>1234567\\.2</tag_6_2>.*</tag_6>"),
                Pattern.compile("<tag_6>.*<tag_6_3>true</tag_6_3>.*</tag_6>")
        };

        IObject convertObj = IOC.resolve(
                Keys.getOrAdd(IObject.class.getCanonicalName()),
                jsonStr.replaceAll("'", "\"")
        );
        String xml = strategy.resolve(convertObj);
        for (Pattern pattern : expectedTags) {
            if (!pattern.matcher(xml).find()) {
                fail("Expected tag: [" + pattern.toString() + "]. But actual xml doesn't contain his!");
            }
        }
    }

}
