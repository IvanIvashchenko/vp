package com.perspective.vpng.strategy.xml_convert;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.SerializeException;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

public class IObjectToXmlConvertStrategy implements IResolveDependencyStrategy {

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            String jsonStr = ((IObject) args[0]).serialize();
            JSONObject convertObj = new JSONObject(jsonStr);
            return (T) XML.toString(convertObj);
        } catch (SerializeException | JSONException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Illegal type of given argument!", ex);
        } catch (IndexOutOfBoundsException ex) {
            throw new ResolveDependencyStrategyException("Expected one argument!", ex);
        }
    }

}
