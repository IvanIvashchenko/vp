package com.perspective.vpng.plugin.build_gw_request;

import com.perspective.vpng.actor.payment_request.wrapper.ClientBackUrlConfig;
import com.perspective.vpng.rule.vsep_gw_request_build.VsepGwRequestBuildRule;
import com.perspective.vpng.strategy.build_gw_request.BuildGwRequestStrategy;
import com.perspective.vpng.strategy.build_gw_request.IGwRequestBuilder;
import com.perspective.vpng.strategy.build_gw_request.exception.BuildGatewayRequestException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Plugin for loading {@link BuildGwRequestStrategy} rule.
 */
public class BuildGwRequestPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    private static final String GW_PROPERTY_COLLECTION_NAME = "gwPropertiesCollection";
    private static final String GW_PROPERTY_COLLECTION_KEY = "gwType";
    private static final String APP_URL = "https://www2-test.vseplatezhi.ru/";

    /**
     * Default constructor
     * @param bootstrap element
     */
    public BuildGwRequestPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads custom implementation of request builder rules, put it in the rule map and
     * registers in <code>IOC</code> with key <code>gwRequestBuilderStrategy</code>.
     * Begin loads rule after loading plugins: <code>IOC</code>, <code>wds_object</code>
     * and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("GwRequestParametersBuilderPlugin");
            item
//                    .after("IOC")
//                    .after("wds_object")
//                    .before("starter")
                    .process(() -> {
                        try {
                            Map<String, IGwRequestBuilder> strategies = new HashMap<>();

                            IOC.register(
                                    Keys.getOrAdd("gwRequestBuildStrategy"),
                                    new ApplyFunctionToArgumentsStrategy(
                                        (args) -> {
                                            try {

                                                ClientBackUrlConfig clientBackUrlParams = (ClientBackUrlConfig) args[2];
                                                strategies.put(
                                                    "vsep",
                                                    new VsepGwRequestBuildRule(clientBackUrlParams)
                                                );
                                                IResolveDependencyStrategy buildGwRequestStrategy = new BuildGwRequestStrategy(
                                                    strategies,
                                                    GW_PROPERTY_COLLECTION_NAME,
                                                    GW_PROPERTY_COLLECTION_KEY
                                                );

                                                return buildGwRequestStrategy.resolve(args);
                                            } catch (BuildGatewayRequestException | ResolveDependencyStrategyException e) {
                                                throw new RuntimeException(e);
                                            }
                                        })
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException(
                                    "GwRequestParametersBuilderPlugin plugin can't load!", ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }

}
