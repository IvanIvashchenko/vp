package com.perspective.vpng.plugin.determination_payment_kind;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IPoorAction;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@PrepareForTest({IOC.class, Keys.class, DeterminationPaymentKindPlugin.class})
@RunWith(PowerMockRunner.class)
public class DeterminationPaymentKindPluginTest {

    private IBootstrap bootstrap;
    private IPlugin plugin;

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        bootstrap = mock(IBootstrap.class);
        plugin = new DeterminationPaymentKindPlugin(bootstrap);
    }

    @Test
    public void Should_CorrectLoadPlugin() throws Exception {
        BootstrapItem item = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("DeterminationPaymentKindPlugin").thenReturn(item);

        when(item.after("IOC")).thenReturn(item);
        when(item.after("wds_object")).thenReturn(item);
        when(item.before("starter")).thenReturn(item);

        plugin.load();

        verifyNew(BootstrapItem.class).withArguments("DeterminationPaymentKindPlugin");

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);

        verify(item).process(actionArgumentCaptor.capture());
        verify(bootstrap).add(item);

        IKey gwRequestBuilderStrategyKey = mock(IKey.class);
        when(Keys.getOrAdd("determinationPaymentKindStrategy"))
                .thenReturn(gwRequestBuilderStrategyKey);

        actionArgumentCaptor.getValue().execute();

        verifyStatic();
        Keys.getOrAdd("determinationPaymentKindStrategy");

        ArgumentCaptor<ApplyFunctionToArgumentsStrategy> argumentCaptor = ArgumentCaptor
                .forClass(ApplyFunctionToArgumentsStrategy.class);

        verifyStatic();
        IOC.register(
                eq(gwRequestBuilderStrategyKey),
                argumentCaptor.capture());

    }

    @Test(expected = PluginException.class)
    public void ShouldThrowException_When_InternalExceptionIsThrown() throws Exception {

        whenNew(BootstrapItem.class).withArguments("DeterminationPaymentKindPlugin").thenThrow(new InvalidArgumentException(""));
        plugin.load();
    }

}

