package com.perspective.vpng.plugin.determination_payment_kind;

import com.perspective.vpng.strategy.determination_payment_kind.DeterminationPaymentKindStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Arrays;
import java.util.List;

/**
 * Plugin for loading {@link DeterminationPaymentKindStrategy} rule.
 */
public class DeterminationPaymentKindPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public DeterminationPaymentKindPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads custom implementation of determination payment kind rule, put it in the rule map and
     * registers in <code>IOC</code> with key <code>DeterminationPaymentKindStrategy</code>.
     * Begin loads rule after loading plugins: <code>IOC</code>, <code>wds_object</code>
     * and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("DeterminationPaymentKindPlugin");
            item
//                    .after("IOC")
//                    .after("wds_object")
//                    .before("starter")
                    .process(() -> {
                        try {
                            // NOTE: Payments types names.
                            // @see {com.perspective.vpng.plugin.format_shopping_cart_step_two.PaymentMethodPlugin}
                            List<String> availablePaymentTypes = Arrays.asList("standard", "communal");
                            IOC.register(
                                    Keys.getOrAdd("determinationPaymentKindStrategy"),
                                    new DeterminationPaymentKindStrategy(
                                            availablePaymentTypes,
                                            "paymentType",
                                            "mixed"
                                    )
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException(
                                    "DeterminationPaymentKindPlugin plugin can't load!", ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
