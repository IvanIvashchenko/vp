package com.perspective.vpng.plugin.message_authentication_code;

import com.perspective.vpng.rule.hmac_message_authentication_code.HmacMessageAuthenticationCode;
import com.perspective.vpng.strategy.message_authentication_code.MACSigning;
import com.perspective.vpng.strategy.message_authentication_code.MessageAuthenticationCodeStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Plugin for loading {@link com.perspective.vpng.strategy.message_authentication_code.MessageAuthenticationCodeStrategy} rule.
 */
public class MessageAuthenticationCodePlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public MessageAuthenticationCodePlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link MessageAuthenticationCodeStrategy} rule and registers in <cdoe>IOC</cdoe>
     * with key <code>messageAuthenticationCodeStrategy</code>.
     * Begin loading plugin after loading plugins: <code>IOC</code> and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading plugin.
     */
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("MessageAuthenticationCodePlugin");
            item
                    /*.after("IOC")
                    .before("starter")*/
                    .process(() -> {
                        try {
                            Map<String, MACSigning> strategies = new HashMap<>();
                            strategies.put(
                                    "hmac",
                                    new HmacMessageAuthenticationCode()
                            );

                            IOC.register(
                                    Keys.getOrAdd("messageAuthenticationCodeStrategy"),
                                    new MessageAuthenticationCodeStrategy(strategies)
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException(
                                    "MessageAuthenticationCodePlugin plugin can't load!", ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
