package com.perspective.vpng.plugin.calculate_distribution;

import com.perspective.vpng.actor.calculate_distribution.ChooseServiceActor;
import com.perspective.vpng.actor.calculate_distribution.wrapper.ChooseServiceActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link com.perspective.vpng.actor.calculate_distribution.ChooseServiceActor} actor.
 */
public class ChooseServiceActorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ChooseServiceActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link com.perspective.vpng.actor.calculate_distribution.ChooseServiceActor} actor and registers in <cdoe>IOC</cdoe> with key <code>ChooseServiceActor</code>.
     * Begin loading plugin after loading plugins: <code>IOC</code> and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading plugin.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ChooseServiceActorPlugin");

            item
                    /*.after("IOC")
                    .before("starter")*/
                    .process(() -> {
                        try {
                            IKey actorKey = Keys.getOrAdd(ChooseServiceActor.class.getCanonicalName());
                            IOC.register(actorKey,
                                    new ApplyFunctionToArgumentsStrategy(
                                            (args) -> {
                                                try {
                                                    IObject params = (IObject) args[0];
                                                    IField servicesFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "servicesFieldName");
                                                    IField payerIdFieldNameFNF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "payerIdFieldNameFN");
                                                    IField withCharacteristicsFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "withCharacteristicsFieldName");
                                                    IField distributionsFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "distributionsFieldName");
                                                    IField invoiceIdFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoiceIdFieldName");

                                                    return new ChooseServiceActor(new ChooseServiceActorConfig() {
                                                        @Override
                                                        public String getServicesFieldName() throws ReadValueException {
                                                            try {
                                                                return servicesFieldNameF.in(params);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                        @Override
                                                        public String getPayerIdFieldNameFN() throws ReadValueException {
                                                            try {
                                                                return payerIdFieldNameFNF.in(params);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                        @Override
                                                        public String getWithCharacteristicsFieldName() throws ReadValueException {
                                                            try {
                                                                return withCharacteristicsFieldNameF.in(params);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                        @Override
                                                        public String getDistributionsFieldName() throws ReadValueException {
                                                            try {
                                                                return distributionsFieldNameF.in(params);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                        @Override
                                                        public String getInvoiceIdFieldName() throws ReadValueException {
                                                            try {
                                                                return invoiceIdFieldNameF.in(params);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    throw new RuntimeException(
                                                        "Can't create choose service actor: " + e.getMessage(), e);
                                                }
                                            }
                                    )
                            );
                        } catch (ResolutionException | InvalidArgumentException | RegistrationException ex) {
                            throw new ActionExecuteException("Calculate choose service plugin can't load: " + ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
