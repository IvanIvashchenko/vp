package com.perspective.vpng.plugin.calculate_distribution;

import com.perspective.vpng.actor.calculate_distribution.CalculateDistributionActor;
import com.perspective.vpng.actor.calculate_distribution.wrapper.CalculateDistributionActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link com.perspective.vpng.actor.calculate_distribution.CalculateDistributionActor} actor.
 */
public class CalculateDistributionActorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public CalculateDistributionActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link com.perspective.vpng.actor.calculate_distribution.CalculateDistributionActor} actor and registers in <cdoe>IOC</cdoe> with key <code>CalculateDistributionActor</code>.
     * Begin loading plugin after loading plugins: <code>IOC</code> and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading plugin.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("CalculateDistributionActorPlugin");

            item
                    /*.after("IOC")
                    .before("starter")*/
                    .process(() -> {
                        try {
                            IKey actorKey = Keys.getOrAdd(CalculateDistributionActor.class.getCanonicalName());
                            IOC.register(actorKey,
                                    new ApplyFunctionToArgumentsStrategy(
                                            (args) -> {
                                                try {
                                                    IObject params = (IObject) args[0];
                                                    IField servicesFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "servicesFieldName");
                                                    IField distributionsFieldNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "distributionsFieldName");

                                                    return new CalculateDistributionActor(new CalculateDistributionActorConfig() {
                                                        @Override
                                                        public String getServicesFieldName() throws ReadValueException {
                                                            try {
                                                                return servicesFieldNameF.in(params);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                        @Override
                                                        public String getDistributionsFieldName() throws ReadValueException {
                                                            try {
                                                                return distributionsFieldNameF.in(params);
                                                            } catch (InvalidArgumentException ex) {
                                                                throw new ReadValueException(ex.getMessage(), ex);
                                                            }
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    throw new RuntimeException(
                                                            "Can't create calculate distribution actor: " + e.getMessage(), e);
                                                }
                                            }
                                    )
                            );
                        } catch (ResolutionException | InvalidArgumentException | RegistrationException ex) {
                            throw new ActionExecuteException("Calculate distribution plugin can't load: " + ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
