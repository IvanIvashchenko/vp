package com.perspective.vpng.plugin.convert_iobject_to_client_back_url_rule;

import com.perspective.vpng.actor.payment_request.wrapper.ClientBackUrlConfig;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_extension_plugins.configuration_object_plugin.InitializeConfigurationObjectStrategies;
import info.smart_tools.smartactors.iobject_extension_plugins.wds_object_plugin.PluginWDSObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.fieldname_plugin.FieldNamePlugin;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.message_processing_plugins.wrapper_generator_plugin.RegisterWrapperGenerator;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ConvertIObjectToClientBackUrlRulePluginTest {

    private static final Bootstrap bootstrap = new Bootstrap();

    @BeforeClass
    public static void prepareIOC() throws Exception {

        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new FieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new InitializeConfigurationObjectStrategies(bootstrap).load();
        new RegisterWrapperGenerator(bootstrap).load();
        new PluginWDSObject(bootstrap).load();

        new ConvertIObjectToClientBackUrlRulePlugin(bootstrap).load();

        bootstrap.start();
    }

    @Test
    public void shouldResolveWrapper_when_iObjectIsGiven() throws Exception {

        IObject rawConfig = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"appUrl\": \"https://vp.ru/\"," +
                "\"prefix\": \"prefix/\"," +
                "\"actionParamName\": \"action\"," +
                "\"gwSuccess\": \"success\"," +
                "\"requestNumberParamName\": \"request_number\"" +
            "}"
        );
        ClientBackUrlConfig config = IOC.resolve(Keys.getOrAdd("convertIObjectToClientBackUrlRule"), rawConfig);
        assertEquals(config.getAppUrl(), "https://vp.ru/");
        assertEquals(config.getPrefix(), "prefix/");
        assertEquals(config.getActionParamName(), "action");
        assertEquals(config.getGwSuccess(), "success");
        assertEquals(config.getRequestNumberParamName(), "request_number");
    }

    @Test
    public void shouldResolveEmptyWrapper_when_iObjectIsEmpty() throws Exception {

        IObject rawConfig = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{}");
        ClientBackUrlConfig config = IOC.resolve(Keys.getOrAdd("convertIObjectToClientBackUrlRule"), rawConfig);
        assertNull(config.getAppUrl());
        assertNull(config.getPrefix());
        assertNull(config.getActionParamName());
        assertNull(config.getGwSuccess());
        assertNull(config.getRequestNumberParamName());
    }
}
