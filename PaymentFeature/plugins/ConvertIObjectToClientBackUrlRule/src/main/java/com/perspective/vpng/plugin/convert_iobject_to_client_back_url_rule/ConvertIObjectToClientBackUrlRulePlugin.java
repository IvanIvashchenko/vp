package com.perspective.vpng.plugin.convert_iobject_to_client_back_url_rule;

import com.perspective.vpng.actor.payment_request.wrapper.ClientBackUrlConfig;
import com.perspective.vpng.rule.convert_iobject_to_wrapper_rule.ConvertIObjectToWrapperRule;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Collections;

/**
 * Plugin for registration rule for transformation IObject to {@link ClientBackUrlConfig}
 */
public class ConvertIObjectToClientBackUrlRulePlugin extends BootstrapPlugin {

    public ConvertIObjectToClientBackUrlRulePlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("ConvertIObjectToClientBackUrlRulePlugin")
    public void item()
        throws ResolutionException, RegistrationException, InvalidArgumentException {
        IObject wrapperConfig = IOC.resolve(Keys.getOrAdd("configuration object"),
            "{" +
                "   \"in_getAppUrl\" : \"wrapperData/appUrl\"," +
                "   \"in_getPrefix\" : \"wrapperData/prefix\"," +
                "   \"in_getActionParamName\" : \"wrapperData/actionParamName\"," +
                "   \"in_getGwSuccess\" : \"wrapperData/gwSuccess\"," +
                "   \"in_getRequestNumberParamName\" : \"wrapperData/requestNumberParamName\"" +
            "}"
        );

        IOC.register(
            Keys.getOrAdd("convertIObjectToClientBackUrlRule"),
            new ApplyFunctionToArgumentsStrategy(
                (args) -> {
                    try {
                        IResolveDependencyStrategy strategy = new ConvertIObjectToWrapperRule(
                            ClientBackUrlConfig.class,
                            wrapperConfig,
                            Collections.singletonList("wrapperData")
                        );

                        return strategy.resolve(args);
                    } catch (ResolveDependencyStrategyException e) {
                        throw new RuntimeException(e);
                    }
                })
        );
    }
}
