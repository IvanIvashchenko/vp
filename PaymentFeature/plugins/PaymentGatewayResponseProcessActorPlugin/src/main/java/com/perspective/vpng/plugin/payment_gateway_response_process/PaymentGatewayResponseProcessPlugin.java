package com.perspective.vpng.plugin.payment_gateway_response_process;

import com.perspective.vpng.actor.payment_gateway_response_process.PaymentGatewayResponseProcessActor;
import com.perspective.vpng.actor.payment_gateway_response_process.wrapper.IActorParams;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link PaymentGatewayResponseProcessActor} actor.
 */
public class PaymentGatewayResponseProcessPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public PaymentGatewayResponseProcessPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link PaymentGatewayResponseProcessActor} actor and
     *              registers in <code>IOC</code>.
     * Begin loads actor after loading plugins: <code>IOC</code>, <code>starter</code>
     *              and before: <code>read_config</code>.
     *
     * @throws PluginException when errors in loading actor.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> createPaymentRequest = new BootstrapItem("PaymentGatewayResponseProcessPlugin");

            createPaymentRequest
                    .process(() -> {
                        try {
                            IField payPaymentRuleNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "payPaymentRuleName");
                            IField payPaymentStrategyNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "payPaymentStrategyName");
                            IField payPaymentRuleFieldNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "payPaymentRuleFieldName");
                            IField checkPaymentPaidStatusRuleNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "checkPaymentPaidStatusRuleName");
                            IField checkPaymentPaidStatusStrategyNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "checkPaymentPaidStatusStrategyName");
                            IField checkPaymentPaidStatusRuleF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "checkPaymentPaidStatusRuleFieldName");
                            IField paymentNumberF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentNumberFieldName");
                            IField invoicesF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoicesFieldName");
                            IField guidF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "guidFieldName");
                            IField dateF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "dateFieldName");
                            IField asyncOperationCollectionNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "asyncOperationCollectionName");
                            IField operationIdFieldNameNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "operationIdFieldName");
                            IOC.register(Keys.getOrAdd("PaymentGatewayResponseProcessActor"),
                                    new ApplyFunctionToArgumentsStrategy(
                                            (args) -> {
                                                try {
                                                    IObject params = (IObject) args[0];
                                                    return new PaymentGatewayResponseProcessActor(new IActorParams() {
                                                        @Override
                                                        public String getPayPaymentRuleName() throws ReadValueException {
                                                            try {
                                                                return payPaymentRuleNameF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getPayPaymentStrategyName() throws ReadValueException {
                                                            try {
                                                                return payPaymentStrategyNameF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getPayPaymentRuleFieldName() throws ReadValueException {
                                                            try {
                                                                return payPaymentRuleFieldNameF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getCheckPaymentPaidStatusRuleName() throws ReadValueException {
                                                            try {
                                                                return checkPaymentPaidStatusRuleNameF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getCheckPaymentPaidStatusStrategyName() throws ReadValueException {
                                                            try {
                                                                return checkPaymentPaidStatusStrategyNameF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getCheckPaymentPaidStatusRuleFieldName() throws ReadValueException {
                                                            try {
                                                                return checkPaymentPaidStatusRuleF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getPaymentNumberFieldName() throws ReadValueException {
                                                            try {
                                                                return paymentNumberF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getInvoicesFieldName() throws ReadValueException {
                                                            try {
                                                                return invoicesF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getGuidFieldName() throws ReadValueException {
                                                            try {
                                                                return guidF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getDateFieldName() throws ReadValueException {
                                                            try {
                                                                return dateF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getAsyncOperationCollectionName() throws ReadValueException {
                                                            try {
                                                                return asyncOperationCollectionNameF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }

                                                        @Override
                                                        public String getOperationIdFieldName() throws ReadValueException {
                                                            try {
                                                                return operationIdFieldNameNameF.in(params);
                                                            } catch (InvalidArgumentException e) {
                                                                throw new ReadValueException(e);
                                                            }
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    throw new RuntimeException(e);
                                                }
                                            }));
                        } catch (ResolutionException | RegistrationException | InvalidArgumentException e) {
                            throw new ActionExecuteException(e);
                        }
                    });
            bootstrap.add(createPaymentRequest);
        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't load CreatePaymentRequest plugin", e);
        }
    }
}
