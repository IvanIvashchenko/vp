package com.perspective.vpng.plugin.payment_result_process;

import com.perspective.vpng.strategy.payment_result_process.PaymentResultProcessStrategy;
import com.perspective.vpng.strategy.payment_result_process.ResultPaymentProcessor;
import com.perspective.vpng.vsep_payment_result_process.VsepPaymentResultProcessRule;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Plugin for register {@link PaymentResultProcessStrategy} rule.
 */
public class PaymentResultProcessPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public PaymentResultProcessPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Prepares rule map in which the key is rule name and value implementation of
     * {@link ResultPaymentProcessor} and set it to {@link PaymentResultProcessStrategy} and registers that rule
     * in <code>IOC</code> with key <code>paidPaymentCollection</code>.
     * Begin register rule after loading plugins: <code>IOC</code>, <code>wds_object</code>
     *              and before: <code>starter</code>.
     *
     * @throws PluginException when errors in register rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("PaymentResultProcessPlugin");
            item
                    /*.after("IOC")
                    .after("wds_object")
                    .before("starter")*/
                    .process(() -> {
                        try {

                            Map<String, ResultPaymentProcessor> strategies = new HashMap<>();
                            strategies.put(
                                    "vsep",
                                    new VsepPaymentResultProcessRule()
                            );
                            IOC.register(
                                    Keys.getOrAdd("paymentResultProcessStrategy"),
                                    new PaymentResultProcessStrategy(strategies)
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
