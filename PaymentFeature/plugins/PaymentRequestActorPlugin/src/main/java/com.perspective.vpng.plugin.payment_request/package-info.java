/**
 * Contains plugin to upload the {@link com.perspective.vpng.actor.payment_request.PaymentRequestActor}
 */
package com.perspective.vpng.plugin.payment_request;