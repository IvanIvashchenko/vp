package com.perspective.vpng.plugin.payment_request;

import com.perspective.vpng.actor.payment_request.PaymentRequestActor;
import com.perspective.vpng.actor.payment_request.wrapper.ClientBackUrlConfig;
import com.perspective.vpng.actor.payment_request.wrapper.IPaymentRequestActorParam;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link PaymentRequestActor} actor.
 */
public class PaymentRequestPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public PaymentRequestPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link PaymentRequestActor} actor and
     *              registers in <code>IOC</code>.
     * Begin loads actor after loading plugins: <code>IOC</code>, <code>starter</code>
     *              and before: <code>read_config</code>.
     *
     * @throws PluginException when errors in loading actor.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> createPaymentRequest = new BootstrapItem("PaymentRequestActorPlugin");

            createPaymentRequest
//                    .after("IOC")
//                    .before("starter")
//                    .before("read_config")
                    .process(() -> {
                        try {
                            IField gwRequestBuilderStrategyNameF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "gwRequestBuildStrategyName");
                            IField clientBackUrlParamsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "clientBackUrlParams");
                            IOC.register(Keys.getOrAdd("PaymentRequestActor"),
                                    new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        try {
                                            IObject params = (IObject) args[0];
                                            return new PaymentRequestActor(new IPaymentRequestActorParam() {
                                                @Override
                                                public String getGwRequestBuildStrategyName() throws ReadValueException {
                                                    try {
                                                        return gwRequestBuilderStrategyNameF.in(params);
                                                    } catch (InvalidArgumentException e) {
                                                        throw new ReadValueException(e);
                                                    }
                                                }

                                                @Override
                                                public ClientBackUrlConfig getClientBackUrlParams() throws ReadValueException {
                                                    try {
                                                        IObject clientBackUrlParams = clientBackUrlParamsF.in(params);
                                                        return IOC.resolve(
                                                            Keys.getOrAdd("convertIObjectToClientBackUrlRule"), clientBackUrlParams
                                                        );
                                                    } catch (InvalidArgumentException | ResolutionException e) {
                                                        throw new ReadValueException(e);
                                                    }
                                                }
                                            });
                                        } catch (Exception e) {
                                            throw new RuntimeException(e);
                                        }
                                    }));
                        } catch (ResolutionException | RegistrationException | InvalidArgumentException e) {
                            throw new ActionExecuteException(e);
                        }
                    });
            bootstrap.add(createPaymentRequest);

        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't load PaymentRequest plugin", e);
        }
    }


}
