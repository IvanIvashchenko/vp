package com.perspective.vpng.plugin.payment_success_process;

import com.perspective.vpng.rule.vsep_payment_success_processor.VsepPaymentSuccessProcessor;
import com.perspective.vpng.strategy.payment_success_process.PaymentSuccessProcessStrategy;
import com.perspective.vpng.strategy.payment_success_process.PaymentSuccessProcessor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Plugin for register {@link PaymentSuccessProcessStrategy} rule.
 */
public class PaymentSuccessProcessPlugin implements IPlugin {

    private static final String ORDER_ID_FIELD_NAME = "orderId";

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public PaymentSuccessProcessPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Prepares rule map in which the key is rule name and value implementation of
     * {@link PaymentSuccessProcessor} and set it to {@link PaymentSuccessProcessStrategy} and registers that rule
     * in <code>IOC</code> with key <code>paidPaymentCollection</code>.
     * Begin register rule after loading plugins: <code>IOC</code>
     *              and before: <code>starter</code>.
     *
     * @throws PluginException when errors in register rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("PaymentSuccessProcessPlugin");
            item
                    /*.after("IOC")
                    .before("starter")*/
                    .process(() -> {
                        try {

                            Map<String, PaymentSuccessProcessor> strategies = new HashMap<>();
                            strategies.put(
                                    "vsep",
                                    new VsepPaymentSuccessProcessor(ORDER_ID_FIELD_NAME)
                            );
                            IOC.register(
                                    Keys.getOrAdd("paymentSuccessProcessStrategy"),
                                    new PaymentSuccessProcessStrategy(strategies)
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
