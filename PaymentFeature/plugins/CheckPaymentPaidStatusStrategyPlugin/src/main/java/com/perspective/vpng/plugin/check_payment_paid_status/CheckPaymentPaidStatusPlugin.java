package com.perspective.vpng.plugin.check_payment_paid_status;

import com.perspective.vpng.rule.check_payment_paid_status_default.CheckPaymentPaidStatusDefault;
import com.perspective.vpng.strategy.check_payment_paid_status.CheckPaymentPaidStatusProcessor;
import com.perspective.vpng.strategy.check_payment_paid_status.CheckPaymentPaidStatusStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

/**
 * Plugin for register {@link CheckPaymentPaidStatusStrategy} rule.
 */
public class CheckPaymentPaidStatusPlugin implements IPlugin {

    private static final String CONNECTION_OPTIONS_KEY = "PostgresConnectionOptions";
    private static final String CONNECTION_POOL_KEY = "PostgresConnectionPool";
    private static final String PAID_PAYMENT_COLLECTION_NAME = "payment";

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public CheckPaymentPaidStatusPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Prepares rule map in which the key is rule name and value implementation of
     * {@link CheckPaymentPaidStatusDefault} and set it to {@link CheckPaymentPaidStatusStrategy} and registers that
     * rule in <code>IOC</code> with key <code>paidPaymentCollection</code>.
     * Begin register rule after loading plugins: <code>IOC</code>, <code>wds_object</code>
     *              and before: <code>starter</code>.
     *
     * @throws PluginException when errors in register rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("CheckPaymentPaidStatusPlugin");
            item
//                    .after("IOC")
//                    .after("wds_object")
//                    .before("starter")
                    .process(() -> {
                        try {
                            Object connectionOptions = IOC.resolve(Keys.getOrAdd(CONNECTION_OPTIONS_KEY));
                            IPool connectionPool = IOC.resolve(Keys.getOrAdd(CONNECTION_POOL_KEY), connectionOptions);
                            Map<String, CheckPaymentPaidStatusProcessor> strategies = new HashMap<>();
                            strategies.put(
                                    "default",
                                    new CheckPaymentPaidStatusDefault(
                                            PAID_PAYMENT_COLLECTION_NAME,
                                            connectionPool,
                                            "operationId"
                                    )
                            );
                            IOC.register(
                                    Keys.getOrAdd("checkPaymentPaidStatusStrategy"),
                                    new CheckPaymentPaidStatusStrategy(strategies)
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
