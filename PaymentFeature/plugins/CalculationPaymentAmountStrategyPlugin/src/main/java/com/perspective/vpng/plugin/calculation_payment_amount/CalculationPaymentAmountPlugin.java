package com.perspective.vpng.plugin.calculation_payment_amount;

import com.perspective.vpng.strategy.calculation_payment_amount.CalculationPaymentAmountStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for register {@link CalculationPaymentAmountStrategy} rule.
 */
public class CalculationPaymentAmountPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    private static final String INVOICE_INCLUDED_FN = "included";
    private static final String INVOICE_SUM_FN = "всего-к-оплате";

    /**
     * Default constructor.
     * @param bootstrap bootstrap element.
     */
    public CalculationPaymentAmountPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link CalculationPaymentAmountStrategy} rule and
     *              registers in <code>IOC</code> with key <code>calculatePaymentAmountStrategy</code>.
     * Begin loads rule after loading plugins: <code>IOC</code>, <code>wds_object</code>
     *              and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("CalculationPaymentAmountPlugin");
            item
                    /*.after("IOC")
                    .after("wds_object")
                    .before("starter")*/
                    .process(() -> {
                        try {
                            IOC.register(
                                    Keys.getOrAdd("calculatePaymentAmountStrategy"),
                                    new CalculationPaymentAmountStrategy(INVOICE_SUM_FN, INVOICE_INCLUDED_FN)
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException("CalculationPaymentAmountPlugin plugin can't load: " +
                                    "can't get CalculatePaymentAmountStrategy key", ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
