package com.perspective.vpng.plugin.determine_active_gateway;

import com.perspective.vpng.rule.determine_active_gateway.DetermineActiveGatewayRule;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link DetermineActiveGatewayRule} rule.
 */
public class DetermineActiveGatewayPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    private static final String DETERMINE_ACTIVE_GATEWAY_DEPENDENCY_NAME = "determineActiveGateway";

    private static final String PAYMENT_METHOD_MATRIX_COLLECTION_NAME = "paymentMethodMatrixCollection";
    private static final String METHOD_MATRIX_COLLECTION_KEY_FIELD_NAME = "type";
    private static final String MATRIX_KIND_ITEM_NAME_FIELD_NAME = "name";
    private static final String GW_ACTIVITY_FIELD_NAME = "activity";
    private static final String MATRIX_KINDS_FIELD_NAME = "kinds";
    private static final String MATRIX_GATEWAYS_FIELD_NAME = "gateways";

    /**
     * Default constructor
     * @param bootstrap element
     */
    public DetermineActiveGatewayPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link DetermineActiveGatewayRule} rule and
     *              registers in <code>IOC</code> with key <code>determineActiveGateway</code>.
     * Begin loads rule after loading plugins: <code>IOC</code>, <code>wds_object</code>
     *              and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("DetermineActiveGatewayPlugin");
            item
/*                    .after("IOC")
                    .after("wds_object")
                    .before("starter")*/
                    .process(() -> {
                        try {
                            IOC.resolve(
                                    Keys.getOrAdd(IResolveDependencyStrategy.class.getCanonicalName()),
                                    DETERMINE_ACTIVE_GATEWAY_DEPENDENCY_NAME,
                                    new DetermineActiveGatewayRule(
                                            PAYMENT_METHOD_MATRIX_COLLECTION_NAME,
                                            METHOD_MATRIX_COLLECTION_KEY_FIELD_NAME,
                                            MATRIX_KIND_ITEM_NAME_FIELD_NAME,
                                            GW_ACTIVITY_FIELD_NAME,
                                            MATRIX_KINDS_FIELD_NAME,
                                            MATRIX_GATEWAYS_FIELD_NAME
                                    )
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException("DetermineActiveGatewayPlugin can't load: " +
                                    "can't get DetermineActiveGatewayRule key", ex
                            );
                        } catch (ResolveDependencyStrategyException ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
