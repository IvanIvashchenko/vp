package com.perspective.vpng.plugin.payment;

import com.perspective.vpng.actor.payment.PaymentActor;
import com.perspective.vpng.actor.payment.wrapper.IPaymentActorParam;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for loading {@link PaymentActor} actor.
 */
public class PaymentActorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public PaymentActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link PaymentActor} actor and
     *              registers in <code>IOC</code>.
     * Begin loads actor after loading plugins: <code>IOC</code>, <code>starter</code>
     *              and before: <code>read_config</code>.
     *
     * @throws PluginException when errors in loading actor.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> makePayment = new BootstrapItem("PaymentPlugin");

            makePayment
//                    .after("IOC")
//                    .before("starter")
//                    .before("read_config")
                    .process(() -> {
                        try {
                            IField calculatePaymentSumName =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "calculatePaymentAmountStrategy");
                            IField paymentKindDeterminationStrategyF =
                                    IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentKindDeterminationStrategy");
                            IField collectionNameF =
                                IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
                            IField initialSequenceValueF =
                                IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "initialSequenceValue");
                            IOC.register(Keys.getOrAdd("PaymentActor"), new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        try {
                                            IObject params = (IObject) args[0];
                                            return new PaymentActor(new IPaymentActorParam() {
                                                @Override
                                                public String getCalculatePaymentAmountStrategy() throws ReadValueException {
                                                    try {
                                                        return calculatePaymentSumName.in(params);
                                                    } catch (InvalidArgumentException e) {
                                                        throw new ReadValueException(e.getMessage(), e);
                                                    }
                                                }

                                                @Override
                                                public String getPaymentKindDeterminationStrategy() throws ReadValueException {
                                                    try {
                                                        return paymentKindDeterminationStrategyF.in(params);
                                                    } catch (InvalidArgumentException e) {
                                                        throw new ReadValueException(e.getMessage(), e);
                                                    }
                                                }

                                                @Override
                                                public String getCollectionName() throws ReadValueException {
                                                    try {
                                                        return collectionNameF.in(params);
                                                    } catch (InvalidArgumentException e) {
                                                        throw new ReadValueException(e.getMessage(), e);
                                                    }
                                                }

                                                @Override
                                                public Integer getInitialSequenceValue() throws ReadValueException {
                                                    try {
                                                        return initialSequenceValueF.in(params);
                                                    } catch (InvalidArgumentException e) {
                                                        throw new ReadValueException(e.getMessage(), e);
                                                    }
                                                }
                                            });
                                        } catch (Exception e) {
                                            throw new RuntimeException(e);
                                        }
                                    }));

                        } catch (ResolutionException | RegistrationException | InvalidArgumentException e) {
                            throw new ActionExecuteException(String
                                    .format("%s plugin can't load!", this.getClass().getCanonicalName()), e);
                        }
                    });
            bootstrap.add(makePayment);

        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't get BootstrapItem from one of reason: ", e);
        }
    }
}
