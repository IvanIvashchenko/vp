package com.perspective.vpng.rule.hmac_message_authentication_code;

import com.perspective.vpng.strategy.message_authentication_code.MACSigning;
import com.perspective.vpng.strategy.message_authentication_code.exception.MACSigningException;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Implementation of hmac message authentication code algorithm
 */
public class HmacMessageAuthenticationCode implements MACSigning {

    /**
     * Create mac for given text.
     *
     * @param text text to be signed.
     * @param signAlgorithm type of sign algorithm.
     * @param encoding result value encoding.
     * @param key for make MAC needed symmetric.
     * @return message authentication code as string.
     * @throws MACSigningException when some error occurred.
     */
    @Override
    public String sign(String text, String signAlgorithm, String encoding, String key) throws MACSigningException {
        try {
            Mac mac = Mac.getInstance(signAlgorithm);
            SecretKeySpec signingKey = new SecretKeySpec(Hex.decodeHex(key.toCharArray()), mac.getAlgorithm());
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(text.getBytes());
            byte[] hexBytes = new Hex().encode(rawHmac);
            return new String(hexBytes, encoding);
        } catch (Exception e) {
            throw new MACSigningException("Can't make signature!", e);
        }
    }

}
