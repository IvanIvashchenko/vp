package com.perspective.vpng.rule.hmac_message_authentication_code;

import com.perspective.vpng.strategy.message_authentication_code.MACSigning;
import com.perspective.vpng.strategy.message_authentication_code.exception.MACSigningException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HmacMessageAuthenticationCodeTest {

    @Test
    public void signMessageTest() throws Exception {
        String textToSign = "Text For signing7";

        String signAlgorithm = "HmacSHA1";
        String encoding = "UTF-8";
        String key = "a12ec87101e0323a2e14305d690795646315550d";
        String expectedResult = "b40969c28b1bbc9e9102e175f23512e6d01b6e65";

        MACSigning macSigning = new HmacMessageAuthenticationCode();
        String signedData = macSigning.sign(textToSign, signAlgorithm, encoding, key);
        assertEquals(expectedResult, signedData);
    }

    @Test(expected = MACSigningException.class)
    public void should_ThrowException_When_signMessage() throws Exception {
        String textToSign = "Text For signing7";

        String signAlgorithm = "";
        String encoding = "";
        String key = "";
        MACSigning macSigning = new HmacMessageAuthenticationCode();
        macSigning.sign(textToSign, signAlgorithm, encoding, key);
    }
}