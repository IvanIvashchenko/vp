package com.perspective.vpng.rule.convert_iobject_to_active_gateway;

import com.perspective.vpng.actor.payment_request.wrapper.IGatewayProperty;
import com.perspective.vpng.rule.convert_iobject_to_wrapper_rule.ConvertIObjectToWrapperRule;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.iobject_extension.configuration_object.ConfigurationObject;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Collections;

/**
 * Try converts gateway property as {@link IObject} to {@link IGatewayProperty} wrapper.
 */
public class ConvertIObjectToActiveGatewayRule implements IPlugin {
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ConvertIObjectToActiveGatewayRule(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link ConvertIObjectToWrapperRule} for {@link IGatewayProperty} wrapper and
     * registers in <code>IOC</code> with key <code>convertIObjectToActiveGatewayRule</code>.
     * Begin loads after loading plugins: <code>IOC</code>, <code>wds_object</code> and
     * before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading rule.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ConvertIObjectToActiveGatewayRulePlugin");
            item
//                    .after("IOC")
//                    .after("wds_object")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IObject wrapperConfig = new ConfigurationObject(
                                    "{" +
                                            "   \"in_getGwType\" : \"wrapperData/gwType\"," +
                                            "   \"in_getTerminalType\" : \"wrapperData/terminalType\"," +
                                            "   \"in_getActivity\" : \"wrapperData/activity\"" +
                                            "}"
                            );

                            IOC.resolve(
                                    Keys.getOrAdd(IResolveDependencyStrategy.class.getCanonicalName()),
                                    "convertIObjectToActiveGatewayRule",
                                    new ConvertIObjectToWrapperRule(
                                            IGatewayProperty.class,
                                            wrapperConfig,
                                            Collections.singletonList("wrapperData")
                                    )
                            );
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException(String.format("%s plugin can't load: " +
                                            "can't get %s key", this.getClass().getCanonicalName(),
                                    ConvertIObjectToWrapperRule.class.getCanonicalName()), ex
                            );
                        } catch (Exception ex) {
                            throw new ActionExecuteException(ex.getMessage(), ex);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }

}
