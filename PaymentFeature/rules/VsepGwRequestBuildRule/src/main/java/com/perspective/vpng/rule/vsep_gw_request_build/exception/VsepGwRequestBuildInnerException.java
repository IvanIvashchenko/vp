package com.perspective.vpng.rule.vsep_gw_request_build.exception;

/**
 * Exception for error in {@link com.perspective.vpng.rule.vsep_gw_request_build.VsepGwRequestBuildRule}
 * when throw some inner error.
 */
public class VsepGwRequestBuildInnerException extends RuntimeException {

    public VsepGwRequestBuildInnerException(String message) {
        super(message);
    }

    public VsepGwRequestBuildInnerException(String message, Throwable cause) {
        super(message, cause);
    }

}
