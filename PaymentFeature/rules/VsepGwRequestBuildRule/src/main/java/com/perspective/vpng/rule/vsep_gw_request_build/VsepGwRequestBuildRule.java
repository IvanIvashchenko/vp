package com.perspective.vpng.rule.vsep_gw_request_build;

import com.perspective.vpng.actor.payment_request.wrapper.ClientBackUrlConfig;
import com.perspective.vpng.strategy.build_gw_request.IGwRequestBuilder;
import com.perspective.vpng.strategy.build_gw_request.exception.BuildGatewayRequestException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Build request parameters for VSEP gateway.
 */
public class VsepGwRequestBuildRule implements IGwRequestBuilder {

    // TODO: 13.09.16 write test for this

    private static final String OPERATION_ID_IN_F_NAME = "operationId";
    private static final String PAYMENT_URL_IN_F_NAME = "paymentUrl";
    private static final String SUM_IN_F_NAME = "sum";
    private static final String USER_GUID_IN_F_NAME = "userGuid";
    private static final String DESCRIPTION_IN_F_NAME = "description";
    private static final String TERMINAL_TYPE_IN_F_NAME = "terminalType";

    private static final String PAYMENT_URL_OUT_F_NAME = "paymentUrl";
    private static final String REQUEST_NUMBER_OUT_F_NAME = "orderId";
    private static final String TERMINAL_OUT_F_NAME = "terminal";
    private static final String MERCHANT_OUT_F_NAME = "merchant";
    private static final String AMOUNT_OUT_F_NAME = "amount";
    private static final String USER_ID_OUT_F_NAME = "userid";
    private static final String DESCRIPTION_OUT_F_NAME = "description";
    private static final String BACK_URL_OUT_F_NAME = "clientBackUrl";
    private static final String DETAIL_OUT_F_NAME = "detail";
    private static final String SIGN_OUT_F_NAME = "sign";
    private static final String GETAWAY_SIGN_OUT_F_NAME = "getawaySign";

    private final ClientBackUrlConfig clientBackUrlConfig;

    public VsepGwRequestBuildRule(final ClientBackUrlConfig clientBackUrlParams) {
        this.clientBackUrlConfig = clientBackUrlParams;
    }

    /**
     * Create gateway parameters for VSEP as IObject
     *
     * @param payment      - payment as IObject
     * @param gwProperties - property for VSEP as IObject
     * @return request parameters wrapped in IObject
     */
    @Override
    public IObject build(IObject payment, IObject gwProperties) throws BuildGatewayRequestException {
        try {
            VsepRequestParamWrapper requestParamWrapper = fillWrapper(payment, gwProperties);
            return makeRequestObj(requestParamWrapper);
        } catch (ResolutionException | InvalidArgumentException |
                ChangeValueException e) {
            throw new BuildGatewayRequestException("Can't create VSEP gateway payment request parameters!", e);
        }
    }

    private IObject makeRequestObj(VsepRequestParamWrapper requestParamWrapper)
            throws ChangeValueException, InvalidArgumentException, ResolutionException, BuildGatewayRequestException {

        IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IObject paramDetailObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        IField vsepPaymentUrlFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), PAYMENT_URL_OUT_F_NAME);
        IField operationIdFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), REQUEST_NUMBER_OUT_F_NAME);
        IField terminalFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), TERMINAL_OUT_F_NAME);
        IField merchantFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), MERCHANT_OUT_F_NAME);
        IField amountFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), AMOUNT_OUT_F_NAME);
        IField useridFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), USER_ID_OUT_F_NAME);
        IField descriptionFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), DESCRIPTION_OUT_F_NAME);
        IField backUrlFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), BACK_URL_OUT_F_NAME);
        IField signFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SIGN_OUT_F_NAME);
        IField getawaySignFOut = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), GETAWAY_SIGN_OUT_F_NAME);
        IField detailField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), DETAIL_OUT_F_NAME);

        vsepPaymentUrlFOut.out(result, requestParamWrapper.getPaymentUrl());
        operationIdFOut.out(result, requestParamWrapper.getRequestNumber());
        terminalFOut.out(paramDetailObj, requestParamWrapper.getTerminal());
        merchantFOut.out(paramDetailObj, requestParamWrapper.getMerchant());
        amountFOut.out(paramDetailObj, requestParamWrapper.getAmount());
        useridFOut.out(paramDetailObj, requestParamWrapper.getUserId());
        descriptionFOut.out(paramDetailObj, requestParamWrapper.getDescription());
        backUrlFOut.out(paramDetailObj, requestParamWrapper.getBackUrl());
        signFOut.out(paramDetailObj, makeSignature(requestParamWrapper));
        getawaySignFOut.out(paramDetailObj, makeGetawaySignature(requestParamWrapper));
        detailField.out(result, paramDetailObj);
        return result;
    }

    private VsepRequestParamWrapper fillWrapper(IObject payment, IObject gwProperties) throws BuildGatewayRequestException {
        try {
            IField operationIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), OPERATION_ID_IN_F_NAME);
            IField paymentUrlF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), PAYMENT_URL_IN_F_NAME);
            IField paymentAmountF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SUM_IN_F_NAME);
            IField userGuidF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), USER_GUID_IN_F_NAME);
            IField descriptionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), DESCRIPTION_IN_F_NAME);
            IField terminalTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), TERMINAL_TYPE_IN_F_NAME);

            IField signAlgorithmTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "signAlgorithmType");
            IField signAlgorithmNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "signAlgorithmName");
            IField signEncodingF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "encoding");
            IField signKeyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "key");
            IObject signRequisitesObj = retrieveSignRequisitesObject(gwProperties);

            String terminalType = terminalTypeF.in(payment);
            String requestNumber = operationIdF.in(payment);
            return new VsepRequestParamWrapper(
                    requestNumber,
                    paymentAmountF.in(payment, BigDecimal.class),
                    createBackRefUrl(requestNumber),
                    retrieveTerminalCodeFromGwProperty(gwProperties, terminalType),
                    retrieveMerchantCodeFromGwProperty(gwProperties, terminalType),
                    userGuidF.in(payment),
                    descriptionF.in(payment),
                    paymentUrlF.in(gwProperties),
                    signAlgorithmTypeF.in(signRequisitesObj),
                    signAlgorithmNameF.in(signRequisitesObj),
                    signEncodingF.in(signRequisitesObj),
                    signKeyF.in(signRequisitesObj)
            );
        } catch (InvalidArgumentException | ResolutionException | ReadValueException e) {
            throw new BuildGatewayRequestException("Can't fill inner wrapper", e);
        }
    }

    private IObject retrieveSignRequisitesObject(IObject gwProperties) throws BuildGatewayRequestException {
        try {
            IField signObjF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sign");
            return signObjF.in(gwProperties, IObject.class);
        } catch (ResolutionException | ReadValueException | InvalidArgumentException e) {
            throw new BuildGatewayRequestException("Can't retrieve sign requisite from gateway property!", e);
        }
    }

    private String retrieveMerchantCodeFromGwProperty(IObject gwProperties, String terminalType)
            throws ResolutionException, ReadValueException, InvalidArgumentException, BuildGatewayRequestException {

        IField merchantsField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "merchants");
        List<IObject> merchants = merchantsField.in(gwProperties, List.class);
        IField merchantCodeField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "code");
        IField merchantIdentifierField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "identifier");

        Optional<IObject> merchantItemOpt = find(merchants, merchantIdentifierField, terminalType);

        IObject merchantObj = merchantItemOpt.orElseThrow(() ->
                new BuildGatewayRequestException(String
                        .format("Can't found merchant by identifier '%s'", terminalType)));

        return merchantCodeField.in(merchantObj, String.class);
    }

    private String retrieveTerminalCodeFromGwProperty(IObject gwProperties, String terminalType)
            throws ResolutionException, ReadValueException, InvalidArgumentException, BuildGatewayRequestException {

        IField terminalsField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "terminals");
        List<IObject> terminals = terminalsField.in(gwProperties, List.class);
        IField terminalCodeField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "code");
        IField terminalIdentifierField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "identifier");

        Optional<IObject> terminalItrmOpt = find(terminals, terminalIdentifierField, terminalType);

        IObject terminalObj = terminalItrmOpt.orElseThrow(() ->
                new BuildGatewayRequestException(String
                        .format("Can't found terminal by identifier '%s'", terminalType)));

        return terminalCodeField.in(terminalObj, String.class);
    }

    private Optional<IObject> find(List<IObject> list, IField fieldName, String valueForEqual) throws BuildGatewayRequestException {
        Stream<IObject> listStream = list.stream();
        try {
            Optional<IObject> itemOpt = listStream.filter(item -> {
                try {
                    String value = fieldName.in(item);
                    return (null != value && !value.isEmpty()) && value.equals(valueForEqual);
                } catch (ReadValueException | InvalidArgumentException ex) {
                    throw new RuntimeException(ex.getMessage(), ex);
                }
            }).findFirst();
            return itemOpt;
        } catch (RuntimeException ex) {
            throw new BuildGatewayRequestException(ex.getMessage(), ex);
        }
    }

    private String makeSignature(VsepRequestParamWrapper requestParamWrapper) throws BuildGatewayRequestException {
        try {

            IObject param = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

            IField signAlgorithmTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "signAlgorithmType");
            IField signAlgorithmNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "signAlgorithmName");
            IField encodingF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "encoding");
            IField keyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "key");
            IField rawTextF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "text");

            signAlgorithmTypeF.out(param, requestParamWrapper.getSignAlgorithmType());
            signAlgorithmNameF.out(param, requestParamWrapper.getSignAlgorithmName());
            encodingF.out(param, requestParamWrapper.getSignEncoding());
            keyF.out(param, requestParamWrapper.getSignKey());
            rawTextF.out(param, requestParamWrapper.joinForSign());

            return IOC.resolve(
                    Keys.getOrAdd("messageAuthenticationCodeStrategy"),
                    param
            );
        } catch (ResolutionException | InvalidArgumentException | ChangeValueException e) {
            throw new BuildGatewayRequestException("Can't make signature!", e);
        }
    }

    private String makeGetawaySignature(VsepRequestParamWrapper requestParamWrapper) throws BuildGatewayRequestException {
        try {

            IObject param = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

            IField signAlgorithmTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "signAlgorithmType");
            IField signAlgorithmNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "signAlgorithmName");
            IField encodingF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "encoding");
            IField keyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "key");
            IField rawTextF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "text");

            signAlgorithmTypeF.out(param, requestParamWrapper.getSignAlgorithmType());
            signAlgorithmNameF.out(param, requestParamWrapper.getSignAlgorithmName());
            encodingF.out(param, requestParamWrapper.getSignEncoding());
            keyF.out(param, requestParamWrapper.getSignKey());
            rawTextF.out(param, requestParamWrapper.joinForGatewaySign());

            return IOC.resolve(
                    Keys.getOrAdd("messageAuthenticationCodeStrategy"),
                    param
            );
        } catch (ResolutionException | InvalidArgumentException | ChangeValueException e) {
            throw new BuildGatewayRequestException("Can't make signature!", e);
        }
    }

    private String createBackRefUrl(String requestNumber) throws ReadValueException {
        return clientBackUrlConfig.getAppUrl() + String.format("%s?%s=%s&%s=%s",
                clientBackUrlConfig.getPrefix(),
                clientBackUrlConfig.getActionParamName(),
                clientBackUrlConfig.getGwSuccess(),
                clientBackUrlConfig.getRequestNumberParamName(),
                requestNumber
        );
    }

}
