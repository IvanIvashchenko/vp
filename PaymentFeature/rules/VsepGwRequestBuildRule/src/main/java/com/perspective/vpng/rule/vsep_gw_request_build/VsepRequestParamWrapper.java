package com.perspective.vpng.rule.vsep_gw_request_build;

import java.math.BigDecimal;

/**
 * Wrapper for usability. Encapsulate some parameters.
 */
public class VsepRequestParamWrapper {

    private String requestNumber;
    private BigDecimal amount;
    private String backUrl;
    private String terminal;
    private String merchant;
    private String userId;
    private String description;
    private String paymentUrl;
    private String signAlgorithmType;
    private String signAlgorithmName;
    private String signEncoding;
    private String signKey;

    public VsepRequestParamWrapper(
            String requestNumber,
            BigDecimal amount,
            String backUrl,
            String terminal,
            String merchant,
            String userId,
            String description,
            String paymentUrl,
            String signAlgorithmType,
            String signAlgorithmName,
            String signEncoding,
            String signKey
    ) {
        this.requestNumber = requestNumber;
        this.amount = amount;
        this.backUrl = backUrl;
        this.terminal = terminal;
        this.merchant = merchant;
        this.userId = userId;
        this.description = description;
        this.paymentUrl = paymentUrl;
        this.signAlgorithmType = signAlgorithmType;
        this.signAlgorithmName = signAlgorithmName;
        this.signEncoding = signEncoding;
        this.signKey = signKey;
    }

    public String joinForSign() {
        StringBuilder sb = new StringBuilder();
        sb.append(getPartForSignature(this.requestNumber));
        sb.append(getPartForSignature(this.amount.toString()));
        sb.append(getPartForSignature(this.backUrl));
        sb.append(getPartForSignature(this.terminal));
        sb.append(getPartForSignature(this.merchant));
        sb.append(getPartForSignature(this.userId));
        sb.append(getPartForSignature(this.description));
        return sb.toString();
    }

    public String joinForGatewaySign() {
        StringBuilder sb = new StringBuilder();
        sb.append(getPartForSignature(this.requestNumber));
        sb.append(getPartForSignature(this.amount.toString()));
        sb.append(getPartForSignature(this.merchant));
        sb.append(getPartForSignature(this.terminal));
        return sb.toString();
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public String getTerminal() {
        return terminal;
    }

    public String getMerchant() {
        return merchant;
    }

    public String getUserId() {
        return userId;
    }

    public String getDescription() {
        return description;
    }

    public String getPaymentUrl() {
        return paymentUrl;
    }

    public String getSignAlgorithmType() {
        return signAlgorithmType;
    }

    public String getSignAlgorithmName() {
        return signAlgorithmName;
    }

    public String getSignEncoding() {
        return signEncoding;
    }

    public String getSignKey() {
        return signKey;
    }

    private String getPartForSignature(String s) {
        return s.length() > 0 ? s.length() + s : "-";
    }

}
