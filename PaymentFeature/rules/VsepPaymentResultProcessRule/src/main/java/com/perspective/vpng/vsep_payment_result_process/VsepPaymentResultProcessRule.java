package com.perspective.vpng.vsep_payment_result_process;

import com.perspective.vpng.strategy.payment_result_process.ResultPaymentProcessor;
import com.perspective.vpng.strategy.payment_result_process.exception.IncorrectSignatureException;
import com.perspective.vpng.strategy.payment_result_process.exception.ResultPaymentProcessException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Implementation of result payment behavior for VSEP gateway.
 * Performs some checks under params incoming from gateway result payment request. Use before pay payment.
 *
 * @see ResultPaymentProcessor
 */
public class VsepPaymentResultProcessRule implements ResultPaymentProcessor {

    private static final String GETAWAY_SIGN_FN = "getawaySign";
    private static final String SIGN_FN = "sign";
    private static final String REQUEST_DETAIL_FN = "detail";

    /**
     * Checks signature incoming from gateway result payment request.
     *
     * @param gatewayParams incoming gateway params.
     * @param storedRequestObj request params made on create payment request step and stored in payment.
     * @throws ResultPaymentProcessException when error occurred
     */
    @Override
    public void process(IObject gatewayParams, IObject storedRequestObj) throws ResultPaymentProcessException {

        try {
            IField getawaySignF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), GETAWAY_SIGN_FN);
            IField signF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SIGN_FN);
            String signFromIncomingRequest = signF.in(gatewayParams, String.class);

            IField detailSectionInStoredReqF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), REQUEST_DETAIL_FN);
            IObject detailSectionInStoredReq = detailSectionInStoredReqF.in(storedRequestObj);
            String storedInRequestSign = getawaySignF.in(detailSectionInStoredReq, String.class);
            boolean signatureComparisonResult = signFromIncomingRequest.equals(storedInRequestSign);

            if (!signatureComparisonResult)
                throw new IncorrectSignatureException("Signature passed from gateway incorrect!");
        } catch (ResolutionException | ReadValueException | InvalidArgumentException | IncorrectSignatureException e) {
            throw new ResultPaymentProcessException(e.getMessage(), e);
        }
    }
}
