package com.perspective.vpng.rule.determine_active_gateway;

import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
public class DetermineActiveGatewayRuleTest {

    private static IResolveDependencyStrategy strategy;
    private static ICachedCollection paymentMethodMatrixCachedCollection;

    private static IField paymentMethodF;
    private static IField paymentKindF;
    private static IField matrixKindsF;
    private static IField matrixKindItemNameF;
    private static IField matrixGatewaysF;
    private static IField gwTypeF;
    private static IField gwActivityF;
    private static IField gwTerminalTypeF;

    private static IObject searchCriteriaObject;
    private static IObject gwItem1;
    private static IObject gwItem2;

    private static IObject matrixKind1;
    private static IObject matrixKind2;
    private static IObject matrixKind3;

    private static final String SEARCHING_PAYMENT_KIND_IN_MATRIX = "standard";
    private static final String MATRIX_COLLECTION_FIND_VALUE = "creditCard";
    private static final String GW_TYPE_FN = "gwType";
    private static final String GW_ACTIVITY_FN = "activity";
    private static final String GW_TERMINAL_TYPE_FN = "terminalType";

    private static final String PAYMENT_METHOD_MATRIX_COLLECTION_NAME = "paymentMethodMatrixCollection";
    private static final String METHOD_MATRIX_COLLECTION_KEY_FIELD_NAME = "type";
    private static final String MATRIX_KIND_ITEM_NAME_FIELD_NAME = "name";
    private static final String GW_ACTIVITY_FIELD_NAME = "activity";
    private static final String MATRIX_KINDS_FIELD_NAME = "kinds";
    private static final String MATRIX_GATEWAYS_FIELD_NAME = "gateways";

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        paymentMethodMatrixCachedCollection = mock(ICachedCollection.class);
        IKey keyForICachedCollection = mock(IKey.class);

        when(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()))
                .thenReturn(keyForICachedCollection);

        when(IOC.resolve(keyForICachedCollection, PAYMENT_METHOD_MATRIX_COLLECTION_NAME, METHOD_MATRIX_COLLECTION_KEY_FIELD_NAME))
                .thenReturn(paymentMethodMatrixCachedCollection);


        paymentMethodF = mock(IField.class);
        final String paymentMethodFN = "paymentMethod";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentMethodFN))
                .thenReturn(paymentMethodF);

        paymentKindF = mock(IField.class);
        final String paymentKindFN = "paymentKind";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentKindFN))
                .thenReturn(paymentKindF);

        matrixKindsF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), MATRIX_KINDS_FIELD_NAME))
                .thenReturn(matrixKindsF);

        matrixGatewaysF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), MATRIX_GATEWAYS_FIELD_NAME))
                .thenReturn(matrixGatewaysF);

        matrixKindItemNameF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), MATRIX_KIND_ITEM_NAME_FIELD_NAME))
                .thenReturn(matrixKindItemNameF);

        gwTypeF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), GW_TYPE_FN))
                .thenReturn(gwTypeF);

        gwActivityF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), GW_ACTIVITY_FN))
                .thenReturn(gwActivityF);

        gwTerminalTypeF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), GW_TERMINAL_TYPE_FN))
                .thenReturn(gwTerminalTypeF);


        searchCriteriaObject = mock(IObject.class);
        IObject findedMatrixItem = mock(IObject.class);
        List<IObject> cacheMustReturn = Arrays.asList(findedMatrixItem);

        when(paymentMethodMatrixCachedCollection.getItems(MATRIX_COLLECTION_FIND_VALUE)).thenReturn(cacheMustReturn);

        matrixKind1 = mock(IObject.class);
        matrixKind2 = mock(IObject.class);
        matrixKind3 = mock(IObject.class);
        List<IObject> matrixKinds = Arrays.asList(matrixKind1, matrixKind2, matrixKind3);

        gwItem1 = mock(IObject.class);
        gwItem2 = mock(IObject.class);
        List<IObject> matrixGws = Arrays.asList(gwItem1, gwItem2);

        when(paymentMethodF.in(searchCriteriaObject)).thenReturn(MATRIX_COLLECTION_FIND_VALUE);
        when(paymentKindF.in(searchCriteriaObject)).thenReturn(SEARCHING_PAYMENT_KIND_IN_MATRIX);

        when(matrixKindsF.in(findedMatrixItem)).thenReturn(matrixKinds);

        when(matrixGatewaysF.in(matrixKind1)).thenReturn(matrixGws);

        strategy = new DetermineActiveGatewayRule(
                PAYMENT_METHOD_MATRIX_COLLECTION_NAME,
                METHOD_MATRIX_COLLECTION_KEY_FIELD_NAME,
                MATRIX_KIND_ITEM_NAME_FIELD_NAME,
                GW_ACTIVITY_FIELD_NAME,
                MATRIX_KINDS_FIELD_NAME,
                MATRIX_GATEWAYS_FIELD_NAME
        );
    }

    @Test
    public void determineActiveGatewayFromMatrixTest() throws Exception {

        when(matrixKindItemNameF.in(matrixKind1)).thenReturn("standard");
        when(matrixKindItemNameF.in(matrixKind2)).thenReturn("communal");
        when(matrixKindItemNameF.in(matrixKind2)).thenReturn("mixed");

        when(gwActivityF.in(gwItem1)).thenReturn(true);
        when(gwTypeF.in(gwItem1)).thenReturn("vsep");
        when(gwTerminalTypeF.in(gwItem1)).thenReturn("standard");

        when(gwActivityF.in(gwItem2)).thenReturn(false);
        when(gwTypeF.in(gwItem2)).thenReturn("psbank");
        when(gwTerminalTypeF.in(gwItem2)).thenReturn("communal");

        IObject resolvedActiveGw = strategy.resolve(searchCriteriaObject);

        assertNotNull(resolvedActiveGw);
        assertEquals(gwActivityF.in(resolvedActiveGw), true);
        assertEquals(gwTypeF.in(resolvedActiveGw), "vsep");
        assertEquals(gwTerminalTypeF.in(resolvedActiveGw), "standard");
    }

    @Test
    public void should_ThrowException_When_DetermineActiveGateway_WithReason_ActiveGwNotFound()
            throws Exception {

        when(matrixKindItemNameF.in(matrixKind1)).thenReturn("standard");
        when(matrixKindItemNameF.in(matrixKind2)).thenReturn("communal");
        when(matrixKindItemNameF.in(matrixKind2)).thenReturn("mixed");

        when(gwActivityF.in(gwItem1)).thenReturn(false);
        when(gwTypeF.in(gwItem1)).thenReturn("vsep");
        when(gwTerminalTypeF.in(gwItem1)).thenReturn("standard");

        when(gwActivityF.in(gwItem2)).thenReturn(false);
        when(gwTypeF.in(gwItem2)).thenReturn("psbank");
        when(gwTerminalTypeF.in(gwItem2)).thenReturn("communal");
        try {
            IObject resolvedActiveGw = strategy.resolve(searchCriteriaObject);
            assertNull(resolvedActiveGw);
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(
                    ex.getMessage(),
                    "Payment method matrix hasn't contains active gateway!"
            );
        }
    }

    @Test
    public void should_ThrowException_When_DetermineActiveGateway_WithReason_CantFindNeedeMatrixKind()
            throws Exception {

        when(matrixKindItemNameF.in(matrixKind1)).thenReturn("1");
        when(matrixKindItemNameF.in(matrixKind2)).thenReturn("2");
        when(matrixKindItemNameF.in(matrixKind3)).thenReturn("3");
        try {

            IObject resolvedActiveGw = strategy.resolve(searchCriteriaObject);
            assertNull(resolvedActiveGw);
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(
                    ex.getMessage(),
                    String.format("Payment method matrix hasn't contains " +
                            "needed kind item with name '%s'!", SEARCHING_PAYMENT_KIND_IN_MATRIX)
            );
        }
    }

    @Test
    public void should_ThrowException_When_DetermineActiveGateway_WithReason_FindMatrixItemByType()
            throws Exception {

        when(paymentMethodMatrixCachedCollection.getItems(MATRIX_COLLECTION_FIND_VALUE)).thenReturn(null);
        try {

            IObject resolvedActiveGw = strategy.resolve(searchCriteriaObject);
            assertNull(resolvedActiveGw);
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(
                    ex.getMessage(),
                    String.format("Can't find item by value '%s' in payment method matrix " +
                                    "cache collection!", MATRIX_COLLECTION_FIND_VALUE)
            );
        }
    }

}
