package com.perspective.vpng.rule.determine_active_gateway;

import com.perspective.vpng.rule.determine_active_gateway.exception.GatewayNotFoundException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Gets active gateway from payment method matrix
 */
public class DetermineActiveGatewayRule implements IResolveDependencyStrategy {

    private ICachedCollection paymentMethodMatrixCachedCollection;

    private final Map<Class, IClassCastStrategy> classCastStrategies;

    private static final String PAYMENT_KIND_FIELD_NAME = "paymentKind";
    private static final String PAYMENT_METHOD_FIELD_NAME = "paymentMethod";

    private final IField kindsF;
    private final IField gatewaysF;

    private final IField kindItemNameF;
    private final IField gwActivityF;

    /**
     * Default constructor.
     *
     * @param paymentMethodMatrixCollectionName - payment method matrix collection name
     * @param paymentMethodMatrixCollectionKeyFieldName - payment method matrix collection key field name
     * @param matrixKindItemNameFieldName - payment method matrix collection item field name
     * @param matrixGwActivityFieldName - payment method matrix collection gateway activity field name
     * @param matrixKindsFieldName - payment method matrix collection item kinds field name
     * @param matrixGatewaysFieldName - payment method matrix collection item gateways field name
     * @throws ResolveDependencyStrategyException when resolution errors
     */
    public DetermineActiveGatewayRule(final String paymentMethodMatrixCollectionName,
                                      final String paymentMethodMatrixCollectionKeyFieldName,
                                      final String matrixKindItemNameFieldName,
                                      final String matrixGwActivityFieldName,
                                      final String matrixKindsFieldName,
                                      final String matrixGatewaysFieldName)
            throws ResolveDependencyStrategyException {

        try {
            paymentMethodMatrixCachedCollection =
                    IOC.resolve(
                            Keys.getOrAdd(
                                    ICachedCollection.class.getCanonicalName()),
                            paymentMethodMatrixCollectionName,
                            paymentMethodMatrixCollectionKeyFieldName
                    );
            this.kindItemNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), matrixKindItemNameFieldName);
            this.gwActivityF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), matrixGwActivityFieldName);
            this.kindsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), matrixKindsFieldName);
            this.gatewaysF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), matrixGatewaysFieldName);
            this.classCastStrategies = new HashMap<Class, IClassCastStrategy>() {{
                put(Boolean.class, object -> (Boolean) object);
                put(String.class, object -> Boolean.valueOf((String) object));
            }};
        } catch (ResolutionException e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }

    /**
     * Finds an active gateway in payment method matrix collection
     *
     * @param args - resolution arguments;
     *             must contains search-criteria-object:
     *             "object" : {
     *               "paymentKind": "someValue",
     *               "paymentMethod": "someValue"
     *             }
     *
     * @param <T> - type of rule return value.
     *
     * @return an active gateway property type of {@link IObject}.
     *
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject searchParam = (IObject) args[0];
            IField paymentMethodF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), PAYMENT_METHOD_FIELD_NAME);
            IField paymentKindF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), PAYMENT_KIND_FIELD_NAME);

            IObject paymentMethodMatrixItem = findMatrixItemByType(paymentMethodF.in(searchParam));
            List<IObject> kindList = kindsF.in(paymentMethodMatrixItem);
            IObject neededKind = findNeededMatrixKind(kindList, paymentKindF.in(searchParam));
            List<IObject> gwList = gatewaysF.in(neededKind);
            IObject neededGw = findNeededGateway(gwList);
            return (T) neededGw;
        } catch (ReadValueException | InvalidArgumentException | GatewayNotFoundException e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        } catch (ResolutionException e) {
            throw new ResolveDependencyStrategyException(String
                    .format("Invalid arguments! Can't retrieve '%s' or '%s' " +
                            "from search-criteria-object", PAYMENT_KIND_FIELD_NAME, PAYMENT_METHOD_FIELD_NAME), e);
        }
    }

    private IObject findMatrixItemByType(String matrixItemType){
        try {
            List<IObject> collectionResult = paymentMethodMatrixCachedCollection.getItems(matrixItemType);
            if (null == collectionResult || collectionResult.isEmpty())
                throw new GatewayNotFoundException(String
                        .format("Can't find item by value '%s' in payment method matrix " +
                                "cache collection!", matrixItemType));
            return collectionResult.get(0);
        } catch (GetCacheItemException e) {
            throw new GatewayNotFoundException("Error occurred when retrieve data " +
                    "from payment method matrix cache collection!", e);
        }
    }

    private IObject findNeededMatrixKind(List<IObject> kinds, String kindName) throws ResolveDependencyStrategyException {
        Stream<IObject> kindsStream = kinds.stream();
        Optional<IObject> matrixKindItemOpt = kindsStream.filter(kind -> {
            try {
                String tmpValue = kindItemNameF.in(kind);
                return tmpValue != null && tmpValue.equals(kindName);
            } catch (ReadValueException | InvalidArgumentException ex) {
                throw new GatewayNotFoundException(ex.getMessage(), ex);
            }
        }).findFirst();

        return matrixKindItemOpt.orElseThrow(() ->
                new ResolveDependencyStrategyException(String
                        .format("Payment method matrix hasn't contains needed kind item with name '%s'!", kindName)));
    }

    private IObject findNeededGateway(List<IObject> gateways) throws ResolveDependencyStrategyException {
        Stream<IObject> gatewaysStream = gateways.stream();
        Optional<IObject> gwItemOpt = gatewaysStream.filter(gwItem -> {
            try {
                Object tmpValue = gwActivityF.in(gwItem);
                return tmpValue != null ?
                        classCastStrategies.get(tmpValue.getClass()).cast(tmpValue) : false;
            } catch (ReadValueException | InvalidArgumentException e) {
                throw new GatewayNotFoundException(e.getMessage(), e);
            }
        }).findFirst();

        return gwItemOpt.orElseThrow(() ->
                new ResolveDependencyStrategyException("Payment method matrix hasn't contains active gateway!"));

    }

    private interface IClassCastStrategy {
        Boolean cast(Object object);
    }

}
