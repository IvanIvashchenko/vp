package com.perspective.vpng.rule.determine_active_gateway.exception;

/**
 * Throws when active gateway couldn't found.
 */
public class GatewayNotFoundException extends RuntimeException{

    public GatewayNotFoundException(String message) {
        super(message);
    }

    public GatewayNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
