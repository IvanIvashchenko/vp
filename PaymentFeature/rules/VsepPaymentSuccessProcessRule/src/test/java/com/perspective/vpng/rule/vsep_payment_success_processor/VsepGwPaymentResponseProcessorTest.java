package com.perspective.vpng.rule.vsep_payment_success_processor;

import com.perspective.vpng.strategy.payment_success_process.exception.PaymentSuccessProcessException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;


@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
public class VsepGwPaymentResponseProcessorTest {

    private IField resultF;
    private IField successF;
    private IField errorF;

    private IObject successObj;
    private IObject errorObj;

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        resultF = mock(IField.class);
        final String paymentMethodFN = "gwResult";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentMethodFN))
                .thenReturn(resultF);

        successObj = mock(IObject.class);

    }

    @Test(expected = PaymentSuccessProcessException.class)
    public void name() throws Exception {
        IObject parameters = mock(IObject.class);
        when(resultF.in(parameters, String.class)).thenReturn("0");

        IField resF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "result"))
                .thenReturn(resF);

        IField orderIdF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "orderId"))
                .thenReturn(orderIdF);

        IObject resObj = mock(IObject.class);
        when(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "resObj"))
                .thenReturn(resObj);

        VsepPaymentSuccessProcessor processor = new VsepPaymentSuccessProcessor("orderId");
        IObject res = processor.process(parameters);

        verify(resF, times(4)).out(any(), any());

        System.out.println(res);

    }
}