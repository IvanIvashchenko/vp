package com.perspective.vpng.rule.vsep_payment_success_processor;

import com.perspective.vpng.strategy.payment_success_process.PaymentSuccessProcessor;
import com.perspective.vpng.strategy.payment_success_process.exception.PaymentSuccessProcessException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Success payment implementation for VSEP gateway.
 */
public class VsepPaymentSuccessProcessor implements PaymentSuccessProcessor {

    private static final String RESULT_FROM_GW_FN = "result";
    private static final String SUCCESS_STATUS_CODE = "0";
    private static final String SUCCESS_PAYMENT = "SUCCESS";
    private static final String ERROR_PAYMENT = "ERROR";
    private static final String RESULT_OUT_FN = "result";
    private static final String ORDER_ID_OUT_FN = "orderId";

    private final IField paymentNumberF;

    public VsepPaymentSuccessProcessor(final String paymentNumberFieldName) {
        try {
            this.paymentNumberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentNumberFieldName);
        } catch (ResolutionException e) {
            throw new RuntimeException();
        }

    }

    /**
     * Analyzes gateway request after pay payment in gateway, if result in param equals '0' then return
     * SUCCESS object else ERROR object.
     *
     * @param gwParameters a parameter from gateway
     * @return object with result of success payment process, see below
     *          {
     *              "result": "SUCCESS or ERROR",
     *              "paymentNumber": "payment identifier..."
     *          }
     *
     * @throws PaymentSuccessProcessException when error occurred.
     */
    @Override
    public IObject process(IObject gwParameters) throws PaymentSuccessProcessException {
        try {
            IField resultFromGwF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), RESULT_FROM_GW_FN);
            String result = resultFromGwF.in(gwParameters, String.class);
            String paymentNumber = paymentNumberF.in(gwParameters);
            if (null == paymentNumber || paymentNumber.isEmpty())
                throw new PaymentSuccessProcessException("Payment number can't be empty or null!");
            if (result.equals(SUCCESS_STATUS_CODE))
                return getResultObj(SUCCESS_PAYMENT, paymentNumber);
            return getResultObj(ERROR_PAYMENT, paymentNumber);
        } catch (ResolutionException | ReadValueException | InvalidArgumentException | ChangeValueException e) {
            throw new PaymentSuccessProcessException(e.getMessage(), e);
        }
    }

    private IObject getResultObj(String resultValue, String paymentNumber)
            throws ChangeValueException, InvalidArgumentException, ResolutionException {

            IObject resultObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IField resF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), RESULT_OUT_FN);
            IField paymentNumberOutF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), ORDER_ID_OUT_FN);
            resF.out(resultObj, resultValue);
            paymentNumberOutF.out(resultObj, paymentNumber);
            return resultObj;

    }
}
