package com.perspective.vpng.rule.pay_payment_default;

import com.perspective.vpng.strategy.pay_payment.PayPaymentProcessor;
import com.perspective.vpng.strategy.pay_payment.exception.PayPaymentException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;

/**
 * Pay payment.
 */
public class PayPaymentDefaultRule implements PayPaymentProcessor {


    private final String paidPaymentCollectionName;
    private final IPool connectionPool;

    /**
     * Default constructor.
     *
     * @param paidPaymentCollectionName collection for store paid payment.
     * @param connectionPool pool.
     */
    public PayPaymentDefaultRule(final String paidPaymentCollectionName, final IPool connectionPool) {
        this.paidPaymentCollectionName = paidPaymentCollectionName;
        this.connectionPool = connectionPool;
    }


    /**
     * Inserts payment to paid payment storage.
     *
     * @param payment object for insert.
     * @return payment.
     * @throws PayPaymentException if error occurred when insert payment.
     */
    @Override
    public IObject process(IObject payment) throws PayPaymentException {

        try (PoolGuard guard = new PoolGuard(connectionPool)){
            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.insert"),
                    guard.getObject(),
                    paidPaymentCollectionName,
                    payment
            );
            task.execute();
        } catch (TaskExecutionException e) {
            throw new PayPaymentException("Error during task execution!", e);
        } catch (ResolutionException e) {
            throw new PayPaymentException("Can't resolve query objects: ", e);
        } catch (PoolGuardException e) {
            throw new PayPaymentException("Error during try get connection to database!", e);
        }

        return payment;
    }

}
