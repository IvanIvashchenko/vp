package com.perspective.vpng.rule.check_payment_paid_status_default;

import com.perspective.vpng.database.field.IDBFieldsHolder;
import com.perspective.vpng.strategy.check_payment_paid_status.CheckPaymentPaidStatusProcessor;
import com.perspective.vpng.strategy.check_payment_paid_status.exception.CheckPaymentPaidStatusException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

/**
 * Default implementation of check payment paid status.
 */
public class CheckPaymentPaidStatusDefault implements CheckPaymentPaidStatusProcessor {


    private final IDBFieldsHolder dbFieldsHolder;
    private final IField paymentNumberF;

    private final String paidPaymentCollectionName;
    private final IPool connectionPool;

    public CheckPaymentPaidStatusDefault(
            final String paidPaymentCollectionName,
            final IPool connectionPool,
            final String paymentNumberFieldName
    ) {
        try {
            this.paidPaymentCollectionName = paidPaymentCollectionName;
            this.connectionPool = connectionPool;
            this.dbFieldsHolder = IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()));
            this.paymentNumberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentNumberFieldName);
        } catch (ResolutionException e) {
            throw new RuntimeException();
        }
    }

    /**
     * Try obtain payment by number from paid payment storage and if it find payment then return OK otherwise WAIT
     * and ERROR if exception, see return section.
     *
     * @param paymentNumber a payment identifier.
     *
     * @return a object with result if payment is paid,
     *              {
     *                  "command": "ok"
     *              }
     *              else if payment not change state
     *              {
     *                  "command": "wait"
     *              }
     *              end else if error
     *              {
     *                  "command": "error"
     *              }
     *
     * @throws CheckPaymentPaidStatusException when error occurred.
     */
    @Override
    public IObject process(String paymentNumber) throws CheckPaymentPaidStatusException {
        final Long[] paymentCount = {0L};
        try (IPoolGuard guard = new PoolGuard(connectionPool)){
            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.count"),
                    guard.getObject(),
                    paidPaymentCollectionName,
                    preparePaymentCountSearchQueryById(paymentNumber),
                    (IAction<Long>) docCount -> paymentCount[0] = docCount
            );
            task.execute();
            return makeResultObj(paymentCount[0]);
        } catch (TaskExecutionException e) {
            throw new RuntimeException("Error during task execution!", e);
        } catch (ResolutionException e) {
            throw new RuntimeException("Can't resolve query objects: ", e);
        } catch (PoolGuardException e) {
            throw new RuntimeException("Error during try get connection to database!", e);
        } catch (InvalidArgumentException | ChangeValueException e) {
            throw new CheckPaymentPaidStatusException(e.getMessage(), e);
        }
    }

    private IObject preparePaymentCountSearchQueryById(final String paymentNumber) throws ResolutionException, ChangeValueException, InvalidArgumentException {
        IKey iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
        IObject searchQuery = IOC.resolve(iObjectKey);
        IObject searchFilter = IOC.resolve(iObjectKey);
        IObject criteria = IOC.resolve(iObjectKey);
        IObject page = IOC.resolve(iObjectKey);

        paymentNumberF.out(searchFilter, criteria);
        dbFieldsHolder.conditions().eq().out(criteria, paymentNumber);

        dbFieldsHolder.searchQuery().pageNumber().out(page, 1);
        dbFieldsHolder.searchQuery().pageSize().out(page, 1);

        dbFieldsHolder.searchQuery().filter().out(searchQuery, searchFilter);
        dbFieldsHolder.searchQuery().page().out(searchQuery, page);

        return searchQuery;
    }

    private IObject makeResultObj(final long count) throws CheckPaymentPaidStatusException {

        IObject resultObj = null;
        IField resultF = null;

        try {
            resultObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            resultF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "command");
            if (count > 0){
                resultF.out(resultObj, "ok");
            } else {
                resultF.out(resultObj, "wait");
            }

        } catch (ResolutionException | InvalidArgumentException | ChangeValueException e) {
            try {
                resultF.out(resultObj, "error");
            } catch (ChangeValueException | InvalidArgumentException ex) {
                throw new CheckPaymentPaidStatusException(ex.getMessage(), ex);
            }
        }
        return resultObj;
    }
}
