package com.perspective.vpng.strategy.payment_success_process.exception;

public class PaymentSuccessProcessException extends Exception {

    public PaymentSuccessProcessException(String message) {
        super(message);
    }

    public PaymentSuccessProcessException(String message, Throwable cause) {
        super(message, cause);
    }
}
