package com.perspective.vpng.strategy.payment_success_process;

import com.perspective.vpng.strategy.payment_success_process.exception.PaymentSuccessProcessException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Map;

/**
 * Checks result of a payment which passing by gateway after pay payment on gateway payment form.
 */
public class PaymentSuccessProcessStrategy implements IResolveDependencyStrategy {

    private static final String GW_TYPE_FN = "gwType";

    private final Map<String, PaymentSuccessProcessor> processStrategies;

    /**
     * Default constructor.
     *
     * @param processStrategies a rule map with custom implementation of success payment processors
     */
    public PaymentSuccessProcessStrategy(final Map<String, PaymentSuccessProcessor> processStrategies) {
        this.processStrategies = processStrategies;
    }

    /**
     * Call success payment process which depending on the gateway type.
     *
     * @param args - resolution arguments;
     *             args[0] must contains parameters for success from gateway and must including gateway type.
     *
     * @param <T> - type of rule return value.
     *
     * @return an object with process result, see below;
     *          {
     *              "result": "SUCCESS or ERROR"
     *          }
     *
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject gwParameters = (IObject) args[0];
            IField gwTypeField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), GW_TYPE_FN);
            String gwType = gwTypeField.in(gwParameters);

            PaymentSuccessProcessor gwResponseProcessor = processStrategies.get(gwType);
            if (null == gwResponseProcessor)
                throw new PaymentSuccessProcessException(String
                        .format("Can't find process gateway response rule by type '%s'!", gwType));

            return (T) gwResponseProcessor.process(gwParameters);
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }

    }
}
