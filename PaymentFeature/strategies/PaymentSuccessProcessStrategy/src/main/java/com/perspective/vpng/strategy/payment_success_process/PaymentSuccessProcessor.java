package com.perspective.vpng.strategy.payment_success_process;

import com.perspective.vpng.strategy.payment_success_process.exception.PaymentSuccessProcessException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

/**
 * You need implement this interface if you want to create custom
 * success payment behavior for some gateway.
 */
@FunctionalInterface
public interface PaymentSuccessProcessor {

    /**
     * Calls in {@link PaymentSuccessProcessStrategy#resolve(Object...)}
     *
     * @param gwParameters a parameter from gateway
     * @return object with result of success payment process, see below
     *          {
     *              "result": "SUCCESS or ERROR"
     *          }
     *
     * @throws PaymentSuccessProcessException when error occurred.
     */
    IObject process(IObject gwParameters) throws PaymentSuccessProcessException;

}
