package com.perspective.vpng.strategy.message_authentication_code;

import com.perspective.vpng.strategy.message_authentication_code.exception.MACSigningException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Map;

/**
 * Makes MAC for given text.
 */
public class MessageAuthenticationCodeStrategy implements IResolveDependencyStrategy {

    private static final String SIGN_ALGORITHM_TYPE_F_NAME = "signAlgorithmType";
    private static final String SIGN_ALGORITHM_NAME_F_NAME = "signAlgorithmName";
    private static final String ENCODING_F_NAME = "encoding";
    private static final String KEY_F_NAME = "key";
    private static final String TEXT_F_NAME = "text";

    private final Map<String, MACSigning> signStrategies;

    /**
     * Default constructor.
     *
     * @param signStrategies rule map.
     */
    public MessageAuthenticationCodeStrategy(final Map<String, MACSigning> signStrategies) {
        this.signStrategies = signStrategies;
    }

    /**
     *
     *
     * @param args resolution arguments;
     *          Note: args[0] should contain IObject and it be as below:
     *             "object": {
     *               "signAlgorithmType": "value",
     *               "signAlgorithmName": "value",
     *               "encoding": "value",
     *               "key": "value",
     *               "text": "value"
     *             }
     *
     * @param <T> - type of rule return value.
     * @return an MAC as string.
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject param = (IObject) args[0];
            IField signAlgorithmTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SIGN_ALGORITHM_TYPE_F_NAME);
            IField signAlgorithmNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SIGN_ALGORITHM_NAME_F_NAME);
            IField encodingF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), ENCODING_F_NAME);
            IField keyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), KEY_F_NAME);
            IField rawTextF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), TEXT_F_NAME);

            String algorithmType = signAlgorithmTypeF.in(param, String.class);
            String algorithmName = signAlgorithmNameF.in(param, String.class);
            String encoding = encodingF.in(param, String.class);
            String key = keyF.in(param, String.class);
            String text = rawTextF.in(param, String.class);


            MACSigning currentSigning = signStrategies.get(algorithmType);
            if (null == currentSigning)
                throw new ResolveDependencyStrategyException(String
                        .format("Can't find rule implementation by sign algorithm type '%s'!", algorithmType));
            return (T) currentSigning.sign(text, algorithmName, encoding, key);
        } catch (ResolutionException | ReadValueException | InvalidArgumentException | MACSigningException e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }
}
