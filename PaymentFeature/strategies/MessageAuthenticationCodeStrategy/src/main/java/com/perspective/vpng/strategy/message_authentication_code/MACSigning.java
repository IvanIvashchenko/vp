package com.perspective.vpng.strategy.message_authentication_code;

import com.perspective.vpng.strategy.message_authentication_code.exception.MACSigningException;

/**
 * You need implement this interface if you want to create custom
 * message authentication code algorithm implementation.
 */
@FunctionalInterface
public interface MACSigning {

    /**
     * Call in {@link MessageAuthenticationCodeStrategy#resolve(Object...)}
     *
     * @param text text to be signed.
     * @param signAlgorithm type of sign algorithm.
     * @param encoding result value encoding
     * @param key for make MAC needed symmetric.
     * @return message authentication code as string.
     * @throws MACSigningException catch some exception and generate this.
     */
    String sign(String text, String signAlgorithm, String encoding, String key) throws MACSigningException;
}
