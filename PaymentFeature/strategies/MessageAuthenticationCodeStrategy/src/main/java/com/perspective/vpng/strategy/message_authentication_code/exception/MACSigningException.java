package com.perspective.vpng.strategy.message_authentication_code.exception;

import com.perspective.vpng.strategy.message_authentication_code.MessageAuthenticationCodeStrategy;

/**
 * Exception for {@link MessageAuthenticationCodeStrategy} rule.
 */
public class MACSigningException extends Exception {

    public MACSigningException(String message) {
        super(message);
    }

    public MACSigningException(String message, Throwable cause) {
        super(message, cause);
    }
}
