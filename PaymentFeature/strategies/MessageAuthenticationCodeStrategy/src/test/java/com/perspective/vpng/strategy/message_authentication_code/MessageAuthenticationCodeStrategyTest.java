package com.perspective.vpng.strategy.message_authentication_code;

import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
public class MessageAuthenticationCodeStrategyTest {

    private IResolveDependencyStrategy strategy;
    private Map<String, MACSigning> signStrategies = new HashMap<>();

    private IField signAlgorithmTypeF;
    private IField signAlgorithmNameF;
    private IField encodingF;
    private IField keyF;
    private IField textF;

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);
        MACSigning macSigning = mock(MACSigning.class);

        signAlgorithmTypeF = mock(IField.class);
        final String signAlgorithmTypeFN = "signAlgorithmType";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), signAlgorithmTypeFN))
                .thenReturn(signAlgorithmTypeF);

        signAlgorithmNameF = mock(IField.class);
        final String signAlgorithmNameFN = "signAlgorithmName";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), signAlgorithmNameFN))
                .thenReturn(signAlgorithmNameF);

        encodingF = mock(IField.class);
        final String encodingFN = "encoding";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), encodingFN))
                .thenReturn(encodingF);

        keyF = mock(IField.class);
        final String keyFN = "key";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), keyFN))
                .thenReturn(keyF);

        textF = mock(IField.class);
        final String textFN = "text";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), textFN))
                .thenReturn(textF);

        signStrategies.put("hmac", macSigning);
        when(macSigning.sign(anyString(), anyString(), anyString(), anyString())).thenReturn("dataSigned");
        strategy = new MessageAuthenticationCodeStrategy(signStrategies);
    }

    @Test
    public void correctSigningTest() throws Exception {
        IObject params = mock(IObject.class);
        when(signAlgorithmTypeF.in(params, String.class)).thenReturn("hmac");
        when(signAlgorithmNameF.in(params, String.class)).thenReturn("HmacSHA1");
        when(encodingF.in(params, String.class)).thenReturn("UTF-8");
        when(keyF.in(params, String.class)).thenReturn("a12ec87101e0323a2e14305d690795646315550d");
        when(textF.in(params, String.class)).thenReturn("text for sign");

        String signedString = strategy.resolve(params);
        assertEquals("dataSigned", signedString);
    }

    @Test
    public void should_ThrowException_When_Signing_WithReason_SigningStrategyNotFound()
            throws Exception {

        IObject params = mock(IObject.class);
        when(signAlgorithmTypeF.in(params, String.class)).thenReturn("not_found");
        try {
            strategy.resolve(params);
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(
                    ex.getMessage(),
                    String.format("Can't find rule implementation by sign algorithm type '%s'!", "not_found")
            );
        }
    }

}