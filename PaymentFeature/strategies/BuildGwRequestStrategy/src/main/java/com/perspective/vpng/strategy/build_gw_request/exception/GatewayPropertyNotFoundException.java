package com.perspective.vpng.strategy.build_gw_request.exception;

/**
 * Exception for error in {@link com.perspective.vpng.strategy.gw_request_parameters_builder.GwRequestBuilderStrategy}
 * when search in cache collection failed
 */
public class GatewayPropertyNotFoundException extends Exception {

    public GatewayPropertyNotFoundException(String message) {
        super(message);
    }

    public GatewayPropertyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
