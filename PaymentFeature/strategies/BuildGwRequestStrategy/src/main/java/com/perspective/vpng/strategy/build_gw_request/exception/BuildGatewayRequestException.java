package com.perspective.vpng.strategy.build_gw_request.exception;

/**
 * Exception for error in {@link com.perspective.vpng.strategy.gw_request_parameters_builder.GwRequestBuilderStrategy}
 */
public class BuildGatewayRequestException extends Exception {

    public BuildGatewayRequestException(String message) {
        super(message);
    }

    public BuildGatewayRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
