package com.perspective.vpng.strategy.build_gw_request;

import com.perspective.vpng.strategy.build_gw_request.exception.BuildGatewayRequestException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

/**
 * You need implement this interface if you want to create custom
 * gateway request parameters builder
 */
@FunctionalInterface
public interface IGwRequestBuilder {

    /**
     * Calls in {@link BuildGwRequestStrategy#resolve(Object...)}
     *
     * @param payment as {@link IObject}
     * @param gwProperties as {@link IObject}
     * @return prepared request parameters as {@link IObject}
     *
     *      Note: returned request param should have structure as below
     *          {
     *              "paymentUrl": "...",
     *              "requestNumber": "...",
     *              "detail": {
     *                  "merchant": "777",
     *                  "amount": "101.01",
     *                  ...
     *                  ...
     *              }
     *          }
     *
     */
    IObject build(IObject payment, IObject gwProperties) throws BuildGatewayRequestException;

}
