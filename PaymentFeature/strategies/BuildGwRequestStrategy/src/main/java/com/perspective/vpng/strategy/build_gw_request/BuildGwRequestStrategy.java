package com.perspective.vpng.strategy.build_gw_request;

import com.perspective.vpng.strategy.build_gw_request.exception.GatewayPropertyNotFoundException;
import com.perspective.vpng.strategy.build_gw_request.exception.BuildGatewayRequestException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Builds parameters for gateway request.
 *
 */
public class BuildGwRequestStrategy implements IResolveDependencyStrategy {

    private final Map<String, IGwRequestBuilder> builderStrategies;
    private ICachedCollection gatewayPropertiesCachedCollection;

    /**
     * Default constructor.
     *
     * @param builderStrategies - map with custom builder implementation
     */
    public BuildGwRequestStrategy(
            final Map<String, IGwRequestBuilder> builderStrategies,
            final String gwPropertyCacheCollectionName,
            final String gwPropertyCacheCollectionKey
            )
            throws BuildGatewayRequestException {

        this.builderStrategies = new HashMap<>(builderStrategies);
        try {
            gatewayPropertiesCachedCollection =
                    IOC.resolve(
                            Keys.getOrAdd(
                                    ICachedCollection.class.getCanonicalName()), gwPropertyCacheCollectionName, gwPropertyCacheCollectionKey
                    );
        } catch (ResolutionException e) {
            throw new BuildGatewayRequestException(
                    String.format("Can't resolve '%s' cache collection!", gwPropertyCacheCollectionName)
            );
        }
    }

    /**
     * Finds needed rule implementation in rule map by gateway type and
     * call it <code>build</code> method.
     *
     * Note: if you want to add support other gateway, you will need to implement {@link IGwRequestBuilder}
     *       and put it to rule map in GwRequestParametersBuilderPlugin
     *
     * @see IGwRequestBuilder
     *
     * @param args - resolution arguments, must contain gateway type and payment as IObject.
     *             1. gateway type as String;
     *             2. payment as IObject
     *             "object" : {
     *                "payment": "{...}"
     *             }
     *
     * @param <T> - type of rule return value.
     * @return prepared {@link IObject} contains gateway parameters.
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            String gatewayType = String.valueOf(args[0]);
            IObject payment = (IObject) args[1];

            IObject requestParameters = null;

            for (Map.Entry<String, IGwRequestBuilder> strategy : builderStrategies.entrySet()){
                if (strategy.getKey().equals(gatewayType)){
                    requestParameters = strategy.getValue().build(payment, getGatewayProperties(gatewayType));
                }
            }

            if (requestParameters == null)
                throw new BuildGatewayRequestException(String
                        .format("Can't find rule implementation by type '%s' " +
                                "for build payment request parameters", gatewayType));
            return (T) requestParameters;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }

    private IObject getGatewayProperties(String gwType) throws GatewayPropertyNotFoundException {
        IObject gwProperties;
        try {
            List<IObject> collectionResult = gatewayPropertiesCachedCollection.getItems(gwType);
            if (null == collectionResult || collectionResult.isEmpty())
                throw new GatewayPropertyNotFoundException(String
                        .format("Can't find gateway property by type '%s' in cache collection!", gwType));

            gwProperties = collectionResult.get(0);
        } catch (GetCacheItemException e) {
            throw new GatewayPropertyNotFoundException("Error occurred when retrieve gateway " +
                    "property from cache collection!", e);
        }

        return gwProperties;
    }
}
