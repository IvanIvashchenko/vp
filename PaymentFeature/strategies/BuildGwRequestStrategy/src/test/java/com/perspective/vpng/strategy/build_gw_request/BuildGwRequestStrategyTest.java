package com.perspective.vpng.strategy.build_gw_request;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
public class BuildGwRequestStrategyTest {

    private static IResolveDependencyStrategy strategy;
    private static Map<String, IGwRequestBuilder> builderStrategies = new HashMap<>();
    private static IGwRequestBuilder gwRequestBuilderMock = mock(IGwRequestBuilder.class);
    private static IObject paymentMock = mock(IObject.class);
    private static IObject gwPropertiesMock = mock(IObject.class);
    private static IObject preparedRequestParamsMock = mock(IObject.class);
    private static final String EXIST_GW_TYPE = "vsep";
    private static final String NOT_EXIST_GW_TYPE = "none";

    private static ICachedCollection gwPropertiesCollection;

    @BeforeClass
    public static void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        gwPropertiesCollection = mock(ICachedCollection.class);
        IKey keyForICachedCollection = mock(IKey.class);

        when(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()))
                .thenReturn(keyForICachedCollection);

        when(IOC.resolve(keyForICachedCollection, "gwPropertiesCollection", "gwType"))
                .thenReturn(gwPropertiesCollection);

        when(gwRequestBuilderMock.build(paymentMock, gwPropertiesMock)).thenReturn(preparedRequestParamsMock);
        builderStrategies.put(EXIST_GW_TYPE, gwRequestBuilderMock);

        strategy = new BuildGwRequestStrategy(builderStrategies, "gwPropertiesCollection", "gwType");
    }

    @Test
    public void gettingRequestParamTest() throws Exception {
        List<IObject> gwProperties = new ArrayList<>();
        gwProperties.add(gwPropertiesMock);
        when(gwPropertiesCollection.getItems(EXIST_GW_TYPE)).thenReturn(gwProperties);
        IObject resolvedRequestParam = strategy.resolve(EXIST_GW_TYPE, paymentMock);
        assertEquals(resolvedRequestParam, preparedRequestParamsMock);
    }

    @Test
    public void should_ThrowException_When_GettingRequestParams_WithReason_GwParametrsNotFound()
            throws Exception {

        when(gwPropertiesCollection.getItems(EXIST_GW_TYPE)).thenReturn(null);
        try {
            IObject resolvedRequestParam = strategy.resolve(EXIST_GW_TYPE, paymentMock);
            assertNull(resolvedRequestParam);
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(
                    ex.getMessage(),
                    String.format("Can't find gateway property by type '%s' in cache collection!", EXIST_GW_TYPE)
            );
        }
    }

    @Test
    public void should_ThrowException_When_GettingRequestParams_WithReason_NeededBuildStrategyNotFound()
            throws Exception {
        List<IObject> gwProperties = new ArrayList<>();
        gwProperties.add(gwPropertiesMock);
        when(gwPropertiesCollection.getItems(NOT_EXIST_GW_TYPE)).thenReturn(gwProperties);
        try {
            IObject resolvedRequestParam = strategy.resolve(NOT_EXIST_GW_TYPE, paymentMock);
            assertNull(resolvedRequestParam);
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(
                    ex.getMessage(),
                    String.format("Can't find rule implementation by type '%s' " +
                            "for build payment request parameters", NOT_EXIST_GW_TYPE)
            );
        }
    }
}
