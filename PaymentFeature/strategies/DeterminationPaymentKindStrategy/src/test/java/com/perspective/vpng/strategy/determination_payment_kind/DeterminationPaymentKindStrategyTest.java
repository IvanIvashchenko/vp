package com.perspective.vpng.strategy.determination_payment_kind;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.powermock.api.mockito.PowerMockito.*;

@PrepareForTest({ IOC.class, Keys.class })
@RunWith(PowerMockRunner.class)
public class DeterminationPaymentKindStrategyTest {

    private static DeterminationPaymentKindStrategy strategy;
    private static IField paymentTypeF;

    @BeforeClass
    public static void setUp() throws Exception {
        List<String> availablePaymentTypes = Arrays.asList("standard", "communal");
        String paymentTypeFieldName = "paymentType";
        String mixedType = "mixed";

        mockStatic(IOC.class);
        mockStatic(Keys.class);
        IKey fieldKey = mock(IKey.class);
        paymentTypeF = mock(IField.class);
        when(Keys.getOrAdd(IField.class.getCanonicalName())).thenReturn(fieldKey);
        when(IOC.resolve(eq(fieldKey), eq(paymentTypeFieldName))).thenReturn(paymentTypeF);

        strategy = new DeterminationPaymentKindStrategy(availablePaymentTypes, paymentTypeFieldName, mixedType);
    }

    @Test
    public void should_DetermineCommunalPaymentKind() throws Exception {
        final String estimatedType = "communal";
        final int invoicesSize = 10;
        List<IObject> invoices = new ArrayList<>(invoicesSize);
        for (int i = 0; i < invoicesSize; ++i) {
            IObject invoice = mock(IObject.class);
            when(paymentTypeF.in(eq(invoice))).thenReturn(estimatedType);
            invoices.add(invoice);
        }
        assertEquals(strategy.resolve(invoices), estimatedType);
    }

    @Test
    public void should_DetermineStandardPaymentKind() throws Exception {
        final String estimatedType = "standard";
        final int invoicesSize = 10;
        List<IObject> invoices = new ArrayList<>(invoicesSize);
        for (int i = 0; i < invoicesSize; ++i) {
            IObject invoice = mock(IObject.class);
            when(paymentTypeF.in(eq(invoice))).thenReturn(estimatedType);
            invoices.add(invoice);
        }
        assertEquals(strategy.resolve(invoices), estimatedType);
    }

    @Test
    public void should_DetermineMixedPaymentKind() throws Exception {
        final String estimatedType = "mixed";
        final int invoicesSize = 10;
        List<IObject> invoices = new ArrayList<>(invoicesSize);

        for (int i = 0; i < invoicesSize; ++i) {
            IObject invoice = mock(IObject.class);
            if (i == invoicesSize % 6) {
                when(paymentTypeF.in(eq(invoice))).thenReturn("communal");
            } else {
                when(paymentTypeF.in(eq(invoice))).thenReturn("standard");
            }
            invoices.add(invoice);
        }
        assertEquals(strategy.resolve(invoices), estimatedType);

        for (int i = 0; i < invoicesSize; ++i) {
            IObject invoice = mock(IObject.class);
            if (i % 2 == 0) {
                when(paymentTypeF.in(eq(invoice))).thenReturn("communal");
            } else {
                when(paymentTypeF.in(eq(invoice))).thenReturn("standard");
            }
            invoices.add(invoice);
        }
        assertEquals(strategy.resolve(invoices), estimatedType);
    }

    @Test
    public void should_ThrowException_WithReason_InvalidPaymentType() throws Exception {
        String exceptionalTypeOnFirstPositionMessage = "";
        String exceptionalTypeOnRandomPositionMessage = "";

        try {
            final int invoicesSize = 10;
            List<IObject> invoices = new ArrayList<>(invoicesSize);
            for (int i = 0; i < invoicesSize; ++i) {
                IObject invoice = mock(IObject.class);
                if (i == 0) {
                    when(paymentTypeF.in(eq(invoice))).thenReturn("exceptionalType");
                } else {
                    when(paymentTypeF.in(eq(invoice))).thenReturn("communal");
                }

                invoices.add(invoice);
            }
            strategy.resolve(invoices);
        } catch (ResolveDependencyStrategyException ex) {
            exceptionalTypeOnFirstPositionMessage = ex.getMessage();
        }

        try {
            final int invoicesSize = 10;
            List<IObject> invoices = new ArrayList<>(invoicesSize);
            for (int i = 0; i < invoicesSize; ++i) {
                IObject invoice = mock(IObject.class);
                if (i % 3 == 2) {
                    when(paymentTypeF.in(eq(invoice))).thenReturn("exceptionalType");
                } else {
                    when(paymentTypeF.in(eq(invoice))).thenReturn("communal");
                }

                invoices.add(invoice);
            }
            strategy.resolve(invoices);
        } catch (ResolveDependencyStrategyException ex) {
            exceptionalTypeOnRandomPositionMessage = ex.getMessage();
        }

        assertEquals(exceptionalTypeOnFirstPositionMessage, "Invalid payment type: " +
                "list of available payment types hasn't contains this type.");
        assertEquals(exceptionalTypeOnRandomPositionMessage, "Invalid payment type: " +
                "list of available payment types hasn't contains this type.");
    }

}
