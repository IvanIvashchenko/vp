package com.perspective.vpng.strategy.determination_payment_kind;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

/**
 * Strategy for determination type of some payment.
 *
 * @see DeterminationPaymentKindStrategy#resolve(Object...)
 */
public class DeterminationPaymentKindStrategy implements IResolveDependencyStrategy {
    /** Field of payment type; contains each provider. */
    private final IField paymentTypeF;
    /** List of available payment types. */
    private final List<String> availableTypes;
    /** Name of mixed payment type. */
    private final String mixedType;

    /**
     * Constructor.
     *
     * @param availableTypes - list of available payment types.
     * @param paymentTypeFieldName - name of payment type field.
     * @param mixedTypeName - name of mixed payment type.
     * @throws ResolutionException when error resolution field with <param>paymentTypeFieldName</param> name.
     */
    public DeterminationPaymentKindStrategy(
            final List<String> availableTypes,
            final String paymentTypeFieldName,
            final String mixedTypeName
    ) throws ResolutionException {
        this.availableTypes = availableTypes;
        this.paymentTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentTypeFieldName);
        this.mixedType = mixedTypeName;
    }

    /**
     * Determines common payment type for invoices.
     *
     * @param args - must contains the list of invoices.
     *             Each invoice must contains the payment type.
     * @param <T> - type of result value.
     * @return a common payment type for given invoices.
     * @throws ResolveDependencyStrategyException when:
     *                      - given invoices contains invalid payment type;
     *                      - error reading payment type field.
     */
    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        List<IObject> invoices = (List<IObject>) args[0];
        try {
            String tmpType = paymentTypeF.in(invoices.get(0));
            if (!availableTypes.contains(tmpType)) {
                throw new ResolveDependencyStrategyException("Invalid payment type: " +
                        "list of available payment types hasn't contains this type.");
            }
            String paymentType;
            for (int i = invoices.size() - 1; i > 0; --i) {
                paymentType = paymentTypeF.in(invoices.get(i));
                if (!availableTypes.contains(paymentType)) {
                    throw new ResolveDependencyStrategyException("Invalid payment type: " +
                            "list of available payment types hasn't contains this type.");
                }
                if (!tmpType.equals(paymentType)) {
                    return (T) mixedType;
                }
            }

            return (T) tmpType;
        } catch (ReadValueException | InvalidArgumentException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }

}
