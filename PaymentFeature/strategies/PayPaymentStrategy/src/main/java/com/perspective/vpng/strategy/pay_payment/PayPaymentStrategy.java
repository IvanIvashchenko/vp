package com.perspective.vpng.strategy.pay_payment;

import com.perspective.vpng.strategy.pay_payment.exception.PayPaymentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Map;

/**
 * Pay payment rule.
 */
public class PayPaymentStrategy implements IResolveDependencyStrategy {

    private final Map<String, PayPaymentProcessor> payPaymentStrategyMap;

    /**
     * Default constructor.
     *
     * @param payPaymentStrategyMap a rule map with custom implementation of pay payment processors
     */
    public PayPaymentStrategy(final Map<String, PayPaymentProcessor> payPaymentStrategyMap) {
        this.payPaymentStrategyMap = payPaymentStrategyMap;
    }

    /**
     * Call pay payment process which depending on passed rule name.
     *
     * @param args - resolution arguments;
     *             args[0] must contains parameters for pay payment process rule implementation.
     *             args[1] must contains payment for pay.
     *             args[2] must contains rule name in parameters for pay payment object (args[0]).
     *
     * @param <T> - type of rule return value.
     *
     * @return a payment.
     *
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject payPaymentParams = (IObject) args[0];
            IObject payment = (IObject) args[1];
            String payStrategyFieldName = (String) args[2];
            IField payStrategyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), payStrategyFieldName);
            String strategy = payStrategyF.in(payPaymentParams, String.class);

            PayPaymentProcessor payPaymentProcessor = payPaymentStrategyMap.get(strategy);
            if (null == payPaymentProcessor)
                throw new PayPaymentException(String
                        .format("Can't find pay payment rule by type '%s'!", strategy));
            return (T) payPaymentProcessor.process(payment);
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }
}
