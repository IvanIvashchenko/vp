package com.perspective.vpng.strategy.pay_payment;

import com.perspective.vpng.strategy.pay_payment.exception.PayPaymentException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

/**
 * You need implement this interface if you want to create custom
 * pay payment behavior.
 */
@FunctionalInterface
public interface PayPaymentProcessor {

    /**
     * Calls in {@link PayPaymentStrategy#resolve(Object...)}
     *
     * @param payment as {@link IObject} for pay.
     * @return payment as IObject.
     * @throws PayPaymentException if error occurred when process payment.
     */
    IObject process(IObject payment) throws PayPaymentException;

}
