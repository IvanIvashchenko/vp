package com.perspective.vpng.strategy.pay_payment.exception;

public class PayPaymentException extends Exception {

    public PayPaymentException(String message) {
        super(message);
    }

    public PayPaymentException(String message, Throwable cause) {
        super(message, cause);
    }
}
