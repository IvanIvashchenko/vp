package com.perspective.vpng.strategy.check_payment_paid_status.exception;

public class CheckPaymentPaidStatusException extends Exception {

    public CheckPaymentPaidStatusException(String message) {
        super(message);
    }

    public CheckPaymentPaidStatusException(String message, Throwable cause) {
        super(message, cause);
    }
}
