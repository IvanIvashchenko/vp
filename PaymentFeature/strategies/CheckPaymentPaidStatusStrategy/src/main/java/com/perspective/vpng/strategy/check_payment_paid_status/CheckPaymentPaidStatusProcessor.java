package com.perspective.vpng.strategy.check_payment_paid_status;

import com.perspective.vpng.strategy.check_payment_paid_status.exception.CheckPaymentPaidStatusException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

/**
 * You need implement this interface if you want to create custom
 * check payment for paid status behavior.
 */
@FunctionalInterface
public interface CheckPaymentPaidStatusProcessor {

    /**
     * Calls in {@link CheckPaymentPaidStatusStrategy#resolve(Object...)}
     *
     * @param paymentNumber a payment identifier.
     *
     * @return check payment paid status result as {@link IObject}.
     */
    IObject process(String paymentNumber) throws CheckPaymentPaidStatusException;

}
