package com.perspective.vpng.strategy.check_payment_paid_status;

import com.perspective.vpng.strategy.check_payment_paid_status.exception.CheckPaymentPaidStatusException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Map;

/**
 * Checks paid payment status.
 */
public class CheckPaymentPaidStatusStrategy implements IResolveDependencyStrategy {

    private final Map<String, CheckPaymentPaidStatusProcessor> checkPaidStatusStrategyMap;

    /**
     * Default constructor.
     *
     * @param checkPaidStatusStrategyMap a rule map with custom implementation of check
     *                                   paid payment status processors.
     */
    public CheckPaymentPaidStatusStrategy(final Map<String, CheckPaymentPaidStatusProcessor> checkPaidStatusStrategyMap) {
        this.checkPaidStatusStrategyMap = checkPaidStatusStrategyMap;
    }

    /**
     * Call check paid payment status process which depending on passed rule name.
     *
     * @param args - resolution arguments;
     *             args[0] must contains parameters for check paid payment status process rule implementation.
     *             args[1] must contains payment identifier field name in parameters for check paid payment status
     *             object (args[0]).
     *             args[2] must contains rule name field name in parameters for check paid payment status
     *             object (args[0]).
     *
     * @param <T> - type of rule return value.
     *
     * @return a object with result if payment is paid,
     *              {
     *                  "command": "ok"
     *              }
     *              else if payment not change state
     *              {
     *                  "command": "wait"
     *              }
     *              end else if error
     *              {
     *                  "command": "error"
     *              }
     *
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject paramForStrategy = (IObject) args[0];
            String paymentNumberFieldName = (String) args[1];
            String checkPaymentPaidStatusRuleFieldName = (String) args[2];

            IField paymentNumberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentNumberFieldName);
            String paymentNumber = paymentNumberF.in(paramForStrategy);

            IField checkPaymentPaidStatusRuleF = IOC
                    .resolve(Keys.getOrAdd(IField.class.getCanonicalName()), checkPaymentPaidStatusRuleFieldName);
            String checkPaymentPaidStatusRuleName = checkPaymentPaidStatusRuleF.in(paramForStrategy);

            CheckPaymentPaidStatusProcessor checkPaymentPaidStatusProcessor = checkPaidStatusStrategyMap
                    .get(checkPaymentPaidStatusRuleName);

            if (null == checkPaymentPaidStatusProcessor)
                throw new CheckPaymentPaidStatusException(String
                        .format("Can't find check paid status rule by type '%s'!", checkPaymentPaidStatusRuleName));
            checkPaymentPaidStatusProcessor.process(paymentNumber);
            return (T) checkPaymentPaidStatusProcessor.process(paymentNumber);
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }
}
