package com.perspective.vpng.strategy.calculation_payment_amount.exception;

public class CalculatePaymentAmountException extends RuntimeException {

    public CalculatePaymentAmountException(String message) {
        super(message);
    }

    public CalculatePaymentAmountException(String message, Throwable cause) {
        super(message, cause);
    }
}
