package com.perspective.vpng.strategy.calculation_payment_amount;

import com.perspective.vpng.strategy.calculation_payment_amount.exception.CalculatePaymentAmountException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

/**
 * Calculate payment amount
 */
public class CalculationPaymentAmountStrategy implements IResolveDependencyStrategy {

    private final IField invoiceSumF;
    private final IField invoiceIsIncludedF;

    /**
     * Default constructor.
     *
     * @param invoiceSumFN invoice sum field name in invoice.
     * @param invoiceIsIncludedFN invoice included field name in invoice.
     */
    public CalculationPaymentAmountStrategy(final String invoiceSumFN, final String invoiceIsIncludedFN) {
        try {
            this.invoiceSumF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), invoiceSumFN);
            this.invoiceIsIncludedF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), invoiceIsIncludedFN);
        } catch (ResolutionException e) {
            throw new RuntimeException();
        }
    }

    /**
     * Finds an active gateway in payment method matrix collection
     *
     * @param args - resolution arguments;
     * @param <T> - type of rule return value.
     *
     * @return an payment amount type of {@link BigDecimal}.
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            Stream<IObject> invoices = ((List<IObject>) args[0]).stream();
            BigDecimal paymentAmount = invoices.map(invoice -> {
                try {
                    return invoiceSumF.<BigDecimal>in(invoice, BigDecimal.class);
                } catch (ReadValueException | InvalidArgumentException e) {
                    throw new CalculatePaymentAmountException("Can't find Sum field in Invoice");
                }
            }).reduce(BigDecimal.ZERO, BigDecimal::add);
            return (T) paymentAmount;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }
}
