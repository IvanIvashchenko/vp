package com.perspective.vpng.strategy.calculation_payment_amount;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
public class CalculationPaymentAmountStrategyTest {

    private IField invoiceSumF;
    private IField invoiceIsIncludedF;

    private IResolveDependencyStrategy strategy;

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        invoiceSumF = mock(IField.class);
        final String sumFN = "sum";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sum")).thenReturn(invoiceSumF);

        invoiceIsIncludedF = mock(IField.class);
        final String includedFN = "included";
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), includedFN)).thenReturn(invoiceIsIncludedF);

        strategy = new CalculationPaymentAmountStrategy(sumFN, includedFN);

    }

    @Test
    public void calculatePaymentAmountTest() throws Exception {
//        IObject invoice1 = mock(IObject.class);
        IObject invoice2 = mock(IObject.class);
        IObject invoice3 = mock(IObject.class);

//        when(invoiceSumF.in(invoice1, BigDecimal.class)).thenReturn(BigDecimal.valueOf(2.0));
//        when(invoiceIsIncludedF.in(invoice1, Boolean.class)).thenReturn(false);
        when(invoiceSumF.in(invoice2, BigDecimal.class)).thenReturn(BigDecimal.valueOf(3.0));
        when(invoiceIsIncludedF.in(invoice2, Boolean.class)).thenReturn(true);
        when(invoiceSumF.in(invoice3, BigDecimal.class)).thenReturn(BigDecimal.valueOf(2.5));
        when(invoiceIsIncludedF.in(invoice3, Boolean.class)).thenReturn(true);

        BigDecimal expectedResult = BigDecimal.valueOf(5.5);
        List<IObject> invoices = Arrays.asList(invoice2, invoice3);
        BigDecimal paymentAmount = strategy.resolve(invoices);
        assertEquals(expectedResult, paymentAmount);
    }

    @Test
    public void should_ThrowException_When_CalculatePaymentSum_WithReason_InvoiceNotContainSum()
            throws Exception {

        IObject invoice1 = mock(IObject.class);
        IObject invoice2 = mock(IObject.class);
        IObject invoice3 = mock(IObject.class);

        when(invoiceSumF.in(invoice1, BigDecimal.class)).thenThrow(ReadValueException.class);
        List<IObject> invoices = Arrays.asList(invoice1, invoice2, invoice3);
        try {
            strategy.resolve(invoices);
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(
                    ex.getMessage(),
                    "Can't find Sum field in Invoice"
            );
        }
    }
}