package com.perspective.vpng.strategy.payment_result_process.exception;

public class IncorrectSignatureException extends Exception {

    public IncorrectSignatureException(String message) {
        super(message);
    }

    public IncorrectSignatureException(String message, Throwable cause) {
        super(message, cause);
    }
}
