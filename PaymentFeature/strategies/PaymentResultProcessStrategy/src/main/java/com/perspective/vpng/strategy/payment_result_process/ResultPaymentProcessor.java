package com.perspective.vpng.strategy.payment_result_process;

import com.perspective.vpng.strategy.payment_result_process.exception.ResultPaymentProcessException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

/**
 * You need implement this interface if you want to create custom
 * result payment behavior for some gateway
 */
@FunctionalInterface
public interface ResultPaymentProcessor {

    /**
     * Calls in {@link PaymentResultProcessStrategy#resolve(Object...)}
     *
     * @param gatewayParams as {@link IObject}
     * @param storedRequestObj as {@link IObject}
     *
     *      Note: It doesn't return any value, if exception not thrown then payment result process successfully done.
     *
     */
    void process(IObject gatewayParams, IObject storedRequestObj) throws ResultPaymentProcessException;

}
