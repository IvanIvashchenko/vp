package com.perspective.vpng.strategy.payment_result_process.exception;

public class ResultPaymentProcessException extends Exception {

    public ResultPaymentProcessException(String message) {
        super(message);
    }

    public ResultPaymentProcessException(String message, Throwable cause) {
        super(message, cause);
    }
}
