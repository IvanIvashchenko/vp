package com.perspective.vpng.strategy.payment_result_process;

import com.perspective.vpng.strategy.payment_result_process.exception.ResultPaymentProcessException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Map;

/**
 * Checks payment on possibility to change it status to paid.
 */
public class PaymentResultProcessStrategy implements IResolveDependencyStrategy {
    private final Map<String, ResultPaymentProcessor> resultStrategyMap;

    /**
     * Default constructor.
     *
     * @param resultStrategyMap a rule map with custom {@link ResultPaymentProcessor} implementation,
     *                          prepared in plugin when register this rule in <code>IOC</code>.
     */
    public PaymentResultProcessStrategy(final Map<String, ResultPaymentProcessor> resultStrategyMap) {
        this.resultStrategyMap = resultStrategyMap;
    }

    /**
     * Call result payment process depending on the gateway type.
     *
     * @param args - resolution arguments;
     *             args[0] must contains parameters for result from gateway.
     *             args[1] must contains stored on step, create gateway request, parameters.
     *
     * @param <T> - type of rule return value.
     *
     * @return <code>true</code> if result payment process successful otherwise exception must be thrown.
     *
     * @throws ResolveDependencyStrategyException when error occurred.
     */
    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject resultParameters = (IObject) args[0];
            IObject storedRequestObj = (IObject) args[1];
            String gwType = (String) args[2];

            ResultPaymentProcessor resultPaymentProcessor = resultStrategyMap.get(gwType);
            if (null == resultPaymentProcessor)
                throw new ResultPaymentProcessException(String
                        .format("Can't find result payment rule by type '%s'!", gwType));
            resultPaymentProcessor.process(resultParameters, storedRequestObj);
            return (T) Boolean.valueOf(true);
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e.getMessage(), e);
        }
    }
}
