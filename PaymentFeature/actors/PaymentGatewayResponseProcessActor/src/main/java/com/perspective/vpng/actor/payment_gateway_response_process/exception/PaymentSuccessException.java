package com.perspective.vpng.actor.payment_gateway_response_process.exception;

public class PaymentSuccessException extends Exception {

    public PaymentSuccessException(String message) {
        super(message);
    }

    public PaymentSuccessException(String message, Throwable cause) {
        super(message, cause);
    }
}
