package com.perspective.vpng.actor.payment_gateway_response_process.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ISuccessPaymentMessage {
    /**
     * Gets gateway incoming request parameters wrapped to {@link IObject}.
     *
     * @return request parameters as {@link IObject}.
     * @throws ReadValueException when couldn't read <code>requestParameters</code> from message.
     */
    IObject getRequestParameters() throws ReadValueException;

    /**
     * Changes <code>successPaymentResult</code> field valued into message.
     *
     * @param successPaymentResult - result of success payment process.
     *
     * @throws ChangeValueException when couldn't changed <code>checkPaymentForPaidResult</code> into message.
     */
    void setSuccessPaymentResult(IObject successPaymentResult) throws ChangeValueException;

}
