package com.perspective.vpng.actor.payment_gateway_response_process.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface IResultPaymentMessage {
    String getOrderId() throws ReadValueException;
    String getSign() throws ReadValueException;
    String getGwType() throws ReadValueException;

    /**
     * Gets a stored request object.
     *
     * @return a request object created on the step of the request parameters build.
     * @throws ReadValueException when couldn't read <code>storedRequestObj</code> from message.
     */
    IObject getStoredRequestObj() throws ReadValueException;

}
