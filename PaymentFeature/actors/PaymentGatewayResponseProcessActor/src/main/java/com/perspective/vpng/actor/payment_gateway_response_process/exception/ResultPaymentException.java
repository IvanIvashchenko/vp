package com.perspective.vpng.actor.payment_gateway_response_process.exception;

public class ResultPaymentException extends Exception {

    public ResultPaymentException(String message) {
        super(message);
    }

    public ResultPaymentException(String message, Throwable cause) {
        super(message, cause);
    }
}
