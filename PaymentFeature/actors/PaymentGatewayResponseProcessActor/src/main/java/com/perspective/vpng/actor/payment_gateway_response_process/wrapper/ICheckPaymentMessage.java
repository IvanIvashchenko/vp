package com.perspective.vpng.actor.payment_gateway_response_process.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ICheckPaymentMessage {
    /**
     * Gets payment number.
     *
     * @return payment number.
     * @throws ReadValueException when couldn't read <code>paymentNumber</code> from message.
     */
    String getPaymentNumber() throws ReadValueException;

    /**
     * Changes <code>checkPaymentForPaidResult</code> field valued into message.
     *
     * @param checkPaymentForPaidResult - result of check payment for pay.
     *
     * @throws ChangeValueException when couldn't changed <code>checkPaymentForPaidResult</code> into message.
     */
    void setCheckPaymentForPaidResult(IObject checkPaymentForPaidResult) throws ChangeValueException;

}
