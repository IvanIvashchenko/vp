package com.perspective.vpng.actor.payment_gateway_response_process.exception;

public class PayPaymentException extends Exception {

    public PayPaymentException(String message) {
        super(message);
    }

    public PayPaymentException(String message, Throwable cause) {
        super(message, cause);
    }
}
