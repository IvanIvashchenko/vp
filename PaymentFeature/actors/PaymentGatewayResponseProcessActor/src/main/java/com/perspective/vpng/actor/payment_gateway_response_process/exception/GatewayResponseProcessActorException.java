package com.perspective.vpng.actor.payment_gateway_response_process.exception;

public class GatewayResponseProcessActorException extends Exception {

    public GatewayResponseProcessActorException(String message) {
        super(message);
    }

    public GatewayResponseProcessActorException(String message, Throwable cause) {
        super(message, cause);
    }
}
