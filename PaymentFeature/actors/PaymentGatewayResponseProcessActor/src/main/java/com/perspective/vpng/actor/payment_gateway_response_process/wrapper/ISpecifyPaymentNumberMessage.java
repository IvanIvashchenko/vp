package com.perspective.vpng.actor.payment_gateway_response_process.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ISpecifyPaymentNumberMessage {
    String getPaymentNumber() throws ReadValueException;

    /**
     * Changes <code>payment</code> field valued into message.
     *
     * @param payment - payment value.
     *
     * @throws ChangeValueException when couldn't changed <code>payment</code> into message.
     */
    void setPayment(IObject payment) throws ChangeValueException;

    /**
     * Changes <code>token</code> field valued into message.
     * @param tokens contains token of async operation, which identifies it into collection
     * @throws ChangeValueException when couldn't changed <code>token</code> into message.
     */
    void setOperationTokens(List<String> tokens) throws ChangeValueException;

    /**
     * Changes <code>asyncOperation</code> field valued into message.
     * @param asyncOperation asynchronous operation
     * @throws ChangeValueException when couldn't changed <code>asyncOperation</code> into message.
     */
    void setAsyncOperation(IObject asyncOperation) throws ChangeValueException;
}
