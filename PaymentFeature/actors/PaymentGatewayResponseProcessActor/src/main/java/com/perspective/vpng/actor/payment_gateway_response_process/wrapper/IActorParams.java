package com.perspective.vpng.actor.payment_gateway_response_process.wrapper;

import com.perspective.vpng.actor.payment_gateway_response_process.PaymentGatewayResponseProcessActor;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for params for {@link PaymentGatewayResponseProcessActor} actor.
 */
public interface IActorParams {

    String getPayPaymentRuleName() throws ReadValueException;

    String getPayPaymentStrategyName() throws ReadValueException;

    String getPayPaymentRuleFieldName() throws ReadValueException;

    String getCheckPaymentPaidStatusRuleName() throws ReadValueException;

    String getCheckPaymentPaidStatusStrategyName() throws ReadValueException;

    String getCheckPaymentPaidStatusRuleFieldName() throws ReadValueException;

    String getPaymentNumberFieldName() throws ReadValueException;

    String getInvoicesFieldName() throws ReadValueException;

    String getGuidFieldName() throws ReadValueException;

    String getDateFieldName() throws ReadValueException;

    String getAsyncOperationCollectionName() throws ReadValueException;

    String getOperationIdFieldName() throws ReadValueException;
}
