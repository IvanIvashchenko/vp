package com.perspective.vpng.actor.payment_gateway_response_process.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface IPayPaymentMessage {
    /**
     * Gets payment. Must be placed before this actor from async operation collection.
     *
     * @return payment as {@link IObject}.
     * @throws ReadValueException when couldn't read <code>payment</code> from message.
     */
    IObject getPayment() throws ReadValueException;

    /**
     * Changes <code>payment</code> field valued into message.
     *
     * @param payment - payment value.
     *
     * @throws ChangeValueException when couldn't changed <code>payment</code> into message.
     */
    void setPayment(IObject payment) throws ChangeValueException;

    /**
     * Changes <code>invoicesIds</code> field valued into message.
     * @param invoicesIds - guids of invoices from payment
     * @throws ChangeValueException when couldn't changed <code>invoicesIds</code> into message.
     */
    void setInvoicesIds(List<String> invoicesIds) throws ChangeValueException;

}
