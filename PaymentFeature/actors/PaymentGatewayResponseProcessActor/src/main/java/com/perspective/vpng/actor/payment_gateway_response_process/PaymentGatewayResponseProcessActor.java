package com.perspective.vpng.actor.payment_gateway_response_process;

import com.perspective.vpng.actor.payment_gateway_response_process.exception.GatewayResponseProcessActorException;
import com.perspective.vpng.actor.payment_gateway_response_process.exception.PayPaymentException;
import com.perspective.vpng.actor.payment_gateway_response_process.exception.PaymentSuccessException;
import com.perspective.vpng.actor.payment_gateway_response_process.exception.ResultPaymentException;
import com.perspective.vpng.actor.payment_gateway_response_process.wrapper.*;
import com.perspective.vpng.database.field.IDBFieldsHolder;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.ifield_name.IFieldName;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Actor for work with incoming gateway request from gateway.
 */
public class PaymentGatewayResponseProcessActor {

    private final String paymentNumberFieldName;

    private final String payPaymentRuleName;
    private final String payPaymentStrategyName;
    private final String payPaymentRuleFieldName;

    private final String checkPaymentPaidStatusRuleName;
    private final String checkPaymentPaidStatusStrategyName;
    private final String checkPaymentPaidStatusRuleFieldName;

    private final IField invoicesField;
    private final IField guidField;
    private final IField dateField;
    private IDBFieldsHolder fieldsHolder;
    private final IField operationIdF;
    private final IField asyncDataF;

    private final IPool connectionPool;
    private final String asyncOperationCollectionName;

    private DateTimeFormatter formatter;

    /**
     * Constructor by payment gateway response process actor params.
     *
     * @param params must contains
     *               1. Payment number field name;
     *               2. Pay payment rule name;
     *               3. Pay payment rule name;
     *               4. Pay payment rule field name;
     *               5. Check payment paid status rule name;
     *               6. Check payment paid status rule name;
     *               7. Check payment paid status rule field name;
     *               8. Invoices field name;
     *               9. Guid field name.
     * @throws GatewayResponseProcessActorException if error occurred when construct actor.
     */
    public PaymentGatewayResponseProcessActor(IActorParams params) throws GatewayResponseProcessActorException {
        try {
            this.paymentNumberFieldName = params.getPaymentNumberFieldName();

            this.payPaymentStrategyName = params.getPayPaymentStrategyName();
            this.payPaymentRuleName = params.getPayPaymentRuleName();
            this.payPaymentRuleFieldName = params.getPayPaymentRuleFieldName();
            this.asyncOperationCollectionName = params.getAsyncOperationCollectionName();
            Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
            this.fieldsHolder = IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()));
            this.operationIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getOperationIdFieldName());
            this.asyncDataF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "asyncData");

            this.checkPaymentPaidStatusRuleName = params.getCheckPaymentPaidStatusRuleName();
            this.checkPaymentPaidStatusStrategyName = params.getCheckPaymentPaidStatusStrategyName();
            this.checkPaymentPaidStatusRuleFieldName = params.getCheckPaymentPaidStatusRuleFieldName();

            this.invoicesField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getInvoicesFieldName());
            this.guidField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getGuidFieldName());
            this.dateField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getDateFieldName());

            this.formatter = IOC.resolve(Keys.getOrAdd("datetime_formatter"));

        } catch (ResolutionException | ReadValueException e) {
            throw new GatewayResponseProcessActorException("Error occurred when construct actor!", e);
        }
    }

    /**
     * It analyses gateway response after pay payment on gateway and if status
     * SUCCESS then return success object as below:
     *              {
     *                    "result" : "SUCCESS"
     *              }
     *
     *                  else
     *              {
     *                   "result": "ERROR"
     *              }
     * and client must react on this responded object.
     *
     * @param message must contain parameters passed from gateway and wrapped to object for example
     *                {
     *                    "gwType": "vsep",
     *                    "orderId": "123456789",
     *                    "status": "0"
     *                }
     * @throws PaymentSuccessException when error occurred.
     */
    public void successPayment(ISuccessPaymentMessage message) throws PaymentSuccessException {
        try {
            IObject succesPaymentResult = IOC.resolve(
                    Keys.getOrAdd("paymentSuccessProcessStrategy"),
                    message.getRequestParameters()
            );
            message.setSuccessPaymentResult(succesPaymentResult);
        } catch (ReadValueException | ResolutionException | ChangeValueException e) {
            throw new PaymentSuccessException("Error occured when success payment!", e);
        }
    }

    public void specifyPaymentNumberFromParam(ISpecifyPaymentNumberMessage message) throws GatewayResponseProcessActorException {

        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> operations = new LinkedList<>();
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    asyncOperationCollectionName,
                    prepareAsyncOpSearchQueryByOperationId(message.getPaymentNumber()),
                    (IAction<IObject[]>) foundDocs -> operations.addAll(Arrays.asList(foundDocs))
            );
            searchTask.execute();

            IObject operation = operations.get(0);
            message.setPayment(asyncDataF.in(operation));
            //set some list by path for correct close of async operation
            message.setOperationTokens(Collections.emptyList());
            message.setAsyncOperation(operation);
        } catch (Exception e) {
            throw new GatewayResponseProcessActorException("Error occure when specify payment number " +
                    "from GW incoming param!", e);
        }
    }

    /**
     * Do some checks before pay payment, such as check incoming signature.
     * Use <code>paymentResultProcessStrategy</code> for checks.
     *
     * @param message must contain:Params
     *                1. Gateway parameters for check wrapped to {@link IObject};
     *                     object example
     *                      {
     *                        "orderId": "123456789",
     *                        "sign": "9ce252752ab6a9d8cca05351a530a5a2c162266c"
     *                      }
     *                2. Stored to Payment on create gateway payment request parameters.
     * @throws ResultPaymentException when message at inconsistent state.
     * @throws GatewayResponseProcessActorException when needed rule not found in <code>IOC</code>.
     */
    public void resultPayment(IResultPaymentMessage message) throws ResultPaymentException, GatewayResponseProcessActorException {
        try {
            IObject resultParams = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IFieldName orderIdFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "orderId");
            IFieldName signFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "sign");
            resultParams.setValue(orderIdFN, message.getOrderId());
            resultParams.setValue(signFN, message.getSign());

            IObject storedRequestObj = message.getStoredRequestObj();
            String gwType = message.getGwType();
            IOC.resolve(
                    Keys.getOrAdd("paymentResultProcessStrategy"),
                    resultParams,
                    storedRequestObj,
                    gwType
            );
        } catch (ReadValueException | ChangeValueException | InvalidArgumentException e) {
            throw new GatewayResponseProcessActorException(e.getMessage(), e);
        } catch (ResolutionException e) {
            throw new ResultPaymentException(e.getMessage(), e);
        }
    }

    /**
     * Pay payment. Insert payment object to paid payment storage.
     *
     * Use <code>PayPaymentStrategy</code> rule.
     *
     * @param message must contain payment object.
     * @throws PayPaymentException
     */
    public void payPayment(IPayPaymentMessage message) throws PayPaymentException {
        try {
            IObject paramForPayStrategy = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IField payStrategyF = IOC.
                    resolve(Keys.getOrAdd(IField.class.getCanonicalName()), payPaymentRuleFieldName);

            payStrategyF.out(paramForPayStrategy, payPaymentRuleName);
            IObject paymentFromAsyncOperations = message.getPayment();
            dateField.out(paymentFromAsyncOperations, LocalDateTime.now().format(formatter));
            //just insert into DB
            IObject payment = IOC.resolve(
                    Keys.getOrAdd(payPaymentStrategyName),
                    paramForPayStrategy,
                    paymentFromAsyncOperations,
                    payPaymentRuleFieldName
            );
            message.setPayment(payment);

            List<IObject> invoices = invoicesField.in(payment);
            List<String> invoicesIds = new ArrayList<>();
            for (IObject invoice : invoices) {
                invoicesIds.add(guidField.in(invoice));
            }
            message.setInvoicesIds(invoicesIds);
        } catch (ResolutionException | InvalidArgumentException | ChangeValueException | ReadValueException e) {
            throw new PayPaymentException("Can't pay payment!", e);
        }
    }

    /**
     * It try gets payment from paid payment storage and if it finds then
     * it set to message object as below
     *          {
     *            "command": "ok"
     *          }
     * else if it not found set message as below
     *          {
     *              "command": "wait"
     *          }
     *
     * If result with <code>wait</code> then it means the system has not yet received information
     * on the transfers from the gateway and client must check status late.
     *
     * Use <code>CheckPaymentPaidStatusStrategy</code> rule.
     *
     * @param message must contain payment identifier.
     * @throws GatewayResponseProcessActorException when error occurred in handler.
     */
    public void checkPaymentForPaid(ICheckPaymentMessage message) throws GatewayResponseProcessActorException {
        try {
            IObject paramForCheckPaidStatusStrategy = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IField paymentNumberF = IOC.
                    resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentNumberFieldName);

            IField checkPaymentPaidStatusRuleF = IOC.
                    resolve(Keys.getOrAdd(IField.class.getCanonicalName()), checkPaymentPaidStatusRuleFieldName);

            String paymentNumber = message.getPaymentNumber();

            paymentNumberF.out(paramForCheckPaidStatusStrategy, paymentNumber);
            checkPaymentPaidStatusRuleF.out(paramForCheckPaidStatusStrategy, checkPaymentPaidStatusRuleName);


            IObject checkPaymentForPaidResult = IOC.resolve(
                    Keys.getOrAdd(checkPaymentPaidStatusStrategyName),
                    paramForCheckPaidStatusStrategy,
                    paymentNumberFieldName,
                    checkPaymentPaidStatusRuleFieldName
            );
            message.setCheckPaymentForPaidResult(checkPaymentForPaidResult);
        } catch (ReadValueException | InvalidArgumentException | ChangeValueException | ResolutionException e) {
            throw new GatewayResponseProcessActorException(e.getMessage(), e);
        }
    }

    /**
     * Method for prepare search query object for search document
     * from async operation collection by field value into asyncData
     * Yes, this is dirty hack
     * TODO:: Think about another approach
     * @param operationId operationId value
     * @return search query
     * @throws ResolutionException
     * @throws ChangeValueException
     * @throws InvalidArgumentException
     */
    private IObject prepareAsyncOpSearchQueryByOperationId(final String operationId)
            throws ResolutionException, ChangeValueException, InvalidArgumentException {
        IKey iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
        IObject searchQuery = IOC.resolve(iObjectKey);
        IObject searchFilter = IOC.resolve(iObjectKey);
        IObject criteria = IOC.resolve(iObjectKey);
        IObject page = IOC.resolve(iObjectKey);

        operationIdF.out(searchFilter, criteria);
        fieldsHolder.conditions().eq().out(criteria, operationId);

        fieldsHolder.searchQuery().pageNumber().out(page, 1);
        fieldsHolder.searchQuery().pageSize().out(page, 1);

        fieldsHolder.searchQuery().filter().out(searchQuery, searchFilter);
        fieldsHolder.searchQuery().page().out(searchQuery, page);

        return searchQuery;
    }
}
