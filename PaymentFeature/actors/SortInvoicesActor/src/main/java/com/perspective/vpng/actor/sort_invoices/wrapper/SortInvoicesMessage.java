package com.perspective.vpng.actor.sort_invoices.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface SortInvoicesMessage {
    List<IObject> getInvoices() throws ReadValueException;
    void setResult(IObject result) throws ChangeValueException;
    void setProviderIds(List<String> ids) throws ChangeValueException;
}
