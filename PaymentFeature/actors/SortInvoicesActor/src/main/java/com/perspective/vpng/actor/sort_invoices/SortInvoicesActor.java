package com.perspective.vpng.actor.sort_invoices;

import com.perspective.vpng.actor.sort_invoices.exception.SortInvoicesException;
import com.perspective.vpng.actor.sort_invoices.wrapper.SortInvoicesMessage;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.ifield_name.IFieldName;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;

public class SortInvoicesActor {
    private IField providerF;
    private IField providerIdF;

    public SortInvoicesActor() {
        try {
            this.providerF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "поставщик");
            this.providerIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "id");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Distribution on providers that specified in invoices
     * @param message message
     * @throws SortInvoicesException when any exception occurred
     */
    public void sortInvoices(SortInvoicesMessage message) throws SortInvoicesException {
        try {
            List<IObject> invoices = message.getInvoices();
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            List<String> providerIds = new ArrayList<>();

            for (IObject invoice : invoices) {
                IObject provider = providerF.in(invoice);
                String providerId = providerIdF.in(provider);

                IFieldName providerIdFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), providerId);

                List<IObject> providerList = (List<IObject>) result.getValue(providerIdFN);

                if (providerList == null) {
                    providerIds.add(providerIdFN.toString());
                    providerList = new ArrayList<>();
                    result.setValue(providerIdFN, providerList);
                }

                providerList.add(invoice);
            }

            message.setProviderIds(providerIds);
            message.setResult(result);
        } catch (Exception e) {
            throw new SortInvoicesException("Failed to sort invoices", e);
        }
    }
}
