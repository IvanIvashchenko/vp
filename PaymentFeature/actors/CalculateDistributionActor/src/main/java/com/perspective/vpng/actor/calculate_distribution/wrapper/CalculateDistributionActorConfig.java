package com.perspective.vpng.actor.calculate_distribution.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CalculateDistributionActorConfig {
    String getServicesFieldName() throws ReadValueException;
    String getDistributionsFieldName() throws ReadValueException;
}
