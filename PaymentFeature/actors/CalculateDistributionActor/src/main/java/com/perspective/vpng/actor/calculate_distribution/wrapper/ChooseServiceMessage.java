package com.perspective.vpng.actor.calculate_distribution.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ChooseServiceMessage {
    List<IObject> getInvoices() throws ReadValueException;
    void setServiceToDistribution(IObject serviceToDistribution) throws ChangeValueException;
    void setInvoice(IObject invoice) throws ChangeValueException;
}
