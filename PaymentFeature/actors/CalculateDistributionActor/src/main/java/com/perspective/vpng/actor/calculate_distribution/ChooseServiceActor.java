package com.perspective.vpng.actor.calculate_distribution;

import com.perspective.vpng.actor.calculate_distribution.exception.ChooseServiceException;
import com.perspective.vpng.actor.calculate_distribution.wrapper.ChooseServiceActorConfig;
import com.perspective.vpng.actor.calculate_distribution.wrapper.ChooseServiceMessage;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class ChooseServiceActor {
    private static IField isCommissionF;
    private static IField characteristicF;
    private static IField payerIdF;
    private static IField invoiceIdF;

    private IField servicesField;
    private IField payerIdFieldNameField;
    private IField withCharacteristicsField;
    private IField distributionsField;
    private IField invoiceIdField;
    private Integer serviceNumber;

    private List<IObject> servicesToDistribution;
    private String currentInvoiceId;

    public ChooseServiceActor(final ChooseServiceActorConfig config) throws ChooseServiceException {
        try {
            isCommissionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "isCommission");
            characteristicF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "characteristic");
            payerIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "payerId");
            invoiceIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoiceId");

            servicesField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getServicesFieldName());
            //for example config.getPayerIdFieldNameFN() = payerIdFieldName
            payerIdFieldNameField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getPayerIdFieldNameFN());
            withCharacteristicsField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getWithCharacteristicsFieldName());
            //распределено
            distributionsField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getDistributionsFieldName());
            invoiceIdField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getInvoiceIdFieldName());
            servicesToDistribution = new ArrayList<>();
            serviceNumber = 0;
            currentInvoiceId = "";

        } catch (ResolutionException | ReadValueException e) {
            throw new ChooseServiceException("Can't create ChooseServiceActor", e);
        }
    }

    //there is a list of invoices into the message
    public void chooseService(final ChooseServiceMessage message) throws ChooseServiceException {

        try {
            List<IObject> invoices = message.getInvoices();
            if (serviceNumber == 0) {

                for (IObject invoice : invoices) {

                    String invoiceId = invoiceIdField.in(invoice);

                    String payerIdFieldName = payerIdFieldNameField.in(invoice);
                    IField payerIdField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), payerIdFieldName);
                    String payerId = payerIdField.in(invoice);
                    Boolean withCharacteristics = withCharacteristicsField.in(invoice);
                    List<IObject> services = servicesField.in(invoice);

                    servicesToDistribution.addAll(services.stream()
                        .filter(s -> {
                            try {
                                Integer characteristic = characteristicF.in(s);
                                Boolean isCommission = isCommissionF.in(s);
                                boolean result = (!isTrue(isCommission)) &&
                                        (!withCharacteristics || (characteristic != null && characteristic != 0));
                                if (result) {
                                    payerIdF.out(s, payerId);
                                    invoiceIdF.out(s, invoiceId);
                                }
                                return result;
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }).collect(Collectors.toList())
                    );

                    distributionsField.out(invoice, Collections.EMPTY_LIST);
                }
            }
            IObject serviceToDistribution = servicesToDistribution.get(serviceNumber);
            message.setServiceToDistribution(serviceToDistribution);
            serviceNumber++;

            //logic for matching service and invoice
            String invoiceId = invoiceIdF.in(serviceToDistribution);
            if (!invoiceId.equals(currentInvoiceId)) {
                for (IObject invoice : invoices) {
                    if (invoiceIdField.in(invoice).equals(invoiceId)) {
                        message.setInvoice(invoice);
                        currentInvoiceId = invoiceId;
                        break;
                    }
                }
            }

        } catch (InvalidArgumentException | ResolutionException | ChangeValueException | ReadValueException e) {
            throw new ChooseServiceException("Error during choice service for distribution", e);
        }
    }

    private boolean isTrue(final Boolean arg) {
        return (arg != null && arg);
    }
}
