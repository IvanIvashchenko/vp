package com.perspective.vpng.actor.calculate_distribution.exception;

public class CalculateDistributionException extends Exception {

    public CalculateDistributionException(final String message) {
        super(message);
    }

    public CalculateDistributionException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CalculateDistributionException(final Throwable cause) {
        super(cause);
    }
}
