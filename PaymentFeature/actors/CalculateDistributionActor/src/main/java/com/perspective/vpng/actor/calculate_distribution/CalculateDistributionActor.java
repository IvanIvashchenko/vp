package com.perspective.vpng.actor.calculate_distribution;

import com.perspective.vpng.actor.calculate_distribution.exception.CalculateDistributionException;
import com.perspective.vpng.actor.calculate_distribution.wrapper.CalculateDistributionActorConfig;
import com.perspective.vpng.actor.calculate_distribution.wrapper.DistributionMessage;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CalculateDistributionActor {

    private final static BigDecimal PERCENT_COEFFICIENT = new BigDecimal(100);

    private static IField recieversF;
    private static IField servicesF;
    private static IField serviceIdF;
    private static IField amountF;
    private static IField idF;
    private static IField restF;
    private static IField fixF;
    private static IField percentF;
    private static IField destRecipientIdF;
    private static IField dateF;
    private static IField isCommissionF;

    private IPool connectionPool;
    private ICachedCollection receiveRuleCollection;
    private DateTimeFormatter formatter;

    public CalculateDistributionActor(CalculateDistributionActorConfig config) throws CalculateDistributionException {

        try {
            receiveRuleCollection = IOC.resolve(
                    Keys.getOrAdd(
                            ICachedCollection.class.getCanonicalName()),
                    "receive_rule",
                    "ид_услуги"
            );

            Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);

            formatter = IOC.resolve(Keys.getOrAdd("datetime_formatter"));
            servicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), config.getServicesFieldName());

            serviceIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "ид_услуги");
            amountF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "сумма");
            idF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "ид");
            restF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "остаток");
            fixF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "fix");
            percentF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "percent");
            destRecipientIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "ид_получателя");
            dateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "дата");
            recieversF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "получатели");
            isCommissionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "комиссия");
        } catch (Exception e) {
            throw new CalculateDistributionException("Failed to create CalculateDistributionActor", e);
        }
    }

    public void calculateDistribution(final DistributionMessage message) throws CalculateDistributionException {

        try {
            List<IObject> invoices = message.getInvoices();
            Map<String, List<IObject>> resultDistributions = new HashMap<>();

            for (IObject invoice : invoices) {
                List<IObject> services = servicesF.in(invoice);

                for (IObject service : services) {
                    if (!isTrue(isCommissionF.in(service))) {
                        String serviceId = serviceIdF.in(service);
                        IObject receiveRule = receiveRuleCollection.getItems(serviceId).get(0);
                        List<IObject> receivers = recieversF.in(receiveRule);

                        BigDecimal serviceAmount = amountF.in(service, BigDecimal.class);
                        BigDecimal restServiceAmount = new BigDecimal(serviceAmount.toString());
                        List<IObject> distributions = new LinkedList<>();
                        IObject restRecipientObject = null;

                        for (IObject recipientRule : receivers) {
                            Boolean restFlag = restF.in(recipientRule);
                            if (isTrue(restFlag)) {
                                if (restRecipientObject != null) {
                                    throw new CalculateDistributionException("Unacceptable two or more receivers with rest modification");
                                }
                                restRecipientObject = recipientRule;
                                continue;
                            }
                            BigDecimal fraction = getFraction(recipientRule, serviceAmount);
                            restServiceAmount = restServiceAmount.subtract(fraction);
                            distributions.add(createDistribution(fraction, serviceId, recipientRule));
                        }
                        checkRestAmount(restServiceAmount);
                        if (restRecipientObject != null) {
                            distributions.add(createDistribution(restServiceAmount, serviceId, restRecipientObject));
                        } else {
                            IObject luckyDistribution = distributions.get(distributions.size() - 1);
                            amountF.out(luckyDistribution, ((BigDecimal) amountF.in(luckyDistribution)).add(restServiceAmount));
                        }

                        for (IObject distribution : distributions) {
                            List recipientList = resultDistributions.get(destRecipientIdF.in(distribution));
                            if (recipientList != null) {
                                recipientList.add(distribution);
                            } else {
                                recipientList = new ArrayList<IObject>() {{
                                    add(distribution);
                                }};
                                resultDistributions.put(destRecipientIdF.in(distribution), recipientList);
                            }
                        }
                    }
                }
            }

            message.setDistributions(resultDistributions);
        } catch (Exception e) {
            throw new CalculateDistributionException("Error during calculate distribution", e);
        }
    }

    private BigDecimal getFraction(IObject recipientRule, BigDecimal wholeAmount) throws Exception {
        BigDecimal fix = fixF.in(recipientRule);
        BigDecimal percent = percentF.in(recipientRule);
        BigDecimal result = new BigDecimal(0);
        if (fix != null) {
            result = result.add(fix);
        }
        if (percent != null) {
            percent = percent.divide(PERCENT_COEFFICIENT);
            result = result.add(wholeAmount.multiply(percent));
        }
        return result;
    }

    private IObject createDistribution(BigDecimal fraction, String serviceId, IObject recipientRule)
        throws Exception {

        //TODO:: write the rule
        IObject distribution = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        amountF.out(distribution, fraction);
        destRecipientIdF.out(distribution, idF.in(recipientRule));
        serviceIdF.out(distribution, serviceId);
        dateF.out(distribution, LocalDateTime.now().format(formatter));

        return distribution;
    }

    private void checkRestAmount(final BigDecimal restServiceAmount) throws CalculateDistributionException {
        if (restServiceAmount.signum() < 0) {
            throw new CalculateDistributionException("Real amount is less than sum which is calculated by rules");
        }
    }

    private boolean isTrue(final Boolean arg) {
        return (arg != null && arg);
    }
}
