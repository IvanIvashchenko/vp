package com.perspective.vpng.actor.calculate_distribution.exception;

public class ChooseServiceException extends Exception {

    public ChooseServiceException(final String message) {
        super(message);
    }

    public ChooseServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
