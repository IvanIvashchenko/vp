package com.perspective.vpng.actor.calculate_distribution.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ChooseServiceActorConfig {

    String getServicesFieldName() throws ReadValueException;
    String getPayerIdFieldNameFN() throws ReadValueException;
    String getWithCharacteristicsFieldName() throws ReadValueException;
    String getDistributionsFieldName() throws ReadValueException;
    String getInvoiceIdFieldName() throws ReadValueException;
}
