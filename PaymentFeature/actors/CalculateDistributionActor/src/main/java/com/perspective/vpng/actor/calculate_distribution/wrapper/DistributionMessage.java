package com.perspective.vpng.actor.calculate_distribution.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;
import java.util.Map;

public interface DistributionMessage {
    List<IObject> getInvoices() throws ReadValueException;
    void setDistributions(Map<String, List<IObject>> distributions) throws ChangeValueException;
}
