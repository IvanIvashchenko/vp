package com.perspective.vpng.actor.payment;

import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class,Keys.class})
public class PaymentActorTest {

//    private MakePaymentActor actor;
//    private MakePaymentActorMessage actorMessage;
//    private Payment payment;
//    private ShoppingCart shoppingCart;
//    private Invoice invoice;
//    private ICachedCollection collectionTests;

//    @Before
//    public void setUp() throws Exception {
//        mockStatic(IOC.class);
//        mockStatic(Keys.class);
//
//        actorMessage = mock(MakePaymentActorMessage.class);
//        payment = mock(Payment.class);
//        shoppingCart = mock(ShoppingCart.class);
//        invoice = mock(Invoice.class);
//        collectionTests = mock(ICachedCollection.class);
//
//
//        IKey keyForICachedCollection = mock(IKey.class);
//        when(Keys.getOrAdd(ICachedCollection.class.getCanonicalName())).thenReturn(keyForICachedCollection);
//        when(IOC.resolve(keyForICachedCollection, "paymentMethods")).thenReturn(collectionTests);
//
//        IKey keyForPayment = mock(IKey.class);
//        when(Keys.getOrAdd(Payment.class.getCanonicalName())).thenReturn(keyForPayment);
//        when(IOC.resolve(keyForPayment)).thenReturn(payment);
//
//        MakePaymentActorParam params = mock(MakePaymentActorParam.class);
//        when(params.getPaymentMethodsCollectionName()).thenReturn("paymentMethods");
////        when(params.getPaymentAsyncCollectionName()).thenReturn("payment");
//
//        actor = new MakePaymentActor(params);
//
//    }

    @Test
    public void createPaymentTest() throws Exception {

//        when(shoppingCart.getInvoices()).thenReturn(Arrays.asList(invoice));
//        when(actorMessage.getShoppingCart()).thenReturn(shoppingCart);
//
//        actor.createPayment(actorMessage);
//        verify(actorMessage).setPayment(payment);
    }

    @Test
    public void calculatePaymentAmount() throws Exception {
//        Invoice invoice_0 = mock(Invoice.class);
//        Invoice invoice_1 = mock(Invoice.class);
//        Invoice invoice_2 = mock(Invoice.class);
//        when(invoice_0.getAmount()).thenReturn(BigDecimal.valueOf(.01));
//        when(invoice_0.getIncluded()).thenReturn(true);
//        when(invoice_1.getAmount()).thenReturn(BigDecimal.valueOf(.04));
//        when(invoice_1.getIncluded()).thenReturn(true);
//        when(invoice_2.getAmount()).thenReturn(BigDecimal.valueOf(100.0));
//        when(invoice_2.getIncluded()).thenReturn(false);
//        when(actorMessage.getPayment()).thenReturn(payment);
//        when(payment.getInvoices()).thenReturn(Arrays.asList(invoice_0, invoice_1, invoice_2));
//        actor.calculatePayment(actorMessage);
//        verify(payment).setAmount(BigDecimal.valueOf(.05));
    }

    @Test
    public void specifyPaymentMethodTest() throws Exception {
//        IObject test = mock(IObject.class);
//        List<IObject> paymentMethods = new ArrayList<>();
//        paymentMethods.add(test);
//        when(actorMessage.getPayment()).thenReturn(payment);
//        when(collectionTests.getItems("enabled")).thenReturn(paymentMethods);
//
//        actor.specifyPaymentMethods(actorMessage);
//
//        verify(payment).setPaymentMethods(paymentMethods);
    }

}