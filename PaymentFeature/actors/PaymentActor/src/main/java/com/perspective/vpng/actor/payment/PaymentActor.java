package com.perspective.vpng.actor.payment;

import com.perspective.vpng.actor.payment.exception.CreatePaymentException;
import com.perspective.vpng.actor.payment.wrapper.ICreatePaymentMessage;
import com.perspective.vpng.actor.payment.wrapper.IPaymentActorParam;
import com.perspective.vpng.util.sequence_holder.SequenceHolder;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.time.Instant;
import java.util.List;
import java.util.Random;

/**
 * Actor creates payment
 */
public class PaymentActor {
    private static IField paymentIdF;
    private static IField invoicesF;
    private static IField userGuidF;
    private static IField descriptionF;
    private static IField paymentMethodF;
    private static IField paymentKindF;
    private static IField sumF;
    private static IField numberF;

    private final IKey paymentKindDeterminationStrategyKey;
    private final IKey paymentCalculationStrategyKey;

    private SequenceHolder sequenceHolder;

    public PaymentActor(final IPaymentActorParam param) throws CreatePaymentException {
        try {
            paymentIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "operationId");
            invoicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoices");
            userGuidF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userGuid");
            descriptionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "description");
            paymentMethodF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentMethod");
            paymentKindF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentKind");
            sumF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sum");
            numberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "number");

            this.paymentKindDeterminationStrategyKey = Keys.getOrAdd(param.getPaymentKindDeterminationStrategy());
            this.paymentCalculationStrategyKey = Keys.getOrAdd(param.getCalculatePaymentAmountStrategy());
            this.sequenceHolder = IOC.resolve(Keys.getOrAdd("SequenceHolder"), param.getCollectionName(), param.getInitialSequenceValue());
        } catch (ResolutionException | ReadValueException e) {
            throw new CreatePaymentException(String.format("Error occurred when construct actor '%s'", this.getClass().getCanonicalName()), e);
        }
    }

    /**
     * Creates new payment.
     *
     * @param message must contain operationId, invoices, userGuid, paymentMethod. Example bellow.
     *
     *                {
     *                  "userGuid":"userId-45-id",
     *                  "invoices": [
     *                       {
     *                           "included": true,
     *                           "sum": 11.12
     *                       },
     *                       {
     *                           "included": true,
     *                           "sum": 10.0
     *                       },
     *                       {
     *                           "included": false,
     *                           "sum": 100.0
     *                       }
     *                   ],
     *                   "paymentMethod": "creditCard"
     *                 }
     *
     * @throws CreatePaymentException when error occurred
     */
    public void createPayment(ICreatePaymentMessage message) throws CreatePaymentException {
        try {
            IObject payment = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

            paymentIdF.out(payment, generateNumericGuid());
            List<IObject> invoices = message.getInvoices();
            invoicesF.out(payment, invoices);
            userGuidF.out(payment, (message.getUserGuid() == null) ? "" : message.getUserGuid());
            descriptionF.out(payment, (message.getDescription() == null) ? "" : message.getDescription());
            paymentMethodF.out(payment, message.getPaymentMethod());
            paymentKindF.out(payment, IOC.resolve(paymentKindDeterminationStrategyKey, invoices));
            sumF.out(payment, IOC.resolve(paymentCalculationStrategyKey, invoices));
            numberF.out(payment, sequenceHolder.getNewId());
            message.setPayment(payment);

        } catch (ReadValueException | ChangeValueException | InvalidArgumentException | ResolutionException e) {
            throw new CreatePaymentException("Error occurred when make payment!", e);
        }
    }

    private String generateNumericGuid() {
        return randomNumber(32).concat(String.valueOf(Instant.now().getEpochSecond()));
    }

    private String randomNumber(int digits) {
        Random r = new Random();
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < digits; i++) {
            result.append(r.ints(0, 10).findFirst().getAsInt());
        }
        return result.toString();
    }
}
