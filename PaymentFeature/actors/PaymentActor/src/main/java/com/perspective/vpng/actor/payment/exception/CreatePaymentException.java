package com.perspective.vpng.actor.payment.exception;

import com.perspective.vpng.actor.payment.PaymentActor;

/**
 * Exception for error in {@link PaymentActor}
 */
public class CreatePaymentException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public CreatePaymentException(String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public CreatePaymentException(String message, Throwable cause) {
        super(message, cause);
    }
}
