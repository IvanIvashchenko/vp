package com.perspective.vpng.actor.payment.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for params for {@link com.perspective.vpng.actor.payment.PaymentActor} actor.
 */
public interface IPaymentActorParam {

    /**
     * Gets a calculate payment amount rule name for resolution IOC.
     *
     * @return a calculate payment amount rule key.
     * @throws ReadValueException when couldn't read value of <code>calculatePaymentStrategy</code> field from message.
     */
    String getCalculatePaymentAmountStrategy() throws ReadValueException;

    /**
     *
     * @return
     * @throws ReadValueException
     */
    String getPaymentKindDeterminationStrategy() throws ReadValueException;

    /**
     * @return payment collection name
     * @throws ReadValueException
     */
    String getCollectionName() throws ReadValueException;

    /**
     * @return initial sequence value for number of payment field
     * @throws ReadValueException
     */
    Integer getInitialSequenceValue() throws ReadValueException;

}
