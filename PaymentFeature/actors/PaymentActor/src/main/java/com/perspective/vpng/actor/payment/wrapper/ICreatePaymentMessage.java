package com.perspective.vpng.actor.payment.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for make payment message.
 */
public interface ICreatePaymentMessage {

    /**
     * Gets invoices for calculates payment amount and determine payment kind.
     *
     * @return invoices.
     *
     * @throws ReadValueException when couldn't read <code>invoices</code> from message.
     */
    List<IObject> getInvoices() throws ReadValueException;

    /**
     * Gets user guid to pass them to payment gateway.
     *
     * @return user guid.
     *
     * @throws ReadValueException when couldn't read <code>userGuid</code> from message.
     */
    String getUserGuid() throws ReadValueException;

    /**
     * Gets description of payment to pass them to payment gateway.
     *
     * @return description of payment.
     *
     * @throws ReadValueException when couldn't read <code>userGuid</code> from message.
     */
    String getDescription() throws ReadValueException;

    /**
     * Gets payment method for determine gateway.
     *
     * @return payment method.
     *
     * @throws ReadValueException when couldn't read <code>paymentMethod</code> from message.
     */
    String getPaymentMethod() throws ReadValueException;

    /**
     * Changes <code>payment</code> field.
     *
     * @param payment - payment with common properties.
     *
     * @throws ChangeValueException when couldn't changed <code>payment</code> into invoice object.
     */
    void setPayment(IObject payment) throws ChangeValueException;
}
