package com.perspective.vpng.actor.payment_request;

import com.perspective.vpng.actor.payment_request.wrapper.ClientBackUrlConfig;
import com.perspective.vpng.actor.payment_request.wrapper.ICreateRequestParamsMessage;
import com.perspective.vpng.actor.payment_request.wrapper.IGatewayProperty;
import com.perspective.vpng.actor.payment_request.wrapper.IPaymentRequestActorParam;
import com.perspective.vpng.actor.payment_request.wrapper.IPrepareMessage;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ IOC.class, Keys.class })
public class PaymentRequestActorTest {

    private PaymentRequestActor createPaymentRequestActor;
    private ICreateRequestParamsMessage createRequestParamsMessageMock;
    private IPrepareMessage prepareMessageMock;
    private IGatewayProperty gatewayPropertyMock;
    private IObject paymentMock;
    private IKey requestBuildStrategyKey;

    private IField paymentKindF;
    private IField paymentMethodF;

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);
        createRequestParamsMessageMock = mock(ICreateRequestParamsMessage.class);
        prepareMessageMock = mock(IPrepareMessage.class);
        gatewayPropertyMock = mock(IGatewayProperty.class);
        paymentMock = mock(IObject.class);

        paymentKindF = mock(IField.class);
        final String paymentKindFN = "paymentKind";
        Mockito.when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentKindFN))
                .thenReturn(paymentKindF);

        paymentMethodF = mock(IField.class);
        final String paymentMethodFN = "paymentMethod";
        Mockito.when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), paymentMethodFN))
                .thenReturn(paymentMethodF);

        final String requestBuildStrategyName = "testStrategy";
        IPaymentRequestActorParam createPaymentRequestActorParamMock = mock(IPaymentRequestActorParam.class);
        when(createPaymentRequestActorParamMock.getGwRequestBuildStrategyName()).thenReturn(requestBuildStrategyName);
        requestBuildStrategyKey = mock(IKey.class);
        when(Keys.getOrAdd(createPaymentRequestActorParamMock
                .getGwRequestBuildStrategyName()))
                .thenReturn(requestBuildStrategyKey);

        createPaymentRequestActor = new PaymentRequestActor(createPaymentRequestActorParamMock);
    }

    @Test
    public void shouldMakeSearchCriteriaObject_When_PaymentMethodAndPaymentKindAreGiven() throws Exception {
        when(prepareMessageMock.getPaymentMethod()).thenReturn("testPaymentMethod");
        when(prepareMessageMock.getActualPaymentKind()).thenReturn("testPaymentKind");
        IObject searchCriteriaObject = mock(IObject.class);

        IKey searchCriteriaObjectKey = mock(IKey.class);
        when(Keys.getOrAdd(IObject.class.getCanonicalName()))
                .thenReturn(searchCriteriaObjectKey);
        when(IOC.resolve(eq(searchCriteriaObjectKey))).thenReturn(searchCriteriaObject);

        createPaymentRequestActor.prepare(prepareMessageMock);
        verify(prepareMessageMock).getActualPaymentKind();
        verify(prepareMessageMock).getPaymentMethod();
        verify(prepareMessageMock).setSearchActiveGwParams(searchCriteriaObject);

        verify(paymentKindF).out(searchCriteriaObject, "testPaymentKind");
        verify(paymentMethodF).out(searchCriteriaObject, "testPaymentMethod");
        verify(prepareMessageMock).setSearchActiveGwParams(searchCriteriaObject);
    }

    @Test
    public void shouldBuildPaymentRequesrParameters() throws Exception {
        when(createRequestParamsMessageMock.getActiveGateway()).thenReturn(gatewayPropertyMock);
        when(gatewayPropertyMock.getGwType()).thenReturn("standard");
        when(createRequestParamsMessageMock.getPayment()).thenReturn(paymentMock);
        IObject strategyResolveResult = mock(IObject.class);

        IGatewayProperty gatewayProperty = createRequestParamsMessageMock.getActiveGateway();
        String gwType = gatewayProperty.getGwType();
        IObject payment = createRequestParamsMessageMock.getPayment();
        when(IOC.resolve(eq(requestBuildStrategyKey), eq(gwType), eq(payment), any(ClientBackUrlConfig.class)))
                .thenReturn(strategyResolveResult);

        IField paymentRequestParamF = mock(IField.class);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentRequestParam")).thenReturn(paymentRequestParamF);

        String serializedParams = mock(String.class);
        IObject newResult = mock(IObject.class);
        IObject detailsObj = mock(IObject.class);
        IField detailsF = mock(IField.class);
        when(strategyResolveResult.serialize()).thenReturn(serializedParams);
        when(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), serializedParams)).thenReturn(newResult);
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "detail")).thenReturn(detailsF);
        when(detailsF.in(newResult)).thenReturn(detailsObj);
        createPaymentRequestActor.createRequestParameters(createRequestParamsMessageMock);
        verify(createRequestParamsMessageMock).setPaymentRequestParam(newResult);
    }
}