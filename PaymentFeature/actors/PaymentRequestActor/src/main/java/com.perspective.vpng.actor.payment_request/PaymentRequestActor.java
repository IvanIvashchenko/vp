package com.perspective.vpng.actor.payment_request;

import com.perspective.vpng.actor.payment_request.exception.PaymentRequestException;
import com.perspective.vpng.actor.payment_request.wrapper.ClientBackUrlConfig;
import com.perspective.vpng.actor.payment_request.wrapper.ICreateRequestParamsMessage;
import com.perspective.vpng.actor.payment_request.wrapper.IPrepareMessage;
import com.perspective.vpng.actor.payment_request.wrapper.IPaymentRequestActorParam;
import com.perspective.vpng.actor.payment_request.wrapper.IGatewayProperty;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.ifield_name.IFieldName;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.DeleteValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.SerializeException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Actor for create parameters for gateway request
 *
 * @see PaymentRequestActor#prepare(IPrepareMessage)
 * @see PaymentRequestActor#createRequestParameters(ICreateRequestParamsMessage)
 */
public class PaymentRequestActor {

    private final String gwRequestBuildStrategyName;
    private final ClientBackUrlConfig clientBackUrlParams;

    public PaymentRequestActor(final IPaymentRequestActorParam params) {
        try {
            this.gwRequestBuildStrategyName = params.getGwRequestBuildStrategyName();
            this.clientBackUrlParams = params.getClientBackUrlParams();
        } catch (ReadValueException e) {
            throw new RuntimeException();
        }
    }

    /**
     * Make search-criteria-object for determine active gateway.
     *
     * @param message - must contains payment kind and payment method.
     *
     * @throws PaymentRequestException when message hasn't contains a payment kind or payment method
     *                  or error handling given message.
     */
    public void prepare(final IPrepareMessage message)
            throws PaymentRequestException {

        try {
            String paymentKind = message.getActualPaymentKind();
            String paymentMethod = message.getPaymentMethod();
            IObject payRequestParams = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IField paymentKindF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentKind");
            paymentKindF.out(payRequestParams, paymentKind);
            IField paymentMethodF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentMethod");
            paymentMethodF.out(payRequestParams, paymentMethod);

            message.setSearchActiveGwParams(payRequestParams);

        } catch ( ChangeValueException | InvalidArgumentException e) {
            throw new PaymentRequestException("Can't write fields to SearchActiveGwParams object! " + e.getMessage(), e);
        } catch ( ResolutionException e) {
            throw new PaymentRequestException("Can't resolve objects from IOC! " + e.getMessage(), e);
        } catch ( ReadValueException e) {
            throw new PaymentRequestException("Invalid given message: can't read value! " + e.getMessage(), e);
        }
    }

    /**
     * Prepare gateway request parameters and set them to message as IObject.
     *
     *
     * @param message must contain gateway property. @see {@link IGatewayProperty}
     * @throws PaymentRequestException
     */
    public void createRequestParameters(final ICreateRequestParamsMessage message) throws PaymentRequestException {
        try {
            IGatewayProperty IGatewayProperty = message.getActiveGateway();
            String gwType = IGatewayProperty.getGwType();
            String terminalType = IGatewayProperty.getTerminalType();
            message.setTerminalType(terminalType);
            message.setGwType(gwType);

            IObject preparedRequestParam = IOC.resolve(
                    Keys.getOrAdd(gwRequestBuildStrategyName),
                    gwType,
                    message.getPayment(),
                    clientBackUrlParams
            );
            IObject payment = message.getPayment();

            IField paymentRequestParamF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentRequestParam");
            paymentRequestParamF.out(payment, preparedRequestParam);

            message.setPayment(payment);

            String serializedRequestParam = preparedRequestParam.serialize();
            IObject responseRequestParam = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), serializedRequestParam );
            IField detailsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "detail");
            IObject detailsResponseRequestParam = detailsF.in(responseRequestParam);
            detailsResponseRequestParam.deleteField(IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "getawaySign"));

            message.setPaymentRequestParam(responseRequestParam);
        } catch (ReadValueException | ResolutionException | DeleteValueException | SerializeException e) {
            throw new PaymentRequestException("Invalid given message: can't retrieve value! " + e.getMessage(), e);
        } catch (ChangeValueException e) {
            throw new PaymentRequestException("Can't write prepared request params to message! " + e.getMessage(), e);
        } catch (InvalidArgumentException e) {
            throw new PaymentRequestException("Can't write prepared request params to payment! " + e.getMessage(), e);
        }
    }
}
