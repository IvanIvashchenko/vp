package com.perspective.vpng.actor.payment_request.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper of message for handler of create request parameters.
 */
public interface ICreateRequestParamsMessage {
    /**
     * Gets parameters of active payment gateway from payment method matrix.
     *
     * @see IGatewayProperty
     *
     * @return a gateway property.
     *
     * @throws ReadValueException when couldn't read <code>activeGateway</code> from message.
     */
    IGatewayProperty getActiveGateway() throws ReadValueException;

    /**
     * Gets a payment as IObject
     *
     * @return a payment.
     *
     * @throws ReadValueException when couldn't read <code>payment</code> from message.
     */
    IObject getPayment() throws ReadValueException;

    /**
     * Changes valued of paymentRequestParams object into message.
     *
     * @param paymentRequestParam - prepared gateway request params iobject.
     *
     * @throws ChangeValueException when couldn't change <code>paymentRequestParam</code> object into message.
     */
    void setPaymentRequestParam(IObject paymentRequestParam) throws ChangeValueException;

    /**
     * Changes valued of terminalType field into payment in message.
     *
     * @param terminalType - terminal type, retrieve from payment method matrix from active gateway section
     *                     and set into payment.
     *
     * @throws ChangeValueException when couldn't change <code>terminalType</code> object into message.
     */
    void setTerminalType(String terminalType) throws ChangeValueException;

    /**
     * Changes valued of swType field into payment in message.
     *
     * @param gwType - gateway type, retrieve from payment method matrix from active gateway section
     *                     and set into payment.
     *
     * @throws ChangeValueException when couldn't change <code>gwType</code> object into message.
     */
    void setGwType(String gwType) throws ChangeValueException;

    /**
     * Changes valued of payment object into message.
     *
     * @param payment - object of payment.
     *
     * @throws ChangeValueException when couldn't change <code>payment</code> object into message.
     */
    void setPayment(IObject payment) throws ChangeValueException;

}
