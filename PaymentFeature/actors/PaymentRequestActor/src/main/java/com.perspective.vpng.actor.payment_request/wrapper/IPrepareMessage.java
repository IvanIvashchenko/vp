package com.perspective.vpng.actor.payment_request.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper of message for handler of prepare search-criteria-object for determine active gateway.
 * @see com.perspective.vpng.actor.payment_request.PaymentRequestActor#prepare(IPrepareMessage)
 */
public interface IPrepareMessage {
    /**
     * Gets a payment method, value may be creditCard, bonus ... etc.
     *
     * @return a payment method.
     *
     * @throws ReadValueException when couldn't read <code>paymentMethod</code> from message.
     */
    String getPaymentMethod() throws ReadValueException;

    /**
     * Gets a payment kind, value may be standard, communal or mixed.
     *
     * @return a payment kind.
     *
     * @throws ReadValueException when couldn't read <code>actualPaymentKind</code> from message.
     */
    String getActualPaymentKind() throws ReadValueException;

    /**
     * Changes valued of paymentRequestParams object into message.
     *
     * @param paymentRequestParams - search-criteria-iobject.
     *
     * @throws ChangeValueException when couldn't change <code>paymentRequestParams</code> object into message.
     */
    void setSearchActiveGwParams(IObject paymentRequestParams) throws ChangeValueException;

}
