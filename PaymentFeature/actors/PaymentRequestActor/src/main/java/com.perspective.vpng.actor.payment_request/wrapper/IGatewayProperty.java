package com.perspective.vpng.actor.payment_request.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for found active gateway parameters
 */
public interface IGatewayProperty {

    /**
     * Gets a type of active gateway (creditCard, bonus etc.).
     *
     * @return a type of gateway.
     *
     * @throws ReadValueException when couldn't read <code>gwType</code> field from gateway property object.
     */
    String getGwType() throws ReadValueException;

    /**
     * Gets a terminal type of active gateway.
     *
     * @return a terminal type of active gateway.
     *
     * @throws ReadValueException when couldn't read <code>terminalType</code> field from gateway property object.
     */
    String getTerminalType() throws ReadValueException;

    /**
     * Gets a activity of active gateway.
     *
     * @return a activity of active gateway.
     *
     * @throws ReadValueException when couldn't read <code>activity</code> field from gateway property object.
     */
    Boolean getActivity() throws ReadValueException;

}
