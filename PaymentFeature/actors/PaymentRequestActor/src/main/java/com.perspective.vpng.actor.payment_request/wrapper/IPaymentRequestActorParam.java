package com.perspective.vpng.actor.payment_request.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for params for {@link com.perspective.vpng.actor.payment_request.PaymentRequestActor} actor.
 */
public interface IPaymentRequestActorParam {

    /**
     * Gets a gateway request params builder rule name for resolution IOC.
     *
     * @return a gateway request params builder rule key.
     * @throws ReadValueException when couldn't read value of <code>gwRequestBuilderStrategyName</code> field from message.
     */
    String getGwRequestBuildStrategyName() throws ReadValueException;

    /**
     * @return wrapper for object with parameters for client back url
     * @throws ReadValueException if any error is occurred
     */
    ClientBackUrlConfig getClientBackUrlParams() throws ReadValueException;

}
