package com.perspective.vpng.actor.payment_request.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for object with parameters of client back url from payment request
 */
public interface ClientBackUrlConfig {

    /**
     * @return application url
     * @throws ReadValueException if any error is occurred
     */
    String getAppUrl() throws ReadValueException;

    /**
     * @return prefix for path
     * @throws ReadValueException if any error is occurred
     */
    String getPrefix() throws ReadValueException;

    /**
     * @return action parameter name
     * @throws ReadValueException if any error is occurred
     */
    String getActionParamName() throws ReadValueException;

    /**
     * @return success result value
     * @throws ReadValueException if any error is occurred
     */
    String getGwSuccess() throws ReadValueException;

    /**
     * @return request number parameter name
     * @throws ReadValueException if any error is occurred
     */
    String getRequestNumberParamName() throws ReadValueException;
}
