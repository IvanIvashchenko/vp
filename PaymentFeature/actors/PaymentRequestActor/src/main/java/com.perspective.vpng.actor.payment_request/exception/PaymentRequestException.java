package com.perspective.vpng.actor.payment_request.exception;

/**
 * Exception for error in {@link com.perspective.vpng.actor.payment_request.PaymentRequestActor} when
 * message in invalid state.
 */
public class PaymentRequestException extends Exception {
    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public PaymentRequestException(String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public PaymentRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
