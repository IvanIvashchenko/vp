DROP TABLE IF EXISTS user_history;

CREATE TABLE user_history(
id SERIAL,
document JSONB
);

DROP TABLE IF EXISTS group_name_property;

CREATE TABLE group_name_property(
id BIGINT NOT NULL,
document JSONB NOT NULL
);

INSERT INTO group_name_property (id, document) VALUES
(1, '{
"type": "PAYMENT",
"status": "PAID",
"groupName": "Оплата прошла"
}'),
(2, '{
"type": "PAYMENT",
"status": "PREPROCESSED",
"groupName": "Оплата прошла"
}'),
(3, '{
"type": "PAYMENT",
"status": "PROCESSED",
"groupName": "Оплата прошла"
}'),
(4, '{
"type": "PAYMENT",
"status": "REFUND",
"groupName": "Возврат"
}'),
(5, '{
"type": "PAYMENT",
"status": "REVERSED",
"groupName": "Возврат"
}'),
(6, '{
"type": "READING",
"status": "PROCESSED",
"groupName": "Переданы"
}'),
(7, '{
"type": "READING",
"status": "ERROR",
"groupName": "Ошибка"
}')
;

