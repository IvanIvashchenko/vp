--
-- Table structure for table "receive_rule"
--
DROP TABLE IF EXISTS "requisites";
CREATE TABLE IF NOT EXISTS "requisites" (
"document" jsonb DEFAULT NULL
) WITH (
  OIDS=FALSE
);

--
--Dumping data for table "requisites"
--
INSERT INTO "requisites" ("document") VALUES
('{
      "бик":"045279828",
      "инн":"5406323202",
      "isActive":true,
      "адрес":"в филиале ГПБ в г. Омске",
      "requisitesKey":"default",
      "startDateTime":"2016-01-20 00:00:00",
      "расчетный-счет":"40702810000311000342"
}');
