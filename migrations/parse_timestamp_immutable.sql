--FUNCTION FOR DB, USE THIS IF HAVE ERROR "NOT FOUND parse_timestamp_immutable(jsonb) FUNCTION"
CREATE OR REPLACE FUNCTION parse_timestamp_immutable(source jsonb) RETURNS timestamptz AS
$$BEGIN RETURN source::text::timestamptz; END;$$
LANGUAGE 'plpgsql' IMMUTABLE;