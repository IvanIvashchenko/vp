-- --------------------------------------------------------


--
-- Table structure for table "form"
-- command for windows: psql -d YOUR_DATABASE_NAME -U YOUR_ROLE -f YOUR_PATH/.../forms_dump.sql

DROP TABLE IF EXISTS "form";
CREATE TABLE IF NOT EXISTS "form" (
"document" jsonb DEFAULT NULL
) WITH (
  OIDS=FALSE
);
--  ALTER TABLE public.form
--  OWNER TO test_user;

--
--Dumping data for table "form"
--

INSERT INTO "form" ("document") VALUES
('{
  "formKey": "alfama",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «АЛФАМА» (интернет-магазин INTERSOUND), ИНН 7017329188",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/alfama.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {},
    "schema": [
      "Номер договора или заказа",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "argus",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ЧОП «Аргус-Томск», ИНН 7017109601",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/argus.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {},
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "argus_control",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Аргус-Контроль», ИНН 7017116430",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/argus.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {},
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "argus_sever",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ЧОП «Аргус-Север», ИНН 7017055963",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/argus.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {},
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
    "formKey": "beeline",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00",
    "block": "modal",
    "title": "Билайн, сотовая связь",
    "subtitle": "Билайн, сотовая связь",
    "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/beeline.jpg",
    "iagree": {
      "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
      "checked": false
    },
    "submitButton": {
      "label": "Перейти к оплате"
    },
    "body": {
      "form": {
        "всего-к-оплате":{
            "block":"static",
            "summarize": [
                "услуги"
            ]
        },
        "услуги": {
          "block": "text",
          "placeholder": "0",
          "label": "Сумма",
          "labelWidth": 3,
          "inputWidth": "200px",
          "textAlign": "right",
          "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>"
        }
      },
      "schema": [
        "телефон",
        "услуги",
        "всего-к-оплате"
      ]
    }
  }
'),
('{
  "formKey": "departamentohrany",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО ЧОО «Департамент охраны»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/departamentohrany_new.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Назначение платежа": {
        "block": "input",
        "type": "text",
        "label": "Назначение платежа",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Неверно указано назначение платежа. Введите назначение платежа"
          }
        ],
        "labelWidth": 3
      }
    },
    "schema": [
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Назначение платежа",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "domservicetdsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Дом-Сервис ТДСК»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/domservicetdsk.png",
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_domservicetdsk.png"
  },
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Содержание ОДИ метка": {
        "block": "label",
        "labelText": "Содержание общедомового имущества, ?"
      },
      "Содержание ОДИ": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      },
      "Текущий ремонт метка": {
        "block": "label",
        "labelText": "Текущий ремонт, ?"
      },
      "Текущий ремонт": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      },
      "Обслуживание ПУ метка": {
        "block": "label",
        "labelText": "Обслуживание приборов учета, ?"
      },
      "Обслуживание ПУ": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      },
      "Вывоз мусора метка": {
        "block": "label",
        "labelText": "Вывоз мусора, ?"
      },
      "Вывоз мусора": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      },
      "Лифт метка": {
        "block": "label",
        "labelText": "Лифт, ?"
      },
      "Лифт": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      },
      "Домофон метка": {
        "block": "label",
        "labelText": "Домофон, ?"
      },
      "Домофон": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      },
      "Пожарная сигнализация метка": {
        "block": "label",
        "labelText": "Пожарная сигнализация, ?"
      },
      "Пожарная сигнализация": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      },
      "Видеонаблюдение метка": {
        "block": "label",
        "labelText": "Видеонаблюдение, ?"
      },
      "Видеонаблюдение": {
        "block": "input",
        "placeholder": 0,
        "cls": "text-right",
        "isSummable": true
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      {
        "block": "table",
        "label": "Вид платежа и сумма",
        "alias": "Вид платежа и сумма",
        "layout": {
          "columns": 2,
          "width": [
            9,
            3
          ]
        },
        "items": [
          "Содержание ОДИ метка",
          "Содержание ОДИ",
          "Текущий ремонт метка",
          "Текущий ремонт",
          "Вывоз мусора метка",
          "Вывоз мусора",
          "Лифт метка",
          "Лифт",
          "Домофон метка",
          "Домофон",
          "Пожарная сигнализация метка",
          "Пожарная сигнализация",
          "Видеонаблюдение метка",
          "Видеонаблюдение"
        ],
        "labelWidth": 3
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "erdivantvtomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ДОМ.RU, Кабельное ТВ, Томская обл.",
  "subtitle": "ДОМ.RU, Кабельное ТВ, Томская обл.",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/dom_ru.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {},
    "schema": [
      "Номер договора",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "erdomrutomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ДОМ.RU, Интернет, Томская обл.»",
  "subtitle": "ДОМ.RU, Интернет, Томская обл.",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/dom_ru.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {},
    "schema": [
      "Номер договора",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ergorsvyaztomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ДОМ.RU, Телефония, Томская обл.",
  "subtitle": "ДОМ.RU, Телефония, Томская обл.",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/dom_ru.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {},
    "schema": [
      "Номер телефона",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "error_payment",
  "size": "sm",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "formHeader": "Платеж не прошел",
  "body": {
    "form": {
      "errorComment": {
        "block": "comment_with_html",
        "text": "Уважаемый пользователь! Попытка оплаты завершена не успешно. Рекомендуем Вам [повторить платеж](javascript:void(0);).\n\rЕсли Вам нужна помощь при оплате, позвоните по телефону круглосуточной технической поддержки **8 800 700-08-38**, либо отправьте заявку на наш электронный адрес [abon@vp.ru](mailto:abon@vp.ru), и наши специалисты свяжутся с Вами\n\r",
        "fontColor": "#96a7b7",
        "textAlign": "center",
        "cls": "modal-inform-body",
        "fontSize": "15px"
      }
    },
    "schema": [
      "errorComment"
    ]
  }
}
'),
('{
  "formKey": "forgot_password",
  "size": "sm",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "formHeader": "Напомнить пароль",
  "submitButton": {
    "label": "Отправить",
    "labelWidth": 8,
    "cls": "btn-default",
    "container": "body"
  },
  "body": {
    "form": {
      "forgotPasswordComment": {
        "block": "comment",
        "text": "Укажите свой e-mail, и мы отправим вам письмо со ссылкой для восстановления пароля:",
        "fontColor": "#252525",
        "fontSize": "13px"
      }
    },
    "schema": [
      "forgotPasswordComment",
      "email"
    ]
  }
}
'),
('{
  "formKey": "gazpromtomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ОАО «Газпром межрегионгаз Новосибирск» Филиал в Томской области",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/gazpromtomsk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Сумма редактируемая": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "value": 0,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>"
      },
      "div": {
        "block": "divider"
      },
      "Газ метка": {
        "block": "label",
        "labelText": "Газ"
      },
      "Газ": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "center"
      },
      "Предыдущие показания": {
        "block": "comment",
        "text": "Предыдущие показания прибора учета: 2708",
        "fontColor": "#252525",
        "fontSize": "13px"
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма редактируемая",
      "div",
      {
        "block": "table",
        "label": "Введите показания приборов учёта",
        "columnHeaders": [
          "Тип ПУ",
          "Текущие показания"
        ],
        "layout": {
          "columns": 2,
          "width": [
            7,
            2
          ]
        },
        "items": [
          "Газ метка",
          "Газ"
        ]
      },
      "Предыдущие показания",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "govorova",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «Говорова 26, 28», ИНН 7017125850",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_govorova.png"
  },
  "body": {
    "form": {
      "Ссылка": {
        "block": "link",
        "labelWidth": 3,
        "href": "https://vp.ru/providers/govorovakaprem",
        "text": "Оплатить капитальный ремонт"
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Ссылка",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "govorovakaprem",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «Говорова 26, 28» Капремонт, ИНН 7017125850",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_govorova.png"
  },
  "body": {
    "form": {},
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "himik",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «Химик», ИНН 7017221360",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukmoydomtomsk.png"
  },
  "body": {
    "form": {
      "Ссылка": {
        "block": "link",
        "labelWidth": 3,
        "href": "https://vp.ru/providers/himikkaprem",
        "text": "Оплатить капитальный ремонт"
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Ссылка",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "himikkaprem",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «Химик» Капремонт, ИНН 7017221360",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukmoydomtomsk.png"
  },
  "body": {
    "form": {},
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "iceberg",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО ЧОП «Айсберг», ИНН 7017049705",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/iceberg_new.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_iceberg.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "длина == 6 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 6 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "iceberg_servis",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО ЧОП «АЙСБЕРГ-СЕРВИС», ИНН 7017189660",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/iceberg_new.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_iceberg.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "длина == 6 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 6 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "05",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jek30",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ЖЭК №30», ИНН 7017113206",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_jek30.png"
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "9999999999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "значение && длина == 13",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Содержание жилья метка": {
        "block": "label",
        "labelText": "Содержание жилья, ?"
      },
      "Содержание жилья": {
        "block": "text",
        "placeholder": 0,
        "textAlign": "right",
        "isSummable": true
      },
      "Сумма": {
        "block": "static",
        "label": "Сумма",
        "labelWidth": 3,
        "value": 0,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "summarize": [
          "Вид платежа и сумма"
        ],
        "validationRules": [
          {
            "isGlobalError": true,
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      {
        "alias": "Вид платежа и сумма",
        "block": "table",
        "label": "Вид платежа и сумма",
        "labelWidth": 3,
        "layout": {
          "columns": 2,
          "width": [
            9,
            3
          ]
        },
        "items": [
          "Содержание жилья метка",
          "Содержание жилья"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jekzhilischnik",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ЖЭК-Жилищник», ИНН 7017363710",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jekzhilischnik.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "mask": "99999",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "name": "accountId"
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jekzhilischnik710",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ЖЭК-Жилищник», ИНН 7017363710",
  "subtitle": "ООО «ЖЭК-Жилищник», ИНН 7017363710",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jekzhilischnik710.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "input",
        "type": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "name": "accountId",
        "disabled": true,
        "value": "90019"
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "input",
        "type": "text",
        "label": "Адрес",
        "labelWidth": 3,
        "name": "Адрес",
        "disabled": true,
        "value": "Томск,Говорова,50,18"
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "value": "825.54",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jilservice047",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Жилсервис «Ленинский», ИНН 7017148047",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jilservice.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_jilservice.jpg"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "value": "352817 11111-9",
        "disabled": true,
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jilservice224",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Жилсервис «Кировский», ИНН 7017161224",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jilservice.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_jilservice.jpg"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jilservice472",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Жилсервис «Черемошники», ИНН 7017208472",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jilservice.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_jilservice.jpg"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jilservice497",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Компания Жилсервис», ИНН7017208497",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jilservice.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_jilservice.jpg"
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "значение",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jilservice520",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Жилсервис», ИНН 7017070520",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jilservice.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_jilservice.jpg"
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "значение",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "jilservice690",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Компания Жилсервис «Каштак», ИНН7017157690",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/jilservice.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_jilservice.jpg"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "value": "000077 12354-6",
        "disabled": true,
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "kapremtomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "«Региональный фонд капитального ремонта многоквартирных домов Томской области» (РФКР МКД ТО)",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/kapremtomsk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_kapremtomsk.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "99999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина == 8 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 8 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Содержание жилья метка": {
        "block": "label",
        "labelText": "Взнос на капитальный ремонт,",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Содержание жилья": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Текущий ремонт метка": {
        "block": "label",
        "labelText": "Проценты в связи с ненадлежащим исполнением обязанности по уплате взносов,",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Текущий ремонт": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Сумма": {
        "block": "static",
        "label": "Сумма",
        "labelWidth": 3,
        "value": 0,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "summarize": [
          "Вид платежа и сумма"
        ],
        "validationRules": [
          {
            "rule": "1 < Сумма && Сумма < 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб.",
            "isGlobalError": true
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      {
        "block": "fieldset",
        "alias": "Вид платежа и сумма",
        "label": "Вид платежа и сумма",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          },
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          }
        ],
        "items": [
          "Содержание жилья метка",
          "Содержание жилья",
          "Текущий ремонт метка",
          "Текущий ремонт"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "krepost",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "Сервисный центр «Крепость»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/krepost.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "999?9",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "3 <= длина && длина <= 4 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 3 до 4 символов."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "login",
  "size": "sm",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "formHeader": "Вход",
  "formHeaderNewFormLink": {
    "formUrl": "/api/configs/vp/register.json",
    "text": "Регистрация",
    "href": "#"
  },
  "submitButton": {
    "label": "Войти",
    "labelWidth": 8,
    "cls": "btn-default",
    "container": "body"
  },
  "body": {
    "form": {
      "email": {
        "block": "input",
        "placeholder": "электронная почта",
        "validationRules": [
          {
            "rule": "email",
            "errorMessage": "Такой почтовый адрес не зарегистрирован, либо пароль неверный. Если Вы уверены, что регистрировались на нашем сайте, но не помните пароль, то попробуйте его восстановить ниже по форме. Просто введите свой электронный адрес, и Вам на электронную почту придет ссылка. Пройдя по ссылке, Вы сможете ввести новый удобный для Вас пароль.",
            "isGlobalError": true
          }
        ]
      },
      "пароль": {
        "block": "input",
        "type": "password",
        "placeholder": "пароль",
        "newFormLink": {
          "formUrl": "/api/configs/vp/forgot_password.json",
          "text": "Напомнить",
          "href": "#"
        },
        "validationRules": [
          {
            "rule": "обязательное && password",
            "errorMessage": "Такой почтовый адрес не зарегистрирован, либо пароль неверный. Если Вы уверены, что регистрировались на нашем сайте, но не помните пароль, то попробуйте его восстановить ниже по форме. Просто введите свой электронный адрес, и Вам на электронную почту придет ссылка. Пройдя по ссылке, Вы сможете ввести новый удобный для Вас пароль.",
            "isGlobalError": true
          }
        ]
      }
    },
    "schema": [
      {
        "block": "fieldset",
        "validateOnlyFirstField": true,
        "items": [
          "email",
          "пароль"
        ],
        "layout": [
          1,
          1
        ]
      }
    ]
  }
}
'),
('{
  "formKey": "mkstomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ЗАО «МКС-Томск», ИНН 7017276320»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/mkstomsk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_mkstomsk.png"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "input",
        "type": "text",
        "label": "Штрихкод",
        "disabled": true,
        "labelWidth": 3,
        "value": "3526512216649",
        "validationRules": [
          {
            "rule": "длина == 13 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "input",
        "type": "text",
        "label": "Адрес",
        "labelWidth": 3,
        "name": "Адрес",
        "disabled": true,
        "value": "Томск г, Никитина ул, д. 56, кв. 166"
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "value": "825.54",
        "label": "Сумма",
        "labelWidth": 3,
        "textAlign": "right",
        "inputWidth": "200px",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "1 <= Сумма && Сумма <= 15000",
            "errorMessage": "Сумма платежа не может быть меньше 1 руб. и больше 15 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "modelnet",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "Model.Net (ООО «Модэл»)",
  "subtitle": "Model.Net (ООО «Модэл»)",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/modelnet.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?99999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина >= 1 && длина <= 6 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 6 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "1 <= Сумма && Сумма <= 15000",
            "errorMessage": "Сумма платежа не может быть меньше 1 руб. и больше 15 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "mvdtomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "«ОВО по г. Томску – филиал ФГКУ УВО УМВД России по Томской области»",
  "subtitle": "«ОВО по г. Томску – филиал ФГКУ УВО УМВД России по Томской области»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/mvdtomsk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "accountId": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "name": "accountId",
        "mask": "9?999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина > 0 && длина < 8 && изЦифр",
            "errorMessage": "Проверьте правильность введеного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 7 символов."
          }
        ]
      },
      "address": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "lastname": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "name": "lastname",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "firstname": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "patronym": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "textAlign": "right",
        "placeholder": "0",
        "type": "text",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "inputWidth": "200px",
        "labelWidth": 3,
        "label": "Сумма",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "accountId",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "labelWidth": 3,
        "items": [
          "lastname",
          "firstname",
          "patronym"
        ]
      },
      "address",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "newtelesystem",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Новые Телесистемы-ТВ»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/newtelesystem.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Номер договора": {
        "block": "text",
        "mask": "9999999",
        "maskPlaceholder": "_",
        "label": "Номер договора",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "значение && изЦифр",
            "errorMessage": "Проверьте правильность введеного номера договора. Номер договора состоит из цифр и содержит 7 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Номер договора",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "oootomica",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Томика»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/oootomica.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "id": {
        "block": "text",
        "label": "Идентификационный номер (ID)",
        "labelWidth": 3,
        "name": "accountId",
        "mask": "9?9999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина > 0 && длина < 9 && значение && изЦифр",
            "errorMessage": "Проверьте правильность введенного идентификационного номера (ID). Идентификационный номер (ID) состоит из цифр и содержит от 1 до 8 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "id",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "patriott",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО ЧОП «Патриот-Т»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/patriott.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "block": "text",
        "mask": "gg9999999",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Неверно введен лицевой счёт. Введите лицевой счёт."
          }
        ]
      },
      "Адрес": {
        "label": "Адрес",
        "labelWidth": 3,
        "block": "input",
        "type": "text",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Неверно введен адрес. Введите адрес."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "pravoporjadok",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ЧОО «ПРАВОПОРЯДОК», ИНН 7017032557",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/pravoporjadok.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "mask": "9?999999999",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "1 <= длина && длина <= 10 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 10 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "pravoporjadokservice",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Правопорядок-сервис», ИНН 7017282564",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/pravoporjadok.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "mask": "9?999999999",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "1 <= длина && длина <= 10 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 10 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "pravoporjadokt",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ЧОО «ПРАВОПОРЯДОК-Т», ИНН 7017109721",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/pravoporjadok.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "mask": "9?999999999",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "1 <= длина && длина <= 10 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 10 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
    "formKey": "rctomsk",
    "block": "modal",
    "title": "ООО «Томский Расчетный Центр»",
    "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/rctomsk.png",
    "iagree": {
      "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
      "checked": true
    },
    "submitButton": {
      "label": "Перейти к оплате"
    },
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00",
    "body": {
      "form": {
        "лицевой-счет": {
          "block": "input",
          "type": "text",
          "disabled": true,
          "label": "Лицевой счёт",
          "labelWidth": 3,
          "name": "лицевой-счет"
        },
        "адрес": {
          "block": "static",
          "label": "Адрес",
          "value": "-",
          "labelWidth": 3
        },
        "наименование-услуги": {
          "block": "label",
          "labelText": "",
          "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
        },
        "к-оплате": {
          "block": "text",
          "placeholder": 0,
          "textAlign": "right",
          "isSummable": true
        },
        "тип-прибора-учета": {
          "block": "label",
          "labelText": "1"
        },
        "номер-прибора-учета": {
          "block": "label",
          "labelText": "1"
        },
        "предыдущие-показания": {
          "block": "text",
          "disabled": true,
          "textAlign": "center",
          "returnNumber": true
        },
        "текущие-показания": {
          "block": "text",
          "textAlign": "center",
          "returnNumber": true
        },
        "всего-к-оплате": {
          "block": "static",
          "label": "Сумма",
          "labelWidth": 3,
          "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
          "validationRules": [
          ],
          "summarize": [
            "платеж1"
          ]
        }
      },
      "schema": [
        "лицевой-счет",
        "плательщик",
        "адрес",
        "период-оплаты",
        {
          "name": "услуги",
          "block": "fieldset",
          "label": "Вид платежа и сумма",
          "alias": "платеж1",
          "labelWidth": 3,
          "layout": [
            {
              "count": 2,
              "width": [
                9,
                3
              ]
            }
          ],
          "items": [
          ]
        },
        {
          "block": "table",
          "label": "Введите показания приборов учёта",
          "name": "приборы-учета",
          "columnHeaders": [
          ],
          "layout": {
            "columns": 4,
            "width": [
              3,
              2,
              2,
              2
            ]
          },
          "items": [
          ]
        },
        "всего-к-оплате"
      ]
    }
  }
'),
('{
  "formKey": "rctomsk",
  "type": "meters",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Томский Расчетный Центр» передача показаний",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/rctomsk.png",
  "submitButton": {
    "label": "Передать показания"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Номер штрихкода указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_rctomsk.png"
  },
  "body": {
    "form": {
      "Штрихкод1": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "value": "200001225100",
        "disabled": true,
        "validationRules": [
          {
            "rule": "длина == 12 && изЦифр",
            "errorMessage": "Проверьте правильность введенного штрихкода. Штрихкод состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Штрихкод2": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "value": "201001225109",
        "disabled": true,
        "validationRules": [
          {
            "rule": "длина == 12 && изЦифр",
            "errorMessage": "Проверьте правильность введенного штрихкода. Штрихкод состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Лицевой счёт": {
        "block": "text",
        "mask": "99999999",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "maskPlaceholder": "",
        "value": "00122510",
        "disabled": true,
        "validationRules": [
          {
            "rule": "длина == 8 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 8 символов."
          }
        ]
      },
      "Водоснабжение и водоотведение метка": {
        "block": "label",
        "labelText": "Водоснабжение и водоотведение, ?"
      },
      "Водоснабжение и водоотведение": {
        "block": "text",
        "placeholder": 0,
        "textAlign": "right",
        "isSummable": true
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Штрихкод1",
      {
        "block": "fieldset",
        "label": "Вид платежа и сумма",
        "alias": "платеж1",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          }
        ],
        "items": [
          "Водоснабжение и водоотведение метка",
          "Водоснабжение и водоотведение"
        ]
      },
      "Штрихкод2",
      {
        "block": "fieldset",
        "label": "Вид платежа и сумма",
        "alias": "платеж2",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          }
        ],
        "items": [
          "Пени метка",
          "Пени"
        ]
      }
    ]
  }
}
'),
('{
    "size": "sm",
    "block": "modal",
    "formHeader": "Регистрация",
    "formKey":"register",
    "isActive":true,
    "startDateTime": "2016-01-20 00:00:00",
    "iagree": {
      "htmlLabel": "Согласен на обработку персональных данных",
      "checked": false,
      "labelWidth": 0,
      "container": "body"
    },
    "submitButton": {
      "label": "Зарегистрироваться",
      "labelWidth": 4,
      "cls": "btn-default"
    },
    "body": {
      "form": {
        "email": {
          "block": "input",
          "placeholder": "email*",
          "validationRules": []
        },
        "пароль": {
          "block": "input",
          "type": "password",
          "placeholder": "пароль*",
          "validationRules": []
        },
        "подтверждение-пароля": {
          "block": "input",
          "type": "password",
          "placeholder": "подтвердить пароль*",
          "validationRules": []
        },
        "фамилия": {
          "block": "input",
          "placeholder": "фамилия*",
          "validationRules": []
        },
        "имя": {
          "block": "input",
          "placeholder": "имя*",
          "validationRules": []
        },
        "отчество": {
          "block": "input",
          "placeholder": "отчество*",
          "validationRules": []
        },
        "дата-рождения": {
          "block": "date",
          "placeholder": "дата рождения*",
          "validationRules": []
        },
        "address": {
          "block": "address",
          "minLength": 3,
          "highlight": true,
          "layout": [
            1,
            2,
            1
          ],
          "url": "https://dadata.ru/api/v2/suggest/address/",
          "items": {
            "additional": {
              "block": "input"
            },
            "apartment": {
              "block": "input"
            }
          },
          "settings": {
            "type": "POST",
            "dataType": "json",
            "headers": {
              "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
              "Content-Type": "application/json"
            },
            "data": {
              "count": 20,
              "locations": [
                "Омская"
              ]
            }
          }
        },
        "телефон": {
          "block": "text",
          "mask": "+7 999 999-99-99",
          "maskPlaceholder": "_",
          "placeholder": "+7 ___ ___-__-__"
        },
        "phoneComment": {
          "block": "comment",
          "text": "Укажите свой номер мобильного телефона, и Вам будут доступны функции sms-напоминания. Мы гарантируем сохранность введенных данных.",
          "fontColor": "#96a7b7",
          "fontSize": "12px"
        },
        "область": {
          "block": "select",
          "values": [
            {
              "name": "Республика Адыгея",
              "value": "adyg_resp"
            },
            {
              "name": "Республика Алтай",
              "value": "altay_resp"
            },
            {
              "name": "Омская область",
              "value": "omsk_oblast",
              "selected": true
            }
          ]
        }
      },
      "schema": [
        "email",
        {
          "block": "fieldset",
          "items": [
            "пароль",
            "подтверждение-пароля"
          ],
          "layout": [
            1,
            1
          ]
        },
        {
          "block": "fieldset",
          "items": [
            "фамилия",
            "имя",
            "отчество",
            "дата-рождения"
          ],
          "layout": [
            1,
            1,
            1,
            1
          ]
        },
        "область",
        "address",
        "телефон",
        "phoneComment"
      ]
    }
  }
'),
('{
  "formKey": "rt_tomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ОАО «Ростелеком», г. Томск, Номер телефона",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/rt_tomsk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "titleDependencies": [
    {
      "title": "ОАО «Ростелеком», г. Томск, Номер телефона",
      "dependencies": "Тип == \"1\""
    },
    {
      "title": "ОАО «Ростелеком», г. Томск, Лицевой счет",
      "dependencies": "Тип == \"2\""
    }
  ],
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Тип метка": {
        "block": "label",
        "labelText": "Выберите вариант оплаты"
      },
      "Тип": {
        "block": "radiogroup",
        "labelWidth": 3,
        "options": [
          {
            "name": "По номеру телефона",
            "value": 1,
            "checked": true
          },
          {
            "name": "По номеру лицевого счёта",
            "value": 2
          }
        ]
      },
      "Лицевой счёт": {
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "block": "text",
        "mask": "9?9999999999999999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Неверно введен лицевой счёт. Введите лицевой счёт."
          },
          {
            "rule": "1 <= длина && длина <= 20 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 20 символов."
          }
        ],
        "dependencies": "Тип == \"2\""
      },
      "Номер телефона": {
        "label": "Номер телефона",
        "labelWidth": 3,
        "block": "text",
        "dependencies": "Тип == \"1\"",
        "mask": "+7 (3822) 999999",
        "placeholder": "+7 (3822) ______",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Неверно введен номер телефона. Введите номер телефона."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Тип метка",
      "Тип",
      "Лицевой счёт",
      "Номер телефона",
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "rt_tomsk_acc",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ОАО «Ростелеком», г. Томск, Номер телефона",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/rt_tomsk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "titleDependencies": [
    {
      "title": "ОАО «Ростелеком», г. Томск, Номер телефона",
      "dependencies": "Тип == \"1\""
    },
    {
      "title": "ОАО «Ростелеком», г. Томск, Лицевой счет",
      "dependencies": "Тип == \"2\""
    }
  ],
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Тип метка": {
        "block": "label",
        "labelText": "Выберите вариант оплаты"
      },
      "Тип": {
        "block": "radiogroup",
        "labelWidth": 3,
        "options": [
          {
            "name": "По номеру телефона",
            "value": 1
          },
          {
            "name": "По номеру лицевого счёта",
            "value": 2,
            "checked": true
          }
        ]
      },
      "Лицевой счёт": {
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "block": "text",
        "mask": "9?9999999999999999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Неверно введен лицевой счёт. Введите лицевой счёт."
          },
          {
            "rule": "1 <= длина && длина <= 20 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 20 символов."
          }
        ],
        "dependencies": "Тип == \"2\""
      },
      "Номер телефона": {
        "label": "Номер телефона",
        "labelWidth": 3,
        "block": "text",
        "dependencies": "Тип == \"1\"",
        "mask": "+7 (3822) 999999",
        "placeholder": "+7 (3822) ______",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Неверно введен номер телефона. Введите номер телефона."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Тип метка",
      "Тип",
      "Лицевой счёт",
      "Номер телефона",
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "sibtelesystem",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Сибирские Телесистемы»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/sibtelesystem.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Номер договора": {
        "block": "text",
        "mask": "9999999",
        "maskPlaceholder": "_",
        "label": "Номер договора",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "значение && изЦифр",
            "errorMessage": "Проверьте правильность введеного номера договора. Номер договора состоит из цифр и содержит 7 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Номер договора",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "skala",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО ЧОП «Скала-Техно», ИНН 7017114633",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/skala.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Номер договора": {
        "block": "text",
        "label": "Номер договора",
        "labelWidth": 3,
        "mask": "999?99999999999999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "3 <= длина && длина <= 20 && обязательное",
            "errorMessage": "Проверьте правильность введенного номера договора. Номер договора состоит из цифр и содержит от 3 до 20 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "static",
        "label": "Сумма",
        "labelWidth": 3,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "isGlobalError": true,
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ],
        "summarize": [
          "Сумма оплаты",
          "Пени"
        ]
      },
      "Сумма оплаты": {
        "label": "Сумма оплаты",
        "labelWidth": 3,
        "block": "text",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "placeholder": 0
      },
      "Пени": {
        "label": "Пени",
        "labelWidth": 3,
        "block": "text",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "placeholder": 0
      }
    },
    "schema": [
      "Номер договора",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма оплаты",
      "Пени",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tdskuyut",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ЗАО «Уют ТДСК», ИНН 7017004711",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tdskuyut.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_tdskuyut.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "99?999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина >= 2 && длина <= 5 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 2 до 5 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Содержание жилья метка": {
        "block": "label",
        "labelText": "Содержание жилья",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Содержание жилья": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Пени метка": {
        "block": "label",
        "labelText": "Пени",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Пени": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Сумма": {
        "block": "static",
        "label": "Сумма",
        "labelWidth": 3,
        "value": 0,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "summarize": [
          "Вид платежа и сумма"
        ],
        "validationRules": [
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб.",
            "isGlobalError": true
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      {
        "block": "fieldset",
        "alias": "Вид платежа и сумма",
        "label": "Вид платежа и сумма",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          },
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          }
        ],
        "items": [
          "Содержание жилья метка",
          "Содержание жилья",
          "Пени метка",
          "Пени"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
    "formKey": "tgk11tomsk",
  	"block": "modal",
  	"isActive": true,
      "startDateTime": "2016-01-20 00:00:00",
  	"title": "АО \"ТомскРТС\" (Томский филиал АО \"ТГК-11\")",
  	"subtitle": "АО \"ТомскРТС\" (Томский филиал АО \"ТГК-11\")",
  	"imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tgk11tomsk.jpg",
  	"iagree": {
  		"htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями</a> договора-оферты",
  		"checked": false
  	},
  	"submitButton": {
  		"label": "Перейти к оплате"
  	},
  	"help": {
  		"message": "Где посмотреть штрихкод?",
  		"helpTitle": "Штрихкод указан на вашей квитанции:",
  		"imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_tgk11tomsk.jpg"
  	},
  	"body": {
  		"form": {
  			"адрес": {
  				"block": "text",
  				"label": "Адрес",
  				"labelWidth": 3,
  				"disabled": true
  			},
  			"порядковый-номер-прибора-учета": {
  				"block": "label",
  				"labelText": "1"
  			},
  			"предыдущие-показания": {
  				"block": "text",
  				"disabled": true,
  				"textAlign": "center",
  				"returnNumber": true
  			},
  			"текущие-показания": {
  				"block": "text",
  				"textAlign": "center",
  				"returnNumber": true
  			},
  			"расход": {
  				"block": "text",
  				"value": "0",
  				"disabled": true,
  				"textAlign": "center"
  			},
  			"услуги-гвс": {
  				"label": "К оплате за ГВС",
  				"labelWidth": 3,
  				"block": "text",
  				"textAlign": "right",
  				"rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>",
  				"placeholder": 0
  			},
  			"услуги-пени": {
  				"label": "Пени",
  				"labelWidth": 3,
  				"block": "text",
  				"textAlign": "right",
  				"rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>",
  				"placeholder": 0
  			},
  			"всего-к-оплате": {
  				"block": "static",
  				"label": "Сумма",
  				"labelWidth": 3,
  				"rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>",
  				"validationRules": [
  					{
  						"rule": "5 <= Сумма && Сумма <= 13000",
  						"isGlobalError": true,
  						"errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
  					}
  				],
  				"summarize": [
  					"услуги-гвс",
  					"услуги-пени",
  					"услуги-отопление"
  				]
  			},
  			"услуги-отопление": {
  				"block": "radioGroupWithContainers",
  				"label": "Вариант оплаты",
  				"labelWidth": 3,
  				"options": [
  					{
  						"name": "К оплате за отопление (1/12):<br><small>(с учётом общей задолженности)</small><br>",
  						"value": "1/12",
  						"checked": true
  					},
  					{
  						"name": "К оплате за отопление (Факт):<br><small>(с учётом общей задолженности)</small><br>",
  						"value": "Факт"
  					}
  				],
  				"containerOptions": {
  					"1/12": [
  						{
  							"name": "к-оплате",
  							"block": "text",
  							"textAlign": "right",
  							"inputWidth": "138px",
  							"rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>",
  							"placeholder": 0
  						}
  					],
  					"Факт": [
  						{
  							"name": "к-оплате",
  							"block": "text",
  							"textAlign": "right",
  							"inputWidth": "138px",
  							"rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>",
  							"placeholder": 0
  						}
  					]
  				}
  			}
  		},
  		"schema": [
  			"штрихкод",
  			"фио-одной-строкой",
  			"адрес",
  			"услуги-отопление",
  			{
  				"block": "table",
  				"label": "Введите показания приборов учёта",
  				"name": "приборы-учета",
  				"columnHeaders": [],
  				"layout": {
  					"columns": 4,
  					"width": [
  						3,
  						2,
  						2,
  						2
  					]
  				},
  				"items": []
  			},
  			"услуги-гвс",
  			"услуги-пени",
  			"всего-к-оплате"
  		]
  	}
}
'),
('{
  "formKey": "TochkaSpa",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ТОЧКА СПА»",
  "subtitle": "ООО «ТОЧКА СПА»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tochkaspa.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Артикул": {
        "block": "input",
        "type": "text",
        "label": "Артикул",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "артикул",
            "errorMessage": "Проверьте правильность введенного артикула. Артикул состоит из букв и цифр и содержит от 7 до 9 символов."
          }
        ]
      },
      "Номер телефона": {
        "block": "text",
        "label": "Номер телефона",
        "labelWidth": 3,
        "mask": "+7 9999999999",
        "maskPlaceholder": "_",
        "inputWidth": "215px",
        "validationRules": [
          {
            "rule": "телефон",
            "errorMessage": "Проверьте правильность введенного номера. Номер телефона необходимо ввести в формате +7 9241234567."
          }
        ]
      },
      "email": {
        "block": "input",
        "label": "E-mail",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "email",
            "errorMessage": "Адрес электронной почты указан неверно. Адрес должен иметь формат name@example."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "value": "825.54",
        "label": "Сумма",
        "labelWidth": 3,
        "textAlign": "right",
        "inputWidth": "200px",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Артикул",
      "Номер телефона",
      "email",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tomica",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "Агентство ТОМИКА",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tomica_cyberplat.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "id": {
        "block": "input",
        "type": "text",
        "label": "Идентификационный номер плательщика (ID)",
        "labelWidth": 3,
        "name": "accountId",
        "validationRules": [
          {
            "rule": "значение && изЦифр",
            "errorMessage": "Неверно указан идентификационный номер плательщика."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "id",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tomtel",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «ТОМТЕЛ»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tomtelosmp.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "name": "accountId",
        "mask": "9?9999999999999999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина > 0 && длина < 21 && изЦифр",
            "errorMessage": "Проверьте правильность введеного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 20 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tomtelinet",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Томсктелесети» Интернет, г. Томск",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tomtel.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "name": "accountId",
        "mask": "9?99999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина >= 1 && длина <= 6 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Номер лицевого счета состоит из цифр и содержит от 1 до 6 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tomteltv",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Томсктелесети» Кабельное ТВ, г. Томск",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tomtel.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?99999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина >= 1 && длина <= 6 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Номер лицевого счета состоит из цифр и содержит от 1 до 6 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма < 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "pre_tsjaruna",
  "block": "modal",
  "isActive": true,
  "mainField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «АрунА», ИНН 7017157997",
  "subtitle": "ТСЖ «АрунА», ИНН 7017157997",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "submitButton": {
    "label": "Далее"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "input",
        "type": "text",
        "label": "Штрихкод",
        "labelWidth": 3,
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "штрихкод",
            "errorMessage": "Проверьте правильность введенного штрихкода. Штрихкод состоит из цифр и содержит 13 символов."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод"
    ]
  }
}
'),
('{
  "formKey": "tsjaruna",
  "block": "modal",
  "isActive": true,
  "accrualCollection" : "tsjaruna",
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «АрунА», ИНН 7017157997",
  "subtitle": "ТСЖ «АрунА», ИНН 7017157997",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "text",
        "label": "Штрихкод",
        "labelWidth": 3,
        "mask": "9999999999999",
        "maskPlaceholder": "_",
        "value": "3529150001123",
        "disabled": true,
        "validationRules": [
          {
            "rule": "штрихкод",
            "errorMessage": "Проверьте правильность введенного штрихкода. Штрихкод состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "input",
        "type": "text",
        "label": "Адрес",
        "labelWidth": 3,
        "name": "Адрес",
        "disabled": true,
        "value": "Томск, ул.Иркутский пр-д, д 13, кв. 11"
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "value": "825.54",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tsjluxemburg",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «Люксембург» , ИНН 7017233415",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_tsjluxemburg.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина >= 1 && длина <= 4 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 4 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tsjubileynoe",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «Юбилейное», г.Томск, ИНН 7017293661",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/tsjubileynoe.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_tsjubileynoe.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?99",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина >= 1 && длина <= 3 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 3 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "tsjvenera",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ТСЖ «Венера», ИНН 7017147251",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "value": "825.54",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "1 <= Сумма && Сумма <= 15000",
            "errorMessage": "Сумма платежа не может быть меньше 1 руб. и больше 15 000 руб."
          }
        ]
      }
    },
    "schema": [
      {
      "block": "fieldset",
      "label": "Плательщик",
      "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
      "labelWidth": 3,
      "items": [
        "Фамилия",
        "Имя",
        "Отчество"
      ]
    },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukcentralnaya080",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК Центральная», ИНН 7017192470",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukcentralnaya.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukcentral.png"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "9999999999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukcentralnaya217",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК Центральная», ИНН 7017192470",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukcentralnaya.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukcentral.png"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "9999999999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukcentralnaya470",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК Центральная», ИНН 7017196080",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukcentralnaya.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukcentral.png"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "9999999999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukgromada",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК «Громада», ИНН 7017157242",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukgromada.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "mask": "99999",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "длина == 5 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 5 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukistochnoye",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Управляющая компания «Источное», ИНН 7017120796",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukistochnoye.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukistochnoye.png"
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "text",
        "label": "Штрихкод",
        "labelWidth": 3,
        "mask": "9999999999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "значение && длина == 13",
            "errorMessage": "Проверьте правильность введенного штрихкода. Штрихкод состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Содержание жилья метка": {
        "block": "label",
        "labelText": "Содержание жилья, ?"
      },
      "Содержание жилья": {
        "block": "text",
        "placeholder": 0,
        "textAlign": "right",
        "isSummable": true
      },
      "Сумма": {
        "block": "static",
        "label": "Сумма",
        "labelWidth": 3,
        "value": 0,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "summarize": [
          "Вид платежа и сумма"
        ],
        "validationRules": [
          {
            "isGlobalError": true,
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      {
        "alias": "Вид платежа и сумма",
        "block": "table",
        "label": "Вид платежа и сумма",
        "labelWidth": 3,
        "layout": {
          "columns": 2,
          "width": [
            9,
            3
          ]
        },
        "items": [
          "Содержание жилья метка",
          "Содержание жилья"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukjilremservis1",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК «Жилремсервис-I», ИНН 7017171110",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukjilremservis1.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukjilremservis1.png"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "labelWidth": 3,
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "обязательное",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukkirovskij_massiv",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК «Кировс?кий Массив»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukkirovskij_massiv.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "значение",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukmayak",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Маякъ»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukmayak.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukmayak.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?9999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина >= 1 && длина <= 5 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 5 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Содержание жилья метка": {
        "block": "label",
        "labelText": "Содержание жилья",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Содержание жилья": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Пени метка": {
        "block": "label",
        "labelText": "Пени",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Пени": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Сумма": {
        "block": "static",
        "label": "Сумма",
        "labelWidth": 3,
        "value": 0,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "summarize": [
          "Вид платежа и сумма"
        ],
        "validationRules": [
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб.",
            "isGlobalError": true
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      {
        "block": "fieldset",
        "alias": "Вид платежа и сумма",
        "label": "Вид платежа и сумма",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          },
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          }
        ],
        "items": [
          "Содержание жилья метка",
          "Содержание жилья",
          "Пени метка",
          "Пени"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukmoydomtomsk",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО УК «Мой дом», ИНН 7017177224",
  "subtitle": "ООО УК «Мой дом», ИНН 7017177224",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukmoydomtomsk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть данные для проверки?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukmoydomtomsk.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "input",
        "type": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "длина == 5 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 5 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "value": "825.54",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "1 <= Сумма && Сумма <= 15000",
            "errorMessage": "Сумма платежа не может быть меньше 1 руб. и больше 15 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "uknashdom",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК «Наш дом», ИНН 7014044716",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_uknashdom.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?99",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "1 <= длина && длина <= 3 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 3 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Содержание жилья метка": {
        "block": "label",
        "labelText": "Содержание жилья,",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Содержание жилья": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Текущий ремонт метка": {
        "block": "label",
        "labelText": "Текущий ремонт,",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Текущий ремонт": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Капитальный ремонт метка": {
        "block": "label",
        "labelText": "Капитальный ремонт,",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"></span>"
      },
      "Капитальный ремонт": {
        "block": "text",
        "value": 0,
        "isSummable": true,
        "textAlign": "right"
      },
      "Сумма": {
        "block": "static",
        "label": "Сумма",
        "labelWidth": 3,
        "value": 0,
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "summarize": [
          "Вид платежа и сумма"
        ],
        "validationRules": [
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб.",
            "isGlobalError": true
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      {
        "block": "fieldset",
        "alias": "Вид платежа и сумма",
        "label": "Вид платежа и сумма",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          },
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          },
          {
            "count": 2,
            "width": [
              9,
              3
            ]
          }
        ],
        "items": [
          "Содержание жилья метка",
          "Содержание жилья",
          "Текущий ремонт метка",
          "Текущий ремонт",
          "Капитальный ремонт метка",
          "Капитальный ремонт"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "uknewage",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК Новый Век», ИНН 7017344330",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/uknewage.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "help": {
    "message": "Где посмотреть штрихкод?",
    "helpTitle": "Штрихкод указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_uknewage.png"
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод": {
        "block": "text",
        "label": "Штрихкод",
        "labelWidth": 3,
        "mask": "9999999999999",
        "maskPlaceholder": "_",
        "validationRules": [
          {
            "rule": "значение",
            "errorMessage": "Проверьте правильность введенного штрихкода квитанции. Штрихкод квитанции состоит из цифр и содержит 13 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukremstrojbyt",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК Ремстройбыт», ИНН 7017143271",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukremstrojbyt.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?9999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "1 <= длина && длина <= 5 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счёта. Лицевой счёт состоит из цифр и содержит от 1 до 5 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "05",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "uksoyuz",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК «Союз»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/uksoyuz.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Штрихкод квитанции": {
        "block": "text",
        "label": "Штрихкод квитанции",
        "mask": "999999 99999-9",
        "maskPlaceholder": "_",
        "labelWidth": 3,
        "validationRules": [
          {
            "rule": "значение",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит 12 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Штрихкод квитанции",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukstrojsojuz",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК СТРОЙСОЮЗ»",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukstrojsojuz.jpg",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "help": {
    "message": "Где посмотреть лицевой счёт?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukstrojsojuz.png"
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?9999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "1 <= длина && длина <= 5 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 5 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указан адрес. Введите адрес."
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "ukzhilfond",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «УК «ЖилФонд», ИНН 7017110830",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/ukzhilfond.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "help": {
    "message": "Где посмотреть данные для проверки?",
    "helpTitle": "Лицевой счёт указан на вашей квитанции:",
    "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_ukzhilfond.png"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9999?99",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "4 <= длина && длина <= 6 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 4 до 6 символов."
          }
        ]
      },
      "Фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Адрес": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "Месяц": {
        "block": "select",
        "value": "06",
        "values": [
          {
            "name": "январь",
            "value": "01"
          },
          {
            "name": "февраль",
            "value": "02"
          },
          {
            "name": "март",
            "value": "03"
          },
          {
            "name": "апрель",
            "value": "04"
          },
          {
            "name": "май",
            "value": "05"
          },
          {
            "name": "июнь",
            "value": "06"
          },
          {
            "name": "июль",
            "value": "07"
          },
          {
            "name": "август",
            "value": "08"
          },
          {
            "name": "сентябрь",
            "value": "09"
          },
          {
            "name": "октябрь",
            "value": "10"
          },
          {
            "name": "ноябрь",
            "value": "11"
          },
          {
            "name": "декабрь",
            "value": "12"
          }
        ]
      },
      "Год": {
        "block": "select",
        "value": "2016",
        "values": [
          {
            "name": "2017",
            "value": "2017"
          },
          {
            "name": "2016",
            "value": "2016"
          },
          {
            "name": "2015",
            "value": "2015"
          },
          {
            "name": "2014",
            "value": "2014"
          }
        ]
      },
      "Назначение платежа": {
        "block": "radiogroup",
        "label": "Выберите назначение платежа",
        "labelWidth": 3,
        "options": [
          {
            "name": "Оплата услуг",
            "value": "uslugi",
            "checked": true
          },
          {
            "name": "Оплата пени",
            "value": "peni"
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "1 <= Сумма && Сумма <= 15000",
            "errorMessage": "Сумма платежа не может быть меньше 1 руб. и больше 15 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "labelWidth": 3,
        "items": [
          "Фамилия",
          "Имя",
          "Отчество"
        ]
      },
      "Адрес",
      {
        "block": "fieldset",
        "label": "Период оплаты",
        "labelWidth": 3,
        "layout": [
          {
            "count": 2,
            "width": [
              4,
              4
            ]
          }
        ],
        "items": [
          "Месяц",
          "Год"
        ]
      },
      "Назначение платежа",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "vozminet",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "Vozmi.Net (ООО «Модэл»)",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/vozminet.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "Лицевой счёт": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "mask": "9?99999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "1 <= длина && длина <= 6 && изЦифр",
            "errorMessage": "Проверьте правильность введенного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 6 символов."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "placeholder": "0",
        "label": "Сумма",
        "labelWidth": 3,
        "inputWidth": "200px",
        "textAlign": "right",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "1 <= Сумма && Сумма <= 15000",
            "errorMessage": "Сумма платежа не может быть меньше 1 руб. и больше 15 000 руб."
          }
        ]
      }
    },
    "schema": [
      "Лицевой счёт",
      "Сумма"
    ]
  }
}
'),
('{
  "formKey": "zhilremservice",
  "block": "modal",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "title": "ООО «Жилремсервис», ИНН 7017095356",
  "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
  "iagree": {
    "htmlLabel": "Я соглашаюсь с <a href=\"/legal-information/docs/\" target=\"_blank\">условиями<\/a> договора-оферты",
    "checked": false
  },
  "submitButton": {
    "label": "Перейти к оплате"
  },
  "body": {
    "form": {
      "accountId": {
        "block": "text",
        "label": "Лицевой счёт",
        "labelWidth": 3,
        "name": "accountId",
        "mask": "9?9999999",
        "maskPlaceholder": "",
        "validationRules": [
          {
            "rule": "длина > 0 && длина < 9 && изЦифр",
            "errorMessage": "Проверьте правильность введеного лицевого счета. Лицевой счет состоит из цифр и содержит от 1 до 8 символов."
          }
        ]
      },
      "address": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "address": {
            "validationRules": [
              {
                "rule": "адрес",
                "errorMessage": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''"
              }
            ]
          },
          "house": {
            "validationRules": [
              {
                "rule": "дом",
                "errorMessage": "Неверно указан номер дома. Введите номер дома."
              }
            ]
          },
          "additional": {
            "block": "input"
          },
          "apartment": {
            "block": "input"
          }
        },
        "settings": {
          "type": "POST",
          "dataType": "json",
          "headers": {
            "Authorization": "Token ffc44375a4d8268790b8aa1bc9d840aedd5aca40",
            "Content-Type": "application/json"
          },
          "data": {
            "count": 20,
            "locations": [
              "Омская"
            ]
          }
        }
      },
      "lastname": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "name": "lastname",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "firstname": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "patronym": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [
          {
            "rule": "обязательное && русский",
            "errorMessage": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв."
          }
        ]
      },
      "Сумма": {
        "block": "text",
        "textAlign": "right",
        "placeholder": "0",
        "type": "text",
        "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\"><\/span>",
        "inputWidth": "200px",
        "labelWidth": 3,
        "label": "Сумма",
        "validationRules": [
          {
            "rule": "число",
            "errorMessage": "Неверно указана сумма."
          },
          {
            "rule": "5 <= Сумма && Сумма <= 13000",
            "errorMessage": "Сумма платежа не может быть меньше 5 руб. и больше 13 000 руб."
          }
        ]
      }
    },
    "schema": [
      "accountId",
      {
        "block": "fieldset",
        "label": "Плательщик",
        "labelWidth": 3,
        "items": [
          "lastname",
          "firstname",
          "patronym"
        ]
      },
      "address",
      "Сумма"
    ]
  }
}
'),
('{
    "size": "sm",
    "block": "modal",
    "formKey": "login",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00",
    "formHeader": "Вход",
    "formHeaderNewFormLink": {
      "formUrl": "http://localhost:3000/static/examples/forms/registration.json",
      "text": "Регистрация",
      "href": "#"
    },
    "submitButton": {
      "label": "Войти",
      "labelWidth": 8,
      "cls": "btn-default",
      "container": "body"
    },
    "body": {
      "form": {
        "email": {
          "block": "input",
          "placeholder": "электронная почта"
        },
        "пароль": {
          "block": "input",
          "type": "password",
          "placeholder": "пароль",
          "newFormLink": {
            "formUrl": "/api/configs/",
            "params": {
              "messageMapId": "getForm",
              "formKey": "restore_password"
            },
            "text": "Напомнить",
            "href": "#"
          }
        }
      },
      "schema": [
        {
          "block": "fieldset",
          "validateOnlyFirstField": true,
          "items": [
            "email",
            "пароль"
          ],
          "layout": [
            1,
            1
          ]
        }
      ]
    }
  }
'),
('{
    "size": "sm",
    "block": "modal",
    "formKey": "restore_password",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00",
    "formHeader": "Напомнить пароль",
    "submitButton": {
      "label": "Отправить",
      "labelWidth": 8,
      "cls": "btn-default",
      "container": "body"
    },
    "body": {
      "form": {
        "email": {
          "block": "input",
          "placeholder": "e-mail"
        },
        "forgotPasswordComment": {
          "block": "comment",
          "text": "Укажите свой e-mail, и мы отправим вам письмо со ссылкой для восстановления пароля:",
          "fontColor": "#252525",
          "fontSize": "13px"
        }
      },
      "schema": [
        "forgotPasswordComment",
        "email"
      ]
    }
  }
'),
('{
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "formKey": "new_password",
  "size": "sm",
  "block": "modal",
  "formHeader": "Новый пароль",
  "formHeaderNewFormLink": {
    "formUrl": "http://localhost:3000/static/examples/forms/registration.json",
    "text": "Введите новый пароль:",
    "href": "#"
  },
  "submitButton": {
    "label": "Поменять",
    "labelWidth": 8,
    "cls": "btn-default",
    "container": "body"
  },
  "body": {
    "form": {
      "пароль": {
        "block": "input",
        "type": "password",
        "placeholder": "пароль*"
      },
      "подтверждение-пароля": {
        "block": "input",
        "type": "password",
        "placeholder": "подтвердить пароль*"
      }
    },
    "schema": [
      {
        "block": "fieldset",
        "validateOnlyFirstField": true,
        "items": [
          "пароль",
          "подтверждение-пароля"
        ],
        "layout": [
          1,
          1
        ]
      }
    ]
  }
}
'),
('{
    "formKey": "update_user_account",
    "isActive": true,
    "startDateTime": "01-01-2016 00:00:00",
    "cls": "form",
    "form": {
      "фамилия": {
        "block": "input",
        "type": "text",
        "placeholder": "фамилия",
        "name": "фамилия",
        "validationRules": [

        ]
      },
      "имя": {
        "block": "input",
        "type": "text",
        "placeholder": "имя",
        "validationRules": [

        ]
      },
      "отчество": {
        "block": "input",
        "type": "text",
        "placeholder": "отчество",
        "validationRules": [

        ]
      },
      "email": {
        "block": "input",
        "label": "Эл. почта",
        "labelWidth": 3,
        "placeholder": "email",
        "validationRules": [

        ]
      },
      "дата-рождения": {
        "labelWidth": 3,
        "label": "Дата рождения",
        "block": "date",
        "placeholder": "ДД/ММ/ГГГГ",
        "validationRules": [

        ]
      },
      "телефон": {
        "labelWidth": 3,
        "inputWidth": "28%",
        "block": "text",
        "mask": "+7 999 999-99-99",
        "label": "Моб. телефон",
        "maskPlaceholder": "_",
        "placeholder": "+7 ___ ___-__-__"
      },
      "область": {
        "labelWidth": 3,
        "label": "Регион",
        "block": "select",
        "values": [
          {
            "name": "Республика Адыгея",
            "value": "adyg_resp"
          },
          {
            "name": "Республика Алтай",
            "value": "altay_resp"
          },
          {
            "name": "Омская область",
            "value": "omsk_oblast",
            "selected": true
          }
        ]
      },
      "address": {
        "block": "address",
        "minLength": 3,
        "highlight": true,
        "label": "Адрес",
        "labelWidth": 3,
        "layout": [
          1,
          3
        ],
        "url": "https://dadata.ru/api/v2/suggest/address/",
        "items": {
          "адрес-одной-строкой": {
            "validationRules": [

            ]
          },
          "дом": {
            "validationRules": [

            ]
          },
          "корпус": {
            "block": "input"
          },
          "квартира": {
            "block": "input"
          }
        }
      },
      "старый-пароль": {
        "block": "input",
        "type": "password",
        "placeholder": "старый пароль"
      },
      "пароль": {
        "block": "input",
        "type": "password",
        "placeholder": "новый пароль"
      },
      "подтверждение-пароля": {
        "block": "input",
        "type": "password",
        "placeholder": "новый пароль"
      },
      "div": {
        "block": "divider"
      },
      "updates": {
        "htmlLabel": "Согласен получать обновления",
        "block": "checkbox",
        "checked": true,
        "labelWidth": 3,
        "name": "updates"
      },
      "news": {
        "htmlLabel": "Согласен получать новости и акции ",
        "block": "checkbox",
        "checked": true,
        "labelWidth": 3,
        "name": "news"
      },
      "okButton": {
        "block": "button",
        "type": "submit",
        "name": "submitButton",
        "label": "Сохранить изменения",
        "labelWidth": 3,
        "cls": "btn-primary"
      }
    },
    "schema": [
      {
        "block": "fieldset",
        "label": "Плательщик",
        "labelWidth": 3,
        "items": [
          "фамилия",
          "имя",
          "отчество"
        ]
      },
      "email",
      "дата-рождения",
      "телефон",
      "область",
      "address",
      {
        "block": "fieldset",
        "label": "Сменить пароль",
        "labelWidth": 3,
        "items": [
          "старый-пароль",
          "пароль",
          "подтверждение-пароля"
        ]
      },
      "div",
      "updates",
      "news",
      "div",
      "okButton"
    ]
  }
'),
('{
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "formKey": "payment_success",
     "items": [{
        "name": "successPaymentModalHeader",
        "block": "successPaymentModalHeader",
        "imageUrl": "https://www-test.vseplatezhi.ru/resources/media/images/registration/bigOk.png",
        "title": "Оплата прошла успешно",
        "receiptTitle": "Посмотреть квитанцию",
        "receiptImage": "https://www-test.vseplatezhi.ru/resources/media/images/categories/kvitanciya.png"
      }, {
        "block": "successPaymentModalBody",
        "title": "Заодно оплатите все квитанции ЖКХ",
        "items": [{
          "imageUrl": "https://www-test.vseplatezhi.ru/resources/media/images/categories/water.png",
          "title": "Вода",
          "link": "/payments/zhkh#providers"
        }, {
          "imageUrl": "https://www-test.vseplatezhi.ru/resources/media/images/categories/electr.png",
          "title": "Электричество",
          "link": "/payments/zhkh#providers"
        }, {
          "imageUrl": "https://www-test.vseplatezhi.ru/resources/media/images/categories/gas.png",
          "title": "Газ",
          "link": "/payments/zhkh#providers"
        }],
        "buttonName": "Оплатить за 5 минут"
      }
     ]
}
'),
('{
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00",
    "formKey": "levobereje",
    "block": "modal",
    "title": "ЗАО «УК «ЛЕВОБЕРЕЖЬЕ», ИНН 5503245214",
    "subtitle": "Уважаемые пользователи, получателем денежных средств является ООО «Телекомсервис», в рамках действующего договора с ЗАО «УК «ЛЕВОБЕРЕЖЬЕ». ИНН 5503245214.",
    "imgUrl": "https://vp.ru/pay/resources/media/images/provider/logo/default/uk.png",
    "submitButton": {
      "label": "Далее"
    },
    "help": {
      "message": "Где посмотреть лицевой счет?",
      "imageUrl": "https://vp.ru/resources/media/images/provider/receipt/receipt_lvb_n.png"
    },
    "body": {
      "form": {
        "лицевой-счет": {
          "block": "text",
          "mask": "9999 9999999",
          "maskPlaceholder": "_",
          "disabled": true,
          "label": "Лицевой счёт",
          "labelWidth": 3,
          "name": "лицевой-счет"
        },
        "плательщик": {
          "block": "text",
          "disabled": true,
          "label": "Инициалы собственника квартиры",
          "labelWidth": 3,
          "name": "плательщик"
        },
        "адрес-одной-строкой": {
          "block": "static",
          "label": "Адрес",
          "labelWidth": 3,
          "name": "адрес-одной-строкой"
        },
        "наименование-услуги": {
          "block": "label",
          "labelText": "1",
          "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>"
        },
        "к-оплате": {
          "block": "text",
          "textAlign": "right",
          "returnNumber": true
        },
        "всего-к-оплате": {
          "block": "static",
          "label": "Сумма",
          "labelWidth": 3,
          "rightMark": "<span class=\"glyphicon glyphicon-ruble\" aria-hidden=\"true\" style=\"font-size: 11px\"><\/span>",
          "validationRules": [
          ],
          "summarize": [
            "услуги"
          ]
        }
      },
      "schema": [
        "лицевой-счет",
        "плательщик",
        "адрес-одной-строкой",
        "период-оплаты",
        {
          "name": "услуги",
          "block": "fieldset",
          "label": "Вид платежа и сумма",
          "alias": "платеж1",
          "labelWidth": 3,
          "layout": [
            {
              "count": 2,
              "width": [
                9,
                3
              ]
            }
          ],
          "items": [
          ]
        },
        "всего-к-оплате"
      ]
    }
}
'),
('{
  "formKey": "jkumoskvaepd",
  "isActive": true,
  "startDateTime": "2016-01-21 00:00:00"
}
');