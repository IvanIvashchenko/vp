DROP TABLE IF EXISTS errors;

CREATE TABLE errors(
id SERIAL,
document JSONB NOT NULL
);

INSERT INTO errors (document) VALUES
('{
    "name": "wrong_street",
    "message": "Неверно указана улица. Название улицы может состоять из русских букв, цифр, точек, запятых и знаков /, ,\", ;, -, (, ),''",
    "errorsID": "f8a8ac53-7e1e-4fe7-a827-b8a4dca0544f",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00"
}'),
('{
    "name": "wrong_house",
    "message": "Неверно указан номер дома. Введите номер дома.",
    "errorsID": "d5a85e73-6bdd-4199-81bd-2d8c99ba1950",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00"
}'),
('{
    "name": "wrong_lastname",
    "message": "Неверно введена фамилия. Значение должно быть указано и состоять только из русских букв.",
    "errorsID": "bcd7582b-b18b-433f-9342-6c80971a782b",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00"
}'),
('{
    "name": "wrong_name",
    "message": "Неверно введено имя. Значение должно быть указано и состоять только из русских букв.",
    "errorsID": "99c445f6-437e-49b4-8aae-5bf372e11abf",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00"
}'),
('{
    "name": "wrong_patronym",
    "message": "Неверно введено отчество. Значение должно быть указано и состоять только из русских букв.",
    "errorsID": "37ac3a83-8914-4256-b937-f0f87c55e565",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00"
}'),
('{
    "name": "wrong_address",
    "message": "Неверно указан адрес. Введите адрес.",
    "errorsID": "83d83bc7-39bc-4f08-a96d-25ad51c86d5a",
    "isActive": true,
    "startDateTime": "2016-01-20 00:00:00"
}'),
('{
   "name": "wrong_birthday",
   "message": "Дата рождения является обязательным полем и должна быть в формате ДД/ММ/ГГГГ",
   "errorsID": "bde21f1c-b93e-4c8f-8772-8e52d08895ed",
   "isActive": true,
   "startDateTime": "2016-01-20 00:00:00"
}'),
('{
    "name": "only_numbers",
    "message": "Поле может содержать только цифры",
    "errorsID": "7d84ec6f-a212-4541-8606-3a468c4c9ca1",
    "isActive": true,
    "startDateTime": "2016-12-01 19:28:16"
}')
;