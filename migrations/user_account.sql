-- Table structure for table "user_account" - коллекция пользователей
DROP TABLE IF EXISTS "user_account";
CREATE TABLE IF NOT EXISTS "user_account" (
  "document" jsonb DEFAULT NULL
) WITH (
OIDS=FALSE
);

INSERT INTO "user_account" ("document") VALUES
('{
  "email": "test_vp@mail.ru",
 "lastName": "Ivanov",
 "password": "not_changed_pass",
 "firstName": "Igor",
 "middleName": "Batoovich",
 "dateOfBirth": "13.05.1988",
 "user_accountID": "bf258b87-f6e6-41df-aede-860fb41558f1"
}');