
--
-- Table structure for table "receive_rule"
--
DROP TABLE IF EXISTS "receive_rule";
CREATE TABLE IF NOT EXISTS "receive_rule" (
"document" jsonb DEFAULT NULL
) WITH (
  OIDS=FALSE
);

--
--Dumping data for table "provider"
--
INSERT INTO "receive_rule" ("document") VALUES
('{
      "ид": "guid",
      "начало": "12-04-2015 00:00:00",
      "завершение": "11-22-2100 00:00:00",
      "ид_услуги": "beeline_сотовая_связь",
      "получатели": [{"fix": 1, "ид": "00011449910569721700286562163249", "percent": 10, "остаток": true}],
      "isActive": true,
      "startDateTime": "2016-01-20 00:00:00"
}');