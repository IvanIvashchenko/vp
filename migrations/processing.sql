DROP TABLE IF EXISTS processing;

CREATE TABLE processing(
id SERIAL,
document JSONB NOT NULL
);

INSERT INTO processing (document) VALUES
('{
    "name": "apelsin_beeline",
    "processing": "apelsin",
    "type": "payment",
    "params": [
        {
            "name": "acc",
            "value": "клиент/телефон"
        },
        {
            "name": "check",
            "value": "number"
        },
        {
            "name": "id",
            "value": "number"
        },
        {
            "name": "amount",
            "value": "сумма_без_комиссии"
        }
     ],
    "customParams": [
        {
            "strategy": "apelsin_getDateStrategy"
        },
        {
            "strategy": "apelsin_getTimeStrategy"
        },
        {
            "strategy": "apelsin_setSignStrategy"
        }
    ],
    "isActive": true,
    "startDateTime": "2016-12-01 19:28:16"
}'),
('{
    "name": "moneta_gibdd",
    "processing": "moneta",
    "type": "payment",
    "payee": "9171",
    "isPayerAmount": false,
    "params": [
        {
            "name": "Envelope/Body/PaymentRequest/amount",
            "value": "всего-к-оплате"
        },
        {
            "name": "Envelope/Body/PaymentRequest/clientTransaction",
            "value": "number"
        }
    ],
    "isActive": true,
    "startDateTime": "2016-12-01 19:28:16"
}'),
('{
    "name": "moneta_gibdd",
    "processing": "moneta",
    "type": "calculation",
    "payee": "9171.1",
    "params": [
        {
            "id": "102",
            "name": "GIBDD_TS_REGISTRATION_NUMBER",
            "code": "CUSTOMFIELD:102",
            "searchValue": "1"
        },
        {
            "id": "103",
            "name": "GIBDD_LICENSE_NUMBER",
            "code": "CUSTOMFIELD:103",
            "searchValue": "1"
        },
        {
            "id": "108",
            "name": "GIBDD_ALTPAYERIDENTIFIER",
            "code": "CUSTOMFIELD:108",
            "searchValue": "5"
        }
    ],
    "isActive": true,
    "startDateTime": "2016-12-01 19:28:16"
}'),
('{
    "name": "rapida_beeline",
    "processing": "rapida",
    "PaymSubjTp": "101",
    "params": [
        {
            "name": "PaymExtId",
            "value": "number"
        },
        {
            "name": "Amount",
            "value": "всего-к-оплате"
        },
        {
            "name": "FeeSum",
            "value": "комиссия"
        }
    ],
    "requestParams": [
      {
        "code": "188",
        "value": "клиент/лицевой-счет/одной-строкой",
        "strategy": "transformation_substring",
        "strategyParams": {
          "beginIndex": 2,
          "endIndex": 12
        }
      }
    ],
    "isActive": true,
    "startDateTime": "2016-12-01 19:28:16"
}'),
('{
    "name": "rapida_beeline",
    "processing": "rapida",
    "type": "check_status",
    "params": [
        {
            "name": "PaymExtId",
            "value": "number"
        }
    ],
    "isActive": true,
    "startDateTime": "2016-12-01 19:28:16"
}'),
('{
    "name": "rapida_jkumoskvaepd",
    "processing": "rapida",
    "type": "calculation",
    "PaymSubjTp": "200",
    "requestParams": [
      {
        "code": "193",
        "value": "account"
      },
      {
        "code": "237",
        "value": "month"
      },
      {
        "code": "238",
        "value": "year"
      }
    ],
    "isActive": true,
    "startDateTime": "2016-12-01 19:28:16"
}'),
('{
    "name": "apelsin_levobereje",
    "processing": "apelsin",
    "type": "calculation",
    "params": [
        {
            "type": "const",
            "value": "lvb_get_acc_info",
            "toField": "type"
        },
        {
            "type": "map",
            "toField": "account",
            "fromField": "account"
        },
        {
            "type": "map",
            "toField": "year",
            "fromField": "year"
        },
        {
            "type": "map",
            "toField": "month",
            "fromField": "month"
        }
    ]
}')
;