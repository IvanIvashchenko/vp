-- Table structure for table "service_schema" - коллекция схем для служебных форм
DROP TABLE IF EXISTS "service_schema";
CREATE TABLE IF NOT EXISTS "service_schema" (
  "document" jsonb DEFAULT NULL
) WITH (
OIDS=FALSE
);

INSERT INTO "service_schema" ("document") VALUES
('{
  "name": "register",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "schema": [
    {
        "name": "email"
    },
    {
        "name": "пароль",
        "from-schema": "register_form_password_from_schema_to_inner"
    },
    {
        "name": "подтверждение-пароля",
        "from-schema": "register_form_password_confirm_from_schema_to_inner"
    },
    {
        "name": "фамилия"
    },
    {
        "name": "имя"
    },
    {
        "name": "отчество"
    },
    {
        "name": "дата-рождения",
        "from-schema": "register_form_birth_date_from_schema_to_inner"
    },
    {
        "name": "область",
        "from-schema": "register_form_region_from_schema_to_inner"
    },
    {
        "name": "адрес-одной-строкой"
    },
    {
        "name": "дом"
    },
    {
        "name": "корпус"
    },
    {
        "name": "квартира"
    },
    {
        "name": "телефон",
        "from-schema": "register_form_phone_from_schema_to_inner",
        "to-schema": "register_form_phone_from_inner_to_schema"
    }
  ]
}'),
('{
  "name": "update_user_account",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "schema": [
    {
        "name": "email"
    },
    {
        "name": "старый-пароль",
        "from-schema": "register_form_old_password_from_schema_to_inner",
        "to-schema": "empty_field"
    },
    {
        "name": "пароль",
        "from-schema": "register_form_password_from_schema_to_inner",
        "to-schema": "empty_field"
    },
    {
        "name": "подтверждение-пароля",
        "to-schema": "empty_field",
        "from-schema": "register_form_password_confirm_from_schema_to_inner"
    },
    {
        "name": "фамилия"
    },
    {
        "name": "имя"
    },
    {
        "name": "отчество"
    },
    {
        "name": "дата-рождения",
        "from-schema": "register_form_birth_date_from_schema_to_inner",
        "to-schema": "register_form_birth_date_from_inner_to_schema"
    },
    {
        "name": "область",
        "from-schema": "register_form_region_from_schema_to_inner"
    },
    {
        "name": "адрес-одной-строкой"
    },
    {
        "name": "дом"
    },
    {
        "name": "корпус"
    },
    {
        "name": "квартира"
    },
    {
        "name": "телефон",
        "from-schema": "register_form_phone_from_schema_to_inner",
        "to-schema": "register_form_phone_from_inner_to_schema"
    }
  ]
}'),
('{
  "name": "login",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "schema": [
    {
        "name": "email"
    },
    {
        "name": "пароль"
    }
  ]
}'),
('{
  "name": "restore_password",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "schema": [
    {
        "name": "email"
    }
  ]
}'),
('{
  "name": "new_password",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "schema": [
    {
        "name": "пароль",
        "from-schema": "register_form_password_from_schema_to_inner"
    },
    {
        "name": "подтверждение-пароля",
        "from-schema": "register_form_password_confirm_from_schema_to_inner"
    }
  ]
}'),
('{
  "name": "payment_success",
  "isActive": true,
  "startDateTime": "2016-01-20 00:00:00",
  "schema": []
}');