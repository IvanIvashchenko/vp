

--
-- Table structure for table "provider"
--
DROP TABLE IF EXISTS "provider";
CREATE TABLE IF NOT EXISTS "provider" (
"document" jsonb DEFAULT NULL
) WITH (
  OIDS=FALSE
);

--
--Dumping data for table "provider"
--

INSERT INTO "provider" ("document") VALUES
('{
  "версия": 1,
  "name": "alfama",
  "короткое-наименование": "АЛФАМА",
  "полное-наименование": "ООО «АЛФАМА» (интернет-магазин INTERSOUND), ИНН 7017329188",
  "userIdentifyField": "номер договора",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "argus",
  "короткое-наименование": "Аргус-Томск",
  "полное-наименование": "ООО ЧОП «Аргус-Томск»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "argus_control",
  "короткое-наименование": "Аргус-Контроль",
  "полное-наименование": "ООО «Аргус-Контроль»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "argus_sever",
  "короткое-наименование": "Аргус-Север",
  "полное-наименование": "ООО ЧОП «Аргус-Север»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "beeline",
  "короткое-наименование": "Билайн",
  "полное-наименование": "Билайн",
  "userIdentifyField": "номер телефона",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "requisites": "default",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "processing": {
    "payment": "Apelsin",
    "verification": "none",
    "checkStatus": "none",
    "calculation": "none"
  },
  "schemas": [
    [
      {
        "name": "телефон"
      },
      {
        "name": "всего-к-оплате",
        "visible": false
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
            "name": "к-оплате"
          }
        ],
        "values": [
          {
            "наименование-услуги": "сотовая-связь"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "departamentohrany",
  "короткое-наименование": "departamentohrany",
  "полное-наименование": "departamentohrany",
  "userIdentifyField": "",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "domservicetdsk",
  "короткое-наименование": "ООО «Дом-Сервис ТДСК»",
  "полное-наименование": "ООО «Дом-Сервис ТДСК»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "erdivantvtomsk",
  "короткое-наименование": "ДОМ.RU, Кабельное ТВ",
  "полное-наименование": "ДОМ.RU, Кабельное ТВ, Томская обл.",
  "userIdentifyField": "номер договора",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "erdomrutomsk",
  "короткое-наименование": "ДОМ.RU, Телефония",
  "полное-наименование": "ДОМ.RU, Телефония, Томская обл.",
  "userIdentifyField": "номер договора",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ergorsvyaztomsk",
  "короткое-наименование": "ergorsvyaztomsk",
  "полное-наименование": "ergorsvyaztomsk",
  "userIdentifyField": "номер телефона",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "gazpromtomsk",
  "короткое-наименование": "Газпром межрегионгаз Новосибирск",
  "полное-наименование": "ОАО «Газпром межрегионгаз Новосибирск» Филиал в Томской области",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "govorova",
  "короткое-наименование": "ТСЖ «Говорова 26, 28»",
  "полное-наименование": "ТСЖ «Говорова 26, 28», ИНН 7017125850",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
      [
        {
            "name": "лицевой-счет"
        },
        {
            "name": "фамилия"
        },
        {
            "name": "имя"
        },
        {
            "name": "отчество"
        },
        {
            "name": "адрес"
        },
        {
            "name": "месяц"
        },
        {
            "name": "год"
        },
        {
            "name": "всего-к-оплате"
        },
        {
            "name": "услуги",
            "unique": ["наименование-услуги"],
            "columns": [
                {
                    "name": "наименование-услуги"
                },
                {
                    "name": "к-оплате"
                },
                {
                    "name": "группа",
                    "visible": false
                }
            ]
        }
      ]
  ]
}'),
('{
  "версия": 1,
  "name": "govorovakaprem",
  "короткое-наименование": "ТСЖ «Говорова 26, 28» Капремонт",
  "полное-наименование": "ТСЖ «Говорова 26, 28» Капремонт, ИНН 7017125850",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
      [
        {
            "name": "лицевой-счет"
        },
        {
            "name": "фамилия"
        },
        {
            "name": "имя"
        },
        {
            "name": "отчество"
        },
        {
            "name": "адрес"
        },
        {
            "name": "месяц"
        },
        {
            "name": "год"
        },
        {
            "name": "всего-к-оплате"
        },
        {
            "name": "услуги",
            "unique": ["наименование-услуги"],
            "columns": [
                {
                    "name": "наименование-услуги"
                },
                {
                    "name": "к-оплате"
                }
            ]
        }
      ]
  ]
}'),
('{
  "версия": 1,
  "name": "himik",
  "короткое-наименование": "ТСЖ «Химик»",
  "полное-наименование": "ТСЖ «Химик», ИНН 7017221360",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
      [
        {
            "name": "лицевой-счет"
        },
        {
            "name": "фамилия"
        },
        {
            "name": "имя"
        },
        {
            "name": "отчество"
        },
        {
            "name": "адрес"
        },
        {
            "name": "месяц"
        },
        {
            "name": "год"
        },
        {
            "name": "всего-к-оплате"
        },
        {
            "name": "услуги",
            "unique": ["наименование-услуги"],
            "columns": [
                {
                    "name": "наименование-услуги"
                },
                {
                    "name": "к-оплате"
                }
            ]
        }
      ]
  ]
}'),
('{
  "версия": 1,
  "name": "himikkaprem",
  "короткое-наименование": "ТСЖ «Химик» Капремонт",
  "полное-наименование": "ТСЖ «Химик» Капремонт, ИНН 7017221360",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фамилия"
      },
      {
        "name": "имя"
      },
      {
        "name": "отчество"
      },
      {
        "name": "адрес"
      },
      {
        "name": "месяц"
      },
      {
        "name": "год"
      },
      {
          "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
              "name": "к-оплате"
          }
        ],
        "values": [
          {
            "наименование": "Капитальный ремонт"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "iceberg",
  "короткое-наименование": "ООО ЧОП «Айсберг»",
  "полное-наименование": "ООО ЧОП «Айсберг», ИНН 7017049705",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "iceberg_servis",
  "короткое-наименование": "ООО ЧОП «АЙСБЕРГ-СЕРВИС»",
  "полное-наименование": "ООО ЧОП «АЙСБЕРГ-СЕРВИС», ИНН 7017189660",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jek30",
  "короткое-наименование": "jek30",
  "полное-наименование": "jek30",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jekzhilischnik",
  "короткое-наименование": "ООО «ЖЭК-ЖИЛИЩНИК»",
  "полное-наименование": "ООО «ЖЭК-ЖИЛИЩНИК», ИНН 7017355212",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jekzhilischnik710",
  "короткое-наименование": "ООО «ЖЭК-Жилищник»",
  "полное-наименование": "ООО «ЖЭК-Жилищник», ИНН 7017363710",
  "userIdentifyField": "клиент/лицевой-счет/одной-строкой",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true,
  "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фамилия"
      },
      {
        "name": "имя"
      },
      {
        "name": "отчество"
      },
      {
        "name": "адрес"
      },
      {
        "name": "месяц"
      },
      {
        "name": "год"
      },
      {
          "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
              "name": "группа",
              "visible": false
          },
          {
              "name": "к-оплате"
          }
        ],
        "values": [
          {
            "наименование": "Капитальный ремонт"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "jilservice047",
  "короткое-наименование": "ООО «Жилсервис «Ленинский»",
  "полное-наименование": "ООО «Жилсервис «Ленинский», ИНН 7017148047",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jilservice224",
  "короткое-наименование": "ООО «Жилсервис «Кировский»",
  "полное-наименование": "ООО «Жилсервис «Кировский», ИНН 7017161224",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jilservice472",
  "короткое-наименование": "ООО «Жилсервис «Черемошники»",
  "полное-наименование": "ООО «Жилсервис «Черемошники», ИНН 7017208472",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jilservice497",
  "короткое-наименование": "ООО «Компания Жилсервис»",
  "полное-наименование": "ООО «Компания Жилсервис», ИНН7017208497",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jilservice520",
  "короткое-наименование": "ООО «Жилсервис»",
  "полное-наименование": "ООО «Жилсервис», ИНН 7017070520",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "jilservice690",
  "короткое-наименование": "ООО «Компания Жилсервис «Каштак»",
  "полное-наименование": "ООО «Компания Жилсервис «Каштак», ИНН7017157690",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "kapremtomsk",
  "короткое-наименование": "РФКР МКД ТО",
  "полное-наименование": "«Региональный фонд капитального ремонта многоквартирных домов Томской области» (РФКР МКД ТО)",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фамилия"
      },
      {
        "name": "имя"
      },
      {
        "name": "отчество"
      },
      {
        "name": "адрес"
      },
      {
        "name": "месяц"
      },
      {
        "name": "год"
      },
      {
          "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
            "name": "к-оплате"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "krepost",
  "короткое-наименование": "Сервисный центр «Крепость»",
  "полное-наименование": "Сервисный центр «Крепость»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "mkstomsk",
  "короткое-наименование": "ЗАО «МКС-Томск»",
  "полное-наименование": "ЗАО «МКС-Томск», ИНН 7017276320",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фамилия"
      },
      {
        "name": "имя"
      },
      {
        "name": "отчество"
      },
      {
        "name": "адрес"
      },
      {
        "name": "месяц"
      },
      {
        "name": "год"
      },
      {
        "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
            "name": "группа",
            "visible": false
          },
          {
            "name": "к-оплате"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "modelnet",
  "короткое-наименование": "Model.Net",
  "полное-наименование": "Model.Net (ООО «Модэл»)",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "mvdtomsk",
  "короткое-наименование": "«ОВО по г. Томску – филиал ФГКУ УВО УМВД России по Томской области»",
  "полное-наименование": "«ОВО по г. Томску – филиал ФГКУ УВО УМВД России по Томской области»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фамилия"
      },
      {
        "name": "имя"
      },
      {
        "name": "отчество"
      },
      {
        "name": "адрес"
      },
      {
        "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
              "name": "группа",
              "visible": false
          },
          {
              "name": "к-оплате"
          }
        ],
        "values": [
          {
            "наименование": "Начисление за охрану"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "newtelesystem",
  "короткое-наименование": "ООО «Новые Телесистемы-ТВ»",
  "полное-наименование": "ООО «Новые Телесистемы-ТВ»",
  "userIdentifyField": "номер договора",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "oootomica",
  "короткое-наименование": "ООО «Томика»",
  "полное-наименование": "ООО «Томика»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "patriott",
  "короткое-наименование": "ООО ЧОП «Патриот-Т»",
  "полное-наименование": "ООО ЧОП «Патриот-Т»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "pravoporjadok",
  "короткое-наименование": "ООО ЧОО «ПРАВОПОРЯДОК»",
  "полное-наименование": "ООО ЧОО «ПРАВОПОРЯДОК»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "pravoporjadokservice",
  "короткое-наименование": "ООО «Правопорядок-сервис»",
  "полное-наименование": "ООО «Правопорядок-сервис»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "pravoporjadokt",
  "короткое-наименование": "ООО ЧОО «ПРАВОПОРЯДОК-Т»",
  "полное-наименование": "ООО ЧОО «ПРАВОПОРЯДОК-Т»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "rctomsk",
  "название-услуги": "жилищно-коммунальное обслуживание",
  "короткое-наименование": "ООО «Томский Расчетный Центр»",
  "полное-наименование": "ООО «Томский Расчетный Центр»",
  "userIdentifyField": "клиент/лицевой-счет/одной-строкой",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "startDayMeterReading": 15,
  "endDayMeterReading": 31,
  "requisites": "default",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "firstStep": ["лицевой-счет"],
  "schemas": [
      [
        {
            "name": "лицевой-счет",
            "constraints": []
        },
        {
            "name": "фамилия",
            "constraints": [
                {
                    "constraint": "обязательное && русский",
                    "error": "bcd7582b-b18b-433f-9342-6c80971a782b"
                }
            ]
        },
        {
            "name": "имя",
            "constraints": [
                {
                    "constraint": "обязательное && русский",
                    "error": "99c445f6-437e-49b4-8aae-5bf372e11abf"
                }
            ]
        },
        {
            "name": "отчество",
            "constraints": [
                {
                    "constraint": "обязательное && русский",
                    "error": "37ac3a83-8914-4256-b937-f0f87c55e565"
                }
            ]
        },
        {
            "name": "адрес"
        },
        {
            "name": "месяц",
            "constraints": []
        },
        {
            "name": "год",
            "constraints": []
        },
        {
            "name": "всего-к-оплате"
        },
        {
            "name": "услуги",
            "unique": ["наименование-услуги"],
            "columns": [
                {
                    "name": "наименование-услуги"
                },
                {
                    "name": "к-оплате"
                }
            ],
            "values": [
                {
                    "наименование-услуги": "Водоснабжение и водоотведение",
                    "к-оплате": ""
                }
            ]
        },
        {
            "name": "приборы-учета",
            "unique": ["номер-прибора-учета"],
            "columns": [
                {
                    "name": "тип-прибора-учета"
                },
                {
                    "name": "группа",
                    "visible": false
                },
                {
                    "name": "единица-измерения",
                    "visible": false
                },
                {
                    "name": "номер-прибора-учета"
                },
                {
                    "name": "наименование-услуги",
                    "visible": false
                },
                {
                    "name": "предыдущие-показания"
                },
                {
                    "name": "текущие-показания"
                }
            ],
            "values": [
                {
                    "группа": "1",
                    "тип-прибора-учета": "Холодная вода",
                    "единица-измерения": "м3",
                    "наименование-услуги": "водоснабжение и водоотведение"
                },
                {
                    "группа": "1",
                    "тип-прибора-учета": "Холодная вода",
                    "единица-измерения": "м3",
                    "наименование-услуги": "водоснабжение и водоотведение"
                },
                {
                    "группа": "1",
                    "тип-прибора-учета": "Горячая вода",
                    "единица-измерения": "м3",
                    "наименование-услуги": "водоснабжение и водоотведение"
                },
                {
                    "группа": "1",
                    "тип-прибора-учета": "Горячая вода",
                    "единица-измерения": "м3",
                    "наименование-услуги": "водоснабжение и водоотведение"
                }
            ]
        }
      ],
      [
        {
            "name": "type",
            "value": "meters"
        },
        {
            "name": "лицевой-счет",
            "constraints": []
        },
        {
            "name": "приборы-учета",
            "unique": ["номер-прибора-учета"],
            "columns": [
                {
                    "name": "тип-прибора-учета"
                },
                {
                    "name": "группа",
                    "visible": false
                },
                {
                    "name": "номер-прибора-учета"
                },
                {
                    "name": "предыдущие-показания"
                },
                {
                    "name": "текущие-показания"
                }
            ],
            "values": [
                {
                    "группа": "1",
                    "тип-прибора-учета": "Холодная вода"
                },
                {
                    "группа": "1",
                    "тип-прибора-учета": "Холодная вода"
                },
                {
                    "группа": "1",
                    "тип-прибора-учета": "Горячая вода"
                },
                {
                    "группа": "1",
                    "тип-прибора-учета": "Горячая вода"
                }
            ]
        }
      ]
  ]
}'),
('{
  "версия": 1,
  "name": "rt_tomsk",
  "короткое-наименование": "rt_tomsk",
  "полное-наименование": "rt_tomsk",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "rt_tomsk_acc",
  "короткое-наименование": "rt_tomsk_acc",
  "полное-наименование": "rt_tomsk_acc",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "sibtelesystem",
  "короткое-наименование": "ООО «Сибирские Телесистемы»",
  "полное-наименование": "ООО «Сибирские Телесистемы»",
  "userIdentifyField": "номер договора",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "skala",
  "короткое-наименование": "ООО ЧОП «Скала-Техно»",
  "полное-наименование": "ООО ЧОП «Скала-Техно», ИНН 7017114633",
  "userIdentifyField": "номер договора",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tdskuyut",
  "короткое-наименование": "ЗАО «Уют ТДСК»",
  "полное-наименование": "ЗАО «Уют ТДСК», ИНН 7017004711",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tgk11tomsk",
  "короткое-наименование": "АО «ТомскРТС»",
  "полное-наименование": "АО «ТомскРТС» (Томский филиал АО «ТГК-11»)",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "firstStep": ["штрихкод"],
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фио"
      },
      {
        "name": "адрес"
      },
      {
        "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги", "группа"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
            "name": "группа",
            "visible": false
          },
          {
            "name": "к-оплате"
          }
        ]
      },
        {
            "name": "приборы-учета",
            "unique": ["порядковый-номер-прибора-учета", "группа"],
            "columns": [
                {
                    "name": "тариф"
                },
                {
                    "name": "группа",
                    "visible": false
                },
                {
                    "name": "расход"
                },
                {
                    "name": "порядковый-номер-прибора-учета"
                },
                {
                    "name": "предыдущие-показания"
                },
                {
                    "name": "текущие-показания"
                }
            ]
        }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "TochkaSpa",
  "короткое-наименование": "ООО «ТОЧКА СПА»",
  "полное-наименование": "ООО «ТОЧКА СПА»",
  "userIdentifyField": "артикул",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tomica",
  "короткое-наименование": "Томика",
  "полное-наименование": "Томика",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tomtel",
  "короткое-наименование": "tomtel",
  "полное-наименование": "tomtel",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tomtelinet",
  "короткое-наименование": "tomtelinet",
  "полное-наименование": "tomtelinet",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tomteltv",
  "короткое-наименование": "tomteltv",
  "полное-наименование": "tomteltv",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tsjaruna",
  "короткое-наименование": "tsjaruna",
  "полное-наименование": "tsjaruna",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фамилия"
      },
      {
        "name": "имя"
      },
      {
        "name": "отчество"
      },
      {
        "name": "адрес"
      },
      {
        "name": "месяц"
      },
      {
        "name": "год"
      },
      {
        "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
            "name": "группа",
            "visible": false
          },
          {
            "name": "к-оплате"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "tsjluxemburg",
  "короткое-наименование": "ТСЖ «Люксембург»",
  "полное-наименование": "ТСЖ «Люксембург» , ИНН 7017233415",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tsjubileynoe",
  "короткое-наименование": "ТСЖ «Юбилейное», г.Томск",
  "полное-наименование": "ТСЖ «Юбилейное», г.Томск, ИНН 7017293661",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true,
  "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "tsjvenera",
  "короткое-наименование": "ТСЖ «Венера»",
  "полное-наименование": "ТСЖ «Венера», ИНН 7017147251",
  "userIdentifyField": "",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukcentralnaya080",
  "короткое-наименование": "УК Центральная",
  "полное-наименование": "УК Центральная",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukcentralnaya217",
  "короткое-наименование": "УК Центральная",
  "полное-наименование": "УК Центральная",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukcentralnaya470",
  "короткое-наименование": "УК Центральная",
  "полное-наименование": "УК Центральная",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukgromada",
  "короткое-наименование": "ООО «УК «Громада»",
  "полное-наименование": "ООО «УК «Громада», ИНН 7017157242",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukistochnoye",
  "короткое-наименование": "ООО «Управляющая компания «Источное»",
  "полное-наименование": "ООО «Управляющая компания «Источное», ИНН 7017120796",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukjilremservis1",
  "короткое-наименование": "УК «Жилремсервис-I»",
  "полное-наименование": "УК «Жилремсервис-I»",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukkirovskij_massiv",
  "короткое-наименование": "ООО «УК «Кировс​кий Массив»",
  "полное-наименование": "ООО «УК «Кировс​кий Массив»",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukmayak",
  "короткое-наименование": "ООО УК «Маякъ»",
  "полное-наименование": "ООО УК «Маякъ»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukmoydomtomsk",
  "короткое-наименование": "ООО УК «Мой дом»",
  "полное-наименование": "ООО УК «Мой дом», ИНН 7017177224",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "uknashdom",
  "короткое-наименование": "ООО Управляющая компания «Наш дом»",
  "полное-наименование": "ООО Управляющая компания «Наш дом»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "uknewage",
  "короткое-наименование": "УК Новый Век",
  "полное-наименование": "УК Новый Век",
  "userIdentifyField": "штрихкод",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "schemas": [
    [
      {
        "name": "лицевой-счет"
      },
      {
        "name": "фамилия"
      },
      {
        "name": "имя"
      },
      {
        "name": "отчество"
      },
      {
        "name": "адрес"
      },
      {
        "name": "месяц"
      },
      {
        "name": "год"
      },
      {
        "name": "всего-к-оплате"
      },
      {
        "name": "услуги",
        "unique": ["наименование-услуги"],
        "columns": [
          {
            "name": "наименование-услуги"
          },
          {
            "name": "к-оплате"
          }
        ]
      }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "ukremstrojbyt",
  "короткое-наименование": "ООО «УК Ремстройбыт»",
  "полное-наименование": "ООО «УК Ремстройбыт», ИНН 7017143271",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "uksoyuz",
  "короткое-наименование": "ООО «УК СОЮЗ»",
  "полное-наименование": "ООО «УК СОЮЗ»",
  "userIdentifyField": "штрихкод квитанции",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukstrojsojuz",
  "короткое-наименование": "ООО «УК СТРОЙСОЮЗ»",
  "полное-наименование": "ООО «УК СТРОЙСОЮЗ»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "ukzhilfond",
  "короткое-наименование": "ООО «УК «ЖилФонд»",
  "полное-наименование": "ООО «УК «ЖилФонд», ИНН 7017110830",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "vozminet",
  "короткое-наименование": "Vozmi.Net",
  "полное-наименование": "Vozmi.Net (ООО «Модэл»)",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "enabled": true,
  "logo": "https://vp.ru/pay/resources/media/images/provider/logo/vozminet.png",
  "description": "Vozmi.Net (ООО «Модэл»)",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "zhilremservice",
  "короткое-наименование": "ООО «Жилремсервис»",
  "полное-наименование": "ООО «Жилремсервис»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  }
}'),
('{
  "версия": 1,
  "name": "levobereje",
  "короткое-наименование": "УК Левобережье»",
  "полное-наименование": "УК Левобережье»",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": true,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["tomskaja_obl"],
  "region": ["Томская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "processing": {
    "calculation": "Apelsin",
    "payment": "Apelsin",
    "checkStatus": "Apelsin"
  },
  "firstStep": ["лицевой-счет", "год", "месяц"],
  "schemas": [
    [
        {
          "name": "лицевой-счет"
        },
        {
          "name": "плательщик"
        },
        {
          "name": "адрес-одной-строкой"
        },
        {
          "name": "всего-к-оплате"
        },
        {
          "name": "год"
        },
        {
          "name": "месяц"
        },
        {
          "name": "услуги",
          "columns": [
            {
              "name": "наименование-услуги"
            },
            {
              "name": "к-оплате"
            }
          ],
          "values": [
            {
              "наименование-услуги": "Содержание и ремонт жилья",
              "к-оплате": 1463.04
            },
            {
              "наименование-услуги": "Домофон",
              "к-оплате": 40.00
            },
            {
              "наименование-услуги": "Дополнительная услуга"
            },
            {
              "наименование-услуги": "Антенна",
              "к-оплате": 65.00
            },
            {
              "наименование-услуги": "Пени за сод. и ремонт жилья"
            },
            {
              "наименование-услуги": "Изготовление и доставка квитанций на кап. ремонт"
            }
          ]
        }
    ]
  ]
}'),
('{
  "версия": 1,
  "name": "jkumoskvaepd",
  "короткое-наименование": "ЖКУ г. Москва (ЕПД)",
  "полное-наименование": "ЖКУ г. Москва (ЕПД)",
  "userIdentifyField": "лицевой счёт",
  "startDateTime": "2016-01-20 00:00:00",
  "isActive": true, "type": "standard",
  "юридические-лица": [],
  "federal": false,
  "participantsCount": 2,
  "servicesCount": 2,
  "месячный-лимит": 250000.00,
  "дневной-лимит": 25000.00,
  "категория": ["Коммунальные службы"],
  "описание": "описание в несколько строк поставщика услуг",
  "visible": true,
  "regions": ["moskovskaja_obl"],
  "region": ["Московская область"],
  "logo": "/resources/media/images/provider/logo/default/uk.png",
  "firstStep": ["лицевой-счет", "год", "месяц"],
  "комиссия": {
    "fix": 2,
    "percent_from_provider": 3,
    "percent": 1,
    "начало": "2016-01-20 00:00:00",
    "комиссия": true,
    "завершение": "2017-01-20 00:00:00"
  },
  "processing": {
    "calculation": "Rapida",
    "payment": "Rapida",
    "verification": "Rapida"
  },
  "schemas": [
    [
        {
          "name": "лицевой-счет"
        },
        {
          "name": "фамилия"
        },
        {
          "name": "имя"
        },
        {
          "name": "отчество"
        },
        {
          "name": "улица"
        },
        {
          "name": "дом"
        },
        {
          "name": "корпус"
        },
        {
          "name": "квартира"
        },
        {
          "name": "всего-к-оплате"
        },
        {
          "name": "год"
        },
        {
          "name": "месяц"
        },
        {
          "name": "услуги",
          "columns": [
            {
              "name": "наименование-услуги"
            },
            {
              "name": "к-оплате"
            }
          ],
          "values": [
          ]
        }
    ]
  ]
}');