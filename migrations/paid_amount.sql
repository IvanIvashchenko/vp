
DROP TABLE IF EXISTS "paid_amount";

CREATE TABLE IF NOT EXISTS paid_amount(
  id BIGSERIAL PRIMARY KEY,
  document jsonb NOT NULL
);