DROP TABLE paymentMethodMatrixCollection;

CREATE TABLE paymentMethodMatrixCollection(

id BIGINT,

document JSONB NOT NULL

);

INSERT INTO paymentMethodMatrixCollection (id, document) VALUES (1, '{

"type": "creditCard","startDateTime": "2016-08-30 10:56:26", "isActive": true,

"kinds": [

{

"name": "standard",

"description": "НК",

"gateways": [

{

"gwType":"vsep",

"terminalType":"standard",

"activity": true

}

]

},

{

"name": "communal",

"description": "К",

"gateways": [

{

"gwType":"vsep",

"terminalType":"standard",

"activity": false

},

{

"gwType":"psb",

"terminalType":"communal",

"activity": true

}

]

},

{

"name": "mixed",

"description": "М",

"gateways": [

{

"gwType":"vsep",

"terminalType":"communalno3ds",

"activity": true

}

]

}

]

}');

INSERT INTO paymentMethodMatrixCollection (id, document) VALUES (2, '

{

"type": "VISA","startDateTime": "2016-08-30 10:56:26", "isActive": true,

"kinds": [

{

"name": "standard",

"description": "НК",

"gateways": [

{

"gwType":"vsep",

"terminalType":"standard",

"activity": true

}

]

},

{

"name": "communal",

"description": "К",

"gateways": [

{

"gwType":"vsep",

"terminalType":"communal",

"activity": true

},

{

"gwType":"psb",

"terminalType":"communal",

"activity": false

}

]

},

{

"name": "mixed",

"description": "М",

"gateways": [

{

"gwType":"vsep",

"terminalType":"communalno3ds",

"activity": true

}

]

}

]

}');

INSERT INTO paymentMethodMatrixCollection (id, document) VALUES (3, '

{

"type": "VISA_ELECTRON","startDateTime": "2016-08-30 10:56:26", "isActive": true,

"kinds": [

{

"name": "standard",

"description": "НК",

"gateways": [

{

"gwType":"vsep",

"terminalType":"standard",

"activity": true

}

]

},

{

"name": "communal",

"description": "К",

"gateways": [

{

"gwType":"vsep",

"terminalType":"communal",

"activity": true

},

{

"gwType":"psb",

"terminalType":"communal",

"activity": false

}

]

},

{

"name": "mixed",

"description": "М",

"gateways": [

{

"gwType":"vsep",

"terminalType":"communalno3ds",

"activity": true

}

]

}

]

}');

DROP TABLE gwPropertiesCollection;

CREATE TABLE gwPropertiesCollection(

id BIGINT NOT NULL,

document JSONB NOT NULL

);

INSERT INTO gwPropertiesCollection (id, document) VALUES (1, '{
"gwType": "vsep",
"startDateTime": "2016-08-30 10:56:26",
"isActive": true,
"paymentUrl": "https://testgate.vseplatezhi.ru/main",
"checkUrl": "https://testgate.vseplatezhi.ru/order/status",
"sign": {
  "signAlgorithmType": "hmac",
  "signAlgorithmName": "HmacSHA1",
  "encoding":"UTF-8",
  "key":"ba7ab96ce78404cc4dc8775e354ceb563d69717c"
},
"merchants": [
  {
    "identifier": "standard",
    "code": "3333"
  },
  {
    "identifier": "communal",
    "code": "3333"
  },
  {
    "identifier": "communalno3ds",
    "code": "3333"
  }
],
  "terminals": [
  {
    "identifier": "standard",
    "code": "3000"
  },
  {
    "identifier": "communal",
    "code": "3000"
  },
  {
    "identifier": "communalno3ds",
    "code": "3000"
  }
]
}');

INSERT INTO gwPropertiesCollection (id, document) VALUES (2, '{

"gwType": "psb","startDateTime": "2016-08-30 10:56:26", "isActive": true,

"paymentUrl": "http://193.200.10.117:8080/cgi-bin/cgi_link",

"checkUrl": "https://rs.psbank.ru:4443/cgi-bin/ecomm_check_test",

"key": "C50E41160302E0F5D6D59F1AA3925C45",

"merchants": [

{

"identifier": "standard",

"code": "790367686219999"

},

{

"identifier": "communal",

"code": "790367686219999"

},

{

"identifier": "communalno3ds",

"code": "790367686219999"

}

],

"terminals": [

{

"identifier": "standard",

"code": "79036804"

},

{

"identifier": "communal",

"code": "79036804"

},

{

"identifier": "communalno3ds",

"code": "79036804"

}

]

}');