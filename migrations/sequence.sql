DROP TABLE IF EXISTS "sequence";
CREATE TABLE IF NOT EXISTS "sequence" (
  "document" jsonb DEFAULT NULL
) WITH (
OIDS=FALSE
);