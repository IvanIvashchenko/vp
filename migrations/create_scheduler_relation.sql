--
-- Table structure for table "scheduler"
--
DROP TABLE IF EXISTS "scheduler";
CREATE TABLE IF NOT EXISTS "scheduler" (
  "document" jsonb DEFAULT NULL
) WITH (
OIDS=FALSE
);