package com.perspective.vpng.actor.exception_handler;

import com.perspective.vpng.actor.exception_handler.wrapper.ExceptionHandlerMessage;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.SerializeException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(IOC.class)
public class ExceptionHandlerActorTest {

    private ExceptionHandlerActor actor;
    private ExceptionHandlerMessage message;

    private String statusString = "statusString";

    @Before
    public void setUp() throws Exception {

        actor = new ExceptionHandlerActor();
    }

    @Test
    public void Should_CorrectlySetResponseMessage() throws SerializeException, ReadValueException, ChangeValueException, ResolutionException {
        message = mock(ExceptionHandlerMessage.class);
        IObject formFromMessage = mock(IObject.class);
        when(message.getForm()).thenReturn(formFromMessage);
        when(message.getStatus()).thenReturn(statusString);
        PowerMockito.mockStatic(IOC.class);

        actor.handleException(message);

        verify(message).setForm(formFromMessage);
        verify(message).setStatus(statusString);
    }

    @Test(expected = RuntimeException.class)
    public void Should_ThrowRuntimeException_WhenInternalExceptionIsThrown() throws ReadValueException {
        message = mock(ExceptionHandlerMessage.class);
        when(message.getForm()).thenThrow(new ReadValueException());

        actor.handleException(message);

        fail();
    }
}