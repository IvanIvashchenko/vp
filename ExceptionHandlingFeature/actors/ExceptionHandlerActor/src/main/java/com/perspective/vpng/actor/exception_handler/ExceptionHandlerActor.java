package com.perspective.vpng.actor.exception_handler;

import com.perspective.vpng.actor.exception_handler.wrapper.ExceptionHandlerMessage;

/**
 * This is actor that handles thrown exceptions
 */
public class ExceptionHandlerActor {

    /**
     * Default constructor for {@link ExceptionHandlerActor}
     */
    public ExceptionHandlerActor() {}

    /**
     *
     * @param wrapper
     */
    public void handleException(final ExceptionHandlerMessage wrapper) {

        try {
            String statusStr = wrapper.getStatus();
            wrapper.setForm(wrapper.getForm());
            wrapper.setStatus(statusStr);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
