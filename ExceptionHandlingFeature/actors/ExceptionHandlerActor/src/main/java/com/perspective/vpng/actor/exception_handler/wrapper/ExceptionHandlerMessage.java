package com.perspective.vpng.actor.exception_handler.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for input message by {@link com.perspective.vpng.actor.exception_handler.ExceptionHandlerActor}
 */
public interface ExceptionHandlerMessage {

    IObject getForm() throws ReadValueException;
    String getStatus() throws ReadValueException;
    void setForm(IObject form) throws ChangeValueException;
    void setStatus(String status) throws ChangeValueException;
}
