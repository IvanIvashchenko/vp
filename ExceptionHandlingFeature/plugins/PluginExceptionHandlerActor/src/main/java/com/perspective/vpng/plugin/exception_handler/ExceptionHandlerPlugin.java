package com.perspective.vpng.plugin.exception_handler;

import com.perspective.vpng.actor.exception_handler.ExceptionHandlerActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for {@link ExceptionHandlerActor}
 */
//TODO:: add tests
public class ExceptionHandlerPlugin implements IPlugin {


    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor.
     *
     * @param bootstrap bootstrap element.
     */
    public ExceptionHandlerPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link com.perspective.vpng.actor.exception_handler.ExceptionHandlerActor} actor and registers in <cdoe>IOC</cdoe> with key <code>ExceptionHandlerActor</code>.
     * Begin loading plugin after loading plugins: <code>IOC</code> and before: <code>starter</code>.
     *
     * @throws PluginException when errors in loading plugin.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("ExceptionHandlerPlugin");

            item
//                    .after("IOC")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IOC.register(Keys.getOrAdd(ExceptionHandlerActor.class.getCanonicalName()), new ApplyFunctionToArgumentsStrategy(
                                            (args) -> {
                                                try {
                                                    return new ExceptionHandlerActor();
                                                } catch (Exception e) {
                                                    throw new RuntimeException(
                                                            "Can't create exceptionHandlerActor: " + e.getMessage(), e);
                                                }
                                            }
                                    )
                            );
                        } catch (ResolutionException | InvalidArgumentException | RegistrationException e) {
                            throw new ActionExecuteException("ExceptionHandler plugin can't load: " + e.getMessage(), e);
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
