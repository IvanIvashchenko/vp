package com.perspective.vpng.plugin.exception_handler;

import com.perspective.vpng.actor.exception_handler.ExceptionHandlerActor;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.base.interfaces.iaction.IPoorAction;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;

@PrepareForTest({IOC.class, Keys.class, IPoorAction.class, ApplyFunctionToArgumentsStrategy.class, ExceptionHandlerPlugin.class})
@RunWith(PowerMockRunner.class)
public class ExceptionHandlerPluginTest {
    private ExceptionHandlerPlugin targetPlugin;
    private IBootstrap bootstrap;

    @Before
    public void before() {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        bootstrap = mock((IBootstrap.class));

        targetPlugin = new ExceptionHandlerPlugin(bootstrap);
    }

    @Test
    public void Should_CorrectLoadPlugin() throws Exception {
        BootstrapItem item = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("ExceptionHandlerPlugin").thenReturn(item);
        when(item.after("IOC")).thenReturn(item);
        when(item.before("starter")).thenReturn(item);

        targetPlugin.load();

        verifyNew(BootstrapItem.class).withArguments("ExceptionHandlerPlugin");
//        verify(item).after("IOC");
//        verify(item).before("starter");
        verify(bootstrap).add(item);

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);
        verify(item).process(actionArgumentCaptor.capture());

        IKey actorKey = mock(IKey.class);
        when(Keys.getOrAdd(ExceptionHandlerActor.class.getCanonicalName())).thenReturn(actorKey);

        ArgumentCaptor<ApplyFunctionToArgumentsStrategy> applyFunctionToArgumentsStrategyArgumentCaptor =
                ArgumentCaptor.forClass(ApplyFunctionToArgumentsStrategy.class);
        actionArgumentCaptor.getValue().execute();

        verifyStatic();
        IOC.register(eq(actorKey), applyFunctionToArgumentsStrategyArgumentCaptor.capture());

        ExceptionHandlerActor actor = PowerMockito.mock(ExceptionHandlerActor.class);
        whenNew(ExceptionHandlerActor.class).withAnyArguments().thenReturn(actor);
    }

    @Test(expected = PluginException.class)
    public void Should_ThrowException_When_() throws Exception {
        whenNew(BootstrapItem.class).withArguments("ExceptionHandlerPlugin").thenThrow(new InvalidArgumentException(""));

        targetPlugin.load();

        verifyNew(BootstrapItem.class).withArguments("ExceptionHandlerPlugin");
    }
}