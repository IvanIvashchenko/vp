Names of packages are written through '-', without "-actor" or "-plugin", pls
Use "actor." or "plugin.". NOT "actors." or "plugins.".
Example:
###WRONG: <artifactId>actors.actor_for_create_anything_actor</artifactId>
###CORRECT: <artifactId>actor.create-anything</artifactId>

Как пользоваться сервером:
1. Архив сервера можно найти в репозитории акторной библиотеки в ветке develop:
https://github.com/SmartTools/smartactors-core/tree/develop/DeveloperTools/Servers/Server
2. Скачиваем, распаковываем. В изначальном состоянии сервер пустой и не рабочий.
3. Для начала необходимо скачать ядро сервера (минимальный набор core features без который сервер не будет работать)
Core-pack доступен в репозитории фич archiva:
http://archiva.smart-tools.info/#browse~smartactors-features/info.smart_tools.smartactors
4. Скачиваем архив core-pack.zip (выбираем последнюю версию)
http://archiva.smart-tools.info/#artifact-details-download-content~smartactors-features/info.smart_tools.smartactors/core-pack/0.2.0-SNAPSHOT
5. Архив распаковываем в папку core сервера.
Core-pack содержит следующий набор фич:
https://github.com/SmartTools/smartactors-core/blob/develop/DeveloperTools/core-list.txt
После этого сервер можно будет запустить командой

java -jar server.jar
6. Теперь надо добавить необходимые для работы дополнительные фичи.
Взять их можно в том же репозитории фич (к примеру могут понадобиться Http-endpoint, MessageBus и др).
7. Выкачиваем архивы необходимых фич и распаковываем их в директорию corefeatures нашего сервера.
После распаковки в директории corefeatures появявтся папки с фичами, каждая папка содержит набор jar файлов и config.json
Внимание! Выбранная фича может зависеть от других, по-этому также необходимо скачать и распаковать в директорию corefeature фичи от которых зависит выбранная.
Посмотреть зависимость фичи от других можно в файле config.json - поле "afterFeatures".
Также для работы многих фич необходимы plugin-packs (можно узнать о их наличии в репозитории фич, они имеют такое же название как и фича + постфикс "-plugins")
8. Далее по необходимости надо в файлах config.json добавить неоходимые секции (objects, maps, endpoints, messageBus, tests)После 1-8 сервер будет иметь необходимый ф-ционал.
Пример сервера с core-pack и http-enpoint можно скачать по следующей ссылке:
https://github.com/SmartTools/smartactors-core/tree/develop/DeveloperTools/Servers/ServerWithCoreAndEndpointДалее о порядке разработки пользовательских фич (как это вижу я):
1. Создаем мавен проект (можно использовать pom.xml, bin.xml и config.json по ссылке: https://github.com/SmartTools/smartactors-core/tree/develop/DeveloperTools/ForFeatureCreation
- позволяют командой mvn clean assembly:assembly создавать zip архив из скомпилированных модулей нашего фича-проекта). Если
2. Каждый модуль данного проекта - это актор или плагин к актору объединенных каким-то общим бизнес процессом. В акторах и плагинах можно использовать необходимый ф-ционал акторной
библиотеки - IObject, IOC, MessageBus и т.д. Для этого в pom-файле к данному модулю надо указать dependency из репозитория модулей библиотеки:
http://archiva.smart-tools.info/#browse~smartactors-modules/info.smart_tools.smartactors
3. После разработки акторов и плагинов фичи надо заполнить при необходимости config.json - указать создание необходимых объектов, цепочек, тестов и т.д.
4. После сборки проекта получаем zip файл - фича. Поступаем с данным файлом так же как и с corefeatures, только пользовательские фичи надо размещать в директории feature нашего сервера.Неприятности (для разработчиков ядра):
1. Я не успел протестировать полную работоспособность сервера, так что возможны некоторые проблемы.
2. Некоторые plugin-pack для фич содержат плагины с зависимостями за пределами фичи - такие зависимости надо удалить, и пересобрать фичи.