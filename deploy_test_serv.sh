#!/bin/bash
echo "Pack vp sources and send to server..."
cd Server/
tar --exclude=.git --exclude=.idea -zcvf ../vpng.tgz .
scp ../vpng.tgz hwdtech@www2-test.vseplatezhi.ru:/home/hwdtech/
rm ../vpng.tgz
echo "Go to remote server"
ssh -t hwdtech@www2-test.vseplatezhi.ru '
    echo "Ok we here";
    rm vp/* -rf;
    tar -xzvf vpng.tgz -C vp;
    cd vp/
    echo "Stop server..."
    sudo kill -9 $(sudo netstat -lnap | grep 9909 | awk '\''{print $7}'\'' | sed "s/\/.*//")
    echo "Start server...";
    nohup /opt/java/jdk1.8.0_101/bin/java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 -jar server.jar 1>system_output.txt 2>exception_output.txt &
    sleep 10
'
