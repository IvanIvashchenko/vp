package com.perspective.vpng.strategy.moneta_processing_request.verification;

import com.perspective.vpng.strategy.moneta_processing_request.IMonetaReqConfig;
import com.perspective.vpng.strategy.moneta_processing_request.MonetaReqExtraParamStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

public class MonetaVerifyReqContentStrategy implements IResolveDependencyStrategy {

    private final IResolveDependencyStrategy constParamStrategy;
    private final IResolveDependencyStrategy extraParamStrategy;

    private final IField providerIdF;
    private final IField nameF;

    private final String collectionName;
    private final IPool connectionPool;

    public MonetaVerifyReqContentStrategy(IMonetaReqConfig config) throws ResolutionException {

        this.collectionName = config.getCollectionName();
        Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
        this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);

        this.constParamStrategy = new MonetaVerifyReqConstParamStrategy(config);
        this.extraParamStrategy = new MonetaReqExtraParamStrategy();

        this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
        this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {

        Set<IObject> result = new HashSet<>();
        try {
            List<IObject> invoices = (List) args[0];
            List<String> providerIds = getProviderIds(invoices);
            for (IObject invoice : invoices) {
                IObject requestObj = constParamStrategy.resolve();
                List<IObject> processingParams = getProcessingParams(providerIds);
                Map<String, IObject> processingMap = associate(providerIds, processingParams);
                requestObj = extraParamStrategy.resolve(
                    invoice,
                    requestObj,
                    processingMap
                );
                result.add(requestObj);
            }

            return (T) result;
        } catch (InvalidArgumentException | ReadValueException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Error during cast to invoices list", ex);
        }
    }

    private List<String> getProviderIds(final List<IObject> invoices)
        throws ReadValueException, InvalidArgumentException {

        List<String> providerIds = new ArrayList<>(invoices.size());
        for (IObject invoice: invoices) {
            String providerId = providerIdF.in(invoice);
            if (!providerIds.contains(providerId)) {
                providerIds.add(providerId);
            }
        }

        return providerIds;
    }


    private List<IObject> getProcessingParams(final List<String> providerIds) throws ResolveDependencyStrategyException {
        List<IObject> params = new ArrayList<>();
        try (IPoolGuard guard = new PoolGuard(connectionPool)) {
            ITask searchTask = IOC.resolve(
                Keys.getOrAdd("db.collection.search"),
                guard.getObject(),
                collectionName,
                prepareSearchQuery(providerIds),
                (IAction<IObject[]>) foundDocs -> {
                    try {
                        params.addAll(Arrays.asList(foundDocs));
                    } catch (Exception e) {
                        throw new ActionExecuteException(e);
                    }
                }
            );
            searchTask.execute();

            return params;
        } catch (ResolutionException | TaskExecutionException | PoolGuardException e) {
            throw new ResolveDependencyStrategyException("Can't get Moneta processing parameters from database", e);
        }
    }

    private IObject prepareSearchQuery(final List<String> keys) throws ResolutionException {
        String queryKeys = keys.stream().map(k -> "\"moneta_".concat(k.concat("\""))).collect(Collectors.toList()).toString();

        return IOC.resolve(
            Keys.getOrAdd(IObject.class.getCanonicalName()),
            String.format(
                "{" +
                    "\"filter\": {" +
                        "\"name\": {\"$in\": %s}" +
                    "}," +
                    "\"page\": {\"size\": %s, \"number\": 1}" +
                "}",
                queryKeys,
                keys.size()
            )
        );
    }

    private Map<String, IObject> associate(final List<String> providerIds, final List<IObject> params)
        throws ReadValueException, InvalidArgumentException {

        Map<String, IObject> container = new HashMap<>(providerIds.size());
        String providerProcessingName;
        for (String providerId: providerIds) {
            for (IObject param: params) {
                providerProcessingName = nameF.in(param);
                if (providerProcessingName.contains(providerId)) {
                    container.put(providerId, param);
                    break;
                }
            }
        }

        return container;
    }

    public static void main(String[] args) throws Exception {

//        URL url = new URL("http://www.google.com/");
//        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
//
//        String inputLine;
//        while ((inputLine = in.readLine()) != null)
//            System.out.println(inputLine);
//        in.close();
//        Document doc = Jsoup.connect("http://www.google.com").get();
//        Elements elements = doc.select("form");
//        Iterator<Element> itr = doc.select("form").iterator();
//        while(itr.hasNext()) {
//            Element element = itr.next();
//            System.out.println(element.html());
//        }


//        ObjectMapper mapper = new ObjectMapper();
//        String obj = new String("skjdfjksfgj");
//        String jsonString = mapper.writeValueAsString(obj);
//
//        String obj1 = mapper.readValue(jsonString, String.class);
//        System.out.println(obj1);

        System.out.println("Prepping...");

        Race r = new Race(
            "Beverly Takes a Bath",
            "RockerHorse",
            "Phineas",
            "Ferb",
            "Tin Cup",
            "I'm Faster Than a Monkey",
            "Glue Factory Reject"
        );

        System.out.println("It's a race of " + r.getDistance() + " lengths");

        System.out.println("Press Enter to run the race....");
        System.in.read();

        r.run();
    }

}

class Race
{
    private Random rand = new Random();

    private int distance = rand.nextInt(250);
    private CountDownLatch start;
    private CountDownLatch finish;

    private List<String> horses = new ArrayList<String>();

    public Race(String... names)
    {
        this.horses.addAll(Arrays.asList(names));
    }

    public void run()
        throws InterruptedException
    {
        System.out.println("And the horses are stepping up to the gate...");
        final CountDownLatch start = new CountDownLatch(1);
        final CountDownLatch finish = new CountDownLatch(horses.size());
        final List<String> places =
            Collections.synchronizedList(new ArrayList<String>());

        for (final String h : horses)
        {
            new Thread(new Runnable() {
                public void run() {
                    try
                    {
                        System.out.println(h +
                            " stepping up to the gate...");
                        start.await();

                        int traveled = 0;
                        while (traveled < distance)
                        {
                            // через 0-2 секунды....
                            Thread.sleep(rand.nextInt(3) * 1000);

                            // ... лошадь проходит дистанцию 0-14 пунктов
                            traveled += rand.nextInt(15);
                            System.out.println(h +
                                " advanced to " + traveled + "!");
                        }
                        finish.countDown();
                        System.out.println(h +
                            " crossed the finish!");
                        places.add(h);
                    }
                    catch (InterruptedException intEx)
                    {
                        System.out.println("ABORTING RACE!!!");
                        intEx.printStackTrace();
                    }
                }
            }).start();
        }

        System.out.println("And... they're off!");
        start.countDown();

        finish.await();
        System.out.println("And we have our winners!");
        System.out.println(places.get(0) + " took the gold...");
        System.out.println(places.get(1) + " got the silver...");
        System.out.println("and " + places.get(2) + " took home the bronze.");
    }

    public int getDistance() {
        return distance;
    }
}
