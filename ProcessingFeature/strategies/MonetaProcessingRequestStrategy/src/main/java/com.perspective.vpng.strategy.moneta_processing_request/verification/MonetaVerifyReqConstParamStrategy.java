package com.perspective.vpng.strategy.moneta_processing_request.verification;

import com.perspective.vpng.strategy.moneta_processing_request.IMonetaReqConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class MonetaVerifyReqConstParamStrategy implements IResolveDependencyStrategy {

    private final IField envelopeF;
    private final String payer;
    private final String paymentPassword;

    public MonetaVerifyReqConstParamStrategy(final IMonetaReqConfig config) throws ResolutionException {
        this.envelopeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "Envelope");
        this.payer = config.getPayer();
        this.paymentPassword = config.getPaymentPassword();
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IObject envelope = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                "{\n" +
                    "    \"Body\": {\n" +
                    "        \"VerifyPaymentRequest\": {\n" +
                    "            \"payer\": \"" + this.payer + "\",\n" +
                    "            \"paymentPassword\": \"" + this.paymentPassword + "\"\n" +
                    "        }\n" +
                    "    }\n" +
                "}"
            );
            envelopeF.out(result, envelope);

            return (T) result;
        } catch (InvalidArgumentException | ChangeValueException | ResolutionException e) {
            throw new ResolveDependencyStrategyException("Failed to add template params to Moneta processing verification request", e);
        }
    }

}
