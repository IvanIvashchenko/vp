package com.perspective.vpng.strategy.moneta_processing_request;

public interface IMonetaReqConfig {

    String getPayer();
    String getPaymentPassword();
    String getCollectionName();

}
