package com.perspective.vpng.strategy.moneta_processing_request;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;
import java.util.Map;

public class MonetaReqExtraParamStrategy implements IResolveDependencyStrategy {

    private final IField paramsF;
    private final IField nameF;
    private final IField valueF;
    private final IField providerIdF;
    private final IField payeeF;
    private final IField isPayerAmountF;

    private final IField payeeRequestF;
    private final IField isPayerAmountRequestF;

    public MonetaReqExtraParamStrategy() throws ResolutionException {

        final IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        final IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());

        paramsF = IOC.resolve(fieldKey, "params");
        nameF = IOC.resolve(fieldKey, "name");
        valueF = IOC.resolve(fieldKey, "value");
        payeeF = IOC.resolve(fieldKey, "payee");
        isPayerAmountF = IOC.resolve(fieldKey, "isPayerAmount");

        providerIdF = IOC.resolve(nestedFieldKey, "поставщик/id");
        payeeRequestF = IOC.resolve(nestedFieldKey, "Envelope/Body/PaymentRequest/payee");
        isPayerAmountRequestF = IOC.resolve(nestedFieldKey, "Envelope/Body/PaymentRequest/isPayerAmount");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        IObject invoice = (IObject) args[0];
        IObject result = (IObject) args[1];
        Map<String, IObject> processingMap = (Map) args[2];

        try {
            IObject processingParams = processingMap.get(providerIdF.in(invoice));
            List<IObject> paramsList = paramsF.in(processingParams);
            String payee = payeeF.in(processingParams);
            Boolean isPayerAmount = isPayerAmountF.in(processingParams);
            payeeRequestF.out(result, payee);
            isPayerAmountRequestF.out(result, isPayerAmount);

            String paramsFN, invoiceFN;
            for (IObject param : paramsList) {
                paramsFN = nameF.in(param);
                invoiceFN = valueF.in(param);
                IField resultF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), paramsFN);
                IField inPaymentField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), invoiceFN);
                resultF.out(result, inPaymentField.in(invoice));
            }

            return (T) result;
        } catch (ResolutionException | ReadValueException | InvalidArgumentException | ChangeValueException e) {
            throw new ResolveDependencyStrategyException("Failed to add extra params to Moneta processing request", e);
        }
    }
}
