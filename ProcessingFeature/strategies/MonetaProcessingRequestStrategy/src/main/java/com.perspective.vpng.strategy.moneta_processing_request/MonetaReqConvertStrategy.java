package com.perspective.vpng.strategy.moneta_processing_request;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.SerializeException;

public class MonetaReqConvertStrategy implements IResolveDependencyStrategy {

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        IObject data = (IObject) args[0];
        try {
            return (T) data.serialize();
        } catch (SerializeException e) {
            throw new ResolveDependencyStrategyException("Can't convert Moneta request object", e);
        }
    }
}
