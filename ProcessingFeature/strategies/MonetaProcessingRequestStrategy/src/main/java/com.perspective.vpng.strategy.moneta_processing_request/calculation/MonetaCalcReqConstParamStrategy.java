package com.perspective.vpng.strategy.moneta_processing_request.calculation;

import com.perspective.vpng.strategy.moneta_processing_request.calculation.wrapper.MonetaCalcReqConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class MonetaCalcReqConstParamStrategy implements IResolveDependencyStrategy {

    private final IField envelopeF;
    private final String version;

    public MonetaCalcReqConstParamStrategy(final MonetaCalcReqConfig config) throws ResolutionException {
        this.envelopeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "Envelope");
        this.version = config.getVersion();
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IObject envelope = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                "{" +
                    "        \"Body\": {\n" +
                    "            \"GetNextStepRequest\": {\n" +
                    "                \"version\": \"" + this.version + "\"\n" +
                    "            }\n" +
                    "        }" +
                "}"
            );
            envelopeF.out(result, envelope);

            return (T) result;
        } catch (InvalidArgumentException | ChangeValueException | ResolutionException e) {
            throw new ResolveDependencyStrategyException("Failed to add template params to Moneta processing calculation request", e);
        }
    }
}
