package com.perspective.vpng.strategy.moneta_processing_request.calculation.wrapper;

public interface MonetaCalcReqConfig {

    String getCollectionName();
    String getVersion();
    String getSearchMethod();
}
