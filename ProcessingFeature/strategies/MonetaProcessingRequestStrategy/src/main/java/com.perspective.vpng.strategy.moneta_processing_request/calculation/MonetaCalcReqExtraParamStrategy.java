package com.perspective.vpng.strategy.moneta_processing_request.calculation;

import com.perspective.vpng.strategy.moneta_processing_request.calculation.wrapper.MonetaCalcReqConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

public class MonetaCalcReqExtraParamStrategy implements IResolveDependencyStrategy {

    private final String searchMethod;

    private final IField requestProviderIdF;
    private final IField payeeF;
    private final IField calculationKeyTypeF;
    private final IField calculationKeyF;
    private final IField codeF;
    private final IField searchValueF;
    private final IField nameF;
    private final IField paramsF;
    private final IField fieldsInfoF;

    public MonetaCalcReqExtraParamStrategy(final MonetaCalcReqConfig config) throws ResolutionException {

        this.searchMethod = config.getSearchMethod();
        this.requestProviderIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/GetNextStepRequest/providerId");
        this.payeeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "payee");
        this.calculationKeyTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "calculationKeyType");
        this.calculationKeyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "calculationKey");
        this.codeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "code");
        this.searchValueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "searchValue");
        this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        this.paramsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "params");
        this.fieldsInfoF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/GetNextStepRequest/fieldsInfo");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        IObject requestInfo = (IObject) args[0];
        IObject requestObj = (IObject) args[1];
        IObject processingParams = (IObject) args[2];
        try {
            requestProviderIdF.out(requestObj, payeeF.in(processingParams));
            List<IObject> params = paramsF.in(processingParams);
            String calculationKeyType = calculationKeyTypeF.in(requestInfo);
            String searchValue = null, code = null;
            for (IObject param : params) {
                if (calculationKeyType.equals(nameF.in(param))) {
                    searchValue = searchValueF.in(param);
                    code = codeF.in(param);
                    break;
                }
            }
            if (searchValue == null || code == null) {
                throw new ResolveDependencyStrategyException(
                    "Can't find code or search value in request params by name " + calculationKeyType
                );
            }
            IObject fieldsInfo = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                "{\n" +
                    "    \"attribute\": [{\n" +
                    "        \"name\": \"" + searchMethod + "\",\n" +
                    "        \"value\": \"" + searchValue +"\"\n" +
                    "    }, {\n" +
                    "        \"name\": \"" + code + "\",\n" +
                    "        \"value\": \"" + calculationKeyF.in(requestInfo) + "\"\n" +
                    "    }]\n" +
                "}"
            );
            fieldsInfoF.out(requestObj, fieldsInfo);
        } catch (ChangeValueException | InvalidArgumentException | ReadValueException | ResolutionException e) {
            throw new ResolveDependencyStrategyException("Failed to add extra params to Moneta processing request", e);
        }

        return (T) requestObj;
    }
}
