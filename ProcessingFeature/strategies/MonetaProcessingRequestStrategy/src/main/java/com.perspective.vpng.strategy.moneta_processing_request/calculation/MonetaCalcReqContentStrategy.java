package com.perspective.vpng.strategy.moneta_processing_request.calculation;

import com.perspective.vpng.strategy.moneta_processing_request.calculation.wrapper.MonetaCalcReqConfig;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MonetaCalcReqContentStrategy implements IResolveDependencyStrategy {

    private final IResolveDependencyStrategy constParamStrategy;
    private final IResolveDependencyStrategy extraParamStrategy;

    private final String collectionName;
    private final IPool connectionPool;

    public MonetaCalcReqContentStrategy(final MonetaCalcReqConfig config) throws ResolutionException {

        this.collectionName = config.getCollectionName();
        Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
        this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);

        this.constParamStrategy = new MonetaCalcReqConstParamStrategy(config);
        this.extraParamStrategy = new MonetaCalcReqExtraParamStrategy(config);
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {

        try {
            IObject requestInfo = (IObject) args[0];
            String providerId = (String) args[1];
            IObject requestObj = constParamStrategy.resolve();
            IObject processingParams = getProcessingParams(providerId);
            requestObj = extraParamStrategy.resolve(
                requestInfo,
                requestObj,
                processingParams
            );

            return (T) requestObj;
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Error during cast", ex);
        }
    }

    private IObject getProcessingParams(final String providerId) throws ResolveDependencyStrategyException {
        List<IObject> params = new ArrayList<>();
        try (IPoolGuard guard = new PoolGuard(connectionPool)) {
            ITask searchTask = IOC.resolve(
                Keys.getOrAdd("db.collection.search"),
                guard.getObject(),
                collectionName,
                prepareSearchQuery(providerId),
                (IAction<IObject[]>) foundDocs -> {
                    try {
                        params.addAll(Arrays.asList(foundDocs));
                    } catch (Exception e) {
                        throw new ActionExecuteException(e);
                    }
                }
            );
            searchTask.execute();
            if (params.isEmpty()) {
                throw new ResolveDependencyStrategyException(
                    "Can't find processing params for processing Moneta and provider " + providerId
                );
            }

            return params.get(0);
        } catch (ResolutionException | TaskExecutionException | PoolGuardException e) {
            throw new ResolveDependencyStrategyException("Can't get Moneta processing parameters from database", e);
        }
    }

    private IObject prepareSearchQuery(final String key) throws ResolutionException {

        String queryKey = new StringBuilder().append("moneta_").append(key).toString();

        return IOC.resolve(
            Keys.getOrAdd(IObject.class.getCanonicalName()),
                "{" +
                    "\"filter\": {" +
                        "\"name\": {\"$eq\": \"" + queryKey + "\"}," +
                        "\"type\": {\"$eq\": \"calculation\"}" +
                    "}," +
                    "\"page\": {\"size\": 1, \"number\": 1}" +
                "}"
        );
    }
}
