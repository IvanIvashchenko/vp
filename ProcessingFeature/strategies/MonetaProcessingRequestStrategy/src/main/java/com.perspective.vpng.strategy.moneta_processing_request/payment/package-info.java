/**
 * Package contains strategies specific for payment request for Moneta
 */
package com.perspective.vpng.strategy.moneta_processing_request.payment;