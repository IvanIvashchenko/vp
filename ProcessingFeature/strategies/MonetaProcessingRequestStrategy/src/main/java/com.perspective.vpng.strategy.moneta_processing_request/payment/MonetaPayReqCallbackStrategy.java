package com.perspective.vpng.strategy.moneta_processing_request.payment;

import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.message_bus.message_bus.MessageBus;
import info.smart_tools.smartactors.scope.iscope.IScope;
import info.smart_tools.smartactors.scope.iscope_provider_container.exception.ScopeProviderException;
import info.smart_tools.smartactors.scope.scope_provider.ScopeProvider;
import org.asynchttpclient.Response;

public class MonetaPayReqCallbackStrategy implements IResolveDependencyStrategy {

    private final IAction<Response> callback;

    private final IField responseF;
    private final IField messageMapIdF;
    private final String messageMapId;

    public MonetaPayReqCallbackStrategy() throws ResolutionException {

        IScope scope;
        //field with identifier for check status request
//        responseIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "PaymentResponse/id");
        responseF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "response");
        messageMapIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "messageMapId");
        messageMapId = "handleMonetaCheckStatusResponse";
        try {
            scope = ScopeProvider.getCurrentScope();
        } catch (ScopeProviderException e) {
            throw new ResolutionException("Can't get current scope", e);
        }
        callback = (response -> {
            try {
                ScopeProvider.setCurrentScope(scope);
                IObject message = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                String body = response.getResponseBody();
                IObject responseObject = IOC.resolve(Keys.getOrAdd("convertXmlToIObject"), body);
                messageMapIdF.out(message, messageMapId);
                responseF.out(message, responseObject);
                MessageBus.send(message);

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public <T> T resolve(final Object... objects) throws ResolveDependencyStrategyException {
        return (T) callback;
    }
}
