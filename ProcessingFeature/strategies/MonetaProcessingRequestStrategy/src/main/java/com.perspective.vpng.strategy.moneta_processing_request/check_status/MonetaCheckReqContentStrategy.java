package com.perspective.vpng.strategy.moneta_processing_request.check_status;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

//TODO:: check it with payment callback. Write another handler into ProcessingActor for using this
public class MonetaCheckReqContentStrategy implements IResolveDependencyStrategy {

    private final IField requestIdF;

    public MonetaCheckReqContentStrategy() throws ResolutionException {

        this.requestIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "PaymentResponse/id");
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {

        Set<IObject> result = new HashSet<>();
        try {
            List<IObject> requestInfoObjects = (List) args[0];
            for (IObject requestInfo : requestInfoObjects) {
                IObject requestObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"Body\": {\"GetOperationDetailsByIdRequest\": \"" + requestIdF.in(requestInfo) + "\"}}"
                );
                result.add(requestObj);
            }

            return (T) result;
        } catch (InvalidArgumentException | ReadValueException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Error during cast to list", ex);
        }
    }

}
