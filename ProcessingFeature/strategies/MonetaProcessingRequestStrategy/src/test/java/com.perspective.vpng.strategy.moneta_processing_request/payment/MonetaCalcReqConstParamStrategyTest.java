package com.perspective.vpng.strategy.moneta_processing_request.payment;

import com.perspective.vpng.strategy.moneta_processing_request.calculation.wrapper.MonetaCalcReqConfig;
import com.perspective.vpng.strategy.moneta_processing_request.calculation.MonetaCalcReqConstParamStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class MonetaCalcReqConstParamStrategyTest {

    private IResolveDependencyStrategy strategy;

    @BeforeClass
    public static void prepareIOC() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {
        MonetaCalcReqConfig config = mock(MonetaCalcReqConfig.class);
        when(config.getVersion()).thenReturn("VERSION_3");
        strategy = new MonetaCalcReqConstParamStrategy(config);
    }

    @Test
    public void should_ResolvePayReqConstParam() throws Exception {
        IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        result = strategy.resolve(result);

        IField versionF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/GetNextStepRequest/version");
        assertEquals(versionF.in(result), "VERSION_3");
    }
}
