package com.perspective.vpng.strategy.moneta_processing_request.payment;

import com.perspective.vpng.strategy.moneta_processing_request.calculation.wrapper.MonetaCalcReqConfig;
import com.perspective.vpng.strategy.moneta_processing_request.calculation.MonetaCalcReqExtraParamStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.ioc_strategy_pack_plugins.resolve_standard_types_strategies_plugin.ResolveStandardTypesStrategiesPlugin;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class MonetaCalcReqExtraParamStrategyTest {

    private IResolveDependencyStrategy strategy;

    @BeforeClass
    public static void prepareIOC() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();
        new ResolveStandardTypesStrategiesPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {
        MonetaCalcReqConfig config = mock(MonetaCalcReqConfig.class);
        when(config.getSearchMethod()).thenReturn("CUSTOMFIELD:200");
        strategy = new MonetaCalcReqExtraParamStrategy(config);
    }

    @Test
    public void should_ResolveCalcReqExtraParam() throws Exception {
        IObject requestInfo = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"providerId\": \"moneta_gibdd\"," +
                "\"calculationKey\": \"123123123\"," +
                "\"calculationKeyType\": \"GIBDD_TS_REGISTRATION_NUMBER\"" +
                "}"
        );
        IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        IObject processingParams = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{\n" +
                "    \"name\": \"moneta_gibdd\",\n" +
                "    \"processing\": \"moneta\",\n" +
                "    \"type\": \"calculation\",\n" +
                "    \"payee\": \"9171.1\",\n" +
                "    \"params\": [\n" +
                "        {\n" +
                "            \"id\": \"102\",\n" +
                "            \"name\": \"GIBDD_TS_REGISTRATION_NUMBER\",\n" +
                "            \"code\": \"CUSTOMFIELD:102\",\n" +
                "            \"searchValue\": \"1\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": \"108\",\n" +
                "            \"name\": \"GIBDD_ALTPAYERIDENTIFIER\",\n" +
                "            \"code\": \"CUSTOMFIELD:108\",\n" +
                "            \"searchValue\": \"5\"\n" +
                "        }\n" +
                "    ]\n" +
                "}"
        );

        result = strategy.resolve(requestInfo, result, processingParams);

        IField attributeF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/GetNextStepRequest/fieldsInfo/attribute");
        IField providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/GetNextStepRequest/providerId");
        IField nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        IField valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
        assertEquals(providerIdF.in(result), "9171.1");
        List<IObject> attributes = attributeF.in(result);
        assertEquals(attributes.size(), 2);
        boolean containsSearchMethod = false;
        boolean containsSearchValue = false;
        boolean containsCalculationKeyType = false;
        boolean containsCalculationKeyValue = false;
        for (IObject attr : attributes) {
            if (nameF.in(attr).equals("CUSTOMFIELD:200")) {
                containsSearchMethod = true;
            }
            if (nameF.in(attr).equals("CUSTOMFIELD:102")) {
                containsCalculationKeyType = true;
            }
            if (valueF.in(attr).equals("1")) {
                containsSearchValue = true;
            }
            if (valueF.in(attr).equals("123123123")) {
                containsCalculationKeyValue = true;
            }
        }
        assertTrue(containsSearchMethod);
        assertTrue(containsSearchValue);
        assertTrue(containsCalculationKeyType);
        assertTrue(containsCalculationKeyValue);
    }
}
