package com.perspective.vpng.strategy.moneta_processing_request.payment;

import com.perspective.vpng.strategy.moneta_processing_request.MonetaReqExtraParamStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.ioc_strategy_pack_plugins.resolve_standard_types_strategies_plugin.ResolveStandardTypesStrategiesPlugin;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MonetaReqExtraParamStrategyTest {

    private IResolveDependencyStrategy strategy;

    @BeforeClass
    public static void prepareIOC() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();
        new ResolveStandardTypesStrategiesPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {
        strategy = new MonetaReqExtraParamStrategy();
    }

    @Test
    public void should_ResolvePayReqExtraParam() throws Exception {
        IObject invoice = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"поставщик\": {\"id\": \"moneta_gibdd\"}," +
                "\"всего-к-оплате\": 776.4," +
                "\"number\": \"500034\"" +
            "}"
        );
        IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        IObject processingParams = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "    \"payee\": \"9171\",\n" +
                "    \"isPayerAmount\": false,\n" +
                "    \"name\": \"moneta_gibdd\",\n" +
                "    \"params\": [\n" +
                "        {\n" +
                "            \"name\": \"Envelope/Body/PaymentRequest/amount\",\n" +
                "            \"value\": \"всего-к-оплате\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"Envelope/Body/PaymentRequest/clientTransaction\",\n" +
                "            \"value\": \"number\"\n" +
                "        }\n" +
                "    ]" +
            "}"
        );
        Map<String, IObject> processingMap = new HashMap<>();
        processingMap.put("moneta_gibdd", processingParams);

        result = strategy.resolve(invoice, result, processingMap);

        IField amountF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/PaymentRequest/amount");
        IField transactionF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/PaymentRequest/clientTransaction");
        IField payeeF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/PaymentRequest/payee");
        IField isPayerAmountF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Envelope/Body/PaymentRequest/isPayerAmount");
        assertEquals(amountF.in(result, BigDecimal.class), BigDecimal.valueOf(776.4));
        assertEquals(transactionF.in(result), "500034");
        assertEquals(payeeF.in(result), "9171");
        assertEquals(isPayerAmountF.in(result), false);
    }

}
