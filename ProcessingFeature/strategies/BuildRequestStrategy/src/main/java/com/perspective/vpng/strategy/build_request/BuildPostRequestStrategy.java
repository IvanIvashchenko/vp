package com.perspective.vpng.strategy.build_request;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import org.asynchttpclient.RequestBuilder;

public class BuildPostRequestStrategy implements IResolveDependencyStrategy {

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        RequestBuilder builder = (RequestBuilder) args[0];
        String url = (String) args[1];
        String content = (String) args[2];

        return (T) builder.setUrl(url).setBody(content).build();
    }
}
