package com.perspective.vpng.strategy.build_request;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import org.asynchttpclient.RequestBuilder;

public class BuildGetRequestStrategy implements IResolveDependencyStrategy {

    private static final String QUESTION_MARK = "?";

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        RequestBuilder builder = (RequestBuilder) args[0];
        String url = (String) args[1];
        String requestStr = (String) args[2];

        return (T) builder.setUrl(url + QUESTION_MARK + requestStr).build();

    }
}
