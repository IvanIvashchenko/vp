package com.perspective.vpng.strategy.apelsin_processing_request.payment;

import com.perspective.vpng.strategy.apelsin_processing_request.payment.wrapper.IApelsinPayReqConfig;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.ioc_strategy_pack_plugins.resolve_standard_types_strategies_plugin.ResolveStandardTypesStrategiesPlugin;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApelsinPayReqConstParamStrategyTest {

    public ApelsinPayReqConstParamStrategyTest() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();
        new ResolveStandardTypesStrategiesPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Test
    public void should_ResolvePayReqConstParam() throws Exception {
        IApelsinPayReqConfig config = mock(IApelsinPayReqConfig.class);
        when(config.getLogin()).thenReturn("test_login");
        when(config.getPoint()).thenReturn(1);
        when(config.getDealer()).thenReturn(2);
        when(config.getSoft()).thenReturn("test_soft");
        when(config.getVersion()).thenReturn("test_version");

        IResolveDependencyStrategy strategy = new ApelsinPayReqConstParamStrategy(config);

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        IField loginF = IOC.resolve(fieldKey, "login");
        IField pointF = IOC.resolve(fieldKey, "point");
        IField dealerF = IOC.resolve(fieldKey, "dealer");
        IField softF = IOC.resolve(fieldKey, "soft");
        IField versionF = IOC.resolve(fieldKey, "version");

        IObject request = strategy.resolve();

        assertEquals(loginF.in(request), "test_login");
        assertEquals((int) pointF.in(request), 1);
        assertEquals((int) dealerF.in(request), 2);
        assertEquals(softF.in(request), "test_soft");
        assertEquals(versionF.in(request), "test_version");
    }

    @Test
    public void should_ThrowException_InvalidConfig() throws Exception {
        try {
            IApelsinPayReqConfig config = mock(IApelsinPayReqConfig.class);
            new ApelsinPayReqConstParamStrategy(config);
        } catch (ResolutionException ex) {
            assertEquals(ex.getCause().getClass(), NullPointerException.class);
            return;
        }
        fail("Expected exception!");
    }

}
