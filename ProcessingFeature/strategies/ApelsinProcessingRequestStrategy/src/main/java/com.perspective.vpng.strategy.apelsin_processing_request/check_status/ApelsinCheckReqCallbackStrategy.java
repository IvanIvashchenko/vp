package com.perspective.vpng.strategy.apelsin_processing_request.check_status;

import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.message_bus.message_bus.MessageBus;
import info.smart_tools.smartactors.scope.iscope.IScope;
import info.smart_tools.smartactors.scope.scope_provider.ScopeProvider;
import org.asynchttpclient.Response;

import java.util.ArrayList;
import java.util.List;

public class ApelsinCheckReqCallbackStrategy implements IResolveDependencyStrategy {

    private final IAction<Response> callback;
    private final IField responseF;
    private final IField payParamsF;
    private final IField processingIdF;
    private final IField serverIdF;
    private final IField statusF;
    private final IField invoicesF;

    public ApelsinCheckReqCallbackStrategy() throws ResolutionException {
        try {
            IScope scope = ScopeProvider.getCurrentScope();
            responseF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "response");
            payParamsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "pay_params");
            processingIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processingId");
            serverIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "server_id");
            statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
            invoicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoices");
            IObject message = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                    ("{" +
                            "'messageMapId': 'setProcessingStatusMap'," +
                            "'processingName': 'Apelsin'" +
                            "}").replace('\'', '"')
            );

            callback = (response -> {
                try {
                    ScopeProvider.setCurrentScope(scope);
                    IObject iobjectResponse = IOC.resolve(Keys.getOrAdd("convertXmlToIObject"), new String(response.getResponseBodyAsBytes(), "windows-1251"));

                    List<IObject> resultList = new ArrayList<>();
                    if (payParamsF.in(responseF.in(iobjectResponse)) instanceof List) {
                        List<IObject> payParams = payParamsF.in(responseF.in(iobjectResponse));

                        for (IObject payParam : payParams) {
                            IObject el = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                            processingIdF.out(el, serverIdF.in(payParam));
                            statusF.out(el, statusF.in(payParam));
                            resultList.add(el);
                        }
                    } else {
                        IObject payParam = payParamsF.in(responseF.in(iobjectResponse));
                        IObject el = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                        processingIdF.out(el, serverIdF.in(payParam));
                        statusF.out(el, statusF.in(payParam));
                        resultList.add(el);
                    }

                    invoicesF.out(message, resultList);
                    MessageBus.send(message);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        return (T) callback;
    }
}
