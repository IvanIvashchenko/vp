package com.perspective.vpng.strategy.apelsin_processing_request.check_status;

import com.perspective.vpng.strategy.apelsin_processing_request.payment.ApelsinPayReqConstParamStrategy;
import com.perspective.vpng.strategy.apelsin_processing_request.payment.wrapper.IApelsinPayReqConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.*;

public class ApelsinCheckReqContentStrategy implements IResolveDependencyStrategy {
    private final IResolveDependencyStrategy constParamStrategy;

    private final IField payParamsF;
    private final IField serverIdF;
    private final IField requestF;
    private final IField processingIdF;


    public ApelsinCheckReqContentStrategy(final IApelsinPayReqConfig config) throws ResolutionException {
        this.constParamStrategy = new ApelsinPayReqConstParamStrategy(config);
        this.payParamsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "pay_params");
        this.serverIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "server_id");
        this.processingIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processingId");
        this.requestF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "request");
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject request = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IObject content = constParamStrategy.resolve();
            List<IObject> invoices = (List) args[0];

            List<IObject> extraParam = new ArrayList<>();
            for (IObject invoice : invoices) {
                IObject el = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                serverIdF.out(el, processingIdF.in(invoice));
                extraParam.add(el);
            }
            payParamsF.out(content, extraParam);

            requestF.out(request, content);
            return (T) Collections.singleton(request);
        } catch (ChangeValueException | InvalidArgumentException | ReadValueException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("", ex);
        }
    }
}
