package com.perspective.vpng.strategy.apelsin_processing_request.payment;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ApelsinPayReqExtraParamStrategy implements IResolveDependencyStrategy {

    private IField providerIdF;
    private IField paramsF;
    private IField customParamsF;
    private IField strategyF;
    private IField nameF;
    private IField valueF;

    public ApelsinPayReqExtraParamStrategy() throws ResolutionException {
        this.paramsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "params");
        this.customParamsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "customParams");
        this.strategyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "strategy");
        this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        this.valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
        this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            List<IObject> invoices = (List) args[0];
            Map<String, IObject> processingMap = (Map) args[1];

            List<IObject> payParams = new ArrayList<>();
            for (IObject invoice : invoices) {
                IObject processingParams = processingMap.get(providerIdF.in(invoice));
                List<IObject> paramsList = paramsF.in(processingParams);
                List<IObject> customParamsList = customParamsF.in(processingParams);
                IObject invoicePayParams = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

                for (IObject param : paramsList) {
                    IField resultF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), (String) nameF.in(param));
                    IField inPaymentField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), (String) valueF.in(param));
                    resultF.out(invoicePayParams, inPaymentField.in(invoice));
                }

                for (IObject param : customParamsList) {
                    IResolveDependencyStrategy strategy = IOC.resolve(Keys.getOrAdd(strategyF.in(param)));
                    strategy.resolve(invoicePayParams, invoice);
                }

                payParams.add(invoicePayParams);
            }

            return (T) payParams;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException("Failed to add extra params to processing request", e);
        }
    }

}
