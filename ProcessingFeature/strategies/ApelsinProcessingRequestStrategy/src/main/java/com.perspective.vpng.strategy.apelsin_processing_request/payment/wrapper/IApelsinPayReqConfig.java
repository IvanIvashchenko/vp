package com.perspective.vpng.strategy.apelsin_processing_request.payment.wrapper;

public interface IApelsinPayReqConfig {

    String getLogin();

    int getPoint();

    int getDealer();

    String getSoft();

    String getVersion();

    String getType();

    String getProcessingCollection();

}
