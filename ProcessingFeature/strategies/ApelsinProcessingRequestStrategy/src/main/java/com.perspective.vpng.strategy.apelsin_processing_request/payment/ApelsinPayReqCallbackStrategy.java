package com.perspective.vpng.strategy.apelsin_processing_request.payment;

import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.message_bus.message_bus.MessageBus;
import info.smart_tools.smartactors.scope.iscope.IScope;
import info.smart_tools.smartactors.scope.scope_provider.ScopeProvider;
import org.asynchttpclient.Response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ApelsinPayReqCallbackStrategy implements IResolveDependencyStrategy {

    private final IAction<Response> callback;
    private final IField responseF;
    private final IField invoicesF;
    private final IField payParamsF;
    private final IField invoiceIdF;
    private final IField processingIdF;
    private final IField idF;
    private final IField serverIdF;

    public ApelsinPayReqCallbackStrategy() throws ResolutionException {
        try {
            IScope scope = ScopeProvider.getCurrentScope();
            responseF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "response");
            invoicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoices");
            payParamsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "pay_params");
            invoiceIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoiceId");
            processingIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processingId");
            idF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "id");
            serverIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "server_id");
            IObject message = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                    ("{" +
                        "'messageMapId': 'createCheckProcessingStatusTaskMap'," +
                        "'processingName': 'Apelsin'" +
                    "}").replace('\'', '"')
            );
            callback = (response -> {
                try {
                    ScopeProvider.setCurrentScope(scope);
                    IObject iobjectResponse = IOC.resolve(Keys.getOrAdd("convertXmlToIObject"), new String(response.getResponseBodyAsBytes(), "windows-1251"));
                    responseF.out(message, iobjectResponse);
                    List<IObject> invoices = payParamsF.in(responseF.in(iobjectResponse)) instanceof Collection ?
                            payParamsF.in(iobjectResponse) :
                            new ArrayList<IObject>(){{add(payParamsF.in(responseF.in(iobjectResponse)));}};
                    List<IObject> invoicesIdsMap = new ArrayList<>();
                    for (IObject invoice : invoices) {
                        IObject el = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                        invoiceIdF.out(el, idF.in(invoice));
                        processingIdF.out(el, serverIdF.in(invoice));
                        invoicesIdsMap.add(el);
                    }
                    invoicesF.out(message, invoicesIdsMap);
                    MessageBus.send(message);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        return (T) callback;
    }
}
