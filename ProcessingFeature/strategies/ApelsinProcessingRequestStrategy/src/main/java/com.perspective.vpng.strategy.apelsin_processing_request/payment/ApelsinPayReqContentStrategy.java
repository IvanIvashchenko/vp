package com.perspective.vpng.strategy.apelsin_processing_request.payment;

import com.perspective.vpng.strategy.apelsin_processing_request.payment.wrapper.IApelsinPayReqConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;

import java.util.*;

public class ApelsinPayReqContentStrategy implements IResolveDependencyStrategy {

    private final IResolveDependencyStrategy constParamStrategy;
    private final IResolveDependencyStrategy extraParamStrategy;

    private final IField payParamsF;
    private final IField providerIdF;
    private final IField nameF;
    private final IField requestF;

    private final String processingCollection;
    private final IPool connectionPool;

    public ApelsinPayReqContentStrategy(final IApelsinPayReqConfig config) throws ResolutionException {

        this.processingCollection = config.getProcessingCollection();

        this.constParamStrategy = new ApelsinPayReqConstParamStrategy(config);
        this.extraParamStrategy = new ApelsinPayReqExtraParamStrategy();

        this.payParamsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "pay_params");
        this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
        this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        this.requestF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "request");

        Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
        this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject request = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IObject content = constParamStrategy.resolve();
            List<IObject> invoices = (List) args[0];
            List<String> providerIds = getProviderIds(invoices);
            List<IObject> processingParams = getProcessingParams(providerIds);
            List<IObject> extraParam = extraParamStrategy.resolve(
                    invoices,
                    associate(providerIds, processingParams)
            );
            payParamsF.out(content, extraParam);

            requestF.out(request, content);
            return (T) Collections.singleton(request);
        } catch (ChangeValueException | InvalidArgumentException | ReadValueException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("", ex);
        }
    }

    private List<String> getProviderIds(final List<IObject> invoices)
            throws ReadValueException, InvalidArgumentException {

        List<String> keys = new ArrayList<>(invoices.size());
        for (IObject invoice: invoices) {
            String providerId = providerIdF.in(invoice);
            if (!keys.contains(providerId)) {
                keys.add(providerId);
            }
        }

        return keys;
    }

    private List<IObject> getProcessingParams(final List<String> keys) {
        List<IObject> items = new ArrayList<>();
        try (IPoolGuard guard = new PoolGuard(connectionPool)) {
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    guard.getObject(),
                    processingCollection,
                    prepareSearchQuery(keys),
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            items.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );
            searchTask.execute();

            return items;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private IObject prepareSearchQuery(final List<String> keys) throws ResolutionException {
        String[] queryKeys = new String[keys.size()];
        for (int i = 0; i < keys.size(); i++) {
            queryKeys[i] = "apelsin_" + keys.get(i);
        }

        return IOC.resolve(
                Keys.getOrAdd(IObject.class.getCanonicalName()),
                String.format(
                        "{" +
                                "'filter': {'name': {'$in': %s}, 'type': {'$eq': 'payment'}}," +
                                "'page': {'size': %s, 'number': 1}" +
                        "}",
                        Arrays.toString(queryKeys),
                        queryKeys.length
                ).replace('\'', '"')
        );
    }

    private Map<String, IObject> associate(final List<String> keys, final List<IObject> params)
            throws ReadValueException, InvalidArgumentException {

        Map<String, IObject> container = new HashMap<>(keys.size());
        String providerProcessingName;
        for (String key: keys) {
            for (IObject param: params) {
                providerProcessingName = nameF.in(param);
                if (providerProcessingName.contains(key)) {
                    container.put(key, param);
                    break;
                }
            }
        }

        return container;
    }

}
