package com.perspective.vpng.strategy.apelsin_processing_request.payment;

import com.perspective.vpng.strategy.apelsin_processing_request.payment.wrapper.IApelsinPayReqConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Optional;

public class ApelsinPayReqConstParamStrategy implements IResolveDependencyStrategy {

    private final IApelsinPayReqConfig config;

    private final IField loginF;
    private final IField pointF;
    private final IField dealerF;
    private final IField softF;
    private final IField versionF;
    private final IField typeF;

    public ApelsinPayReqConstParamStrategy(IApelsinPayReqConfig config) throws ResolutionException {
        this.config = config;
        checkConfig(config);

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        loginF = IOC.resolve(fieldKey, "login");
        pointF = IOC.resolve(fieldKey, "point");
        dealerF = IOC.resolve(fieldKey, "dealer");
        softF = IOC.resolve(fieldKey, "soft");
        versionF = IOC.resolve(fieldKey, "version");
        typeF = IOC.resolve(fieldKey, "type");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject request = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            loginF.out(request, config.getLogin());
            pointF.out(request, config.getPoint());
            dealerF.out(request, config.getDealer());
            softF.out(request, config.getSoft());
            versionF.out(request, config.getVersion());
            typeF.out(request, config.getType());
            return (T) request;
        } catch (ChangeValueException | InvalidArgumentException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException("Failed prepare request for Apelsin processing!", ex);
        }
    }

    private void checkConfig(IApelsinPayReqConfig config) throws ResolutionException {
        try {
            Optional.of(config.getLogin());
            Optional.of(config.getPoint());
            Optional.of(config.getDealer());
            Optional.of(config.getSoft());
            Optional.of(config.getVersion());
            if (config.getLogin().isEmpty() ||
                config.getSoft().isEmpty() || config.getVersion().isEmpty()) {
                throw new ResolutionException("Invalid config for Apelsin processing!");
            }
        } catch (NullPointerException ex) {
            throw new ResolutionException("Invalid config for Apelsin processing!", ex);
        }
    }

}
