package com.perspective.vpng.strategy.apelsin_processing_request.calculation;

import com.perspective.vpng.strategy.apelsin_processing_request.payment.ApelsinPayReqConstParamStrategy;
import com.perspective.vpng.strategy.apelsin_processing_request.payment.wrapper.IApelsinPayReqConfig;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ApelsinCalcReqContentStrategy implements IResolveDependencyStrategy {
    private final IResolveDependencyStrategy constParamStrategy;
    private IApelsinPayReqConfig config;
    private IPool connectionPool;
    private String PROCESSING_COLLECTION = "processing";
    private IField paramsF;
    private IField typeF;
    private IField toFieldF;
    private IField fromFieldF;
    private IField valueF;
    private IField requestF;

    public ApelsinCalcReqContentStrategy(final IApelsinPayReqConfig config) {
        try {
            this.config = config;
            this.constParamStrategy = new ApelsinPayReqConstParamStrategy(config);
            Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
            paramsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "params");
            typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            toFieldF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "toField");
            fromFieldF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "fromField");
            valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
            requestF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "request");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(Object... objects) throws ResolveDependencyStrategyException {
        try {
            IObject filterData = (IObject) objects[0];
            String providerId = (String) objects[1];
            IObject calculationParams = getCalculationParams(providerId);
            List<IObject> params = paramsF.in(calculationParams);

            IObject content = constParamStrategy.resolve();

            for (IObject param : params) {
                if (typeF.in(param).equals("map")) {
                    IField resultF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), (String) toFieldF.in(param));
                    IField inPaymentField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), (String) fromFieldF.in(param));
                    resultF.out(content, inPaymentField.in(filterData));
                }

                if (typeF.in(param).equals("const")) {
                    IField resultF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), (String) toFieldF.in(param));
                    resultF.out(content, valueF.in(param));
                }
            }
            IObject request = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            requestF.out(request, content);
            return (T) request;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private IObject getCalculationParams(String providerId) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                    Keys.getOrAdd(IObject.class.getCanonicalName()),
                    ("{" +
                            "'filter': {" +
                                "'$and': [" +
                                    "{'name': {'$eq': '" + "apelsin_" + providerId + "'}}," +
                                    "{'type': {'$eq': 'calculation'}}" +
                                "]" +
                            "}" +
                    "}").replace('\'', '"')
            );

            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    PROCESSING_COLLECTION,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            searchResult.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();

            return searchResult.get(0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
