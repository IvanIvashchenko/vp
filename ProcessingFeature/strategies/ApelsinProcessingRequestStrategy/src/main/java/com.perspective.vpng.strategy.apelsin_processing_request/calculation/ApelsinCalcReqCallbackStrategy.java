package com.perspective.vpng.strategy.apelsin_processing_request.calculation;

import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.scope.iscope.IScope;
import info.smart_tools.smartactors.scope.scope_provider.ScopeProvider;
import org.asynchttpclient.Response;

public class ApelsinCalcReqCallbackStrategy implements IResolveDependencyStrategy {

    private final IAction<Response> callback;
    private final IField responseF;
    private IObject container;

    public ApelsinCalcReqCallbackStrategy() throws ResolutionException {
        try {
            IScope scope = ScopeProvider.getCurrentScope();
            responseF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "response");
            callback = (response -> {
                try {
                    ScopeProvider.setCurrentScope(scope);
                    IObject iobjectResponse = IOC.resolve(Keys.getOrAdd("convertXmlToIObject"), new String(response.getResponseBodyAsBytes(), "windows-1251"));
                    responseF.out(container, iobjectResponse);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        container = (IObject) args[0];
        return (T) callback;
    }
}
