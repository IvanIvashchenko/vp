package com.perspective.vpng.strategy.apelsin_processing_request.transformation;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import sun.misc.BASE64Encoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.util.Collections;

public class ApelsinGetSignStrategy implements IResolveDependencyStrategy {
    private static final String APELSIN_REQUEST_ENCODING = "windows-1251";
    private static final String SIGNATURE_ALGO = "MD5withRSA";
    private static final BASE64Encoder BASE_64_ENCODER = new BASE64Encoder();

    private String keyStoreType;
    private String keyStorePath;
    private String keyStorePass;

    private IField idF;
    private IField serviceF;
    private IField signF;
    private IField accF;
    private IField checkF;
    private IField amountF;
    private IField dateF;
    private IField timeF;

    public ApelsinGetSignStrategy(IObject config) {
        try {
            IKey fieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
            this.idF = IOC.resolve(fieldKey, "id");
            this.serviceF = IOC.resolve(fieldKey, "service");
            this.signF = IOC.resolve(fieldKey, "sign");
            this.accF = IOC.resolve(fieldKey, "acc");
            this.checkF = IOC.resolve(fieldKey, "check");
            this.amountF = IOC.resolve(fieldKey, "amount");
            this.dateF = IOC.resolve(fieldKey, "date");
            this.timeF = IOC.resolve(fieldKey, "time");

            IField keyStoreTypeF = IOC.resolve(fieldKey, "processing.apelsin.ssl.keystore.type");
            this.keyStoreType = keyStoreTypeF.in(config);
            IField keyStorePathF = IOC.resolve(fieldKey, "processing.apelsin.ssl.keystore.path");
            this.keyStorePath = keyStorePathF.in(config);
            IField keyStorePassF = IOC.resolve(fieldKey, "processing.apelsin.ssl.keystore.pass");
            this.keyStorePass = keyStorePassF.in(config);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(Object... objects) throws ResolveDependencyStrategyException {
        try {
            IObject result = (IObject) objects[0];
            serviceF.out(result, "1");

            String what = (String) idF.in(result, String.class) +
                    serviceF.in(result, String.class) +
                    accF.in(result, String.class) +
                    checkF.in(result, String.class) +
                    amountF.in(result, String.class) +
                    dateF.in(result, String.class) +
                    timeF.in(result, String.class);

            String sign = generateSign(what);
            signF.out(result, sign);

            return  null;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException("Failed to generate sign for apelsin processing request", e);
        }
    }

    private String generateSign(String what) {
        try {
            byte[] textBytes = what.getBytes(APELSIN_REQUEST_ENCODING);
            Signature dsa = Signature.getInstance(SIGNATURE_ALGO);

            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            InputStream keyInput = new FileInputStream(new File(keyStorePath));
            keyStore.load(keyInput, keyStorePass.toCharArray());
            keyInput.close();

            String alias = "";
            for (String s : Collections.list(keyStore.aliases())) {
                alias = s;
            }
            Key key = keyStore.getKey(alias, keyStorePass.toCharArray());
            dsa.initSign((PrivateKey) key);
            dsa.update(textBytes);
            byte[] encryptedBytes = dsa.sign();
            return BASE_64_ENCODER.encode(encryptedBytes);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
