package com.perspective.vpng.strategy.apelsin_processing_request.transformation;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ApelsinGetDateStrategy implements IResolveDependencyStrategy {
    private DateTimeFormatter standardFormatter;
    private IField dateF;

    public ApelsinGetDateStrategy() {
        try {
            this.dateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "date");
            this.standardFormatter = IOC.resolve(Keys.getOrAdd("datetime_formatter"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(Object... objects) throws ResolveDependencyStrategyException {
        try {
            IObject result = (IObject) objects[0];
            IObject invoice = (IObject) objects[1];

            LocalDateTime invoiceDate = LocalDateTime.from(standardFormatter.parse(dateF.in(invoice)));
            String date = invoiceDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
            dateF.out(result, date);

            return null;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException(e);
        }
    }
}
