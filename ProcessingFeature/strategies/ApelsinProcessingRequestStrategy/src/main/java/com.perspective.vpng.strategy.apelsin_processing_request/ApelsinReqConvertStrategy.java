package com.perspective.vpng.strategy.apelsin_processing_request;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class ApelsinReqConvertStrategy implements IResolveDependencyStrategy {

    private final IKey convertIObjectToXmlStrategyKey;

    public ApelsinReqConvertStrategy(String convertIObjectToXmlStrategyKey) throws ResolutionException {
        this.convertIObjectToXmlStrategyKey = Keys.getOrAdd(convertIObjectToXmlStrategyKey);
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            IObject obj = (IObject) args[0];
            return (T) ("<?xml version=\"1.0\" encoding=\"windows-1251\"?>" + IOC.resolve(convertIObjectToXmlStrategyKey, obj));
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Illegal type of given argument!", ex);
        } catch (IndexOutOfBoundsException ex) {
            throw new ResolveDependencyStrategyException("Expected one argument!", ex);
        } catch (ResolutionException ex) {
            throw new ResolveDependencyStrategyException("Convert xml to IObject is failed!", ex);
        }
    }

}
