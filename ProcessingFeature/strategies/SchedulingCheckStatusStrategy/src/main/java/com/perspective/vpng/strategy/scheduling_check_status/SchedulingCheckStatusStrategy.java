package com.perspective.vpng.strategy.scheduling_check_status;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield_name.IFieldName;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.scheduler.interfaces.ISchedulerEntry;
import info.smart_tools.smartactors.scheduler.interfaces.ISchedulingStrategy;
import info.smart_tools.smartactors.scheduler.interfaces.exceptions.EntryScheduleException;
import info.smart_tools.smartactors.scheduler.interfaces.exceptions.EntryStorageAccessException;
import info.smart_tools.smartactors.scheduler.interfaces.exceptions.SchedulingStrategyExecutionException;

import java.time.*;
import java.time.format.DateTimeParseException;
import java.time.temporal.TemporalAmount;
import java.util.List;

public class SchedulingCheckStatusStrategy implements ISchedulingStrategy {
    private final IFieldName timesFN;
    private final IFieldName intervalFN;
    private final IFieldName startFN;
    private final IFieldName intervalsFN;
    private final IFieldName finalIntervalFN;

    private long nextTime(final LocalDateTime startTime, List<IObject> intervals, final long now, final long finalInterval) {
        try {
            long lStartTime = datetimeToMillis(startTime);

            if (lStartTime >= now) {
                return lStartTime;
            }

            long nextTime = lStartTime;
            for (IObject interval : intervals) {
                Integer intervalValue = (Integer) interval.getValue(intervalFN);
                Integer times = (Integer) interval.getValue(timesFN);
                for (int i = 0; i < times; i++) {
                    nextTime += intervalValue;
                    if (nextTime >= now) {
                        return nextTime;
                    }
                }
            }

            while (nextTime < now) {
                nextTime += finalInterval;
            }

            return nextTime;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private long datetimeToMillis(final LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneOffset.UTC).toInstant().toEpochMilli();
    }

    private LocalDateTime millisToDatetime(final long millis) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneOffset.UTC);
    }

    /**
     * The constructor.
     *
     * @throws ResolutionException if fails resolving dependencies
     */
    public SchedulingCheckStatusStrategy()
            throws ResolutionException {
        startFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "start");
        intervalsFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "intervals");
        timesFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "times");
        intervalFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "interval");
        finalIntervalFN = IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), "finalInterval");
    }

    @Override
    public void init(final ISchedulerEntry entry, final IObject args) throws SchedulingStrategyExecutionException {
        try {
            String start = (String) args.getValue(startFN);
            Integer finalInterval = (Integer) args.getValue(finalIntervalFN);
            LocalDateTime startTime;
            List<IObject> intervals = (List) args.getValue(intervalsFN);
            LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);

            if (start == null) {
                startTime = now;
            } else {
                startTime = LocalDateTime.parse(start);
            }

            entry.getState().setValue(startFN, startTime.toString());
            entry.getState().setValue(intervalsFN, intervals);
            entry.getState().setValue(finalIntervalFN, finalInterval);
            entry.save();

            entry.scheduleNext(nextTime(startTime, intervals, datetimeToMillis(now), finalInterval));
        } catch (ReadValueException | InvalidArgumentException | EntryScheduleException | EntryStorageAccessException
                | ChangeValueException e) {
            throw new SchedulingStrategyExecutionException("Error occurred initializing scheduler entry.", e);
        }
    }

    @Override
    public void postProcess(final ISchedulerEntry entry) throws SchedulingStrategyExecutionException {
        try {
            String start = (String) entry.getState().getValue(startFN);
            Integer finalInterval = (Integer) entry.getState().getValue(finalIntervalFN);
            List<IObject> intervals = (List) entry.getState().getValue(intervalsFN);
            LocalDateTime now = LocalDateTime.now(ZoneOffset.UTC);

            entry.scheduleNext(nextTime(LocalDateTime.parse(start), intervals, datetimeToMillis(now), finalInterval));
        } catch (ReadValueException | InvalidArgumentException | EntryScheduleException e) {
            throw new SchedulingStrategyExecutionException("Error occurred rescheduling scheduler entry.", e);
        }
    }

    @Override
    public void restore(final ISchedulerEntry entry) throws SchedulingStrategyExecutionException {
        try {
            List<IObject> intervals = (List) entry.getState().getValue(intervalsFN);
            LocalDateTime startTime = LocalDateTime.parse((String) entry.getState().getValue(startFN));
            Integer finalInterval = (Integer) entry.getState().getValue(finalIntervalFN);

            long nextTime = nextTime(startTime, intervals, System.currentTimeMillis(), finalInterval);

            entry.scheduleNext(nextTime);
        } catch (ReadValueException | InvalidArgumentException | EntryScheduleException e) {
            throw new SchedulingStrategyExecutionException("Error occurred restoring scheduler entry.", e);
        }
    }

    @Override
    public void processException(final ISchedulerEntry entry, final Throwable e) throws SchedulingStrategyExecutionException {
        try {
            entry.cancel();
        } catch (EntryStorageAccessException | EntryScheduleException ee) {
            throw new SchedulingStrategyExecutionException("Error occurred cancelling failed scheduler entry.", ee);
        }
    }
}
