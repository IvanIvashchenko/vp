package com.perspective.vpng.strategy.rapida_processing_request.payment;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.message_bus.message_bus.MessageBus;
import info.smart_tools.smartactors.scope.iscope.IScope;
import info.smart_tools.smartactors.scope.iscope_provider_container.exception.ScopeProviderException;
import info.smart_tools.smartactors.scope.scope_provider.ScopeProvider;
import org.asynchttpclient.Response;

import java.util.List;
import java.util.Map;

public class RapidaPayReqCallbackStrategy implements IResolveDependencyStrategy {

    private final IAction<Response> callback;
    private final IField messageMapIdF;
    private final IField errCodeF;
    private final IField resultF;
    private final IField paymExtIdF;
    private final IField numberF;
    private final IField invoiceF;

    private List<IObject> invoices;

    public RapidaPayReqCallbackStrategy(final Map<String, List<String>> resultStatusMap) throws ResolutionException {

        IScope scope;
        errCodeF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/ErrCode");
        resultF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/Result");
        messageMapIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "messageMapId");
        paymExtIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/PaymExtId");
        numberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "number");
        invoiceF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoice");
        try {
            scope = ScopeProvider.getCurrentScope();
        } catch (ScopeProviderException e) {
            throw new ResolutionException("Can't get current scope", e);
        }
        callback = (response -> {
            try {
                ScopeProvider.setCurrentScope(scope);
                IObject message = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                String body = response.getResponseBody();
                IObject responseObject = IOC.resolve(Keys.getOrAdd("convertXmlToIObject"), body);

                String result = resultF.in(responseObject).toString().concat(errCodeF.in(responseObject).toString());
                String messageMapId = null;
                for (String resultStatus : resultStatusMap.keySet()) {
                    List<String> results = resultStatusMap.get(resultStatus);
                    if (results.contains(result.toUpperCase())) {
                        messageMapId = resultStatus;
                        break;
                    }
                }
                IObject invoice = getInvoice(paymExtIdF.in(responseObject, String.class));
                invoiceF.out(message, invoice);
                messageMapIdF.out(message, messageMapId);
                MessageBus.send(message);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        invoices = (List<IObject>) args[0];
        return (T) callback;
    }

    private IObject getInvoice(final String number) throws ResolveDependencyStrategyException {

        try {
            for (IObject invoice : invoices) {
                if (numberF.in(invoice).equals(number)) {
                    return invoice;
                }
            }
        } catch (ReadValueException | InvalidArgumentException e) {
            throw new ResolveDependencyStrategyException(e);
        }
        throw new ResolveDependencyStrategyException("Can't find invoice with such number: " + number);
    }
}
