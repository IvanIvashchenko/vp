package com.perspective.vpng.strategy.rapida_processing_request.verification;

import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.scope.iscope.IScope;
import info.smart_tools.smartactors.scope.iscope_provider_container.exception.ScopeProviderException;
import info.smart_tools.smartactors.scope.scope_provider.ScopeProvider;
import org.asynchttpclient.Response;

import java.util.Map;

public class RapidaVerifyReqCallbackStrategy implements IResolveDependencyStrategy {

    private final IAction<Response> callback;
    private final IField errCodeF;
    private final IField descriptionF;
    private final IField paymExtIdF;
    private final IField errorCodeF;
    private final IField errorTextF;

    private Map<String, IObject> errorContainer;

    public RapidaVerifyReqCallbackStrategy() throws ResolutionException {

        IScope scope;
        errCodeF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/ErrCode");
        descriptionF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/Description");
        paymExtIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/PaymExtId");
        errorCodeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "errorCode");
        errorTextF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "errorText");
        try {
            scope = ScopeProvider.getCurrentScope();
        } catch (ScopeProviderException e) {
            throw new ResolutionException("Can't get current scope", e);
        }
        callback = (response -> {
            try {
                ScopeProvider.setCurrentScope(scope);
                String body = response.getResponseBody();
                IObject responseObject = IOC.resolve(Keys.getOrAdd("convertXmlToIObject"), body);
                if (!errCodeF.in(responseObject).equals(0)) {
                    IObject error = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                    errorCodeF.out(error, errCodeF.in(responseObject));
                    errorTextF.out(error, descriptionF.in(responseObject));
                    errorContainer.put(paymExtIdF.in(responseObject).toString(), error);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        errorContainer = (Map<String, IObject>) args[0];
        return (T) callback;
    }
}
