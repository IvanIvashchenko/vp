package com.perspective.vpng.strategy.rapida_processing_request.verification;

import com.perspective.vpng.strategy.rapida_processing_request.IRapidaProcessingConfig;
import com.perspective.vpng.strategy.rapida_processing_request.RapidaReqExtraParamStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class RapidaVerifyReqContentStrategy implements IResolveDependencyStrategy {

    private final IResolveDependencyStrategy constParamStrategy;
    private final IResolveDependencyStrategy extraParamStrategy;

    private final IField providerIdF;
    private final IField nameF;

    private final String collectionName;
    private final IPool connectionPool;

    public RapidaVerifyReqContentStrategy(final IRapidaProcessingConfig config) throws ResolutionException {

        this.collectionName = config.getCollectionName();
        Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
        this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);

        this.constParamStrategy = new RapidaVerifyReqConstParamStrategy(config);
        this.extraParamStrategy = new RapidaReqExtraParamStrategy();

        this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
        this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {

        Set<IObject> result = new HashSet<>();
        try {
            List<IObject> invoices = (List) args[0];
            List<String> providerIds = getProviderIds(invoices);
            for (IObject invoice : invoices) {
                IObject requestObj = constParamStrategy.resolve();
                List<IObject> processingParams = getProcessingParams(providerIds);
                Map<String, IObject> processingMap = associate(providerIds, processingParams);
                requestObj = extraParamStrategy.resolve(
                    invoice,
                    requestObj,
                    processingMap
                );
                result.add(requestObj);
            }

            return (T) result;
        } catch (InvalidArgumentException | ReadValueException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Error during cast to invoices list", ex);
        }
    }

    private List<String> getProviderIds(final List<IObject> invoices)
        throws ReadValueException, InvalidArgumentException {

        List<String> providerIds = new ArrayList<>(invoices.size());
        for (IObject invoice: invoices) {
            String providerId = providerIdF.in(invoice);
            if (!providerIds.contains(providerId)) {
                providerIds.add(providerId);
            }
        }

        return providerIds;
    }

    private List<IObject> getProcessingParams(final List<String> providerIds) throws ResolveDependencyStrategyException {
        List<IObject> params = new ArrayList<>();
        try (IPoolGuard guard = new PoolGuard(connectionPool)) {
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    guard.getObject(),
                    collectionName,
                    prepareSearchQuery(providerIds),
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            params.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );
            searchTask.execute();

            return params;
        } catch (ResolutionException | TaskExecutionException | PoolGuardException e) {
            throw new ResolveDependencyStrategyException("Can't get Rapida processing parameters from database", e);
        }
    }

    private IObject prepareSearchQuery(final List<String> keys) throws ResolutionException {
        String queryKeys = keys.stream().map(k -> "\"rapida_".concat(k.concat("\""))).collect(Collectors.toList()).toString();

        return IOC.resolve(
            Keys.getOrAdd(IObject.class.getCanonicalName()),
            String.format(
                "{" +
                    "\"filter\": {" +
                        "\"name\": {\"$in\": %s}" +
                    "}," +
                    "\"page\": {\"size\": %s, \"number\": 1}" +
                "}",
                queryKeys,
                keys.size()
            )
        );
    }

    private Map<String, IObject> associate(final List<String> providerIds, final List<IObject> params)
        throws ReadValueException, InvalidArgumentException {

        Map<String, IObject> container = new HashMap<>(providerIds.size());
        String providerProcessingName;
        for (String providerId: providerIds) {
            for (IObject param: params) {
                providerProcessingName = nameF.in(param);
                if (providerProcessingName.contains(providerId)) {
                    container.put(providerId, param);
                    break;
                }
            }
        }

        return container;
    }
}
