package com.perspective.vpng.strategy.rapida_processing_request.verification;

import com.perspective.vpng.strategy.rapida_processing_request.IRapidaProcessingConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class RapidaVerifyReqConstParamStrategy implements IResolveDependencyStrategy {

    private static final String FUNCTION = "check";

    private final IField functionF;
    private final IField termTypeF;
    private final IField termIdF;

    private final IRapidaProcessingConfig config;

    public RapidaVerifyReqConstParamStrategy(final IRapidaProcessingConfig config) throws ResolutionException {

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        this.functionF = IOC.resolve(fieldKey, "Function");
        this.termTypeF = IOC.resolve(fieldKey, "TermType");
        this.termIdF = IOC.resolve(fieldKey, "TermId");
        this.config = config;
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        try {
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            functionF.out(result, FUNCTION);
            termTypeF.out(result, config.getTermType());
            termIdF.out(result, config.getTermId());

            return (T) result;
        } catch (InvalidArgumentException | ChangeValueException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException(
                "Failed to add const params to Rapida verification processing request", ex);
        }
    }
}
