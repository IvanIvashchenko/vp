package com.perspective.vpng.strategy.rapida_processing_request;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Map;

public class RapidaReqHeaderStrategy implements IResolveDependencyStrategy {

    private final Map<String, Object> headers;
    private final String url;
    private final String method;

    private final IField headersF;
    private final IField methodF;
    private final IField urlF;

    public RapidaReqHeaderStrategy(final Map<String, Object> headers, final String url, final String method) throws ResolutionException {
        this.headers = headers;
        this.url = url;
        this.method = method;

        final IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        headersF = IOC.resolve(fieldKey, "headers");
        methodF = IOC.resolve(fieldKey, "method");
        urlF = IOC.resolve(fieldKey, "url");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        try {
            IObject header = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            headersF.out(header, headers);
            urlF.out(header, url);
            methodF.out(header, method);
            return (T) header;
        } catch (ResolutionException | ChangeValueException | InvalidArgumentException ex) {
            throw new ResolveDependencyStrategyException("Error during resolve headers for Rapida request", ex);
        }
    }
}
