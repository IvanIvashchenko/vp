package com.perspective.vpng.strategy.rapida_processing_request;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class RapidaReqExtraParamStrategy implements IResolveDependencyStrategy {

    private final IField paymSubjTpF;
    private final IField paramsF;

    private final IField providerIdF;
    private final IField processingParamsF;
    private final IField requestParamsF;
    private final IField nameF;
    private final IField valueF;
    private final IField codeF;
    private final IField strategyF;
    private final IField strategyParamsF;

    private final String amountFN;
    private final String feeSumFN;

    public RapidaReqExtraParamStrategy() throws ResolutionException {

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        this.paymSubjTpF = IOC.resolve(fieldKey, "PaymSubjTp");
        this.paramsF = IOC.resolve(fieldKey, "Params");

        this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
        this.processingParamsF = IOC.resolve(fieldKey, "params");
        this.requestParamsF = IOC.resolve(fieldKey, "requestParams");
        this.nameF = IOC.resolve(fieldKey, "name");
        this.valueF = IOC.resolve(fieldKey, "value");
        this.codeF = IOC.resolve(fieldKey, "code");
        this.strategyF = IOC.resolve(fieldKey, "strategy");
        this.strategyParamsF = IOC.resolve(fieldKey, "strategyParams");

        this.amountFN = "Amount";
        this.feeSumFN = "FeeSum";
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        IObject invoice = (IObject) args[0];
        IObject result = (IObject) args[1];
        Map<String, IObject> processingMap = (Map) args[2];

        try {
            IObject processingParamsObj = processingMap.get(providerIdF.in(invoice));
            List<IObject> processingParams = processingParamsF.in(processingParamsObj);
            String paymSubjTp = paymSubjTpF.in(processingParamsObj);
            paymSubjTpF.out(result, paymSubjTp);
            List<IObject> params = requestParamsF.in(processingParamsObj);
            String paramsStr = "";
            String invoiceParamFN, strategyName;
            for (IObject param : params) {
                invoiceParamFN = valueF.in(param);
                IField inPaymentField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), invoiceParamFN);
                Object invoiceValue = inPaymentField.in(invoice);
                strategyName = strategyF.in(param);
                if (strategyName != null) {
                    invoiceValue = IOC.resolve(Keys.getOrAdd(strategyName), invoiceValue, strategyParamsF.in(param));
                }
                paramsStr = paramsStr.concat(codeF.in(param)).concat(" ").concat(invoiceValue.toString());
            }
            paramsF.out(result, paramsStr);

            String paramsFN, invoiceFN;
            for (IObject param : processingParams) {

                paramsFN = nameF.in(param);
                invoiceFN = valueF.in(param);
                IField resultF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), paramsFN);
                IField inPaymentField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), invoiceFN);
                Object invoiceValue = inPaymentField.in(invoice);
                if (paramsFN.equals(amountFN) || paramsFN.equals(feeSumFN)) {
                    BigDecimal value = BigDecimal.valueOf((Double) invoiceValue);
                    value = value.multiply(BigDecimal.valueOf(100));
                    invoiceValue = value.intValueExact();
                }
                resultF.out(result, invoiceValue);
            }

            return (T) result;
        } catch (ResolutionException | ReadValueException | InvalidArgumentException | ChangeValueException e) {
            throw new ResolveDependencyStrategyException("Failed to add extra params to Rapida processing request", e);
        } catch (ArithmeticException e) {
            throw new ResolveDependencyStrategyException("Too long fraction value", e);
        }
    }
}
