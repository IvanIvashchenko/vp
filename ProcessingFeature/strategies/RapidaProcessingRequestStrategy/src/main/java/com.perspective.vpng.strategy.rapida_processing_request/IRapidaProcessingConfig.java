package com.perspective.vpng.strategy.rapida_processing_request;

public interface IRapidaProcessingConfig {

    String getTermType();
    String getTermId();
    String getDateTimeFormat();

    String getCollectionName();

}
