package com.perspective.vpng.strategy.rapida_processing_request;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class RapidaReqConvertStrategy implements IResolveDependencyStrategy {

    private final String encoding;
    private final IKey convertIObjectToUrlParamsStrategyKey;

    public RapidaReqConvertStrategy(final String convertStrategyKey, final String encoding) throws ResolutionException {
        this.encoding = encoding;
        this.convertIObjectToUrlParamsStrategyKey = Keys.getOrAdd(convertStrategyKey);
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            return IOC.resolve(convertIObjectToUrlParamsStrategyKey, args[0], encoding);
        } catch (ResolutionException ex) {
            throw new ResolveDependencyStrategyException("Can't convert IObject to url request parameters for Rapida", ex);
        }
    }
}
