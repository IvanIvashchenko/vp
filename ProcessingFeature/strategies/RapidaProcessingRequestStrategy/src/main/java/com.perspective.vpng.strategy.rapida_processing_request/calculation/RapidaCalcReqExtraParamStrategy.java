package com.perspective.vpng.strategy.rapida_processing_request.calculation;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

public class RapidaCalcReqExtraParamStrategy implements IResolveDependencyStrategy {

    private final IField paymSubjTpF;
    private final IField paramsF;

    private final IField requestParamsF;
    private final IField valueF;
    private final IField codeF;
    private final IField strategyF;
    private final IField strategyParamsF;

    public RapidaCalcReqExtraParamStrategy() throws ResolutionException {

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        this.paymSubjTpF = IOC.resolve(fieldKey, "PaymSubjTp");
        this.paramsF = IOC.resolve(fieldKey, "Params");

        this.requestParamsF = IOC.resolve(fieldKey, "requestParams");
        this.valueF = IOC.resolve(fieldKey, "value");
        this.codeF = IOC.resolve(fieldKey, "code");
        this.strategyF = IOC.resolve(fieldKey, "strategy");
        this.strategyParamsF = IOC.resolve(fieldKey, "strategyParams");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        IObject filterData = (IObject) args[0];
        IObject result = (IObject) args[1];
        IObject processingParamsObj = (IObject) args[2];

        try {
            String paymSubjTp = paymSubjTpF.in(processingParamsObj);
            paymSubjTpF.out(result, paymSubjTp);
            List<IObject> params = requestParamsF.in(processingParamsObj);
            String paramsStr = "";
            String filterDataParamFN, strategyName;
            for (IObject param : params) {
                filterDataParamFN = valueF.in(param);
                IField inFilterDataField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), filterDataParamFN);
                Object filterDataValue = inFilterDataField.in(filterData);
                strategyName = strategyF.in(param);
                if (strategyName != null) {
                    filterDataValue = IOC.resolve(Keys.getOrAdd(strategyName), filterDataValue, strategyParamsF.in(param));
                }
                paramsStr = paramsStr.concat(codeF.in(param)).concat(" ").concat(filterDataValue.toString().concat(";"));
            }
            paramsF.out(result, paramsStr);

            return (T) result;
        } catch (ResolutionException | ReadValueException | InvalidArgumentException | ChangeValueException e) {
            throw new ResolveDependencyStrategyException("Failed to add extra params to Rapida processing request", e);
        }
    }
}
