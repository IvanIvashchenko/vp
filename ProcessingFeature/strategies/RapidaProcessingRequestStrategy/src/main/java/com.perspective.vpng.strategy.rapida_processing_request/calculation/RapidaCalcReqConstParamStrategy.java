package com.perspective.vpng.strategy.rapida_processing_request.calculation;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class RapidaCalcReqConstParamStrategy implements IResolveDependencyStrategy {

    private static final String FUNCTION = "getextinfo";
    private final IField functionF;

    public RapidaCalcReqConstParamStrategy() throws ResolutionException {

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        this.functionF = IOC.resolve(fieldKey, "Function");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        try {
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            functionF.out(result, FUNCTION);

            return (T) result;
        } catch (InvalidArgumentException | ChangeValueException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException(
                    "Failed to add const params to Rapida get calculation processing request", ex);
        }
    }

}
