package com.perspective.vpng.strategy.rapida_processing_request.calculation;

import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RapidaCalcReqContentStrategy implements IResolveDependencyStrategy {

    private final IResolveDependencyStrategy constParamStrategy;
    private final IResolveDependencyStrategy extraParamStrategy;

    private final String collectionName;
    private final IPool connectionPool;

    public RapidaCalcReqContentStrategy(final String collectionName) throws ResolutionException {

        this.collectionName = collectionName;
        Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
        this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);

        this.constParamStrategy = new RapidaCalcReqConstParamStrategy();
        this.extraParamStrategy = new RapidaCalcReqExtraParamStrategy();
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {

        try {
            IObject filterData = (IObject) args[0];
            String providerId = (String) args[1];
            IObject requestObj = constParamStrategy.resolve();
            IObject processingParams = getProcessingParams(providerId);
            requestObj = extraParamStrategy.resolve(
                filterData,
                requestObj,
                processingParams
            );

            return (T) requestObj;
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Error during cast to invoices list", ex);
        }
    }

    private IObject getProcessingParams(final String providerId) throws ResolveDependencyStrategyException {
        List<IObject> params = new ArrayList<>();
        try (IPoolGuard guard = new PoolGuard(connectionPool)) {
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    guard.getObject(),
                    collectionName,
                    prepareSearchQuery(providerId),
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            params.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );
            searchTask.execute();
            if (params.isEmpty()) {
                throw new ResolveDependencyStrategyException(
                    "Can't find Rapida processing parameters for calculation for providerId " + providerId
                );
            }

            return params.get(0);
        } catch (ResolutionException | TaskExecutionException | PoolGuardException e) {
            throw new ResolveDependencyStrategyException("Can't get Rapida processing parameters from database", e);
        }
    }

    private IObject prepareSearchQuery(final String providerId) throws ResolutionException {

        return IOC.resolve(
            Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"filter\": {" +
                    "\"name\": {\"$eq\": \"rapida_" + providerId +"\"}," +
                    "\"type\": {\"$eq\": \"calculation\"}" +
                "}," +
                "\"page\": {\"size\": 1, \"number\": 1}" +
            "}"
        );
    }
}
