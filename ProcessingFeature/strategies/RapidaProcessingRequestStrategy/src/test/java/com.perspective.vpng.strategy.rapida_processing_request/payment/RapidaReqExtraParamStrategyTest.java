package com.perspective.vpng.strategy.rapida_processing_request.payment;

import com.perspective.vpng.strategy.rapida_processing_request.RapidaReqExtraParamStrategy;
import com.perspective.vpng.strategy.standard_transformation.SubstringStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.ioc_strategy_pack_plugins.resolve_standard_types_strategies_plugin.ResolveStandardTypesStrategiesPlugin;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class RapidaReqExtraParamStrategyTest {

    private IResolveDependencyStrategy strategy;

    @BeforeClass
    public static void prepareIOC() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();
        new ResolveStandardTypesStrategiesPlugin(bootstrap).load();
        bootstrap.start();

        IOC.register(Keys.getOrAdd("transformation_substring"), new SubstringStrategy());
    }

    @Before
    public void setUp() throws Exception {
        strategy = new RapidaReqExtraParamStrategy();
    }

    @Test
    public void should_ResolvePayReqExtraParam() throws Exception {
        IObject invoice = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"поставщик\": {\"id\": \"rapida_beeline\"}," +
                "\"клиент\": {\"лицевой-счет\": {\"одной-строкой\": \"+79894445588\"}}," +
                "\"всего-к-оплате\": 776.4," +
                "\"комиссия\": 77.0," +
                "\"number\": \"500034\"" +
                "}"
        );
        IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        IObject processingParams = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "    \"PaymSubjTp\": \"101\",\n" +
                "    \"requestParams\": [\n" +
                "      {\n" +
                "        \"code\": \"188\",\n" +
                "        \"value\": \"клиент/лицевой-счет/одной-строкой\"\n" +
                "      }\n" +
                "    ]," +
                "    \"name\": \"rapida_beeline\"," +
                "    \"params\": [\n" +
                "        {\n" +
                "            \"name\": \"PaymExtId\",\n" +
                "            \"value\": \"number\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"Amount\",\n" +
                "            \"value\": \"всего-к-оплате\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"FeeSum\",\n" +
                "            \"value\": \"комиссия\"\n" +
                "        }\n" +
                "    ]" +
            "}"
        );
        Map<String, IObject> processingMap = new HashMap<>();
        processingMap.put("rapida_beeline", processingParams);

        result = strategy.resolve(invoice, result, processingMap);

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        IField paymSubjTpF = IOC.resolve(fieldKey, "PaymSubjTp");
        IField paramsF = IOC.resolve(fieldKey, "Params");
        IField paymExtIdF = IOC.resolve(fieldKey, "PaymExtId");
        IField amountF = IOC.resolve(fieldKey, "Amount");
        IField feeSumF = IOC.resolve(fieldKey, "FeeSum");

        assertEquals(amountF.in(result, Integer.class), Integer.valueOf(77640));
        assertEquals(paymSubjTpF.in(result), "101");
        assertEquals(paramsF.in(result), "188 +79894445588");
        assertEquals(paymExtIdF.in(result), "500034");
        assertEquals(feeSumF.in(result, Integer.class), Integer.valueOf(7700));
    }

    @Test
    public void should_ResolvePayReqExtraParam_when_StrategyIsGiven() throws Exception {
        IObject invoice = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"поставщик\": {\"id\": \"rapida_beeline\"}," +
                "\"клиент\": {\"лицевой-счет\": {\"одной-строкой\": \"+79894445588\"}}," +
                "\"всего-к-оплате\": 776.4," +
                "\"комиссия\": 77.0," +
                "\"number\": \"500034\"" +
                "}"
        );
        IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        IObject processingParams = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "    \"PaymSubjTp\": \"101\",\n" +
                "    \"requestParams\": [\n" +
                "      {\n" +
                "        \"code\": \"188\",\n" +
                "        \"value\": \"клиент/лицевой-счет/одной-строкой\",\n" +
                "        \"strategy\": \"transformation_substring\",\n" +
                "        \"strategyParams\": {\n" +
                "          \"beginIndex\": 2,\n" +
                "          \"endIndex\": 12\n" +
                "        }" +
                "      }\n" +
                "    ]," +
                "    \"name\": \"rapida_beeline\"," +
                "    \"params\": [\n" +
                "        {\n" +
                "            \"name\": \"PaymExtId\",\n" +
                "            \"value\": \"number\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"Amount\",\n" +
                "            \"value\": \"всего-к-оплате\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"FeeSum\",\n" +
                "            \"value\": \"комиссия\"\n" +
                "        }\n" +
                "    ]" +
                "}"
        );
        Map<String, IObject> processingMap = new HashMap<>();
        processingMap.put("rapida_beeline", processingParams);

        result = strategy.resolve(invoice, result, processingMap);

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        IField paymSubjTpF = IOC.resolve(fieldKey, "PaymSubjTp");
        IField paramsF = IOC.resolve(fieldKey, "Params");
        IField paymExtIdF = IOC.resolve(fieldKey, "PaymExtId");
        IField amountF = IOC.resolve(fieldKey, "Amount");
        IField feeSumF = IOC.resolve(fieldKey, "FeeSum");

        assertEquals(amountF.in(result, Integer.class), Integer.valueOf(77640));
        assertEquals(paymSubjTpF.in(result), "101");
        assertEquals(paramsF.in(result), "188 9894445588");
        assertEquals(paymExtIdF.in(result), "500034");
        assertEquals(feeSumF.in(result, Integer.class), Integer.valueOf(7700));
    }
}
