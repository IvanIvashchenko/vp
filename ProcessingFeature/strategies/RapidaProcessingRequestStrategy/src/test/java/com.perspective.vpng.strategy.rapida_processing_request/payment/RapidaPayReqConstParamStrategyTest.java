package com.perspective.vpng.strategy.rapida_processing_request.payment;

import com.perspective.vpng.strategy.rapida_processing_request.IRapidaProcessingConfig;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class RapidaPayReqConstParamStrategyTest {

    private IResolveDependencyStrategy strategy;

    @BeforeClass
    public static void prepareIOC() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {
        IRapidaProcessingConfig config = mock(IRapidaProcessingConfig.class);
        when(config.getTermType()).thenReturn("003");
        when(config.getTermId()).thenReturn("22");
        when(config.getDateTimeFormat()).thenReturn("yyyyMMdd'T'HHmmssZ");
        strategy = new RapidaPayReqConstParamStrategy(config);
    }

    @Test
    public void should_ResolvePayReqConstParam() throws Exception {

        IObject result = strategy.resolve();

        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        IField functionF = IOC.resolve(fieldKey, "Function");
        IField termTypeF = IOC.resolve(fieldKey, "TermType");
        IField termIdF = IOC.resolve(fieldKey, "TermId");
        IField termTimeF = IOC.resolve(fieldKey, "TermTime");

        assertEquals(functionF.in(result), "payment");
        assertEquals(termTypeF.in(result), "003");
        assertEquals(termIdF.in(result), "22");
        assertNotNull(termTimeF.in(result));
    }
}
