package com.perspective.vpng.strategy.ssl_processing_request;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Optional;

public class SslProcessingRequestStrategy implements IResolveDependencyStrategy {

    private final String sslPath;
    private final String sslKey;

    public SslProcessingRequestStrategy(String sslPath, String sslKey) throws ResolveDependencyStrategyException {
        this.sslPath = Optional.ofNullable(sslPath)
                .orElseThrow(() -> new ResolveDependencyStrategyException("Ssl path is null!"));
        this.sslKey = Optional.ofNullable(sslKey)
                .orElseThrow(() -> new ResolveDependencyStrategyException("Ssl key is null!"));
        if (sslKey.isEmpty() || sslPath.isEmpty()) {
            throw new ResolveDependencyStrategyException("Ssl path or key is empty!");
        }
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            return IOC.resolve(
                    Keys.getOrAdd(IObject.class.getName()),
                    String.format(
                            "{'path': '%s', 'key': '%s'}".replaceAll("\'", "\""),
                            sslPath,
                            sslKey
                    )
            );
        } catch (ResolutionException ex) {
            throw new ResolveDependencyStrategyException("Resolution strategy error!", ex);
        }
    }

}
