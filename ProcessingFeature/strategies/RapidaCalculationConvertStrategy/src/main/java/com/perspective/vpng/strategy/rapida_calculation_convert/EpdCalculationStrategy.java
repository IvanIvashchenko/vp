package com.perspective.vpng.strategy.rapida_calculation_convert;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.field_name.FieldName;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

public class EpdCalculationStrategy implements IResolveDependencyStrategy {

    private final IField calculationObjF;
    private final IField resultF;
    private final IField descriptionF;
    private final IField nameF;
    private final IField valueF;
    private final IField columnsF;
    private final IField valuesF;
    private final IField servicesF;
    private final IField titleF;
    private final IField codeF;
    private final IField fieldF;
    private final IField serviceNameF;
    private final IField amountF;

    private final IField statusF;
    private final IField schemaF;
    private final IField errorF;

    private Map<String, String> filterFields;
    private Map<String, IAction<Object[]>> actionMaps;


    public EpdCalculationStrategy() throws ResolutionException {
        this.calculationObjF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/Data");
        this.resultF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/Result");
        this.descriptionF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Response/Description");
        this.servicesF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "Subscriber/Field/Field");
        this.titleF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "title");
        this.codeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "code");
        this.fieldF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "Field");
        this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        this.valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
        this.columnsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "columns");
        this.valuesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "values");
        this.serviceNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "наименование-услуги");
        this.amountF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "к-оплате");

        this.statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
        this.schemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schema");
        this.errorF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "error");

        this.filterFields = new HashMap<String, String>(){{
            put("лицевой-счет", "account");
            put("месяц", "month");
            put("год", "year");
        }};

        this.actionMaps = new TreeMap<String, IAction<Object[]>>(String.CASE_INSENSITIVE_ORDER) {{
            put("ERROR", (args -> {

                IObject result = (IObject) args[0];
                IObject response = (IObject) args[1];
                try {
                    String description = descriptionF.in(response);
                    if (description.startsWith("Данные о коммунальных платежах")) {
                        List<IObject> schema = (List) args[2];
                        IObject filter = (IObject) args[3];
                        List<IObject> resultSchema = new ArrayList<>();

                        for (IObject schemaEl : schema) {
                            IObject newEl = createNewElement(schemaEl, filter);

                            if (nameF.in(schemaEl).equals("услуги")) {
                                columnsF.out(newEl, columnsF.in(schemaEl));
                                List<IObject> newServices = new ArrayList<>();
                                IObject communalService = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                                serviceNameF.out(communalService, "Оплата коммунальных услуг");
                                amountF.out(communalService, BigDecimal.ZERO);
                                newServices.add(communalService);

                                addInsurance(newServices, BigDecimal.ZERO, null);
                                valuesF.out(newEl, newServices);
                            }

                            resultSchema.add(newEl);
                        }
                        statusF.out(result, "ok");
                        schemaF.out(result, resultSchema);
                    } else {
                        statusF.out(result, "error");
                        errorF.out(result, description);
                    }
                } catch (ChangeValueException | ReadValueException | ResolutionException e) {
                    throw new ActionExecuteException(e);
                }
            }));
            put("OK", (args -> {

                IObject result = (IObject) args[0];
                IObject response = (IObject) args[1];
                try {

                    IObject calculation = calculationObjF.in(response);
                    List<IObject> schema = (List) args[2];
                    IObject filter = (IObject) args[3];
                    List<IObject> resultSchema = new ArrayList<>();

                    for (IObject schemaEl : schema) {
                        IObject newEl = createNewElement(schemaEl, filter);

                        if (nameF.in(schemaEl).equals("услуги")) {
                            columnsF.out(newEl, columnsF.in(schemaEl));
                            List<IObject> newServices = new ArrayList<>();
                            try {
                                List<IObject> services = servicesF.in(calculation);
                                for (IObject service : services) {
                                    handleService(service, newServices);
                                }
                            } catch (ClassCastException e) {
                                IObject service = servicesF.in(calculation);
                                handleService(service, newServices);
                            }
                            valuesF.out(newEl, newServices);
                        }

                        resultSchema.add(newEl);
                    }
                    statusF.out(result, "ok");
                    schemaF.out(result, resultSchema);
                } catch (ReadValueException | ChangeValueException | ResolutionException e) {
                    throw new ActionExecuteException(e);
                }
            }));
        }};
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        try {
            IObject response = (IObject) args[0];
            String status = resultF.in(response);
            List<IObject> schema = (List) args[1];
            IObject filter = (IObject) args[2];
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            actionMaps.get(status).execute(new Object[] {result, response, schema, filter});

            return (T) result;
        } catch (ReadValueException | InvalidArgumentException | ResolutionException | ActionExecuteException e) {
            throw new ResolveDependencyStrategyException("Error during convert EPD calculation object to system calculation ", e);
        }
    }

    private IObject createNewElement(final IObject schemaEl, final IObject filter)
        throws ResolutionException, ReadValueException, InvalidArgumentException, ChangeValueException {

        IObject newEl = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        nameF.out(newEl, nameF.in(schemaEl));
        if (filterFields.containsKey(nameF.in(schemaEl))) {
            valueF.out(newEl, filter.getValue(new FieldName(filterFields.get(nameF.in(schemaEl)))));
        }

        return newEl;
    }

    private void addInsurance(final List<IObject> newServices, final BigDecimal insuranceValue, final String insuranceType)
        throws ResolutionException, ChangeValueException, InvalidArgumentException {

        IObject insuranceService = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        String insuranceName = insuranceType == null ? "Добровольное страхование" : "Добровольное страхование ".concat(insuranceType);
        serviceNameF.out(insuranceService, insuranceName);
        amountF.out(insuranceService, insuranceValue);
        newServices.add(insuranceService);
    }

    private void handleService(final IObject service, final List<IObject> newServices)
        throws ResolutionException, ReadValueException, InvalidArgumentException, ChangeValueException {

        IObject schemaService = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        List<IObject> subParams = fieldF.in(service);
        BigDecimal insuranceValue = BigDecimal.ZERO;
        for (IObject subParam : subParams) {
            if (Optional.ofNullable(titleF.in(subParam)).orElse("").equals("Сумма")) {
                amountF.out(schemaService, valueF.in(subParam));
                continue;
            }
            if (Optional.ofNullable(codeF.in(subParam, Integer.class)).orElse(0).equals(164)) {
                insuranceValue = valueF.in(subParam, BigDecimal.class);
                continue;
            }
            if (Optional.ofNullable(codeF.in(subParam, Integer.class)).orElse(0).equals(165)) {
                BigDecimal decimalPart = valueF.in(subParam, BigDecimal.class);
                insuranceValue = insuranceValue.add(decimalPart.divide(BigDecimal.valueOf(100), 2));
            }
        }
        serviceNameF.out(schemaService, nameF.in(service));
        newServices.add(schemaService);
        addInsurance(newServices, insuranceValue, nameF.in(service));
    }
}
