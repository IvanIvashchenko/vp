package com.perspective.vpng.strategy.apelsin_calculation_convert;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.field_name.FieldName;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LevoberejeCalculationStrategy implements IResolveDependencyStrategy{
    private Map<String, String> simpleFields;
    private IField nameF;
    private IField valueF;
    private IField valuesF;
    private IField columnsF;
    private IField servicesF;
    private IField calculationF;
    private IField serviceNameF;
    private IField toPayF;
    private IField responseF;

    private final IField statusF;
    private final IField schemaF;
    private final IField errorF;

    public LevoberejeCalculationStrategy() {
        try {
            this.simpleFields = new HashMap<String, String>(){{
                put("лицевой-счет", "account");
                put("год", "year");
                put("месяц", "month");
                put("плательщик", "fullname");
            }};
            this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
            this.valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
            this.valuesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "values");
            this.columnsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "columns");
            this.servicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "services");
            this.calculationF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "for_payment");
            this.serviceNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "наименование-услуги");
            this.toPayF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "к-оплате");
            this.responseF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "response/account_info");

            this.statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
            this.schemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schema");
            this.errorF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "error");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(Object... objects) throws ResolveDependencyStrategyException {
        try {
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IObject calc = responseF.in((IObject) objects[0]);
            List<IObject> schema = (List) objects[1];
            List<IObject> resultSchema = new ArrayList<>();

            for (IObject el : schema) {
                IObject newEl = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                nameF.out(newEl, nameF.in(el));
                if (nameF.in(el).equals("адрес-одной-строкой")) {
                    valueF.out(newEl, (String) calc.getValue(new FieldName("address")) + calc.getValue(new FieldName("flat")));
                }

                if (simpleFields.containsKey(nameF.in(el))) {
                    valueF.out(newEl, calc.getValue(new FieldName(simpleFields.get(nameF.in(el)))));
                }

                if (nameF.in(el).equals("услуги")) {
                    columnsF.out(newEl, columnsF.in(el));
                    List<IObject> services = servicesF.in(calc);
                    List<IObject> newServices = new ArrayList<>();

                    for (IObject service : services) {
                        IObject schemaService = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                        toPayF.out(schemaService, calculationF.in(service));
                        serviceNameF.out(schemaService, nameF.in(service));
                        newServices.add(schemaService);
                    }

                    valuesF.out(newEl, newServices);
                }

                resultSchema.add(newEl);
            }
            statusF.out(result, "ok");
            schemaF.out(result, resultSchema);

            return (T) result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
