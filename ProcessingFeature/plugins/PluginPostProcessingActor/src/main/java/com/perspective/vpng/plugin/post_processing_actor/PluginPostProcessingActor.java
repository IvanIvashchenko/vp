package com.perspective.vpng.plugin.post_processing_actor;

import com.perspective.vpng.actor.post_processing.PostProcessingActor;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginPostProcessingActor extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public PluginPostProcessingActor(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginPostProcessingActor")
    public void item() throws Exception {

        PostProcessingActor actor = new PostProcessingActor();
        IOC.register(Keys.getOrAdd("PostProcessingActor"), new ApplyFunctionToArgumentsStrategy((args) -> actor));
    }
}
