package com.perspective.vpng.plugin.set_processing_status_actor;

import com.perspective.vpng.actor.set_processing_status.SetProcessingStatusActor;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginSetProcessingStatusActor extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public PluginSetProcessingStatusActor(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("PluginSetProcessingStatusActor")
    public void item() throws Exception {
        IOC.register(Keys.getOrAdd("SetProcessingStatusActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> new SetProcessingStatusActor((IObject) args[0])));
    }

}