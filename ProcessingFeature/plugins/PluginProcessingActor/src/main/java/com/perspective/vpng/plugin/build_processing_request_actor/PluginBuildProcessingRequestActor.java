package com.perspective.vpng.plugin.build_processing_request_actor;

import com.perspective.vpng.actor.processing.IProcessingStrategyPack;
import com.perspective.vpng.actor.processing.ProcessingActor;
import com.perspective.vpng.actor.processing.exception.ProcessingException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

public class PluginBuildProcessingRequestActor extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public PluginBuildProcessingRequestActor(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginProcessingActor")
    public void item() throws Exception {
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        IField actionsF = IOC.resolve(fieldKey, "actions");
        IField strategiesF = IOC.resolve(fieldKey, "strategies");
        Map<String, IField> requiredFields = new HashMap<String, IField>() {{
                put("header", IOC.resolve(fieldKey, "header"));
                put("content", IOC.resolve(fieldKey, "content"));
                put("callback", IOC.resolve(fieldKey, "callback"));
                put("ssl", IOC.resolve(fieldKey, "ssl"));
                put("convert", IOC.resolve(fieldKey, "convert"));
        }};
        IOC.register(Keys.getOrAdd("ProcessingActor"), new ApplyFunctionToArgumentsStrategy((args) -> {
            try {
                IObject config = (IObject) args[0];
                List<String> actions = actionsF.in(config);
                IObject strategies = strategiesF.in(config);
                if (actions.isEmpty()) {
                    throw new RuntimeException("List of actions is empty(must contain at least one action)!");
                }
                Map<String, Map<String, String>> actionStrategies = new HashMap<>(actions.size());
                for (String action: actions) {
                    IField actionF = IOC.resolve(fieldKey, action);
                    IObject actionContainer = actionF.in(strategies);
                    if (actionContainer == null) {
                        throw new RuntimeException("Processing strategies for action[" + action + "] is not exist!");
                    }
                    Map<String, String> strategyPack = new HashMap<>(actions.size());
                    actionStrategies.put(action, strategyPack);
                    for (Map.Entry<String, IField> entry: requiredFields.entrySet()) {
                        String strategy = entry.getValue().in(actionContainer);
                        if (strategy == null){
                            throw new RuntimeException("Strategy pack for action[" + action + "] " +
                                    "doesn't contain strategy of building a request " + entry.getKey() + "!");
                        }
                        strategyPack.put(entry.getKey(), strategy);
                    }
                }

                return new ProcessingActor(actions, new IProcessingStrategyPack() {
                    @Override
                    public String getHeaderStrategy(String processing, String action) {
                        return actionStrategies.get(action).get("header") + processing;
                    }

                    @Override
                    public String getContentStrategy(String processing, String action) {
                        return actionStrategies.get(action).get("content") + processing;
                    }

                    @Override
                    public String getCallbackStrategy(String processing, String action) {
                        return actionStrategies.get(action).get("callback") + processing;
                    }

                    @Override
                    public String getSslStrategy(String processing, String action) {
                        return actionStrategies.get(action).get("ssl") + processing;
                    }

                    @Override
                    public String getConvertStrategy(String processing, String action) {
                        return actionStrategies.get(action).get("convert") + processing;
                    }
                });
            } catch (ReadValueException | ResolutionException | ProcessingException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }));
    }

}