package com.perspective.vpng.plugin.send_processing_request_actor;

import com.perspective.vpng.actor.send_processing_request.RequestSenderActor;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginRequestSenderActor extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public PluginRequestSenderActor(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("PluginRequestSenderActor")
    public void item() throws Exception {
        RequestSenderActor actor = new RequestSenderActor();
        IOC.register(Keys.getOrAdd("RequestSenderActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> actor));
    }

}