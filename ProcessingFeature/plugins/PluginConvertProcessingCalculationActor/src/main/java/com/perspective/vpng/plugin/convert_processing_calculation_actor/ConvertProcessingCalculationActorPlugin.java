package com.perspective.vpng.plugin.convert_processing_calculation_actor;

import com.perspective.vpng.actor.convert_processing_calculation.ConvertProcessingCalculationActor;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class ConvertProcessingCalculationActorPlugin extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public ConvertProcessingCalculationActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("ConvertProcessingCalculationActorPlugin")
    public void item() throws Exception {
        IOC.register(Keys.getOrAdd("ConvertProcessingCalculationActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> new ConvertProcessingCalculationActor((IObject) args[0]))
        );
    }
}
