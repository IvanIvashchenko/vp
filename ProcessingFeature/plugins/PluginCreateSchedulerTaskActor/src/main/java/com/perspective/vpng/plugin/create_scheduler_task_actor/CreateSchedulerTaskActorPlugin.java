package com.perspective.vpng.plugin.create_scheduler_task_actor;

import com.perspective.vpng.actor.create_scheduler_task.CreateSchedulerTaskActor;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class CreateSchedulerTaskActorPlugin extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public CreateSchedulerTaskActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("CreateSchedulerTaskActorPlugin")
    public void item() throws Exception {
        IOC.register(Keys.getOrAdd("CreateSchedulerTaskActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> new CreateSchedulerTaskActor((IObject) args[0]))
        );
    }
}