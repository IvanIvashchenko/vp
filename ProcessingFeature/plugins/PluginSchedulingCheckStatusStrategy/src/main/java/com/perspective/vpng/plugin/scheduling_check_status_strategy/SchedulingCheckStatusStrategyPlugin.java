package com.perspective.vpng.plugin.scheduling_check_status_strategy;

import com.perspective.vpng.strategy.scheduling_check_status.SchedulingCheckStatusStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.singleton_strategy.SingletonStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class SchedulingCheckStatusStrategyPlugin extends BootstrapPlugin {
    /**
     * The constructor.
     *
     * @param bootstrap    the bootstrap
     */
    public SchedulingCheckStatusStrategyPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    /**
     * Register strategies.
     *
     * @throws ResolutionException if error occurs resolving the keys
     * @throws RegistrationException if error occurs registering the strategies in IOC
     * @throws InvalidArgumentException if {@link SingletonStrategy} hates {@link
     *                                  info.smart_tools.smartactors.scheduler.interfaces.ISchedulingStrategy scheduling strategies}
     */
    @Item("SchedulingCheckStatusStrategyPlugin")
    public void registerSchedulingStrategies()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(
                Keys.getOrAdd("SchedulingCheckStatusStrategy"),
                new SingletonStrategy(new SchedulingCheckStatusStrategy()));
    }
}
