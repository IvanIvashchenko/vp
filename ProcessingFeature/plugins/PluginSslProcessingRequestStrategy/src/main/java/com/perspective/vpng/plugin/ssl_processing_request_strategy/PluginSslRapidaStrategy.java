package com.perspective.vpng.plugin.ssl_processing_request_strategy;

import com.perspective.vpng.strategy.ssl_processing_request.SslProcessingRequestStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginSslRapidaStrategy extends BootstrapPlugin {

    public PluginSslRapidaStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginSslRapidaStrategy")
    public void registerSslRapidaStrategy() throws Exception {
        StrategyHelper helper = StrategyHelper.getInstance();
        IOC.register(
                Keys.getOrAdd(helper.COMMON_KEY + "Rapida"),
                new SslProcessingRequestStrategy(
                        helper.getSslPath("processing.rapida.ssl.path"),
                        helper.getSslKey("processing.rapida.ssl.key")
                )
        );
    }

}
