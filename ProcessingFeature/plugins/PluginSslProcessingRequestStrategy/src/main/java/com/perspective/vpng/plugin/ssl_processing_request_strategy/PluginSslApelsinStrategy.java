package com.perspective.vpng.plugin.ssl_processing_request_strategy;

import com.perspective.vpng.strategy.ssl_processing_request.SslProcessingRequestStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginSslApelsinStrategy extends BootstrapPlugin {

    public PluginSslApelsinStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginSslApelsinStrategy")
    public void registerSslApelsinStrategy() throws Exception {
        StrategyHelper helper = StrategyHelper.getInstance();
        IOC.register(
                Keys.getOrAdd(helper.COMMON_KEY + "Apelsin"),
                new SslProcessingRequestStrategy(
                        helper.getSslPath("processing.apelsin.ssl.path"),
                        helper.getSslKey("processing.apelsin.ssl.key")
                )
        );
    }

}
