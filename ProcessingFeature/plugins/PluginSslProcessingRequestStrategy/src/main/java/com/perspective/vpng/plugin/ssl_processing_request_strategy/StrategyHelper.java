package com.perspective.vpng.plugin.ssl_processing_request_strategy;

import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Optional;

public class StrategyHelper {

    private static StrategyHelper instance = null;

    final String COMMON_KEY = "getSslProcessing";

    private final IObject config;

    private final IKey fieldKey;

    private StrategyHelper() {
        try {
            config = IOC.resolve(Keys.getOrAdd("global constants"));
            fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        } catch (ResolutionException ex) {
            throw new RuntimeException(ex);
        }
    }

    static StrategyHelper getInstance() {
        return instance != null ? instance : (instance = new StrategyHelper());
    }

    String getSslPath(String propertyName) throws Exception {
        IField sslPathF = IOC.resolve(fieldKey, propertyName);
        String sslPath = (String) Optional.ofNullable(sslPathF.in(config))
                .orElseThrow(() -> new IllegalArgumentException("SSL path is null!"));
        if (sslPath.isEmpty()) {
            throw new IllegalArgumentException("SSL path is empty!");
        }
        return sslPath;
    }

    String getSslKey(String propertyName) throws Exception {
        IField sslKeyF = IOC.resolve(fieldKey, propertyName);
        String sslKey = (String) Optional.ofNullable(sslKeyF.in(config))
                .orElseThrow(() -> new IllegalArgumentException("SSL key is null!"));
        if (sslKey.isEmpty()) {
            throw new IllegalArgumentException("SSL key is empty!");
        }
        return sslKey;
    }

}
