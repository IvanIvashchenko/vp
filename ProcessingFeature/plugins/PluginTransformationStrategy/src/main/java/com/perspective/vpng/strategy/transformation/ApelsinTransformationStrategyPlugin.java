package com.perspective.vpng.strategy.transformation;

import com.perspective.vpng.strategy.apelsin_processing_request.transformation.ApelsinGetDateStrategy;
import com.perspective.vpng.strategy.apelsin_processing_request.transformation.ApelsinGetSignStrategy;
import com.perspective.vpng.strategy.apelsin_processing_request.transformation.ApelsinGetTimeStrategy;
import info.smart_tools.smartactors.base.strategy.singleton_strategy.SingletonStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class ApelsinTransformationStrategyPlugin extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public ApelsinTransformationStrategyPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("ApelsinTransformationStrategyPlugin")
    public void item() throws Exception {
        IOC.register(Keys.getOrAdd("apelsin_getDateStrategy"),
                new SingletonStrategy(new ApelsinGetDateStrategy()));

        IOC.register(Keys.getOrAdd("apelsin_getTimeStrategy"),
                new SingletonStrategy(new ApelsinGetTimeStrategy()));

        IObject globalConfig = IOC.resolve(Keys.getOrAdd("global constants"));
        IOC.register(Keys.getOrAdd("apelsin_setSignStrategy"),
                new SingletonStrategy(new ApelsinGetSignStrategy(globalConfig)));
    }
}
