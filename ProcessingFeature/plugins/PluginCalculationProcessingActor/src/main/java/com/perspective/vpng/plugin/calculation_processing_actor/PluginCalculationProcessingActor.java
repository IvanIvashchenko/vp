package com.perspective.vpng.plugin.calculation_processing_actor;

import com.perspective.vpng.actor.calculation_processing.CalculationProcessingActor;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginCalculationProcessingActor extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public PluginCalculationProcessingActor(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginCalculationProcessingActor")
    public void item() throws Exception {
        CalculationProcessingActor actor = new CalculationProcessingActor();
        IOC.register(Keys.getOrAdd("CalculationProcessingActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> actor)
        );
    }
}
