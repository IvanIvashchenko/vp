package com.perspective.vpng.plugin.processing_request_content_strategy;

public class StrategyHelper {

    public static final String COMMON_KEY = "getReqContent";
    public static final String PAYMENT_KEY = "getPayReqContent";
    public static final String VERIFICATION_KEY = "getVerifyReqContent";
    public static final String CHECK_STATUS_KEY = "getCheckReqContent";
    public static final String CALCULATION_KEY = "getCalcReqContent";

}
