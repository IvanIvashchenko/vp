package com.perspective.vpng.plugin.processing_request_content_strategy;

import com.perspective.vpng.strategy.apelsin_processing_request.calculation.ApelsinCalcReqContentStrategy;
import com.perspective.vpng.strategy.apelsin_processing_request.payment.wrapper.IApelsinPayReqConfig;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginApelsinCalcReqContentStrategy extends BootstrapPlugin {

    public PluginApelsinCalcReqContentStrategy(IBootstrap bootstrap) throws ResolutionException {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginApelsinCalcReqContentStrategy")
    public void registerApelsinContentStrategy() throws Exception {
        IObject config = IOC.resolve(Keys.getOrAdd("global constants"));
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        IField loginF = IOC.resolve(fieldKey, "processing.apelsin.login");
        IField pointF = IOC.resolve(fieldKey, "processing.apelsin.point");
        IField dealerF = IOC.resolve(fieldKey, "processing.apelsin.dealer");
        IField softF = IOC.resolve(fieldKey, "processing.apelsin.soft");
        IField versionF = IOC.resolve(fieldKey, "processing.apelsin.version");
        IField typeF = IOC.resolve(fieldKey, "processing.apelsin.type");
        IField processingCollectionF = IOC.resolve(fieldKey, "processing.collection");

        String login = loginF.in(config);
        int point = pointF.in(config);
        int dealer = dealerF.in(config);
        String soft = softF.in(config);
        String version = versionF.in(config);
        String type = typeF.in(config);
        String processingCollection = processingCollectionF.in(config);

        IResolveDependencyStrategy strategy = new ApelsinCalcReqContentStrategy(
                new IApelsinPayReqConfig() {
                    @Override
                    public String getLogin() {
                        return login;
                    }

                    @Override
                    public int getPoint() {
                        return point;
                    }

                    @Override
                    public int getDealer() {
                        return dealer;
                    }

                    @Override
                    public String getSoft() {
                        return soft;
                    }

                    @Override
                    public String getVersion() {
                        return version;
                    }

                    @Override
                    public String getType() {
                        return type;
                    }

                    @Override
                    public String getProcessingCollection() {
                        return processingCollection;
                    }
                }
        );

        IOC.register(Keys.getOrAdd(StrategyHelper.CALCULATION_KEY + "Apelsin"), strategy);
    }
}
