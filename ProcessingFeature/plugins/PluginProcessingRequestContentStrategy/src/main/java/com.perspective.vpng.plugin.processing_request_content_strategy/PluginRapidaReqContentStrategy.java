package com.perspective.vpng.plugin.processing_request_content_strategy;

import com.perspective.vpng.strategy.rapida_processing_request.IRapidaProcessingConfig;
import com.perspective.vpng.strategy.rapida_processing_request.calculation.RapidaCalcReqContentStrategy;
import com.perspective.vpng.strategy.rapida_processing_request.check_status.RapidaCheckReqContentStrategy;
import com.perspective.vpng.strategy.rapida_processing_request.payment.RapidaPayReqContentStrategy;
import com.perspective.vpng.strategy.rapida_processing_request.verification.RapidaVerifyReqContentStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginRapidaReqContentStrategy extends BootstrapPlugin {

    public PluginRapidaReqContentStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginRapidaReqContentStrategy")
    public void registerRapidaContentStrategy() throws Exception {
        IObject globalConfig = IOC.resolve(Keys.getOrAdd("global constants"));
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        IField termTypeConfigF = IOC.resolve(fieldKey, "processing.rapida.termType");
        IField termIdConfigF = IOC.resolve(fieldKey, "processing.rapida.termId");
        IField dateTimeFormatF = IOC.resolve(fieldKey, "processing.rapida.dateTimeFormat");
        IField processingCollectionF = IOC.resolve(fieldKey, "processing.collection");

        String termType = termTypeConfigF.in(globalConfig);
        String termId = termIdConfigF.in(globalConfig);
        String dateTimeFormat = dateTimeFormatF.in(globalConfig);
        String processingCollection = processingCollectionF.in(globalConfig);

        try {
            IOC.register(Keys.getOrAdd(StrategyHelper.PAYMENT_KEY + "Rapida"), new RapidaPayReqContentStrategy(new IRapidaProcessingConfig() {
                @Override
                public String getTermType() {
                    return termType;
                }

                @Override
                public String getTermId() {
                    return termId;
                }

                @Override
                public String getDateTimeFormat() {
                    return dateTimeFormat;
                }

                @Override
                public String getCollectionName() {
                    return processingCollection;
                }
            }));
            IOC.register(Keys.getOrAdd(StrategyHelper.VERIFICATION_KEY + "Rapida"), new RapidaVerifyReqContentStrategy(new IRapidaProcessingConfig() {
                @Override
                public String getTermType() {
                    return termType;
                }

                @Override
                public String getTermId() {
                    return termId;
                }

                @Override
                public String getDateTimeFormat() {
                    return dateTimeFormat;
                }

                @Override
                public String getCollectionName() {
                    return processingCollection;
                }
            }));
            IOC.register(Keys.getOrAdd(StrategyHelper.CHECK_STATUS_KEY + "Rapida"), new RapidaCheckReqContentStrategy(processingCollection));
            IOC.register(Keys.getOrAdd(StrategyHelper.CALCULATION_KEY + "Rapida"), new RapidaCalcReqContentStrategy(processingCollection));
        } catch (Exception e) {
            throw new RegistrationException(e);
        }
    }

}
