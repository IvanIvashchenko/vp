package com.perspective.vpng.plugin.processing_request_content_strategy;

import com.perspective.vpng.strategy.moneta_processing_request.IMonetaReqConfig;
import com.perspective.vpng.strategy.moneta_processing_request.calculation.MonetaCalcReqContentStrategy;
import com.perspective.vpng.strategy.moneta_processing_request.calculation.wrapper.MonetaCalcReqConfig;
import com.perspective.vpng.strategy.moneta_processing_request.check_status.MonetaCheckReqContentStrategy;
import com.perspective.vpng.strategy.moneta_processing_request.payment.MonetaPayReqContentStrategy;
import com.perspective.vpng.strategy.moneta_processing_request.verification.MonetaVerifyReqContentStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginMonetaReqContentStrategy extends BootstrapPlugin {

    public PluginMonetaReqContentStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginMonetaReqContentStrategy")
    public void registerMonetaContentStrategy() throws Exception {

        IObject globalConfig = IOC.resolve(Keys.getOrAdd("global constants"));
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        IField payerConfigF = IOC.resolve(fieldKey, "processing.moneta.payer");
        IField paymentPasswordConfigF = IOC.resolve(fieldKey, "processing.moneta.paymentPassword");
        IField searchMethodConfigF = IOC.resolve(fieldKey, "processing.moneta.search_method");
        IField versionF = IOC.resolve(fieldKey, "processing.moneta.version");
        IField processingCollectionF = IOC.resolve(fieldKey, "processing.collection");

        String payer = payerConfigF.in(globalConfig);
        String paymentPassword = paymentPasswordConfigF.in(globalConfig);
        String processingCollection = processingCollectionF.in(globalConfig);
        String searchMethod = searchMethodConfigF.in(globalConfig);
        String version = versionF.in(globalConfig);

        IOC.register(Keys.getOrAdd(StrategyHelper.PAYMENT_KEY + "Moneta"), new MonetaPayReqContentStrategy(new IMonetaReqConfig() {
            @Override
            public String getPayer() {
                return payer;
            }

            @Override
            public String getPaymentPassword() {
                return paymentPassword;
            }

            @Override
            public String getCollectionName() {
                return processingCollection;
            }
        }));
        IOC.register(Keys.getOrAdd(StrategyHelper.VERIFICATION_KEY + "Moneta"), new MonetaVerifyReqContentStrategy(new IMonetaReqConfig() {
            @Override
            public String getPayer() {
                return payer;
            }

            @Override
            public String getPaymentPassword() {
                return paymentPassword;
            }

            @Override
            public String getCollectionName() {
                return processingCollection;
            }
        }));
        IOC.register(Keys.getOrAdd(StrategyHelper.CALCULATION_KEY + "Moneta"), new MonetaCalcReqContentStrategy(new MonetaCalcReqConfig() {
            @Override
            public String getCollectionName() {
                return processingCollection;
            }

            @Override
            public String getVersion() {
                return version;
            }

            @Override
            public String getSearchMethod() {
                return searchMethod;
            }
        }));
        IOC.register(Keys.getOrAdd(StrategyHelper.CHECK_STATUS_KEY + "Moneta"), new MonetaCheckReqContentStrategy());
    }
}
