package com.perspective.vpng.plugin.rapida_convert_calculation_strategy;

import com.perspective.vpng.strategy.rapida_calculation_convert.EpdCalculationStrategy;
import info.smart_tools.smartactors.base.strategy.singleton_strategy.SingletonStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class RapidaConvertCalculateStrategyPlugin extends BootstrapPlugin {

    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public RapidaConvertCalculateStrategyPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("RapidaConvertCalculateStrategyPlugin")
    public void item() throws Exception {
        IOC.register(Keys.getOrAdd("Rapida_convert_jkumoskvaepd"),
            new SingletonStrategy(new EpdCalculationStrategy()));
    }
}
