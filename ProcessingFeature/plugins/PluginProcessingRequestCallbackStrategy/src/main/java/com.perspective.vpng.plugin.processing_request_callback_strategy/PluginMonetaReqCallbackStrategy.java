package com.perspective.vpng.plugin.processing_request_callback_strategy;

import com.perspective.vpng.strategy.moneta_processing_request.check_status.MonetaCheckReqCallbackStrategy;
import com.perspective.vpng.strategy.moneta_processing_request.payment.MonetaPayReqCallbackStrategy;
import com.perspective.vpng.strategy.moneta_processing_request.verification.MonetaVerifyReqCallbackStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginMonetaReqCallbackStrategy extends BootstrapPlugin {

    public PluginMonetaReqCallbackStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginMonetaReqCallbackStrategy")
    public void registerMonetaCallbackStrategy() throws Exception {
        IObject message = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IOC.register(Keys.getOrAdd(StrategyHelper.PAYMENT_KEY + "Moneta"), new MonetaPayReqCallbackStrategy());
        IOC.register(Keys.getOrAdd(StrategyHelper.VERIFICATION_KEY + "Moneta"), new MonetaVerifyReqCallbackStrategy());
        IOC.register(Keys.getOrAdd(StrategyHelper.CHECK_STATUS_KEY + "Moneta"), new MonetaCheckReqCallbackStrategy());
    }

}
