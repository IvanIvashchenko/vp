package com.perspective.vpng.plugin.processing_request_callback_strategy;

import com.perspective.vpng.strategy.rapida_processing_request.calculation.RapidaCalcReqCallbackStrategy;
import com.perspective.vpng.strategy.rapida_processing_request.check_status.RapidaCheckReqCallbackStrategy;
import com.perspective.vpng.strategy.rapida_processing_request.payment.RapidaPayReqCallbackStrategy;
import com.perspective.vpng.strategy.rapida_processing_request.verification.RapidaVerifyReqCallbackStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PluginRapidaReqCallbackStrategy extends BootstrapPlugin {

    public PluginRapidaReqCallbackStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginRapidaReqCallbackStrategy")
    public void registerRapidaCallbackStrategy() throws Exception {

        IObject globalConfig = IOC.resolve(Keys.getOrAdd("global constants"));

        IField successStatusListF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processing.rapida.successStatusList");
        IField finalErrorStatusListF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processing.rapida.finalErrorStatusList");
        IField repeatErrorStatusListF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processing.rapida.repeatErrorStatusList");
        IField checkStatusListF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processing.rapida.checkStatusList");

        Map<String, List<String>> resultStatusMap = new HashMap<String, List<String>>() {{
            put("rapidaPaymentSuccess", successStatusListF.in(globalConfig));
            put("rapidaPaymentError", finalErrorStatusListF.in(globalConfig));
            put("rapidaPaymentRepeat", repeatErrorStatusListF.in(globalConfig));
            put("rapidaCheckPaymentStatus", checkStatusListF.in(globalConfig));
        }};

        IOC.register(Keys.getOrAdd(StrategyHelper.PAYMENT_KEY + "Rapida"), new RapidaPayReqCallbackStrategy(resultStatusMap));
        IOC.register(Keys.getOrAdd(StrategyHelper.VERIFICATION_KEY + "Rapida"), new RapidaVerifyReqCallbackStrategy());
        IOC.register(Keys.getOrAdd(StrategyHelper.CHECK_STATUS_KEY + "Rapida"), new RapidaCheckReqCallbackStrategy());
        IOC.register(Keys.getOrAdd(StrategyHelper.CALCULATION_KEY + "Rapida"), new RapidaCalcReqCallbackStrategy());
    }
}
