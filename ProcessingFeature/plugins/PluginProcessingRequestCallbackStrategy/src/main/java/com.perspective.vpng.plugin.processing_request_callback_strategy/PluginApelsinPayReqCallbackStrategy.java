package com.perspective.vpng.plugin.processing_request_callback_strategy;

import com.perspective.vpng.strategy.apelsin_processing_request.calculation.ApelsinCalcReqCallbackStrategy;
import com.perspective.vpng.strategy.apelsin_processing_request.check_status.ApelsinCheckReqCallbackStrategy;
import com.perspective.vpng.strategy.apelsin_processing_request.payment.ApelsinPayReqCallbackStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginApelsinPayReqCallbackStrategy extends BootstrapPlugin {

    public PluginApelsinPayReqCallbackStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginApelsinPayReqCallbackStrategy")
    public void registerApelsinCallbackStrategy() throws Exception {
        IOC.register(Keys.getOrAdd(StrategyHelper.PAYMENT_KEY + "Apelsin"), new ApelsinPayReqCallbackStrategy());
        IOC.register(Keys.getOrAdd(StrategyHelper.CALCULATION_KEY + "Apelsin"), new ApelsinCalcReqCallbackStrategy());
        IOC.register(Keys.getOrAdd(StrategyHelper.CHECK_STATUS_KEY + "Apelsin"), new ApelsinCheckReqCallbackStrategy());
    }
}
