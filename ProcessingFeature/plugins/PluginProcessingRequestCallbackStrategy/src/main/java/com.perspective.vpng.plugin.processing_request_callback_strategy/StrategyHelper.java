package com.perspective.vpng.plugin.processing_request_callback_strategy;

public class StrategyHelper {

    public static final String COMMON_KEY = "getReqCallback";
    public static final String CALCULATION_KEY = "getCalcReqCallback";
    public static final String PAYMENT_KEY = "getPayReqCallback";
    public static final String VERIFICATION_KEY = "getVerifyReqCallback";
    public static final String CHECK_STATUS_KEY = "getCheckReqCallback";

}
