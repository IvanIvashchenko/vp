package com.perspective.vpng.plugin.set_processing_id_to_invoice_actor;

import com.perspective.vpng.actor.set_processing_id_to_invoice.SetProcessingIdToInvoiceActor;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginSetProcessingIdToInvoiceActor extends BootstrapPlugin {

    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public PluginSetProcessingIdToInvoiceActor(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginSetProcessingIdToInvoiceActor")
    public void item() throws Exception {
    IOC.register(Keys.getOrAdd("SetProcessingIdToInvoiceActor"),
            new ApplyFunctionToArgumentsStrategy((args) -> new SetProcessingIdToInvoiceActor()));
    }
}
