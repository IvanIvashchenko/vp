package com.perspective.vpng.plugin.build_request_strategy;

import com.perspective.vpng.strategy.build_request.BuildGetRequestStrategy;
import com.perspective.vpng.strategy.build_request.BuildPostRequestStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginBuildRequestStrategy extends BootstrapPlugin {

    public PluginBuildRequestStrategy(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginBuildRequestStrategy")
    public void item() throws Exception {

        IOC.register(Keys.getOrAdd("buildRequest_GET"), new BuildGetRequestStrategy());
        IOC.register(Keys.getOrAdd("buildRequest_POST"), new BuildPostRequestStrategy());
    }
}
