package com.perspective.vpng.plugin.processing_request_header_strategy;

import com.perspective.vpng.strategy.apelsin_processing_request.ApelsinReqHeaderStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import sun.misc.BASE64Encoder;

import java.util.Arrays;
import java.util.HashMap;

public class PluginApelsinPayReqHeaderStrategy extends BootstrapPlugin {

    public PluginApelsinPayReqHeaderStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginApelsinPayReqHeaderStrategy")
    public void registerApelsinHeaderStrategy() throws Exception {
        IObject config = IOC.resolve(Keys.getOrAdd("global constants"));
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        IField urlF = IOC.resolve(fieldKey, "processing.apelsin.payment_url");
        IField methodF = IOC.resolve(fieldKey, "processing.apelsin.request_method");
        IField loginF = IOC.resolve(fieldKey, "processing.apelsin.login");
        IField passwordF = IOC.resolve(fieldKey, "processing.apelsin.password");

        String url = urlF.in(config);
        String method = methodF.in(config).toString().toUpperCase();
        String login = loginF.in(config);
        String password = passwordF.in(config);

        if (!Arrays.asList("POST" ,"GET").contains(method)) {
            throw new Exception("Unknown request method for apelsin processing: [" + method + "]!");
        }

        String userpassword = login + ":" + password;
        String encodedAuthorization = new BASE64Encoder().encode(userpassword.getBytes());

        IResolveDependencyStrategy strategy = new ApelsinReqHeaderStrategy(
                new HashMap<String, Object>() {{
                    put("Accept", "text/xml");
                    put("Accept-Charset", "utf-8");
                    put("Content-Type", "text/xml");
                    put("Authorization", "Basic " + encodedAuthorization);
                }},
                url,
                method
        );
        IOC.register(Keys.getOrAdd(StrategyHelper.COMMON_KEY + "Apelsin"), strategy);
    }

}
