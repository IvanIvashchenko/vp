package com.perspective.vpng.plugin.processing_request_header_strategy;

import com.perspective.vpng.strategy.rapida_processing_request.RapidaReqHeaderStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

public class PluginRapidaReqHeaderStrategy extends BootstrapPlugin {

    public PluginRapidaReqHeaderStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginRapidaReqHeaderStrategy")
    public void registerRapidaHeaderStrategy() throws Exception {

        IObject config = IOC.resolve(Keys.getOrAdd("global constants"));
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        IField urlF = IOC.resolve(fieldKey, "processing.rapida.payment_url");
        IField encodingF = IOC.resolve(fieldKey, "processing.rapida.encoding");
        IField methodF = IOC.resolve(fieldKey, "processing.rapida.request_method");

        String url = urlF.in(config);
        String method = methodF.in(config);
        String encoding = encodingF.in(config);
        Map<String, Object> headers = new HashMap<String, Object>() {{
            put("Accept", "application/xml");
            put("Accept-Charset", encoding);
        }};

        IOC.register(Keys.getOrAdd(StrategyHelper.COMMON_KEY + "Rapida"), new RapidaReqHeaderStrategy(headers, url, method));
    }

}
