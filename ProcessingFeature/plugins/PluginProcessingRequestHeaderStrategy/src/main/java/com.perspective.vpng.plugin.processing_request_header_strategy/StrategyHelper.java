package com.perspective.vpng.plugin.processing_request_header_strategy;

public class StrategyHelper {

    public static final String COMMON_KEY = "getReqHeader";
    public static final String PAYMENT_KEY = "getPayReqHeader";
    public static final String VERIFICATION_KEY = "getVerifyReqHeader";

}
