package com.perspective.vpng.plugin.processing_request_header_strategy;

import com.perspective.vpng.strategy.moneta_processing_request.MonetaReqHeaderStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

public class PluginMonetaPayReqHeaderStrategy extends BootstrapPlugin {

    public PluginMonetaPayReqHeaderStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("PluginMonetaPayReqHeaderStrategy")
    public void registerMonetaHeaderStrategy() throws Exception {

        IObject config = IOC.resolve(Keys.getOrAdd("global constants"));
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        IField urlF = IOC.resolve(fieldKey, "processing.moneta.payment_url");
        IField methodF = IOC.resolve(fieldKey, "processing.moneta.request_method");

        String url = urlF.in(config);
        String method = methodF.in(config);
        Map<String, Object> headers = new HashMap<String, Object>() {{
            put("Content-type", "application/json;charset=UTF-8");
        }};

        IOC.register(
                Keys.getOrAdd(StrategyHelper.COMMON_KEY + "Moneta"),
                new MonetaReqHeaderStrategy(headers, url, method)
        );
    }

}
