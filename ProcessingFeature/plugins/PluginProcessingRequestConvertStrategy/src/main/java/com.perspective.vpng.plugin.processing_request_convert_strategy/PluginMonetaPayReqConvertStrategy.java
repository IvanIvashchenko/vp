package com.perspective.vpng.plugin.processing_request_convert_strategy;

import com.perspective.vpng.strategy.moneta_processing_request.MonetaReqConvertStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginMonetaPayReqConvertStrategy extends BootstrapPlugin {

    public PluginMonetaPayReqConvertStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginMonetaPayReqConvertStrategy")
    public void registerMonetaConvertStrategy() throws Exception {
        IOC.register(Keys.getOrAdd(StrategyHelper.COMMON_KEY + "Moneta"), new MonetaReqConvertStrategy());
    }

}
