package com.perspective.vpng.plugin.processing_request_convert_strategy;

public class StrategyHelper {

    public static final String COMMON_KEY = "convertReqContent";
    public static final String PAYMENT_KEY = "convertPayRequestContent";
    public static final String VERIFICATION_KEY = "convertVerifyRequestContent";

}
