package com.perspective.vpng.plugin.processing_request_convert_strategy;

import com.perspective.vpng.strategy.apelsin_processing_request.ApelsinReqConvertStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginApelsinPayReqConvertStrategy extends BootstrapPlugin {

    public PluginApelsinPayReqConvertStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginApelsinPayReqConvertStrategy")
    public void registerApelsinConvertStrategy() throws Exception {
        IResolveDependencyStrategy strategy = new ApelsinReqConvertStrategy("convertIObjectToXml");
        IOC.register(Keys.getOrAdd(StrategyHelper.COMMON_KEY + "Apelsin"), strategy);
        IOC.register(Keys.getOrAdd(StrategyHelper.PAYMENT_KEY + "Apelsin"), strategy);
    }
}
