package com.perspective.vpng.plugin.processing_request_convert_strategy;

import com.perspective.vpng.strategy.rapida_processing_request.RapidaReqConvertStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class PluginRapidaReqConvertStrategy extends BootstrapPlugin {

    public PluginRapidaReqConvertStrategy(IBootstrap bootstrap) {
        super(bootstrap);
    }

    @Item("PluginRapidaReqConvertStrategy")
    public void registerRapidaConvertStrategy() throws Exception {
        IObject globalConfig = IOC.resolve(Keys.getOrAdd("global constants"));
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        IField encodingF = IOC.resolve(fieldKey, "processing.rapida.encoding");
        IOC.register(Keys.getOrAdd(StrategyHelper.COMMON_KEY + "Rapida"),
            new RapidaReqConvertStrategy("convertIObjectToUrlParams", encodingF.in(globalConfig))
        );
    }

}
