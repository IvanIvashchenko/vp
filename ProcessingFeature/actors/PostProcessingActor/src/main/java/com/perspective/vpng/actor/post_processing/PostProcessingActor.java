package com.perspective.vpng.actor.post_processing;

import com.perspective.vpng.actor.post_processing.exception.PostProcessingException;
import com.perspective.vpng.actor.post_processing.wrapper.HandleProcessingResultMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PostProcessingActor {

    private final IField numberF;

    public PostProcessingActor() throws PostProcessingException {
        try {
            this.numberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "number");
        } catch (ResolutionException e) {
            throw new PostProcessingException("Can't construct PostProcessingActor", e);
        }
    }

    public void handleVerificationResult(final HandleProcessingResultMessage message) throws PostProcessingException {

        try {
            Map<String, IObject> errorContainer = message.getCallbackParameter();
            List<IObject> invoices = message.getInvoices();
            if (!errorContainer.isEmpty()) {
                List<IObject> errors = new ArrayList<>();
                for (String number : errorContainer.keySet()) {
                    IObject error = errorContainer.get(number);
                    numberF.out(error, number);
                    errors.add(error);
                }
                //TODO:: add invalid invoice object to error?
                message.setErrors(errors);
            }
            List<IObject> validInvoices = invoices.stream().filter(invoice -> {
                try {
                    return !errorContainer.containsKey(numberF.in(invoice).toString());
                } catch (ReadValueException | InvalidArgumentException e) {
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList());
            message.setValidInvoices(validInvoices);
        } catch (ReadValueException | InvalidArgumentException | ChangeValueException e) {
            throw new PostProcessingException("Error during handle verification processing result", e);
        }
    }
}
