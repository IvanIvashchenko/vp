package com.perspective.vpng.actor.post_processing.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;
import java.util.Map;

public interface HandleProcessingResultMessage {

    List<IObject> getInvoices() throws ReadValueException;
    Map<String, IObject> getCallbackParameter() throws ReadValueException;

    void setValidInvoices(List<IObject> invoices) throws ChangeValueException;
    void setErrors(List<IObject> errors) throws ChangeValueException;
}
