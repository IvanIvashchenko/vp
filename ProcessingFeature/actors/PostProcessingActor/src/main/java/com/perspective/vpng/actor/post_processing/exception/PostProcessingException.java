package com.perspective.vpng.actor.post_processing.exception;

public class PostProcessingException extends Exception {

    public PostProcessingException(final String message) {
        super(message);
    }

    public PostProcessingException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
