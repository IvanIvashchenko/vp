package com.perspective.vpng.actor.set_processing_id_to_invoice.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface SetProcessingIdMessage {
    List<IObject>  getInvoices() throws ReadValueException;
}
