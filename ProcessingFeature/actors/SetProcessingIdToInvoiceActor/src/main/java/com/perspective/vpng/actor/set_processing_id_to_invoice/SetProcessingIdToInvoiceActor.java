package com.perspective.vpng.actor.set_processing_id_to_invoice;

import com.perspective.vpng.actor.set_processing_id_to_invoice.wrapper.SetProcessingIdMessage;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SetProcessingIdToInvoiceActor {
    private String collectionName = "user_history";
    private IField invoiceIdF;
    private IField processingIdF;
    private IPool connectionPool;

    public SetProcessingIdToInvoiceActor() {
        try {
            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            invoiceIdF = IOC.resolve(fieldKey, "invoiceId");
            processingIdF = IOC.resolve(fieldKey, "processingId");
            Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void setId(SetProcessingIdMessage message) {
        try {
            for (IObject invoiceIdObj : message.getInvoices()) {
                IObject invoice = getInvoiceFromDB(invoiceIdF.in(invoiceIdObj));
                processingIdF.out(invoice, processingIdF.in(invoiceIdObj));
                saveInvoiceInDB(invoice);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private IObject getInvoiceFromDB(Integer number) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                    Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"filter\": {\"number\": {\"$eq\": " + number + "}}, \"collectionName\": \"" + collectionName + "\"}"
            );

            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    collectionName,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            searchResult.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();
            return searchResult.get(0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void saveInvoiceInDB(IObject invoice) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            ITask upsertTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.upsert"),
                    poolGuard.getObject(),
                    collectionName,
                    invoice
            );
            upsertTask.execute();
        } catch (PoolGuardException | ResolutionException | TaskExecutionException e) {
            throw new RuntimeException("Can't execute database operation.", e);
        }
    }
}
