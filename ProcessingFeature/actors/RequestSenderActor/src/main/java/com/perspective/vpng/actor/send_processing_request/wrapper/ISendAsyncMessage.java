package com.perspective.vpng.actor.send_processing_request.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ISendAsyncMessage {

    List<IObject> getRequests() throws ReadValueException;

}
