package com.perspective.vpng.actor.send_processing_request.exception;

public class RequestSendingException extends Exception {

    public RequestSendingException(String message, Throwable cause) {
        super(message, cause);
    }
}
