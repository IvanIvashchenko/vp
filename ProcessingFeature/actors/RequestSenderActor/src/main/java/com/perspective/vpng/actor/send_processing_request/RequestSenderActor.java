package com.perspective.vpng.actor.send_processing_request;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.perspective.vpng.actor.send_processing_request.exception.RequestSendingException;
import com.perspective.vpng.actor.send_processing_request.wrapper.ISendAsyncMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.scope.iscope.IScope;
import info.smart_tools.smartactors.scope.iscope_provider_container.exception.ScopeProviderException;
import info.smart_tools.smartactors.scope.scope_provider.ScopeProvider;
import io.netty.handler.ssl.ClientAuth;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.SslProvider;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.asynchttpclient.AsyncHandler;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import org.asynchttpclient.HttpResponseBodyPart;
import org.asynchttpclient.HttpResponseHeaders;
import org.asynchttpclient.HttpResponseStatus;
import org.asynchttpclient.Request;
import org.asynchttpclient.RequestBuilder;
import org.asynchttpclient.Response;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeoutException;

public class RequestSenderActor {
    private final IField headerF;
    private final IField contentF;
    private final IField callbackF;

    private final IField urlF;
    private final IField methodF;
    private final IField headersF;

    private final IField sslF;
    private final IField sslPathF;
    private final IField sslKeyF;

    public RequestSenderActor() throws ResolutionException {
        final IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

        headerF = IOC.resolve(fieldKey, "header");
        contentF = IOC.resolve(fieldKey, "content");
        callbackF = IOC.resolve(fieldKey, "callback");

        urlF = IOC.resolve(fieldKey, "url");
        headersF = IOC.resolve(fieldKey, "headers");
        methodF = IOC.resolve(fieldKey, "method");

        sslF = IOC.resolve(fieldKey, "ssl");
        sslPathF = IOC.resolve(fieldKey, "path");
        sslKeyF = IOC.resolve(fieldKey, "key");
    }

    public void sendAsync(ISendAsyncMessage message) throws RequestSendingException {
        try {
            List<IObject> requests = message.getRequests();
            if (requests == null || requests.isEmpty()) {
                return;
            }
            for (IObject request : requests) {
                sendAsyncRequest(headerF.in(request), sslF.in(request),
                        contentF.in(request), callbackF.in(request));
            }
        } catch (ReadValueException | InvalidArgumentException ex) {
            throw new RequestSendingException(ex.getMessage(), ex);
        }
    }

    public void sendSync(ISendAsyncMessage message) throws RequestSendingException {
        try {
            List<IObject> requests = message.getRequests();
//            for (IObject request : requests) {
//                sendSyncRequest(headerF.in(request), sslF.in(request), contentF.in(request), callbackF.in(request));
//            }
            //TODO:: refactor
            ForkJoinPool forkJoinPool = new ForkJoinPool(4);
            IScope scope = ScopeProvider.getCurrentScope();
            forkJoinPool.submit(() ->
                requests.parallelStream().forEach(request -> {
                    try {
                        ScopeProvider.setCurrentScope(scope);
                        this.sendSyncRequest(headerF.in(request), sslF.in(request), contentF.in(request), callbackF.in(request));
                    } catch (RequestSendingException | ReadValueException | InvalidArgumentException | ScopeProviderException e) {
                        throw new RuntimeException(e);
                    }
                })
            ).get();
        } catch (ReadValueException | InterruptedException | ExecutionException | ScopeProviderException ex) {
            throw new RequestSendingException(ex.getMessage(), ex);
        }
    }

    private void sendSyncRequest(final IObject header, final IObject ssl, String content, IAction callback) throws RequestSendingException {

        try {
            Map<String, Object> headers = headersF.in(header);
            String method = methodF.in(header);
            RequestBuilder builder = new RequestBuilder(method);
            for (Map.Entry<String, Object> _header : headers.entrySet()) {
                builder.addHeader(_header.getKey(), _header.getValue());
            }
            Request request = IOC.resolve(Keys.getOrAdd("buildRequest_" + method), builder, urlF.in(header), content);

            AsyncHttpClient client = createClient(sslKeyF.in(ssl), sslPathF.in(ssl));
            Response response = client.executeRequest(request).get();
            callback.execute(response);
        } catch (Exception ex) {
            throw new RequestSendingException(ex.getMessage(), ex);
        }
    }

    private void sendAsyncRequest(final IObject header, final IObject ssl, String content, IAction callback)
            throws RequestSendingException {
        try {
            Map<String, Object> headers = headersF.in(header);
            String method = methodF.in(header);
            RequestBuilder builder = new RequestBuilder(method);
            for (Map.Entry<String, Object> _header : headers.entrySet()) {
                builder.addHeader(_header.getKey(), _header.getValue());
            }
            Request request = IOC.resolve(Keys.getOrAdd("buildRequest_" + method), builder, urlF.in(header), content);

            AsyncHttpClient client = createClient(sslKeyF.in(ssl), sslPathF.in(ssl));
            client.executeRequest(request, new AsyncHandler<Response>() {
                private final Response.ResponseBuilder builder =
                        new Response.ResponseBuilder();

                @Override
                public void onThrowable(Throwable throwable) {
                    try {
                        System.out.println(throwable.getMessage());
                        client.close();
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }

                @Override
                public Response onCompleted() throws Exception {
                    client.close();
                    callback.execute(builder.build());
                    return null;
                }

                @Override
                public State onHeadersReceived(final HttpResponseHeaders headers)
                        throws Exception {
                    builder.accumulate(headers);
                    return State.CONTINUE;
                }

                @Override
                public State onBodyPartReceived(final HttpResponseBodyPart content)
                        throws Exception {
                    builder.accumulate(content);
                    return State.CONTINUE;
                }

                @Override
                public State onStatusReceived(final HttpResponseStatus status)
                        throws Exception {
                    builder.accumulate(status);
                    return State.CONTINUE;
                }
            });
        } catch (Exception ex) {
            throw new RequestSendingException(ex.getMessage(), ex);
        }
    }

    private AsyncHttpClient createClient(String certKey, String certPath) {
        try {
            SslContext sslContext = SslContextBuilder
                    .forClient()
                    .keyManager(new File(certPath), new File(certKey))
                    .sslProvider(SslProvider.JDK)
                    .trustManager(InsecureTrustManagerFactory.INSTANCE)
                    .clientAuth(ClientAuth.OPTIONAL)
                    .build();
            DefaultAsyncHttpClientConfig.Builder builder = new DefaultAsyncHttpClientConfig.Builder();
            builder.setSslContext(sslContext).setRequestTimeout(10 * 1000);

            return new DefaultAsyncHttpClient(builder.build());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


//    private static final int MYTHREADS = 30;
//
//    public static void main(String args[]) throws Exception {
//        ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
////        String[] hostList = { "https://crunchify.com", "http://yahoo.com",
////            "http://www.ebay.com", "http://google.com",
////            "http://www.example.co", "https://paypal.com",
////            "http://bing.com/", "http://techcrunch.com/",
////            "http://mashable.com/", "http://thenextweb.com/",
////            "http://wordpress.com/", "http://wordpress.org/",
////            "http://example.com/", "http://sjsu.edu/",
////            "http://ebay.co.uk/", "http://google.co.uk/",
////            "http://www.wikipedia.org/",
////            "http://en.wikipedia.org/wiki/Main_Page" };
//
//        for (int i = 0; i < 3; i++) {
//            String url = "https://jsonplaceholder.typicode.com/posts/" + i;
//
//            Runnable worker = new MyRunnable(url);
//            executor.execute(worker);
//        }
//        executor.shutdown();
//        // Wait until all threads are finish
//        while (!executor.isTerminated()) {
//
//        }
//        System.out.println("\nFinished all threads");
//    }
//
//    public static class MyRunnable implements Runnable {
//        private final String url;
//
//        MyRunnable(String url) {
//            this.url = url;
//        }
//
//        @Override
//        public void run() {
//
//            String result = "";
//            int code = 200;
//            try {
//                URL siteURL = new URL(url);
//                HttpURLConnection connection = (HttpURLConnection) siteURL
//                    .openConnection();
//                connection.setRequestMethod("GET");
//                connection.connect();
//
//                code = connection.getResponseCode();
//                if (code == 200) {
//                    result = "Green\t";
//                }
//            } catch (Exception e) {
//                result = "->Red<-\t";
//            }
//            System.out.println(url + "\t\tStatus:" + result);
//        }
//    }


    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {

        List<Map<String, Object>> result = new ArrayList<>();
        List<Integer> numbers = Arrays.asList(1, 2, 3);
        ForkJoinPool forkJoinPool = new ForkJoinPool(3);
        forkJoinPool.submit(() ->
            numbers.forEach(number -> {
                try {
                    DefaultAsyncHttpClientConfig.Builder builder = new DefaultAsyncHttpClientConfig.Builder();
                    builder.setRequestTimeout(10 * 1000);

                    AsyncHttpClient client = new DefaultAsyncHttpClient(builder.build());


                    RequestBuilder requestBuilder = new RequestBuilder("GET");
                    String url = "https://jsonplaceholder.typicode.com/posts/" + number;

                    Request request = requestBuilder.setUrl(url).build();


                    Response response = client.executeRequest(request).get();
                    Map<String, Object> responseMap = new HashMap<>();


                    ObjectMapper mapper = new ObjectMapper();
                    String json = response.getResponseBody();


                    responseMap = mapper.readValue(json, new TypeReference<Map<String, String>>(){});

                    result.add(responseMap);
                } catch (InterruptedException | ExecutionException e) {
                    throw new RuntimeException(e);
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            })
        ).get();
        int i = 1;
    }
}
