package com.perspective.vpng.actor.set_processing_status;

import com.perspective.vpng.actor.set_processing_status.wrapper.SetStatusMessage;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SetProcessingStatusActor {
    private String historyCollectionName;
    private IPool connectionPool;
    private IField statusF;
    private IField processingIdF;

    public SetProcessingStatusActor(IObject config) {
        try {
            Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
            IField historyCollectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "historyCollectionName");
            this.historyCollectionName = historyCollectionNameF.in(config);
            this.statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
            this.processingIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "processingId");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void setStatus(SetStatusMessage message) {
        try {
            List<IObject> invoiceStatuses = message.getInvoices();
            List<Integer> ids = invoiceStatuses.stream()
                    .map(obj -> {
                        try {
                            return (Integer) processingIdF.in(obj);
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .collect(Collectors.toList());
            List<IObject> invoices = getInvoicesFromDB(ids);

            for (IObject invoice : invoices) {
                for (IObject invoiceStatus : invoiceStatuses) {
                    if (processingIdF.in(invoice).equals(processingIdF.in(invoiceStatus))) {
                        statusF.out(invoice, statusF.in(invoiceStatus));
                        saveInvoiceInDB(invoice);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<IObject> getInvoicesFromDB(List<Integer> ids) {
        try {
            IObject searchQuery = prepareSearchQuery(ids);

            List<IObject> items = new LinkedList<>();
            try (IPoolGuard guard = new PoolGuard(connectionPool)) {
                ITask searchTask = IOC.resolve(
                        Keys.getOrAdd("db.collection.search"),
                        guard.getObject(),
                        historyCollectionName,
                        searchQuery,
                        (IAction<IObject[]>) foundDocs -> {
                            try {
                                items.addAll(Arrays.asList(foundDocs));
                            } catch (Exception e) {
                                throw new ActionExecuteException(e);
                            }
                        }
                );
                searchTask.execute();
            } catch (PoolGuardException e) {
                throw new RuntimeException(e);
            }
            return items;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void saveInvoiceInDB(IObject invoice) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            ITask upsertTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.upsert"),
                    poolGuard.getObject(),
                    historyCollectionName,
                    invoice
            );
            upsertTask.execute();
        } catch (PoolGuardException | ResolutionException | TaskExecutionException e) {
            throw new RuntimeException("Can't execute database operation.", e);
        }
    }

    private IObject prepareSearchQuery(List<Integer> ids) {
        try {
            String idsString = ids.stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(", "));

            String template =
                    "{" +
                        "\"filter\": {" +
                            "\"processingId\": {\"$in\": [" + idsString + "]}" +
                        "}," +
                            //TODO :: maybe move to config?
                        "\"page\": {\"size\": 100, \"number\": 1}" +
                    "}";

            return IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), template);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
