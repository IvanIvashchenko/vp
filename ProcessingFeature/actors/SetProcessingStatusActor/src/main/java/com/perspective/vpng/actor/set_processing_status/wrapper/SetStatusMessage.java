package com.perspective.vpng.actor.set_processing_status.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface SetStatusMessage {
    List<IObject> getInvoices() throws ReadValueException;
}
