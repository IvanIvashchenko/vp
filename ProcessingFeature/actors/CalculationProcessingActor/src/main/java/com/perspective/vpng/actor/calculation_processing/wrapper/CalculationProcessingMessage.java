package com.perspective.vpng.actor.calculation_processing.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;
import java.util.Map;

public interface CalculationProcessingMessage {
    IObject getCalculationFilter() throws ReadValueException;
    String getProviderId() throws ReadValueException;
    String getProcessingName() throws ReadValueException;
    void setRequests(List<IObject> req) throws ChangeValueException;

    String getReqHeaderStrategy() throws ReadValueException;

    String getReqContentStrategy() throws ReadValueException;

    String getReqCallbackStrategy() throws ReadValueException;

    String getSslStrategy() throws ReadValueException;

    String getReqConvertStrategy() throws ReadValueException;

    void setCallbackParameter(IObject container) throws ChangeValueException;
}
