package com.perspective.vpng.actor.calculation_processing.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CheckIsProcessingCalculationMessage {
    String getProviderId() throws ReadValueException;
    void setFlag(Boolean flag) throws ChangeValueException;
    void setProcessingName(String name) throws ChangeValueException;
}
