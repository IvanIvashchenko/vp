package com.perspective.vpng.actor.calculation_processing;

import com.perspective.vpng.actor.calculation_processing.wrapper.CalculationProcessingMessage;
import com.perspective.vpng.actor.calculation_processing.wrapper.CheckIsProcessingCalculationMessage;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.Collections;

public class CalculationProcessingActor {
    private ICachedCollection providerCollection;
    private final IField requestHeaderF;
    private final IField requestDataF;
    private final IField requestCallbackF;
    private final IField sslF;
    private final IField processingF;
    private final IField calculationF;

    public CalculationProcessingActor() {
        try {
            this.providerCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), "provider", "name");

            final IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            requestHeaderF = IOC.resolve(fieldKey, "header");
            requestDataF = IOC.resolve(fieldKey, "content");
            requestCallbackF = IOC.resolve(fieldKey, "callback");
            sslF = IOC.resolve(fieldKey, "ssl");
            processingF = IOC.resolve(fieldKey, "processing");
            calculationF = IOC.resolve(fieldKey, "calculation");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public void buildRequest(CalculationProcessingMessage message) {
        try {
            IKey headerStrategyKey, contentStrategyKey,
                    callbackStrategyKey, convertStrategyKey, sslStrategyKey;
            IObject request = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            String providerId = message.getProviderId();
            String processingName = message.getProcessingName();
            IObject filter = message.getCalculationFilter();
            IObject errorContainer = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            message.setCallbackParameter(errorContainer);

            headerStrategyKey = Keys.getOrAdd(message.getReqHeaderStrategy() + processingName);
            contentStrategyKey = Keys.getOrAdd(message.getReqContentStrategy() + processingName);
            callbackStrategyKey = Keys.getOrAdd(message.getReqCallbackStrategy() + processingName);
            sslStrategyKey = Keys.getOrAdd(message.getSslStrategy() + processingName);
            convertStrategyKey = Keys.getOrAdd(message.getReqConvertStrategy() + processingName);

            IObject content = IOC.resolve(contentStrategyKey, filter, providerId);
            String stringContent = IOC.resolve(convertStrategyKey, content);
            requestHeaderF.out(request, IOC.resolve(headerStrategyKey, stringContent.getBytes().length));
            requestDataF.out(request, stringContent);
            requestCallbackF.out(request, IOC.resolve(callbackStrategyKey, errorContainer));
            sslF.out(request, IOC.resolve(sslStrategyKey));

            message.setRequests(Collections.singletonList(request));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void checkIsProcessingCalc(CheckIsProcessingCalculationMessage message) {
        try {
            String providerId = message.getProviderId();
            IObject provider = providerCollection.getItems(providerId).get(0);
            IObject processingObj = processingF.in(provider);

            if (processingObj == null || calculationF.in(processingObj) == null || calculationF.in(processingObj).equals("none")) {
                message.setFlag(false);
                return;
            }
            message.setFlag(true);
            message.setProcessingName(calculationF.in(processingObj));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
