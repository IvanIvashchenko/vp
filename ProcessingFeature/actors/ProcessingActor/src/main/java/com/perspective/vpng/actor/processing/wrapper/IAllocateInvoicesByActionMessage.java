package com.perspective.vpng.actor.processing.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface IAllocateInvoicesByActionMessage {

    String getAction() throws ReadValueException;

    List<IObject> getProviders() throws ReadValueException;

    List<IObject> getInvoices() throws ReadValueException;

    void setAllocatedInvoices(List<IObject> invoices) throws ChangeValueException;

    void setFlagOfAllocatedNotEmpty(boolean flag) throws ChangeValueException;

}
