package com.perspective.vpng.actor.processing.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;
import java.util.Map;

public interface IBuildRequestMessage {

    List<IObject> getInvoices() throws ReadValueException;

    List<IObject> getProviders() throws ReadValueException;

    String getProcessingAction() throws ReadValueException;

    String getMode() throws ReadValueException;

    Object getCallbackParameters() throws ReadValueException;

    void setRequests(List<IObject> requests) throws ChangeValueException;

    void setCallbackParameter(Map<String, IObject> callbackParameter) throws ChangeValueException;

}
