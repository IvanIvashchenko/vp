package com.perspective.vpng.actor.processing.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface IBuildCheckStatusRequestMessage {
    String getProcessing() throws ReadValueException;
    String getAction() throws ReadValueException;
    List<IObject> getInvoices() throws ReadValueException;
    void setRequests(List<IObject> requests) throws ChangeValueException;
}
