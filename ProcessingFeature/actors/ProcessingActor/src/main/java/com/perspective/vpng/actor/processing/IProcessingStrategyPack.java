package com.perspective.vpng.actor.processing;

public interface IProcessingStrategyPack {

    String getHeaderStrategy(String processing, String action);

    String getContentStrategy(String processing, String action);

    String getCallbackStrategy(String processing, String action);

    String getSslStrategy(String processing, String action);

    String getConvertStrategy(String processing, String action);

}
