package com.perspective.vpng.actor.processing;

import com.perspective.vpng.actor.processing.exception.ProcessingException;
import com.perspective.vpng.actor.processing.wrapper.IAllocateInvoicesByActionMessage;
import com.perspective.vpng.actor.processing.wrapper.IBuildCheckStatusRequestMessage;
import com.perspective.vpng.actor.processing.wrapper.IBuildRequestMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ProcessingActor {

    private final IProcessingStrategyPack strategyPack;
    private final Map<String, IRequestBuildingStrategy> requestBuildingStrategies;
    private final List<String> actions;
    private final List<String> modes;

    private final IKey iObjKey;

    private final IField providerIdF;
    private final IField providerIdInvoiceF;
    private final IField processingF;

    private final IField requestHeaderF;
    private final IField requestDataF;
    private final IField requestCallbackF;
    private final IField sslF;

    public ProcessingActor(
            final List<String> processingActions,
            IProcessingStrategyPack strategyPack
    ) throws ProcessingException {
        try {
            this.strategyPack = strategyPack;
            requestBuildingStrategies = new HashMap<String, IRequestBuildingStrategy>() {{
                put("ignore", (processing, action, invoices, container, requests) -> {
                    if (processing == null || processing.equals("none")) {
                        return;
                    }
                    buildRequest(processing, action, invoices, container, requests);
                });
                put("alert", (processing, action, invoices, container, requests) -> {
                    if (processing == null || processing.equals("none")) {
                        throw new ProcessingException("Provider of the invoice doesn't support " +
                                "the action[" + action + "] of processing!");
                    }
                    buildRequest(processing, action, invoices, container, requests);
                });
            }};
            actions = processingActions;
            modes = Arrays.asList("ignore", "alert");

            iObjKey = Keys.getOrAdd(IObject.class.getCanonicalName());
            final IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());

            providerIdF = IOC.resolve(fieldKey, "name");
            providerIdInvoiceF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
            processingF = IOC.resolve(fieldKey, "processing");

            requestHeaderF = IOC.resolve(fieldKey, "header");
            requestDataF = IOC.resolve(fieldKey, "content");
            requestCallbackF = IOC.resolve(fieldKey, "callback");
            sslF = IOC.resolve(fieldKey, "ssl");
        } catch (ResolutionException ex) {
            throw new ProcessingException(ex.getMessage(), ex);
        }
    }

    private interface IRequestBuildingStrategy {
        void buildRequest(
                String processing,
                String action,
                List<IObject> invoices,
                Object container,
                List<IObject> requests
        ) throws ProcessingException, ResolutionException,
                ChangeValueException, InvalidArgumentException;
    }

    public void buildRequest(IBuildRequestMessage message) throws ProcessingException {
        try {
            List<IObject> invoices = message.getInvoices();
            if (invoices == null || invoices.isEmpty()) {
                message.setRequests(Collections.emptyList());
                return;
            }
            String action = message.getProcessingAction();
            if (action == null || !actions.contains(action)) {
                throw new ProcessingException("Illegal given processing action: [" + action + "]!");
            }
            String mode = message.getMode();
            mode = mode != null && modes.contains(mode) ? mode : "ignore";
            Map<String, List<IObject>> invoicesGroups = groupByProviderId(invoices);
            Map<String, IObject> providers = associateProviderById(message.getProviders());
            Object container = message.getCallbackParameters();
            IRequestBuildingStrategy buildingStrategy = requestBuildingStrategies.get(mode);
            IField actionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), action);
            List<IObject> requests = new ArrayList<>(invoicesGroups.size());

            String actionProcessing;
            IObject provider, processing;
            IObject emptyProcessing = IOC.resolve(iObjKey);
            for (Map.Entry<String, List<IObject>> group: invoicesGroups.entrySet()) {
                provider = Optional.ofNullable(providers.get(group.getKey()))
                        .orElseThrow(() -> new ProcessingException("Given list of providers " +
                                "doesn't contain provider with id:[" + group.getKey() + "]!"));
                processing = Optional.ofNullable((IObject) processingF.in(provider))
                        .orElse(emptyProcessing);
                actionProcessing = actionF.in(processing);
                buildingStrategy.buildRequest(actionProcessing, action, invoices, container, requests);
            }
            message.setRequests(requests);
        } catch (InvalidArgumentException | ReadValueException |
                ChangeValueException | ResolutionException ex) {
            throw new ProcessingException(ex.getMessage(), ex);
        } catch (Exception ex) {
            throw new ProcessingException(ex);
        }
    }

    public void allocateInvoicesByAction(IAllocateInvoicesByActionMessage message) throws ProcessingException {
        try {
            String action = message.getAction();
            IField actionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), action);
            Map<String, IObject>  providers = associateProviderById(message.getProviders());
            List<IObject> invoices = message.getInvoices();
            List<IObject> result = new ArrayList<>(invoices.size());

            String procActionVal;
            IObject provider, processing;
            for (IObject invoice: invoices) {
                final String providerId = providerIdInvoiceF.in(invoice);
                provider = Optional.ofNullable(providers.get(providerId))
                        .orElseThrow(() -> new ProcessingException("Given list of providers " +
                                "doesn't contain provider with id:[" + providerId + "]!"));
                processing = processingF.in(provider);
                if (processing != null) {
                    procActionVal = actionF.in(processing);
                    if (procActionVal != null && procActionVal.equals(action)) {
                        result.add(invoice);
                    }
                }
            }
            message.setAllocatedInvoices(result);
        } catch (ReadValueException | ChangeValueException |
                InvalidArgumentException | ResolutionException ex) {
            throw new ProcessingException("Error allocated invoices by processing action!", ex);
        } catch (NullPointerException ex) {
            throw new ProcessingException("Invalid format of given message!");
        }
    }

    public void addParamContainer(final IBuildRequestMessage message) throws ProcessingException {

        ConcurrentMap<String, IObject> errorContainer = new ConcurrentHashMap<>();
        try {
            message.setCallbackParameter(errorContainer);
        } catch (ChangeValueException e) {
            throw new ProcessingException("Error during prepare processing operation!", e);
        }
    }

    public void buildCheckStatusRequest(IBuildCheckStatusRequestMessage message) throws ProcessingException {
        try {
            List<IObject> requests = new ArrayList<>();
            buildRequest(message.getProcessing(), message.getAction(), message.getInvoices(), null, requests);
            message.setRequests(requests);
        } catch (Exception e) {
            throw new ProcessingException("Failed to build request for check status", e);
        }
    }

    private void buildRequest(
            String processing,
            String action,
            List<IObject> invoices,
            Object container,
            List<IObject> requests
    ) throws ResolutionException, ChangeValueException, InvalidArgumentException {
        IKey headerStrategyKey = Keys.getOrAdd(strategyPack.getHeaderStrategy(processing, action));
        IKey contentStrategyKey = Keys.getOrAdd(strategyPack.getContentStrategy(processing, action));
        IKey callbackStrategyKey = Keys.getOrAdd(strategyPack.getCallbackStrategy(processing, action));
        IKey sslStrategyKey = Keys.getOrAdd(strategyPack.getSslStrategy(processing, action));
        IKey convertStrategyKey = Keys.getOrAdd(strategyPack.getConvertStrategy(processing, action));

        IObject request;
        String content;
        Set<IObject> dataset = IOC.resolve(contentStrategyKey, invoices);
        for (IObject data : dataset) {
            request = IOC.resolve(iObjKey);
            content = IOC.resolve(convertStrategyKey, data);

            requestHeaderF.out(request, IOC.resolve(headerStrategyKey, content.getBytes().length));
            requestDataF.out(request, content);
            requestCallbackF.out(request, IOC.resolve(callbackStrategyKey, container));
            sslF.out(request, IOC.resolve(sslStrategyKey));

            requests.add(request);
        }
    }

    private Map<String, IObject> associateProviderById(final List<IObject> providers)
            throws ReadValueException, InvalidArgumentException {

        Map<String, IObject> groups = new HashMap<>(providers.size());
        for (IObject provider: providers) {
            groups.put(providerIdF.in(provider), provider);
        }
        return groups;
    }

    private Map<String, List<IObject>> groupByProviderId(List<IObject> invoices)
            throws ReadValueException, InvalidArgumentException {

        Map<String, List<IObject>> groups = new HashMap<>(invoices.size());
        for (IObject invoice: invoices) {
            groups.computeIfAbsent(
                    providerIdInvoiceF.in(invoice),
                    key -> new ArrayList<>()
            ).add(invoice);
        }
        return groups;
    }
}
