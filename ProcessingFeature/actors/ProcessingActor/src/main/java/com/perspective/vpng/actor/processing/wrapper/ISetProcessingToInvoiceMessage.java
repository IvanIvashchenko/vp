package com.perspective.vpng.actor.processing.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ISetProcessingToInvoiceMessage {

    List<IObject> getInvoices() throws ReadValueException;

    List<IObject> getProviders() throws ReadValueException;

}
