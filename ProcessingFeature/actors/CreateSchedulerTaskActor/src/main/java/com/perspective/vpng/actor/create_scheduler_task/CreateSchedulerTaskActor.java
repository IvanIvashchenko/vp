package com.perspective.vpng.actor.create_scheduler_task;

import com.perspective.vpng.actor.create_scheduler_task.wrapper.CreateTaskMessage;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

public class CreateSchedulerTaskActor {
    private IField messageF;
    private IField idsF;
    private IField processingNameF;
    private IField messageMapIdF;
    private String taskTemplate =
            ("{" +
                    "'strategy': 'SchedulingCheckStatusStrategy'," +
                    "'intervals': [" +
                        "{" +
                            "'interval': 30000," +
                            "'times': 1" +
                        "}" +
                    "]," +
                    "'finalInterval': 3600000" +
            "}").replace('\'', '"');
    private final String SEND_CHECK_STATUS_MAP = "sendCheckProcessingStatusMap";

    public CreateSchedulerTaskActor(IObject config) {
        try {
            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            this.messageMapIdF = IOC.resolve(fieldKey, "messageMapId");
            this.idsF = IOC.resolve(fieldKey, "ids");
            this.processingNameF = IOC.resolve(fieldKey, "processingName");
            this.messageF = IOC.resolve(fieldKey, "message");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void createTask(CreateTaskMessage message) {
        try {
            List<IObject> ids = message.getPaymentIds();
            String processingName = message.getProcessingName();
            IObject task = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), taskTemplate);
            IObject taskMessage = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            idsF.out(taskMessage, ids);
            processingNameF.out(taskMessage, processingName);
            messageMapIdF.out(taskMessage, SEND_CHECK_STATUS_MAP);
            messageF.out(task, taskMessage);
            message.setTask(task);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
