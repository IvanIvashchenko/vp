package com.perspective.vpng.actor.create_scheduler_task.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface CreateTaskMessage {
    List<IObject> getPaymentIds() throws ReadValueException;
    String getProcessingName() throws ReadValueException;
    void setTask(IObject task) throws ChangeValueException;
}
