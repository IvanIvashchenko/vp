package com.perspective.vpng.actor.convert_processing_calculation.exception;

/**
 * Exception for wrap general errors (smth like ReadValue/ChangeValue etc)
 */
public class GeneralConvertCalculationException extends Exception {

    public GeneralConvertCalculationException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
