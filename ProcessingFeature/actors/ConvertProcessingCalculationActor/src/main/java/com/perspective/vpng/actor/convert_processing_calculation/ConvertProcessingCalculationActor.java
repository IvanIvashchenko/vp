package com.perspective.vpng.actor.convert_processing_calculation;

import com.perspective.vpng.actor.convert_processing_calculation.exception.GeneralConvertCalculationException;
import com.perspective.vpng.actor.convert_processing_calculation.exception.ProcessingCalculationException;
import com.perspective.vpng.actor.convert_processing_calculation.wrapper.ConvertProcessingCalculationMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

public class ConvertProcessingCalculationActor {
    private IField schemasF;
    private ICachedCollection providerCollection;

    private final IField statusF;
    private final IField schemaF;
    private final IField errorF;

    private static final String SUCCESS_STATUS = "ok";

    public ConvertProcessingCalculationActor(final IObject config) {
        try {
            IField providerCollectionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerCollection");
            this.providerCollection = IOC.resolve(
                    Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), providerCollectionF.in(config), "name"
            );
            this.schemasF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schemas");

            this.statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
            this.schemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schema");
            this.errorF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "error");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void convert(ConvertProcessingCalculationMessage message)
        throws GeneralConvertCalculationException, ProcessingCalculationException {
        try {
            IObject calculation = message.getCalculation();
            String providerId = message.getProviderId();
            String processingName = message.getProcessingName();
            IObject provider = providerCollection.getItems(providerId).get(0);
            List<IObject> providerSchema = ((List<List>) schemasF.in(provider)).get(0);
            IResolveDependencyStrategy convertStrategy = IOC.resolve(Keys.getOrAdd(processingName + "_convert_" + providerId));
            IObject result = convertStrategy.resolve(calculation, providerSchema, message.getFilter());
            if (!statusF.in(result).equals(SUCCESS_STATUS)) {
                throw new ProcessingCalculationException("Error in processing calculation object: " + errorF.in(result));
            }
            List<IObject> resultSchema = schemaF.in(result);
            message.setSchema(resultSchema);

        } catch (ReadValueException | ChangeValueException | InvalidArgumentException | ResolutionException | GetCacheItemException |
                ResolveDependencyStrategyException e) {
            throw new GeneralConvertCalculationException("Error during converting calculation from processing", e);
        }
    }
}
