package com.perspective.vpng.actor.convert_processing_calculation.exception;

/**
 * Exception for handling error from processing (smth like invalid account number)
 */
public class ProcessingCalculationException extends Exception {

    public ProcessingCalculationException(final String message) {
        super(message);
    }
}
