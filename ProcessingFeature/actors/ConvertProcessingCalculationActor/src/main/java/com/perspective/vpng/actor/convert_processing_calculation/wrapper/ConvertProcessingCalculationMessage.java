package com.perspective.vpng.actor.convert_processing_calculation.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface ConvertProcessingCalculationMessage {
    IObject getCalculation() throws ReadValueException;
    String getProcessingName() throws ReadValueException;
    String getProviderId() throws ReadValueException;
    IObject getFilter() throws ReadValueException;
    void setSchema(List<IObject> schema) throws ChangeValueException;
}
