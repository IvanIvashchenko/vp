package com.perspective.vpng.util.template_engine.rule;

import com.perspective.vpng.util.template_engine.ITemplateEngine;
import com.perspective.vpng.util.template_engine.TestDataGenerator;
import com.perspective.vpng.util.template_engine.rule.exception.TagHandlingException;
import com.perspective.vpng.util.template_engine.exception.TemplateFillingException;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TableRuleTest {

    private ITagHandlingRule rule = TableRule.create();
    private TestDataGenerator dataGenerator = new TestDataGenerator();

    @Test
    public void successHandleTagTest() throws TagHandlingException, TemplateFillingException {
        ITemplateEngine templateEngine = mock(ITemplateEngine.class);
        Map<String, Object> testData = dataGenerator.generateDataForTableRuleTest();
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");

        StringBuilder out = new StringBuilder();
        int offset = 0;
        for (int beginTagIndex, endTagIndex; (beginTagIndex = template.indexOf("${", offset)) >= 0;) {
            out.append(template.substring(offset, beginTagIndex));
            endTagIndex = template.indexOf("}", beginTagIndex) + 1;
            beginTagIndex = template.indexOf("${", offset);
            String tag = template.substring(beginTagIndex, endTagIndex);
            offset = rule.handleTag(template, beginTagIndex, endTagIndex, tagValues.get(tag), out, templateEngine);
        }

        List<String> tags = Arrays.asList("${table:EmployeeAge}", "${table:EmployeeSurname}");
        List<String> rowTemplates = Arrays.asList("\n${name}\t${id}\t${age}", "\n${id}\t${surname}");
        Iterator<String> iter = rowTemplates.iterator();
        for (String tag : tags) {
            List<Map<String, Object>> t = (List<Map<String, Object>>) tagValues.get(tag);
            String tmp = iter.next();
            for (Map<String, Object> tagValue : t) {
                verify(templateEngine).fillTemplate(eq(tmp), eq(tagValue), anyObject());
            }
        }
    }

    @Test
    public void should_ThrowsException_WithReason_IllegalGivenTagValueParam() {
        ITemplateEngine templateEngine = mock(ITemplateEngine.class);
        Map<String, Object> testData = dataGenerator.generateDataForTableRuleTest();
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");
        tagValues.forEach((key, value) -> tagValues.put(key, "testIllegalValue"));
        int offset = 0;
        try {
            for (int beginTagIndex, endTagIndex; (beginTagIndex = template.indexOf("${", offset)) >= 0;) {
                endTagIndex = template.indexOf("}", beginTagIndex) + 1;
                beginTagIndex = template.indexOf("${", offset);
                String tag = template.substring(beginTagIndex, endTagIndex);
                offset = rule.handleTag(template, beginTagIndex, endTagIndex, tagValues.get(tag), null, templateEngine);
            }
        } catch (TagHandlingException ex) {
            assertEquals(ex.getMessage(), "Tag handling error: Illegal type of the value of the tag!");
        }
    }

    @Test
    public void should_ThrowsException_WithReason_NotFoundClosingTableTag() {
        ITemplateEngine templateEngine = mock(ITemplateEngine.class);
        Map<String, Object> testData = dataGenerator.generateDataForTableRuleTest();
        String template = testData.get("template").toString().replace("${EOT}", "");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");
        int offset = 0;
        try {
            for (int beginTagIndex, endTagIndex; (beginTagIndex = template.indexOf("${", offset)) >= 0;) {
                endTagIndex = template.indexOf("}", beginTagIndex) + 1;
                beginTagIndex = template.indexOf("${", offset);
                String tag = template.substring(beginTagIndex, endTagIndex);
                offset = rule.handleTag(template, beginTagIndex, endTagIndex, tagValues.get(tag), null, templateEngine);
            }
        } catch (TagHandlingException ex) {
            assertEquals(ex.getMessage(), "Tag handling error: Closing tag is not found!");
        }
    }

}
