package com.perspective.vpng.util.template_engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestDataGenerator {

    public Map<String, Object> generateDataForDefaultRuleTest() {
        Map<String, Object> testData = new HashMap<>(3);
        String filledTemplate = "Hello(fParam) mr.(sParam)Anderson(thParam)!" +
                "\nWhat it's going!!!???";

        StringBuilder template = new StringBuilder();
        template.append("Hello(${tag1}) mr.(${tag2})Anderson(${tag3})!")
                .append("\nWhat it's going${specialTag}???");

        Map<String, Object> tagValues = new HashMap<>();
        tagValues.put("${tag1}", "fParam");
        tagValues.put("${tag2}", "sParam");
        tagValues.put("${tag3}", "thParam");
        tagValues.put("${specialTag}", "!!!");

        testData.put("template", template.toString());
        testData.put("tagValues", tagValues);
        testData.put("filledTemplate", filledTemplate);

        return testData;
    }

    public Map<String, Object> generateDataForTableRuleTest() {
        Map<String, Object> testData = new HashMap<>(3);

        String filledTemplate = "\n\nName\tID\tAge" +
                "\nmr.Anderson\t1\t34" +
                "\nmr.Jones\t2\t45" +
                "\nmr.McDac\t3\t30" +
                "\n\nID\tSurname" +
                "\n11\tPopovich" +
                "\n12\tAbramovich";

        StringBuilder template = new StringBuilder();
        template.append("\n\nName\tID\tAge")
                .append("${table:EmployeeAge}")
                .append("\n${name}\t${id}\t${age}")
                .append("${EOT}")
                .append("\n\nID\tSurname")
                .append("${table:EmployeeSurname}")
                .append("\n${id}\t${surname}")
                .append("${EOT}");

        Map<String, Object> tagValues = new HashMap<>();

        Map<String, Object> fRow = new HashMap<>(3);
        fRow.put("${name}", "mr.Anderson");
        fRow.put("${id}", "1");
        fRow.put("${age}", "34");
        Map<String, Object> sRow = new HashMap<>(3);
        sRow.put("${name}", "mr.Jones");
        sRow.put("${id}", "2");
        sRow.put("${age}", "45");
        Map<String, Object> thRow = new HashMap<>(3);
        thRow.put("${name}", "mr.McDac");
        thRow.put("${id}", "3");
        thRow.put("${age}", "30");

        List<Map<String, Object>> table = new ArrayList<>(3);
        table.add(fRow);
        table.add(sRow);
        table.add(thRow);

        tagValues.put("${table:EmployeeAge}", table);

        fRow = new HashMap<>(3);
        fRow.put("${id}", "11");
        fRow.put("${surname}", "Popovich");
        sRow = new HashMap<>(3);
        sRow.put("${id}", "12");
        sRow.put("${surname}", "Abramovich");

        table = new ArrayList<>(2);
        table.add(fRow);
        table.add(sRow);

        tagValues.put("${table:EmployeeSurname}", table);

        testData.put("template", template.toString());
        testData.put("tagValues", tagValues);
        testData.put("filledTemplate", filledTemplate);

        return testData;
    }

    public Map<String, Object> generateDataForSubTemplateRuleTest() {
        Map<String, Object> testData = new HashMap<>(3);

        String filledTemplate = "Test sub-template: " +
                "I'm kill you(mr.Anderson), bla bla! And all (right)!, " +
                "Oh girl, it's you!!!? I'm confused you up with one fat bastard!!!";

        StringBuilder template = new StringBuilder();
        template.append("Test sub-template: ${tagSubTemplate1}, ${tagSubTemplate2}!!!");

        String subTemplatePattern1 = "I'm kill you(${insideTag1}), bla bla! And all (${insideTag2})!";
        Map<String, Object> insideTag1 = new HashMap<>();
        insideTag1.put("${insideTag1}", "mr.Anderson");
        insideTag1.put("${insideTag2}", "right");
        ITemplate subTemplate1 = Template.of(subTemplatePattern1, insideTag1);

        String subTemplatePattern2 = "Oh girl, it's you!!!? I'm confused you up with ${insideTag1}";
        Map<String, Object> insideTag2 = new HashMap<>();
        insideTag2.put("${insideTag1}", "one fat bastard");
        ITemplate subTemplate2 = Template.of(subTemplatePattern2, insideTag2);

        Map<String, Object> tagValues = new HashMap<>();
        tagValues.put("${tagSubTemplate1}", subTemplate1);
        tagValues.put("${tagSubTemplate2}", subTemplate2);

        testData.put("template", template.toString());
        testData.put("tagValues", tagValues);
        testData.put("filledTemplate", filledTemplate);

        return testData;
    }

}
