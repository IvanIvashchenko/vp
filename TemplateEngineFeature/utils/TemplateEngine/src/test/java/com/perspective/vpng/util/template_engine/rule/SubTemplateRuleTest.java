package com.perspective.vpng.util.template_engine.rule;

import com.perspective.vpng.util.template_engine.ITemplate;
import com.perspective.vpng.util.template_engine.ITemplateEngine;
import com.perspective.vpng.util.template_engine.TestDataGenerator;
import com.perspective.vpng.util.template_engine.rule.exception.TagHandlingException;
import com.perspective.vpng.util.template_engine.exception.TemplateFillingException;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SubTemplateRuleTest {

    private ITagHandlingRule rule = SubTemplateRule.create();
    private TestDataGenerator dataGenerator = new TestDataGenerator();

    @Test
    public void successHandleTagTest() throws TagHandlingException, TemplateFillingException {
        ITemplateEngine templateEngine = mock(ITemplateEngine.class);
        Map<String, Object> testData = dataGenerator.generateDataForSubTemplateRuleTest();
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");

        StringBuilder out = new StringBuilder();
        List<ITemplate> templates = tagValues.entrySet()
                .stream()
                .map(entry -> (ITemplate) entry.getValue())
                .collect(Collectors.toList());
        Iterator iter = templates.iterator();
        int offset = 0;
        for (int beginTagIndex, endTagIndex; (beginTagIndex = template.indexOf("${", offset)) >= 0;) {
            out.append(template.substring(offset, beginTagIndex));
            endTagIndex = template.indexOf("}", beginTagIndex) + 1;
            beginTagIndex = template.indexOf("${", endTagIndex);
            offset = rule.handleTag(template, beginTagIndex, endTagIndex, iter.next(), out, templateEngine);
            assertEquals(offset, endTagIndex);
        }
        for (ITemplate val : templates) {
            verify(templateEngine).fillTemplate(eq(val.getTemplate()), eq(val.getTagValues()), anyObject());
        }
    }

    @Test
    public void should_ThrowsException_WithReason_IllegalGivenTagValueParam() {
        ITemplateEngine templateEngine = mock(ITemplateEngine.class);
        Map<String, Object> testData = dataGenerator.generateDataForSubTemplateRuleTest();
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");
        tagValues.forEach((key, value) -> tagValues.put(key, "testIllegalValue"));
        List templates = tagValues.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

        int offset = 0;
        Iterator iter = templates.iterator();
        try {
            for (int beginTagIndex, endTagIndex; (beginTagIndex = template.indexOf("${", offset)) >= 0;) {
                endTagIndex = template.indexOf("}", beginTagIndex) + 1;
                beginTagIndex = template.indexOf("${", endTagIndex);
                offset = rule.handleTag(template, beginTagIndex, endTagIndex, iter.next(), null, templateEngine);
                assertEquals(offset, endTagIndex);
            }
        } catch (TagHandlingException ex) {
            assertEquals(ex.getMessage(), "Tag handling error: Illegal type of the value of the tag!");
        }
    }

}
