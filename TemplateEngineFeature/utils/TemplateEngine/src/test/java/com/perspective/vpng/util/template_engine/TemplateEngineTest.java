package com.perspective.vpng.util.template_engine;

import com.perspective.vpng.util.template_engine.exception.TemplateFillingException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TemplateEngineTest {

    private ITemplateEngine templateEngine = TemplateEngine.create();
    private TestDataGenerator dataGenerator = new TestDataGenerator();

    @Test
    public void fillTemplateWithOnlySimpleTagsTest() throws TemplateFillingException {
        Map<String, Object> testData = dataGenerator.generateDataForDefaultRuleTest();
        String filledTemplate = (String) testData.get("filledTemplate");
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");
        assertEquals(filledTemplate, templateEngine.fillTemplate(template, tagValues));
    }

    @Test
    public void fillTemplateWithTableTagsTest() throws TemplateFillingException {
        Map<String, Object> testData = dataGenerator.generateDataForTableRuleTest();
        String filledTemplate = (String) testData.get("filledTemplate");
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");
        assertEquals(filledTemplate, templateEngine.fillTemplate(template, tagValues));
    }

    @Test
    public void fillTemplateWithSubTemplateTagsTest() throws TemplateFillingException {
        Map<String, Object> testData = dataGenerator.generateDataForSubTemplateRuleTest();
        String filledTemplate = (String) testData.get("filledTemplate");
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = (Map<String, Object>) testData.get("tagValues");
        assertEquals(filledTemplate, templateEngine.fillTemplate(template, tagValues));
    }

    @Test
    public void fillTemplateWithMixedTagsTest() throws TemplateFillingException {
        String filledTemplate = "Hello(fParam) " +
                "-- I'm kill you(mr.Anderson), bla bla! And all (right) you! -- " +
                "mr.(sParam)Anderson(thParam)!" +
                "\n\nName\tID\tAge" +
                "\nmr.Anderson\t1\t34" +
                "\nmr.Jones\t2\t45" +
                "\nmr.McDac\t3\t30" +
                "\n\nWhat a fuck it's going!!!???";

        StringBuilder template = new StringBuilder();
        template.append("Hello(${tag1}) -- ${tagInsideTemplate} -- mr.(${tag2})Anderson(${tag3})!")
                .append("\n\nName\tID\tAge")
                .append("${table:EmployeeAge}")
                .append("\n${name}\t${id}\t${age}${EOT}")
                .append("\n\nWhat a fuck it's going${specialTag}???");

        String insideTemplatePattern = "I'm kill you(${insideTag1}), bla bla! And all (${insideTag2}) you!";
        Map<String, Object> insideTag = new HashMap<>();
        insideTag.put("${insideTag1}", "mr.Anderson");
        insideTag.put("${insideTag2}", "right");
        ITemplate insideTemplate = Template.of(insideTemplatePattern, insideTag);

        Map<String, Object> fRow = new HashMap<>();
        fRow.put("${name}", "mr.Anderson");
        fRow.put("${id}", "1");
        fRow.put("${age}", "34");
        Map<String, Object> sRow = new HashMap<>();
        sRow.put("${name}", "mr.Jones");
        sRow.put("${id}", "2");
        sRow.put("${age}", "45");
        Map<String, Object> thRow = new HashMap<>();
        thRow.put("${name}", "mr.McDac");
        thRow.put("${id}", "3");
        thRow.put("${age}", "30");

        List<Map<String, Object>> table = new ArrayList<>();
        table.add(fRow);
        table.add(sRow);
        table.add(thRow);

        Map<String, Object> tags = new HashMap<>();
        tags.put("${tag1}", "fParam");
        tags.put("${tag2}", "sParam");
        tags.put("${tag3}", "thParam");
        tags.put("${specialTag}", "!!!");
        tags.put("${tagInsideTemplate}", insideTemplate);
        tags.put("${table:EmployeeAge}", table);

        assertEquals(filledTemplate, templateEngine.fillTemplate(template.toString(), tags));
    }

    @Test
    public void fillTemplateWithoutTagValues() throws TemplateFillingException {
        Map<String, Object> testData = dataGenerator.generateDataForDefaultRuleTest();
        String template = (String) testData.get("template");
        Map<String, Object> tagValues = new HashMap<>();
        assertEquals(template, templateEngine.fillTemplate(template, tagValues));
    }

}
