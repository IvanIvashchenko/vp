package com.perspective.vpng.util.template_engine.rule;

import com.perspective.vpng.util.template_engine.ITemplate;
import com.perspective.vpng.util.template_engine.ITemplateEngine;
import com.perspective.vpng.util.template_engine.rule.exception.TagHandlingException;

/**
 * Rule of handling a tag of template.
 * <br>Tags may be simple(strings, integer numbers, sub-templates etc.) or complex(tables etc.).
 *
 * @see ITemplate
 * @see ITemplateEngine
 */
public interface ITagHandlingRule {
    /**
     * Handles a some tag in the template and writes the result of processing into the output buffer.
     *
     * @param template string template with tags.
     * @param beginTagIndex begin index of tag into the given template.
     * @param endTagIndex end index of tag into the given template.
     * @param value value of the specified tag.
     * @param out output buffer for the result.
     * @param templateEngine template engine(template filling is recursive process:
     *                       template engine can invoke a rule of handling tag
     *                       and that rule can invoke template engine when need fill a sub-template.)
     *
     * @return index offset in the given template to continue handling of the template.
     *
     * @throws TagHandlingException when errors in handling of tag.
     */
    int handleTag(String template, int beginTagIndex, int endTagIndex, Object value,
                  StringBuilder out, ITemplateEngine templateEngine) throws TagHandlingException;

}
