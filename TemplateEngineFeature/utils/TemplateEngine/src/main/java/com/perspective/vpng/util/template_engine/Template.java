package com.perspective.vpng.util.template_engine;

import java.util.Map;

/**
 * Simple default impl. of {@link ITemplate}.
 */
public class Template implements ITemplate {
    /** String template with tags. */
    private final String pattern;
    /** Values of tags of specified template. */
    private final Map<String, Object> tags;

    private Template(final String pattern, final Map<String, Object> tags) {
        this.pattern = pattern;
        this.tags = tags;
    }

    /**
     * Constructs a new template container with specified pattern and values of tags.
     *
     * @param pattern string template with specific tags.
     * @param tagValues values of tags of specified template.
     * @return a template container with string pattern and values of tags.
     * @throws IllegalArgumentException when the given pattern or values of tags are null or empty.
     */
    public static Template of(final String pattern, final Map<String, Object> tagValues) {
        if (pattern == null || pattern.isEmpty() || tagValues == null || tagValues.isEmpty()) {
            throw new IllegalArgumentException("Pattern or values of tags must not be a null or empty!");
        }
        return new Template(pattern, tagValues);
    }

    /**
     * @return string pattern.
     */
    @Override
    public String getTemplate() {
        return pattern;
    }

    /**
     * @return values of tags of specified pattern.
     */
    @Override
    public Map<String, Object> getTagValues() {
        return tags;
    }
}
