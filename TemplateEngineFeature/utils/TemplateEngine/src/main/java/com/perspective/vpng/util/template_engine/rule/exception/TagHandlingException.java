package com.perspective.vpng.util.template_engine.rule.exception;

import com.perspective.vpng.util.template_engine.rule.ITagHandlingRule;

/**
 * Exception of template's tag handling process.
 *
 * @see ITagHandlingRule
 */
public class TagHandlingException extends Exception {
    /**
     * Constructs a new exception with the specified detail message and
     * cause.
     * <br>Note that the detail message associated with
     * {@code cause} is <i>not</i> automatically incorporated in
     * this exception's detail message.
     * <br>Appends to message's header - <i>Tag handling error:</i>
     *
     * @param  message the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).
     *
     * @see Exception#Exception(String, Throwable)
     */
    public TagHandlingException(String message, Throwable cause) {
        super("Tag handling error: " + message, cause);
    }

}
