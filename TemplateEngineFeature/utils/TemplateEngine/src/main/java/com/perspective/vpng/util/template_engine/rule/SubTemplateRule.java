package com.perspective.vpng.util.template_engine.rule;

import com.perspective.vpng.util.template_engine.ITemplate;
import com.perspective.vpng.util.template_engine.ITemplateEngine;
import com.perspective.vpng.util.template_engine.rule.exception.TagHandlingException;
import com.perspective.vpng.util.template_engine.exception.TemplateFillingException;

/**
 * Rule of handling <i>sub-templates</i> tag in template.
 * <br><i>Sub-template</i> tag is simple tag, for example: ${tagName}
 *
 * @see ITagHandlingRule
 * @see SubTemplateRule#handleTag(String, int, int, Object, StringBuilder, ITemplateEngine)
 */
public class SubTemplateRule implements ITagHandlingRule {

    private SubTemplateRule() { }

    /**
     * Factory method for encapsulation of the creation a new instance of {@link SubTemplateRule}.
     *
     * @return a new instance of {@link SubTemplateRule}.
     */
    public static SubTemplateRule create() {
        return new SubTemplateRule();
    }

    /**
     * Appends a sub-template in the given template.
     * <br><b><i>Uses recursion!</i></b>
     *
     * @param template a string template with tags.
     * @param beginTagIndex a begin index of tag into the given template.
     * @param endTagIndex a end index of tag into the given template.
     * @param value a value of the specified tag. Must be an object of type {@link ITemplate}.
     * @param out an output buffer for the result.
     * @param templateEngine a template engine(template filling is recursive process:
     *                       template engine can invoke a rule of handling tag
     *                       and that rule can invoke template engine when need fill a sub-template.)
     *
     * @return end index of tag in the given template.
     *
     * @throws TagHandlingException when the given value has incorrect type
     *                       or errors in the handling of sub-template.
     */
    @Override
    public int handleTag(
            final String template,
            final int beginTagIndex,
            final int endTagIndex,
            final Object value,
            final StringBuilder out,
            final ITemplateEngine templateEngine
    ) throws TagHandlingException {
        try {
            ITemplate subTemplate = (ITemplate) value;
            templateEngine.fillTemplate(subTemplate.getTemplate(), subTemplate.getTagValues(), out);
            return endTagIndex;
        } catch (ClassCastException ex) {
            throw new TagHandlingException("Illegal type of the value of the tag!", ex);
        } catch (TemplateFillingException ex) {
            throw new TagHandlingException(ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            throw new TagHandlingException("Given parameters must not be a null!", ex);
        }
    }

}
