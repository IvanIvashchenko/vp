package com.perspective.vpng.util.template_engine;

import com.perspective.vpng.util.template_engine.exception.TemplateFillingException;
import com.perspective.vpng.util.template_engine.rule.ITagHandlingRule;
import com.perspective.vpng.util.template_engine.rule.SubTemplateRule;
import com.perspective.vpng.util.template_engine.rule.TableRule;
import com.perspective.vpng.util.template_engine.rule.exception.TagHandlingException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Default impl. of {@link ITemplateEngine} for handling string template with specific tags.
 *
 * @see TemplateEngine#fillTemplate(ITemplate)
 * @see TemplateEngine#fillTemplate(String, Map, StringBuilder)
 * @see TemplateEngine#fillTemplate(String, Map)
 */
public class TemplateEngine implements ITemplateEngine {
    /** The rules for handling a specific tags in template. */
    private Map<Class, ITagHandlingRule> rules;
    /** The beginning of the each tag. */
    private static String BEGIN_TAG = "${";
    /** The end of the each tag. */
    private static String END_TAG = "}";

    /**
     * Constructs a new template engine without parameters
     *              and with default rules.
     *
     * @see TemplateEngine#initRules()
     */
    protected TemplateEngine() {
        initRules();
    }

    /**
     * Constructs a new template engine with the specified rules of tags handling.
     *
     * @param rules specific rules of tags handling.
     *
     * @see ITagHandlingRule
     */
    protected TemplateEngine(final Map<Class, ITagHandlingRule> rules) {
        initRules();
        this.rules.putAll(rules);
    }

    /**
     * Init a default rules(rules for handling <i>table</i> and <i>sub-template</i> tags) for template engine.
     *
     * @see TableRule
     * @see SubTemplateRule
     */
    protected void initRules() {
        this.rules = new HashMap<Class, ITagHandlingRule>() {{
            put(List.class, TableRule.create());
            put(ITemplate.class, SubTemplateRule.create());
        }};
    }

    /**
     * Factory method for encapsulation of creation
     *              a new instance of {@link TemplateEngine} without parameters.
     *
     * @return a new instance of {@link TemplateEngine}.
     *
     * @see TemplateEngine#TemplateEngine()
     */
    public static TemplateEngine create() {
        return new TemplateEngine();
    }

    /**
     * Factory method for encapsulation of creation a new instance of {@link TemplateEngine}
     *              with the specified rules of tags handling.
     *
     * @param rules specific rules of tags handling.
     * @return a new instance of {@link TemplateEngine}.
     * @throws IllegalArgumentException when the given map of rules is null or empty.
     *
     * @see TemplateEngine#TemplateEngine(Map)
     */
    public static TemplateEngine create(final Map<Class, ITagHandlingRule> rules) {
        try {
            return new TemplateEngine(new HashMap<>(rules));
        } catch (NullPointerException ex) {
            throw new IllegalArgumentException("Rules must not be null!", ex);
        }
    }

    /**
     * Fills a tags into template by values.
     * <br>Template and values of tags must contains in given container of template.
     *
     * @param template contains a template and a values of tags.
     * @return a filled template.
     * @throws TemplateFillingException when template filling  error.
     *
     * @see ITemplate
     */
    @Override
    public String fillTemplate(final ITemplate template) throws TemplateFillingException {
        return fillTemplate(template.getTemplate(), template.getTagValues());
    }

    /**
     * Fills a tags into the given template by the given values.
     *
     * @param template string pattern with tags.
     * @param tagValues values of tags which contains in the template.
     * @return a filled template.
     * @throws TemplateFillingException when template filling  error.
     */
    @Override
    public String fillTemplate(
            final String template,
            final Map<String, Object> tagValues
    ) throws TemplateFillingException {
        StringBuilder filledTemplate = new StringBuilder(template.length());
        fillTemplate(template, tagValues, filledTemplate);
        return filledTemplate.toString();
    }

    /**
     * Fills a tags into the given template by the given values
     *              and writes filled template in output buffer.
     * <br>If map of tags values is empty
     *              then writes the given template in the output buffer without change.
     *
     * @param template string pattern with tags.
     * @param tagValues values of tags which contains in the template.
     * @param out output buffer for a filled template.
     * @throws TemplateFillingException when template filling  error.
     */
    @Override
    public void fillTemplate(
            final String template,
            final Map<String, Object> tagValues,
            final StringBuilder out
    ) throws TemplateFillingException {
        try {
            if (tagValues.isEmpty()) {
                out.append(template);
                return;
            }
            int offset = 0;
            for (int beginTagIndex, endTagIndex; (beginTagIndex = template.indexOf(BEGIN_TAG, offset)) >= 0;) {
                out.append(template.substring(offset, beginTagIndex));
                endTagIndex = template.indexOf(END_TAG, beginTagIndex) + 1;
                String tag = template.substring(beginTagIndex, endTagIndex);
                Object value = tagValues.get(tag);
                offset = handleTag(template, beginTagIndex, endTagIndex, tag, value, out);
            }
            if (template.length() - offset > 0) {
                out.append(template.substring(offset, template.length()));
            }
        } catch (TagHandlingException ex) {
            throw new TemplateFillingException(ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            throw new TemplateFillingException("Given parameters must not be a null!", ex);
        }
    }

    /**
     * Changes begin value of tag.
     * <br>Default equals <i>${</i>
     *
     * @param beginTag new begin value of tag.
     *
     * @throws IllegalArgumentException when the given <i>begin tag</i> is null or empty.
     */
    @Override
    public void setBeginTag(final String beginTag)  {
        if (beginTag == null || beginTag.isEmpty()) {
            throw new IllegalArgumentException("Begin tag must not be a null or empty!");
        }
        BEGIN_TAG = beginTag;
    }

    /**
     * Changes end value of tag.
     * <br>Default equals <i>}</i>
     *
     * @param endTag new end value of tag.
     *
     * @throws IllegalArgumentException when the given <i>end tag</i> is null or empty.
     */
    @Override
    public void setEndTag(final String endTag)  {
        if (endTag == null || endTag.isEmpty()) {
            throw new IllegalArgumentException("End tag must not be a null or empty!");
        }
        END_TAG = endTag;
    }

    /**
     * Handles tag in the given template and writes result in the output buffer.
     * Uses rules of handling tag.
     * <br>If rule for specified tag value is not found
     *              then converts the value to string and replaces the tag on him.
     * <br>If value of tag is null then writes the specified tag in the output buffer.
     *
     * @param template string template with tags.
     * @param beginTagIndex begin index of tag into the given template.
     * @param endTagIndex end index of tag into the given template.
     * @param tag handled tag.
     * @param value value of the specified tag.
     * @param out output buffer for the result.
     *
     * @return index offset in the given template to continue handling of the template.
     *
     * @throws TagHandlingException when errors in handling of the specified tag.
     *
     * @see TemplateEngine#rules
     * @see ITagHandlingRule
     */
    protected int handleTag(
            final String template,
            final int beginTagIndex,
            final int endTagIndex,
            final String tag,
            final Object value,
            final StringBuilder out
    ) throws TagHandlingException {
        if (value == null) {
            out.append(tag);
            return endTagIndex;
        }
        Class key = value.getClass();
        ITagHandlingRule rule = rules.get(key);
        if (rule != null) {
            return rule.handleTag(template, beginTagIndex, endTagIndex, value, out, this);
        }
        Class[] interfaces;
        for (Map.Entry<Class, ITagHandlingRule> entry : rules.entrySet()) {
            interfaces = key.getInterfaces();
            if (Stream.of(interfaces).anyMatch(val -> val.equals(entry.getKey()))) {
                return entry.getValue()
                        .handleTag(template, beginTagIndex, endTagIndex, value, out, this);
            }
        }
        out.append(value);
        return endTagIndex;
    }

}
