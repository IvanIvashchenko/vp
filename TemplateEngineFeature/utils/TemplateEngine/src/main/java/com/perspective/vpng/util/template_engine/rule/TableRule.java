package com.perspective.vpng.util.template_engine.rule;

import com.perspective.vpng.util.template_engine.ITemplateEngine;
import com.perspective.vpng.util.template_engine.rule.exception.TagHandlingException;
import com.perspective.vpng.util.template_engine.exception.TemplateFillingException;

import java.util.List;
import java.util.Map;

/**
 * Rule of handling <i>table</i> tag into template.
 * <br><i>Table</i> tag is complex tag, this means that it has a closing tag(default is ${EOT}).
 * <br>The table header must be placed before the opening tag,
 *              between the opening and closing tags must to be placed the table's row template.
 * <br>The number of rows in the table is determined according to the incoming parameters
 *              when template filling.
 *
 * <br>For example, template with <i>table</i> tag:
 *              <p>... column1   column2   column3${tableTag}${columnValue1}   ${columnValue2}   ${columnValue3}${EOT}</p>
 *
 * @see ITagHandlingRule
 * @see TableRule#handleTag(String, int, int, Object, StringBuilder, ITemplateEngine)
 */
public class TableRule implements ITagHandlingRule {
    /** The closing table tag. */
    private String closingTableTag = "${EOT}";

    private TableRule() { }

    /**
     * Default factory method for encapsulation of the creation a new instance of {@link TableRule}.
     *
     * @return a new instance of {@link TableRule}.
     */
    public static TableRule create() {
        return new TableRule();
    }

    /**
     * Appends a table in the given template.
     * <br>The table header must be placed before the opening tag,
     *              between the opening and closing tags must to be placed the table's row template.
     * <br>The number of rows in the table is determined according to the incoming parameters
     *              when template filling.
     * <br><b><i>Uses recursion!</i></b>
     *
     * @param template a string template with tags.
     * @param beginTagIndex a begin index of tag into the given template.
     * @param endTagIndex a end index of tag into the given template.
     * @param value a value of the specified tag. Must be a list({@link List}) of {@link Map<String, Object>} object.
     *              <br>List size determines the number of rows in the table.
     *              Each item of the list is a sets of values for the each table row.
     *              <br>For example:
     *                  [ ${tag1} : tag1Value, ${tag2} : tag2Value, ... ].
     * @param out an output buffer for the result.
     * @param templateEngine a template engine(template filling is recursive process:
     *              template engine can invoke a rule of handling tag
     *              and that rule can invoke template engine when need fill a sub-template.)
     *
     * @return last index of the closing <i>table</i> tag in the given template.
     *
     * @throws TagHandlingException when invalid type of given param value or missing a closing table tag
     *              or errors in the filling of the table row.
     */
    @Override
    public int handleTag(
            final String template,
            final int beginTagIndex,
            final int endTagIndex,
            final Object value,
            final StringBuilder out,
            final ITemplateEngine templateEngine
    ) throws TagHandlingException {
        try {
            int endComplexTagIndex = template.indexOf(closingTableTag, endTagIndex);
            String rowTemplate = template.substring(endTagIndex, endComplexTagIndex);
            List<Map<String, Object>> table = (List<Map<String, Object>>) value;
            for (Map<String, Object> row : table) {
                templateEngine.fillTemplate(rowTemplate, row, out);
            }
            return endComplexTagIndex + closingTableTag.length();
        } catch (ClassCastException ex) {
            throw new TagHandlingException("Illegal type of the value of the tag!", ex);
        } catch (IndexOutOfBoundsException ex) {
            throw new TagHandlingException("Closing tag is not found!", ex);
        } catch (TemplateFillingException ex) {
            throw new TagHandlingException(ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            throw new TagHandlingException("Given parameters must not be a null!", ex);
        }
    }

    /**
     * @return the closing table tag(Default is ${EOT}).
     *
     * @see TableRule#closingTableTag
     */
    public String getClosingTableTag() {
        return closingTableTag;
    }

    /**
     * Changes the closing table tag for rule.
     *
     * @param closingTableTag closing tag for <i>table</i> tag.
     *
     * @return a link of this {@link TableRule} object.
     */
    public TableRule setClosingTableTag(final String closingTableTag) {
        this.closingTableTag = closingTableTag == null ? this.closingTableTag : closingTableTag;
        return this;
    }

}
