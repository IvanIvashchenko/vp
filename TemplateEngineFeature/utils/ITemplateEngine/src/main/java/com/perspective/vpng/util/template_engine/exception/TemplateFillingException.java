package com.perspective.vpng.util.template_engine.exception;

import com.perspective.vpng.util.template_engine.ITemplateEngine;

/**
 * Exception of template filling process.
 *
 * @see Exception
 * @see ITemplateEngine
 */
public class TemplateFillingException extends Exception {
    /**
     * Constructs a new exception with the specified detail message.
     * <br>Appends to message's header - <i>Template filling error:</i>
     *
     * @param message the detail message. The detail message is saved for
     *          later retrieval by the {@link #getMessage()} method.
     *
     * @see Exception#Exception(String)
     */
    public TemplateFillingException(final String message) {
        super("Template filling error: " + message);
    }

    /**
     * Constructs a new exception with the specified detail message and
     * cause.
     * <br>Note that the detail message associated with
     * {@code cause} is <i>not</i> automatically incorporated in
     * this exception's detail message.
     * <br>Appends to message's header - <i>Template filling error:</i>
     *
     * @param  message the detail message (which is saved for later retrieval
     *         by the {@link #getMessage()} method).
     * @param  cause the cause (which is saved for later retrieval by the
     *         {@link #getCause()} method).
     *
     * @see Exception#Exception(String, Throwable)
     */
    public TemplateFillingException(final String message, final Throwable cause) {
        super("Template filling error: " + message, cause);
    }

}
