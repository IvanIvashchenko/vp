package com.perspective.vpng.util.template_engine;

import com.perspective.vpng.util.template_engine.exception.TemplateFillingException;

import java.util.Map;

/**
 * Template filling util.
 * <br>Replaces the tags in the template to specific values.</br>
 *
 * @see ITemplateEngine#fillTemplate(ITemplate)
 * @see ITemplateEngine#fillTemplate(String, Map)
 * @see ITemplateEngine#fillTemplate(String, Map, StringBuilder)
 *
 * @see ITemplateEngine#setBeginTag(String)
 * @see ITemplateEngine#setEndTag(String)
 */
public interface ITemplateEngine {
    /**
     * Fills a tags into template by values.
     * <br>Template and values of tags must contains in given container of template.
     *
     * @param template contains a template and a values of tags.
     * @return a filled template.
     * @throws TemplateFillingException when template filling  error.
     *
     * @see ITemplate
     */
    String fillTemplate(ITemplate template) throws TemplateFillingException;

    /**
     * Fills a tags into the given template by the given values.
     *
     * @param template string pattern with tags.
     * @param tagValues values of tags which contains in the template.
     * @return a filled template.
     * @throws TemplateFillingException when template filling  error.
     */
    String fillTemplate(String template, Map<String, Object> tagValues) throws TemplateFillingException;

    /**
     * Fills a tags into the given template by the given values
     *              and writes filled template in output buffer.
     *
     * @param template string pattern with tags.
     * @param tagValues values of tags which contains in the template.
     * @param out output buffer for a filled template.
     * @throws TemplateFillingException when template filling  error.
     */
    void fillTemplate(String template, Map<String, Object> tagValues, StringBuilder out) throws TemplateFillingException;

    /**
     * Changes begin value of tag.
     * <br>Default equals <i>${</i>
     *
     * @param beginTag new begin value of tag.
     */
    void setBeginTag(String beginTag);

    /**
     * Changes end value of tag.
     * <br>Default equals <i>}</i>
     *
     * @param endTag new end value of tag.
     */
    void setEndTag(String endTag);

}
