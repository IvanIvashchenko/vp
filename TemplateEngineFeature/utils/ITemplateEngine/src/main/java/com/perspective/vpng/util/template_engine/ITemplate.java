package com.perspective.vpng.util.template_engine;

import java.util.Map;

/**
 * Container for a string template and values of tags of the specified pattern.
 */
public interface ITemplate {
    /** Gets a string pattern. */
    String getTemplate();
    /** Gets a values of tags of specified pattern. */
    Map<String, Object> getTagValues();

}
