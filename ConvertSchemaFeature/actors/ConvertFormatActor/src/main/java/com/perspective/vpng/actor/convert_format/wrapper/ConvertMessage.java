package com.perspective.vpng.actor.convert_format.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ConvertMessage {
    void setResultObject(Object resultObject) throws ChangeValueException;
    Object getFromObject() throws ReadValueException;
    String getConvertStrategy() throws ReadValueException;
}
