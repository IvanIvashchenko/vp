package com.perspective.vpng.actor.convert_format.exception;

/**
 * Exception for {@link com.perspective.vpng.actor.convert_format.ConvertFormatActor}
 */
public class ConvertFormatException extends Exception {

    public ConvertFormatException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
