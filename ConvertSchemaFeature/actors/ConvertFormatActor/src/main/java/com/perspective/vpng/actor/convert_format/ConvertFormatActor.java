package com.perspective.vpng.actor.convert_format;

import com.perspective.vpng.actor.convert_format.exception.ConvertFormatException;
import com.perspective.vpng.actor.convert_format.wrapper.ConvertForEachMessage;
import com.perspective.vpng.actor.convert_format.wrapper.ConvertMessage;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;

public class ConvertFormatActor {
    public void convert(ConvertMessage message) throws ConvertFormatException {
        try {
            String strategyName = message.getConvertStrategy();
            Object resultObj = IOC.resolve(Keys.getOrAdd(strategyName), message.getFromObject());
            message.setResultObject(resultObj);
        } catch (Exception e) {
            throw new ConvertFormatException("Failed to convert object", e);
        }
    }

    /**
     * Handler wrapper for {@link this.convert} which can work with list of objects
     * @param message contains list of objects into "from" format
     */
    public void convertForEach(final ConvertForEachMessage message) throws ConvertFormatException {

        List<Object> resultObjects = new ArrayList<>();
        try {
            List<IObject> fromObjects = message.getFromObjects();
            for (IObject fromObject : fromObjects) {
                message.setFromObject(fromObject);
                convert(message);
                resultObjects.add(message.getResultObject());
            }
            message.setResultObjects(resultObjects);
        } catch (Exception e) {
            throw new ConvertFormatException("Failed to convert list of objects", e);
        }
    }
}
