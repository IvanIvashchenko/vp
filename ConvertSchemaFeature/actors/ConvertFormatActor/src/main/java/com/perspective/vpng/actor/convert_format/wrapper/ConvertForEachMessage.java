package com.perspective.vpng.actor.convert_format.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for {@link com.perspective.vpng.actor.convert_format.ConvertFormatActor} needed for case when list
 * of source objects come to the input
 */
public interface ConvertForEachMessage extends ConvertMessage {

    List<IObject> getFromObjects() throws ReadValueException;
    Object getResultObject() throws ReadValueException;

    void setFromObject(IObject fromObject) throws ChangeValueException;
    void setResultObjects(List<Object> resultObjects) throws ChangeValueException;
}
