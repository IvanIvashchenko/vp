package com.perspective.vpng.plugin.from_inner_to_schema_strategy;

import com.perspective.vpng.strategy.from_inner_to_schema.ConverterFunction;
import com.perspective.vpng.strategy.from_inner_to_schema.FromInnerToSchemaStrategy;
import com.perspective.vpng.strategy.from_inner_to_schema.exception.ConverterFunctionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Plugin for register strategy for convert from inner format to schema
 */
public class FromInnerToSchemaStrategyPlugin implements IPlugin {

    private static final String PROVIDER_COLLECTION_NAME = "provider";
    private static final String PROVIDER_KEY_NAME = "name";
    private static final String SEPARATOR = "||";

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    private IField valueF;
    private IField nameF;
    private IField valuesF;
    private IField columnsF;
    private IField uniqueF;
    private IField toSchemaF;
    private IField servicesField;
    private IField visibleField;
    private IField requiredField;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public FromInnerToSchemaStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }


    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("FromInnerToSchemaStrategyPlugin");
            item
                .process(() -> {
                    try {
                        valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
                        nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
                        valuesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "values");
                        columnsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "columns");
                        uniqueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "unique");
                        toSchemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "toSchema");
                        servicesField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "услуги");
                        visibleField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "visible");
                        requiredField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "required");
                        Map<String, ConverterFunction> defaultTypes = new LinkedHashMap<>();

                        defaultTypes.put("лицевой-счет", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/одной-строкой");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("лицевой-счет-1", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/фрагменты");

                                return convertFragment(inner, schemaObjIn, innerField, 0);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("лицевой-счет-2", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/фрагменты");

                                return convertFragment(inner, schemaObjIn, innerField, 1);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("лицевой-счет-3", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/фрагменты");

                                return convertFragment(inner, schemaObjIn, innerField, 2);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("фамилия", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/фамилия");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("имя", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/имя");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("отчество", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/отчество");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("фио", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/одной-строкой");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("адрес", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/одной-строкой");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("улица", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/город-улица");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("дом", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/дом");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("корпус", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/корпус");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("квартира", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/квартира");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("телефон", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/одной-строкой");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("email", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "email");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("месяц", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "период-оплаты/месяц");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("год", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "период-оплаты/год");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("всего-к-оплате", (inner, schemaObjIn) -> {
                            try {
                                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "всего-к-оплате");

                                return convertSingle(inner, schemaObjIn, innerField);
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert field from inner format to schema", e);
                            }
                        });

                        defaultTypes.put("услуги", (inner, schemaObjIn) -> {
                            try {
                                return convertTable(inner, schemaObjIn, "услуги");
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert services field from inner format to schema", e);
                            }
                        });
                        defaultTypes.put("приборы-учета", (inner, schemaObjIn) -> {
                            try {
                                return convertTable(inner, schemaObjIn, "приборы-учета");
                            } catch (Exception e) {
                                throw new ConverterFunctionException("Can't convert meters field from inner format to schema", e);
                            }
                        });

                        IOC.register(
                            Keys.getOrAdd("InnerSchema"),
                            new FromInnerToSchemaStrategy(
                                defaultTypes, PROVIDER_COLLECTION_NAME, PROVIDER_KEY_NAME, "chooseInvoiceSchemaStrategy"
                            )
                        );
                        IOC.register(
                            Keys.getOrAdd("InnerSchemaMeters"),
                            new FromInnerToSchemaStrategy(
                                defaultTypes, PROVIDER_COLLECTION_NAME, PROVIDER_KEY_NAME, "chooseMetersSchemaStrategy"
                            )
                        );
                    } catch (Exception ex) {
                        throw new ActionExecuteException("FromSchemaToInnerConvertStrategyPlugin plugin can't load: " + ex.getMessage(), ex);
                    }
                });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }

    private IObject convertSingle(final IObject inner, final IObject schemaObjIn, final IField innerField)
            throws ResolutionException, ReadValueException, InvalidArgumentException, ChangeValueException {

        IObject resultSchemaEl = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        nameF.out(resultSchemaEl, nameF.in(schemaObjIn));
        copyOptionalField(schemaObjIn, resultSchemaEl, visibleField);
        copyOptionalField(schemaObjIn, resultSchemaEl, requiredField);
        String value;
        try {
            value = innerField.in(inner, String.class);
        } catch (InvalidArgumentException e) {
            valueF.out(resultSchemaEl, "");
            return resultSchemaEl;
        }
        valueF.out(resultSchemaEl, value);

        return resultSchemaEl;
    }

    private IObject convertFragment(final IObject inner, final IObject schemaObjIn, final IField innerField, final int index)
        throws ResolutionException, ReadValueException, InvalidArgumentException, ChangeValueException {

        IObject resultSchemaEl = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        nameF.out(resultSchemaEl, nameF.in(schemaObjIn));
        copyOptionalField(schemaObjIn, resultSchemaEl, visibleField);
        copyOptionalField(schemaObjIn, resultSchemaEl, requiredField);
        List<String> fragments = innerField.in(inner);
        valueF.out(resultSchemaEl, fragments.get(index));

        return resultSchemaEl;
    }

    private IObject convertTable(final IObject inner, final IObject schemaObjIn, final String innerName)
        throws ResolutionException, ReadValueException, InvalidArgumentException, ChangeValueException, ResolveDependencyStrategyException {

        IObject schemaObjOut = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        List<IObject> services = servicesField.in(inner);
        List<IObject> columnsIn = columnsF.in(schemaObjIn);
        List<IObject> columnsOut = new ArrayList<>();
        List<String> valuesKeyNames = new ArrayList<>();
        List<String> keyColumnNames = uniqueF.in(schemaObjIn);
        Map<String, IResolveDependencyStrategy> customStrategies = new HashMap<>();

        //for each columns copy visibility flag and column name
        //and remember this name as a key for corresponding object into values array
        for (IObject columnIn : columnsIn) {
            IObject columnOut;
            if (toSchemaF.in(columnIn) != null) {
                IResolveDependencyStrategy customStrategy = IOC.resolve(Keys.getOrAdd(toSchemaF.in(columnIn)));
                columnOut = customStrategy.resolve(inner, schemaObjIn);
                //save strategy for possible further using as an aggregating function
                customStrategies.put(nameF.in(columnOut, String.class), customStrategy);
            } else {
                columnOut = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                nameF.out(columnOut, nameF.in(columnIn));
                copyOptionalField(columnIn, columnOut, visibleField);
                copyOptionalField(columnIn, columnOut, requiredField);
            }
            columnsOut.add(columnOut);
            valuesKeyNames.add(nameF.in(columnOut));
        }
        columnsF.out(schemaObjOut, columnsOut);
        List<IObject> valuesOut = new ArrayList<>();

        //Services from inner format. For each service try to insert to values array one object with
        //number of fields = number of objects into columns.
        //For each service there are 2 lists: all fields of row (valuesKeyNames) and list of fields, by which
        //we may differ one row from another (keyColumnNames)
        //rowWrapperMap contains concatenated keyColumnNames as a key and pair of row and ability to add flag as a value
        Map<String, TableRowWrapper> rowWrapperMap = new HashMap<>();
        boolean nextService;
        String rowKey, partialRowKey;
        StringBuilder builder;
        for (IObject service : services) {

            nextService = false;
            rowKey = "";
            builder = new StringBuilder();
            for (String keyColumnName : keyColumnNames) {
                IField keyColumnValueField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), keyColumnName);
                //if service doesn't contain one of key fields => skip this service
                if (keyColumnValueField.in(service) == null) {
                    nextService = true;
                    break;
                }
                partialRowKey = keyColumnValueField.in(service, String.class);
                rowKey = builder.append(rowKey).append(partialRowKey.toLowerCase()).append(SEPARATOR).toString();
            }
            if (nextService) {
                continue;
            }
            TableRowWrapper rowWrapper = rowWrapperMap.get(rowKey);
            IObject valueOut;
            //if map doesn't contain such key yet => create new row, fill it by values from service and put to the map
            if (rowWrapper == null) {
                valueOut = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                rowWrapper = new TableRowWrapper(valueOut, true);
                for (String valueKeyName : valuesKeyNames) {
                    IField valueField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), valueKeyName);
                    String value = valueField.in(service, String.class);
                    valueField.out(valueOut, value);
                }
                rowWrapperMap.put(rowKey, rowWrapper);
            //if map contains such key, get pair by key, check that this row should be added to the table,
            //add new values from current service and check that existed values equal to values from current service
            //by corresponded keys. If this condition false => try to use custom aggregating function, if it doesn't present for
            //this column, mark this row as an unnecessary for this table
            } else {
                if (rowWrapper.isNeedToAddToTable()) {
                    valueOut = rowWrapper.getTableRow();
                    for (String valueKeyName : valuesKeyNames) {
                        IField valueField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), valueKeyName);
                        String valueFromService = valueField.in(service, String.class);
                        String valueFromRow = valueField.in(valueOut, String.class);
                        if (valueFromRow == null) {
                            valueField.out(valueOut, valueFromService);
                        } else if (valueFromService != null && !valueFromRow.equalsIgnoreCase(valueFromService)) {
                            IResolveDependencyStrategy aggregatingFunction = customStrategies.get(valueKeyName);
                            if (aggregatingFunction == null) {
                                rowWrapper.setNeedToAddToTable(false);
                            } else {
                                aggregatingFunction.resolve(valueOut, valueFromRow, valueFromService);
                            }
                        }
                    }
                }
            }
        }
        //Fill the table by all necessary rows
        for (String key : rowWrapperMap.keySet()) {
            TableRowWrapper rowWrapper = rowWrapperMap.get(key);
            if (rowWrapper.isNeedToAddToTable()) {
                valuesOut.add(rowWrapper.getTableRow());
            }
        }
        valuesF.out(schemaObjOut, valuesOut);
        nameF.out(schemaObjOut, innerName);

        return schemaObjOut;
    }

    private void copyOptionalField(final IObject in, final IObject out, final IField field)
            throws ReadValueException, InvalidArgumentException, ChangeValueException {
        if (field.in(in) != null) {
            field.out(out, field.in(in));
        }
    }
}
