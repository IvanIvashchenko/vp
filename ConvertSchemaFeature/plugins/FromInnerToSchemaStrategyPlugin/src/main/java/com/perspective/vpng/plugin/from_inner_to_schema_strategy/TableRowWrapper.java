package com.perspective.vpng.plugin.from_inner_to_schema_strategy;

import info.smart_tools.smartactors.iobject.iobject.IObject;

/**
 * Helper class, which contains potential table row and flag for need to addition this row to table
 */
class TableRowWrapper {

    private IObject tableRow;
    private boolean needToAddToTable;

    /**
     * Constructor
     * @param tableRow object with table row value
     * @param needToAddToTable opportunity of addition flag
     */
    TableRowWrapper(final IObject tableRow, final boolean needToAddToTable) {
        this.tableRow = tableRow;
        this.needToAddToTable = needToAddToTable;
    }

    public IObject getTableRow() {
        return tableRow;
    }

    public boolean isNeedToAddToTable() {
        return needToAddToTable;
    }

    public void setNeedToAddToTable(final boolean needToAddToTable) {
        this.needToAddToTable = needToAddToTable;
    }
}
