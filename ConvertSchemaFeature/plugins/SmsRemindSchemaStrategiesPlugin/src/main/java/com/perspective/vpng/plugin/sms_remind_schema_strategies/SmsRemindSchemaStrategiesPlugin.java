package com.perspective.vpng.plugin.sms_remind_schema_strategies;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.base.strategy.singleton_strategy.SingletonStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class SmsRemindSchemaStrategiesPlugin extends BootstrapPlugin {
    public SmsRemindSchemaStrategiesPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("RegisterSchemaStrategiesPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("sms_remind_flag_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "remind/добавить-напоминание");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("sms_remind_value_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "remind/напоминание");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("sms_remind_type_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "remind/тип-напоминания");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
    }
}
