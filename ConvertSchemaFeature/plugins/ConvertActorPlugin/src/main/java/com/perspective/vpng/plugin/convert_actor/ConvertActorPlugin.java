package com.perspective.vpng.plugin.convert_actor;

import com.perspective.vpng.actor.convert_format.ConvertFormatActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class ConvertActorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public ConvertActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    /**
     * Loads {@link ConvertActorPlugin} actor and
     *              registers in <code>IOC</code>.
     * Begin loads actor after loading plugins: <code>IOC</code>, <code>starter</code>
     *              and before: <code>read_config</code>.
     *
     * @throws PluginException when errors in loading actor.
     */
    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> makePayment = new BootstrapItem("ConvertActorPlugin");

            makePayment
                    .process(() -> {
                        try {
                            IOC.register(Keys.getOrAdd(ConvertFormatActor.class.getCanonicalName()), new ApplyFunctionToArgumentsStrategy(
                                    (args) -> new ConvertFormatActor()

                            ));
                        } catch (ResolutionException | RegistrationException | InvalidArgumentException e) {
                            throw new ActionExecuteException(String
                                    .format("%s plugin can't load!", this.getClass().getCanonicalName()), e);
                        }
                    });
            bootstrap.add(makePayment);

        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't get BootstrapItem from one of reason: ", e);
        }
    }
}
