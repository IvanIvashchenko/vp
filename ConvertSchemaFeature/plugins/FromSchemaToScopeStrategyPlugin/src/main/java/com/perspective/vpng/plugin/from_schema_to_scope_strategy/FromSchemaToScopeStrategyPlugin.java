package com.perspective.vpng.plugin.from_schema_to_scope_strategy;

import com.perspective.vpng.strategy.from_schema_to_scope.FromSchemaToScopeStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class FromSchemaToScopeStrategyPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public FromSchemaToScopeStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {

        try {
            IBootstrapItem<String> item = new BootstrapItem("FromSchemaToScopeStrategyPlugin");
            item.process(() -> {
                try {
                    IOC.register(
                            Keys.getOrAdd("FromSchemaToScopeStrategy"),
                            new FromSchemaToScopeStrategy()
                    );
                } catch (Exception ex) {
                    throw new ActionExecuteException(ex.getMessage(), ex);
                }
            });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }

}
