package com.perspective.vpng.plugin.register_schema_strategies;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.base.strategy.singleton_strategy.SingletonStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class RegisterSchemaStrategiesPlugin extends BootstrapPlugin {
    public RegisterSchemaStrategiesPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("RegisterSchemaStrategiesPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("register_form_phone_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "клиент/телефон");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("register_form_phone_from_inner_to_schema"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject client = (IObject) args[0];
                                        IObject resultObj = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField dateF = IOC.resolve(nestedFieldKey, "клиент/телефон");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        valueF.out(resultObj, dateF.in(client));
                                        return resultObj;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("register_form_password_from_schema_to_inner"),
                new SingletonStrategy(
                    new ApplyFunctionToArgumentsStrategy(
                        (args) -> {
                            try {
                                IObject el = (IObject) args[0];
                                IObject result = (IObject) args[1];
                                IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                IField passwordF = IOC.resolve(nestedFieldKey, "пароль");
                                IField valueF = IOC.resolve(nestedFieldKey, "value");
                                passwordF.out(result, valueF.in(el));
                                return null;
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }
                    )
                )
        );
        IOC.register(Keys.getOrAdd("register_form_password_confirm_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "подтверждение-пароля");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("register_form_old_password_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "старый-пароль");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("register_form_region_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "клиент/адрес/область");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("register_form_birth_date_from_schema_to_inner"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject el = (IObject) args[0];
                                        IObject result = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField passwordF = IOC.resolve(nestedFieldKey, "клиент/дата-рождения");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        passwordF.out(result, valueF.in(el));
                                        return null;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("register_form_birth_date_from_inner_to_schema"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject client = (IObject) args[0];
                                        IObject resultObj = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField dateF = IOC.resolve(nestedFieldKey, "клиент/дата-рождения");
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        valueF.out(resultObj, dateF.in(client));
                                        return resultObj;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
        IOC.register(Keys.getOrAdd("empty_field"),
                new SingletonStrategy(
                        new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        IObject resultObj = (IObject) args[1];
                                        IKey nestedFieldKey = Keys.getOrAdd(NestedField.class.getCanonicalName());
                                        IField valueF = IOC.resolve(nestedFieldKey, "value");
                                        valueF.out(resultObj, "");
                                        return resultObj;
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        )
                )
        );
    }
}
