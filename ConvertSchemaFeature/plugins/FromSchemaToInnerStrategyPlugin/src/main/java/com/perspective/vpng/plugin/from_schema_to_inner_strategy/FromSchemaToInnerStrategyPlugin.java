package com.perspective.vpng.plugin.from_schema_to_inner_strategy;

import com.perspective.vpng.strategy.from_schema_to_inner.FromSchemaToInnerStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class FromSchemaToInnerStrategyPlugin implements IPlugin {
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Default constructor
     * @param bootstrap element
     */
    public FromSchemaToInnerStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("FromSchemaToInnerConvertStrategyPlugin");
            item
                .process(() -> {
                    try {
                        IField valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
                        Map<String, BiConsumer<IObject, IObject>> defaultTypes = new LinkedHashMap<>();


                        defaultTypes.put("добавить-метку", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "добавить-метку");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("метка", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "метка");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("лицевой-счет", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/одной-строкой");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("фамилия", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/фамилия");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("имя", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/имя");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("отчество", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/отчество");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("фио", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/одной-строкой");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("адрес", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/одной-строкой");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("дом", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/дом");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("корпус", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/корпус");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("квартира", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/квартира");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("телефон", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/одной-строкой");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("email", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "email");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("месяц", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "период-оплаты/месяц");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("год", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "период-оплаты/год");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });
                        defaultTypes.put("всего-к-оплате", (el, res) -> {
                            try {
                                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "всего-к-оплате");
                                field.out(res, valueF.in(el));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        });

                        IOC.register(Keys.getOrAdd("SchemaInner"), new FromSchemaToInnerStrategy(defaultTypes));
                    } catch (ResolutionException ex) {
                        throw new ActionExecuteException(
                                "FromSchemaToInnerConvertStrategyPlugin plugin can't load!", ex
                        );
                    } catch (Exception ex) {
                        throw new ActionExecuteException(ex.getMessage(), ex);
                    }
                });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
