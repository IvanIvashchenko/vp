package com.perspective.vpng.strategy.from_schema_to_scope;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.field.field.Field;
import info.smart_tools.smartactors.iobject.ds_object.DSObject;
import info.smart_tools.smartactors.iobject.field_name.FieldName;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
public class FromSchemaToScopeStrategyTest {

    private final IResolveDependencyStrategy strategy;


    public FromSchemaToScopeStrategyTest() throws ResolutionException, InvalidArgumentException {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        IKey fieldKey = mock(IKey.class);
        when(Keys.getOrAdd(IField.class.getCanonicalName())).thenReturn(fieldKey);
        when(IOC.resolve(eq(fieldKey), anyString())).thenAnswer((Answer) invocation ->
                new Field(new FieldName((String) invocation.getArguments()[1])));

        strategy = new FromSchemaToScopeStrategy();
    }

    @Test
    public void should_CreateScopeFromSchema()
            throws InvalidArgumentException, ReadValueException, ResolveDependencyStrategyException {

        IObject schema = new DSObject(
                "{\"schema\": [\n" +
                "        {\n" +
                "            \"name\": \"лицевой-счет\",\n" +
                "            \"constraints\": [],\n" +
                "            \"value\": 124125\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"фамилия\",\n" +
                "            \"value\": \"Абдурахман\",\n" +
                "            \"constraints\": [\n" +
                "                {\n" +
                "                    \"constraint\": \"обязательное && русский\",\n" +
                "                    \"error\": \"wrong_lastname\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"имя\",\n" +
                "            \"value\": \"Володя\",\n" +
                "            \"constraints\": [\n" +
                "                {\n" +
                "                    \"constraint\": \"обязательное && русский\",\n" +
                "                    \"error\": \"wrong_name\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"отчество\",\n" +
                "            \"value\": \"Ольгович\",\n" +
                "            \"constraints\": [\n" +
                "                {\n" +
                "                    \"constraint\": \"обязательное && русский\",\n" +
                "                    \"error\": \"wrong_patronym\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"улица\",\n" +
                "            \"value\": \"Забугорная\",\n" +
                "            \"constraints\": [\n" +
                "                {\n" +
                "                    \"constraint\": \"адрес\",\n" +
                "                    \"error\": \"wrong_street\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"дом\",\n" +
                "            \"value\": 11,\n" +
                "            \"constraints\": [\n" +
                "                {\n" +
                "                    \"constraint\": \"дом\",\n" +
                "                    \"error\": \"wrong_house\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"корпус\",\n" +
                "            \"constraints\": []\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"квартира\",\n" +
                "            \"value\": 12,\n" +
                "            \"constraints\": []\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"месяц\",\n" +
                "            \"constraints\": []\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"год\",\n" +
                "            \"constraints\": []\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"всего-к-оплате\",\n" +
                "            \"value\": 412412" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"услуги\",\n" +
                "            \"unique\": [\"наименование-услуги\"],\n" +
                "            \"columns\": [\n" +
                "                {\n" +
                "                    \"name\": \"наименование-услуги\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"к-оплате\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"приборы-учета\",\n" +
                "            \"unique\": [\"номер-прибора-учета\"],\n" +
                "            \"columns\": [\n" +
                "                {\n" +
                "                    \"name\": \"тип-прибора-учета\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"группа\",\n" +
                "                    \"visible\": false\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"единица-измерения\",\n" +
                "                    \"visible\": false\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"номер-прибора-учета\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"наименование-услуги\",\n" +
                "                    \"visible\": false\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"предыдущие-показания\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"текущие-показания\"\n" +
                "                }\n" +
                "            ],\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"тип-прибора-учета\": \"Холодная вода\",\n" +
                "                    \"единица-измерения\": \"м3\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"группа\": \"1\",\n" +
                "                    \"единица-измерения\": \"м3\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"тип-прибора-учета\": \"Горячая вода\",\n" +
                "                    \"единица-измерения\": \"м3\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"группа\": \"1\",\n" +
                "                    \"тип-прибора-учета\": \"Горячая вода\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "      ]}");

        Map<String, Object> scope = strategy.resolve(schema.getValue(new FieldName("schema")));
        assertEquals(124125, scope.get("лицевой-счет"));
        assertEquals("Абдурахман", scope.get("фамилия"));
        assertEquals("Володя", scope.get("имя"));
        assertEquals("Ольгович", scope.get("отчество"));
        assertEquals("Забугорная", scope.get("улица"));
        assertEquals(11, scope.get("дом"));
        assertEquals(null, scope.get("копус"));
        assertEquals(12, scope.get("квартира"));
        assertEquals(null, scope.get("месяц"));
        assertEquals(null, scope.get("год"));
        assertEquals(412412, scope.get("всего-к-оплате"));
        assertFalse(scope.containsKey("услуги"));
        Map<String, List<Object>> meteringDevicesScope = new HashMap<String, List<Object>>() {{
            put("тип-прибора-учета", Arrays.asList("Холодная вода", null, "Горячая вода", "Горячая вода"));
            put("группа", Arrays.asList(null, "1", null, "1"));
            put("единица-измерения", Arrays.asList("м3", "м3", "м3", null));
            put("номер-прибора-учета", Arrays.asList(null, null, null, null));
            put("наименование-услуги", Arrays.asList("водоснабжение и водоотведение", "водоснабжение и водоотведение",
                    "водоснабжение и водоотведение", "водоснабжение и водоотведение"));
            put("предыдущие-показания", Arrays.asList(null, null, null, null));
            put("текущие-показания", Arrays.asList(null, null, null, null));
        }};
        assertEquals(meteringDevicesScope, scope.get("приборы-учета"));
    }

    @Test
    public void should_ThrowException_WithReason_IllegalArguments() {
        try {
            Map<String, Object> schema = Collections.emptyMap();
            strategy.resolve(schema);
            fail("Expected exception!");
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(ex.getCause().getClass(), ClassCastException.class);
        }
    }

    @Test
    public void should_ThrowException_WithReason_GivenSchema_IsNull() {
        try {
            strategy.resolve(null);
            fail("Expected exception!");
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(ex.getCause().getClass(), NullPointerException.class);
        }
    }

}
