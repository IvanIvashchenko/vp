package com.perspective.vpng.strategy.from_schema_to_scope;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.*;

public class FromSchemaToScopeStrategy implements IResolveDependencyStrategy {

    private final IKey fieldKey;

    private final IField nameF;
    private final IField valueF;
    private final IField valuesF;
    private final IField columnsF;

    public FromSchemaToScopeStrategy() throws ResolutionException {
        fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        this.nameF = IOC.resolve(fieldKey, "name");
        this.valueF = IOC.resolve(fieldKey, "value");
        this.valuesF = IOC.resolve(fieldKey, "values");
        this.columnsF = IOC.resolve(fieldKey, "columns");
    }

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            List<IObject> schema = (List) args[0];
            Map<String, Object> scope = new HashMap<>(schema.size());
            for (IObject block : schema) {
                List<IObject> columns = columnsF.in(block);
                if (columns != null) {
                    Map<String, List> table = new HashMap<>(columns.size());
                    List<IObject> rows = valuesF.in(block);
                    if (rows == null || rows.isEmpty()) {
                        continue;
                    }
                    for (IObject column : columns) {
                        String columnName = nameF.in(column);
                        List<Object> columnValues = new ArrayList<>(rows.size());
                        IField valueField = IOC.resolve(fieldKey, columnName);
                        for (IObject row : rows) {
                            Object value = valueField.in(row);
                            columnValues.add(value);
                        }
                        table.put(columnName, columnValues);
                    }
                    scope.put(nameF.in(block), table);
                    continue;
                }
                Object value = valueF.in(block);
                scope.put(nameF.in(block), value);
            }
            return (T) scope;
        } catch (ClassCastException ex) {
            throw new ResolveDependencyStrategyException("Invalid format of the given schema!" + ex.getMessage(), ex);
        } catch (NullPointerException ex) {
            throw new ResolveDependencyStrategyException("The given schema is null!" + ex.getMessage(), ex);
        } catch (ReadValueException | InvalidArgumentException | ResolutionException ex) {
            throw new ResolveDependencyStrategyException("Failed converted the give schema to scope: "
                    + ex.getMessage(), ex);
        }
    }

}
