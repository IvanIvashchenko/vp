package com.perspective.vpng.strategy.from_schema_to_inner;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field.field.Field;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ds_object.DSObject;
import info.smart_tools.smartactors.iobject.field_name.FieldName;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;

import org.junit.Before;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class FromSchemaToInnerStrategyTest {
    private IResolveDependencyStrategy strategy;

    @Before
    public void setUp() throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();
        bootstrap.start();

        Field valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
        Map<String, BiConsumer<IObject, IObject>> defaultTypes = new LinkedHashMap<>();
        defaultTypes.put("лицевой-счет", (el, res) -> {
            try {
                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/лицевой-счет/одной-строкой");
                field.out(res, valueF.in(el));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        defaultTypes.put("фамилия", (el, res) -> {
            try {
                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/фамилия");
                field.out(res, valueF.in(el));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        defaultTypes.put("имя", (el, res) -> {
            try {
                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/имя");
                field.out(res, valueF.in(el));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        defaultTypes.put("отчество", (el, res) -> {
            try {
                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/отчество");
                field.out(res, valueF.in(el));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        defaultTypes.put("адрес", (el, res) -> {
            try {
                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/одной-строкой");
                field.out(res, valueF.in(el));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        defaultTypes.put("месяц", (el, res) -> {
            try {
                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "период-оплаты/месяц");
                field.out(res, valueF.in(el));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        defaultTypes.put("год", (el, res) -> {
            try {
                NestedField field = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "период-оплаты/год");
                field.out(res, valueF.in(el));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        strategy = new FromSchemaToInnerStrategy(defaultTypes);
    }

    @Test
    public void shouldCreateInvoice() throws Exception {
        IObject schema = new DSObject(
                "{\"schema\": [" +
                    "{" +
                        "\"name\": \"лицевой-счет\"," +
                        "\"value\": \"123\"" +
                    "}," +
                    "{" +
                        "\"name\": \"фамилия\"," +
                        "\"value\": \"йцукен\"" +
                    "}," +
                    "{" +
                        "\"name\": \"имя\"," +
                        "\"value\": \"йцукен\"" +
                    "}," +
                    "{" +
                        "\"name\": \"отчество\"," +
                        "\"value\": \"йцукен\"" +
                    "}," +
                    "{" +
                        "\"name\": \"адрес\"," +
                        "\"value\": \"выаыа\"" +
                    "}," +
                    "{" +
                        "\"name\": \"месяц\"," +
                        "\"value\": \"2\"" +
                    "}," +
                    "{" +
                        "\"name\": \"год\"," +
                        "\"value\": \"2\"" +
                    "}," +
                    "{" +
                        "\"name\": \"услуги\"," +
                        "\"columns\": [" +
                            "{" +
                                "\"name\": \"наименование\"" +
                            "}" +
                        "]," +
                        "\"values\": [" +
                            "{" +
                                "\"наименование\": \"Капитальный ремонт\"," +
                                "\"сумма\": \"23\"" +
                            "}" +
                        "]" +
                    "}" +
                "]}");
        IObject result = strategy.resolve(schema.getValue(new FieldName("schema")));

        assertTrue(((IObject)result.getValue(new FieldName("период-оплаты"))).getValue(new FieldName("год")).equals("2"));
    }

    @Test
    public void shouldResolveSimpleObject_When_ConvertFunctionIsExist() throws Exception {
        IObject fromSchema = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                "{" +
                "  \"schema\":" +
                "    [" +
                "        {" +
                "            \"name\": \"простое поле в схеме\"," +
                "            \"from-schema\": \"trule\"" +
                "        }" +
                "    ]" +
                "}"
        );

        IResolveDependencyStrategy trule = mock(IResolveDependencyStrategy.class);
        IOC.register(Keys.getOrAdd("trule"), new ApplyFunctionToArgumentsStrategy(
                (args) -> trule
        ));
        when(trule.resolve(any(IObject.class), any(IObject.class))).thenReturn(null);

        IObject schema = strategy.resolve(fromSchema.getValue(new FieldName("schema")));
        verify(trule).resolve(any(IObject.class),any(IObject.class));
    }
}
