package com.perspective.vpng.strategy.from_schema_to_inner.exception;

public class ConvertSchemaException extends Exception {
    public ConvertSchemaException(String message) {
        super(message);
    }

    public ConvertSchemaException(String message, Throwable cause) {
        super(message, cause);
    }
}
