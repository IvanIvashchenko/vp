package com.perspective.vpng.strategy.from_schema_to_inner;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject_wrapper.IObjectWrapper;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

public class FromSchemaToInnerStrategy implements IResolveDependencyStrategy {
    private Map<String, BiConsumer<IObject, IObject>> defaultTypes = new LinkedHashMap<>();
    private static IField nameF;
    private static IField valuesF;
    private static IField servicesF;
    private static IField fromSchemaF;
    private static IField columnsF;

    public FromSchemaToInnerStrategy(Map<String, BiConsumer<IObject, IObject>> defaultTypes) {
        try {
            this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
            this.valuesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "values");
            this.servicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "услуги");
            this.fromSchemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "from-schema");
            this.columnsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "columns");
            this.defaultTypes = defaultTypes;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> T resolve(Object... objects) throws ResolveDependencyStrategyException {
        try {
            List<IObject> schema = (List) objects[0];
            IObject result = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

            for (IObject el : schema) {
                String elName = nameF.in(el);

                String rule = fromSchemaF.in(el);
                if (rule != null) {
                    IResolveDependencyStrategy strategy = IOC.resolve(Keys.getOrAdd(rule));
                    strategy.resolve(el, result);
                    continue;
                }

                if (defaultTypes.containsKey(elName)) {
                    defaultTypes.get(elName).accept(el, result);
                }

                if (columnsF.in(el) != null) {
                    List<IObject> resultServices = servicesF.in(result);
                    if (resultServices == null) {
                        resultServices = new LinkedList<>();
                        servicesF.out(result, resultServices);
                    }
                    List<IObject> elServices = valuesF.in(el);
                    resultServices.addAll(elServices);
                }
            }
            return (T) result;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException("Failed to convert schema to inner format", e);
        }
    }
}
