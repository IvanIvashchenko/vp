package com.perspective.vpng.strategy.from_inner_to_schema.exception;

/**
 * Exception for {@link com.perspective.vpng.strategy.from_inner_to_schema.ConverterFunction}
 */
public class ConverterFunctionException extends Exception {

    public ConverterFunctionException(final String message) {
        super(message);
    }

    public ConverterFunctionException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
