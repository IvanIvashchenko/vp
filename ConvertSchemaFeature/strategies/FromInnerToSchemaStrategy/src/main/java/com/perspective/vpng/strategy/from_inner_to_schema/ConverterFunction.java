package com.perspective.vpng.strategy.from_inner_to_schema;

import com.perspective.vpng.strategy.from_inner_to_schema.exception.ConverterFunctionException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

@FunctionalInterface
public interface ConverterFunction {

    /**
     * Converts object from inner format to schema by schema sample
     * @param inner object into inner format
     * @param schemaObjIn element of schema from database
     * @return element of schema for client
     * @throws ConverterFunctionException
     */
    IObject apply(IObject inner, IObject schemaObjIn) throws ConverterFunctionException;
}
