package com.perspective.vpng.strategy.from_inner_to_schema;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Strategy for convert object from inner format to schema.
 */
public class FromInnerToSchemaStrategy implements IResolveDependencyStrategy {

    private Map<String, ConverterFunction> defaultTypes = new HashMap<>();
    private final ICachedCollection cachedCollection;
    private final String chooseSchemaStrategyName;
    private final IField providerIdF;
    private final IField schemasF;
    private final IField nameF;
    private final IField toSchemaF;
    private final IField guidF;

    public FromInnerToSchemaStrategy(
        final Map<String, ConverterFunction> defaultTypes, final String collectionName, final String keyName,
        final String chooseSchemaStrategyName
    ) throws ResolveDependencyStrategyException {

        this.defaultTypes = defaultTypes;
        try {
            this.cachedCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), collectionName, keyName);
            this.chooseSchemaStrategyName = chooseSchemaStrategyName;
            this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
            this.schemasF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schemas");
            this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
            this.toSchemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "to-schema");
            this.guidF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "guid");
        } catch (ResolutionException e) {
            throw new ResolveDependencyStrategyException("Can't create FromInnerToSchemaStrategy", e);
        }
    }

    @Override
    public <T> T resolve(final Object... objects) throws ResolveDependencyStrategyException {

        try {
            IObject innerObject = (IObject) objects[0];
            List<IObject> resultSchema = new ArrayList<>();
            List<IObject> providerSchema;
            if (objects.length == 1) {
                IObject provider = IOC.resolve(
                        Keys.getOrAdd("SelectionProviderStrategy"),
                        new Object[]{cachedCollection.getItems(providerIdF.in(innerObject))}
                );
                List<List<IObject>> providerSchemas = schemasF.in(provider);
                providerSchema = IOC.resolve(Keys.getOrAdd("ChooseSchemaStrategy"), chooseSchemaStrategyName, providerSchemas);
            } else {
                providerSchema = (List) objects[1];
            }

            for (IObject schemaObj : providerSchema) {
                if (toSchemaF.in(schemaObj) != null) {
                    IResolveDependencyStrategy customStrategy = IOC.resolve(Keys.getOrAdd(toSchemaF.in(schemaObj)));
                    resultSchema.add(customStrategy.resolve(innerObject, schemaObj));
                    continue;
                }

                String elName = nameF.in(schemaObj);
                if (defaultTypes.containsKey(elName)) {
                    resultSchema.add(defaultTypes.get(elName).apply(innerObject, schemaObj));
                }
            }
            //TODO:: think about use it in separate handler
            if (guidF.in(innerObject) != null) {
                IObject guidObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"name\": \"guid\", \"value\": \"" + guidF.in(innerObject) + "\", \"visible\": false}"
                );
                resultSchema.add(guidObject);
            }

            return (T) resultSchema;
        } catch (Exception e) {
            throw new ResolveDependencyStrategyException("Failed to convert inner format object to schema", e);
        }
    }
}
