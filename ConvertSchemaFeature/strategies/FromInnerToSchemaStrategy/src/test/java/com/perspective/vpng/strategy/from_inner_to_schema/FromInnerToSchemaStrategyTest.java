package com.perspective.vpng.strategy.from_inner_to_schema;

import com.perspective.vpng.strategy.from_inner_to_schema.exception.ConverterFunctionException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class FromInnerToSchemaStrategyTest {

    private FromInnerToSchemaStrategy strategy;
    private IField valueF;
    private IField nameF;
    private IField valuesF;
    private IField columnsF;
    private ICachedCollection cachedCollection;

    @BeforeClass
    public static void prepareIOC() throws Exception {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {

        cachedCollection = mock(ICachedCollection.class);

        IOC.register(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), new ApplyFunctionToArgumentsStrategy(
            (args) -> cachedCollection
        ));
        IOC.register(Keys.getOrAdd("ChooseSchemaStrategy"), new ApplyFunctionToArgumentsStrategy(
            (args) -> ((List) args[1]).get(0)
        ));
        IOC.register(Keys.getOrAdd("SelectionProviderStrategy"), new ApplyFunctionToArgumentsStrategy(
            (args) -> ((List) args[0]).get(0)
        ));

        valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
        nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        valuesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "values");
        columnsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "columns");


        Map<String, ConverterFunction> defaultTypes = new HashMap<>();
        defaultTypes.put("простое поле в схеме", (inner, schemaObjIn) -> {
            try {
                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "простое-поле");
                return convertSingle(inner, schemaObjIn, innerField);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        defaultTypes.put("вложенное поле в схеме", (inner, schemaObjIn) -> {
            try {
                IField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "вложенное-поле/второй-уровень");
                return convertSingle(inner, schemaObjIn, innerField);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        defaultTypes.put("услуги", (inner, schemaObjIn) -> {
            try {
                return convertTable(inner, schemaObjIn, "услуги");
            } catch (Exception e) {
                throw new ConverterFunctionException("Can't convert services field from inner format to schema", e);
            }
        });

        defaultTypes.put("приборы-учета", (inner, schemaObjIn) -> {
            try {
                return convertTable(inner, schemaObjIn, "приборы-учета");
            } catch (Exception e) {
                throw new ConverterFunctionException("Can't convert meters field from inner format to schema", e);
            }
        });

        strategy = new FromInnerToSchemaStrategy(defaultTypes, "provider", "name", "strategy");
    }

    @Test
    public void shouldResolveSimpleObject_When_ConvertFunctionIsExist() throws Exception {

        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{\n" +
                "  \"name\": \"jekzhilischnik710\",\n" +
                "  \"schemas\": [\n" +
                "    [\n" +
                "        {\n" +
                "            \"name\": \"простое поле в схеме\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"вложенное поле в схеме\"\n" +
                "        }\n" +
                "    ]\n" +
                "  ]\n" +
                "}"
        );
        when(cachedCollection.getItems(anyString())).thenReturn(Collections.singletonList(provider));

        IObject innerObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"поставщик\": {\"id\": \"jekzhilishnik710\"}," +
                "\"простое-поле\": \"text\"," +
                "\"вложенное-поле\": {\"второй-уровень\": \"2015\"}" +
            "}"
        );
        List<IObject> schema = strategy.resolve(innerObject);

        assertEquals(schema.size(), 2);
        IObject simpleFieldObj = schema.get(0);
        assertEquals(nameF.in(simpleFieldObj), "простое поле в схеме");
        assertEquals(valueF.in(simpleFieldObj), "text");
        simpleFieldObj = schema.get(1);
        assertEquals(nameF.in(simpleFieldObj), "вложенное поле в схеме");
        assertEquals(valueF.in(simpleFieldObj), "2015");
    }

    @Test
    public void shouldResolveObjectWithCustomStrategy_When_ToSchemaIsExist() throws Exception {

        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{\n" +
                "  \"name\": \"jekzhilischnik710\",\n" +
                "  \"schemas\": [\n" +
                "    [\n" +
                "        {\n" +
                "            \"name\": \"простое поле в схеме\",\n" +
                "            \"to-schema\": \"trule\"\n" +
                "        }\n" +
                "    ]\n" +
                "  ]\n" +
                "}"
        );
        when(cachedCollection.getItems(anyString())).thenReturn(Collections.singletonList(provider));

        IObject customSchemaObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "        {\n" +
            "            \"name\": \"простое поле в схеме\",\n" +
            "            \"value\": \"custom value from trule\"\n" +
            "        }\n"
        );
        IResolveDependencyStrategy trule = mock(IResolveDependencyStrategy.class);
        IOC.register(Keys.getOrAdd("trule"), new ApplyFunctionToArgumentsStrategy(
            (args) -> trule
        ));
        when(trule.resolve(any(IObject.class), any(IObject.class))).thenReturn(customSchemaObject);

        IObject innerObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"поставщик\": {\"id\": \"jekzhilishnik710\"}," +
                "\"простое-поле\": \"text\"" +
                "}"
        );
        List<IObject> schema = strategy.resolve(innerObject);

        assertEquals(schema.size(), 1);
        IObject simpleFieldObj = schema.get(0);
        assertEquals(nameF.in(simpleFieldObj), "простое поле в схеме");
        assertEquals(valueF.in(simpleFieldObj), "custom value from trule");
    }

    @Test
    public void shouldResolveObjectWithTableFields() throws Exception {

        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{\n" +
                "  \"name\": \"rctomsk\",\n" +
                "  \"schemas\": [\n" +
                "      [\n" +
                "        {\n" +
                "            \"name\": \"лицевой-счет\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"фамилия\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"имя\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"отчество\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"адрес\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"месяц\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"год\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"услуги\",\n" +
                "            \"columns\": [\n" +
                "                {\n" +
                "                    \"name\": \"наименование-услуги\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"всего-к-оплате\"\n" +
                "                }\n" +
                "            ],\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"наименование-услуги\": \"Водоснабжение и водоотведение\",\n" +
                "                    \"всего-к-оплате\": \"\"\n" +
                "                }\n" +
                "            ]\n" +
                "        },\n" +
                "        {\n" +
                "            \"name\": \"приборы-учета\",\n" +
                "            \"columns\": [\n" +
                "                {\n" +
                "                    \"name\": \"тип-прибора-учета\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"группа\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"единица-измерения\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"номер-прибора-учета\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"наименование-услуги\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"предыдущие-показания\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"name\": \"текущие-показания\"\n" +
                "                }\n" +
                "            ],\n" +
                "            \"values\": [\n" +
                "                {\n" +
                "                    \"группа\": \"1\",\n" +
                "                    \"тип-прибора-учета\": \"Холодная вода\",\n" +
                "                    \"единица-измерения\": \"м3\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"группа\": \"1\",\n" +
                "                    \"тип-прибора-учета\": \"Холодная вода\",\n" +
                "                    \"единица-измерения\": \"м3\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"группа\": \"1\",\n" +
                "                    \"тип-прибора-учета\": \"Горячая вода\",\n" +
                "                    \"единица-измерения\": \"м3\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"группа\": \"1\",\n" +
                "                    \"тип-прибора-учета\": \"Горячая вода\",\n" +
                "                    \"единица-измерения\": \"м3\",\n" +
                "                    \"наименование-услуги\": \"водоснабжение и водоотведение\"\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "      ]\n" +
                "  ]\n" +
                "}"
        );
        when(cachedCollection.getItems(anyString())).thenReturn(Collections.singletonList(provider));

        IObject innerObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{\n" +
                "\"rctomskID\":\"a01bad3d-5dbc-485a-b0a4-9168549e5a5e\",\n" +
                "\"услуги\":[\n" +
                "{\n" +
                "\"группа\":\"1\",\n" +
                "\"тип-прибора-учета\":\"Холодная вода\",\n" +
                "\"единица-измерения\":\"м3\",\n" +
                "\"номер-прибора-учета\":\"808276\",\n" +
                "\"наименование-услуги\":\"водоснабжение и водоотведение\",\n" +
                "\"предыдущие-показания\":\"19\"\n" +
                "},\n" +
                "{\n" +
                "\"группа\":\"1\",\n" +
                "\"тип-прибора-учета\":\"Холодная вода\",\n" +
                "\"единица-измерения\":\"м3\",\n" +
                "\"номер-прибора-учета\":\"808376\",\n" +
                "\"наименование-услуги\":\"водоснабжение и водоотведение\",\n" +
                "\"предыдущие-показания\":\"83\"\n" +
                "},\n" +
                "{\n" +
                "\"группа\":\"1\",\n" +
                "\"тип-прибора-учета\":\"Горячая вода\",\n" +
                "\"единица-измерения\":\"м3\",\n" +
                "\"номер-прибора-учета\":\"884317\",\n" +
                "\"наименование-услуги\":\"водоснабжение и водоотведение\",\n" +
                "\"предыдущие-показания\":\"50\"\n" +
                "},\n" +
                "{\n" +
                "\"группа\":\"1\",\n" +
                "\"тип-прибора-учета\":\"Горячая вода\",\n" +
                "\"единица-измерения\":\"м3\",\n" +
                "\"номер-прибора-учета\":\"885732\",\n" +
                "\"наименование-услуги\":\"водоснабжение и водоотведение\",\n" +
                "\"предыдущие-показания\":\"22\"\n" +
                "}\n" +
                "],\n" +
                "\"поставщик\":{\n" +
                "\"id\":\"rctomsk\"\n" +
                "}\n" +
                "}"
        );
        List<IObject> schema = strategy.resolve(innerObject);
        assertEquals(schema.size(), 2);
        IObject servicesObj = schema.get(0);
        List<IObject> columns = columnsF.in(servicesObj);
        List<IObject> values = valuesF.in(servicesObj);
        assertEquals(columns.size(), 2);
        assertEquals(values.size(), 4);
        IObject metersObj = schema.get(1);
        columns = columnsF.in(metersObj);
        values = valuesF.in(metersObj);
        assertEquals(columns.size(), 7);
        assertEquals(values.size(), 4);
        IObject value = values.get(0);
        for (IObject column : columns) {
            String name = nameF.in(column);
            IField customNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), name);
            assertNotNull(customNameF.in(value));
        }
    }

    private IObject convertSingle(final IObject inner, final IObject schemaObjIn, final IField innerField)
        throws ResolutionException, ReadValueException, InvalidArgumentException, ChangeValueException {

        IObject resultSchemaEl = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        nameF.out(resultSchemaEl, nameF.in(schemaObjIn));
        String value;
        try {
             value = innerField.in(inner);
        } catch (InvalidArgumentException e) {
            valueF.out(resultSchemaEl, "");
            return resultSchemaEl;
        }
        valueF.out(resultSchemaEl, value);

        return resultSchemaEl;
    }

    private IObject convertTable(final IObject inner, final IObject schemaObjIn, final String innerName)
        throws ResolutionException, ReadValueException, InvalidArgumentException, ChangeValueException, ResolveDependencyStrategyException {

        IObject schemaObjOut = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IField toSchemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "to-schema");
        IField listF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "список");
        listF.out(schemaObjOut, listF.in(schemaObjIn));
        NestedField innerField = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "услуги");
        List<IObject> services = innerField.in(inner);
        List<IObject> columnsIn = columnsF.in(schemaObjIn);
        List<IObject> columnsOut = new ArrayList<>();
        List<String> valuesKeyNames = new ArrayList<>();
        //for each columns copy column name
        //and remember this name as a key for corresponding object into values array
        for (IObject columnIn : columnsIn) {
            IObject columnOut;
            if (toSchemaF.in(columnIn) != null) {
                IResolveDependencyStrategy customStrategy = IOC.resolve(Keys.getOrAdd(toSchemaF.in(columnIn)));
                columnOut = customStrategy.resolve(inner, schemaObjIn);
            } else {
                columnOut = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                nameF.out(columnOut, nameF.in(columnIn));
            }
            columnsOut.add(columnOut);
            valuesKeyNames.add(nameF.in(columnIn));
        }
        columnsF.out(schemaObjOut, columnsOut);
        List<IObject> valuesOut = new ArrayList<>();

        //Services from inner format. For each service insert to values array one object with
        //number of fields = number of objects into columns
        for (IObject service : services) {
            IObject valueOut = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            for (String valueKeyName : valuesKeyNames) {
                IField valueField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), valueKeyName);
                valueField.out(valueOut, valueField.in(service) != null ? valueField.in(service) : "");
            }
            valuesOut.add(valueOut);
        }
        valuesF.out(schemaObjOut, valuesOut);
        nameF.out(schemaObjOut, innerName);

        return schemaObjOut;
    }
}
