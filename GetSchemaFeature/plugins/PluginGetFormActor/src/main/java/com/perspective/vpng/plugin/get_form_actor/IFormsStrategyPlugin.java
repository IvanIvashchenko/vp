package com.perspective.vpng.plugin.get_form_actor;

import com.perspective.vpng.actor.get_form.strategy.ChooseInvoiceFormStrategy;
import com.perspective.vpng.actor.get_form.strategy.ChooseMetersFormStrategy;
import com.perspective.vpng.actor.get_form.strategy.FirstItemStrategy;
import com.perspective.vpng.actor.get_form.strategy.IChooseFormStrategy;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.Map;

public class IFormsStrategyPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor
     * @param bootstrap the bootstrap
     */
    public IFormsStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("IChooseFormStrategyPlugin");
            Map<String, IChooseFormStrategy> strategyMap = new HashMap<>();
            item
//                    .after("IOC")
                    .process(() -> {
                        try {
                            strategyMap.put("chooseMetersFormStrategy", new ChooseMetersFormStrategy());
                            strategyMap.put("firstItemStrategy", new FirstItemStrategy());
                            strategyMap.put("chooseInvoiceFormStrategy", new ChooseInvoiceFormStrategy());
                            IKey strategyKey = Keys.getOrAdd(IChooseFormStrategy.class.getCanonicalName());
                            IOC.register(strategyKey, new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        try {
                                            String strategyName = (String) args[0];
                                            return strategyMap.get(strategyName);
                                        } catch (Exception e) {
                                            throw new RuntimeException(e);
                                        }
                                    }));
                        } catch (Exception e) {
                            throw new ActionExecuteException("IChooseFormStrategyPlugin plugin can't load: " + e.getMessage(), e);
                        }
                    });
            bootstrap.add(item);
        } catch (Exception e) {
            throw new PluginException("Can't load GetFirstFormItemStrategy plugin", e);
        }
    }
}
