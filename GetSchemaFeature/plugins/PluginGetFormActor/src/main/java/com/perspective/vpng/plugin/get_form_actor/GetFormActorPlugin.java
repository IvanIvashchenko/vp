package com.perspective.vpng.plugin.get_form_actor;

import com.perspective.vpng.actor.get_form.GetFormActor;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;

/**
 * Plugin for register actor for get form
 */
public class GetFormActorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor
     * @param bootstrap the bootstrap
     */
    public GetFormActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("GetFormActorPlugin");
            item
//                .after("IOC")
//                .after("CreateCachedCollectionPlugin")
//                .before("starter")
                .process(() -> {
                    try {
                        IKey actorKey = Keys.getOrAdd(GetFormActor.class.getCanonicalName());
                        IOC.register(actorKey, new ApplyFunctionToArgumentsStrategy(
                                (args) -> {
                                    try {
                                        return new GetFormActor((IObject) args[0]);
                                    } catch (Exception e) {
                                        throw new RuntimeException(e);
                                    }
                                }));
                    } catch (ResolutionException e) {
                        throw new ActionExecuteException("GetFormActor plugin can't load: can't get GetFormActor key", e);
                    } catch (InvalidArgumentException e) {
                        throw new ActionExecuteException("GetFormActor plugin can't load: can't create rule", e);
                    } catch (RegistrationException e) {
                        throw new ActionExecuteException("GetFormActor plugin can't load: can't register new rule", e);
                    }
                });
            bootstrap.add(item);
        } catch (Exception e) {
            throw new PluginException("Can't load GetFormActor plugin", e);
        }
    }
}
