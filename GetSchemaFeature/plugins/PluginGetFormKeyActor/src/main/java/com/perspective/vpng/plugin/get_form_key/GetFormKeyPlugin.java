package com.perspective.vpng.plugin.get_form_key;

import com.perspective.vpng.actor.get_form_key.GetFormKeyActor;
import com.perspective.vpng.actor.get_form_key.exception.InitActorException;
import com.perspective.vpng.actor.get_form_key.wrapper.IActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetFormKeyPlugin implements IPlugin{
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    public GetFormKeyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("GetFormKeyActorPlugin");

            item
//                    .after("IOC")
//                    .before("starter")
                    .process(()-> {
                        try {
                            IKey ruleKey = Keys.getOrAdd("GetFormKeyActor");
                            IOC.register(ruleKey, new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        IObject params = (IObject) args[0];
                                        try {
                                            IField specialKeysF = IOC.resolve(
                                                    Keys.getOrAdd(IField.class.getCanonicalName()),
                                                    "specialKeys"
                                            );
                                            IField availableKeysF = IOC.resolve(
                                                Keys.getOrAdd(IField.class.getCanonicalName()),
                                                "availableKeys"
                                            );
                                            IField availableKeysMapF = IOC.resolve(
                                                Keys.getOrAdd(IField.class.getCanonicalName()),
                                                "availableKeysMap"
                                            );

                                            return new GetFormKeyActor(new IActorConfig() {
                                                @Override
                                                public List<String> getSpecialTypes() throws ReadValueException {
                                                    try {
                                                        return specialKeysF.in(params);
                                                    } catch (InvalidArgumentException ex) {
                                                        throw new ReadValueException(ex.getMessage());
                                                    }
                                                }

                                                @Override
                                                public Map<String, String> getAvailableKeys() throws ReadValueException {
                                                    try {
                                                        IObject availableKeysObj = availableKeysMapF.in(params);
                                                        List<String> availableKeys = availableKeysF.in(params);
                                                        Map<String, String> availableKeysMap = new HashMap<>();
                                                        for (String key : availableKeys) {
                                                            IField field = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), key);
                                                            availableKeysMap.put(key, field.in(availableKeysObj));
                                                        }
                                                        return availableKeysMap;
                                                    } catch (InvalidArgumentException | ResolutionException ex) {
                                                        throw new ReadValueException(ex.getMessage());
                                                    }
                                                }
                                            });
                                        } catch (InitActorException | ResolutionException ex) {
                                            throw new RuntimeException(ex);
                                        }
                                    }
                            ));
                        } catch (ResolutionException ex) {
                            throw new ActionExecuteException("Can't get key for load", ex);
                        } catch (InvalidArgumentException ex) {
                            throw new ActionExecuteException("Can't apply lambda", ex);
                        } catch (RegistrationException ex) {
                            throw new ActionExecuteException(ex.getMessage());
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't create BootstrapItem", ex);
        }
    }
}
