package com.perspective.vpng.plugin.add_meters_attributes_actor;

import com.perspective.vpng.actor.add_meters_attributes.AddMetersAttributesActor;
import com.perspective.vpng.actor.add_meters_attributes.wrapper.AddMetersAttributesConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class AddMetersAttributesActorPlugin extends BootstrapPlugin {

    public AddMetersAttributesActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("AddMetersAttributesActorPlugin")
    public void item() throws ResolutionException, RegistrationException, InvalidArgumentException {

        IOC.register(Keys.getOrAdd("addMetersAttributesActor"),
            new ApplyFunctionToArgumentsStrategy((args) -> {
                IObject config = (IObject) args[0];
                try {
                    IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                    IField collectionNameF = IOC.resolve(fieldKey, "collectionName");
                    IField cacheKeyF = IOC.resolve(fieldKey, "cacheKey");
                    return new AddMetersAttributesActor(new AddMetersAttributesConfig() {
                        @Override
                        public String getCollectionName() throws ReadValueException {
                            try {
                                return collectionNameF.in(config);
                            } catch (InvalidArgumentException ex) {
                                throw new ReadValueException("Can't read collection name!", ex);
                            }
                        }

                        @Override
                        public String getCacheKey() throws ReadValueException {
                            try {
                                return cacheKeyF.in(config);
                            } catch (InvalidArgumentException ex) {
                                throw new ReadValueException("Can't read cache key!", ex);
                            }
                        }
                    });
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            })
        );
    }
}
