package com.perspective.vpng.actor.add_meters_attributes;

import com.perspective.vpng.actor.add_meters_attributes.exception.AddMetersAttributesException;
import com.perspective.vpng.actor.add_meters_attributes.wrapper.AddMetersAttributesConfig;
import com.perspective.vpng.actor.add_meters_attributes.wrapper.AddMetersAttributesMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Actor for add some additional data to meters reading schema
 */
public class AddMetersAttributesActor {

    private ICachedCollection collection;
    private final IField startDayMeterReadingF;
    private final IField endDayMeterReadingF;

    public AddMetersAttributesActor(final AddMetersAttributesConfig config) throws AddMetersAttributesException {
        try {
            collection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), config.getCollectionName(), config.getCacheKey());
            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            this.startDayMeterReadingF = IOC.resolve(fieldKey, "startDayMeterReading");
            this.endDayMeterReadingF = IOC.resolve(fieldKey, "endDayMeterReading");
        } catch (ResolutionException | ReadValueException e) {
            throw new AddMetersAttributesException("Can't create AddMetersAttributesActor", e);
        }
    }

    /**
     * @param message contains schema and provider identifier
     * @throws AddMetersAttributesException if any error is occurred
     */
    public void addMetersAttributes(final AddMetersAttributesMessage message) throws AddMetersAttributesException {

        List<IObject> providers;
        try {
            providers = collection.getItems(message.getProviderId());
            if (providers.size() != 1) {
                throw new AddMetersAttributesException("Only one provider should be found by id " + message.getProviderId());
            }
            IObject provider = providers.get(0);
            int currentDay = LocalDateTime.now().getDayOfMonth();
            Integer startDayMeterReading = startDayMeterReadingF.in(provider);
            Integer endDayMeterReading = endDayMeterReadingF.in(provider);
            checkDays(startDayMeterReading, endDayMeterReading);
            //if both days are null actually
            if (startDayMeterReading == null) {
                //TODO:: Clarify about action here
                return;
            }
            if (checkStartBeforeEndCase(startDayMeterReading, endDayMeterReading, currentDay) ||
                checkEndBeforeStartCase(startDayMeterReading, endDayMeterReading, currentDay)
            ) {

                String startStr = "{\"name\": \"startDayMeterReading\", \"value\": \"" + startDayMeterReading + "\", \"visible\": false}";
                String endStr = "{\"name\": \"endDayMeterReading\", \"value\": \"" + endDayMeterReading + "\", \"visible\": false}";
                IObject start = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), startStr);
                IObject end = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), endStr);
                List<IObject> schema = message.getSchema();
                schema.add(start);
                schema.add(end);
            }
        } catch (ReadValueException | InvalidArgumentException | GetCacheItemException | ResolutionException e) {
            throw new AddMetersAttributesException("Error during addition meters attributes to form", e);
        }
    }

    private void checkDays(final Integer startDay, final Integer endDay) throws AddMetersAttributesException {
        if (startDay == null && endDay != null) {
            throw new AddMetersAttributesException("Start day of meter reading is null, but end day isn't");
        }
        if (startDay != null && endDay == null) {
            throw new AddMetersAttributesException("End day of meter reading is null, but start day isn't");
        }
    }

    private boolean checkStartBeforeEndCase(final Integer startDay, final Integer endDay, final int currentDay) {
        return (startDay <= endDay) && ((currentDay < startDay) || (endDay < currentDay));
    }

    private boolean checkEndBeforeStartCase(final Integer startDay, final Integer endDay, final int currentDay) {
        return (endDay <= startDay) && ((currentDay < endDay) || (startDay < currentDay));
    }
}
