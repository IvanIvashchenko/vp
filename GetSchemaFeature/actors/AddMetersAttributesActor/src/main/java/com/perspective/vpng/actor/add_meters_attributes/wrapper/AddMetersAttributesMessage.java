package com.perspective.vpng.actor.add_meters_attributes.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for {@link com.perspective.vpng.actor.add_meters_attributes.AddMetersAttributesActor} message
 */
public interface AddMetersAttributesMessage {

    /**
     * @return provider id for cache collection
     * @throws ReadValueException if any error is occurred
     */
    String getProviderId() throws ReadValueException;

    /**
     * @return schema for current provider
     * @throws ReadValueException if any error is occurred
     */
    List<IObject> getSchema() throws ReadValueException;
}
