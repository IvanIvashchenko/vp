package com.perspective.vpng.actor.add_meters_attributes.exception;

public class AddMetersAttributesException extends Exception {

    public AddMetersAttributesException(final String message) {
        super(message);
    }

    public AddMetersAttributesException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
