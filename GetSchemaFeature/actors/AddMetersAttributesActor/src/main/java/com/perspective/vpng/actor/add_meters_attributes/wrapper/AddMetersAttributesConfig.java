package com.perspective.vpng.actor.add_meters_attributes.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for {@link com.perspective.vpng.actor.add_meters_attributes.AddMetersAttributesActor} constructor
 */
public interface AddMetersAttributesConfig {

    /**
     * @return collection name
     * @throws ReadValueException if any error is occurred
     */
    String getCollectionName() throws ReadValueException;

    /**
     * @return cache key name
     * @throws ReadValueException if any error is occurred
     */
    String getCacheKey() throws ReadValueException;
}
