package com.perspective.vpng.actor.add_meters_attributes;

import com.perspective.vpng.actor.add_meters_attributes.exception.AddMetersAttributesException;
import com.perspective.vpng.actor.add_meters_attributes.wrapper.AddMetersAttributesConfig;
import com.perspective.vpng.actor.add_meters_attributes.wrapper.AddMetersAttributesMessage;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class AddMetersAttributesActorTest {

    private AddMetersAttributesActor actor;
    private IField nameF;
    private IField valueF;
    private static ICachedCollection collection;

    @BeforeClass
    public static void prepareIOC() throws Exception {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();

        bootstrap.start();

        collection = mock(ICachedCollection.class);
        IOC.register(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), new ApplyFunctionToArgumentsStrategy(
            (args) -> collection
        ));
    }

    @Before
    public void setUp() throws Exception {

        nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");

        AddMetersAttributesConfig config = mock(AddMetersAttributesConfig.class);
        when(config.getCacheKey()).thenReturn("name");
        when(config.getCollectionName()).thenReturn("collection");

        actor = new AddMetersAttributesActor(config);
    }

    @Test
    public void shouldSetStartEndBlocksToSchema_When_CurrentDateIsNotIntoInterval() throws Exception {

        List<IObject> schema = new ArrayList<>();
        AddMetersAttributesMessage message = mock(AddMetersAttributesMessage.class);
        when(message.getProviderId()).thenReturn("provider");
        when(message.getSchema()).thenReturn(schema);

        List<IObject> providers = new ArrayList<>();
        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"name\": \"provider\"," +
                "\"startDayMeterReading\": 32," +
                "\"endDayMeterReading\": 33" +
            "}"
        );
        providers.add(provider);
        when(collection.getItems(eq("provider"))).thenReturn(providers);
        actor.addMetersAttributes(message);

        assertEquals(schema.size(), 2);
        assertEquals(nameF.in(schema.get(0)), "startDayMeterReading");
        assertEquals(valueF.in(schema.get(0)), "32");
        assertEquals(nameF.in(schema.get(1)), "endDayMeterReading");
        assertEquals(valueF.in(schema.get(1)), "33");
    }

    @Test(expected = AddMetersAttributesException.class)
    public void shouldThrowException_When_StartDayIsNullAndEndDayIsNotNull() throws Exception {

        AddMetersAttributesMessage message = mock(AddMetersAttributesMessage.class);
        when(message.getProviderId()).thenReturn("provider");

        List<IObject> providers = new ArrayList<>();
        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"name\": \"provider\"," +
                "\"endDayMeterReading\": 31" +
            "}"
        );
        providers.add(provider);
        when(collection.getItems(eq("provider"))).thenReturn(providers);
        actor.addMetersAttributes(message);

        fail();
    }

    @Test(expected = AddMetersAttributesException.class)
    public void shouldThrowException_When_StartDayIsNotNullAndEndDayIsNull() throws Exception {

        AddMetersAttributesMessage message = mock(AddMetersAttributesMessage.class);
        when(message.getProviderId()).thenReturn("provider");

        List<IObject> providers = new ArrayList<>();
        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"name\": \"provider\"," +
                "\"startDayMeterReading\": 11" +
            "}"
        );
        providers.add(provider);
        when(collection.getItems(eq("provider"))).thenReturn(providers);
        actor.addMetersAttributes(message);

        fail();
    }

    @Test
    public void shouldDoNothing_When_BothDaysAreNull() throws Exception {

        AddMetersAttributesMessage message = mock(AddMetersAttributesMessage.class);
        when(message.getProviderId()).thenReturn("provider");

        List<IObject> providers = new ArrayList<>();
        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"name\": \"provider\"" +
            "}"
        );
        providers.add(provider);
        when(collection.getItems(eq("provider"))).thenReturn(providers);
        actor.addMetersAttributes(message);

        verify(message, never()).getSchema();
    }

    @Test
    public void shouldDoNothing_When_CurrentDateIsIntoInterval() throws Exception {

        AddMetersAttributesMessage message = mock(AddMetersAttributesMessage.class);
        when(message.getProviderId()).thenReturn("provider");

        List<IObject> providers = new ArrayList<>();
        IObject provider = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"name\": \"provider\"," +
                "\"startDayMeterReading\": 0," +
                "\"endDayMeterReading\": 32" +
            "}"
        );
        providers.add(provider);
        when(collection.getItems(eq("provider"))).thenReturn(providers);
        actor.addMetersAttributes(message);

        verify(message, never()).getSchema();
    }
}
