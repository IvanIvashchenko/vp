package com.perspective.vpng.actor.add_errors_to_schema;

import com.perspective.vpng.actor.add_errors_to_schema.wrapper.AddErrorsMessage;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field.field.Field;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.field_name.FieldName;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class AddErrorsToSchemaActorTest {
    private AddErrorsToSchemaActor actor;
    private static ICachedCollection collection = mock(ICachedCollection.class);

    @BeforeClass
    public static void prepareIOC() throws Exception{

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();

        bootstrap.start();
        IOC.register(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), new ApplyFunctionToArgumentsStrategy(
                (args) -> collection
        ));
        when(collection.getItems(any())).thenReturn(Collections.singletonList(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"message\": \"textMessage\"}")));
    }

    @Before
    public void setUp() throws Exception {
        IObject params = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{'collectionName': 'name', 'collectionKey': 'key'}".replace('\'', '"'));
        actor = new AddErrorsToSchemaActor(params);
    }

    @Test
    public void ShouldSetErrorToSimpleFieldToSchema() throws Exception {
        AddErrorsMessage message = mock(AddErrorsMessage.class);
        IObject schemaEl = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                ("{" +
                    "'constraints':" +
                        "[{" +
                            "'error': 'errorKey'" +
                        "}]" +
                "}").replace('\'', '"')
        );
        when(message.getSchema()).thenReturn(Collections.singletonList(schemaEl));
        actor.setErrorsToSchema(message);
        assertEquals(((List<IObject>) new NestedField("constraints").in(schemaEl)).get(0).getValue(new FieldName("error")), "textMessage");
    }

    @Test
    public void ShouldSetErrorToTableFieldToSchema() throws Exception {
        AddErrorsMessage message = mock(AddErrorsMessage.class);
        IObject schemaEl = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                ("{" +
                    "'columns':" +
                        "[" +
                            "{" +
                              "'constraints':" +
                                "[{" +
                                    "'error': 'errorKey'" +
                                "}]" +
                            "}" +
                        "]" +
                "}").replace('\'', '"')
        );
        when(message.getSchema()).thenReturn(Collections.singletonList(schemaEl));
        actor.setErrorsToSchema(message);
        assertEquals((((List<IObject>) new Field(new FieldName("constraints")).in(((List<IObject>) schemaEl.getValue(new FieldName("columns"))).get(0))).get(0)).getValue(new FieldName("error")), "textMessage");
    }
}
