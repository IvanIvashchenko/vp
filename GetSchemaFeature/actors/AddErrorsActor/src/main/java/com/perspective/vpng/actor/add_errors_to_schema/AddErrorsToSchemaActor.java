package com.perspective.vpng.actor.add_errors_to_schema;

import com.perspective.vpng.actor.add_errors_to_schema.wrapper.AddErrorsMessage;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;

public class AddErrorsToSchemaActor {
    private ICachedCollection errorCollection;
    private IField errorF;
    private IField constraintsF;
    private IField messageF;
    private IField columnsF;

    public AddErrorsToSchemaActor(IObject params) {
        try {
            IField collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            IField collectionKeyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionKey");
            this.errorCollection = IOC.resolve(Keys.getOrAdd(
                    ICachedCollection.class.getCanonicalName()),
                    collectionNameF.in(params),
                    collectionKeyF.in(params)
            );
            this.columnsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "columns");
            this.errorF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "error");
            this.constraintsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "constraints");
            this.messageF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "message");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void setErrorsToSchema(final AddErrorsMessage message) {
        try {
            List<IObject> schema = message.getSchema();
            setErrors(schema);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void setErrors(List<IObject> schema) {
        try {
            for (IObject schemaEl : schema) {
                List<IObject> columns = columnsF.in(schemaEl);
                if (columns != null) {
                    setErrors(columns);
                } else {
                    List<IObject> constraintsObject = constraintsF.in(schemaEl);
                    if (constraintsObject != null) {
                        for (IObject constrObj : constraintsObject) {
                            errorF.out(constrObj, messageF.in(errorCollection.getItems(errorF.in(constrObj)).get(0)));
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Failed to set errors to schema");
        }
    }
}
