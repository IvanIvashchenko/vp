package com.perspective.vpng.actor.add_errors_to_schema.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface AddErrorsMessage {
    List<IObject> getSchema() throws ReadValueException;
}
