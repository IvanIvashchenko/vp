package com.perspective.vpng.actor.get_form_key;

import com.perspective.vpng.actor.get_form_key.exception.GetFormKeyException;
import com.perspective.vpng.actor.get_form_key.exception.InitActorException;
import com.perspective.vpng.actor.get_form_key.wrapper.IActorConfig;
import com.perspective.vpng.actor.get_form_key.wrapper.IGetFormKeyAfterPaymentMessage;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class GetFormKeyActor {
    private Map<String, String> availableKeys;
    private List<String> specialTypes;

    public GetFormKeyActor(final IActorConfig config) throws InitActorException {
        try {
            this.specialTypes = Optional.ofNullable(config.getSpecialTypes())
                    .orElseThrow(() -> new InitActorException("Special types must not be a null!"));
            this.availableKeys = config.getAvailableKeys();
        } catch (ReadValueException ex) {
            throw new InitActorException(ex.getMessage());
        }
    }

    public void getFormKeyAfterPayment(final IGetFormKeyAfterPaymentMessage message) throws GetFormKeyException {
        try {
            String formType = Optional.ofNullable(message.getFormType())
                    .orElseThrow(() -> new GetFormKeyException("Type of form must be not a null!"));
            String formKey = availableKeys.get(formType);
            if (specialTypes.contains(formType)) {
                String role = message.getUserId() != null ? "User" : "Anonymous";
                formKey = formKey + role;
            }
            message.setFormKey(formKey);
        } catch (ReadValueException | ChangeValueException ex) {
            throw new GetFormKeyException(ex.getMessage());
        }
    }

}
