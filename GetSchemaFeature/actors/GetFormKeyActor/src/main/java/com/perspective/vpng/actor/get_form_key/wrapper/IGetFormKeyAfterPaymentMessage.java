package com.perspective.vpng.actor.get_form_key.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface IGetFormKeyAfterPaymentMessage {
    String getUserId() throws ReadValueException;
    String getFormType() throws ReadValueException;

    void setFormKey(String formKey) throws ChangeValueException;
}
