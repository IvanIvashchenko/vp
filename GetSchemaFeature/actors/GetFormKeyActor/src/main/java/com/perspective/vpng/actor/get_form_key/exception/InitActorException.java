package com.perspective.vpng.actor.get_form_key.exception;

public class InitActorException extends Exception {
    public InitActorException(String message) {
        super(message);
    }
}
