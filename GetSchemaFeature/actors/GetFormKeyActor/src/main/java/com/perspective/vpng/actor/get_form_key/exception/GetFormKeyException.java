package com.perspective.vpng.actor.get_form_key.exception;

public class GetFormKeyException extends Exception {
    public GetFormKeyException() {
        super();
    }

    public GetFormKeyException(String message) {
        super(message);
    }
}
