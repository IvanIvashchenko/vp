package com.perspective.vpng.actor.get_form_key.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;
import java.util.Map;

public interface IActorConfig {
    List<String> getSpecialTypes() throws ReadValueException;
    Map<String, String> getAvailableKeys() throws ReadValueException;
}
