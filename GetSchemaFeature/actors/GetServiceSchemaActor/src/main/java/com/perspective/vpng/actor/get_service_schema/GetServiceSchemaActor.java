package com.perspective.vpng.actor.get_service_schema;

import com.perspective.vpng.actor.get_service_schema.wrapper.GetServiceSchemaMessage;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class GetServiceSchemaActor {
    private IField schemaF;
    private ICachedCollection schemaCollection;

    public GetServiceSchemaActor(IObject params) {
        try {
            IField collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            IField collectionKeyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionKey");
            this.schemaCollection = IOC.resolve(Keys.getOrAdd(
                    ICachedCollection.class.getCanonicalName()),
                    collectionNameF.in(params),
                    collectionKeyF.in(params)
            );
            this.schemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schema");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void getSchema(GetServiceSchemaMessage message) {
        try {
            String schemaName = message.getSchemaName();
            String schemaString = schemaCollection.getItems(schemaName).get(0).serialize();
            IObject schemaObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), schemaString);
            message.setSchema(schemaF.in(schemaObject));
            message.setFormKey(schemaName);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
