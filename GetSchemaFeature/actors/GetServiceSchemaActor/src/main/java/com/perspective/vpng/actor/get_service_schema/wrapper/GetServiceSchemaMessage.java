package com.perspective.vpng.actor.get_service_schema.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface GetServiceSchemaMessage {
    String getSchemaName() throws ReadValueException;
    void setSchema(List<IObject> schema) throws ChangeValueException;
    void setFormKey(String formKey) throws ChangeValueException;
}
