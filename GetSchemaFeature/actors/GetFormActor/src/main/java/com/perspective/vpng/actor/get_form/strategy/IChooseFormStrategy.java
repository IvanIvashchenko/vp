package com.perspective.vpng.actor.get_form.strategy;

import com.perspective.vpng.actor.get_form.exception.ChooseFormStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

import java.util.List;

/**
 * Interface for rule for choosing form
 */
public interface IChooseFormStrategy {

    /**
     * @param forms the forms from cached collection
     * @param message whole msg
     * @return form
     */
    IObject getForm(List<IObject> forms, IObject message) throws ChooseFormStrategyException;
}
