package com.perspective.vpng.actor.get_form.exception;

import com.perspective.vpng.actor.get_form.GetFormActor;

/**
 * Exception for {@link GetFormActor} errors
 */
public class GetFormActorException extends Exception {

    /**
     * Constructor with specific error message and cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public GetFormActorException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
