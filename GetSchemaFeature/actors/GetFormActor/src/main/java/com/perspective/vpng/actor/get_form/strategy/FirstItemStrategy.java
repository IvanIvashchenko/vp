package com.perspective.vpng.actor.get_form.strategy;

import com.perspective.vpng.actor.get_form.exception.ChooseFormStrategyException;
import info.smart_tools.smartactors.iobject.iobject.IObject;

import java.util.List;

/**
 * Return first element of list
 */
public class FirstItemStrategy implements IChooseFormStrategy {
    @Override
    public IObject getForm(final List<IObject> forms, IObject message) throws ChooseFormStrategyException {

        if (forms.isEmpty()) {
            throw new ChooseFormStrategyException("Forms list is empty!");
        }
        return forms.get(0);
    }
}
