package com.perspective.vpng.actor.get_form.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for GetFormActor
 */
public interface GetFormMessage {
    /**
     * @return collection name
     * @exception ReadValueException Calling when try read value of variable
     */
    String getFormKey() throws ReadValueException;

    /**
     * @return message
     * @throws ReadValueException if any error is occurred
     */
    IObject getMessage() throws ReadValueException;

    /**
     * @return name of strategy for select form
     * @throws ReadValueException if any error is occurred
     */
    String getStrategyName() throws ReadValueException;

    /**
     * Set form to message
     * @param form the form from cached collection
     * @exception ChangeValueException Calling when try change value of variable
     */
    void setForm(IObject form) throws ChangeValueException;
}
