package com.perspective.vpng.actor.get_form.strategy;

import com.perspective.vpng.actor.get_form.exception.ChooseFormStrategyException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

/**
 * Strategy for choice form for meters from list of forms
 */
public class ChooseMetersFormStrategy implements IChooseFormStrategy {

    private static final String METERS_TYPE = "meters";

    private final IField formTypeF;

    public ChooseMetersFormStrategy() throws ChooseFormStrategyException {
        try {
            formTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
        } catch (ResolutionException e) {
            throw new ChooseFormStrategyException("Can't create ChooseMetersFormStrategy", e);
        }
    }

    @Override
    public IObject getForm(final List<IObject> forms, IObject message) throws ChooseFormStrategyException {

        IObject result = null;
        try {
            String type;
            for (IObject form : forms) {
                type = formTypeF.in(form);
                if (type != null && type.equals(METERS_TYPE)) {
                    result = form;
                }
            }
            if (result == null) {
                throw new ChooseFormStrategyException("Can't choose form by form type");
            }
        } catch (ReadValueException | InvalidArgumentException e) {
            throw new ChooseFormStrategyException("Error during choice form by form type", e);
        }
        return result;
    }
}
