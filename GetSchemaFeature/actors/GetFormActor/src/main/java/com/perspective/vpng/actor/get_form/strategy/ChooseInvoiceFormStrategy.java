package com.perspective.vpng.actor.get_form.strategy;

import com.perspective.vpng.actor.get_form.exception.ChooseFormStrategyException;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

/**
 * Strategy for choice specific invoice form from list of forms
 */
public class ChooseInvoiceFormStrategy implements IChooseFormStrategy {

    private static final String INVOICE_TYPE = "invoice";

    private final IField formTypeF;

    public ChooseInvoiceFormStrategy() throws ChooseFormStrategyException {
        try {
            formTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
        } catch (ResolutionException e) {
            throw new ChooseFormStrategyException("Can't create ChooseInvoiceFormStrategy", e);
        }
    }

    //TODO:: add logic for A/B testing or replace this strategy
    @Override
    public IObject getForm(final List<IObject> forms, final IObject message) throws ChooseFormStrategyException {

        IObject result = null;
        try {
            String type;
            for (IObject form : forms) {
                type = formTypeF.in(form);
                if (type == null || type.equals(INVOICE_TYPE)) {
                    result = form;
                }
            }
            if (result == null) {
                throw new ChooseFormStrategyException("Can't choose form by form type");
            }
        } catch (ReadValueException | InvalidArgumentException e) {
            throw new ChooseFormStrategyException("Error during choice invoice form", e);
        }
        return result;
    }
}
