package com.perspective.vpng.actor.get_form.exception;

import com.perspective.vpng.actor.get_form.strategy.IChooseFormStrategy;

/**
 * Exception for {@link IChooseFormStrategy}
 */
public class ChooseFormStrategyException extends Exception {

    public ChooseFormStrategyException(final String message) {
        super(message);
    }

    public ChooseFormStrategyException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
