/**
 * Contains strategies for choosing form from collection
 */
package com.perspective.vpng.actor.get_form.strategy;