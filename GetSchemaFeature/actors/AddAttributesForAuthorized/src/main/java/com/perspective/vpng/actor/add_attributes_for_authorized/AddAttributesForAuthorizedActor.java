package com.perspective.vpng.actor.add_attributes_for_authorized;

import com.perspective.vpng.actor.add_attributes_for_authorized.wrapper.AddAttributesMessage;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;

public class AddAttributesForAuthorizedActor {
    private IField tagInputF;
    private IField smsRemindF;
    private IField formF;
    private IField schemaF;
    private IField bodyF;
    private String schemaTemplate1 =
            ("{" +
                "'block':'checkboxWithCollapsedContainer'," +
                "'name':'метка'," +
                "'checkbox':{" +
                    "'name':'addTagCheckbox'," +
                    "'block':'checkbox'," +
                    "'checked':false," +
                    "'htmlLabel':'Добавить метку'," +
                    "'helpIconMessage':'Введите название, нажмите Enter'," +
                    "'labelWidth':3" +
                "}," +
                "'items':[" +
                    "'tagInput'" +
                "]" +
            "}").replace('\'', '"');
    private String schemaTemplate2 =
            "'div'".replace('\'', '"');
    private String schemaTemplate3 =
            ("{" +
                "'block':'checkboxWithCollapsedContainer'," +
                "'name':'Напоминание'," +
                "'checkbox':{" +
                    "'name':'добавить-напоминание'," +
                    "'block':'checkbox'," +
                    "'checked':false," +
                    "'htmlLabel':'Настроить SMS-напоминания'," +
                    "'helpIconMessage':'Мы можем напомнить вам о платеже'," +
                    "'labelWidth':3" +
                "}," +
                "'items':[" +
                    "'напоминание'" +
                "]" +
            "}").replace('\'', '"');

    private String markTemplate =
            ("{" +
                "'block':'tagInput'," +
                "'label':'Метки платежа'," +
                "'labelWidth':3," +
                "'dataMain':{" +
                    "'labelscountlimit':20," +
                    "'paymentcountlimit':1," +
                    "'totaluserlabelcount':1," +
                    "'alllabels':[" +
                        "{" +
                            "'name':'Тестовая метка1'" +
                        "}," +
                        "{" +
                            "'name':'Тестовая метка2'" +
                        "}," +
                        "{" +
                            "'name':'Тестовая метка3'" +
                        "}" +
                    "]" +
                "}" +
            "}").replace('\'', '"');

    private String smsRemindTemplate =
            ("{" +
                "'block': 'radioGroupWithContainers'," +
                "'label': 'SMS-напоминание'," +
                "'labelWidth': 3," +
                "'shouldNotDisable': true," +
                "'options': [" +
                    "{" +
                        "'name': 'Единовременное напоминание <br>'," +
                        "'value': 'единовременно'," +
                        "'checked': true" +
                    "}," +
                    "{" +
                        "'name': 'Напоминать каждое выбранное число месяца <br>'," +
                        "'value': 'каждый-месяц'" +
                    "}" +
                "]," +
                "'containerOptions': {" +
                    "'единовременно': [" +
                        "{" +
                            "'name': 'дата'," +
                            "'block': 'date'," +
                            "'inputWidth': '140px'" +
                        "}" +
                    "]," +
                    "'каждый-месяц': [" +
                        "{" +
                            "'name': 'число'," +
                            "'block': 'select'," +
                            "'inputWidth': '140px'," +
                            "'value': '1'," +
                            "'values': [" +
                                "{" +
                                    "'name': '1'," +
                                    "'value': '1'" +
                                "}," +
                                "{" +
                                    "'name': '2'," +
                                    "'value': '2'" +
                                "}," +
                                "{" +
                                    "'name': '3'," +
                                    "'value': '3'" +
                                "}," +
                                "{" +
                                    "'name': '4'," +
                                    "'value': '4'" +
                                "}," +
                                "{" +
                                    "'name': '5'," +
                                    "'value': '5'" +
                                "}," +
                                "{" +
                                    "'name': '6'," +
                                    "'value': '6'" +
                                "}," +
                                "{" +
                                    "'name': '7'," +
                                    "'value': '7'" +
                                "}," +
                                "{" +
                                    "'name': '8'," +
                                    "'value': '8'" +
                                "}," +
                                "{" +
                                    "'name': '9'," +
                                    "'value': '9'" +
                                "}," +
                                "{" +
                                    "'name': '10'," +
                                    "'value': '10'" +
                                "}," +
                                "{" +
                                    "'name': '11'," +
                                    "'value': '11'" +
                                "}," +
                                "{" +
                                    "'name': '12'," +
                                    "'value': '12'" +
                                "}," +
                                "{" +
                                    "'name': '13'," +
                                    "'value': '13'" +
                                "}," +
                                "{" +
                                    "'name': '14'," +
                                    "'value': '14'" +
                                "}," +
                                "{" +
                                    "'name': '15'," +
                                    "'value': '15'" +
                                "}," +
                                "{" +
                                    "'name': '16'," +
                                    "'value': '16'" +
                                "}," +
                                "{" +
                                    "'name': '17'," +
                                    "'value': '17'" +
                                "}," +
                                "{" +
                                    "'name': '18'," +
                                    "'value': '18'" +
                                "}," +
                                "{" +
                                    "'name': '19'," +
                                    "'value': '19'" +
                                "}," +
                                "{" +
                                    "'name': '20'," +
                                    "'value': '20'" +
                                "}," +
                                "{" +
                                    "'name': '21'," +
                                    "'value': '21'" +
                                "}," +
                                "{" +
                                    "'name': '22'," +
                                    "'value': '22'" +
                                "}," +
                                "{" +
                                    "'name': '23'," +
                                    "'value': '23'" +
                                "}," +
                                "{" +
                                    "'name': '24'," +
                                    "'value': '24'" +
                                "}," +
                                "{" +
                                    "'name': '25'," +
                                    "'value': '25'" +
                                "}," +
                                "{" +
                                    "'name': '26'," +
                                    "'value': '26'" +
                                "}," +
                                "{" +
                                    "'name': '27'," +
                                    "'value': '27'" +
                                "}," +
                                "{" +
                                    "'name': '28'," +
                                    "'value': '28'" +
                                "}," +
                                "{" +
                                    "'name': '29'," +
                                    "'value': '29'" +
                                "}," +
                                "{" +
                                    "'name': '30'," +
                                    "'value': '30'" +
                                "}" +
                            "]" +
                        "}" +
                    "]" +
                "}" +
            "}").replace('\'', '"');


    public AddAttributesForAuthorizedActor() throws ResolutionException {
        smsRemindF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "напоминание");
        tagInputF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "метка");
        formF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "form");
        schemaF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "schema");
        bodyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "body");
    }

    public void addFormAttributes(AddAttributesMessage message) {
        try {
            String formString = message.getForm().serialize();
            IObject body = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), formString);
            IObject form = bodyF.in(body);
            IObject formForm = formF.in(form);
            List<Object> formSchema = schemaF.in(form);

            formSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), schemaTemplate1));
            formSchema.add(schemaTemplate2);
            formSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), schemaTemplate3));

            IObject tagObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), markTemplate);
            tagInputF.out(formForm, tagObject);
            IObject smsObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), smsRemindTemplate);
            smsRemindF.out(formForm, smsObject);

            message.setForm(body);
        } catch (Exception e) {
            throw new RuntimeException("Failed to add attributes", e);
        }
    }

    public void addSchemaAttributes(AddAttributesMessage message) {
        try {
            List<IObject> oldSchema =  message.getOuterSchema();
            List<IObject> outerSchema = new ArrayList<>();

            for (IObject schemaEl : oldSchema) {
                String objectStr = schemaEl.serialize();
                outerSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), objectStr));
            }

            outerSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{'name': 'добавить-метку'}".replace('\'', '"')));
            outerSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{'name': 'метка'}".replace('\'', '"')));
            outerSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{'name': 'добавить-напоминание', 'from-schema': 'sms_remind_flag_from_schema_to_inner'}".replace('\'', '"')));
            outerSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{'name': 'тип-напоминания', 'from-schema': 'sms_remind_type_from_schema_to_inner'}".replace('\'', '"')));
            outerSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{'name': 'напоминание', 'from-schema': 'sms_remind_value_from_schema_to_inner'}".replace('\'', '"')));
            message.setSchema(outerSchema);
        } catch (Exception e) {
            throw new RuntimeException("Failed to add attributes", e);
        }
    }
}
