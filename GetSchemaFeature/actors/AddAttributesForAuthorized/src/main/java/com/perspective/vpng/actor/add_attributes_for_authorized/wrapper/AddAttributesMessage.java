package com.perspective.vpng.actor.add_attributes_for_authorized.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface AddAttributesMessage {
    IObject getForm() throws ReadValueException;
    void setForm(IObject form) throws ChangeValueException;
    List<String> getUserMarks() throws ReadValueException;
    List<IObject> getOuterSchema() throws ReadValueException;
    void setSchema(List<IObject> schema) throws ChangeValueException;
}
