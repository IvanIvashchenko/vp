package com.perspective.vpng.strategy.selection_scheme;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ChooseInvoiceSchemaStrategyTest {

    private IResolveDependencyStrategy strategy;
    private IField nameF;

    @BeforeClass
    public static void prepareIOC() throws Exception {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {

        nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        strategy = new ChooseInvoiceSchemaStrategy();
    }

    @Test
    public void shouldChooseInvoiceSchemaByServiceObjectWithType() throws Exception {

        List<List<IObject>> schemas = new ArrayList<>();
        List<IObject> schema1 = new ArrayList<>();
        List<IObject> schema2 = new ArrayList<>();
        IObject schemaObj1 = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"лицевой-счет\"}");
        IObject schemaObj2 = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"фамилия\"}");
        IObject invoiceTypeObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"type\", \"value\": \"invoice\"}");
        IObject metersTypeObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"type\", \"value\": \"meters\"}");
        schema1.add(schemaObj1);
        schema1.add(metersTypeObj);
        schema2.add(schemaObj2);
        schema2.add(invoiceTypeObj);
        schemas.add(schema1);
        schemas.add(schema2);

        List<IObject> invoiceSchema = strategy.resolve(schemas);
        assertEquals(invoiceSchema.size(), 2);
        assertEquals(nameF.in(invoiceSchema.get(0)), "фамилия");
    }

    @Test
    public void shouldChooseInvoiceSchema_when_SchemaWithoutTypeIsGiven() throws Exception {

        List<List<IObject>> schemas = new ArrayList<>();
        List<IObject> schema1 = new ArrayList<>();
        List<IObject> schema2 = new ArrayList<>();
        IObject schemaObj1 = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"лицевой-счет\"}");
        IObject schemaObj2 = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"фамилия\"}");
        IObject metersTypeObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"type\", \"value\": \"meters\"}");
        schema1.add(schemaObj1);
        schema1.add(metersTypeObj);
        schema2.add(schemaObj2);
        schemas.add(schema1);
        schemas.add(schema2);

        List<IObject> invoiceSchema = strategy.resolve(schemas);
        assertEquals(invoiceSchema.size(), 1);
        assertEquals(nameF.in(invoiceSchema.get(0)), "фамилия");
    }

    @Test(expected = ResolveDependencyStrategyException.class)
    public void shouldThrowException_when_NoOneInvoiceSchemaHasBeenFound() throws Exception {

        List<List<IObject>> schemas = new ArrayList<>();
        List<IObject> invoiceSchema = new ArrayList<>();
        List<IObject> defaultSchema = new ArrayList<>();
        IObject schemaObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"лицевой-счет\"}");
        IObject typeObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"type\", \"value\": \"meters\"}");
        invoiceSchema.add(schemaObj);
        invoiceSchema.add(typeObj);
        defaultSchema.add(schemaObj);
        defaultSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"адрес\"}"));
        defaultSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"type\", \"value\": \"anotherValue\"}"));
        schemas.add(defaultSchema);
        schemas.add(invoiceSchema);

        strategy.resolve(schemas);

        fail();
    }
}
