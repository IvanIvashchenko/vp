package com.perspective.vpng.strategy.selection_scheme;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ChooseMetersSchemaStrategyTest {

    private IResolveDependencyStrategy strategy;
    private IField nameF;

    @BeforeClass
    public static void prepareIOC() throws Exception {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();

        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {

        nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
        strategy = new ChooseMetersSchemaStrategy();
    }

    @Test
    public void shouldChooseMetersSchemaByServiceObjectWithType() throws Exception {

        List<List<IObject>> schemas = new ArrayList<>();
        List<IObject> schema = new ArrayList<>();
        IObject schemaObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"лицевой-счет\"}");
        IObject typeObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"type\", \"value\": \"meters\"}");
        schema.add(schemaObj);
        schema.add(typeObj);
        schemas.add(new ArrayList<>());
        schemas.add(schema);

        List<IObject> metersSchema = strategy.resolve(schemas);
        assertEquals(metersSchema.size(), 2);
        assertEquals(nameF.in(metersSchema.get(0)), "лицевой-счет");
    }

    @Test(expected = ResolveDependencyStrategyException.class)
    public void shouldThrowException_when_NoOneMetersSchemaHasBeenFound() throws Exception {

        List<List<IObject>> schemas = new ArrayList<>();
        List<IObject> invoiceSchema = new ArrayList<>();
        List<IObject> defaultSchema = new ArrayList<>();
        IObject schemaObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"лицевой-счет\"}");
        IObject typeObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"type\", \"value\": \"invoice\"}");
        invoiceSchema.add(schemaObj);
        invoiceSchema.add(typeObj);
        defaultSchema.add(schemaObj);
        defaultSchema.add(IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"name\": \"адрес\"}"));
        schemas.add(defaultSchema);
        schemas.add(invoiceSchema);

        strategy.resolve(schemas);

        fail();
    }
}
