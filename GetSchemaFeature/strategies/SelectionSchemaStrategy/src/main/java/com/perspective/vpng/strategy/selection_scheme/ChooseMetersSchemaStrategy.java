package com.perspective.vpng.strategy.selection_scheme;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

/**
 * Strategy for choice meters schema from schemas list by according type
 */
public class ChooseMetersSchemaStrategy implements IResolveDependencyStrategy {

    private static final String METERS_TYPE = "meters";
    private static final String TYPE = "type";

    private final IField nameF;
    private final IField valueF;

    public ChooseMetersSchemaStrategy() throws ResolveDependencyStrategyException {
        try {
            nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
            valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
        } catch (ResolutionException e) {
            throw new ResolveDependencyStrategyException("Can't create ChooseMetersFormStrategy", e);
        }
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        List<List<IObject>> schemas = ((List) args[0]);
        try {
            for (List<IObject> schema : schemas) {
                for (IObject schemaObj : schema) {
                    if (nameF.in(schemaObj).equals(TYPE) && valueF.in(schemaObj).equals(METERS_TYPE)) {
                        return (T) schema;
                    }
                }
            }

            throw new ResolveDependencyStrategyException("There is no one meters schema in schemas list");
        } catch (ReadValueException | InvalidArgumentException e) {
            throw new ResolveDependencyStrategyException("Can't choose meters schema", e);
        }
    }
}
