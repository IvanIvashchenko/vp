package com.perspective.vpng.strategy.modification_schema;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.eq;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({IOC.class, Keys.class })
public class GetFirstStepSchemaStrategyTest {

    private final IResolveDependencyStrategy strategy;

    private final IField firstStepF;
    private final IField nameF;
    public GetFirstStepSchemaStrategyTest() throws ResolutionException {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        IKey fieldKey = mock(IKey.class);
        when(Keys.getOrAdd(IField.class.getCanonicalName())).thenReturn(fieldKey);
        firstStepF = mock(IField.class);
        nameF = mock(IField.class);
        when(IOC.resolve(eq(fieldKey), eq("firstStep"))).thenReturn(firstStepF);
        when(IOC.resolve(eq(fieldKey), eq("name"))).thenReturn(nameF);

        strategy = new GetFirstStepSchemaStrategy();
    }

    @Test
    public void should_GetFirstStepSchema()
            throws ReadValueException, InvalidArgumentException, ResolveDependencyStrategyException {

        final IObject provider = mock(IObject.class);
        List<Integer> tests = Arrays.asList(5, 12, 37, 54, 63);
        for (Integer test : tests) {
            final int schemaSize = test;
            final int firstStepSize = schemaSize % 2 == 0 ? schemaSize / 2 : schemaSize / 2 + 1;
            int i;

            List<IObject> schema = new ArrayList<>(schemaSize);
            for (i = 0; i < schemaSize; i++) {
                IObject iObject = mock(IObject.class);
                when(nameF.in(iObject)).thenReturn("testF" + i);
                schema.add(iObject);

            }
            List<String> firstStep = new ArrayList<>(firstStepSize);
            for (i = 0; i < schemaSize; i += 2) {
                firstStep.add("testF" + i);
            }

            when(firstStepF.in(provider)).thenReturn(firstStep);
            List<IObject> newSchema = strategy.resolve(schema, provider);

            assertEquals(newSchema.size(), firstStepSize);
            for (IObject block : newSchema) {
                assertTrue(firstStep.contains(nameF.in(block)));
            }
        }
    }

    @Test
    public void should_GetSourceSchemaWithoutFirstStep() throws Exception {
        final IObject provider = mock(IObject.class);
        final int schemaSize = 5;
        int i;

        List<IObject> schema = new ArrayList<>(schemaSize);
        for (i = 0; i < schemaSize; i++) {
            IObject iObject = mock(IObject.class);
            when(nameF.in(iObject)).thenReturn("testF" + i);
            schema.add(iObject);

        }

        List<IObject> newSchema;
        when(firstStepF.in(provider)).thenReturn(null);
        newSchema = strategy.resolve(schema, provider);
        assertTrue(schema.equals(newSchema));
        when(firstStepF.in(provider)).thenReturn(Collections.emptyList());
        newSchema = strategy.resolve(schema, provider);
        assertTrue(schema.equals(newSchema));
    }

    @Test
    public void should_ThrowException_WithReason_InvalidSchemaFormat() throws Exception {
        IObject provider = mock(IObject.class);
        List<IObject> schema = Collections.singletonList(mock(IObject.class));
        when(firstStepF.in(provider)).thenReturn(Collections.singletonList("testF"));
        try {
            strategy.resolve(schema, provider);
            fail("Expected exception!");
        } catch (ResolveDependencyStrategyException ex) {
            assertEquals(ex.getMessage(), "Invalid schema format: field's name is null!");
        }
    }

}
