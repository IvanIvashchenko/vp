package com.perspective.vpng.strategy.modification_schema;

import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GetFirstStepSchemaStrategy implements IResolveDependencyStrategy {

    private final IField nameF;
    private final IField firstStepF;

    public GetFirstStepSchemaStrategy() throws ResolutionException {
        IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
        this.firstStepF = IOC.resolve(fieldKey, "firstStep");
        this.nameF = IOC.resolve(fieldKey, "name");
    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {
        try {
            List<IObject> schema = (List) args[0];
            IObject provider = (IObject) args[1];
            List<String> firstStep = firstStepF.in(provider);
            if (firstStep != null && !firstStep.isEmpty()) {
                return (T) generateFirstStepSchema(schema, firstStep);
            }
            return (T) schema;
        } catch (ReadValueException | InvalidArgumentException ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }

    private List<IObject> generateFirstStepSchema(List<IObject> schema, List<String> fieldsList)
            throws ReadValueException, InvalidArgumentException {
        List<IObject> resultSchema = new ArrayList<>();
        for (IObject objEl : schema) {
            String name = (String) Optional.ofNullable(nameF.in(objEl))
                    .orElseThrow(() -> new InvalidArgumentException("Invalid schema format: field's name is null!"));
            if (fieldsList.contains(name)) {
                resultSchema.add(objEl);
            }
        }
        return resultSchema;
    }

}
