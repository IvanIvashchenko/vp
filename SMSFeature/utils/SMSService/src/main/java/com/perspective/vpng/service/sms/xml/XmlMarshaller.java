package com.perspective.vpng.service.sms.xml;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Map;


/**
 * <code>XmlMarshaller</code> class is responsible for marshaling data transfer objects to given <code>OutputStream</code>.
 */
public class XmlMarshaller {

    private final Marshaller marshaller;

    /**
     * Constructs <code>XMLMarshaller</code> for given classes.
     * <p/>
     * Makes initialisation of JAXBContext and Marshaller.
     *
     * @param classesToBeBound class or classes for JAXBContext creation.
     */
    public XmlMarshaller(final Class<?>... classesToBeBound) {
        this(true, classesToBeBound);
    }

    /**
     * Constructs <code>XMLMarshaller</code> for given classes.
     * <p/>
     * Makes initialization of JAXBContext and Marshaller.
     *
     * @param fragment         - JAXB_FRAGMENT if true
     * @param classesToBeBound class or classes for JAXBContext creation.
     */
    public XmlMarshaller(final boolean fragment, final Class<?>... classesToBeBound) {
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
            marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.valueOf(fragment));
        } catch (JAXBException e) {
            throw new RuntimeException("Unable to create marshaller", e);
        }
    }

    /**
        * Constructs <code>XMLMarshaller</code> for given classes.
        * <p/>
        * Makes initialization of JAXBContext and Marshaller.
        *
        * @param properties - map of jaxb properies
        * @param classesToBeBound class or classes for JAXBContext creation.
        */
       public XmlMarshaller(Map<String, Object> properties, final Class<?>... classesToBeBound) {
           try {
               final JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
               marshaller = jaxbContext.createMarshaller();
               for (String property : properties.keySet()) {
                   marshaller.setProperty(property, properties.get(property));
               }
//               marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//               marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.valueOf(fragment));
//               marshaller.setProperty(Marshaller.JAXB_ENCODING, "windows-1251");
           } catch (JAXBException e) {
               throw new RuntimeException("Unable to create marshaller", e);
           }
       }

    /**
     * Marshals the source object to <code>OutputStream</code> in xml format.
     *
     * @param source Object to marshal
     * @param target stream for marshaling
     */
    public void marshal(final Object source, final OutputStream target) {
        try {
            marshaller.marshal(source, target);
        } catch (JAXBException e) {
            throw new RuntimeException("Unable to marshal an object", e);
		}
	}

    public void marshal(final Object source, final Writer target) {
        try {
            marshaller.marshal(source, target);
        } catch (JAXBException e) {
            throw new RuntimeException("Unable to marshal an object", e);
		}
	}
}
