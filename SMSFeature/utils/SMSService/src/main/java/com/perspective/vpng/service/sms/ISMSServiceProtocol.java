package com.perspective.vpng.service.sms;

import com.perspective.vpng.service.sms.exception.SendSmsFailureException;

import java.io.IOException;

public interface ISMSServiceProtocol {
    void openSession() throws IOException, IllegalArgumentException;

    void sendSms(String number, String message) throws SendSmsFailureException;

    void closeSession();
}
