package com.perspective.vpng.service.sms.xml;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.ValidationEventHandler;
import javax.xml.stream.XMLStreamReader;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.InputStream;


/**
 * <code>XmlUnmarshaller</code> class is responsible for unmarshaling data transfer objects from given Stream.
 */
public class XmlUnmarshaller {

    private final Unmarshaller unmarshaller;

    /**
     * Constructs <code>XmlUnmarshaller</code> for given classes.
     * <p/>
     * Makes initialisation of JAXBContext and Marshaller.
     *
     * @param classesToBeBound class or classes for JAXBContext creation.
     */
    public XmlUnmarshaller(final Class<?>... classesToBeBound) {
        try {
            final JAXBContext jaxbContext = JAXBContext.newInstance(classesToBeBound);
            unmarshaller = jaxbContext.createUnmarshaller();
        } catch (JAXBException e) {
            throw new RuntimeException("Unable to create unmarshaller", e);
        }
    }

    /**
     * Initializes validation xml unmarshaller parameters in order to enables xml validation using appropriate xml schema.
     *
     * @param schemaFileName         the schema file name for validation
     * @param validationEventHandler the appropriate validation event handler
     */
    public void initValidationParameters(final String schemaFileName, final ValidationEventHandler validationEventHandler) {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(new File(schemaFileName));
            unmarshaller.setSchema(schema);
            if (validationEventHandler != null) {
                unmarshaller.setEventHandler(validationEventHandler);
            }
        } catch (SAXException e) {
            throw new RuntimeException("Could not load schema from " + schemaFileName, e);
        } catch (JAXBException e) {
            throw new RuntimeException("Could not set validation event handler", e);
        }
    }

    /**
     * Unmarshals the object from <code>InputStream</code>.
     *
     * @param source stream for unmarshaling
     * @return an unmarshalled object.
     */
    public Object unmarshall(final InputStream source) {
        try {
            return unmarshaller.unmarshal(source);
        } catch (JAXBException e) {
            throw new RuntimeException("Unable to unmarshall an object from input stream", e);
        }
    }

    /**
     * Unmarshals the object from <code>XMLStreamReader</code>.
     *
     * @param source XMLStreamReader for unmarshaling
     * @return an unmarshalled object.
     */
    public Object unmarshall(final XMLStreamReader source) {
        try {
            return unmarshaller.unmarshal(source);
        } catch (JAXBException e) {
            throw new RuntimeException("Unable to unmarshall an object from XMLStreamReader", e);
		}
	}
}
