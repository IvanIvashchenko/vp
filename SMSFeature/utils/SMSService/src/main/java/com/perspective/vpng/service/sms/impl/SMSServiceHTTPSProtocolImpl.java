package com.perspective.vpng.service.sms.impl;

import com.perspective.vpng.service.sms.ISMSServiceProtocol;
import com.perspective.vpng.service.sms.exception.SendSmsFailureException;
import com.perspective.vpng.service.sms.xml.*;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.xml.stream.XMLInputFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class SMSServiceHTTPSProtocolImpl implements ISMSServiceProtocol {

    private static final String READ_TIMEOUT_KEY = "sms.https.service.readtimeout";
    private static final String LOGIN_KEY = "sms.https.service.login";
    private static final String PASSWORD_KEY = "sms.https.service.password";
    private static final String SENDER_KEY = "sms.https.service.sender";
    private static final String URL_KEY = "sms.https.service.url";
    private static final String PHONE_PREFIX = "sms.https.service.phoneprefix";

    private final Integer readTimeout;
    private final String login;
    private final String password;
    private final String sender;
    private final String url;
    private final String phonePrefix;

    public SMSServiceHTTPSProtocolImpl(IObject params) {
        try {
            IField readTimeoutF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), READ_TIMEOUT_KEY);
            this.readTimeout = readTimeoutF.in(params);
            IField loginF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), LOGIN_KEY);
            this.login = loginF.in(params);
            IField passwordF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), PASSWORD_KEY);
            this.password = passwordF.in(params);
            IField senderF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SENDER_KEY);
            this.sender = senderF.in(params);
            IField urlF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), URL_KEY);
            this.url = urlF.in(params);
            IField phonePrefixF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), PHONE_PREFIX);
            this.phonePrefix = phonePrefixF.in(params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void openSession() {
        // Nothing to do
    }

    @Override
    public void closeSession() {
        // Nothing to do
    }

    private String buildPhoneNumber(String number) {
        if (number.length() == 10) {
            return phonePrefix.concat(number);
        } else {
            return number;
        }
    }

    @Override
    public void sendSms(String number, String message) throws SendSmsFailureException {
        try {
            URL url = new URL(this.url);
            final HttpsURLConnection connection = initConnection(url);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            XMLSendRequestPackage sendRequestPackage = new XMLSendRequestPackage();
            XMLSendRequestMessage sendRequestMessage = new XMLSendRequestMessage();
            XMLSendRequestDefault sendRequestDefault = new XMLSendRequestDefault();
            XMLSendRequestMsg sendRequestMsg = new XMLSendRequestMsg();

            sendRequestPackage.setLogin(login);
            sendRequestPackage.setPassword(password);
            sendRequestPackage.setMessage(sendRequestMessage);

            sendRequestDefault.setSender(sender);

            sendRequestMessage.setDefault(sendRequestDefault);
            sendRequestMessage.setMsg(sendRequestMsg);

            sendRequestMsg.setType("0");
            sendRequestMsg.setUrl("");
            sendRequestMsg.setRecipient(buildPhoneNumber(number));

            sendRequestMsg.setContent(message);

            XmlMarshaller xmlMarshaller = new XmlMarshaller(XMLSendRequestPackage.class);
            XmlUnmarshaller xmlUnMarshaller = new XmlUnmarshaller(XMLSendResponsePackage.class);
            xmlMarshaller.marshal(sendRequestPackage, connection.getOutputStream());
            connection.connect();

            try (Reader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"))) {
                XMLSendResponsePackage xmlSendResponse = (XMLSendResponsePackage) xmlUnMarshaller.unmarshall(XMLInputFactory.newInstance().createXMLStreamReader(reader));
            }

            connection.disconnect();
        } catch (Exception e) {
            throw new SendSmsFailureException("Unable to send sms", e);
        }
    }

    private HttpsURLConnection initConnection(URL serviceUrl) throws IOException, NoSuchAlgorithmException, KeyManagementException {
        final HttpsURLConnection serviceConnection = (HttpsURLConnection) serviceUrl.openConnection();
        serviceConnection.setReadTimeout(readTimeout);
        serviceConnection.setHostnameVerifier(
                new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                }
        );
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, null, null);
        serviceConnection.setSSLSocketFactory(sc.getSocketFactory());
        return serviceConnection;
    }
}
