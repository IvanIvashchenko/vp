package com.perspective.vpng.service.sms.exception;

public class SendSmsFailureException extends Exception {
    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public SendSmsFailureException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */

    public SendSmsFailureException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public SendSmsFailureException(final Throwable cause) {
        super(cause);
    }
}
