package com.perspective.vpng.service.sms.impl;

import com.perspective.vpng.service.sms.ISMSServiceProtocol;
import com.perspective.vpng.service.sms.exception.SendSmsFailureException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.jsmpp.bean.*;
import org.jsmpp.session.BindParameter;
import org.jsmpp.session.SMPPSession;
import org.jsmpp.util.AbsoluteTimeFormatter;
import org.jsmpp.util.TimeFormatter;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Random;

public class SMSServiceSMPPProtocolImpl implements ISMSServiceProtocol {
    private SMPPSession session;
    private static final int MAX_MULTIPART_MSG_SEGMENT_SIZE_UCS2 = 134;
    private static final int MAX_SINGLE_MSG_SEGMENT_SIZE_UCS2 = 69;
    private static final int MAX_MULTIPART_MSG_SEGMENT_SIZE_7BIT = 153;
    private static final int MAX_SINGLE_MSG_SEGMENT_SIZE_7BIT = 160;
    private static final String SMPP_HOST = "sms.smpp.service.host";
    private static final String SMPP_PORT = "sms.smpp.service.port";
    private static final String SMPP_LOGIN = "sms.smpp.service.login";
    private static final String SMPP_PASS = "sms.smpp.service.password";
    private static final String SOURCE_ADDR = "sms.smpp.service.source_addr";
    private static final String SYSTEM_TYPE = "sms.smpp.service.system_type";
    private static final String SERVICE_TYPE = "sms.smpp.service.service_type";
    private static final String CYRILLIC_ENCODING = "sms.smpp.service.smsCyrillicEncoding";
    private static final String PHONE_PREFIX = "sms.smpp.service.phoneprefix";

    private static TimeFormatter timeFormatter = new AbsoluteTimeFormatter();
    private final String smppHost;
    private final Integer smppPort;
    private final String smppLogin;
    private final String smppPass;
    private final String sourceAddr;
    private final String systemType;
    private final String serviceType;
    private final boolean cyrillicEncoding;
    private final String phonePrefix;

    public SMSServiceSMPPProtocolImpl(IObject params) {
        try {
            IField smppHostF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SMPP_HOST);
            this.smppHost = smppHostF.in(params);
            IField smppPortF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SMPP_PORT);
            this.smppPort = smppPortF.in(params);
            IField smppLoginF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SMPP_LOGIN);
            this.smppLogin = smppLoginF.in(params);
            IField smppPassF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SMPP_PASS);
            this.smppPass = smppPassF.in(params);
            IField sourceAddrF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SOURCE_ADDR);
            this.sourceAddr = sourceAddrF.in(params);
            IField systemTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SYSTEM_TYPE);
            this.systemType = systemTypeF.in(params);
            IField serviceTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), SERVICE_TYPE);
            this.serviceType = serviceTypeF.in(params);
            IField cyrillicEncodingF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), CYRILLIC_ENCODING);
            this.cyrillicEncoding = cyrillicEncodingF.in(params);
            IField phonePrefixF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), PHONE_PREFIX);
            this.phonePrefix = phonePrefixF.in(params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void openSession() throws IOException {
        session = new SMPPSession();
        session.connectAndBind(
                smppHost,
                smppPort,
                new BindParameter(
                        BindType.BIND_TRX,
                        smppLogin,
                        smppPass,
                        systemType,
                        TypeOfNumber.UNKNOWN,
                        NumberingPlanIndicator.UNKNOWN,
                        null
                )
        );
    }

    @Override
    public void closeSession() {
        if (session != null)
            session.unbindAndClose();
    }


    @Override
    public void sendSms(String number, String message) throws SendSmsFailureException {
        if (session == null) {
            return;
        }

        // configure variables acording to if message contains national characters
        Alphabet alphabet;
        int maximumSingleMessageSize;
        int maximumMultipartMessageSegmentSize;
        byte[] byteSingleMessage;
        if (cyrillicEncoding) {
            byteSingleMessage = message.getBytes(Charset.forName("UTF-16"));
            alphabet = Alphabet.ALPHA_UCS2;
            maximumSingleMessageSize = MAX_SINGLE_MSG_SEGMENT_SIZE_UCS2;
            maximumMultipartMessageSegmentSize = MAX_MULTIPART_MSG_SEGMENT_SIZE_UCS2;
        } else {
            byteSingleMessage = message.getBytes();
            alphabet = Alphabet.ALPHA_DEFAULT;
            maximumSingleMessageSize = MAX_SINGLE_MSG_SEGMENT_SIZE_7BIT;
            maximumMultipartMessageSegmentSize = MAX_MULTIPART_MSG_SEGMENT_SIZE_7BIT;
        }

        // check if message needs splitting and set required sending parameters
        byte[][] byteMessagesArray;
        ESMClass esmClass;
        if (message.length() > maximumSingleMessageSize) {
            // split message according to the maximum length of a segment
            byteMessagesArray = splitUnicodeMessage(byteSingleMessage, maximumMultipartMessageSegmentSize);
            // set UDHI so PDU will decode the header
            esmClass = new ESMClass(MessageMode.DEFAULT, MessageType.DEFAULT, GSMSpecificFeature.UDHI);
        } else {
            byteMessagesArray = new byte[][]{byteSingleMessage};
            esmClass = new ESMClass();
        }

        // submit all messages
        for (byte[] aByteMessagesArray : byteMessagesArray) {
            submitMessage(session, aByteMessagesArray, sourceAddr, buildPhoneNumber(number), alphabet, esmClass);
        }
    }

    private String buildPhoneNumber(String number) {
        if (number.length() == 10) {
            return phonePrefix.concat(number);
        } else {
            return number;
        }
    }

    private byte[][] splitUnicodeMessage(byte[] aMessage, Integer maximumMultipartMessageSegmentSize) {

        final byte UDHIE_HEADER_LENGTH = 0x05;
        final byte UDHIE_IDENTIFIER_SAR = 0x00;
        final byte UDHIE_SAR_LENGTH = 0x03;

        // determine how many messages have to be sent
        int numberOfSegments = aMessage.length / maximumMultipartMessageSegmentSize;
        int messageLength = aMessage.length;
        if (numberOfSegments > 255) {
            numberOfSegments = 255;
            messageLength = numberOfSegments * maximumMultipartMessageSegmentSize;
        }
        if ((messageLength % maximumMultipartMessageSegmentSize) > 0) {
            numberOfSegments++;
        }

        // prepare array for all of the msg segments
        byte[][] segments = new byte[numberOfSegments][];

        int lengthOfData;

        // generate new reference number
        byte[] referenceNumber = new byte[1];
        new Random().nextBytes(referenceNumber);

        // split the message adding required headers
        for (int i = 0; i < numberOfSegments; i++) {
            if (numberOfSegments - i == 1) {
                lengthOfData = messageLength - i * maximumMultipartMessageSegmentSize;
            } else {
                lengthOfData = maximumMultipartMessageSegmentSize;
            }

            // new array to store the header
            segments[i] = new byte[6 + lengthOfData];

            // UDH header
            // doesn't include itself, its header length
            segments[i][0] = UDHIE_HEADER_LENGTH;
            // SAR identifier
            segments[i][1] = UDHIE_IDENTIFIER_SAR;
            // SAR length
            segments[i][2] = UDHIE_SAR_LENGTH;
            // reference number (same for all messages)
            segments[i][3] = referenceNumber[0];
            // total number of segments
            segments[i][4] = (byte) numberOfSegments;
            // segment number
            segments[i][5] = (byte) (i + 1);

            // copy the data into the array
            System.arraycopy(aMessage, (i * maximumMultipartMessageSegmentSize), segments[i], 6, lengthOfData);

        }
        return segments;
    }

    private String submitMessage(SMPPSession session, byte[] message, String sourceMsisdn, String phoneNumber, Alphabet alphabet, ESMClass esmClass)
            throws SendSmsFailureException {
        String messageId;
        try {
            messageId = session.submitShortMessage(serviceType, TypeOfNumber.ALPHANUMERIC, NumberingPlanIndicator.UNKNOWN, sourceMsisdn,
                    TypeOfNumber.INTERNATIONAL, NumberingPlanIndicator.ISDN, phoneNumber,
                    esmClass, (byte) 0, (byte) 1, timeFormatter.format(new Date()), null, new RegisteredDelivery(SMSCDeliveryReceipt.DEFAULT),
                    (byte) 0, new GeneralDataCoding(alphabet), (byte) 0, message);

        } catch (Exception e) {
            throw new SendSmsFailureException("Failed to send sms", e);
        }
        return messageId;
    }
}
