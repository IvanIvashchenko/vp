package com.perspective.vpng.actor.send_sms.exception;

public class SendSmsActorException extends Exception {
    public SendSmsActorException(String message) {
        super(message);
    }

    public SendSmsActorException(String message, Throwable cause) {
        super(message, cause);
    }
}
