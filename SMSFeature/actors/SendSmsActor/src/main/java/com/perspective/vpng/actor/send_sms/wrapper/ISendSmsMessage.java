package com.perspective.vpng.actor.send_sms.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ISendSmsMessage {
    String getNumber() throws ReadValueException;
    String getMessage() throws ReadValueException;
}
