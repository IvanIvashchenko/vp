package com.perspective.vpng.actor.send_sms.wrapper;

import com.perspective.vpng.service.sms.ISMSServiceProtocol;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface IActorParams {
    ISMSServiceProtocol getProtocol() throws ReadValueException;
}
