package com.perspective.vpng.actor.send_sms;

import com.perspective.vpng.actor.send_sms.exception.SendSmsActorException;
import com.perspective.vpng.actor.send_sms.wrapper.IActorParams;
import com.perspective.vpng.actor.send_sms.wrapper.ISendSmsMessage;
import com.perspective.vpng.service.sms.ISMSServiceProtocol;

public class SendSmsActor {
    private ISMSServiceProtocol protocol;

    public SendSmsActor(IActorParams  params) throws SendSmsActorException {
        try {
            this.protocol = params.getProtocol();
        } catch (Exception e) {
            throw new SendSmsActorException("Failed to create SendSmsActor", e);
        }
    }

    public void sendSms(ISendSmsMessage message) throws SendSmsActorException {
        try {
            protocol.sendSms(message.getNumber(), message.getMessage());
        } catch (Exception e) {
            throw new SendSmsActorException("Failed to send sms", e);
        }
    }
}
