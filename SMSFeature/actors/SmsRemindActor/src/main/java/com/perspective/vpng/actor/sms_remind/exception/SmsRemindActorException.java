package com.perspective.vpng.actor.sms_remind.exception;

public class SmsRemindActorException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public SmsRemindActorException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause   specific cause
     */
    public SmsRemindActorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public SmsRemindActorException(final Throwable cause) {
        super(cause);
    }
}
