package com.perspective.vpng.actor.sms_remind.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CheckSmsRemindMessage {
    Boolean getIsRemind() throws ReadValueException;
    void setIsRemind(Boolean flag) throws ChangeValueException;
}
