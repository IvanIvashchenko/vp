package com.perspective.vpng.actor.sms_remind.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CreateSchedulerTaskMessage {
    String getSmsSendMMId() throws ReadValueException;
    String getProviderName() throws ReadValueException;
    void setTask(IObject task) throws ChangeValueException;
    String getRemindType() throws ReadValueException;
    String getRemindValue() throws ReadValueException;
    String getUserId() throws ReadValueException;
}
