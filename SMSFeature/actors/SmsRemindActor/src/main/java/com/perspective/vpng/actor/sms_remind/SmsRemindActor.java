package com.perspective.vpng.actor.sms_remind;

import com.perspective.vpng.actor.sms_remind.exception.SmsRemindActorException;
import com.perspective.vpng.actor.sms_remind.wrapper.CheckSmsRemindMessage;
import com.perspective.vpng.actor.sms_remind.wrapper.CreateSchedulerTaskMessage;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

public class SmsRemindActor {
    private final IPool connectionPool;
    private final String userCollection;
    private final String userCollectionKey;
    private final IField messageMapIdF;
    private final IField messageF;
    private final IField strategyF;
    private final IField startF;
    private final IField intervalF;
    private final IField timeF;
    private final IField saveF;
    private final IField neverTooLateF;
    private final IField setEntryIdF;
    private final IField phoneF;
    private final IField userPhoneF;
    private final IField userIdF;
    private final IField providerIdF;

    private String smsTemplate = "ВсеПлатежи: Напоминаем вам о необходимости оплаты услуг: %s";

    public SmsRemindActor(IObject params) {
        try {
            messageMapIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "messageMapId");
            messageF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "message");
            strategyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "strategy");
            startF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "start");
            intervalF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "interval");
            timeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "time");
            saveF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "save");
            neverTooLateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "neverTooLate");
            setEntryIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "setEntryId");
            phoneF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "phone");
            userPhoneF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/телефон");
            userIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "userId");
            providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "providerId");

            IField userCollectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userCollectionName");
            IField userCollectionKeyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userCollectionKey");
            this.userCollection = userCollectionNameF.in(params);
            this.userCollectionKey = userCollectionKeyF.in(params);

            ConnectionOptions connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
        } catch (Exception e) {
            throw new RuntimeException("Failed to create SmsRemindActor", e);
        }
    }

    public void createSchedulerTask(CreateSchedulerTaskMessage message) throws SmsRemindActorException {
        try {
            IObject user = getUserFromDB(message.getUserId());

            IObject task = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IObject innerMessage = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            messageF.out(task, innerMessage);
            messageMapIdF.out(innerMessage, message.getSmsSendMMId());
            messageF.out(innerMessage, String.format(smsTemplate, message.getProviderName()));
            phoneF.out(innerMessage, userPhoneF.in(user));
            setEntryIdF.out(task, "taskId");
            userIdF.out(task, message.getUserId());
            providerIdF.out(task, message.getProviderName());

            if (message.getRemindType().equals("единовременно")) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date converterDate = sdf.parse(message.getRemindValue());
                LocalDateTime ldt = LocalDateTime.ofInstant(converterDate.toInstant(), ZoneOffset.UTC);
                timeF.out(task, ldt.toString());
                saveF.out(task, true);
                neverTooLateF.out(task, true);
                strategyF.out(task, "do once scheduling strategy");
            }

            if (message.getRemindType().equals("каждый-месяц")) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_MONTH, Integer.parseInt(message.getRemindValue()));
                startF.out(task, c.toInstant().atZone(ZoneOffset.UTC).toLocalDateTime().toString());
                intervalF.out(task, "P1M");
                strategyF.out(task, "repeat continuously scheduling strategy");
            }

            message.setTask(task);
        } catch (Exception e) {
            throw new SmsRemindActorException("Failed to create task for sending sms remind", e);
        }
    }

    public void checkIsRemind(CheckSmsRemindMessage message) {
        try {
            Object isRemind = message.getIsRemind();
            message.setIsRemind(isRemind != null);
        } catch (ReadValueException e) {
            try {
                message.setIsRemind(false);
            } catch (ChangeValueException ex) {
                throw new RuntimeException(ex);
            }
        } catch (ChangeValueException e) {
            throw new RuntimeException(e);
        }
    }

    private IObject getUserFromDB(String userId) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                    Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"filter\": {\""+ userCollectionKey + "\": {\"$eq\": \"" + userId + "\"}}, \"collectionName\": \"" + userCollection + "\"}"
            );

            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    userCollection,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            searchResult.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();
            return searchResult.get(0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
