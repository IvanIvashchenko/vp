package com.perspective.vpng.plugin.sms_remind_actor;

import com.perspective.vpng.actor.sms_remind.SmsRemindActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class SmsRemindActorPlugin  extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public SmsRemindActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("SmsRemindActorPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("SmsRemindActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> new SmsRemindActor((IObject) args[0])));
    }
}
