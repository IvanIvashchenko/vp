package com.perspective.vpng.plugin.send_sms_actor;

import com.perspective.vpng.actor.send_sms.SendSmsActor;
import com.perspective.vpng.service.sms.ISMSServiceProtocol;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class SendSmsActorPlugin extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public SendSmsActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("SendSmsActorPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("SendSmsActor"),
                new ApplyFunctionToArgumentsStrategy(
                        (args) -> {
                            try {
                                IField protocolTypeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "protocolType");
                                ISMSServiceProtocol protocol = IOC.resolve(Keys.getOrAdd(protocolTypeF.in((IObject) args[0])));
                                return new SendSmsActor(() -> protocol);
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }
                )
        );
    }
}
