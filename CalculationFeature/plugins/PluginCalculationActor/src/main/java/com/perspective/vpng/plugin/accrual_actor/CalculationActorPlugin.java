package com.perspective.vpng.plugin.accrual_actor;

import com.perspective.vpng.actor.calculations.CalculationActor;
import com.perspective.vpng.actor.calculations.wrapper.IActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

import java.util.List;

public class CalculationActorPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor
     *
     * @param bootstrap the bootstrap
     */
    public CalculationActorPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("CalculationActorPlugin");
            item.process(() -> {
                try {
                    IKey actorKey = Keys.getOrAdd("CalculationActor");
                    IOC.register(actorKey, new ApplyFunctionToArgumentsStrategy(
                            (args) -> {
                                IObject config = (IObject) args[0];
                                try {
                                    IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                                    IField calculationKeyF = IOC.resolve(fieldKey, "calculationKey");
                                    IField paymentPeriodYearF = IOC.resolve(fieldKey, "paymentPeriodYear");
                                    IField paymentPeriodMonthF = IOC.resolve(fieldKey, "paymentPeriodMonth");
                                    IField schemaRequiredItemsF = IOC.resolve(fieldKey, "schemaRequiredItems");

                                    return new CalculationActor(new IActorConfig() {

                                        @Override
                                        public IPool getConnectionPool() throws ReadValueException {
                                            try {
                                                ConnectionOptions connectionOptions = IOC.resolve(
                                                        Keys.getOrAdd("PostgresConnectionOptions")
                                                );
                                                return IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
                                            } catch (ResolutionException ex) {
                                                throw new ReadValueException("Can't read connection pool field!", ex);
                                            }
                                        }

                                        @Override
                                        public List<String> getSchemaRequiredItems() throws ReadValueException {
                                            try {
                                                return schemaRequiredItemsF.in(config);
                                            } catch (InvalidArgumentException ex) {
                                                throw new ReadValueException("Can't read schema required items field!", ex);
                                            }
                                        }

                                        @Override
                                        public String getCalculationKey() throws ReadValueException {
                                            try {
                                                return calculationKeyF.in(config);
                                            } catch (InvalidArgumentException ex) {
                                                throw new ReadValueException("Can't read calculation key field name!", ex);
                                            }
                                        }

                                        @Override
                                        public String getPaymentPeriodYearName() throws ReadValueException {
                                            try {
                                                return paymentPeriodYearF.in(config);
                                            } catch (InvalidArgumentException ex) {
                                                throw new ReadValueException("Can't read payment period year field name!", ex);
                                            }
                                        }

                                        @Override
                                        public String getPaymentPeriodMonthName() throws ReadValueException {
                                            try {
                                                return paymentPeriodMonthF.in(config);
                                            } catch (InvalidArgumentException ex) {
                                                throw new ReadValueException("Can't read payment period month field name!", ex);
                                            }
                                        }
                                    });
                                } catch (Exception ex) {
                                    throw new RuntimeException(ex);
                                }
                            }));
                } catch (ResolutionException e) {
                    throw new ActionExecuteException("CalculationActor plugin can't load: can't get CalculationActor key", e);
                } catch (InvalidArgumentException e) {
                    throw new ActionExecuteException("CalculationActor plugin can't load: can't create rule", e);
                } catch (RegistrationException e) {
                    throw new ActionExecuteException("CalculationActor plugin can't load: can't register new rule", e);
                }
            });
            bootstrap.add(item);
        } catch (Exception e) {
            throw new PluginException("Can't load CalculationActor plugin", e);
        }
    }

}
