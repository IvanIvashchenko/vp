package com.perspective.vpng.strategy.getting_calculation_key;

import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;

import java.util.List;

public class GettingCalculationKeyStrategy implements IResolveDependencyStrategy {

    @Override
    public <T> T resolve(Object... args) throws ResolveDependencyStrategyException {
        try {
            if (args[0] instanceof List) {
                List<String> fragments = (List<String>) args[0];
                StringBuilder key = new StringBuilder();
                fragments.forEach(key::append);
                return (T) key.toString();
            }
            return (T) args[0];
        } catch (Exception ex) {
            throw new ResolveDependencyStrategyException(ex.getMessage(), ex);
        }
    }

}
