package com.perspective.vpng.actor.calculations.wrapper;

import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface IActorConfig {

    IPool getConnectionPool() throws ReadValueException;

    List<String> getSchemaRequiredItems() throws ReadValueException;

    String getCalculationKey() throws ReadValueException;

    String getPaymentPeriodYearName() throws ReadValueException;

    String getPaymentPeriodMonthName() throws ReadValueException;

}
