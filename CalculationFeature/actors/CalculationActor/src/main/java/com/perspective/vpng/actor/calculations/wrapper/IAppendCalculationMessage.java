package com.perspective.vpng.actor.calculations.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface IAppendCalculationMessage {

    List<IObject> getCalculation() throws ReadValueException;

    List<IObject> getSchema() throws ReadValueException;

    void setSchemaWithCalculation(List<IObject> schemaWithCalculation) throws ChangeValueException;

}
