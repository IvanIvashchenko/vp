package com.perspective.vpng.actor.calculations.exception;

public class CalculationActorException extends Exception {

    public CalculationActorException(String message) {
        super(message);
    }

    public CalculationActorException(String message, Throwable cause) {
        super(message, cause);
    }

}
