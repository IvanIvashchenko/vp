package com.perspective.vpng.actor.calculations;

import com.perspective.vpng.actor.calculations.exception.CalculationActorException;
import com.perspective.vpng.actor.calculations.wrapper.IActorConfig;
import com.perspective.vpng.actor.calculations.wrapper.IAppendCalculationMessage;
import com.perspective.vpng.actor.calculations.wrapper.ITakeCalculationMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class CalculationActor {

    private final IPool connectionPool;

    private final List<String> schemaRequiredItems;

    private final String calculationKeyName;
    private final String paymentPeriodYearName;
    private final String paymentPeriodMonthName;

    private final IField itemNameF;
    private final IField itemValF;

    public CalculationActor(final IActorConfig config) throws CalculationActorException {
        try {
            this.connectionPool = config.getConnectionPool();
            this.schemaRequiredItems = new ArrayList<>(config.getSchemaRequiredItems());

            this.calculationKeyName = config.getCalculationKey();
            this.paymentPeriodYearName = config.getPaymentPeriodYearName();
            this.paymentPeriodMonthName = config.getPaymentPeriodMonthName();

            IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
            this.itemNameF = IOC.resolve(fieldKey, "name");
            this.itemValF = IOC.resolve(fieldKey, "value");
        } catch (ReadValueException | ResolutionException ex) {
            throw new CalculationActorException(ex.getMessage(), ex);
        }
        checkActorState();
    }

    public void takeCalculation(final ITakeCalculationMessage message)
            throws CalculationActorException, PoolGuardException {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> calculations = new ArrayList<>();
            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    message.getCollectionName(),
                    prepareQueryParams(message.getCalculationKey()),
                    (IAction<IObject[]>) calculationDocs ->
                            calculations.addAll(Arrays.asList(calculationDocs))
            );
            task.execute();
            if (calculations.size() == 0) {
                throw new CalculationActorException(
                    "There are no calculations for this provider " + message.getCalculationKey() + " and date range."
                );
            }
            message.setCalculation(calculations.get(0));
        } catch (ReadValueException | InvalidArgumentException |
                TaskExecutionException | ResolutionException | ChangeValueException ex) {
            throw new CalculationActorException(ex.getMessage(), ex);
        }
    }

    public void appendCalculation(final IAppendCalculationMessage message) throws CalculationActorException {
        try {
            List<IObject> calculation = message.getCalculation();
            List<IObject> schema = message.getSchema();
            List<IObject> schemaWithCalculation = new ArrayList<>();
            String itemName;
            IObject calculationItem;
            IKey iObjKey = Keys.getOrAdd(IObject.class.getCanonicalName());
            int size = schema.size();
            for (int i = 0; i < size; i++) {
                IObject item = schema.get(i);
                itemName = itemNameF.in(item);
                calculationItem = tryGetItem(calculation, itemName, i);
                if (calculationItem != null) {
                    IObject itemWithVal = IOC.resolve(iObjKey, item);
                    itemValF.out(itemWithVal, itemValF.in(calculationItem));
                    schemaWithCalculation.add(itemWithVal);
                } else if (schemaRequiredItems.contains(itemName)) {
                    IObject itemCopy = IOC.resolve(iObjKey, item);
                    schemaWithCalculation.add(itemCopy);
                }
            }
            message.setSchemaWithCalculation(schemaWithCalculation);
        } catch (ReadValueException | ResolutionException |
                InvalidArgumentException | ChangeValueException ex) {
            throw new CalculationActorException(ex.getMessage(), ex);
        }
    }

    private IObject prepareQueryParams(String calculationKey)
            throws ResolutionException, ChangeValueException, InvalidArgumentException {
        Calendar calendar = Calendar.getInstance();
        String searchQuery = new StringBuilder()
                .append("{")
                    .append("\"filter\": {")
                        .append("\"").append(calculationKeyName).append("\": {\"$eq\": \"").append(calculationKey).append("\" },")
                        .append("\"" + paymentPeriodYearName + "\": {\"$gte\": " + (calendar.get(Calendar.YEAR) - 1) + "}")
                    .append("},")
                    .append("\"page\": {")
                        .append("\"size\": 24,")
                        .append("\"number\": 1")
                    .append("},")
                    .append("\"sort\": [")
                        .append("{\"").append(paymentPeriodYearName).append("\": \"desc\" },")
                        .append("{\"").append(paymentPeriodMonthName).append("\": \"desc\" }")
                    .append("]")
                .append( "}")
                .toString();
        return IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), searchQuery);
    }

    private IObject tryGetItem(final List<IObject> container, String itemName, int beginIndex)
            throws ReadValueException, InvalidArgumentException {
        int size = container.size();
        for (int i = beginIndex; i < size; i++) {
            IObject item = container.get(i);
            if (itemNameF.in(item).equals(itemName)) {
                return item;
            }
        }
        return null;
    }

    private void checkActorState() throws CalculationActorException {
        if (isNullOrEmpty(this.connectionPool) && isNullOrEmpty(this.calculationKeyName) &&
                isNullOrEmpty(this.paymentPeriodYearName) && isNullOrEmpty(this.paymentPeriodMonthName)) {
            throw new CalculationActorException("Illegal configuration for the calculations actor!");
        }
    }

    private boolean isNullOrEmpty(final Object obj) {
        if (obj instanceof String) {
            return ((String) obj).isEmpty();
        }
        return obj == null;
    }

}
