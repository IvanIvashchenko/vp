package com.perspective.vpng.actor.calculations.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ITakeCalculationMessage {

    String getCalculationKey() throws ReadValueException;

    String getCollectionName() throws ReadValueException;

    void setCalculation(IObject calculation) throws ChangeValueException;

}
