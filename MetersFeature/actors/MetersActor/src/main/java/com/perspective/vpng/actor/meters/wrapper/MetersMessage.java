package com.perspective.vpng.actor.meters.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for message for {@link com.perspective.vpng.actor.meters.MetersActor}
 */
public interface MetersMessage {

    /**
     * @return meters object
     * @throws ReadValueException if any error is occurred
     */
    IObject getMeters() throws ReadValueException;

    /**
     * @return user identifier
     * @throws ReadValueException if any error is occurred
     */
    String getUserGuid() throws ReadValueException;

    /**
     * Sets user identifier
     * @param userGuid user identifier
     * @throws ChangeValueException if any error is occurred
     */
    void setUserGuid(String userGuid) throws ChangeValueException;

    /**
     * Sets meters object identifier
     * @param guid identifier
     * @throws ChangeValueException
     */
    void setGuid(String guid) throws ChangeValueException;
}
