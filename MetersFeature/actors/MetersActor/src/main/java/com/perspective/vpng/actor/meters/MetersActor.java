package com.perspective.vpng.actor.meters;

import com.perspective.vpng.actor.meters.exception.MetersActorException;
import com.perspective.vpng.actor.meters.wrapper.MetersActorConfig;
import com.perspective.vpng.actor.meters.wrapper.MetersMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

/**
 * Actor for work with meters objects
 */
public class MetersActor {

    private final String metersCollectionName;
    private final IPool connectionPool;
    private final IField identifierF;

    public MetersActor(final MetersActorConfig config) throws MetersActorException {
        try {
            this.metersCollectionName = config.getCollectionName();
            this.connectionPool = config.getConnectionPool();
            this.identifierF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), metersCollectionName + "ID");
        } catch (ReadValueException | ResolutionException e) {
            throw new MetersActorException("Cam't create MetersActor", e);
        }
    }

    /**
     * Creates meters object into database, copies identifier to guid field
     * @param message contains meters object
     * @throws MetersActorException if any error is occurred
     */
    public void createMetersReading(final MetersMessage message) throws MetersActorException {

        try (PoolGuard guard = new PoolGuard(connectionPool)) {
            message.setUserGuid(message.getUserGuid() != null ? message.getUserGuid() : "");
            IObject meters = message.getMeters();
            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.insert"),
                guard.getObject(),
                metersCollectionName,
                meters
            );
            task.execute();

            String guid = identifierF.in(meters);
            message.setGuid(guid);
        } catch (ReadValueException | ResolutionException | TaskExecutionException | PoolGuardException | ChangeValueException |
                InvalidArgumentException e) {
            throw new MetersActorException("Can't create meters object from user", e);
        }
    }

    /**
     * Deletes meters from according collection
     * @param message contains meters with metersID field
     * @throws MetersActorException if any error is occurred
     */
    public void deleteMetersReading(final MetersMessage message) throws MetersActorException {

        try (PoolGuard guard = new PoolGuard(connectionPool)) {
            IObject meters = message.getMeters();
            String guid = identifierF.in(meters);
            message.setGuid(guid);

            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.delete"),
                guard.getObject(),
                metersCollectionName,
                meters
            );
            task.execute();
        } catch (ReadValueException | ResolutionException | TaskExecutionException | PoolGuardException |
                ChangeValueException | InvalidArgumentException e) {
            throw new MetersActorException("Can't create meters object from user", e);
        }
    }
}
