package com.perspective.vpng.actor.meters.exception;

public class MetersActorException extends Exception {

    public MetersActorException(final String message) {
        super(message);
    }

    public MetersActorException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
