package com.perspective.vpng.actor.meters.wrapper;

import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for config for {@link com.perspective.vpng.actor.meters.MetersActor}
 */
public interface MetersActorConfig {

    /**
     * @return meters collection name
     * @throws ReadValueException if any error is occurred
     */
    String getCollectionName() throws ReadValueException;

    /**
     * @return connection pool
     * @throws ReadValueException if any error is occurred
     */
    IPool getConnectionPool() throws ReadValueException;
}
