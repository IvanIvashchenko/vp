package com.perspective.vpng.plugin.meters;

import com.perspective.vpng.actor.meters.MetersActor;
import com.perspective.vpng.actor.meters.wrapper.MetersActorConfig;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class MetersPlugin extends BootstrapPlugin {

    public MetersPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }

    @BootstrapPlugin.Item("MetersPlugin")
    public void item()
        throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("MetersActor"),
            new ApplyFunctionToArgumentsStrategy((args) -> {
                IObject config = (IObject) args[0];
                try {
                    IKey fieldKey = Keys.getOrAdd(IField.class.getCanonicalName());
                    IField collectionNameF = IOC.resolve(fieldKey, "metersCollectionName");
                    return new MetersActor(new MetersActorConfig() {
                        @Override
                        public String getCollectionName() throws ReadValueException {
                            try {
                                return collectionNameF.in(config);
                            } catch (InvalidArgumentException ex) {
                                throw new ReadValueException("Can't read collection name!", ex);
                            }
                        }

                        @Override
                        public IPool getConnectionPool() throws ReadValueException {
                            try {
                                ConnectionOptions connectionOptions = IOC.resolve(
                                    Keys.getOrAdd("PostgresConnectionOptions")
                                );
                                return IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
                            } catch (ResolutionException ex) {
                                throw new ReadValueException("Can't read connection pool field!", ex);
                            }
                        }
                    });
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            })
        );
    }
}
