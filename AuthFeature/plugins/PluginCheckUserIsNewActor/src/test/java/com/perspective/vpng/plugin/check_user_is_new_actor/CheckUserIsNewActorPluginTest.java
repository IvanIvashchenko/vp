package com.perspective.vpng.plugin.check_user_is_new_actor;

import com.perspective.vpng.actor.check_user_is_new.CheckUserIsNewActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IFunction;
import info.smart_tools.smartactors.base.interfaces.iaction.IPoorAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@PrepareForTest({IOC.class, Keys.class, CheckUserIsNewActorPlugin.class, ApplyFunctionToArgumentsStrategy.class})
@RunWith(PowerMockRunner.class)
public class CheckUserIsNewActorPluginTest {
    private CheckUserIsNewActorPlugin plugin;
    private IBootstrap bootstrap;

    @Before
    public void setUp() throws Exception {

        mockStatic(IOC.class);
        mockStatic(Keys.class);

        bootstrap = mock(IBootstrap.class);
        plugin = new CheckUserIsNewActorPlugin(bootstrap);
    }

    @Test
    public void MustCorrectLoadPlugin() throws Exception {

        BootstrapItem bootstrapItem = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin").thenReturn(bootstrapItem);

        when(bootstrapItem.after(anyString())).thenReturn(bootstrapItem);
        when(bootstrapItem.before(anyString())).thenReturn(bootstrapItem);

        plugin.load();

        verifyNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin");

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);

        //verify(bootstrapItem).after("IOC");         verify(bootstrapItem).before("starter");
        verify(bootstrapItem).process(actionArgumentCaptor.capture());

        verify(bootstrap).add(bootstrapItem);

        IKey checkUserIsNewActorKey = mock(IKey.class);
        when(Keys.getOrAdd("CheckUserIsNewActor")).thenReturn(checkUserIsNewActorKey);

        actionArgumentCaptor.getValue().execute();

        verifyStatic();
        Keys.getOrAdd("CheckUserIsNewActor");

        ArgumentCaptor<ApplyFunctionToArgumentsStrategy> applyFunctionToArgStrategyArgumentCaptor = ArgumentCaptor.forClass(ApplyFunctionToArgumentsStrategy.class);

        verifyStatic();
        IOC.register(eq(checkUserIsNewActorKey), applyFunctionToArgStrategyArgumentCaptor.capture());

        CheckUserIsNewActor actor = mock(CheckUserIsNewActor.class);
        IObject actorParams = mock(IObject.class);
        whenNew(CheckUserIsNewActor.class).withArguments(actorParams).thenReturn(actor);

        assertTrue("Objects must return correct object", applyFunctionToArgStrategyArgumentCaptor.getValue().resolve(actorParams) == actor);

        verifyNew(CheckUserIsNewActor.class).withArguments(actorParams);
    }

   @Test
    public void MustInCorrectLoadNewIBootstrapItemThrowException() throws Exception {

        whenNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin").thenThrow(new InvalidArgumentException(""));

        try {
            plugin.load();
        } catch (PluginException e) {

            verifyNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin");
            return;
        }
        assertTrue("Must throw exception, but was not", false);
    }

    @Test
    public void MustInCorrectExecuteActionWhenKeysThrowException() throws Exception {

        BootstrapItem bootstrapItem = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin").thenReturn(bootstrapItem);

        //when(bootstrapItem.after(anyString())).thenReturn(bootstrapItem);
        //when(bootstrapItem.before(anyString())).thenReturn(bootstrapItem);

        plugin.load();

        verifyNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin");

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);

        //verify(bootstrapItem).after("IOC");         verify(bootstrapItem).before("starter");
        verify(bootstrapItem).process(actionArgumentCaptor.capture());

        verify(bootstrap).add(bootstrapItem);

        when(Keys.getOrAdd("CheckUserIsNewActor")).thenThrow(new ResolutionException(""));

        try {
            actionArgumentCaptor.getValue().execute();
        } catch (ActionExecuteException e) {
            verifyStatic();
            Keys.getOrAdd("CheckUserIsNewActor");
            return;
        }
        assertTrue("Must throw exception", false);
    }

    @Test
    public void MustInCorrectExecuteActionWhenIOCRegisterThrowException() throws Exception {

        BootstrapItem bootstrapItem = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin").thenReturn(bootstrapItem);

//        when(bootstrapItem.after(anyString())).thenReturn(bootstrapItem);
//        when(bootstrapItem.before(anyString())).thenReturn(bootstrapItem);

        plugin.load();

        verifyNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin");

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);

        //verify(bootstrapItem).after("IOC");         verify(bootstrapItem).before("starter");
        verify(bootstrapItem).process(actionArgumentCaptor.capture());

        verify(bootstrap).add(bootstrapItem);

        IKey createAsyncOpKey = mock(IKey.class);
        when(Keys.getOrAdd("CheckUserIsNewActor")).thenReturn(createAsyncOpKey);

        ArgumentCaptor<IFunction<Object[], Object>> targetFuncArgumentCaptor = ArgumentCaptor.forClass((Class) IFunction.class);

        doThrow(new RegistrationException("")).when(IOC.class);
        IOC.register(eq(createAsyncOpKey), any());

        whenNew(ApplyFunctionToArgumentsStrategy.class).withArguments(targetFuncArgumentCaptor.capture())
                .thenReturn(mock(ApplyFunctionToArgumentsStrategy.class));//the method which was used for constructor is importantly

        try {
            actionArgumentCaptor.getValue().execute();
        } catch (ActionExecuteException e) {

            verifyStatic();
            Keys.getOrAdd("CheckUserIsNewActor");

            verifyNew(ApplyFunctionToArgumentsStrategy.class).withArguments(targetFuncArgumentCaptor.getValue());

            verifyStatic();
            IOC.register(eq(createAsyncOpKey), any(ApplyFunctionToArgumentsStrategy.class));

            CheckUserIsNewActor actor = mock(CheckUserIsNewActor.class);
            IObject actorParams = mock(IObject.class);

            whenNew(CheckUserIsNewActor.class).withArguments(actorParams).thenReturn(actor);

            assertTrue("Objects must return correct object", targetFuncArgumentCaptor.getValue().execute(new Object[]{actorParams}) == actor);

            verifyNew(CheckUserIsNewActor.class).withArguments(actorParams);
            return;
        }
        assertTrue("Must throw exception", false);
    }

    @Test
    public void MustInCorrectExecuteActionWhenNewCreateInstanceThrowException() throws Exception {

        BootstrapItem bootstrapItem = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin").thenReturn(bootstrapItem);

        when(bootstrapItem.after(anyString())).thenReturn(bootstrapItem);
        when(bootstrapItem.before(anyString())).thenReturn(bootstrapItem);

        plugin.load();

        verifyNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin");

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);

        //verify(bootstrapItem).after("IOC");         verify(bootstrapItem).before("starter");
        verify(bootstrapItem).process(actionArgumentCaptor.capture());

        verify(bootstrap).add(bootstrapItem);

        IKey createAsyncOpKey = mock(IKey.class);
        when(Keys.getOrAdd(CheckUserIsNewActor.class.getCanonicalName())).thenReturn(createAsyncOpKey);

        ArgumentCaptor<IFunction<Object[], Object>> targetFuncArgumentCaptor = ArgumentCaptor.forClass((Class) IFunction.class);

        whenNew(ApplyFunctionToArgumentsStrategy.class).withArguments(targetFuncArgumentCaptor.capture())
                .thenThrow(new RegistrationException(""));//the method which was used for constructor is importantly

        try {
            actionArgumentCaptor.getValue().execute();
        } catch (ActionExecuteException e) {

            verifyStatic();
            Keys.getOrAdd("CheckUserIsNewActor");

            verifyNew(ApplyFunctionToArgumentsStrategy.class).withArguments(targetFuncArgumentCaptor.getValue());

            CheckUserIsNewActor actor = mock(CheckUserIsNewActor.class);
            IObject actorParams = mock(IObject.class);

            whenNew(CheckUserIsNewActor.class).withArguments(actorParams).thenReturn(actor);

            assertTrue("Objects must return correct object", targetFuncArgumentCaptor.getValue().execute(new Object[]{actorParams}) == actor);

            verifyNew(CheckUserIsNewActor.class).withArguments(actorParams);
            return;
        }
        assertTrue("Must throw exception", false);
    }

    @Test
    public void MustInCorrectResolveWhenNewCheckUserIsNewActorThrowException() throws Exception {

        BootstrapItem bootstrapItem = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin").thenReturn(bootstrapItem);

        when(bootstrapItem.after(anyString())).thenReturn(bootstrapItem);
        when(bootstrapItem.before(anyString())).thenReturn(bootstrapItem);

        plugin.load();

        verifyNew(BootstrapItem.class).withArguments("CheckUserIsNewActorPlugin");

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);

        //verify(bootstrapItem).after("IOC");
        //verify(bootstrapItem).before("starter");
        verify(bootstrapItem).process(actionArgumentCaptor.capture());

        verify(bootstrap).add(bootstrapItem);

        IKey checkUserIsNewActorKey = mock(IKey.class);
        when(Keys.getOrAdd("CheckUserIsNewActor")).thenReturn(checkUserIsNewActorKey);

        ArgumentCaptor<IFunction> targetFuncArgumentCaptor = ArgumentCaptor.forClass((Class) IFunction.class);

        ApplyFunctionToArgumentsStrategy applyFunctionToArgumentsStrategy = mock(ApplyFunctionToArgumentsStrategy.class);
        whenNew(ApplyFunctionToArgumentsStrategy.class)
                .withArguments(targetFuncArgumentCaptor.capture())
                .thenReturn(applyFunctionToArgumentsStrategy);

        actionArgumentCaptor.getValue().execute();

        verifyStatic();
        Keys.getOrAdd("CheckUserIsNewActor");

        verifyStatic();
        IOC.register(checkUserIsNewActorKey, applyFunctionToArgumentsStrategy);

        IObject actorParams = mock(IObject.class);

        whenNew(CheckUserIsNewActor.class).withArguments(actorParams).thenThrow(new InvalidArgumentException(""));

        try {
            targetFuncArgumentCaptor.getValue().execute(new Object[]{actorParams});
        } catch (RuntimeException e) {
            verifyNew(CheckUserIsNewActor.class).withArguments(actorParams);
            return;
        }
        assertTrue("Must throw exception", false);
    }
}
