/**
 * Package contains plugin with rule for registration
 * {@link com.perspective.vpng.actor.authentication.users.UserAuthByLoginActor}
 */
package com.perspective.vpng.plugin.user_auth_by_login;