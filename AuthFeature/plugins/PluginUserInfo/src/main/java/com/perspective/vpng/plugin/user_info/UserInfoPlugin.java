package com.perspective.vpng.plugin.user_info;

import com.perspective.vpng.actor.user_info.UserInfoActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class UserInfoPlugin implements IPlugin {
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    public UserInfoPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("UserInfoActorPlugin");

            item
//                    .after("IOC")
//                    .before("starter")
                    .process(()-> {
                        try {
                            IKey ruleKey = Keys.getOrAdd(UserInfoActor.class.getCanonicalName());
                            IOC.register(ruleKey, new ApplyFunctionToArgumentsStrategy(
                                    (args) -> {
                                        try {
                                            return new UserInfoActor();
                                        } catch (Exception e) {
                                            throw new RuntimeException("Failed to create UserInfoActor", e);
                                        }
                                    }
                            ));
                        } catch (ResolutionException e) {
                            throw new ActionExecuteException("Can't get key for load", e);
                        } catch (InvalidArgumentException ex) {
                            throw new ActionExecuteException("Can't apply lambda", ex);
                        } catch (RegistrationException e) {
                            throw new ActionExecuteException(e.getMessage());
                        }
                    });

            bootstrap.add(item);
        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't create BootstrapItem for UserInfoActorPlugin", e);
        }
    }

}
