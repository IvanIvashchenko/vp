/**
 * Package contains 'CreateUserActor' plugin.
 * Implementation of {@link info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin}.
 */
package com.perspective.vpng.plugin.create_user;