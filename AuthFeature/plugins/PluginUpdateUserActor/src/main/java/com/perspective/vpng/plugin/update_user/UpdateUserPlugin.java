package com.perspective.vpng.plugin.update_user;

import com.perspective.vpng.actor.update_user.UpdateUserActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin.
 * Implements {@link IPlugin}
 * UpdateUserActor.
 */
public class UpdateUserPlugin implements IPlugin {
    /** Local storage for instance of {@link IBootstrap}*/
    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor for UpdateUserActor
     * @param bootstrap instance of {@link IBootstrap}
     * @throws InvalidArgumentException if any errors occurred
     */
    public UpdateUserPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) throws InvalidArgumentException {
        if (null == bootstrap) {
            throw new InvalidArgumentException("Incoming argument should be not null.");
        }
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("UpdateUserActorPlugin");

            item
                    .process(() -> {
                        try {
                            IKey createCreateUserKey = Keys.getOrAdd("UpdateUserActor");
                            try {
                                IOC.register(createCreateUserKey, new ApplyFunctionToArgumentsStrategy(
                                        (args) -> new UpdateUserActor((IObject) args[0])
                                ));
                            } catch (RegistrationException e) {
                                throw new RuntimeException(e);
                            } catch (InvalidArgumentException e) {
                                throw new RuntimeException("Can't create actor with this args: ", e);
                            }
                        } catch (ResolutionException e) {
                            throw new RuntimeException("Can't get ActorParams wrapper or Key for ActorParams", e);
                        }
                    });
            bootstrap.add(item);
        } catch (Exception e) {
            throw new PluginException(e);
        }
    }
}
