package com.perspective.vpng.actor.authentication;

import com.perspective.vpng.actor.authentication.exception.AuthFailException;
import com.perspective.vpng.actor.authentication.wrapper.AuthenticationMessage;
import com.perspective.vpng.actor.authentication.wrapper.CheckUserIsAuthMessage;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.Objects;

/**
 * Actor that validate the created session
 */
public class AuthenticationActor {

    /**
     * Default constructor
     */
    public AuthenticationActor() {}

    /**
     * Validate the session
     * @param message the incoming message
     * @throws AuthFailException Throw when auth info is null or auth info is not equal with session auth info
     */
    public void authenticateSession(final AuthenticationMessage message) throws AuthFailException {
        try {
            String requestAuthInfo = message.getRequestUserAgent();
            String sessionAuthInfo = message.getSessionUserAgent();
            if (requestAuthInfo == null || !Objects.equals(requestAuthInfo, sessionAuthInfo)) {
                throw new AuthFailException("Failed to validate session: authentication info is incorrect");
            }
        } catch (ReadValueException e) {
            //Throw when one of parameters can't be read
            throw new AuthFailException("Failed to validate session: one of parameters can't be read", e);
        }
    }

    public void checkUserIsAuth(CheckUserIsAuthMessage message) throws AuthFailException {
        try {
            Object isUser = message.getUserId();
            message.setIsAuthFlag(isUser != null);
        } catch (ReadValueException | ChangeValueException e) {
            throw new AuthFailException("Failed to validate user", e);
        }
    }
}
