/**
 * Contains exceptions for authentication actor
 */
package com.perspective.vpng.actor.authentication.exception;