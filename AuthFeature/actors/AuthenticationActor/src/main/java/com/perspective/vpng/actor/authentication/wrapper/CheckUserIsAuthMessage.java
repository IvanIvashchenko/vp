package com.perspective.vpng.actor.authentication.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface CheckUserIsAuthMessage {
    Object getUserId() throws ReadValueException;
    void setIsAuthFlag(Boolean flag) throws ChangeValueException;
}
