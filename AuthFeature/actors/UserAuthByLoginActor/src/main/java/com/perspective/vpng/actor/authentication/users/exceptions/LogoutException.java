package com.perspective.vpng.actor.authentication.users.exceptions;

public class LogoutException extends Exception {
    /**
     * Constructor with specific error message
     * @param message specific error message
     */
    public LogoutException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific cause as arguments
     * @param cause specific cause
     */
    public LogoutException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public LogoutException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
