/**
 * Package contains actor for auth user by login/password pair
 */
package com.perspective.vpng.actor.authentication.users;