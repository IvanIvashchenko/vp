package com.perspective.vpng.actor.authentication.users.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface ILogoutMessage {
    IObject getSession() throws ReadValueException;

}
