/**
 * Package contains exceptions for {@link com.perspective.vpng.actor.authentication.users.UserAuthByLoginActor}
 */
package com.perspective.vpng.actor.authentication.users.exceptions;