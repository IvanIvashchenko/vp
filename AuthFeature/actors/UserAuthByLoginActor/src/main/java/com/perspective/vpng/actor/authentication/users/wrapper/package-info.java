/**
 * Package contains wrapper for {@link com.perspective.vpng.actor.authentication.users.UserAuthByLoginActor}
 */
package com.perspective.vpng.actor.authentication.users.wrapper;