package com.perspective.vpng.actor.check_user_by_email.wrapper;

import com.perspective.vpng.actor.check_user_by_email.CheckUserByEmailActor;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for params for {@link CheckUserByEmailActor}
 */
public interface ActorParams {
    /**
     * Return the collection name for users
     * @return Name of target collection
     * @throws ReadValueException Throw when can't correct read value
     */
    String getCollectionName() throws ReadValueException;

    /**
     * @return the key for target collection
     * @throws ReadValueException Throw when can't correct read value
     */
    String getCollectionKey() throws ReadValueException;
}
