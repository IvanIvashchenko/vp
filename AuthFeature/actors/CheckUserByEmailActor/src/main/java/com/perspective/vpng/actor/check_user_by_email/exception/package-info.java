/**
 * Contain exceptions for {@link com.perspective.vpng.actor.check_user_by_email.CheckUserByEmailActor}
 */
package com.perspective.vpng.actor.check_user_by_email.exception;