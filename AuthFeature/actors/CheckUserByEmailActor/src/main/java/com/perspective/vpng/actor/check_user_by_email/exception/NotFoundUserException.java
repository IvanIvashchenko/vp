package com.perspective.vpng.actor.check_user_by_email.exception;

import com.perspective.vpng.actor.check_user_by_email.CheckUserByEmailActor;

/**
 * Exception for {@link CheckUserByEmailActor} checkUser method
 */
public class NotFoundUserException extends Exception {
    /**
     * Constructor with specific error message
     * @param message specific error message
     */
    public NotFoundUserException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific cause as arguments
     * @param cause specific cause
     */
    public NotFoundUserException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public NotFoundUserException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
