package com.perspective.vpng.actor.create_user.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for message
 */
public interface MessageWrapper {
    /**
     * getter for User
     * @return user
     * @throws ReadValueException Throw when can't correct read value
     */
    IObject getUser() throws ReadValueException;
}
