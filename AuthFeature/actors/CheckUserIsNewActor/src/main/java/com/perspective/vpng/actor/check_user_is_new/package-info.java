/**
 * Actor that check that user with this email was not registered before
 */
package com.perspective.vpng.actor.check_user_is_new;