package com.perspective.vpng.actor.check_user_is_new.wrapper;

import com.perspective.vpng.actor.check_user_is_new.CheckUserIsNewActor;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for message for {@link CheckUserIsNewActor} check method
 */
public interface MessageWrapper {

    /**
     * getter for email
     * @return email
     * @throws ReadValueException Throw when can't correct read value
     */
    String getEmail() throws ReadValueException;
}
