package com.perspective.vpng.actor.restore_password;

import com.perspective.vpng.actor.restore_password.exception.RestorePasswordException;
import com.perspective.vpng.actor.restore_password.wrapper.RestorePasswordConfig;
import com.perspective.vpng.actor.restore_password.wrapper.RestorePasswordMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.database.database_storage.utils.CollectionName;
import info.smart_tools.smartactors.database_in_memory_plugins.in_memory_database_plugin.PluginInMemoryDatabase;
import info.smart_tools.smartactors.database_in_memory_plugins.in_memory_db_tasks_plugin.PluginInMemoryDBTasks;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.exception.ProcessExecutionException;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.security_plugins.password_encoder_plugin.PasswordEncoderPlugin;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RestorePasswordActor.class)
public class RestorePasswordActorTest {

    private RestorePasswordActor actor;
    private RestorePasswordMessage message;
    private CollectionName collectionName;
    private IPool connectionPool;

    private IField passwordF;

    @BeforeClass
    public static void prepareIOC() throws PluginException, ProcessExecutionException, InvalidArgumentException {
        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new PasswordEncoderPlugin(bootstrap).load();

        new PluginInMemoryDatabase(bootstrap).load();
        new PluginInMemoryDBTasks(bootstrap).load();
        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {
        this.collectionName = CollectionName.fromString("collectionName");
        this.connectionPool = mock(IPool.class);

        String charsetName = "utf-8";
        String algorithmName = "MD5";
        String encoderName = "Base64Encoder";

        passwordF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "пароль");

        message = mock(RestorePasswordMessage.class);
        RestorePasswordConfig config = mock(RestorePasswordConfig.class);
        when(config.getCollectionName()).thenReturn(collectionName.toString());
        when(config.getConnectionPool()).thenReturn(connectionPool);
        when(config.getAlgorithm()).thenReturn(algorithmName);
        when(config.getCharset()).thenReturn(charsetName);
        when(config.getEncoder()).thenReturn(encoderName);

        IObject createOptions = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        try (PoolGuard guard = new PoolGuard(connectionPool)) {
            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.create"),
                    guard.getObject(),
                    collectionName,
                    createOptions
            );
            task.execute();
        }
        actor = new RestorePasswordActor(config);
    }

    @Test
    public void Should_CorrectRestorePassword_ById() throws Exception {
        when(message.getEmail()).thenReturn("111");
        when(message.getNewPassword()).thenReturn("qwerty");
        when(message.getConfirmPassword()).thenReturn("qwerty");
        String qwertyHash = "2FeO34RYzgb7xbt2pYxcpA==";

        IObject user1 = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"email\": \"111\", \"пароль\": \"not_qwerty\"}");
        IObject user2 = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"email\": \"222\", \"пароль\": \"qwerty\"}");

        try (PoolGuard guard = new PoolGuard(connectionPool)) {
            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.insert"),
                    guard.getObject(),
                    collectionName,
                    user1
            );
            task.execute();
            task = IOC.resolve(
                    Keys.getOrAdd("db.collection.insert"),
                    guard.getObject(),
                    collectionName,
                    user2
            );
            task.execute();
        }

        actor.restorePasswordByUserEmail(message);

        verify(message, atLeastOnce()).getEmail();
        verify(message, atLeastOnce()).getNewPassword();

        List<IObject> users = new ArrayList<>();

        IObject searchQuery = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                String.format(
                        "{ " +
                                "\"filter\": { \"email\": { \"$eq\": \"%s\" } }" +
                                "}",
                        "111")
        );

        passwordF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "пароль");

        try (PoolGuard guard = new PoolGuard(connectionPool)) {
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    guard.getObject(),
                    collectionName,
                    searchQuery,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            users.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );
            searchTask.execute();
        }

        assertEquals(passwordF.in(users.get(0)), qwertyHash);
    }

    @Test(expected = RestorePasswordException.class)
    public void Should_ThrowException_WhenNewPasswordEqualOldPassword() throws Exception {
        when(message.getEmail()).thenReturn("222");
        when(message.getNewPassword()).thenReturn("qwerty");
        when(message.getConfirmPassword()).thenReturn("qwerty");

        actor.restorePasswordByUserEmail(message);

        verify(message, atLeastOnce()).getEmail();
        verify(message, atLeastOnce()).getNewPassword();
    }


    @Test(expected = RestorePasswordException.class)
    public void Should_ThrowException_When_UserNotFound() throws Exception {
        when(message.getEmail()).thenReturn("111");
        when(message.getNewPassword()).thenReturn("qwerty");
        when(message.getConfirmPassword()).thenReturn("qwerty");

        actor.restorePasswordByUserEmail(message);
    }

    @Test(expected = RestorePasswordException.class)
    public void Should_ThrowException_When_UserIdIsNull() throws Exception {
        when(message.getEmail()).thenReturn(null);
        when(message.getNewPassword()).thenReturn("qwerty");
        when(message.getConfirmPassword()).thenReturn("qwerty");

        actor.restorePasswordByUserEmail(message);
    }

    @Test(expected = RestorePasswordException.class)
    public void Should_ThrowException_When_UserIdIsEmpty() throws Exception {
        when(message.getEmail()).thenReturn("");
        when(message.getNewPassword()).thenReturn("qwerty");

        actor.restorePasswordByUserEmail(message);
    }

    @Test(expected = RestorePasswordException.class)
    public void Should_ThrowException_When_DBIsShutdown() throws Exception{
        when(message.getEmail()).thenReturn("111");
        when(message.getNewPassword()).thenReturn("qwerty");
        when(message.getConfirmPassword()).thenReturn("qwerty");

        RestorePasswordConfig config = mock(RestorePasswordConfig.class);
        when(config.getCollectionName()).thenReturn("incorrectName");
        when(config.getConnectionPool()).thenReturn(mock(IPool.class));

        actor = new RestorePasswordActor(config);
        actor.restorePasswordByUserEmail(message);
    }
}
