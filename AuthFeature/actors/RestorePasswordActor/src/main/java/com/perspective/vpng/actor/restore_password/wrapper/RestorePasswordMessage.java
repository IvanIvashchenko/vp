package com.perspective.vpng.actor.restore_password.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface RestorePasswordMessage {

    /**
     * Getter for email
     * @return email
     * @throws ReadValueException if any error during get is occurred
     */
    String getEmail() throws ReadValueException;

    /**
     * Getter for new password
     * @return new password
     * @throws ReadValueException if any error during get is occurred
     */
    String getNewPassword() throws ReadValueException;

    /**
     * Getter for confirm new password
     * @return new password2
     * @throws ReadValueException if any error during get is occurred
     */
    String getConfirmPassword() throws ReadValueException;

    /**
     * Setter for authentication status
     * @param status text status
     * @throws ChangeValueException if any error is occurred
     */
    void setAuthStatus(final String status) throws ChangeValueException;

    /**
     * Setter: message to respond
     * @param message text message
     * @throws ChangeValueException if any error is occurred
     */
    void setAuthMessage(final String message) throws ChangeValueException;
}