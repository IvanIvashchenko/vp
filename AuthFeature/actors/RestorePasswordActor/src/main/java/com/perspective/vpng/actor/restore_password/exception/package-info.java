/**
 * Contains exceptions for {@link com.perspective.vpng.actor.restore_password.RestorePasswordActor}
 */
package com.perspective.vpng.actor.restore_password.exception;