package com.perspective.vpng.actor.restore_password;

import com.perspective.vpng.actor.restore_password.exception.RestorePasswordException;
import com.perspective.vpng.actor.restore_password.wrapper.PrepareEmailMessage;
import com.perspective.vpng.actor.restore_password.wrapper.RestorePasswordConfig;
import com.perspective.vpng.actor.restore_password.wrapper.RestorePasswordMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.security.encoding.encoders.EncodingException;
import info.smart_tools.smartactors.security.encoding.encoders.IPasswordEncoder;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Actor that restores user password
 */
public class RestorePasswordActor {
    private String SIGN_MESSAGE = "ВсеПлатежи";
    private String SUBJECT_MESSAGE = "Восстановление пароля";
    private static final String RESTORE_PASSWORD_ERROR_MSG = "Restoring password has been failed because: ";

    private String collectionName;
    private IPool connectionPool;
    private IPasswordEncoder passwordEncoder;

    private static IField signF;
    private static IField subjectF;
    private static IField textF;
    private static IField typeF;
    private static IField recipientF;

    private IField passwordF;

    private String MESSAGE_TEMPLATE =
            "<p>" +
                    "<b>Уважаемый пользователь!</b>\n" +
                    "</p>\n" +
                    "<br>\n" +
                    "<p>\n" +
                    "Нажмите на ссылку для подтверждения смены пароля: <a href=\"%s&token=%s\" target=\"_blank\" rel=\"noopener\">%s&token=%s</a>\n" +
                    "</p>\n" +
                    "<p>\n" +
                    "Если Вы не пытались изменить пароль на сервисе <a href=\"https://vp.ru\" target=\"_blank\" rel=\"noopener\">ВсеПлатежи</a> и считаете это письмо ошибкой, просто игнорируйте данное сообщение.\n" +
                    "</p>\n" +
                    "<p>\n" +
                    "С уважением,<br>\n" +
                    "команда <a href=\"https://vp.ru\" target=\"_blank\" rel=\"noopener\">ВсеПлатежи</a><br>\n" +
                    "</p>";

    /**
     * Default constructor
     * @param params contains collectionName and connectionPool
     * @throws RestorePasswordException when throws any exception during creates actor
     */
    public RestorePasswordActor(RestorePasswordConfig params) throws RestorePasswordException {
        try {
            collectionName = params.getCollectionName();
            connectionPool = params.getConnectionPool();
            passwordEncoder = IOC.resolve(
                    Keys.getOrAdd("PasswordEncoder"),
                    params.getAlgorithm(),
                    params.getEncoder(),
                    params.getCharset()
            );

            signF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sign");
            subjectF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "subject");
            textF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "text");
            recipientF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "recipient");
            typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");

            passwordF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "пароль");
        } catch (ReadValueException | ResolutionException e) {
            throw new RestorePasswordException("Can't create RestorePasswordActor: ", e);
        }
    }

    /**
     * Handler that prepares email for restoring password
     * @param message input message
     * @throws RestorePasswordException when throws any exception
     */
    public void prepareEmail(PrepareEmailMessage message) throws RestorePasswordException {
        try {
            String email = message.getEmail();
            String url = message.getRestorePasswordUrl();
            String token = message.getToken();

            IObject recipientObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            recipientF.out(recipientObject, email);
            typeF.out(recipientObject, "To");
            message.setRecipients(Collections.singletonList(recipientObject));

            IObject attributes = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            signF.out(attributes, new String(SIGN_MESSAGE.getBytes(), Charset.forName("ISO-8859-1")));
            subjectF.out(attributes, SUBJECT_MESSAGE);
            message.setMessageAttributes(attributes);

            IObject part = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"type\": \"text\", \"mime\": \"text/html\"}");
            textF.out(part, new String(String.format(MESSAGE_TEMPLATE, url, token, url, token).getBytes(Charset.forName("UTF-8")), Charset.forName("ISO-8859-1")));
            message.setMessageParts(Collections.singletonList(part));
        } catch (InvalidArgumentException | ResolutionException | ReadValueException | ChangeValueException e) {
            throw new RestorePasswordException("Failed to prepare email for restore password", e);
        }
    }

    /**
     * Handler that restores password
     * Await:
     * {
     *     "EMAIL" : "EMAIL OF TARGET USER"
     * }
     * @param message input message
     * @throws RestorePasswordException when throws any exception
     */
    public void restorePasswordByUserEmail(RestorePasswordMessage message) throws RestorePasswordException {
        try (PoolGuard guard = new PoolGuard(connectionPool)){
            String email = message.getEmail();
            String newPassword = message.getNewPassword();
            String confirmPassword = message.getConfirmPassword();

            validateEmail(email);
            validatePasswords(newPassword, confirmPassword);

            IObject searchQuery = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                    String.format(
                            "{ " +
                                    "\"filter\": { \"email\": { \"$eq\": \"%s\" } }" +
                                    "}",
                            email)
            );
            List<IObject> users = new ArrayList<>();

            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    guard.getObject(),
                    collectionName,
                    searchQuery,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            users.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );
            task.execute();

            validateSearchResult(users, message);
            IObject user = users.get(0);
            if (passwordF.in(user).equals(passwordEncoder.encode(newPassword))) {
                throw new RestorePasswordException("New password doesn't equal old password");
            }
            passwordF.out(user, passwordEncoder.encode(newPassword));

            ITask insertTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.upsert"),
                    guard.getObject(),
                    collectionName,
                    user
            );
            insertTask.execute();
        } catch (ReadValueException | ChangeValueException e) {
            throw new RestorePasswordException("Can't handle RestorePasswordMessage: ", e);
        } catch (ResolutionException e) {
            throw new RestorePasswordException("Can't resolve query objects: ", e);
        } catch (InvalidArgumentException e) {
            throw new RestorePasswordException("Can't prepare search query: ", e);
        } catch (PoolGuardException e) {
            throw new RestorePasswordException("Error during try get connection to database", e);
        } catch (TaskExecutionException e) {
            throw new RestorePasswordException("Error during task execution", e);
        } catch (EncodingException e) {
            throw new RestorePasswordException("Error during encoding password", e);
        }
    }

    private void validateEmail(String email) throws RestorePasswordException {
        if (email == null || email.equals(""))
            throw new RestorePasswordException(RESTORE_PASSWORD_ERROR_MSG +
                    "email for restoring password is null!");
    }

    private void validatePasswords(String pass1, String pass2) throws RestorePasswordException {
        if ((pass1 == null || pass1.equals("")) || (pass2 == null || pass2.equals(""))) {
            throw new RestorePasswordException(RESTORE_PASSWORD_ERROR_MSG +
                    "new password is null!");
        }
        if (!pass1.equals(pass2)) {
            throw new RestorePasswordException(RESTORE_PASSWORD_ERROR_MSG +
                    "password and confirm password must be equals!");
        }
    }

    private void validateSearchResult(final List<IObject> searchResult, final RestorePasswordMessage message) throws RestorePasswordException {
        try {
            if (searchResult.isEmpty()) {
                setFailResponse(message, "Can't find user with such email: " + message.getEmail());
                throw new RestorePasswordException(RESTORE_PASSWORD_ERROR_MSG +
                        "user with email: [" + message.getEmail() + "] doesn't exist!");
            }
            if (searchResult.size() > 1) {
                setFailResponse(message, "There are several users with such email: " + message.getEmail());
                throw new RestorePasswordException(RESTORE_PASSWORD_ERROR_MSG +
                        "too many users with email: [" + message.getEmail() + "]!");
            }
        } catch (ReadValueException | ChangeValueException e) {
            throw new RestorePasswordException(RESTORE_PASSWORD_ERROR_MSG + e.getMessage(), e);
        }
    }

    private void setFailResponse(final RestorePasswordMessage message, final String errorMessage) throws ChangeValueException {
        message.setAuthStatus("FAIL");
        message.setAuthMessage(errorMessage);
    }
}