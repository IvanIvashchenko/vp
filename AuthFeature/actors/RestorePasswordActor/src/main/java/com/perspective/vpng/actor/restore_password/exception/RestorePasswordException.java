package com.perspective.vpng.actor.restore_password.exception;

/**
 * Exceptions for RestorePasswordActor
 */
public class RestorePasswordException extends Exception {
    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public RestorePasswordException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause   specific cause
     */
    public RestorePasswordException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public RestorePasswordException(final Throwable cause) {
        super(cause);
    }
}
