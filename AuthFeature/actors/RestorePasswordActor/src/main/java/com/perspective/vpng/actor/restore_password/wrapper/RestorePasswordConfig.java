package com.perspective.vpng.actor.restore_password.wrapper;

import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;

public interface RestorePasswordConfig {

    /**
     * Collection Name getter
     * @return collection name
     * @throws ReadValueException if error during get is occurred
     */
    String getCollectionName() throws ReadValueException;

    /**
     * Connection Pool getter
     * @return connection pool
     * @throws ReadValueException if error during get is occurred
     */
    IPool getConnectionPool() throws ReadValueException;

    /**
     * @return algorithm name
     * @throws ReadValueException if any error is occurred
     */
    String getAlgorithm() throws ReadValueException;

    /**
     * @return charset name
     * @throws ReadValueException if any error is occurred
     */
    String getCharset() throws ReadValueException;

    /**
     * @return encoder name
     * @throws ReadValueException if any error is occurred
     */
    String getEncoder() throws ReadValueException;
}