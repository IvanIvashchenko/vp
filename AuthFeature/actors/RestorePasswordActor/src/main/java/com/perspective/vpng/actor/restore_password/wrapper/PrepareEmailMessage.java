package com.perspective.vpng.actor.restore_password.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for prepareEmail handler by {@link com.perspective.vpng.actor.restore_password.RestorePasswordActor}
 */
public interface PrepareEmailMessage {

    /**
     * Getter for email
     * @return email
     * @throws ReadValueException if any error during get is occurred
     */
    String getEmail() throws ReadValueException;

    /**
     * Getter for url
     * @return url
     * @throws ReadValueException if any error during get is occurred
     */
    String getRestorePasswordUrl() throws ReadValueException;

    /**
     * Getter for token
     * @return token
     * @throws ReadValueException if any error during get is occurred
     */
    String getToken() throws ReadValueException;

    /**
     * Setter for recipients email
     * @param emails list of emails
     * @throws ChangeValueException if any error during set is occurred
     */
    void setRecipients(List<IObject> emails) throws ChangeValueException;

    /**
     * Setter for email parts
     * @param parts list of IObjects
     * @throws ChangeValueException if any error during set is occurred
     */
    void setMessageParts(List<IObject> parts) throws ChangeValueException;

    /**
     * Setter for email parts
     * @param attributes IObject
     * @throws ChangeValueException if any error during set is occurred
     */
    void setMessageAttributes(IObject attributes) throws ChangeValueException;
}