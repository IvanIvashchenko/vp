package com.perspective.vpng.actor.update_user.exception;

public class UpdateUserException extends Exception {
    /**
     * Constructor with specific error message
     * @param message specific error message
     */
    public UpdateUserException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific cause as arguments
     * @param cause specific cause
     */
    public UpdateUserException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public UpdateUserException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
