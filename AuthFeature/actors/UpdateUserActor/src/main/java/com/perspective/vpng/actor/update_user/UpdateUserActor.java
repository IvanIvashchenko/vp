package com.perspective.vpng.actor.update_user;

import com.perspective.vpng.actor.update_user.exception.UpdateUserException;
import com.perspective.vpng.actor.update_user.wrapper.UpdateUserMessage;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.security.encoding.encoders.IPasswordEncoder;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.*;

public class UpdateUserActor {
    private String USER_COLLECTION = "user_account";
    private IPool connectionPool;
    private IPasswordEncoder passwordEncoder;


    private IField oldPasswordF;
    private IField passwordF;
    private IField confirmPasswordF;
    private IField clientF;
    private IField emailF;

    public UpdateUserActor(IObject params) {
        try {
            oldPasswordF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "старый-пароль");
            passwordF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "пароль");
            confirmPasswordF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "подтверждение-пароля");
            emailF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "email");
            clientF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "клиент");

            IField algorithmF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "algorithm");
            IField encoderF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "encoder");
            IField charsetF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "charset");
            this.passwordEncoder = IOC.resolve(
                    Keys.getOrAdd("PasswordEncoder"),
                    algorithmF.in(params),
                    encoderF.in(params),
                    charsetF.in(params)
            );

            ConnectionOptions connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
        } catch (Exception e) {
            throw new RuntimeException("Failed to create instance of UpdateUserActor", e);
        }
    }

    public void updateUser(UpdateUserMessage message) throws UpdateUserException {
        try {
            String userId = message.getUserId();
            IObject clientUser = message.getFormData();
            IObject user = getUserFromDB(userId);

            if (!passwordF.in(clientUser).equals("") && !confirmPasswordF.in(clientUser).equals("") && !oldPasswordF.in(clientUser).equals("")) {

                if (!passwordEncoder.encode(oldPasswordF.in(clientUser)).equals(passwordF.in(user))) {
                    throw new UpdateUserException("Wrong old password");
                }

                if (!passwordF.in(clientUser).equals(confirmPasswordF.in(clientUser))) {
                    throw new UpdateUserException("Password and confirm passwords are not equals");
                }

                passwordF.out(user, passwordEncoder.encode(passwordF.in(clientUser)));
            }

            clientF.out(user, clientF.in(clientUser));
            emailF.out(user, emailF.in(clientUser));

            saveUserInDB(user);
        } catch (Exception e) {
            throw new UpdateUserException("Failed to update user", e);
        }
    }

    private void saveUserInDB(IObject user) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            ITask upsertTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.upsert"),
                    poolGuard.getObject(),
                    USER_COLLECTION,
                    user
            );
            upsertTask.execute();
        } catch (PoolGuardException | ResolutionException | TaskExecutionException e) {
            throw new RuntimeException("Can't execute database operation.", e);
        }
    }

    private IObject getUserFromDB(String userId) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                    Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"filter\": {\"userId\": {\"$eq\": \"" + userId + "\"}}, \"collectionName\": \"" + USER_COLLECTION + "\"}"
            );

            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    USER_COLLECTION,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            searchResult.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();
            return searchResult.get(0);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
