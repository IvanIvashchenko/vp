package com.perspective.vpng.actor.update_user.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface UpdateUserMessage {
    IObject getFormData() throws ReadValueException;
    String getUserId() throws ReadValueException;
}
