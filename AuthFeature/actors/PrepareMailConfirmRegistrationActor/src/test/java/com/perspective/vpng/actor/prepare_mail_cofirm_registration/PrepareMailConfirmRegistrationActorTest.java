package com.perspective.vpng.actor.prepare_mail_cofirm_registration;

import com.perspective.vpng.actor.prepare_mail_cofirm_registration.wrapper.PrepareMailMessage;
import info.smart_tools.smartactors.iobject.ds_object.DSObject;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.*;


@PrepareForTest({IOC.class, Keys.class})
@RunWith(PowerMockRunner.class)
public class PrepareMailConfirmRegistrationActorTest {

    private PrepareMailConfirmRegistrationActor actor;

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);

        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sign")).thenReturn(mock(IField.class));
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "subject")).thenReturn(mock(IField.class));
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type")).thenReturn(mock(IField.class));
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "recipient")).thenReturn(mock(IField.class));
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "mime")).thenReturn(mock(IField.class));
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "text")).thenReturn(mock(IField.class));
        when(IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "url")).thenReturn(mock(IField.class));

        actor = new PrepareMailConfirmRegistrationActor(null);
    }

    @Test
    public void Should_CorrectWork() throws Exception {
        mockStatic(IOC.class);
        //IObject attributes = new DSObject("{ \"sign\": \"SmartActors\"," +
        //                                    "\"subject\": \"Подтверждение регистрации\" }");
        IObject attributes = mock(IObject.class);
        IKey attrKey = mock(IKey.class);
        IObject messagePart = mock(IObject.class);
        when(Keys.getOrAdd(IObject.class.getCanonicalName())).thenReturn(attrKey);
        when(IOC.resolve(attrKey)).thenReturn(attributes).thenReturn(messagePart);

        PrepareMailMessage message = mock(PrepareMailMessage.class);
        when(message.getEmail()).thenReturn("test_mail@testdot.com");
        when(message.getToken()).thenReturn("23f0230f91h2");
        //when(message.getUrl()).thenReturn("test_url");

        actor.prepare(message);

        verify(message).getEmail();
        verify(message).getToken();
        //verify(message).getUrl();
        //verify(message).setMessageAttributes(attributes);
        verify(message).setRecipients(anyList());
        verify(message).setMessageParts(anyList());
    }

    @Test(expected = TaskExecutionException.class)
    public void Should_ThrowException_When_EmailIsNull() throws TaskExecutionException, ReadValueException {
        PrepareMailMessage message = mock(PrepareMailMessage.class);
        when(message.getEmail()).thenReturn(null);
        when(message.getToken()).thenReturn("23f0230f91h2");
        //when(message.getUrl()).thenReturn("test_url");

        actor.prepare(message);
    }

    @Test(expected = TaskExecutionException.class)
    public void Should_ThrowException_When_EmailIsEmpty() throws TaskExecutionException, ReadValueException {
        PrepareMailMessage message = mock(PrepareMailMessage.class);
        when(message.getEmail()).thenReturn("");
        when(message.getToken()).thenReturn("23f0230f91h2");
        //when(message.getUrl()).thenReturn("test_url");

        actor.prepare(message);
    }
}