package com.perspective.vpng.actor.prepare_mail_cofirm_registration;

import com.perspective.vpng.actor.prepare_mail_cofirm_registration.wrapper.PrepareMailMessage;
import com.perspective.vpng.util.template_engine.ITemplateEngine;
import com.perspective.vpng.util.template_engine.TemplateEngine;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Actor for preparing email for confirm registration
 */
public class PrepareMailConfirmRegistrationActor {
    private ITemplateEngine templateEngine = TemplateEngine.create();
    private static IField signF;
    private static IField subjectF;
    private static IField typeF;
    private static IField mimeF;
    private static IField textF;
    private static IField recipientF;
    private String url;

    private String template =
            "<div data-x-div-type=\"body\">" +
            "    <b>Здравствуйте!</b>" +
            "    <br><p>" +
            "       Пожалуйста, используйте следующую ссылку для продолжения регистрации:" +
            "       <a href=\"${url}\" target=\"_blank\" tabindex=\"-1\" rel=\"external\">${url}</a>" +
            "    </p>" +
            "    <p>Сохраните данное письмо, чтобы иметь возможность воспользоваться ссылкой в любой удобный момент.</p>" +
            "    <p>Если Вы будете вынуждены прервать процесс регистрации на одном из шагов, Вы всегда сможете вернуться к этому шагу, пройдя по данной ссылке.</p>" +
            "    <p>" +
            "    С уважением,<br>" +
            "    команда <a href=\"https://vp.ru\" target=\"_blank\" tabindex=\"-1\" rel=\"external\">ВсеПлатежи</a>" +
            "    </p>" +
            "</div>";

    /**
     * default constructor
     * @param params IObject with params
     */
    public PrepareMailConfirmRegistrationActor(final IObject params) {
        try {

            signF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sign");
            subjectF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "subject");
            typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            mimeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "mime");
            textF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "text");
            recipientF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "recipient");
            IField urlF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "url");
            url = urlF.in(params);
        } catch (ResolutionException | ReadValueException | InvalidArgumentException e) {
            throw new RuntimeException("Failed to create PrepareMailConfirmRegistrationActor", e);
        }
    }

    /**
     * Prepare attributes for sending email
     * @param message the message
     * @throws TaskExecutionException sometimes
     */
    public void prepare(final PrepareMailMessage message) throws TaskExecutionException {
        try {
            String email = message.getEmail();
            if (email == null || email.equals("")) {
                throw new TaskExecutionException("Email for confirm is not found");
            }

            IObject recipientObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            recipientF.out(recipientObject, email);
            typeF.out(recipientObject, "To");

            IObject attributes = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            signF.out(attributes, "SmartActors");
            subjectF.out(attributes, "Подтверждение регистрации");

            Map<String, Object> tagValues = new HashMap<>();
            tagValues.put("${url}", url +"?action=confirmregistration&" + "token=" + message.getToken());
            String mailMeassage = templateEngine.fillTemplate(template, tagValues);

            IObject messagePart = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            textF.out(messagePart, new String(mailMeassage.getBytes(), Charset.forName("ISO-8859-1")));
            mimeF.out(messagePart, "text/html");
            typeF.out(messagePart, "text");

            message.setRecipients(Collections.singletonList(recipientObject));
            message.setMessageAttributes(attributes);
            message.setMessageParts(Collections.singletonList(messagePart));
        } catch (Exception e) {
            throw new TaskExecutionException("Failed to prepare email for confirm registration", e);
        }
    }
}
