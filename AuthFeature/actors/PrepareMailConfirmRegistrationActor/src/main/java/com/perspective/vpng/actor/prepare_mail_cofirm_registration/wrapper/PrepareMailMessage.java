package com.perspective.vpng.actor.prepare_mail_cofirm_registration.wrapper;

import com.perspective.vpng.actor.prepare_mail_cofirm_registration.PrepareMailConfirmRegistrationActor;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for message for {@link PrepareMailConfirmRegistrationActor}
 */
public interface PrepareMailMessage {
    /**
     * Getter for email
     * @return email
     * @throws ReadValueException if any error during get is occurred
     */
    String getEmail() throws ReadValueException;

    /**
     * Getter for token
     * @return token
     * @throws ReadValueException if any error during get is occurred
     */
    String getToken() throws ReadValueException;

    /**
     * Getter for url
     * @return url
     * @throws ReadValueException if any error during get is occurred
     */
    //String getUrl() throws ReadValueException;

    /**
     * Setter for email parts
     * @param parts list of IObjects
     * @throws ChangeValueException if any error during set is occurred
     */
    void setMessageParts(List<IObject> parts) throws ChangeValueException;

    /**
     * Setter for email parts
     * @param attr IObject
     * @throws ChangeValueException if any error during set is occurred
     */
    void setMessageAttributes(IObject attr) throws ChangeValueException;

    /**
     * Setter for recipients
     * @param recipients list of emails
     * @throws ChangeValueException if any error during set is occurred
     */
    void setRecipients(List<IObject> recipients) throws ChangeValueException;
}
