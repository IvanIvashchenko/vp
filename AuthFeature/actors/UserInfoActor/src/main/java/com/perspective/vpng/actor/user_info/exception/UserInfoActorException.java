package com.perspective.vpng.actor.user_info.exception;

/**
 * Exception for error in {@link com.perspective.vpng.actor.user_info.UserInfoActor} methods
 */
public class UserInfoActorException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public UserInfoActorException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause specific cause
     */
    public UserInfoActorException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public UserInfoActorException(final Throwable cause) {
        super(cause);
    }
}