package com.perspective.vpng.actor.user_info;

import com.perspective.vpng.actor.user_info.exception.UserInfoActorException;
import com.perspective.vpng.actor.user_info.wrapper.GetUserInfoMessage;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class UserInfoActor {
    private String USER_COLLECTION = "user_account";
    private String SHOPPING_CART_COLLECTION = "shopping_cart";
    private Integer NONCLIENT_MESSAGE_COUNT = -1;
    private Integer NONCLIENT_RESPONSE_TYPE = -1;
    private Integer CLIENT_RESPONSE_TYPE = 1;
    private IPool connectionPool;

    private IField collectionNameF;
    private IField pageSizeF;
    private IField pageNumberF;
    private IField pageF;
    private IField filterF;

    private IField userIdF;
    private IField userNumberF;
    private IField emailF;
    private IField equalsF;

    private IField nameF;
    private IField firstNameF;
    private IField messagesCountF;
    private IField regionF;
    private IField valueF;
    private IField invoicesF;

    public UserInfoActor() throws UserInfoActorException {
        try {
            Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);

            collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            pageSizeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "size");
            pageNumberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "number");
            pageF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "page");
            filterF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "filter");

            userIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userId");
            userNumberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userNumber");
            emailF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "email");
            equalsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "$eq");

            firstNameF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/фио/имя");
            messagesCountF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "messagesCount");
            nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
            valueF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "value");
            regionF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "клиент/адрес/область");
            invoicesF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "invoices");
        } catch (Exception e) {
            throw new UserInfoActorException("Can't create UserInfoActor", e);
        }
    }

    public void getInfo(GetUserInfoMessage message) throws UserInfoActorException {
        try {
            String userId = message.getUserId();

            if (userId == null) {
                message.setUsername("nonclient");
                message.setMessagesCount(NONCLIENT_MESSAGE_COUNT);
                message.setType(NONCLIENT_RESPONSE_TYPE);
            } else {
                IObject user = resolveLogin(message);

                message.setUsername(emailF.in(user));
                message.setMessagesCount(messagesCountF.in(user));
                message.setType(CLIENT_RESPONSE_TYPE);
                message.setName(firstNameF.in(user));
                message.setRegion(regionF.in(user));
                message.setUserId(userNumberF.in(user));
                message.setCartSize(getCartCount(userId));
            }

            List<IObject> headers = message.getHeaders();
            if (headers == null) {
                headers = new ArrayList<>();
            }
            IObject typeHeader = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            nameF.out(typeHeader, "Content-Type");
            valueF.out(typeHeader, "text/javascript;charset=UTF-8");

            headers.add(typeHeader);
            message.setHeaders(headers);
        } catch (ReadValueException | ChangeValueException | InvalidArgumentException | ResolutionException e) {
            throw new UserInfoActorException("Failed to get user info", e);
        }
    }

    private IObject resolveLogin(final GetUserInfoMessage message) throws UserInfoActorException {
        try {
            IObject searchQuery = prepareQueryParams(message);
            List<IObject> items = new LinkedList<>();
            try (IPoolGuard guard = new PoolGuard(connectionPool)) {
                ITask searchTask = IOC.resolve(
                        Keys.getOrAdd("db.collection.search"),
                        guard.getObject(),
                        USER_COLLECTION,
                        searchQuery,
                        (IAction<IObject[]>) foundDocs -> {
                            try {
                                items.addAll(Arrays.asList(foundDocs));
                            } catch (Exception e) {
                                throw new ActionExecuteException(e);
                            }
                        }
                );
                searchTask.execute();
            } catch (PoolGuardException e) {
                throw new RuntimeException(e);
            }
            return items.get(0);
        } catch (ResolutionException | TaskExecutionException | InvalidArgumentException | ChangeValueException | ReadValueException e) {
            throw new UserInfoActorException("Failed to get user info: can't resolve user login", e);
        }
    }

    private IObject prepareQueryParams(final GetUserInfoMessage message)
            throws ResolutionException, ChangeValueException, InvalidArgumentException, ReadValueException {

        IObject filter = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IObject page = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IObject searchQuery = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        collectionNameF.out(searchQuery, this.USER_COLLECTION);
        pageSizeF.out(page, 1);
        pageNumberF.out(page, 1);
        pageF.out(searchQuery, page);
        filterF.out(searchQuery, filter);

        IObject loginObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        equalsF.out(loginObject, message.getUserId());
        userIdF.out(filter, loginObject);

        return searchQuery;
    }

    private Integer getCartCount(String cartKey) throws UserInfoActorException {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                    Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"filter\": {\"guid\": {\"$eq\": \"" + cartKey + "\"}}, \"collectionName\": \"" + SHOPPING_CART_COLLECTION + "\"}"
            );
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    SHOPPING_CART_COLLECTION,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            searchResult.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();

            if (!searchResult.isEmpty()) {
                IObject shoppingCart = searchResult.get(0);
                List<IObject> cartInvoices = invoicesF.in(shoppingCart);

                if (cartInvoices == null) {
                    return 0;
                }
                return cartInvoices.size();
            }
            return 0;
        } catch (ReadValueException | InvalidArgumentException | TaskExecutionException | ResolutionException | PoolGuardException e) {
            throw new UserInfoActorException("Failed to get user info: can't get user shopping cart count", e);
        }
    }
}
