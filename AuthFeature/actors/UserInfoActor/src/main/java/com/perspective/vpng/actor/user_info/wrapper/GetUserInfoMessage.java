package com.perspective.vpng.actor.user_info.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface GetUserInfoMessage {
    String getUserId() throws ReadValueException;

    void setObjResponse(IObject obj) throws ChangeValueException;
    void setMessagesCount(Integer count) throws ChangeValueException;
    void setUsername(String name) throws ChangeValueException;
    void setType(Integer type) throws ChangeValueException;
    void setRegion(String regionCode) throws ChangeValueException;
    void setUserId(Integer id) throws ChangeValueException;
    void setName(String name) throws ChangeValueException;
    void setCartSize(Integer size) throws ChangeValueException;

    /**
     * Get list of headers to context
     * @throws ReadValueException when something happens
     */
    List<IObject> getHeaders() throws ReadValueException;
    void setHeaders(List<IObject> headers) throws ChangeValueException;

}
