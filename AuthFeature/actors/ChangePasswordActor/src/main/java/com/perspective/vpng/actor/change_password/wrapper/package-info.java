/**
 * Package contains wrappers for {@link com.perspective.vpng.actor.change_password.ChangePasswordActor}
 */
package com.perspective.vpng.actor.change_password.wrapper;