package com.perspective.vpng.actor.get_user;

import com.perspective.vpng.actor.get_user.wrapper.GetUserMessage;
import com.perspective.vpng.actor.get_user.wrapper.GetUserSchemaMessage;
import com.perspective.vpng.actor.get_user.wrapper.SaveUserMessage;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class GetUserActor {
    private String userCollection;
    private IPool pool;

    public GetUserActor(IObject params) {
        try {
            Object options = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.pool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), options);

            IField collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            this.userCollection = collectionNameF.in(params);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void getUser(GetUserMessage message) {
        try {
            String userId = message.getUserId();
            IObject user = getUserFromDB(userId);
            message.setUser(user);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void getUserSchema(GetUserSchemaMessage message) {
        try {
            IObject user = getUserFromDB(message.getUserId());
            List<IObject> schema = message.getSchema();
            message.setSchema(IOC.resolve(Keys.getOrAdd("InnerSchema"), user, schema));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveUser(SaveUserMessage message) throws Exception {
        IObject user = message.getUser();
        saveUserInDB(user);
    }

    private IObject getUserFromDB(String userId) throws ResolutionException {
        final List<IObject> items = new LinkedList<>();
        try (IPoolGuard poolGuard = new PoolGuard(pool)) {
            ITask task = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    userCollection,
                    IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
                            String.format(
                                    "{ " +
                                            "\"filter\": { \"userId\": { \"$eq\": \"%s\" } }" +
                                            "}",
                                    userId)
                    ),
                    (IAction<IObject[]>) docs ->
                            items.addAll(Arrays.asList(docs))

            );

            task.execute();
        } catch (PoolGuardException | TaskExecutionException e) {
            throw new RuntimeException("Can't get connection from pool.", e);
        }
        return items.get(0);
    }

    private void saveUserInDB(IObject user) {
        try (IPoolGuard poolGuard = new PoolGuard(pool)) {
            ITask upsertTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.upsert"),
                    poolGuard.getObject(),
                    userCollection,
                    user
            );
            upsertTask.execute();
        } catch (PoolGuardException | ResolutionException | TaskExecutionException e) {
            throw new RuntimeException("Can't execute database operation.", e);
        }
    }
}
