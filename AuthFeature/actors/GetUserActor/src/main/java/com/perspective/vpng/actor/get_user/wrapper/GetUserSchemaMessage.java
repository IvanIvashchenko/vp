package com.perspective.vpng.actor.get_user.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface GetUserSchemaMessage {
    String getUserId() throws ReadValueException;
    List<IObject> getSchema() throws ReadValueException;
    void setSchema(List<IObject> schema) throws ChangeValueException;
}
