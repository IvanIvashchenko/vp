package com.perspective.vpng.actor.get_user.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface GetUserMessage {
    String getUserId() throws ReadValueException;
    void setUser(IObject user) throws ChangeValueException;
}
