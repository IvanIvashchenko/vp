package com.perspective.vpng.actor.user_history;

import com.perspective.vpng.actor.user_history.exception.UserHistoryException;
import com.perspective.vpng.actor.user_history.wrapper.IGetHistoryMessage;
import com.perspective.vpng.actor.user_history.wrapper.RepeatPaymentMessage;
import com.perspective.vpng.actor.user_history.wrapper.SaveToHistoryMessage;
import com.perspective.vpng.actor.user_history.wrapper.UserHistoryParams;
import com.perspective.vpng.plugin.datetime_formatter_strategy.PluginDateTimeFormatter;
import com.perspective.vpng.plugin.db_fields_holder.DatabaseFieldsHolderPlugin;
import com.perspective.vpng.plugin.sequence_holder.SequenceHolderPlugin;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database.database_storage.utils.CollectionName;
import info.smart_tools.smartactors.database_in_memory_plugins.in_memory_database_plugin.PluginInMemoryDatabase;
import info.smart_tools.smartactors.database_in_memory_plugins.in_memory_db_tasks_plugin.PluginInMemoryDBTasks;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.database_postgresql_plugins.cached_collection_plugin.CreateCachedCollectionPlugin;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.field_plugins.nested_field_plugin.NestedFieldPlugin;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

public class UserHistoryActorTest {

    private UserHistoryActor actor;
    private IPool pool;
    private CollectionName collection;

    private IField typeField;
    private IField statusField;
    private IField userGuidField;

    @BeforeClass
    public static void prepareIOC() throws Exception {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new DatabaseFieldsHolderPlugin(bootstrap).load();
        new SequenceHolderPlugin(bootstrap).load();
        new CreateCachedCollectionPlugin(bootstrap).load();
        new PluginDateTimeFormatter(bootstrap).load();
        new NestedFieldPlugin(bootstrap).load();

        new PluginInMemoryDatabase(bootstrap).load();
        new PluginInMemoryDBTasks(bootstrap).load();
        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {

        pool = mock(IPool.class);
        collection = CollectionName.fromString("collection");

        IKey keyConnectionOptions = Keys.getOrAdd("PostgresConnectionOptions");
        IOC.register(keyConnectionOptions, new ApplyFunctionToArgumentsStrategy(
            (args) -> Mockito.mock(ConnectionOptions.class)
        ));
        IKey keyConnectionPool = Keys.getOrAdd("PostgresConnectionPool");
        IOC.register(keyConnectionPool, new ApplyFunctionToArgumentsStrategy(
            (args) -> pool
        ));

        UserHistoryParams params = mock(UserHistoryParams.class);
        when(params.getCollectionName()).thenReturn("collection");
        when(params.getProviderCollectionName()).thenReturn("provider_collection");
        when(params.getConnectionPool()).thenReturn(pool);
        when(params.getProviderIdFieldName()).thenReturn("formKey");
        when(params.getProviderIdSearchQueryFieldName()).thenReturn("formKey");
        when(params.getUserIdFieldName()).thenReturn("userGuid");
        when(params.getStatusFieldName()).thenReturn("status");
        when(params.getTypeFieldName()).thenReturn("type");
        when(params.getDateFieldName()).thenReturn("date");
        when(params.getSumFieldName()).thenReturn("сумма");

        when(params.getGuidFieldName()).thenReturn("guid");
        when(params.getOperationFieldName()).thenReturn("operation");
        when(params.getGroupNameFieldName()).thenReturn("groupName");
        when(params.getNumberFieldName()).thenReturn("number");
        when(params.getPaymentIdFieldName()).thenReturn("paymentId");
        when(params.getDescriptionFieldName()).thenReturn("description");
        when(params.getProviderDescriptionFieldName()).thenReturn("description");
        when(params.getLogoFieldName()).thenReturn("logo");
        when(params.getAmountFieldName()).thenReturn("amount");
        when(params.getEnabledProviderFieldName()).thenReturn("enabledProvider");
        when(params.getEnabledFieldName()).thenReturn("enabled");
        when(params.getHumanDateFieldName()).thenReturn("humanDate");

        IObject createOptions = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        try (PoolGuard guard = new PoolGuard(pool)) {
            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.create"),
                guard.getObject(),
                collection,
                createOptions
            );
            task.execute();
        }

        actor = new UserHistoryActor(params);

        typeField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
        statusField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
        userGuidField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userGuid");

    }

    @Test
    public void ShouldGetHistory() throws Exception {

        IObject document = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"fieldA\": \"valueA\"," +
                "\"date\": \"2016-10-10\"," +
                "\"сумма\": \"123\"," +
                "\"type\": \"payment\"," +
                "\"status\": \"paid\"," +
                "\"userGuid\": \"111\"," +
                "\"formKey\": \"vozminet\"" +
            "}"
        );

        try (PoolGuard guard = new PoolGuard(pool)) {
            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.insert"),
                guard.getObject(),
                collection,
                document
            );
            task.execute();
        }

        IGetHistoryMessage message = mock(IGetHistoryMessage.class);
        when(message.getPageNumber()).thenReturn(1);
        when(message.getPageSize()).thenReturn(1);
        when(message.getProviderId()).thenReturn("vozminet");
        when(message.getUserId()).thenReturn("111");
        when(message.getType()).thenReturn("payment");
        when(message.getFromDate()).thenReturn("23/10/2015");
        when(message.getToDate()).thenReturn("23/10/2017");
        when(message.getFromAmount()).thenReturn(new BigDecimal(10.0));
        when(message.getToAmount()).thenReturn(new BigDecimal(10000.0));

        actor.getUserHistory(message);

        verify(message).setOperations(anyListOf(IObject.class));
    }

    @Test(expected = UserHistoryException.class)
    public void ShouldThrowException_When_InternalExceptionIsThrown() throws Exception {

        IGetHistoryMessage message = mock(IGetHistoryMessage.class);
        when(message.getProviderId()).thenThrow(new ReadValueException());

        actor.getUserHistory(message);

        fail();
    }

    @Test
    public void ShouldRepeatPayment() throws Exception {

        IObject document = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"date\": \"2016-10-10\"," +
                "\"сумма\": \"123\"," +
                "\"type\": \"payment\"," +
                "\"status\": \"paid\"," +
                "\"userGuid\": \"321-321\"," +
                "\"guid\": \"123-123\"" +
            "}"
        );

        try (PoolGuard guard = new PoolGuard(pool)) {
            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.insert"),
                guard.getObject(),
                collection,
                document
            );
            task.execute();
        }

        RepeatPaymentMessage message = mock(RepeatPaymentMessage.class);
        when(message.getOperationGuid()).thenReturn("123-123");
        when(message.getUserId()).thenReturn("321-321");

        actor.repeatPayment(message);

        verify(message).setForm(any(IObject.class));
    }

    @Test(expected = UserHistoryException.class)
    public void ShouldThrowException_when_NoOneInvoiceHasBeenFound() throws Exception {

        RepeatPaymentMessage message = mock(RepeatPaymentMessage.class);
        when(message.getOperationGuid()).thenReturn("123-123");

        actor.repeatPayment(message);

        fail();
    }

    @Test
    public void ShouldSavePaidPaymentToHistory() throws Exception {

        prepareToUseSequence();

        SaveToHistoryMessage message = mock(SaveToHistoryMessage.class);
        IObject payment = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()), "{\"userGuid\": \"1234345\"}");
        List<IObject> invoices = new ArrayList<>();
        IObject invoice1 = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"field1\": \"value1\"," +
                "\"date\": \"2016-10-10\"," +
                "\"сумма\": \"123\"," +
                "\"formKey\": \"vozminet\"" +
            "}"
        );
        invoices.add(invoice1);
        when(message.getPayment()).thenReturn(payment);
        when(message.getInvoices()).thenReturn(invoices);
        when(message.getStatus()).thenReturn("PAID");

        actor.savePaymentToHistory(message);

        assertTrue(((String)typeField.in(invoice1)).equalsIgnoreCase("PAYMENT"));
        assertEquals(statusField.in(invoice1), "PAID");
        assertEquals(userGuidField.in(invoice1), "1234345");
    }

    @Test
    public void ShouldSaveMetersToHistory() throws Exception {

        prepareToUseSequence();

        SaveToHistoryMessage message = mock(SaveToHistoryMessage.class);
        IObject meters = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"field1\": \"value1\"," +
                "\"date\": \"2016-10-10\"," +
                "\"сумма\": \"123\"," +
                "\"userGuid\": \"1234345\"," +
                "\"formKey\": \"vozminet\"" +
            "}"
        );
        when(message.getMeters()).thenReturn(meters);
        when(message.getStatus()).thenReturn("PLANNED");

        actor.saveMetersToHistory(message);
        assertEquals(statusField.in(meters), "PLANNED");
    }

    private void prepareToUseSequence() throws Exception {

        IObject createOptions = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        try (PoolGuard guard = new PoolGuard(pool)) {
            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.create"),
                guard.getObject(),
                CollectionName.fromString("sequence"),
                createOptions
            );
            task.execute();
        }
    }
}
