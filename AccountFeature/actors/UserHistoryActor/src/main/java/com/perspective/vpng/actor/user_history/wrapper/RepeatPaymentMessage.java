package com.perspective.vpng.actor.user_history.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface RepeatPaymentMessage {

    /**
     * @return payment guid
     * @throws ReadValueException if any error is occurred
     */
    String getOperationGuid() throws ReadValueException;

    /**
     * @return identifier of authorized user
     * @throws ReadValueException if any error is occurred
     */
    String getUserId() throws ReadValueException;

    /**
     * Sets form with payment for repeat
     * @throws ChangeValueException if any error is occurred
     */
    void setForm(IObject form) throws ChangeValueException;
}
