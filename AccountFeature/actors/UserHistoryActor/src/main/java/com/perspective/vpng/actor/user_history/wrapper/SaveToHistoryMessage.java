package com.perspective.vpng.actor.user_history.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

/**
 * Wrapper for {@link com.perspective.vpng.actor.user_history.UserHistoryActor} save handlers
 */
public interface SaveToHistoryMessage {

    /**
     * @return invoices from payment
     * @throws ReadValueException if any errors are occurred.
     */
    List<IObject> getInvoices() throws ReadValueException;

    /**
     * @return payment
     * @throws ReadValueException if any errors are occurred.
     */
    IObject getPayment() throws ReadValueException;

    /**
     * @return status of operation
     * @throws ReadValueException if any errors are occurred.
     */
    String getStatus() throws ReadValueException;

    /**
     * @return meters object
     * @throws ReadValueException if any errors are occurred.
     */
    IObject getMeters() throws ReadValueException;
}
