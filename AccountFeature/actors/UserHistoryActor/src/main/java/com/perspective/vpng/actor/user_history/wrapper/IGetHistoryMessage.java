package com.perspective.vpng.actor.user_history.wrapper;

import com.perspective.vpng.actor.user_history.UserHistoryActor;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.math.BigDecimal;
import java.util.List;

/**
 * Wrapper for message for {@link UserHistoryActor}
 */
public interface IGetHistoryMessage {

    /**
     * Gets a payments page number.
     * @return a number of payments page.
     * @throws ReadValueException when errors in reading page number from message.
     */
    Integer getPageNumber() throws ReadValueException;

    /**
     * Sets a payments page number.
     * @param pageNumber a number of payments page.
     * @throws ChangeValueException when errors in writing page number to message.
     */
    void setPageNumber(Integer pageNumber) throws ChangeValueException;

    /**
     * Gets a size of page.
     * @return a size of invoice page.
     * @throws ReadValueException when errors in reading page size from message.
     */
    Integer getPageSize() throws ReadValueException;

    /**
     * Gets start date for filter
     * @return start date for filter
     * @throws ReadValueException when errors in reading start date from message.
     */
    String getFromDate() throws ReadValueException;

    /**
     * Gets end date for filter
     * @return end date for filter
     * @throws ReadValueException when errors in reading end date from message.
     */
    String getToDate() throws ReadValueException;

    /**
     * Gets min sum for filter
     * @return min sum
     * @throws ReadValueException when errors in reading from message.
     */
    BigDecimal getFromAmount() throws ReadValueException;

    /**
     * Gets max sum for filter
     * @return max sum
     * @throws ReadValueException when errors in reading from message.
     */
    BigDecimal getToAmount() throws ReadValueException;

    /**
     * Gets current user identifier
     * @return user id
     * @throws ReadValueException when errors in reading from message.
     */
    String getUserId() throws ReadValueException;

    /**
     * Gets provider identifier
     * @return provider id
     * @throws ReadValueException when errors in reading from message.
     */
    String getProviderId() throws ReadValueException;

    /**
     * Gets operation type
     * @return operation type
     * @throws ReadValueException when errors in reading from message.
     */
    String getType() throws ReadValueException;

    /**
     * Adds user operations to message.
     * @param operations - added operations.
     * @throws ChangeValueException  if any errors are occurred.
     */
    void setOperations(List<IObject> operations) throws ChangeValueException;

    /**
     * Sets status
     * @param status - status
     * @throws ChangeValueException  if any errors are occurred.
     */
    void setStatus(String status) throws ChangeValueException;

    /**
     * Set true if repeater should repeat chain at current level of sequence.
     * @param repeat is true if repeater should repeat a chain
     * @throws ChangeValueException if any error occurs
     */
    void setRepeat(boolean repeat) throws ChangeValueException;
}
