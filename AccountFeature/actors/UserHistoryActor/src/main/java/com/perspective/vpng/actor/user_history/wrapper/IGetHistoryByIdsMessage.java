package com.perspective.vpng.actor.user_history.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface IGetHistoryByIdsMessage {
    List<String> getIds() throws ReadValueException;
    String getUserId() throws ReadValueException;
    void setInvoices(List<IObject> invoices) throws ChangeValueException;
}
