package com.perspective.vpng.actor.user_history.exception;

public class UserHistoryException extends Exception {

    /**
     * Constructor with specific error message as argument
     * @param message specific error message
     */
    public UserHistoryException(final String message) {
        super(message);
    }

    /**
     * Constructor with specific error message and specific cause as arguments
     * @param message specific error message
     * @param cause   specific cause
     */
    public UserHistoryException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with specific cause as argument
     * @param cause specific cause
     */
    public UserHistoryException(final Throwable cause) {
        super(cause);
    }
}
