package com.perspective.vpng.actor.user_history;

import com.perspective.vpng.actor.user_history.exception.UserHistoryException;
import com.perspective.vpng.actor.user_history.wrapper.*;
import com.perspective.vpng.database.field.IConditionField;
import com.perspective.vpng.database.field.IDBFieldsHolder;
import com.perspective.vpng.database.field.ISearchQueryField;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database.cached_collection.exception.GetCacheItemException;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.ifield_name.IFieldName;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.DeleteValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Actor for manipulations with user payments and meters data history
 */
public class UserHistoryActor {

    private static final String PAYMENT_TYPE = "PAYMENT";
    private static final String METERS_DATA_TYPE = "READING";

    private static final String ALL_PROVIDERS = "-1";


    private Map<String, List<IObject>> allUserHistoryItemsMap;
    private Integer startPageNumber = 1;
    private Integer pageSize = 100;

    private String collectionName;
    private IPool connectionPool;
    private ICachedCollection providerCollection;
    private DateTimeFormatter standardFormatter;

    private IField collectionNameF;

    private final ISearchQueryField searchQueryField;
    private final IConditionField conditionField;

    private final IField providerIdF;
    private final IField providerIdSearchQueryF;
    private final IField userGuidF;
    private final IField statusF;
    private final IField typeF;
    private final IField dateF;
    private final IField sumF;
    private final IField guidF;

    //fields for rendering
    private final IField timestampF;
    private final IField operationF;
    private final IField groupNameF;
    private final IField numberF;
    private final IField paymentIdF;
    private final IField descriptionF;
    private final IField providerDescriptionF;
    private final IField logoF;
    private final IField amountF;
    private final IField enabledProviderF;
    private final IField enabledF;
    private final IField humanDateF;

    /**
     * Constructor
     * @param params for initialize actor
     * @throws UserHistoryException if any error during creation is occurred
     */
    public UserHistoryActor(final UserHistoryParams params) throws UserHistoryException {

        try {
            this.allUserHistoryItemsMap = new HashMap<>();

            this.collectionName = params.getCollectionName();
            this.connectionPool = params.getConnectionPool();
            this.collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            this.providerCollection = IOC.resolve(
                    Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), params.getProviderCollectionName(), "name"
            );
            this.standardFormatter =  IOC.resolve(Keys.getOrAdd("datetime_formatter"));

            this.searchQueryField =((IDBFieldsHolder) IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()))).searchQuery();
            this.conditionField = ((IDBFieldsHolder) IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()))).conditions();

            this.providerIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), params.getProviderIdFieldName());
            this.providerIdSearchQueryF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getProviderIdSearchQueryFieldName());
            this.userGuidF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getUserIdFieldName());
            this.statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getStatusFieldName());
            this.typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getTypeFieldName());
            this.dateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getDateFieldName());
            this.sumF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getSumFieldName());

            this.guidF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getGuidFieldName());
            this.timestampF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getDateFieldName());
            this.operationF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getOperationFieldName());
            this.groupNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getGroupNameFieldName());
            this.numberF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getNumberFieldName());
            this.paymentIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getPaymentIdFieldName());
            this.descriptionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getDescriptionFieldName());
            this.providerDescriptionF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getProviderDescriptionFieldName());

            this.logoF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getLogoFieldName());
            this.amountF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getAmountFieldName());
            this.enabledProviderF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getEnabledProviderFieldName());
            this.enabledF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getEnabledFieldName());
            this.humanDateF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), params.getHumanDateFieldName());
        } catch (ResolutionException | ReadValueException e) {
            throw new UserHistoryException("Can't create UserHistoryActor", e);
        }
    }

    /**
     * Gets list of payments for current user with some offset and pay_limit.
     * @param message with query settings
     * @throws UserHistoryException if any error is occurred
     */
    public void getUserHistory(final IGetHistoryMessage message) throws UserHistoryException {

        List<IObject> operations = new LinkedList<>();
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            IObject searchQuery = prepareQueryParams(message);
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    collectionName,
                    searchQuery,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            operations.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();
        } catch (ResolutionException | ChangeValueException | ReadValueException | InvalidArgumentException | PoolGuardException |
                TaskExecutionException e) {
            throw new UserHistoryException("Can't get user operations history", e);
        } catch (Exception e) {
            throw new UserHistoryException(e.getMessage(), e);
        }

        try {
            IObject historyItem;
            List<IObject> historyItems = new ArrayList<>();

            DateTimeFormatter humanDateFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy HH:mm");
            LocalDateTime timestampDateTime;
            for (IObject operation : operations) {
                historyItem = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

                timestampDateTime = LocalDateTime.parse(dateF.in(operation), standardFormatter);
                timestampF.out(historyItem, timestampDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
                operationF.out(historyItem, typeF.in(operation));
                statusF.out(historyItem, statusF.in(operation));
                groupNameF.out(historyItem, IOC.resolve(Keys.getOrAdd("determinationGroupNameStrategy"), typeF.in(operation), statusF.in(operation)));
                numberF.out(historyItem, numberF.in(operation));
                paymentIdF.out(historyItem, guidF.in(operation));
                IObject provider = providerCollection.getItems(providerIdF.in(operation)).get(0);
                descriptionF.out(historyItem, providerDescriptionF.in(provider));
                logoF.out(historyItem, logoF.in(provider));
                enabledProviderF.out(historyItem, enabledF.in(provider));
                amountF.out(historyItem, sumF.in(operation));
                humanDateF.out(historyItem, timestampDateTime.format(humanDateFormatter));

                historyItems.add(historyItem);
            }

            String key = getKeyForHistoryMap(message);
            List<IObject> tmpHistoryItems = allUserHistoryItemsMap.get(key) == null ? new ArrayList<>() : allUserHistoryItemsMap.get(key);
            tmpHistoryItems.addAll(historyItems);
            if (operations.size() < pageSize) {
                message.setRepeat(false);

                message.setOperations(tmpHistoryItems);
                message.setStatus("SUCCESS");

                allUserHistoryItemsMap.remove(key);
            } else {
                message.setRepeat(true);

                Integer currentPageNumber = message.getPageNumber() == null ? startPageNumber : message.getPageNumber();
                message.setPageNumber(currentPageNumber + 1);

                allUserHistoryItemsMap.put(key, tmpHistoryItems);
            }
        } catch (ResolutionException | ChangeValueException | ReadValueException | InvalidArgumentException | GetCacheItemException e) {
            throw new UserHistoryException("Can't get user operations history", e);
        } catch (Exception e) {
            throw new UserHistoryException(e.getMessage(), e);
        }
    }

    public void repeatPayment(final RepeatPaymentMessage message) throws UserHistoryException {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            String paymentGuid = message.getOperationGuid();
            IObject searchQuery = prepareSearchQueryForRepeatPayment(paymentGuid, message.getUserId());
            List<IObject> operations = new LinkedList<>();
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    collectionName,
                    searchQuery,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            operations.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();
            if (operations.size() != 1) {
                throw new UserHistoryException(
                        "One operation should be found by guid: " + paymentGuid + ", but has been found " + operations.size()
                );
            }
            IObject invoice = operations.get(0);
            guidF.out(invoice, IOC.resolve(Keys.getOrAdd("db.collection.nextid")));
            dateF.out(invoice, LocalDateTime.now().format(standardFormatter));
            invoice.deleteField(IOC.resolve(Keys.getOrAdd(IFieldName.class.getCanonicalName()), collectionName + "ID"));
            message.setForm(invoice);
        } catch (ReadValueException | PoolGuardException | InvalidArgumentException | ChangeValueException | DeleteValueException | ResolutionException |
                TaskExecutionException e) {
            throw new UserHistoryException("Error during repeating payment.", e);
        }
    }

    public void getHistoryByIds(IGetHistoryByIdsMessage message) throws UserHistoryException {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<String> invoiceGuids = message.getIds();
            List<IObject> result = new ArrayList<>();
            for (String guid : invoiceGuids) {
                IObject searchQuery = prepareSearchQueryForRepeatPayment(guid, message.getUserId());
                List<IObject> operations = new LinkedList<>();
                ITask searchTask = IOC.resolve(
                        Keys.getOrAdd("db.collection.search"),
                        poolGuard.getObject(),
                        collectionName,
                        searchQuery,
                        (IAction<IObject[]>) foundDocs -> {
                            try {
                                operations.addAll(Arrays.asList(foundDocs));
                            } catch (Exception e) {
                                throw new ActionExecuteException(e);
                            }
                        }
                );

                searchTask.execute();
                if (operations.size() != 1) {
                    throw new UserHistoryException(
                            "One operation should be found by guid: " + guid + ", but has been found " + operations.size()
                    );
                }
                result.add(operations.get(0));
            }
            message.setInvoices(result);
        } catch (ReadValueException | PoolGuardException | InvalidArgumentException | ChangeValueException | ResolutionException |
                TaskExecutionException e) {
            throw new UserHistoryException("Error during getting payments.", e);
        }
    }

    public void savePaymentToHistory(final SaveToHistoryMessage message) throws UserHistoryException {

        try {
            List<IObject> invoices = message.getInvoices();
            IObject payment = message.getPayment();
            for (IObject invoice : invoices) {
                try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
                    typeF.out(invoice, PAYMENT_TYPE);
                    statusF.out(invoice, message.getStatus());
                    userGuidF.out(invoice, userGuidF.in(payment));
                    addUserOperationToHistory(invoice, poolGuard);
                } catch (InvalidArgumentException | ResolutionException | ChangeValueException | TaskExecutionException e1) {
                    throw new UserHistoryException("Can't save invoices to user history", e1);
                }
            }
        } catch (ReadValueException | PoolGuardException e) {
            throw new UserHistoryException("Error during read payment objects", e);
        }
    }

    public void saveMetersToHistory(final SaveToHistoryMessage message) throws UserHistoryException {

        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            IObject meters = message.getMeters();
            typeF.out(meters, METERS_DATA_TYPE);
            statusF.out(meters, message.getStatus());
            addUserOperationToHistory(meters, poolGuard);
        } catch (ReadValueException | ChangeValueException | InvalidArgumentException |
            PoolGuardException | ResolutionException | TaskExecutionException e) {
            throw new UserHistoryException("Can't save meters object to user history", e);
        }
    }

    private IObject prepareQueryParams(final IGetHistoryMessage message)
            throws ResolutionException, ChangeValueException, InvalidArgumentException, ReadValueException {

        IKey iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
        IObject page = IOC.resolve(iObjectKey);
        IObject searchQuery = IOC.resolve(iObjectKey);
        IObject searchFilter = IOC.resolve(iObjectKey);
        IObject userIdCriteria = IOC.resolve(iObjectKey);
        IObject datesCriteria = IOC.resolve(iObjectKey);

        collectionNameF.out(searchQuery, this.collectionName);

        String providerGuid = message.getProviderId();
        if (!providerGuid.equalsIgnoreCase(ALL_PROVIDERS)) {
            IObject providerIdCriteria = IOC.resolve(iObjectKey);
            conditionField.eq().out(providerIdCriteria, providerGuid);
            providerIdSearchQueryF.out(searchFilter, providerIdCriteria);
        }
        //comment userId if need test chain
        conditionField.eq().out(userIdCriteria, message.getUserId());
        userGuidF.out(searchFilter, userIdCriteria);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        String dateFrom = message.getFromDate();
        conditionField.dateFrom().out(datesCriteria,
                LocalDateTime.of(LocalDate.from(formatter.parse(dateFrom)), LocalTime.MIDNIGHT).format(standardFormatter)
        );
        String dateTo = message.getToDate();
        conditionField.dateTo().out(datesCriteria,
                LocalDateTime.of(LocalDate.from(formatter.parse(dateTo)), LocalTime.MAX).format(standardFormatter)
        );
        dateF.out(searchFilter, datesCriteria);

        if (message.getType().equalsIgnoreCase(PAYMENT_TYPE)) {
            IObject amountsCriteria = IOC.resolve(iObjectKey);
            boolean needFilter = false;
            try {
                conditionField.gt().out(amountsCriteria, message.getFromAmount());
                needFilter = true;
            } catch (ReadValueException ignored) {}
            try {
                conditionField.lt().out(amountsCriteria, message.getToAmount());
                needFilter = true;
            } catch (ReadValueException ignored) {}
            if (needFilter) {
                sumF.out(searchFilter, amountsCriteria);
            }
        }

        searchQueryField.pageNumber().out(page, message.getPageNumber() == null ? startPageNumber : message.getPageNumber());
        searchQueryField.pageSize().out(page, pageSize);

        searchQueryField.page().out(searchQuery, page);
        searchQueryField.filter().out(searchQuery, searchFilter);

        return searchQuery;
    }

    private IObject prepareSearchQueryForRepeatPayment(final String paymentGuid, final String userId)
            throws ChangeValueException, InvalidArgumentException, ResolutionException, ReadValueException {

        IKey iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
        IObject page = IOC.resolve(iObjectKey);
        IObject searchQuery = IOC.resolve(iObjectKey);
        IObject searchFilter = IOC.resolve(iObjectKey);
        IObject paymentGuidCriteria = IOC.resolve(iObjectKey);
        IObject userIdCriteria = IOC.resolve(iObjectKey);

        collectionNameF.out(searchQuery, collectionName);

        conditionField.eq().out(paymentGuidCriteria, paymentGuid);
        guidF.out(searchFilter, paymentGuidCriteria);

        //comment userId if need test chain
        conditionField.eq().out(userIdCriteria, userId);
        userGuidF.out(searchFilter, userIdCriteria);

        searchQueryField.pageNumber().out(page, 1);
        searchQueryField.pageSize().out(page, 1);

        searchQueryField.page().out(searchQuery, page);
        searchQueryField.filter().out(searchQuery, searchFilter);

        return searchQuery;
    }

    private void addUserOperationToHistory(IObject userOperation, IPoolGuard poolGuard) throws ResolutionException, TaskExecutionException {

        ITask upsertTask = IOC.resolve(
                Keys.getOrAdd("db.collection.insert"),
                poolGuard.getObject(),
                collectionName,
                userOperation
        );
        upsertTask.execute();
    }

    private String getKeyForHistoryMap(final IGetHistoryMessage message) throws ReadValueException, UnsupportedEncodingException, NoSuchAlgorithmException {

        StringBuffer buffer = new StringBuffer()
            .append(message.getFromDate())
            .append(message.getToDate());

        //In case if fromAmount is empty string
        try {
            buffer.append(message.getFromAmount());
        } catch (Exception e) {
            buffer.append(0);
        }

        //In case if toAmpunt is empty string
        try {
            buffer.append(message.getToAmount());
        } catch (Exception e) {
            buffer.append(0);
        }

        buffer
            .append(message.getProviderId())
            .append(message.getType())
            .append(message.getUserId())
        ;

        MessageDigest digest = MessageDigest.getInstance("MD5");
        byte[] hashedBytes = digest.digest(buffer.toString().getBytes("UTF-8"));

        buffer = new StringBuffer();
        for (byte hashedByte : hashedBytes) {
            buffer.append(Integer.toString((hashedByte & 0xff) + 0x100, 16).substring(1));
        }

        return buffer.toString();
    }
}