/**
 * Contains exception for {@link com.perspective.vpng.actor.user_history.UserHistoryActor}
 */
package com.perspective.vpng.actor.user_history.exception;