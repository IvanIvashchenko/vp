package com.perspective.vpng.actor.user_history.wrapper;

import com.perspective.vpng.actor.user_history.UserHistoryActor;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

/**
 * Wrapper for constructor of {@link UserHistoryActor}
 */
public interface UserHistoryParams {

    /**
     * @return collection name string
     * @throws ReadValueException if any error is occurred
     */
    String getCollectionName() throws ReadValueException;

    /**
     * @return provider collection name string
     * @throws ReadValueException if any error is occurred
     */
    String getProviderCollectionName() throws ReadValueException;

    /**
     * @return connection pool
     * @throws ReadValueException if any error is occurred
     */
    IPool getConnectionPool() throws ReadValueException;

    /**
     * @return provider identifier field name
     * @throws ReadValueException if any error is occurred
     */
    String getProviderIdFieldName() throws ReadValueException;

    /**
     * @return provider identifier field name
     * @throws ReadValueException if any error is occurred
     */
    String getProviderIdSearchQueryFieldName() throws ReadValueException;

    /**
     * @return user identifier field name
     * @throws ReadValueException if any error is occurred
     */
    String getUserIdFieldName() throws ReadValueException;


    /**
     * @return status field name
     * @throws ReadValueException if any error is occurred
     */
    String getStatusFieldName() throws ReadValueException;

    /**
     * @return type field name
     * @throws ReadValueException if any error is occurred
     */
    String getTypeFieldName() throws ReadValueException;

    /**
     * @return date field name
     * @throws ReadValueException if any error is occurred
     */
    String getDateFieldName() throws ReadValueException;

    /**
     * @return sum field name
     * @throws ReadValueException if any error is occurred
     */
    String getSumFieldName() throws ReadValueException;

    /**
     * @return guid field name
     * @throws ReadValueException if any error is occurred
     */
    String getGuidFieldName() throws ReadValueException;

    /**
     * @return operation field name
     * @throws ReadValueException if any error is occurred
     */
    String getOperationFieldName() throws ReadValueException;

    /**
     * @return group name field name
     * @throws ReadValueException if any error is occurred
     */
    String getGroupNameFieldName() throws ReadValueException;

    /**
     * @return number field name
     * @throws ReadValueException if any error is occurred
     */
    String getNumberFieldName() throws ReadValueException;

    /**
     * @return payment id field name
     * @throws ReadValueException if any error is occurred
     */
    String getPaymentIdFieldName() throws ReadValueException;

    /**
     * @return description field name
     * @throws ReadValueException if any error is occurred
     */
    String getDescriptionFieldName() throws ReadValueException;

    /**
     * @return provider description field name
     * @throws ReadValueException if any error is occurred
     */
    String getProviderDescriptionFieldName() throws ReadValueException;

    /**
     * @return logo field name
     * @throws ReadValueException if any error is occurred
     */
    String getLogoFieldName() throws ReadValueException;

    /**
     * @return amount field name
     * @throws ReadValueException if any error is occurred
     */
    String getAmountFieldName() throws ReadValueException;

    /**
     * @return enabled provider field name
     * @throws ReadValueException if any error is occurred
     */
    String getEnabledProviderFieldName() throws ReadValueException;

    /**
     * @return enabled field name
     * @throws ReadValueException if any error is occurred
     */
    String getEnabledFieldName() throws ReadValueException;

    /**
     * @return human date field name
     * @throws ReadValueException if any error is occurred
     */
    String getHumanDateFieldName() throws ReadValueException;
}
