package com.perspective.vpng.actor.providers_history;

import com.perspective.vpng.database.field.IDBFieldsHolder;
import com.perspective.vpng.actor.providers_history.exception.GetHistoryProvidersException;
import com.perspective.vpng.actor.providers_history.wrapper.MessageWrapper;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.database.cached_collection.CachedCollection;
import info.smart_tools.smartactors.database.cached_collection.ICachedCollection;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;

import java.util.*;

public class GetHistoryProvidersActor {
    private IPool connectionPool;
    private IDBFieldsHolder fieldsHolder;
    private CachedCollection providerCollection;
    private String collectionName;

    private static IField collectionNameF;
    private static IField userGuidF;
    private static IField formKeyF;
    private static IField typeF;
    private static IField nameF;
    private static IField fullNameF;
    private static IField idF;
    private static IField resultF;
    private static IField messageF;
    private static IField dataF;
    private static IField providersF;
    private static IField meterReadingsF;

    public GetHistoryProvidersActor(IObject params) {
        try {
            this.collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            this.userGuidF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userGuid");
            this.formKeyF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "поставщик/id");
            this.typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            this.nameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "name");
            this.fullNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "короткое-наименование");
            this.idF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "id");

            this.resultF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "result");
            this.messageF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "message");
            this.dataF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "data");
            this.providersF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providers");
            this.meterReadingsF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "meterReadings");

            ConnectionOptions connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
            this.fieldsHolder = IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()));
            this.collectionName = collectionNameF.in(params);
            this.providerCollection = IOC.resolve(Keys.getOrAdd(ICachedCollection.class.getCanonicalName()), "provider", "name");
        } catch (Exception e) {
            throw new RuntimeException("Failed to create GetHistoryProvidersActor", e);
        }
    }

    public void getProviders(MessageWrapper message) throws GetHistoryProvidersException {
        try {
            IObject resultObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
            IObject searchQuery = prepareQueryParams(message.getUserGuid());
            List<IObject> history = getHistory(searchQuery);

            Map<String, IObject> providersMap = new LinkedHashMap<>();
            Map<String, IObject> metersMap = new LinkedHashMap<>();

            for (IObject invoice : history) {
                String name = formKeyF.in(invoice);
                IObject provider = providerCollection.getItems(name).get(0);
                IObject providerInfoObj = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

                nameF.out(providerInfoObj, fullNameF.in(provider));
                idF.out(providerInfoObj, name);

                if (((String)typeF.in(invoice)).equalsIgnoreCase("PAYMENT")) {
                    providersMap.put(name, providerInfoObj);
                }

                if (((String)typeF.in(invoice)).equalsIgnoreCase("READING")) {
                    metersMap.put(name, providerInfoObj);
                }

                message.setResult("SUCCESS");
                message.setMessage("");
                IObject data = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                providersF.out(data, providersMap.values());
                meterReadingsF.out(data, metersMap.values());
                message.setData(data);
            }
        } catch (Exception e) {
            throw new GetHistoryProvidersException(e);
        }
    }

    private List<IObject> getHistory(final IObject query) throws GetHistoryProvidersException {
        List<IObject> operations = new LinkedList<>();

        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    collectionName,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            operations.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );
            searchTask.execute();

            return operations;
        } catch(Exception e) {
            throw new GetHistoryProvidersException("Failed to get providers from history", e);
        }
    }

    private IObject prepareQueryParams(final String userGuid)
            throws ResolutionException, ChangeValueException, InvalidArgumentException, ReadValueException {

        IObject searchQuery = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IObject filter = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IObject userIdObject = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        IObject page = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));

        fieldsHolder.conditions().eq().out(userIdObject, userGuid);
        userGuidF.out(filter, userIdObject);

        collectionNameF.out(searchQuery, this.collectionName);
        //TODO: change to normal paging
        fieldsHolder.searchQuery().pageSize().out(page, 100);
        fieldsHolder.searchQuery().pageNumber().out(page, 1);
        fieldsHolder.searchQuery().page().out(searchQuery, page);

        fieldsHolder.searchQuery().filter().out(searchQuery, filter);

        return searchQuery;
    }
}
