package com.perspective.vpng.actor.providers_history.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

public interface MessageWrapper {
    String getUserGuid() throws ReadValueException;
    void setResult(String result) throws ChangeValueException;
    void setMessage(String message) throws ChangeValueException;
    void setData(IObject data) throws ChangeValueException;
}
