package com.perspective.vpng.actor.get_sms_reminds;

import com.perspective.vpng.actor.get_sms_reminds.wrapper.GetSmsRemindsMessage;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.pool_guard.IPoolGuard;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.field.nested_field.NestedField;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class GetSmsRemindsActor {
    private IPool connectionPool;
    private String collectionName;
    private IField providerIdF;
    private IField idF;
    private IField entryIdF;
    private IField strategyF;
    private IField typeF;
    private IField startF;
    private IField timeF;

    public GetSmsRemindsActor(IObject params) {
        try {
            IField collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            this.collectionName = collectionNameF.in(params);
            ConnectionOptions connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
            this.connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);

            this.idF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "id");
            this.entryIdF = IOC.resolve(Keys.getOrAdd(NestedField.class.getCanonicalName()), "entryId");
            this.providerIdF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerId");
            this.strategyF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "strategy");
            this.typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            this.startF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "start");
            this.timeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "time");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void getReminds(GetSmsRemindsMessage message) {
        try {
            String userId = message.getUserId();
            List<IObject> tasks = getRemindsFromDB(userId);

            List<IObject> result = new ArrayList<>();
            for(IObject task : tasks) {
                IObject remind = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
                providerIdF.out(remind, providerIdF.in(task));
                idF.out(remind, entryIdF.in(task));

                if (strategyF.in(task).equals("repeat continuously scheduling strategy")) {
                    LocalDateTime ldt = LocalDateTime.parse(startF.in(task));
                    typeF.out(remind, "Каждое " + ldt.getDayOfMonth() + " число месяца");
                }

                if (strategyF.in(task).equals("do once scheduling strategy")) {
                    LocalDateTime ldt = LocalDateTime.parse(timeF.in(task));
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    typeF.out(remind, ldt.format(formatter));
                }

                result.add(remind);
            }

            message.setResult(result);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get sms reminds", e);
        }
    }

    private List<IObject> getRemindsFromDB(String userId) {
        try (IPoolGuard poolGuard = new PoolGuard(connectionPool)) {
            List<IObject> searchResult = new LinkedList<>();
            IObject query = IOC.resolve(
                    Keys.getOrAdd(IObject.class.getCanonicalName()),
                    "{\"filter\": {\"userId\": {\"$eq\": \"" + userId + "\"}}, \"collectionName\": \"" + collectionName + "\"}"
            );

            ITask searchTask = IOC.resolve(
                    Keys.getOrAdd("db.collection.search"),
                    poolGuard.getObject(),
                    collectionName,
                    query,
                    (IAction<IObject[]>) foundDocs -> {
                        try {
                            searchResult.addAll(Arrays.asList(foundDocs));
                        } catch (Exception e) {
                            throw new ActionExecuteException(e);
                        }
                    }
            );

            searchTask.execute();
            return searchResult;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
