package com.perspective.vpng.actor.get_sms_reminds.wrapper;

import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;

import java.util.List;

public interface GetSmsRemindsMessage {
    String getUserId() throws ReadValueException;
    void setResult(List<IObject> reminds) throws ChangeValueException;
}
