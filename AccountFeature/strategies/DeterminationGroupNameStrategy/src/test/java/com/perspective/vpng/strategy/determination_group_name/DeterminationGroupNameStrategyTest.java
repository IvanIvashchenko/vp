package com.perspective.vpng.strategy.determination_group_name;

import com.perspective.vpng.plugin.db_fields_holder.DatabaseFieldsHolderPlugin;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.database_in_memory_plugins.in_memory_database_plugin.PluginInMemoryDatabase;
import info.smart_tools.smartactors.database_in_memory_plugins.in_memory_db_tasks_plugin.PluginInMemoryDBTasks;
import info.smart_tools.smartactors.feature_loading_system.bootstrap.Bootstrap;
import info.smart_tools.smartactors.field_plugins.ifield_plugin.IFieldPlugin;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject_plugins.dsobject_plugin.PluginDSObject;
import info.smart_tools.smartactors.iobject_plugins.ifieldname_plugin.IFieldNamePlugin;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.ioc_plugins.ioc_keys_plugin.PluginIOCKeys;
import info.smart_tools.smartactors.ioc_plugins.ioc_simple_container_plugin.PluginIOCSimpleContainer;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.powermock.api.mockito.PowerMockito.mock;

public class DeterminationGroupNameStrategyTest {

    private IResolveDependencyStrategy strategy;
    private IPool pool;
    private String collection;

    @BeforeClass
    public static void prepareIOC() throws Exception {

        Bootstrap bootstrap = new Bootstrap();
        new PluginIOCSimpleContainer(bootstrap).load();
        new PluginIOCKeys(bootstrap).load();
        new IFieldNamePlugin(bootstrap).load();
        new IFieldPlugin(bootstrap).load();
        new PluginDSObject(bootstrap).load();
        new DatabaseFieldsHolderPlugin(bootstrap).load();

        new PluginInMemoryDatabase(bootstrap).load();
        new PluginInMemoryDBTasks(bootstrap).load();
        bootstrap.start();
    }

    @Before
    public void setUp() throws Exception {

        pool = mock(IPool.class);
        collection = "collection";

        IObject createOptions = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()));
        try (PoolGuard guard = new PoolGuard(pool)) {
            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.create"),
                guard.getObject(),
                collection,
                createOptions
            );
            task.execute();
        }

        strategy = new DeterminationGroupNameStrategy(collection, pool);
    }

    @Test
    public void shouldFindGroupName() throws Exception {

        IObject document = IOC.resolve(Keys.getOrAdd(IObject.class.getCanonicalName()),
            "{" +
                "\"groupName\": \"Оплачено\"," +
                "\"date\": \"2016-10-10\"," +
                "\"сумма\": \"123\"," +
                "\"type\": \"payment\"," +
                "\"status\": \"paid\"," +
                "\"formKey\": \"vozminet\"" +
            "}"
        );

        try (PoolGuard guard = new PoolGuard(pool)) {
            ITask task = IOC.resolve(
                Keys.getOrAdd("db.collection.insert"),
                guard.getObject(),
                collection,
                document
            );
            task.execute();
        }

        String groupName = strategy.resolve("payment", "paid");
        assertEquals(groupName, "Оплачено");
    }

    @Test(expected = ResolveDependencyStrategyException.class)
    public void shouldThrowException_When_InternalExceptionIsThrown() throws Exception {

        strategy.resolve(null, null);
        fail();
    }
}
