package com.perspective.vpng.strategy.determination_group_name;

import com.perspective.vpng.database.field.IConditionField;
import com.perspective.vpng.database.field.IDBFieldsHolder;
import com.perspective.vpng.database.field.ISearchQueryField;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IAction;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.IResolveDependencyStrategy;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.base.pool_guard.PoolGuard;
import info.smart_tools.smartactors.base.pool_guard.exception.PoolGuardException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ChangeValueException;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.task.interfaces.itask.ITask;
import info.smart_tools.smartactors.task.interfaces.itask.exception.TaskExecutionException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Strategy for resolve groupName of user history item by type and status of operation
 */
public class DeterminationGroupNameStrategy implements IResolveDependencyStrategy {

    private final ISearchQueryField searchQueryField;
    private final IConditionField conditionField;

    private IField collectionNameF;
    private final IField statusF;
    private final IField typeF;
    private final IField groupNameF;

    private final String collectionName;
    private final IPool connectionPool;

    public DeterminationGroupNameStrategy(final String collectionName, final IPool connectionPool) throws ResolveDependencyStrategyException {
        this.collectionName = collectionName;
        this.connectionPool = connectionPool;

        try {
            this.searchQueryField =((IDBFieldsHolder) IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()))).searchQuery();
            this.conditionField = ((IDBFieldsHolder) IOC.resolve(Keys.getOrAdd(IDBFieldsHolder.class.getCanonicalName()))).conditions();
            this.collectionNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
            this.statusF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "status");
            this.typeF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "type");
            this.groupNameF = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "groupName");
        } catch (ResolutionException e) {
            throw new ResolveDependencyStrategyException("Can't create DeterminationGroupNameStrategy", e);
        }

    }

    @Override
    public <T> T resolve(final Object... args) throws ResolveDependencyStrategyException {

        List<IObject> result = new ArrayList<>();
        String type = (String) args[0];
        String status = (String) args[1];
        try (PoolGuard guard = new PoolGuard(connectionPool)){
            IObject searchQuery = prepareSearchQuery(type, status);
            ITask searchTask = IOC.resolve(
                Keys.getOrAdd("db.collection.search"),
                guard.getObject(),
                collectionName,
                searchQuery,
                (IAction<IObject[]>) foundDocs -> {
                    try {
                        result.addAll(Arrays.asList(foundDocs));
                    } catch (Exception e) {
                        throw new ActionExecuteException(e);
                    }
                }
            );
            searchTask.execute();

            if (result.isEmpty()) {
                throw new ResolveDependencyStrategyException("No group name has been found by type " + type + "and status " + status);
            }

            return (T) groupNameF.in(result.get(0));
        } catch (TaskExecutionException e) {
            throw new ResolveDependencyStrategyException("Error during task execution!", e);
        } catch (ResolutionException e) {
            throw new ResolveDependencyStrategyException("Can't resolve query objects: ", e);
        } catch (PoolGuardException e) {
            throw new ResolveDependencyStrategyException("Error during try get connection to database!", e);
        } catch (InvalidArgumentException | ChangeValueException | ReadValueException e) {
            throw new ResolveDependencyStrategyException("Can't construct query", e);
        }
    }

    private IObject prepareSearchQuery(final String type, final String status)
        throws ResolutionException, ChangeValueException, InvalidArgumentException {

        IKey iObjectKey = Keys.getOrAdd(IObject.class.getCanonicalName());
        IObject page = IOC.resolve(iObjectKey);
        IObject searchQuery = IOC.resolve(iObjectKey);
        IObject searchFilter = IOC.resolve(iObjectKey);
        IObject typeCriteria = IOC.resolve(iObjectKey);
        IObject statusCriteria = IOC.resolve(iObjectKey);

        collectionNameF.out(searchQuery, this.collectionName);

        conditionField.eq().out(typeCriteria, type);
        conditionField.eq().out(statusCriteria, status);

        statusF.out(searchFilter, statusCriteria);
        typeF.out(searchFilter, typeCriteria);

        searchQueryField.pageNumber().out(page, 1);
        searchQueryField.pageSize().out(page, 1);

        searchQueryField.page().out(searchQuery, page);
        searchQueryField.filter().out(searchQuery, searchFilter);

        return searchQuery;
    }
}
