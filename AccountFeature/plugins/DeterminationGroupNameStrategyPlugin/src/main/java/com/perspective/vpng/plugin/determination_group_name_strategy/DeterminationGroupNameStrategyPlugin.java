package com.perspective.vpng.plugin.determination_group_name_strategy;

import com.perspective.vpng.strategy.determination_group_name.DeterminationGroupNameStrategy;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.interfaces.iresolve_dependency_strategy.exception.ResolveDependencyStrategyException;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

/**
 * Plugin for {@link DeterminationGroupNameStrategy}
 */
public class DeterminationGroupNameStrategyPlugin implements IPlugin {

    private static final String GROUP_NAME_PROPERTY_COLLECTION_NAME = "group_name_property";

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor with parameters
     * @param bootstrap the bootstrap
     */
    public DeterminationGroupNameStrategyPlugin(final IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }


    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("DeterminationGroupNameStrategyPlugin");
            item
                .process(() -> {
                    try {
                        Object connectionOptions = IOC.resolve(Keys.getOrAdd("PostgresConnectionOptions"));
                        IPool connectionPool = IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
                        IOC.register(
                            Keys.getOrAdd("determinationGroupNameStrategy"),
                            new DeterminationGroupNameStrategy(
                                GROUP_NAME_PROPERTY_COLLECTION_NAME,
                                connectionPool
                            )
                        );
                    } catch (ResolutionException ex) {
                        throw new ActionExecuteException("DeterminationGroupNameStrategyPlugin can't load: " +
                            "can't get DeterminationGroupNameStrategyPlugin key", ex
                        );
                    } catch (RegistrationException | ResolveDependencyStrategyException ex) {
                        throw new ActionExecuteException(ex.getMessage(), ex);
                    }
                });

            bootstrap.add(item);
        } catch (InvalidArgumentException ex) {
            throw new PluginException("Can't get BootstrapItem from one of reason", ex);
        }
    }
}
