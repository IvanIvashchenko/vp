package com.perspective.vpng.plugin.user_history;

import com.perspective.vpng.actor.user_history.UserHistoryActor;
import com.perspective.vpng.actor.user_history.wrapper.UserHistoryParams;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.iaction.IPoorAction;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.ikey.IKey;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@PrepareForTest({IOC.class, Keys.class, UserHistoryPlugin.class})
@RunWith(PowerMockRunner.class)
public class UserHistoryPluginTest {

    private IBootstrap bootstrap;
    private IPlugin plugin;

    @Before
    public void setUp() throws Exception {
        mockStatic(IOC.class);
        mockStatic(Keys.class);

        bootstrap = mock(IBootstrap.class);
        plugin = new UserHistoryPlugin(bootstrap);
    }

    @Test
    public void Should_CorrectLoadPlugin() throws Exception {
        BootstrapItem item = mock(BootstrapItem.class);
        whenNew(BootstrapItem.class).withArguments("UserHistoryActorPlugin").thenReturn(item);

        plugin.load();

        verifyNew(BootstrapItem.class).withArguments("UserHistoryActorPlugin");

        ArgumentCaptor<IPoorAction> actionArgumentCaptor = ArgumentCaptor.forClass(IPoorAction.class);

        verify(item).process(actionArgumentCaptor.capture());
        verify(bootstrap).add(item);

        IKey userPaymentHistoryActorKey = mock(IKey.class);
        when(Keys.getOrAdd("UserHistoryActor")).thenReturn(userPaymentHistoryActorKey);

        actionArgumentCaptor.getValue().execute();

        verifyStatic();
        Keys.getOrAdd("UserHistoryActor");

        ArgumentCaptor<ApplyFunctionToArgumentsStrategy> argumentCaptor = ArgumentCaptor.forClass(ApplyFunctionToArgumentsStrategy.class);

        verifyStatic();
        IOC.register(eq(userPaymentHistoryActorKey), argumentCaptor.capture());

        IObject configObj = mock(IObject.class);
        IKey configKey = mock(IKey.class);
        UserHistoryParams params = mock(UserHistoryParams.class);
        when(Keys.getOrAdd(UserHistoryParams.class.getCanonicalName())).thenReturn(configKey);
        when(IOC.resolve(configKey, configObj)).thenReturn(params);

        UserHistoryActor actor = mock(UserHistoryActor.class);
        whenNew(UserHistoryActor.class).withArguments(params).thenReturn(actor);
        argumentCaptor.getValue().resolve(configObj);
        verifyNew(UserHistoryActor.class);
    }

    @Test(expected = PluginException.class)
    public void ShouldThrowException_When_InternalExceptionIsThrown() throws Exception {

        whenNew(BootstrapItem.class).withArguments("UserHistoryActorPlugin").thenThrow(new InvalidArgumentException(""));
        plugin.load();
    }
}