package com.perspective.vpng.plugin.user_history;

import com.perspective.vpng.actor.user_history.UserHistoryActor;
import com.perspective.vpng.actor.user_history.wrapper.UserHistoryParams;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.interfaces.ipool.IPool;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.database_postgresql.postgres_connection.wrapper.ConnectionOptions;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_item.BootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap_item.IBootstrapItem;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.IPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.iplugin.exception.PluginException;
import info.smart_tools.smartactors.iobject.ifield.IField;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.iobject.iobject.exception.ReadValueException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;
import info.smart_tools.smartactors.base.interfaces.iaction.exception.ActionExecuteException;

/**
 * Plugin for UserHistoryActor
 */
public class UserHistoryPlugin implements IPlugin {

    private final IBootstrap<IBootstrapItem<String>> bootstrap;

    /**
     * Constructor with parameters
     * @param bootstrap the bootstrap
     */
    public UserHistoryPlugin(IBootstrap<IBootstrapItem<String>> bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void load() throws PluginException {
        try {
            IBootstrapItem<String> item = new BootstrapItem("UserHistoryActorPlugin");

            item
//                    .after("IOC")
//                    .before("starter")
                    .process(() -> {
                        try {
                            IField collectionNameField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "collectionName");
                            IField providerCollectionNameField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerCollectionName");
                            IField providerIdFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerIdFieldName");
                            IField providerIdSearchQueryFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerIdSearchQueryFieldName");
                            IField userIdFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "userIdFieldName");
                            IField statusFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "statusFieldName");
                            IField typeFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "typeFieldName");
                            IField dateFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "dateFieldName");
                            IField sumFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "sumFieldName");
                            IField guidFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "guidFieldName");
                            IField operationFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "operationFieldName");
                            IField groupNameFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "groupNameFieldName");
                            IField numberFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "numberFieldName");
                            IField paymentIdFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "paymentIdFieldName");
                            IField descriptionFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "descriptionFieldName");
                            IField providerDescriptionFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "providerDescriptionFieldName");
                            IField logoFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "logoFieldName");
                            IField amountFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "amountFieldName");
                            IField enabledProviderFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "enabledProviderFieldName");
                            IField enabledFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "enabledFieldName");
                            IField humanDateFNField = IOC.resolve(Keys.getOrAdd(IField.class.getCanonicalName()), "humanDateFieldName");
                            IOC.register(Keys.getOrAdd(UserHistoryParams.class.getCanonicalName()),
                                    new ApplyFunctionToArgumentsStrategy(
                                            args -> {
                                                IObject params = (IObject) args[0];
                                                return new UserHistoryParams() {
                                                    @Override
                                                    public String getProviderIdSearchQueryFieldName() throws ReadValueException {
                                                        try {
                                                            return providerIdSearchQueryFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException("Can't read collection name", e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getProviderDescriptionFieldName() throws ReadValueException {
                                                        try {
                                                            return providerDescriptionFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException("Can't read collection name", e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getCollectionName() throws ReadValueException {
                                                        try {
                                                            return collectionNameField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException("Can't read collection name", e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getProviderCollectionName() throws ReadValueException {
                                                        try {
                                                            return providerCollectionNameField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException("Can't read provider collection name", e);
                                                        }
                                                    }

                                                    @Override
                                                    public IPool getConnectionPool() throws ReadValueException {
                                                        try {
                                                            ConnectionOptions connectionOptions = IOC.resolve(
                                                                    Keys.getOrAdd("PostgresConnectionOptions")
                                                            );
                                                            return IOC.resolve(Keys.getOrAdd("PostgresConnectionPool"), connectionOptions);
                                                        } catch (Exception e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getProviderIdFieldName() throws ReadValueException {
                                                        try {
                                                            return providerIdFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getUserIdFieldName() throws ReadValueException {
                                                        try {
                                                            return userIdFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getStatusFieldName() throws ReadValueException {
                                                        try {
                                                            return statusFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getTypeFieldName() throws ReadValueException {
                                                        try {
                                                            return typeFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getDateFieldName() throws ReadValueException {
                                                        try {
                                                            return dateFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getSumFieldName() throws ReadValueException {
                                                        try {
                                                            return sumFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getGuidFieldName() throws ReadValueException {
                                                        try {
                                                            return guidFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getOperationFieldName() throws ReadValueException {
                                                        try {
                                                            return operationFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getGroupNameFieldName() throws ReadValueException {
                                                        try {
                                                            return groupNameFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getNumberFieldName() throws ReadValueException {
                                                        try {
                                                            return numberFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getPaymentIdFieldName() throws ReadValueException {
                                                        try {
                                                            return paymentIdFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getDescriptionFieldName() throws ReadValueException {
                                                        try {
                                                            return descriptionFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getLogoFieldName() throws ReadValueException {
                                                        try {
                                                            return logoFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getAmountFieldName() throws ReadValueException {
                                                        try {
                                                            return amountFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getEnabledProviderFieldName() throws ReadValueException {
                                                        try {
                                                            return enabledProviderFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getEnabledFieldName() throws ReadValueException {
                                                        try {
                                                            return enabledFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }

                                                    @Override
                                                    public String getHumanDateFieldName() throws ReadValueException {
                                                        try {
                                                            return humanDateFNField.in(params, String.class);
                                                        } catch (InvalidArgumentException e) {
                                                            throw new ReadValueException(e);
                                                        }
                                                    }
                                                };
                                            }));
                            IOC.register(Keys.getOrAdd("UserHistoryActor"), new ApplyFunctionToArgumentsStrategy(
                                    args -> {
                                        try {
                                            IObject config = (IObject) args[0];
                                            return new UserHistoryActor(
                                                    IOC.resolve(Keys.getOrAdd(UserHistoryParams.class.getCanonicalName()), config)
                                            );
                                        } catch (Exception e) {
                                            throw new RuntimeException("Error during resolving UserHistoryActor", e);
                                        }
                                    }
                            ));
                        } catch (ResolutionException e) {
                            throw new ActionExecuteException("UserHistory plugin can't load: can't get key");
                        } catch (InvalidArgumentException e) {
                            throw new ActionExecuteException("UserHistory plugin can't load: can't create strategy");
                        } catch (RegistrationException e) {
                            throw new ActionExecuteException("UserHistory plugin can't load: can't register new strategy");
                        }
                    });
            bootstrap.add(item);
        } catch (InvalidArgumentException e) {
            throw new PluginException("Can't get BootstrapItem", e);
        }
    }
}
