package com.perspective.vpng.plugin.get_sms_reminds_actor;

import com.perspective.vpng.actor.get_sms_reminds.GetSmsRemindsActor;
import info.smart_tools.smartactors.base.exception.invalid_argument_exception.InvalidArgumentException;
import info.smart_tools.smartactors.base.strategy.apply_function_to_arguments.ApplyFunctionToArgumentsStrategy;
import info.smart_tools.smartactors.feature_loading_system.bootstrap_plugin.BootstrapPlugin;
import info.smart_tools.smartactors.feature_loading_system.interfaces.ibootstrap.IBootstrap;
import info.smart_tools.smartactors.iobject.iobject.IObject;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.RegistrationException;
import info.smart_tools.smartactors.ioc.iioccontainer.exception.ResolutionException;
import info.smart_tools.smartactors.ioc.ioc.IOC;
import info.smart_tools.smartactors.ioc.named_keys_storage.Keys;

public class GetSmsRemindsActorPlugin extends BootstrapPlugin {
    /**
     * Constructor
     * @param bootstrap the bootstrap item
     */
    public GetSmsRemindsActorPlugin(final IBootstrap bootstrap) {
        super(bootstrap);
    }


    @BootstrapPlugin.Item("GetSmsRemindsActorPlugin")
    public void item()
            throws ResolutionException, RegistrationException, InvalidArgumentException {
        IOC.register(Keys.getOrAdd("GetSmsRemindsActor"),
                new ApplyFunctionToArgumentsStrategy((args) -> new GetSmsRemindsActor((IObject) args[0])));
    }
}